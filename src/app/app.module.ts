import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { AuthHttp, AuthConfig, AUTH_PROVIDERS, provideAuth} from 'angular2-jwt';

import { ENV_PROVIDERS } from './environment';
import { routing } from './app.routing';
import { AuthService } from './services/auth.service';
import { NavigationService } from './services/navigation.service';
import { AuthGuard  } from './guards/auth.guard';
import { AccessGuard  } from './guards/access.guard';

// App is our top level component
import { App } from './app.component';
import { Configuration } from './app.constants';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';

import { Ng2Webstorage } from 'ng2-webstorage';
//import { ChartsModule } from 'ng2-charts';
import * as Chart from 'chart.js';
window['Chart'] = Chart;
import { AgmCoreModule } from 'angular2-google-maps/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import * as spinner from 'ng2-spin-kit/app/spinners';
import * as $ from 'jquery';
// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [App],
  declarations: [
    App
  ],
  imports: [ // import Angular's modules
   NgMultiSelectDropDownModule.forRoot(),
  //AngularMultiSelectModule,
    BrowserModule,
    HttpModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    Ng2Webstorage,
    routing,
   // ChartsModule,
    AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD-oftP9oMjfzi9ZUw-woK91ik15yGsE6I'
       })
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    provideAuth({
       headerPrefix: 'Bearer',
       tokenName: 'id_token',
       noClientCheck: true
    }),
    AuthService,
    NavigationService,
    AuthGuard,
    AccessGuard,
    Configuration,
    CookieService
  ]
})

export class AppModule {

  constructor(public appRef: ApplicationRef, public appState: AppState) {
  }

  hmrOnInit(store: StoreType) {
    if (!store || !store.state) return;
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }
    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
