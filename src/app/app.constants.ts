import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
//localhost
//public Server: string = ENV == 'production' ? "http://aph-api.fayastage.com/": 'http://localhost:51053/';
// Development Server
//public Server: string = ENV == 'production' ? "http://aph-api.fayastage.com/": 'http://103.79.223.60:6060/';
// Testing Server for QA
public Server: string = ENV == 'production' ? "http://aph-api.fayastage.com/": 'http://192.168.1.24:6061/';
// Live Server
//public Server: string = 'http://192.168.1.97:82/api/';//'http://192.168.0.169:81/';http://192.168.1.97:82/api/
//public Server: string = 'https://fms.tristar-group.co/api/';


    public ApiUrl: string = "api/v1/";
    public rocketShipServerUrl: string = ENV == 'production' ? "https://printserver.local:8081": 'http://192.168.0.151:8080';
    public ServerWithApiUrl = this.Server + this.ApiUrl;
    public itemsPerPage = 10;
    public rows = ['10','20', '30', '40', '50', '100'];
    public pickval ="picking_up";
    public presentationTimerValue = 0;
}
