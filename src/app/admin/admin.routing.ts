import { Routes, RouterModule } from '@angular/router';
import { Admin } from './admin.component';
import { AuthGuard } from '../guards/auth.guard';
import { AccessGuard } from '../guards/access.guard';
import { UserManagementComponent } from 'app/admin/usermanagement/usermanagement.component';
import { UserManagementListComponent } from 'app/admin/usermanagement/list/usermanagement-list.component';
import { UserManagementAddComponent } from 'app/admin/usermanagement/add/usermanagement-add.component';
import { UserManagementEditComponent } from 'app/admin/usermanagement/edit/usermanagement-edit.component';
import { StockAdjustmentComponent } from 'app/admin/stockadjustment/stockadjustment.component';
import { StockAdjustmentListComponent } from 'app/admin/stockadjustment/list/stockadjustment-list.component';
import { StockAdjustmentAddComponent } from 'app/admin/stockadjustment/add/stockadjustment-add.component';
import { StockAdjustmentEditComponent } from 'app/admin/stockadjustment/edit/stockadjustment-edit.component';
import { RolesComponent } from 'app/admin/rolesandprivilages/roles.component';
import { RolesAddComponent } from 'app/admin/rolesandprivilages/add/roles-add.component';
import { RolesListComponent } from 'app/admin/rolesandprivilages/list/roles-list.component';
import { RolesEditComponent } from 'app/admin/rolesandprivilages/edit/roles-edit.component';
import { RolesViewComponent } from 'app/admin/rolesandprivilages/view/roles-view.component';
import { PhysicalDipComponent } from 'app/admin/physical-dip/physical-dip.component';
import { PasswordResetComponent } from 'app/admin/password-reset/password-reset.component';
import { PhysicalDipListComponent } from 'app/admin/physical-dip/list/physical-dip-list.component';
import { PhysicalDipAddComponent } from 'app/admin/physical-dip/add/physical-dip-add.component';
import { PhysicalDipEditComponent } from 'app/admin/physical-dip/edit/physical-dip-edit.component';
import { PhysicalDipViewComponent } from 'app/admin/physical-dip/view/physical-dip-view.component';
import { ReportsComponent } from 'app/admin/reports/reports.component';
import { ReportsListComponent } from 'app/admin/reports/list/reports-list.component';
import { RefuellingHistoryComponent } from 'app/admin/refuellinghistory/refuellinghistory.component';
import { RefuellingHistoryListComponent } from 'app/admin/refuellinghistory/list/refuellinghistory-list.component';
import { SystemPreferenceComponent } from 'app/admin/systempreference/systempreference.component';
import { SystemPreferenceAddComponent } from 'app/admin/systempreference/add/systempreference-add.component';
 
const routes: Routes = [
  {
    path: '',
    component: Admin,
    canActivate: [AuthGuard, AccessGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => System.import('./dashboard/dashboard.module') },
      { path: 'fuel-issuance-summary', loadChildren: () => System.import('./fuelissuance/fuel-issuance.module') },
      { path: 'new-fuel-issuance', loadChildren: () => System.import('./fuelissuance/fuel-issuance.module') },
      { path: 'receipt-and-transfer', loadChildren: () => System.import('./receiptandtransfer/transaction.module') },
      { path: 'master', loadChildren: () => System.import('./master/master.module') },
      {
        path: 'roles-and-privileges',
        component: RolesComponent,
        children: [
          { path: '', component: RolesListComponent },
          { path: 'list', component: RolesListComponent },
          { path: 'add', component: RolesAddComponent },
          { path: 'edit/:id', component: RolesEditComponent },
          { path: 'view/:id', component: RolesViewComponent }
        ]
      },

      {
        path: 'user-management',
        component: UserManagementComponent,
        children: [
          { path: '', component: UserManagementListComponent },
          { path: 'list', component: UserManagementListComponent },
          { path: 'add', component: UserManagementAddComponent },
          { path: 'edit/:id', component: UserManagementEditComponent },
        ]
      },
     

      // Stock Adjustment
      {
        path: 'stock-adjustment',
        component: StockAdjustmentComponent,
        children: [
          { path: '', component: StockAdjustmentListComponent },
          { path: 'list', component: StockAdjustmentListComponent },
          { path: 'add', component: StockAdjustmentAddComponent },
          { path: 'view/:id', component: StockAdjustmentEditComponent },
        ]
      },
      // Stock Adjustment

      // Physical Dip
      {
        path: 'physical-dip',
        component: PhysicalDipComponent,
        children: [
          { path: '', component: PhysicalDipListComponent },
          { path: 'list', component: PhysicalDipListComponent },
          { path: 'add', component: PhysicalDipAddComponent },
          { path: 'edit/:id/:tankId', component: PhysicalDipEditComponent },
          { path: 'view/:id', component: PhysicalDipViewComponent },
        ]
      },
        // Physical Dip

        // Reports
      {
        path: 'reports',
        component: ReportsComponent,
        children: [
          { path: '', component: ReportsListComponent },
        ]
      },
    // Reports

    // Refuelling History
    {
      path: 'refuelling-history',
      component: RefuellingHistoryComponent,
      children: [
        { path: '', component: RefuellingHistoryListComponent },
        { path: 'list', component: RefuellingHistoryListComponent },

      ]
    },
    // Refuelling History
    // System Preference
    {
      path: 'system-preferences',
      component: SystemPreferenceComponent,
      children: [
        { path: '', component: SystemPreferenceAddComponent },
        { path: 'add', component: SystemPreferenceAddComponent },

      ]
    },
    // System Preferenceentation Mode
    ]
  },
  {
    path: 'forgetPassword/:token', component: PasswordResetComponent// <-- use when pwd reset token is added in api
   // path: 'forgetPassword', component: PasswordResetComponent
  },
];

export const routing = RouterModule.forChild(routes);