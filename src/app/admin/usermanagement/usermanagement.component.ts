import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'usermanagement',
    template: `<router-outlet></router-outlet>`
})
export class UserManagementComponent {
	constructor(private router: Router) {
	}
}
