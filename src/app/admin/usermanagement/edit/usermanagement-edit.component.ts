import { Component, OnInit } from '@angular/core';
import { SublocalityService } from "../services/sublocality.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserManagementFormService } from 'app/admin/usermanagement/services/usermanagement.service';

@Component({
  selector: 'edit',
  template: require('./usermanagement-edit.html')
})
export class UserManagementEditComponent implements OnInit {
  id: any;
  public error = {};
  public success = '';
  public usermanagement: any;
  public page = 'edit';
  showLoader: boolean = false;

  constructor(private router: Router, private route: ActivatedRoute,
    private service: UserManagementFormService) {

  }
  ngOnInit() {
    this.route.params.forEach((params: Params) => {

      this.id = params['id'];
      this.usermanagement = this.service.getusermanagement(this.id);
    });
  }

  onSave(bu: any) {
    this.showLoader = true;
    const Data = {
      Id: this.id,
      FirstName: bu.firstName,
      LastName: bu.lastName,
      MobileNumber: bu.mobileNumber,
      UNID: bu.unid,
      UserImage: bu.userImage,
      Email: bu.email,
      UserRoleId: bu.userRoleId,
      Password: bu.password,
      ActiveStatusId: bu.status,
      // countryId:bu.countryId,
      BusinessUnitString:bu.businessunitId.toString(),
      Site: bu.siteId,
      IsDeleted: 0,

    }
    console.log(Data)
    this.service.Save(Data).then(r => {
      this.showLoader = false;
      this.success = 'User Updated Successfully!';
      jQuery('html, body').animate({ scrollTop: 0 }, { duration: 1000 });
      setTimeout(function () {
        this.success = '';
        this.router.navigate(['./home/user-management']);
      }.bind(this), 3000);
    }).catch(r => {
      this.handleError(r);
    })
  }
  handleError(e: any) {
    //     return Promise.reject(error.json());
    this.error = e;
    let detail = e.detail
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}
