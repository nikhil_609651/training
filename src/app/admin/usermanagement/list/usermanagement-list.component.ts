import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserManagementFormService } from 'app/admin/usermanagement/services/usermanagement.service';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'usermanagement-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./usermanagement-list.html')
})
export class UserManagementListComponent implements OnInit {
  uploadedimage = '';
  deletemessage: string;
  user_deleted_id: any;
  selectedstatusAll: any;
  selectedStatus: any;
  checkStatusArr: any[] = [];
  selectedroleAll: any;
  checkArr: any[] = [];
  statuslist: any;
  rolelist: any;

  attachmentGroup: any[];
  attachments: any;
  attachmentmessage: string;
  uId: any;
  viewForm: FormGroup;
  public usermanagementlist: any = [];
  public start: number = 1;
  public loading: boolean;
  public totalfeilds = 0;
  public page: number = 1;
  public itemsPerPage: number = 3;
  public maxSize: number = 5;
  public numPages: number = 2;
  public length: number = 5;
  public next = '';
  public rows: Array<any> = [];
  API_URL: string;

  // For headers
  public columns: Array<any> = [
    { title: 'ID', name: 'id', sort: true, filter: false },
    { title: 'UNID', name: 'unid', sort: true, filter: false },
    { title: 'Name', name: 'name', sort: true, filter: false },
    { title: 'Email', name: 'email', sort: true, filter: false },
    { title: 'Role', name: 'role', sort: true, filter: true },
    { title: 'Status', name: 'status', sort: true, filter: true },
    { title: 'Actions', name: 'actions', sort: false, filter: false }
  ];
  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;
  
  constructor(private router: Router,
    private service: UserManagementFormService,
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {
        /**user privileges**/	
        this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
          this.privileges = privileges;
          this.showListPage = true;
        });
	  
        this.itemsPerPage = configuration.itemsPerPage;
        this.params.set('limit', configuration.itemsPerPage.toString());
        //this.params.set('countryfilter', '');
        this.params.set('StatusFilter', '');
        this.rows = configuration.rows;
        this.viewForm = fb.group({

          'firstname': ['',],
          'lastname': ['',],
          'unidnumber': [''],
          'email': [''],
          'mobile': [''],
          'id': [''],
          'role': [''],
          'FileType': [],
        });
        var vForm = this.viewForm;
  }
  // tslint:disable-next-line:member-ordering
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-bordered']
  };

  ngOnInit() {
    this.getUserManagementList(this.params);
    this.totalfeilds = this.columns.length;
    this.rolesForFilter();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  // For getting BusinessUnit List
  getUserManagementList(params: any) {
    this.loading = true;
    this.service.getUserManagementList(params).then(response => {
      this.usermanagementlist = response['result'];
      if (this.usermanagementlist.length > 0) {
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      }
      else {
        this.page = 1;
        this.getAllLists(this.params);
      }
    }).catch(r => {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.service.getUserManagementList(params).then(response => {
      this.usermanagementlist = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r => {
      this.handleError(r);
    });
  }

  addnew() {
    this.router.navigate(['./home/user-management/add']);
  }

  edit(id) {
    this.router.navigate(['./home/user-management/edit/', id]);
  }

  // For Sorting
  Sort(param, order) {
    if (order === 'desc') {
      this.params.set('ordering', '-' + param);
    }
    if (order === 'asc') {
      this.params.set('ordering', param);
    }
    this.getUserManagementList(this.params);
  }

  view(data) {
    this.viewForm.controls['id'].setValue(data.id);
    this.viewForm.controls['unidnumber'].setValue(data.unid);
    this.viewForm.controls['mobile'].setValue(data.mobileNumber);
    this.viewForm.controls['role'].setValue(data.userRoles.name);
    this.viewForm.controls['email'].setValue(data.email);
    this.viewForm.controls['firstname'].setValue(data.firstName);
    this.viewForm.controls['lastname'].setValue(data.lastName);
    if(data.userImage) {
      this.uploadedimage = this.configuration.Server + data.userImage;
    }
  }

  viewclose() {
    setTimeout(function () {
      jQuery('#ViewDetails').modal('toggle');
    }.bind(this), 0);
  }

  // Filter
  filterArray :any = [];
  selectAll(item, event) {
    if (item === 'Role') {
      this.filterArray = [];
      for (var i = 0; i < this.rolelist.length; i++) {
        this.rolelist[i].selected = this.selectedroleAll;
        if (event.target.checked) {
          this.filterArray.push(this.rolelist[i].id.toString());
        } else {
          this.filterArray = [];
        }
      }
      this.params.set('RoleFilter', this.filterArray.toString());
    }
  }

  checkIfAllSelected(option, event, item) {
    if (item == 'Role') {
      this.selectedroleAll = this.rolelist.every(function (item: any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkArr.indexOf(key);
      if (event.target.checked) {
        this.checkArr.push(event.target.value);
      } else {
        this.checkArr.splice(index, 1);
      }
      this.params.set('RoleFilter', this.checkArr.toString());
    }

    if (item === 'Status') {
      var key = event.target.value.toString();
      var index = this.checkStatusArr.indexOf(key);
      if (event.target.checked) {
        this.checkStatusArr.push(event.target.value);
      } else {
        this.checkStatusArr.splice(index, 1);
      }
      this.params.set('StatusFilter', this.checkStatusArr.toString());
    }
  }

  // For Applying Filtering to List
  apply(item) {
    this.getUserManagementList(this.params);
    if (item === 'Role') {
      setTimeout(function () {
        jQuery('#RoleFilter').modal('toggle');
      }.bind(this), 0);
    }
    if (item === 'Status') {
      setTimeout(function () {
        jQuery('#StatusFilter').modal('toggle');
      }.bind(this), 0);
    }
    jQuery('RoleFilter').modal('hide');
    jQuery('ViewStatusModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  // Delete User
  deleteuser(pr) {
    this.user_deleted_id = pr;
  }

  deletemessageclear() {
    this.deletemessage = '';
  }

  deleteconfirm(pr) {
    // this.user_deleted_id= pr.id;
    this.service.deleteUser(this.user_deleted_id).then(r => {
      this.getUserManagementList(this.params);
      this.deletemessage = "User Deleted Successfully";
    }).catch(r => {
      this.handleError(r);
      this.deletemessage = r.name[0];
    })
  }

  rolesForFilter() {
    this.service.getRoles().then(r => {
      this.rolelist = r.result;
    }).catch(r => {
      this.handleError(r);
    })
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    this.getUserManagementList(this.params);
  }

  private handleError(e: any) {
    let detail = e.detail;
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}