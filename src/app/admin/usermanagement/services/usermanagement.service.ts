import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../app.constants';

@Injectable()
export class UserManagementFormService {
    private uploadUrl = '';
    public listUrl = '';
    public userIdUrl ='';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public roledropdownUrl = '';
    public usermanagementurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    public countrydropdownUrl='';
    public userroledropdownUrl ='';
    public businessunitUrl = '';
    public sitelistUrl ='';
    public businessunitByCountryUrl = '';
    public siteByBUUrl = '';
    public rolesUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'UserManagement/User/';
        this.saveUrl = _configuration.ServerWithApiUrl+'UserManagement/SaveUser';
        this.updateUrl = _configuration.ServerWithApiUrl+'UserManagement/SaveUser';
        this.deleteUrl = _configuration.ServerWithApiUrl+'UserManagement/DeleteUserByID?id=';
        this.roledropdownUrl = _configuration.ServerWithApiUrl+'UserManagement/Country';
        this.usermanagementurl = _configuration.ServerWithApiUrl+'UserManagement/GetUserByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'UserManagement/GetAllUser?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'UserManagement/DeleteUser?Id=';
        this.uploadUrl = _configuration.ServerWithApiUrl+'UserManagement/SaveUserImage';
        this.countrydropdownUrl = _configuration.ServerWithApiUrl+'Masters/GetAllCountries';
        this.businessunitUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
        this.sitelistUrl=_configuration.ServerWithApiUrl+'Masters/Site';
        this.userroledropdownUrl =_configuration.ServerWithApiUrl+'UserManagement/UserRoles'
        this.businessunitByCountryUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnitByCountryId?CountryFilter=';
        this.siteByBUUrl = _configuration.ServerWithApiUrl+'Masters/SiteByBusinessUnitId?BusinessFilter=';
        this.userIdUrl =_configuration.ServerWithApiUrl+'UserManagement/GetUserId';
        this.rolesUrl = _configuration.ServerWithApiUrl+'UserManagement/UserRoles';
    }

    // site by BUs
    getSiteListByBusinessUnit(BusinessFilter: string): Promise<any> {
        return this.authHttp.get(this.siteByBUUrl + BusinessFilter)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //     // BusinessUnit by countryList
    getBusinessUnitListByCountryList(CountryFilter: string): Promise<any> {
        return this.authHttp.get(this.businessunitByCountryUrl + CountryFilter)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }


    // List API for User
    getUserManagementList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getUserId(): Promise<any>{
        return this.authHttp.get(this.userIdUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getusermanagement(id: string) {
        return this.authHttp.get(this.usermanagementurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    Upload(um: any, fn){
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.uploadUrl, um)
            .subscribe(data => {
                            fn(data)
                        },
                        err => console.log(err),
                        () => console.log('Complete')
            );
    }

    Save(bu: any): Promise<any>  {

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    // UserRole
    getUserRoleList(): Promise<any> {
        return this.authHttp.get(this.userroledropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Country
    getCountryList(): Promise<any> {
        return this.authHttp.get(this.countrydropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // BusinessUnit
    getBusinessUnitList(): Promise<any> {
        return this.authHttp.get(this.businessunitUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Site
    getSiteList(): Promise<any>{
        return this.authHttp.get(this.sitelistUrl,)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    deleteUser(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getallusermanagementattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getRoles() {
        return this.authHttp.get(this.rolesUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}