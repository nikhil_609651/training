import { Component, OnInit } from '@angular/core';
import { SublocalityService } from "../services/sublocality.service";
import { Router } from '@angular/router';
import { UserManagementFormService } from 'app/admin/usermanagement/services/usermanagement.service';
import { Configuration } from 'app/app.constants';

@Component({
  selector: 'add',
  template: require('./usermanagement-add.html')
})
export class UserManagementAddComponent {

  public error = {};
  public success = '';
  public usermanagement: any;
  public page = '';
  showLoader: boolean = false;
  constructor(private router: Router,
    private service: UserManagementFormService, private configuration: Configuration) {


  }
  ngOnInit() {
    this.page = 'add';
  }
  onSave(bu: any) {
    this.showLoader = true;
    if(bu.password == "") {
      bu.password = 'admin@123'
    }
    const params = {
      Id: 0,
      FirstName: bu.firstName,
      LastName: bu.lastName,
      MobileNumber: bu.mobileNumber,
      UNID: bu.unid,
      UserImage: bu.userImage,
      Email: bu.email,
      UserRoleId: bu.userRoleId,
      Password: bu.password,
      ActiveStatusId: bu.status,
      Site: bu.siteId,
      IsDeleted: 0,
      BusinessUnitString: bu.businessunitId.toString(),
    }
    this.service.Save(params).then(r => {
      this.showLoader = false;
      this.success = 'User Created Successfully!';
      jQuery('html, body').animate({ scrollTop: 0 }, { duration: 1000 });
      setTimeout(function () {
        this.success = '';
        this.router.navigate(['./home/user-management']);
      }.bind(this), 3000);
    }).catch(r => {
      this.showLoader = false;
      this.handleError(r);
    })
  }


  handleError(e: any) {
    //     return Promise.reject(error.json());
    this.error = e;
    let detail = e.detail
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }


}