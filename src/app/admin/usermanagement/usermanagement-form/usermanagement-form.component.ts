import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserManagementFormService } from 'app/admin/usermanagement/services/usermanagement.service';
import { Configuration } from 'app/app.constants';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'usermanagement-form',
  template: require('./usermanagement-form.html'),
  styleUrls: ['./usermanagement-form.scss']
})

export class UserManagementFormComponent {
  id: number = 0;
  params: any;
  userId: string;
  length: any;
  selectedCountry: any[];
  selectedBusinessUnit: any[];
  buIdString: string = '';
  countryIdString: string;
  selectedSite: any[];
  countryidarray: any = [];
  businessunitidarray: any = [];
  siteidarray: any = [];
  firstNameRequired: string;
  lastNameRequired: string;
  unidRequired: string;
  emailRequired: string;
  invalidEmail: string;
  userRoleIdRequired: string;
  countryIdRequired: string;
  businessunitIdRequired: string;
  siteIdRequired: string;
  siteGroups: any = [];
  businessunitGroups: any;
  countryGroups: any;
  status: any = 1;
  savebutton: boolean = true;
  public success = '';
  @Input() usermanagement;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  @ViewChild('fileInput') fileInputVariable: any;
  usermanagementForm: FormGroup;
  public show_password = true;
  public hide_password = false;
  public show_confirm_password = true;
  public hide_confirm_password = false;
  public passwordPattern = ".{7,20}";
  newPassword: string = '';
  confirmPassword: string = '';
  @ViewChild('ShowPassword') ShowPassword;
  @ViewChild('ShowConfirm') ShowConfirm;
  public loading: boolean;
  public formSubmited = false;
  public usermanagement_id = '';
  public coptions = [];
  public coptionss = [];
  public boptions = [];
  public boptionss = [];
  public soptions = [];
  public soptionss = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  SiteSettings = {};

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }

  privilegeSubscription : Subscription;

  constructor(private router: Router, private fb: FormBuilder, private route: ActivatedRoute,
    private usermanagementservice: UserManagementFormService, private _sharedService : SharedService,
    private configuration: Configuration) {
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
      this.privileges = privileges;
    });
    this.usermanagementForm = fb.group({
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'unid': ['', Validators.required],
      'email': ['', Validators.required],
      'mobileNumber': ['',],
      'id': ['',this.userId],
      'userRoleId': ['', Validators.required],
      'siteId': ['', Validators.required],
      'userImage': [''],
      'countryId': ['', Validators.required],
      'businessunitId': ['', Validators.required],
      'password': [''],
      'status': [1],
    });
    var cForm = this.usermanagementForm;
    this.onselectednew_country();
  }

  //public  uploadedimage: any;
  uploadedimage = '';
  sendimage = '';
  public roleList: Array<any> = [];
  ngOnInit() {
    this.getid();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      clearSearchFilter: true,
    };
    this.route.params.forEach((params: Params) => {
      this.usermanagement_id = params['id'];
    });
    this.userroleList();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getid() {
    if (this.page == 'add') {
      this.usermanagementservice.getUserId().then(response => {
        this.length = response;
        this.userId = "E00" + (this.length + 1).toString();
        this.usermanagementForm.controls['id'].setValue(this.userId);
      }).catch(r => {
        this.handleError(r);
      });
    }
  }

  onItemSelect(event, item) {
    if (item === 'Country') {
      this.boptions = [];
      this.boptionss = [];
      this.selectedBusinessUnit = [];
      this.countryidarray.push(event.item_id);
      this.usermanagementForm.controls['countryId'].setValue(this.countryidarray.toString());
    }
    if (this.countryidarray.length > 0) {
      this.countryIdString = '';
      for (var i = 0; i < this.countryidarray.length; i++) {
        this.countryIdString = this.countryidarray[i] + ',' + this.countryIdString;
      }
      this.getBUListByCountryList(this.countryIdString);
    }
    if (item === 'BusinessUnit') {
      this.soptionss = [];
      this.selectedSite = [];
      this.soptions = [];
      this.siteidarray = [];
      this.businessunitidarray.push(event.item_id);
      this.usermanagementForm.controls['businessunitId'].setValue(this.businessunitidarray.toString());
    }
    if (this.businessunitidarray.length > 0) {
      this.buIdString = '';
      for (var i = 0; i < this.businessunitidarray.length; i++) {
        this.buIdString = this.businessunitidarray[i] + ',' + this.buIdString;
      }
      this.getSiteListByBUId(this.buIdString);
    }
    if (item === 'Site') {
      this.siteidarray.push(event.item_id);
      this.usermanagementForm.controls['siteId'].setValue(this.siteidarray.toString());
    }
  }

  onChangeRole(id) {
    if (id == 1001) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 1 });
    }
    else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: -1 });
    }
  }
  onItemDeSelect(event, item) {

    if (item === 'Country') {
      this.boptions = [];
      this.boptionss = [];
      this.selectedBusinessUnit = [];
      this.siteidarray = [];
      let index = this.countryidarray.indexOf(event.item_id);
      this.countryidarray.splice(index, 1);
      this.usermanagementForm.controls['countryId'].setValue(this.countryidarray);
    }
    if (this.countryidarray.length === 0) {
      //this.businessunitList();
    }
    if (this.countryidarray.length > 0) {
      this.countryIdString = '';
      for (var i = 0; i < this.countryidarray.length; i++) {
        this.countryIdString = this.countryidarray[i] + ',' + this.countryIdString;
      }
      this.getBUListByCountryList(this.countryIdString);
    }
    if (item === 'BusinessUnit') {
      this.soptionss = [];
      this.soptions = [];
      this.selectedSite = [];
      this.siteidarray = [];
      let index = this.businessunitidarray.indexOf(event.item_id);
      this.businessunitidarray.splice(index, 1);
      this.usermanagementForm.controls['businessunitId'].setValue(this.businessunitidarray);
    }
    if (this.businessunitidarray.length === 0) {
      //this.siteList();
    }
    if (this.businessunitidarray.length > 0) {
      this.buIdString = '';
      for (var i = 0; i < this.businessunitidarray.length; i++) {
        this.buIdString = this.businessunitidarray[i] + ',' + this.buIdString;
      }
      this.getSiteListByBUId(this.buIdString);
    }
    if (item === 'Site') {
      let index = this.siteidarray.indexOf(event.item_id);
      this.siteidarray.splice(index, 1);
      this.usermanagementForm.controls['siteId'].setValue(this.siteidarray.toString());
    }
  }

  getBUListByCountryList(countryIdString) {
    this.usermanagementservice.getBusinessUnitListByCountryList(countryIdString).then(r => {
      var LongArray = [];
      for (let i = 0; i < r.length; i++) {
        var Obj = {
          item_text: r[i].buName,
          item_id: +r[i].buId,
        }
        LongArray.push(Obj);
        if (i == (r.length - 1)) {
          this.boptionss = LongArray;
          this.onselectedbusinessunit();
        }
      }
    });
  }

  getSiteListByBUId(buIdString) {
    this.usermanagementservice.getSiteListByBusinessUnit(buIdString).then(r => {
      var LongArray = [];
      for (let i = 0; i < r.length; i++) {
        var Obj = {
          item_text: r[i].name,
          item_id: +r[i].siteId,
        }
        LongArray.push(Obj);
        if (i == (r.length - 1)) {
          this.soptionss = LongArray;
          this.onselectedsite();
        }
      }
    });
  }

  onSelectAll(event, item) {
    if (item === 'Country') {
      this.countryidarray = [];
      this.businessunitidarray = [];
      for (let i = 0; i < event.length; i++) {
        this.countryidarray.push(event[i].item_id);
      }
      this.usermanagementForm.controls['countryId'].setValue(this.countryidarray.toString());
      if (this.countryidarray.length > 0) {
        this.countryIdString = '';
        for (var i = 0; i < this.countryidarray.length; i++) {
          this.countryIdString = this.countryidarray[i] + ',' + this.countryIdString;
        }
        this.getBUListByCountryList(this.countryIdString);
      }
    }
    if (item === 'BusinessUnit') {
      this.businessunitidarray = [];
      this.soptionss = [];
      this.soptions = [];
      this.selectedSite = [];
      this.siteidarray = [];
      for (let i = 0; i < event.length; i++) {
        this.businessunitidarray.push(event[i].item_id);
      }
      this.usermanagementForm.controls['businessunitId'].setValue(this.businessunitidarray.toString());
      if (this.businessunitidarray.length > 0) {
        this.buIdString = '';
        for (var i = 0; i < this.businessunitidarray.length; i++) {
          this.buIdString = this.businessunitidarray[i] + ',' + this.buIdString;
        }
        this.getSiteListByBUId(this.buIdString);
      }
    }
    if (item === 'Site') {
      this.siteidarray = [];
      for (let i = 0; i < event.length; i++) {
        this.siteidarray.push(event[i].item_id);
      }
      this.usermanagementForm.controls['siteId'].setValue(this.siteidarray.toString());
    }

  }

  onDeSelectAll(event, item) {

    if (item === 'Country') {

      for (let i = 0; i < event.length; i++) {
        this.countryidarray.splice(event[i].item_id);
      }
      this.countryidarray = [];
      this.usermanagementForm.controls['countryId'].setValue(this.countryidarray.toString());
      if (this.countryidarray.length > 0) {
        this.countryIdString = '';
        for (var i = 0; i < this.countryidarray.length; i++) {
          this.countryIdString = this.countryidarray[i] + ',' + this.countryIdString;
        }
        this.getBUListByCountryList(this.countryIdString);
      }
    }
    if (item === 'BusinessUnit') {
      this.businessunitidarray = [];
      for (let i = 0; i < event.length; i++) {
        this.businessunitidarray.splice(event[i].item_id);
      }
      this.usermanagementForm.controls['businessunitId'].setValue(this.businessunitidarray.toString());
        this.buIdString = '';
        for (var i = 0; i < this.businessunitidarray.length; i++) {
          this.buIdString = this.businessunitidarray[i] + ',' + this.buIdString;
        }
        this.getSiteListByBUId(this.buIdString);
    }
    if (item === 'Site') {

      for (let i = 0; i < event.length; i++) {
        this.siteidarray.splice(event[i].item_id);
      }
      this.siteidarray = [];
      this.usermanagementForm.controls['siteId'].setValue(this.siteidarray.toString());
    }

  }

  // For getting Type of Role List
  userroleList() {
    this.usermanagementservice.getUserRoleList().then(r => {
      this.roleList = r.result;
    });
  }

  // For getting Type of Country List
  countryList() {
    this.usermanagementservice.getCountryList().then(r => {
      this.countryGroups = r;
    });
  }

  // For getting Type of Site List
  siteList() {
    this.usermanagementservice.getSiteList().then(r => {
      this.siteGroups = r.result;

      var LongArray = [];
      for (var i = 0; i < this.siteGroups.length; i++) {
        var Obj = {
          item_text: this.siteGroups[i].name,
          item_id: +this.siteGroups[i].siteId,
        }
        LongArray.push(Obj);
        if (i == (this.siteGroups.length - 1)) {
          this.soptionss = LongArray;
          this.onselectedsite();
        }
      }
    });
  }

  imageSizeError = '';
  fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      if(file.size < 2000000) {
        this.imageSizeError = '';
        let formData: FormData = new FormData();
        formData.append('userImage', file);
        this.usermanagementservice.Upload(formData, function (dataVal) {
          let data = dataVal['_body'];
          let response = JSON.parse(data);
          this.uploadedimage = this.configuration.Server + response.location;
          this.sendimage = (response.location);
          this.usermanagementForm.controls['userImage'].setValue(this.sendimage);
        }.bind(this));
      } else {
        this.imageSizeError = 'Image size must be less than 2MB'
      }
    }
  }

  oldPassword : string = '';
  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.usermanagement && change.usermanagement.currentValue) {
      this.loading = false;
      this.id = change.usermanagement.currentValue.id;
      this.usermanagementForm.controls['id'].setValue("E00" + change.usermanagement.currentValue.id);
      this.usermanagementForm.controls['unid'].setValue(change.usermanagement.currentValue.unid);
      this.usermanagementForm.controls['mobileNumber'].setValue(change.usermanagement.currentValue.mobileNumber);
      this.usermanagementForm.controls['userRoleId'].setValue(change.usermanagement.currentValue.userRoleId);
      this.usermanagementForm.controls['email'].setValue(change.usermanagement.currentValue.email);
      this.usermanagementForm.controls['firstName'].setValue(change.usermanagement.currentValue.firstName);
      this.usermanagementForm.controls['lastName'].setValue(change.usermanagement.currentValue.lastName);
      this.usermanagementForm.controls['userImage'].setValue(change.usermanagement.currentValue.userImage);
      this.usermanagementForm.controls['password'].setValue(change.usermanagement.currentValue.password);
      this.oldPassword = change.usermanagement.currentValue.password;

      if (change.usermanagement.currentValue.activeStatusId == 1) {
        this.status = 1;
        this.usermanagementForm.controls['status'].setValue(1);
      } else {
        this.status = 0;
        this.usermanagementForm.controls['status'].setValue(2);
      }

      let usersLocation = change.usermanagement.currentValue.usersLocation;
      let site = [];
      for (let i = 0; i < usersLocation.length; i++) {
        site.push(usersLocation[i].site);
      }
      var LongArray = [];
      for (var i = 0; i < site.length; i++) {
        var Obj = {
          item_text: site[i].name,
          item_id: site[i].siteId,
        }
        LongArray.push(Obj);
        if (i == (site.length - 1)) {
          this.selectedSite = LongArray;
        }
      }
      for (let i = 0; i < LongArray.length; i++) {
        this.siteidarray.push(LongArray[i].item_id);
      }
      this.usermanagementForm.controls['siteId'].setValue(this.siteidarray.toString());

      let businessunit = change.usermanagement.currentValue.businessUnit;
      var LongBArray = [];
      for (var i = 0; i < businessunit.length; i++) {
        var Obj = {
          item_text: businessunit[i].buName,
          item_id: businessunit[i].buId,
        }
        LongBArray.push(Obj);
        if (i == (businessunit.length - 1)) {
          this.selectedBusinessUnit = LongBArray;
        }
      }
      for (let i = 0; i < LongBArray.length; i++) {
        this.businessunitidarray.push(LongBArray[i].item_id);
      }
      this.usermanagementForm.controls['businessunitId'].setValue(this.businessunitidarray.toString());
      let country = change.usermanagement.currentValue.country;
      var LongCArray = [];
      for (var i = 0; i < country.length; i++) {
        var Obj = {
          item_text: country[i].name,
          item_id: country[i].countryId,
        }
        LongCArray.push(Obj);
      }
      if(LongCArray.length > 0) {
        this.selectedCountry = LongCArray;
      }
      for (let i = 0; i < LongCArray.length; i++) {
        this.countryidarray.push(LongCArray[i].item_id);

      }
      this.usermanagementForm.controls['countryId'].setValue(this.countryidarray.toString());
      let cList = [];
      country.forEach(country => {
        cList.push(country.countryId);
      });
      this.getBUListByCountryList(cList.toString());
      let bList = [];
      businessunit.forEach(businessunit => {
        bList.push(businessunit.buId);
      });
      this.getSiteListByBUId(bList.toString());
      if(change.usermanagement.currentValue.userImage) {
        this.uploadedimage = this.configuration.Server + change.usermanagement.currentValue.userImage;
      }      
    } else {
      this.loading = false;
    }
  }

  selectAll(event) {
    if (event.target.checked == true) {
      this.status = 1;
    } else {
      this.status = 2;
    }
    this.usermanagementForm.controls['status'].setValue(this.status);
  }

  onselectednew_country() {
    var self = this;
    this.usermanagementservice.getCountryList().then(r => {
      this.countryGroups = r;
      this.loading = false;
      var LongArray = [];
      for (var i = 0; i < this.countryGroups.length; i++) {
        var Obj = {
          item_text: this.countryGroups[i].name,
          item_id: +this.countryGroups[i].countryId,
        }
        LongArray.push(Obj);
        if (i == (this.countryGroups.length - 1)) {
          this.coptionss = LongArray;
          this.onselectedcountry();
        }
      }
    });
  }

  onselectednew_businessunit() {
    var self = this;
    this.usermanagementservice.getBusinessUnitList().then(r => {
      this.businessunitGroups = r.result;
      this.loading = false;
      var LongArray = [];
      for (var i = 0; i < this.businessunitGroups.length; i++) {
        var Obj = {
          item_text: this.businessunitGroups[i].buName,
          item_id: +this.businessunitGroups[i].buId,
        }
        LongArray.push(Obj);
        if (i == (this.businessunitGroups.length - 1)) {
          this.boptionss = LongArray;
          this.onselectedbusinessunit();
        }
      }
    });
  }

  onselectednew_site() {
    var self = this;
    this.usermanagementservice.getSiteList().then(r => {
      this.siteGroups = r.result;
      this.loading = false;
      var LongArray = [];
      for (var i = 0; i < this.siteGroups.length; i++) {
        var Obj = {
          item_text: this.siteGroups[i].name,
          item_id: +this.siteGroups[i].siteId,
        }
        LongArray.push(Obj);
        if (i == (this.siteGroups.length - 1)) {
          this.soptionss = LongArray;
          this.onselectedsite();
        }
      }
    });
  }

  onselectedcountry() {
    this.coptions = this.coptionss;
  }

  onselectedbusinessunit() {
    this.boptions = this.boptionss;
  }

  onselectedsite() {
    this.soptions = [];
    this.soptions = this.soptionss;
  }

  clearerror() {
    this.error = "";
  }

  clearmsgs() {
    this.businessunitidarray == [];
    this.countryidarray == [];
    this.siteidarray == [];
    this.imageSizeError = '';
    this.uploadedimage = '';
    this.passwordError = "";
    this.newPassword = "";
    this.confirmPassword = "";
    if (this.usermanagementForm.controls['siteId'].hasError('required') || (this.usermanagementForm.controls['siteId'].touched)) {
      this.siteIdRequired = "";
    }
    if (this.siteidarray == []) {
      this.siteIdRequired = "";
    }
    if (this.usermanagementForm.controls['firstName'].hasError('required') || (this.usermanagementForm.controls['firstName'].touched)) {
      this.firstNameRequired = "";
    }
    if (this.usermanagementForm.controls['lastName'].hasError('required') || (this.usermanagementForm.controls['lastName'].touched)) {
      this.lastNameRequired = "";
    }
    if (this.usermanagementForm.controls['unid'].hasError('required') || (this.usermanagementForm.controls['unid'].touched)) {
      this.unidRequired = "";
    }
    if (this.usermanagementForm.controls['email'].hasError('required') || (this.usermanagementForm.controls['email'].touched)) {
      this.emailRequired = "";
    }
    if (this.usermanagementForm.controls['userRoleId'].hasError('required') || (this.usermanagementForm.controls['userRoleId'].touched)) {
      this.userRoleIdRequired = "";
    }
    if (this.usermanagementForm.controls['email'].hasError('pattern') || (this.usermanagementForm.controls['email'].touched)) {
      this.invalidEmail = "";
    }
    if (this.usermanagementForm.controls['countryId'].hasError('required') || (this.usermanagementForm.controls['countryId'].touched)) {
      this.countryIdRequired = "";
    }
    if (this.countryidarray == []) {
      this.countryIdRequired = "";
    }
    if (this.usermanagementForm.controls['businessunitId'].hasError('required') || (this.usermanagementForm.controls['businessunitId'].touched)) {
      this.businessunitIdRequired = "";
    }
    if (this.businessunitidarray == []) {
      this.businessunitIdRequired = "";
    }
    this.error = "";
    //this.usermanagementForm.reset();
    this.selectedCountry =[];
    this.selectedBusinessUnit=[];
    this.selectedSite=[];
    this.getid();
    this.userId=this.usermanagementForm.value['id'];
    this.usermanagementForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      unid: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      mobileNumber: new FormControl(''),
      id: new FormControl(this.userId),
      userRoleId: new FormControl('', [Validators.required]),
      siteId: new FormControl('', Validators.required),
      userImage: new FormControl(''),
      countryId: new FormControl('', Validators.required),
      businessunitId: new FormControl('', Validators.required),
      status: new FormControl(1),
    })
  }

  passwordError : string = '';
  onSubmit(validPost) {
    //validPost.userImage=this.sendimage;
    this.error = [];
    this.imageSizeError = '';
    validPost.Id = this.id;
    this.formSubmited = true;
    if (this.countryidarray == [] || this.countryidarray.length == 0) {
      this.countryIdRequired = "Country is required";
    }
    if (this.businessunitidarray == [] || this.businessunitidarray.length == 0) {
      this.businessunitIdRequired = "Business Unit is required";
    }
    if (this.siteidarray == [] || this.siteidarray.length == 0) {
      this.siteIdRequired = "Site Name is required";
    }
    this.userRoleIdRequired = "Role is required";
    this.emailRequired = "Email is required";
    this.invalidEmail = "Invalid Email Address";
    this.unidRequired = "UNID is required";
    this.lastNameRequired = "Last Name is required";
    this.firstNameRequired = "First Name is required";
    this.passwordError = "";
    if(this.newPassword != '' || this.confirmPassword != '') {
      if(this.newPassword !== this.confirmPassword) {
        this.passwordError = "Passwords do not match";
      } else if (/^(?=.*\d)[0-9]{8,}$/.test(this.newPassword)) {
        this.passwordError = "Password should be atleast 8 characters  length and should contain atleast a digit";
      } else {
        this.passwordError = "";
      }
    }
    if(this.passwordError == "" ) {
      if(this.newPassword != "") {
        validPost.password = this.newPassword;
      } else {
        validPost.password = this.oldPassword;
      }
      if (this.usermanagementForm.valid && Object.keys(this.error).length === 0) {
        this.saved.emit(validPost);
      } else {
        jQuery('form').find(':input.ng-invalid:first').focus();
      }
    }
  }

  passwordKeyUp() {
    this.passwordError = "";
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      return false;
      //event.preventDefault();
    }
  }

  emailKeyUp() {
    if(this.error.hasOwnProperty('secondKey')) {
      delete this.error.secondKey;
    }
  }

  onUNIDchange() {
    if(this.error.hasOwnProperty('firstKey')) {
      delete this.error.firstKey;
    }
  }

  showHidePassword(value, element ) {
    if( element == 'password'){
      if (this.ShowPassword.nativeElement.type = 'password') {
        this.show_password = true;
        this.hide_password = false;
      } else {
        this.show_password = false;
        this.hide_password = true;
      }

      if (value === 'show') {
        this.show_password = false;
        this.hide_password = true;
        this.ShowPassword.nativeElement.type = 'text';
      } else {
        this.show_password = true;
        this.hide_password = false;
        this.ShowPassword.nativeElement.type = 'password';
      }
    } else if( element == 'confirmPassword') {
      if (this.ShowConfirm.nativeElement.type = 'password') {
        this.show_confirm_password = true;
        this.hide_confirm_password = false;
      } else {
        this.show_confirm_password = false;
        this.hide_confirm_password = true;
      }

      if (value === 'show') {
        this.show_confirm_password = false;
        this.hide_confirm_password = true;
        this.ShowConfirm.nativeElement.type = 'text';
      } else {
        this.show_confirm_password = true;
        this.hide_confirm_password = false;
        this.ShowConfirm.nativeElement.type = 'password';
      }
    }
    }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail;
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}
