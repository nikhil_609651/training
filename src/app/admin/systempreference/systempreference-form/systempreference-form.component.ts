import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from 'app/app.constants';
import { SystemPreferenceFormService } from 'app/admin/systempreference/systempreference.service';
import { IMyOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'systempreference-form',
  template: require('./systempreference-form.html'),
  styleUrls: ['./systempreference-form.css']
})
export class SystemPreferenceFormComponent  {
  isCollapsed = []; //: boolean = true;

  id: any=0;
  systempreferenceForm: FormGroup;
  public formSubmited = false;
  private currentdate = new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
   // disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() }
  };
  public FromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  public ToDate = '';//: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  selectedFromDate = new Date();
  selectedToDate = new Date();
  dateErrorMsg: any = '';
  messageReq: any = 'Text is Required';
  deleteItem: any;
  deleteMsg = "";
  @Input() preferencelist;
  currencylist: any;
  messageLengthError = "";
  @Input() showText;
  @Input() statusList;
  //preferencelist: any;

  @Output() saved = new EventEmitter();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router, private fb: FormBuilder, private route: ActivatedRoute, private _sharedService : SharedService,
              private service: SystemPreferenceFormService, private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	      this.privileges = privileges;
      });

      this.systempreferenceForm = fb.group({
        'currency': [''],
        'message': ['', Validators.required],
        'fromDate': [''],
        'toDate': [''],
        'id':[0],
      });
      var cForm = this.systempreferenceForm;
  }

  ngOnInit() {
   this.getcurrencylist();
   this.getpreferencelist();
   this.systempreferenceForm.controls['currency'].setValue(1);  /**by default currency to be set to USD */
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getcurrencylist(){
    this.service.geCurrencyList().then(r => {
      this.currencylist = r;
    });
  }

  getpreferencelist() {
    this.statusList = [];
    this.service.gePreferenceList().then(r => {
      this.preferencelist = r;
      let i = 0;
      this.preferencelist.forEach(list => {
        this.statusList.push(list.onOrOff);
        if(list.displayData.length > 45 ) {
          this.showText[i] = 'Read more[...]';
        }
        i++;
      });
      this.statusList.push(true);
    });
  }

  onSubmit(validPost) {
    this.messageReq = "Text is Required";
    this.formSubmited = true;
    if(this.systempreferenceForm.valid && this.dateErrorMsg == "" ) {
      //this.savebutton = false;
      validPost.fromDate = this.FromDate;
      validPost.toDate = this.ToDate;
      this.saved.emit(validPost);
      this.clearmsgs();
      this.getpreferencelist();
    } else {
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  onDateChanged(event: any, type: any) {
    this.selectedFromDate.setHours(0, 0, 0, 0);
    this.selectedToDate.setHours(0, 0, 0, 0);
    if(type === 'FromDate') {
      this.selectedFromDate = event.jsdate;
      this.selectedFromDate.setHours(0, 0, 0, 0);
      if(this.ToDate != '' && this.selectedToDate < this.selectedFromDate) {
        this.dateErrorMsg = 'To Date should be greater than From Date';
      } else {
        this.dateErrorMsg = '';
        this.systempreferenceForm.controls['fromDate'].setValue(event.formatted);
        this.FromDate = event.formatted;
      }
    }
    if(type === 'ToDate') {
      this.selectedToDate = event.jsdate;
      this.selectedToDate.setHours(0, 0, 0, 0);
      if(this.selectedFromDate > this.selectedToDate && this.selectedFromDate != this.selectedToDate) {
        this.dateErrorMsg = 'To Date should be greater than From Date';
      } else {
        this.dateErrorMsg = '';
        this.systempreferenceForm.controls['toDate'].setValue(event.formatted);
        this.ToDate = event.formatted;
      }
    }
  }

  deleteConfirm() {
    let item = this.deleteItem
    let FromDate = item.fromDate;
    let ToDate=item.toDate;
    if(typeof FromDate == 'object'){
      FromDate=FromDate.date.day + '/' + FromDate.date.month + '/' + FromDate.date.year;
    } else {
      FromDate= item.fromDate;
    }
    if(typeof ToDate == 'object'){
      ToDate=ToDate.date.day + '/' + ToDate.date.month + '/' + ToDate.date.year;
    } else {
      ToDate= item.toDate;
    }
    let formData = new FormData();
    formData.append('Id',item.id);
    formData.append('CurrencyId',item.currencyId);
    formData.append('DisplayData', item.displayData);
    formData.append('FromDate',FromDate);
    formData.append('ToDate', ToDate);
    formData.append('OnOrOff','true');
    formData.append('IsDeleted','1');
    this.service.Save(formData).then(r =>  {
      this.deleteMsg = "Entry Deleted Successfully";
      this.getpreferencelist();
    }).catch(r =>  {
      this.deleteMsg = "Error while trying to delete entry";
      this.handleError(r);
    })
  }

  edit(item) {
    let fDate = new Date(item.fromDate);
    let tDate = new Date(item.toDate);
    let FromDate: Object = { date: { day: fDate.getDate(), month: fDate.getMonth() + 1, year:fDate.getFullYear() } };
    let ToDate: Object = { date: { day:tDate.getDate(), month: tDate.getMonth() + 1, year: tDate.getFullYear() } };
    this.selectedFromDate = fDate;
    this.selectedToDate = tDate;
    this.systempreferenceForm.controls['message'].setValue(item.displayData);
    this.systempreferenceForm.controls['fromDate'].setValue(FromDate);
    this.systempreferenceForm.controls['toDate'].setValue(ToDate);
    this.systempreferenceForm.controls['id'].setValue(item.id);
    this.systempreferenceForm.controls['currency'].setValue(item.currencyId);
    jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
  }

  deleteEntry(item) {
    this.deleteItem = item;
  }

  deleteMessageClear() {
    this.deleteMsg = "";
  }

  closeModal() {
    this.deleteMsg = "";
    setTimeout(function () {
      jQuery('#del').modal('toggle');
    }.bind(this) , 0);
  }

  statusChange(event, data) {
    let formData = new FormData();
    formData.append('Id',data.id);
    formData.append('CurrencyId',data.currency.currencyId);
    formData.append('DisplayData', data.displayData);
    formData.append('FromDate', data.fromDate);
    formData.append('ToDate', data.toDate);
    formData.append('OnOrOff', event.target.checked);
    this.service.Save(formData).then(r =>  {
        //this.success = 'Status Changed Successfully!';
        //jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
    }).catch(r =>  {
        this.handleError(r);
    })
  }

  clearmsgs() {
    this.dateErrorMsg = "";
    this.messageReq = "";
    this.messageLengthError = "";
    let FromDate: Object = { date: { day: new Date().getDate(), month: new Date().getMonth() + 1, year: new Date().getFullYear() } };
    this.systempreferenceForm = this.fb.group({
      'currency': [1],
      'message': ['', Validators.required],
      'fromDate': [FromDate],
      'toDate': [''],
      'id':[0],
    });
  }

  /**set max length for text to 250 */
  onKeyUp(event) {
    let text = event.target.value;
    let textLength = String(text).length;
    if( textLength == 250 ) {
      this.messageLengthError = "Maximum length is 250"
    } else {
      this.messageLengthError = "";
    }
  }

  readMore(i, displayData) {
    this.isCollapsed[i] = !this.isCollapsed[i];
    this.showText[i] === 'Read more[...]' ? this.showText[i] = 'Read less[...]' : this.showText[i] = 'Read more[...]';
  }

  closeDeleteModal() {
    this.deleteMsg = '';
    jQuery('#del').modal('toggle');
    jQuery('#del').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  private handleError(error: any) {
    return Promise.reject(error.json());
  }
}