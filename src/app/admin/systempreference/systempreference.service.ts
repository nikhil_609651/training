import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class SystemPreferenceFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public currencylistUrl='';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

          this.currencylistUrl = _configuration.ServerWithApiUrl+'SystemPreference/CurrencyType/';
          this.listUrl = _configuration.ServerWithApiUrl+'SystemPreference/SystemPreferences/';
          this.saveUrl = _configuration.ServerWithApiUrl+'SystemPreference/SaveSystemPreference';
        this.deleteUrl = _configuration.ServerWithApiUrl+'SystemPreference/DeleteSystemPreferenceByID?id=';
    }

    // Currency List
    geCurrencyList(): Promise<any> {
        return this.authHttp.get(this.currencylistUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
 // Preference List
 gePreferenceList(): Promise<any> {
    return this.authHttp.get(this.listUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

//   Save API for Receiver
Save(data: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, data)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}
// Delete
delete(id){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}