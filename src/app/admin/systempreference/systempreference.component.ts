import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'systempreference',
    template: `<router-outlet></router-outlet>`
})
export class SystemPreferenceComponent {
	constructor(private router: Router) {
	}
}
