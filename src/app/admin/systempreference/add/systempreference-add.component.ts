import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Configuration } from 'app/app.constants';
import { SystemPreferenceFormService } from 'app/admin/systempreference/systempreference.service';

@Component({
  selector: 'add',
  template: require('./systempreference-add.html')
})
export class SystemPreferenceAddComponent {

    public error = {};
    public success = '';
    public usermanagement: any;
    public page = '';
    preferencelist;
    showText = [];
    statusList = [];
    constructor(private router: Router, private service: SystemPreferenceFormService,private configuration:Configuration) {
    }

    ngOnInit() {
      this.page = 'add';
    }

    onSave(data: any) {
    let FromDate = data.fromDate;
    let ToDate=data.toDate;
    if(typeof FromDate == 'object') {
      if(FromDate.date.day < 10 && FromDate.date.month >= 10) {
        FromDate= '0' + FromDate.date.day + '/' + FromDate.date.month + '/' + FromDate.date.year;
      } else if(FromDate.date.day >= 10 && FromDate.date.month < 10) {
        FromDate= FromDate.date.day + '/' + '0' + FromDate.date.month + '/' + FromDate.date.year;
      } else if(FromDate.date.day < 10 && FromDate.date.month < 10) {
        FromDate= '0' +FromDate.date.day + '/' + '0' + FromDate.date.month + '/' + FromDate.date.year;
      } else {
        FromDate= FromDate.date.day + '/' + FromDate.date.month + '/' + FromDate.date.year;
      }
    } else {
      FromDate= data.fromDate;
    }
    if(typeof ToDate == 'object') {
      if(ToDate.date.day < 10 && ToDate.date.month >= 10) {
        ToDate= '0' + ToDate.date.day + '/' + ToDate.date.month + '/' + ToDate.date.year;
      } else if(ToDate.date.day >= 10 && ToDate.date.month < 10) {
        ToDate= ToDate.date.day + '/' + '0' + ToDate.date.month + '/' + ToDate.date.year;
      } else if(ToDate.date.day < 10 && ToDate.date.month < 10) {
        ToDate= '0' +ToDate.date.day + '/' + '0' + ToDate.date.month + '/' + ToDate.date.year;
      } else {
        ToDate= ToDate.date.day + '/' + ToDate.date.month + '/' + ToDate.date.year;
      }
      
    } else {
      ToDate= data.toDate;
    }
    let formData = new FormData();
    formData.append('Id',data.id);
    formData.append('CurrencyId',data.currency);
    formData.append('DisplayData', data.message);
    formData.append('FromDate', FromDate);
    formData.append('ToDate', ToDate);
    formData.append('OnOrOff','true');
    this.service.Save(formData).then(r =>  {
        this.success = 'User Created Successfully!';
        jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
        setTimeout(function() {
            this.success = '';
            this.getpreferencelist();
        }.bind(this), 3000);
    }).catch(r =>  {
        this.handleError(r);
    })
  }

  getpreferencelist() {
    this.service.gePreferenceList().then(r => {
      this.preferencelist = r;
      let i = 0;
      this.preferencelist.forEach(list => {
        this.statusList.push(true);
        if(list.displayData.length > 45 ) {
          this.showText[i] = 'Read more[...]';
        }
        i++;
      });
      this.router.navigate(['./home/system-preferences']);
    });
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}
