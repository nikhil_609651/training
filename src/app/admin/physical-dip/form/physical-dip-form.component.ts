import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IMyOptions } from 'mydatepicker';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { PhysicalDipComponentService } from 'app/admin/physical-dip/physical-dip.component.service';

@Component({
  selector: 'physical-dip-form',
  templateUrl: './physical-dip-form.component.html'
})
export class PhysicalDipFormComponent implements OnInit, OnChanges {

    @Input() page;
    @Output() savePhysicalDip = new EventEmitter();
    private currentdate = new Date();
    private myDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}
    };
    private date: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    pdForm : FormGroup;
    error : any;
    siteList : any = '';
    polBySitelist : any = '';
    siteId : any = '';
    tankList : any = '';
    closingDIP : any = '';
    temperature : any = '';
    density : any = '';
    volume : any = '';
    volumeIssuedAt15 : any = '';
    selectedDate : Date;
    physicalDipId : any = '';
    storageTankId : any = '';
    isSiteRequired : boolean = false;
    isPoleRequired : boolean = false;
    isDateRequired : boolean = false;
    isClosingDipRequired = [];//: boolean = false;
    isTemperatureRequired = [];
    isDensityRequired = [];
    isVolumeRequired = [];
    isBookStockRequired = [];
    physicalDip : any;
    readOnly : boolean = false;
    storageTankList : any = [];
    showTankTable = true;
    isVolReadOnly  = [];
    isRequiredFieldsEmpty = [];
    isTankRequired = false;
    temperatureLimit: any = '';
    densityLimit: any = '';

	constructor(private _router: Router, private physicalDipService : PhysicalDipComponentService, private _activatedRoute : ActivatedRoute ) {
        this.getSiteList();
    }

    ngOnInit() {
        this.showTankTable = false;
        if( this.page === 'add' ) {
            this.initPDForm();
        } else if( this.page === 'edit' ) {
            this.readOnly = false;
            this._activatedRoute.params.subscribe( params => {
                this.physicalDipId = params['id'];
                this.storageTankId = params['tankId'];
            });
            this.initPDForm();
            this.getPhysicalDipById(this.physicalDipId);
        } else if( this.page === 'view' ) {
            this.readOnly = true; 
            this._activatedRoute.params.subscribe( params => this.physicalDipId = params['id']);
            this.initPDForm();
            this.getPhysicalDipById(this.physicalDipId);
        }
    }

    initPDForm(data = []) {
        let pdTable = new FormArray([])
        this.pdForm = new FormGroup({
            id : new FormControl(0),
            site : new FormControl('', [Validators.required]),
            pol : new FormControl('', [Validators.required]),
            date : new FormControl(''),
            pdTable : pdTable ,
            pipelineAndDrum : new FormGroup({
                quantity : new FormControl(''),
                drumStockQuantity : new FormControl(''),
                drumStockVolume : new FormControl('')
            }),
            remarks : new FormControl('')
        })
    }

    initDataToPDForm(data) {
        this.pdForm.controls['id'].setValue(data['id']);
        this.pdForm.controls['site'].setValue(data['site'].name);
        this.siteId = data['siteId'];
        this.physicalDipService.getPolListFromSiteById( this.siteId ).then(response => {
            this.polBySitelist = response;
            this.pdForm.controls['pol'].setValue(data['pol'].name);
            this.getTankListFromSite(data['polId'], data);
        })
        .catch(r => {
            this.handleError(r);
        });
        this.pdForm.controls['date'].setValue(data['date']);
        this.pdForm.controls['remarks'].setValue(data['remarks']);
        (<FormGroup>this.pdForm.controls['pipelineAndDrum']).controls['drumStockQuantity'].setValue(data['drumStockQuantity']);
        (<FormGroup>this.pdForm.controls['pipelineAndDrum']).controls['drumStockVolume'].setValue(data['drumStockVolume']);
        (<FormGroup>this.pdForm.controls['pipelineAndDrum']).controls['quantity'].setValue(data['pipelineQuantity']);
        if(this.readOnly){
            this.disableForm();
        }
        if(this.page === 'edit'){
            this.disableForm();
        }
    }

    disableForm() {
    }

    ngOnChanges(formChange) {
    }

    getSiteList() {
        this.physicalDipService.getSiteList().then(response => {
            this.siteList = response.result;
        })
        .catch(r => {
            this.handleError(r);
        })
    }
    
    getPolListFromSite( id ) {
        this.siteId = id.target.value;
        this.isSiteRequired = false;
        this.showTankTable = false;
        this.pdForm.controls['pol'].setValue('');
        this.physicalDipService.getPolListFromSiteById( id.target.value ).then(response => {
            this.polBySitelist = response;
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    initPdTable(tanksList, data) {
        let index = 0;
        let tanks;
        if(typeof data == 'undefined'){
            tanks = tanksList;
            let pdTableLength = (<FormArray>this.pdForm.controls['pdTable']).length;
            for(let j = 0; j < pdTableLength; j++) {
                (<FormArray>this.pdForm.controls['pdTable']).removeAt(j);
            }
            /*(<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['closingDip'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['temperature'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['density'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['volume'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['volumeAt15Degree'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['atgReading'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['bookStock'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['lossGain'].setValue('');
            (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[0]).controls['ullage'].setValue('');*/
        } else{
            tanks = data.physicalDipTankDetails;
            this.storageTankList = data.physicalDipTankDetails;
        }

        tanks.forEach(tank => {
            let closingDIP = this.closingDIP;
            let temperature = this.temperature;
            let density = this.density;
            let volume = this.volume;
            let volumeAt15Degree = '';
            let atgReading = 0;
            let bookStock = 0;
            let lossGain = '';
            let ullage = '';
            let tankName = '';
            if(typeof data != "undefined") {
                closingDIP = data.physicalDipTankDetails[index].closingDip ? data.physicalDipTankDetails[index].closingDip : 0;
                temperature = data.physicalDipTankDetails[index].temprature ? data.physicalDipTankDetails[index].temprature : 0;
                density = data.physicalDipTankDetails[index].density ? data.physicalDipTankDetails[index].density : 0;
                volume = data.physicalDipTankDetails[index].volume ? data.physicalDipTankDetails[index].volume : 0;
                volumeAt15Degree = data.physicalDipTankDetails[index].volumeAt15Degree ? data.physicalDipTankDetails[index].volumeAt15Degree : 0;
                atgReading = data.physicalDipTankDetails[index].atgReading ? data.physicalDipTankDetails[index].atgReading : 0;
                bookStock = data.physicalDipTankDetails[index].bookStock ? data.physicalDipTankDetails[index].bookStock : 0;
                lossGain = data.physicalDipTankDetails[index].lossGain ? data.physicalDipTankDetails[index].lossGain : 0;
                ullage = data.physicalDipTankDetails[index].ullage ? data.physicalDipTankDetails[index].ullage : 0;
            }
            tankName = tanksList[index].tankName;
            (<FormArray>this.pdForm.controls['pdTable']).push(
                new FormGroup({
                    SiteId : new FormControl(tank.siteId),
                    POLId : new FormControl(tank.polId),
                    StorageTankId : new FormControl(tank.storageTankId),
                    tankName : new FormControl(tankName),
                    closingDip : new FormControl(closingDIP, Validators.required),
                    temprature : new FormControl(temperature, Validators.required),
                    density : new FormControl(density, Validators.required),
                    volume : new FormControl(volume, Validators.required),
                    volumeAt15Degree : new FormControl(volumeAt15Degree),
                    atgReading : new FormControl(atgReading),
                    bookStock : new FormControl(tank.bookStock, Validators.required),
                    lossGain : new FormControl(lossGain),
                    ullage : new FormControl(ullage)
                })
            );
            if((this.page == 'view') || (this.page == 'edit' && tank.storageTankId != this.storageTankId)){
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['closingDip'].disable();
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['temprature'].disable();
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['density'].disable();
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['volume'].disable();
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['atgReading'].disable();
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['bookStock'].disable();
            }
            index++;
         });
     }

    onDateChanged(event: any) {
        this.date = event.formatted;
        this.isDateRequired = false;
        this.pdForm.controls['date'].setValue(event.formatted);
    }

    /*************************************************
     * get tank list from site
     * get pol availability(book stock) for each tank
     *************************************************/
    getTankListFromSite( polId, data ) {
        this.isPoleRequired = false;
        if ( polId == null ) {
            polId = 0;
        }
        this.physicalDipService.getTankListFromSiteById( this.siteId, polId ).then(response => {
            this.tankList = response;
            if(this.tankList.length > 0){
                this.showTankTable = true;
            }
            let polAvailabilityArray = [];
            for(let i = 0; i < response.length; i++) {
                this.physicalDipService.getTankPOLAvailibility( response[i].storageTankId ).then(polAvailability => {
                    polAvailabilityArray.push(polAvailability);
                    this.tankList[i]['bookStock'] = 0;
                    if(polAvailability.result[0].polAvailableQuantity) {
                        this.tankList[i].bookStock = polAvailability.result[0].polAvailableQuantity;
                    }
                    if(polAvailabilityArray.length == this.tankList.length) {
                        this.storageTankList = response;
                        this.initPdTable(this.tankList, data);
                    }
                }).catch(r => {
                    this.handleError(r);
                })
                this.isVolReadOnly.push(false);
            }
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    calculateVolAt15Degree( tank, index ) {
        let temperature = tank.controls['temprature'].value;
        let volume = tank.controls['volume'].value;
        let density = tank.controls['density'].value;
        if(temperature && volume && density) {
            this.physicalDipService.getCalculateQuantityAt15DegreeCelsius( density.toString(), temperature.toString(), volume.toString() ).then(response => {
                this.volumeIssuedAt15 = response.quantityAt15Degree;
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['volumeAt15Degree'].setValue(this.volumeIssuedAt15);
              })
              .catch(r => {
                  this.handleError(r);
              })
       }
    }

    /*************************************************
     * calculate ullage and loss/gain
     * ullage = capacity - bookStock
     * lossGain = volume - bookStock
     *************************************************/
    calculateUllageAndLossGain( tank, index ){
        let volume = tank.controls['volume'].value;
        let capacity = this.tankList[index].capacity;
        let bookStock = tank.controls['bookStock'].value;
        let lossGain = volume - bookStock;
        let ullage = capacity - bookStock;
        (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['lossGain'].setValue(lossGain);
        (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['ullage'].setValue(ullage);
    }
    
    onKeyPress(event, index, control) {
        this.isTankRequired = false;
        if(control === 'closingDIP') {
            this.isClosingDipRequired[index] = false;
        } else if(control === 'temperature'){
            this.isTemperatureRequired[index] = false;
        } else if(control === 'density'){
            this.isDensityRequired[index] = false;
        } else if(control === 'volume'){
            this.isVolumeRequired[index] = false;
        } else if(control === 'bookStock'){
            this.isBookStockRequired[index] = false;
        }
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 31 ) && (charCode < 48 || charCode > 57) && ( charCode != 46 )) {
            return false;
        }
        var number = event.target.value;
        var wholePart = Math.floor(number);
        var wholePartLength = wholePart.toString().length;
        var fractionPart = number % 1;
        var fractionPartLength = (number.toString().length - wholePartLength) - 1;
        fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
        var noOfDigits = fractionPart.toString().length;
        if(noOfDigits >= 4){
            return false;
        } else {
            return true;
        }
    }
    
    onKeyUp(event, control) {
        var number = event;
        let num = Number(number);
        if(control == 'temperature') {
            if(num > 50 || (number == '.') || number == '') {
                this.temperatureLimit = "Temperature should be between 0 and 50";
            } else {
                this.temperatureLimit = "";
            }
        } else if(control == 'density') {
            if((num < 0.670)  || (num > 1.06) || (number == '.') || number == ''  ){
                this.densityLimit = "Density should be between 0.670 and 1.06";
            } else {
                this.densityLimit = "";
            }
        }
    }

    /*******************************************************
     * if the entered dip entry value has a corresponding
     * caliberation value in the tank caliberation chart,
     * populate that value to the volume field and make the
     * field readonly
     *************************************************/
    onDipKeyUp(index, event) {
        let dipValue = event.target.value;
        this.tankList[index].tankCaliberationChart.forEach((caliberationChart)=>{
            let caliberationHeight = caliberationChart.height;
            let caliberationVolume = caliberationChart.volume;
            if(dipValue == caliberationHeight) {
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['volume'].setValue(caliberationVolume);
                this.isVolReadOnly[index] = true;
                let tank = (<FormArray>this.pdForm.controls['pdTable']).controls[index]
                this.calculateUllageAndLossGain( tank, index );
                this.calculateVolAt15Degree( tank, index );
                this.isVolumeRequired[index] = false;
            } else {
                (<FormGroup>(<FormArray>this.pdForm.controls['pdTable']).controls[index]).controls['volume'].setValue('');
                this.isVolReadOnly[index] = false;
            }
        });
    }

    onSubmit(form) {
        this.isRequiredFieldsEmpty = [];
        this.isClosingDipRequired = [];
        this.isTemperatureRequired = [];
        this.isDensityRequired = [];
        this.isVolumeRequired = [];
        this.isBookStockRequired= [];
        if(form.valid) {
            this.savePhysicalDip.emit(form.value);
        } else {
            if(form.controls['site'].hasError('required')) {
                this.isSiteRequired = true;
            }
            if(form.controls['pol'].hasError('required')) {
                this.isPoleRequired = true;
            }
            if(form.controls['date'].hasError('required')) {
                this.isDateRequired = true;
            }
            if( this.page == 'edit') {
                for( let i = 0; i < this.physicalDip.physicalDipTankDetails.length; i ++) {
                    let closingDipValue = form.controls['pdTable'].controls[i].controls['closingDip'].value;
                    let temperatureValue = form.controls['pdTable'].controls[i].controls['temprature'].value;
                    let densityValue = form.controls['pdTable'].controls[i].controls['density'].value;
                    let volumeValue = form.controls['pdTable'].controls[i].controls['volume'].value;
                    let bookStockValue = form.controls['pdTable'].controls[i].controls['bookStock'].value;
                    if( closingDipValue != "" || temperatureValue != "" || densityValue != "" || volumeValue != "" ){
                        this.isRequiredFieldsEmpty.push(true);
                    }
                    if(this.isRequiredFieldsEmpty[i]){
                        if(form.controls['pdTable'].controls[i].controls['closingDip'].value == ""){
                            this.isClosingDipRequired.push(true);
                        } else {
                            this.isClosingDipRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['temprature'].value == ""){
                            this.isTemperatureRequired.push(true);
                        } else {
                            this.isTemperatureRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['density'].value == ""){
                            this.isDensityRequired.push(true);
                        } else {
                            this.isDensityRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['volume'].value == ""){
                            this.isVolumeRequired.push(true);
                        } else {
                            this.isVolumeRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['bookStock'].value == ""){
                            this.isBookStockRequired.push(true);
                        } else {
                            this.isBookStockRequired.push(false);
                        }
                        //jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
                    }
                    if(this.isRequiredFieldsEmpty.length == 0 ) {
                        this.isTankRequired = true;
                        jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
                    }
                }
            } else if( this.page == 'add'){
                for( let i = 0; i < this.tankList.length; i ++) {
                    let closingDipValue = form.controls['pdTable'].controls[i].controls['closingDip'].value;
                    let temperatureValue = form.controls['pdTable'].controls[i].controls['temprature'].value;
                    let densityValue = form.controls['pdTable'].controls[i].controls['density'].value;
                    let volumeValue = form.controls['pdTable'].controls[i].controls['volume'].value;
                    let bookStockValue = form.controls['pdTable'].controls[i].controls['bookStock'].value;
                    /*if( closingDipValue != "" || temperatureValue != "" || densityValue != "" || volumeValue != "" || bookStockValue != "" ) {
                        this.isRequiredFieldsEmpty.push(true);
                    }
                    if(this.isRequiredFieldsEmpty[i]){
                        if(form.controls['pdTable'].controls[i].controls['closingDip'].value == ""){
                            this.isClosingDipRequired.push(true);
                        } else {
                            this.isClosingDipRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['temperature'].value == ""){
                            this.isTemperatureRequired.push(true);
                        } else {
                            this.isTemperatureRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['density'].value == ""){
                            this.isDensityRequired.push(true);
                        } else {
                            this.isDensityRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['volume'].value == ""){
                            this.isVolumeRequired.push(true);
                        } else {
                            this.isVolumeRequired.push(false);
                        }
                        if(form.controls['pdTable'].controls[i].controls['bookStock'].value == ""){
                            this.isBookStockRequired.push(true);
                        } else {
                            this.isBookStockRequired.push(false);
                        }
                        //jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
                    }*/
                    if((closingDipValue != "" && temperatureValue != "" && densityValue != "" && volumeValue != "")
                       ) {
                        this.isRequiredFieldsEmpty.push(true);
                    }
                }
                if(this.isRequiredFieldsEmpty.length == 0 && this.densityLimit == "" && this.temperatureLimit == "") {
                    this.isTankRequired = true;
                    jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
                } else {
                    this.savePhysicalDip.emit(form.value);
                }
            }
            /*if( this.isSiteRequired || this.isPoleRequired || this.isDateRequired){
                jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
            }*/
        }
        /*let saveForm = false;
        this.isRequiredFieldsEmpty.forEach((required)=>{
            if(required) {
                saveForm = true;
            } else {
                saveForm = false;
            }
        })
        if(saveForm) {
            this.savePhysicalDip.emit(form.value);
        }*/
    }

    getPhysicalDipById(id) {
        this.physicalDipService.getPhysicalDipById(id).then(response => {
            this.physicalDip = response;
            this.storageTankList = response.physicalDipTankDetails;
            //this.initPDForm(this.physicalDip);
            this.initDataToPDForm(this.physicalDip);
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    cancelBtn(){
        this._router.navigate(['./home/physical-dip']);
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
    }
}
