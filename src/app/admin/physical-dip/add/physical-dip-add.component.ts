import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { PhysicalDipComponentService } from 'app/admin/physical-dip/physical-dip.component.service';

@Component({
  selector: 'add',
  templateUrl: './physical-dip-add.component.html'
})
export class PhysicalDipAddComponent {
    page : string = 'add';
    success : any = '';
    error : any = '';
	constructor(private _router: Router, private physicalDipService : PhysicalDipComponentService ) {
    }

    onSavePhysicalDip(pdForm) {
        let index = 0;
        for( let i = 0; i < pdForm.pdTable.length; i++) {
            if(pdForm.pdTable[i].closingDip == "" || pdForm.pdTable[i].density == "" || pdForm.pdTable[i].volume == "" || pdForm.pdTable[i].temperature == "" ) {
                pdForm.pdTable.splice(i , 1);
                i = 0;
            }
        }
        let physicalDipBatch = JSON.stringify(pdForm.pdTable);
        let formData = new FormData();
        //formData.append('Id',pdForm.);
        let pdFormDate = pdForm.date;
        let date;
        if(pdFormDate.date.month < 10 && pdFormDate.date.day < 10) {
            date = '0' + pdFormDate.date.day + '/' + '0' + pdFormDate.date.month + '/' + pdFormDate.date.year;
        }else if(pdFormDate.date.day < 10 && pdFormDate.date.month >= 10) {
            date = '0' + pdFormDate.date.day + '/' + pdFormDate.date.month + '/' + pdFormDate.date.year;
        }else if(pdFormDate.date.day >= 10 && pdFormDate.date.month < 10) {
            date = pdFormDate.date.day + '/' + '0' + pdFormDate.date.month + '/' + pdFormDate.date.year;
        } else {
            date = pdFormDate.date.day + '/' + pdFormDate.date.month + '/' + pdFormDate.date.year;
        }
        formData.append('Id', '0');
        formData.append('TransactionDate', date);
        formData.append('SiteId', pdForm.site);
        formData.append('POLId',pdForm.pol);
        //formData.append('StorageTankId','');
        formData.append('DrumStockQuanity', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('DrumStockQuanity', pdForm.pipelineAndDrum.drumStockQuantity);
        }
        formData.append('DrumStockVolume', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('DrumStockVolume', pdForm.pipelineAndDrum.drumStockVolume);
        }
        formData.append('PipelineQuantity', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('PipelineQuantity', pdForm.pipelineAndDrum.quantity);
        }
        formData.append('Remarks', pdForm.remarks);
        formData.append('PhysicalDipBatchDetails',physicalDipBatch);
        this.physicalDipService.savePhysicalDip(formData).then(r =>  {
            this.success = 'Physical Dip Created Successfully!'; 
            jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
            setTimeout(function() {
               this.success = '';
               this._router.navigate(['./home/physical-dip']);
            }.bind(this), 3000);
          }).catch(r =>  {
            this.handleError(r);
          })
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
    }
}
