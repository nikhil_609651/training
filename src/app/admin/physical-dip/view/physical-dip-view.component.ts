import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'view',
  templateUrl: './physical-dip-view.component.html'
})
export class PhysicalDipViewComponent {

    page : any = 'view';
    success : any = '';
    error : any = '';
	constructor(private _router: Router) {
    }
}
