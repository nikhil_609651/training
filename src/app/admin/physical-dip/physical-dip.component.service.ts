import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class PhysicalDipComponentService {
    listPhysicalDip = '';
    savePhysicalDipUrl = '';
    siteUrl = '';
    getPolfromSiteUrl = '';
    getTankFromPolUrl = '';
    getVolAt15Url = '';
    siteFilterUrl = '';
    polFilterUrl = '';
    approvedUserFilterUrl = '';
    statusFilterUrl = '';
    tankFilterUrl = '';
    physicalDipByIdUrl = '';
    deletePhysicalDipById = '';
    tankPOLAvailabilityUrl = '';

    constructor( private _authHttp : AuthHttp, private _config : Configuration ){
        this.listPhysicalDip = _config.ServerWithApiUrl + 'PhysicalDip/PhysicalDip';
        this.siteUrl = _config.ServerWithApiUrl+'Masters/Site/';
        this.getPolfromSiteUrl = _config.ServerWithApiUrl + 'Masters/getAllPOLBySite?SiteId=';
        this.getTankFromPolUrl = _config.ServerWithApiUrl + 'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.getVolAt15Url = _config.ServerWithApiUrl + 'ReceiptAndTransfer/CalculateQuantityAt15DegreeCelsius?Temprature=';
        this.savePhysicalDipUrl = _config.ServerWithApiUrl + 'PhysicalDip/SavePhysicalDip';
        this.siteFilterUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetPhysicalDipSiteFilter';
        this.physicalDipByIdUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetPhysicalDipByID?id=';
        this.deletePhysicalDipById = _config.ServerWithApiUrl + 'PhysicalDip/DeletePhysicalDipByID?id=';
        this.polFilterUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetPOLFilter';
        this.approvedUserFilterUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetNewPhysicalDipApprovedUserFilter';
        this.statusFilterUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetPhysicalDipApprovedStatusFilter';
        this.tankFilterUrl = _config.ServerWithApiUrl + 'PhysicalDip/GetPhysicalDipStorageTankFilter';
        this.tankPOLAvailabilityUrl = _config.ServerWithApiUrl + 'ReceiptAndTransfer/TankPOLAvailibility';
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }

    getPhysicalDipList( params ): Promise<any> {
        return this._authHttp.get(this.listPhysicalDip, {search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getSiteList(): Promise<any> {
        return this._authHttp.get(this.siteUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolListFromSiteById(id: string): Promise<any> {
        return this._authHttp.get(this.getPolfromSiteUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankListFromSiteById(siteid: any,polid: any): Promise<any> {
        return this._authHttp.get( this.getTankFromPolUrl +  siteid+ '&POLId=' + polid )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCalculateQuantityAt15DegreeCelsius(density: string, temperature:string, volume): Promise<any> {
        return this._authHttp.get( this.getVolAt15Url + temperature + '&Density=' + density+ '&Quantity=' + volume )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteListForFilter(userId): Promise<any> {
        return this._authHttp.get(this.siteFilterUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPOLListForFilter(): Promise<any> {
        return this._authHttp.get(this.polFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getApprovedUserListForFilter(): Promise<any> {
        return this._authHttp.get(this.approvedUserFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getStatusListForFilter(): Promise<any> {
        return this._authHttp.get(this.statusFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getStorageTankListForFilter(): Promise<any> {
        return this._authHttp.get(this.tankFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPhysicalDipById( id ): Promise<any> {
        return this._authHttp.get(this.physicalDipByIdUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    savePhysicalDip(formParams: any): Promise<any>   {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this._authHttp.post(this.savePhysicalDipUrl, formParams)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletePhysicalDip(id): Promise<any> {
        return this._authHttp.post(this.deletePhysicalDipById + id,'')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankPOLAvailibility( id ): Promise<any> {
        return this._authHttp.get(this.tankPOLAvailabilityUrl + "?TankFilter=" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
}