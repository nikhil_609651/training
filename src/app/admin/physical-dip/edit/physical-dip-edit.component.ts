import { Component } from '@angular/core';

import { Router } from '@angular/router';
import { PhysicalDipComponentService } from 'app/admin/physical-dip/physical-dip.component.service';

@Component({
  selector: 'edit',
  templateUrl: './physical-dip-edit.component.html'
})
export class PhysicalDipEditComponent {

    page : any = 'edit';
    success : any = '';
    error : any = '';
    physicalDip : any;
    
	constructor(private _router: Router, private physicalDipService : PhysicalDipComponentService) {
    }

    /*getPhysicalDipById(id) {
        this.physicalDipService.getPhysicalDipById(id).then(response => {
            this.physicalDip = response.result;
            this.initPDForm(this.physicalDip);
        })
        .catch(r => {
            this.handleError(r);
        })
    }*/

    onSavePhysicalDip(pdForm){
        let siteId = pdForm.pdTable[0].SiteId;
        let polId = pdForm.pdTable[0].POLId;
        let physicalDipBatch = JSON.stringify(pdForm.pdTable);
        let formData = new FormData();
        formData.append('Id', pdForm.id);
        let date;
        if(new Date().getDate()<10) {
            date = '0'+new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else {
            date = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear(); 
        }
        if (pdForm.date) {
            date = pdForm.date.formatted;
        }
        formData.append('TransactionDate', date);
        formData.append('SiteId', siteId);
        formData.append('POLId', polId);
        //formData.append('StorageTankId','');
        formData.append('DrumStockQuanity', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('DrumStockQuanity', pdForm.pipelineAndDrum.drumStockQuantity);
        }
        formData.append('DrumStockVolume', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('DrumStockVolume', pdForm.pipelineAndDrum.drumStockVolume);
        }
        formData.append('PipelineQuantity', '0');
        if(pdForm.pipelineAndDrum.drumStockQuantity){
            formData.append('PipelineQuantity', pdForm.pipelineAndDrum.quantity);
        }
        formData.append('Remarks', pdForm.remarks);
        formData.append('PhysicalDipBatchDetails',physicalDipBatch);
        this.physicalDipService.savePhysicalDip(formData).then(r =>  {
            this.success = 'Physical Dip Updated Successfully!'; 
            jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
            setTimeout(function() {
               this.success = '';
               this._router.navigate(['./home/physical-dip']);
            }.bind(this), 3000);
          }).catch(r =>  {
            this.handleError(r);
          })
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
    }
}
