import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'physical-dip',
  template: `<router-outlet></router-outlet>`
})
export class PhysicalDipComponent {

	constructor(private _router: Router) {
    }
}
