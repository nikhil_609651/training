import { Component, OnInit } from "@angular/core";
import { Configuration } from "app/app.constants";
import { PhysicalDipComponentService } from "app/admin/physical-dip/physical-dip.component.service";
import { Router } from "@angular/router";
import { URLSearchParams } from "@angular/http"
import { FormGroup, FormArray, FormControl } from "@angular/forms";
import { Subscription } from "rxjs/internal/Subscription";
import { SharedService } from "app/services/shared.service";
import { IMyOptions } from 'mydatepicker';

@Component({
    selector: 'list',
    templateUrl: './physical-dip-list.component.html',
})
export class PhysicalDipListComponent implements OnInit {

    public rows:Array<any> = [];
    public columns : Array<any> = [

        { title: 'ID', name: 'Id', sort: true },
        { title: 'Site name', name: 'Site', sort: true, filter:true },
        { title: 'Date', name: 'Date', sort: true, filter:false },
        { title: 'POL', name: 'POL', sort: false, filter:true },
        { title: 'Tank ID', name: 'TankId', sort: true, filter:true  },
        { title: 'Tank Name', name: 'TankName', sort: true, filter:true },
        { title: 'Opening stock in the tank', name: 'Opening stock', sort:true, filter:false },
        { title: 'Closing stock in the tank', name: 'Closing stock in the tank', sort:true, filter:false },
        { title: 'Physical stock', name: 'Physical stock', sort:true, filter:false },
        { title: 'Closing DIP cm', name: 'Closing DIP cm', sort:true, filter:false },
        { title: 'Observed density', name: 'Observed density', sort:false, filter:false },
        { title: 'Observed temperature', name: 'Observed temperature', sort:false, filter:false },
        { title: 'Volume at 15°C', name: 'volumeAt15Degree', sort:false, filter:false },
        { title: 'Loss/ Gain', name: 'Loss/ Gain', sort:true, filter:false },
        { title: 'Ullage', name: 'Ullage', sort: true, filter:false },
        { title: 'Status', name: 'Status', sort: true, filter:true },
        { title: 'Approver', name: 'Approver', sort: false, filter:true },
        { title: 'Action', className: ['text-center'], name: 'actions', sort: false, filter:false }

      ];

    public config:any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-bordered']
    };

    params: URLSearchParams = new URLSearchParams();
    private currentdate= new Date();
    private initialSearchFromDate: Object = { date: { day: 1, month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    private initialSearchToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

    private searchFromDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
    };
    
    private searchToDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
    };
  
    private myDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
    };

    private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

    privileges : any = {
      isAdd: false,
      isDelete : false,
      isEdit : false,
      isView : false
    }
    privilegeSubscription : Subscription;

    error : any = '';
    physicalDipList : any ;
    loading : boolean = false;
    public page : number = 1;
    public length : number = 0;
    public next : string = '';
    public itemsPerPage : number = 10;
    public start : number = 1;
    public maxSize : number = 5;
    public numPages : number = 2;
    deleteMsg : string = '';
    physicalDipID : any = '';
    siteFiltersList : any;
    polFiltersList : any;
    approvedUserFiltersList : any;
    statusFiltersList : any;
    storageTankFiltersList : any;
    selectedsiteAll : any;
    selectedPolAll : any;
    selectedTankIdAll : any;
    selectedTankNameAll : any;
    selectedStatusAll : any;
    selectedApproverAll : any;
    checkedsite: string[] = [];
    checkedPol: string[] = [];
    checkedTankId: string[] = [];
    checkedTankName: string[] = [];
    checkedStatus: string[] = [];
    checkedApprover: string[] = [];
    viewPdForm : FormGroup;
    filterArray : any = [];
    physicalDip : any;
    tankDetails : any = [];
    fromDate = '';
    toDate = '';
    API_URL_Export: string;

    constructor( private configuration : Configuration, private physicalDipService : PhysicalDipComponentService,
       private _router : Router, private _sharedService : SharedService ) {

        /**user privileges**/	
        this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	        this.privileges = privileges;
        });
        let searchToDate = '';
        if(Number(new Date().getMonth()+1) < 10 && new Date().getDate() < 10) {
          searchToDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else if(new Date().getDate() < 10 && Number(new Date().getMonth()+1) >= 10) {
          searchToDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else if(new Date().getDate() >= 10 && Number(new Date().getMonth()+1) < 10) {
          searchToDate = new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else {
          searchToDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        }
        let searchFromDate = '';
        if(Number(new Date().getMonth()+1) < 10) {
          searchFromDate = '01' + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else if( Number(new Date().getMonth()+1) >= 10) {
          searchFromDate = '01' + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        } else {
          searchFromDate = '01/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
        }
        this.params.set('FromDate', searchFromDate);
        this.params.set('ToDate', searchToDate);
        this.params.set('limit', this.configuration.itemsPerPage.toString());
        this.params.set('UserId', localStorage.getItem('user_nameId'));
        this.rows = configuration.rows;
        if ((new Date().getDate() < 10) && (new Date().getMonth() > 10) ) {
          this.toDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }
    
        if ((new Date().getDate() < 10) && (new Date().getMonth() < 10) ) {
          this.toDate = '0' + new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }
    
        if ((new Date().getDate() > 10) && (new Date().getMonth() < 10) ) {
          this.toDate =  new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate =  new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }
    }

    ngOnInit(){
        let userId = localStorage.getItem('user_nameId');
        this.initViewPdForm();
        this.getAllPhysicalDips( this.params );
        this.getAllSitesForFilter(userId);
        this.getAllPOLForFilter();
        this.getApprovedUserListForFilter();
        this.getStatusListForFilter();
        this.getStorageTankListForFilter();
    }

    ngOnDestroy() {
      this.privilegeSubscription.unsubscribe();
    }

    getAllPhysicalDips( params : any ) {
        console.log('params');
        this.physicalDipService.getPhysicalDipList( params ).then(response =>  {
            console.log('response', response);
            this.physicalDipList = response.result;
            if( response.result.length > 0 ) {
              this.length = response['count'];
              this.next = response['next'];
            }
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
          });
    }

    sort(param, order) {
        this.params.set('ordering', param);
        if(order === 'desc'){
          this.params.set('ordering', '-'+param);
        }
        if(order === 'asc'){
          this.params.set('ordering', param);
        }
        this.getAllPhysicalDips(this.params);
    }


    public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
        let params = this.params;
        let start = (page.page - 1) * page.itemsPerPage;
        this.start = start + 1;
        params.set('limit', page.itemsPerPage);
        params.set('offset',start.toString());
        var sortParam = '';
        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
          params.set(this.config.filtering.name, this.config.filtering.filterString);
        }
        var sortParam = '';
        this.config.sorting.columns.forEach( function( col, key ) {
          if(col.sort){
            if(col.sort == 'desc') {
              sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
            } else if(col.sort == 'asc') {
              sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
            }
          }
        });
        if(sortParam != '') {
          params.set('ordering', sortParam);
        }
        this.getAllPhysicalDips(this.params);
    }

    onSelectChange(event) {
        console.log('event',event)
        let changedValue = parseInt(event.target.value);

        if(this.next || (changedValue < this.itemsPerPage)) {
          this.itemsPerPage =  event.target.value;
          console.log('this.itemsPerPage',this.itemsPerPage)
          console.log('this.page',this.page)
          let params = this.params;

          params.set('limit', event.target.value);
          this.getAllPhysicalDips(params);
        }
    }

    addNew() {
        this._router.navigate(['./home/physical-dip/add']);
    }

    deleteConfirm() {
        this.physicalDipService.deletePhysicalDip(  this.physicalDipID ).then(response =>  {
          console.log('response', response);
          if(response) {
            this.deleteMsg = 'Physical Dip successfully deleted';
          } else {
            this.deleteMsg = 'Failed to delete Physical Dip';
          }
          }).catch(response =>  {
            this.deleteMsg = 'Failed to delete Physical Dip';
            this.handleError(response);
        });
    }

    cancelModal() {
        this._router.navigate(['./home/physical-dip']);
    }

    deletePhysicalDip(roleId) {
       this.physicalDipID = roleId;
       this.deleteMsg = '';
    }

    deleteMessageClear(){
       this.deleteMsg = '';
    }

    editPhysicalDip(id, tankId){
        this._router.navigate(['./home/physical-dip/edit', id , tankId ]);
    }

    initViewPdForm(){
        let pdTable = new FormArray([])
        this.viewPdForm = new FormGroup({
            id : new FormControl(0),
            site : new FormControl(''),
            pol : new FormControl(''),
            date : new FormControl(''),
            pdTable : pdTable ,
            pipelineAndDrum : new FormGroup({
                quantity : new FormControl(''),
                drumStockQuantity : new FormControl(''),
                drumStockVolume : new FormControl('')
            }),
            remarks : new FormControl('')
        });
    }

    getPhysicalDipById(id) {
        this.physicalDipService.getPhysicalDipById(id).then(response => {
            console.log('viewPhysicalDip', response);
            this.physicalDip = response;
            this.tankDetails = response.physicalDipTankDetails;
            //this.storageTankList = response.physicalDipTankDetails;
            this.initViewPhysicalDip(this.physicalDip);
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    viewPhysicalDip(id){
        this.physicalDipID = id;
        console.log('viewphysicalDipID', this.physicalDipID);
        this.getPhysicalDipById(this.physicalDipID);
    }

    initViewPhysicalDip(data){
        this.viewPdForm.controls['id'].setValue(data['id']);
        this.viewPdForm.controls['site'].setValue(data['site'].name);
        this.viewPdForm.controls['pol'].setValue(data['pol'].name);
        /*this.siteId = data['siteId'];
        this.physicalDipService.getPolListFromSiteById( this.pdForm.controls['site'].value ).then(response => {
            this.polBySitelist = response;
            this.pdForm.controls['pol'].setValue(data['polId']);
            this.getTankListFromSite(data['polId'], data);
        })
        .catch(r => {
            this.handleError(r);
        });*/
        let dipDate = new Date(data['transactionDate']);
        let formattedDipDate = dipDate.getDate() + '-' + Number(dipDate.getMonth()+1) + '-' + dipDate.getFullYear();
        this.viewPdForm.controls['date'].setValue(formattedDipDate);
        this.viewPdForm.controls['remarks'].setValue(data['remarks']);
        (<FormGroup>this.viewPdForm.controls['pipelineAndDrum']).controls['drumStockQuantity'].setValue(data['drumStockQuantity']);
        (<FormGroup>this.viewPdForm.controls['pipelineAndDrum']).controls['drumStockVolume'].setValue(data['drumStockVolume']);
        (<FormGroup>this.viewPdForm.controls['pipelineAndDrum']).controls['quantity'].setValue(data['pipelineQuantity']);
    }

    viewclose(){
        setTimeout(function () {
            jQuery('#ViewPhysicalDip').modal('toggle');
        }.bind(this), 0);
    }

    getAllSitesForFilter(userId) {
        this.physicalDipService.getSiteListForFilter(userId).then(response =>  {
            console.log('sitefilterresponse', response);
            this.siteFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    getAllPOLForFilter() {
        this.physicalDipService.getPOLListForFilter().then(response =>  {
            console.log('polFiltersList', response);
            this.polFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    getApprovedUserListForFilter() {
        this.physicalDipService.getApprovedUserListForFilter().then(response =>  {
            console.log('approvedUserFiltersList', response);
            this.approvedUserFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    getStatusListForFilter() {
        this.physicalDipService.getStatusListForFilter().then(response =>  {
            console.log('getStatusListForFilter', response);
            this.statusFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    getStorageTankListForFilter() {
        this.physicalDipService.getStorageTankListForFilter().then(response =>  {
            console.log('storageTankFiltersList', response);
            this.storageTankFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    apply(item) {
        this.getAllPhysicalDips(this.params);
        if(item === 'POL'){
              setTimeout(function () {
                jQuery('#ViewPOLModal').modal('toggle');
              }.bind(this) , 0);
             }
         if(item === 'Site'){
          setTimeout(function () {
            jQuery('#ViewSiteModal').modal('toggle');
          }.bind(this) , 0);
         }
         if(item === 'TankId'){
            setTimeout(function () {
              jQuery('#ViewTankIdModal').modal('toggle');
            }.bind(this) , 0);
           }
           if(item === 'TankName'){
            setTimeout(function () {
              jQuery('#ViewTankNameModal').modal('toggle');
            }.bind(this) , 0);
           }
         if(item === 'Status'){
          setTimeout(function () {
            jQuery('#ViewStatusModal').modal('toggle');
          }.bind(this) , 0);
         }
         if(item === 'Approver'){
          setTimeout(function () {
            jQuery('#ViewApproverModal').modal('toggle');
          }.bind(this) , 0);
         }
         jQuery('ViewPOLModal').modal('hide');
         jQuery('ViewSiteModal').modal('hide');
         jQuery('ViewTankIdModal').modal('hide');
         jQuery('ViewTankNameModal').modal('hide');
         jQuery('ViewStatusModal').modal('hide');
         jQuery('ViewApproverModal').modal('hide');
         jQuery('body').removeClass('modal-open');
         jQuery('.modal-backdrop').remove();
    }

    selectAll(item, event) {
          if(item === 'Site') {
              this.filterArray = [];
              for (var i = 0; i < this.siteFiltersList.length; i++) {
                this.siteFiltersList[i].selected = this.selectedsiteAll;
                if(event.target.checked){
                    this.filterArray.push(this.siteFiltersList[i].siteId.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('SiteFilter', this.filterArray.toString());
          }
          if(item === 'POL'){
              this.filterArray = [];
              for (var i = 0; i < this.polFiltersList.length; i++) {
                this.polFiltersList[i].selected = this.selectedPolAll;
                if(event.target.checked){
                    this.filterArray.push(this.polFiltersList[i].id.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('POLFilter', this.filterArray.toString());
          }
          if(item === 'TankId'){
              this.filterArray = [];
              for (var i = 0; i < this.storageTankFiltersList.length; i++) {
                this.storageTankFiltersList[i].selected = this.selectedTankIdAll;
                if(event.target.checked){
                    this.filterArray.push(this.storageTankFiltersList[i].storageTankId.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('TankIdFilter', this.filterArray.toString());
          }
          if(item === 'TankName'){
              this.filterArray = [];
              for (var i = 0; i < this.storageTankFiltersList.length; i++) {
                this.storageTankFiltersList[i].selected = this.selectedsiteAll;
                if(event.target.checked){
                    this.filterArray.push(this.storageTankFiltersList[i].storageTankId.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('TankNameFilter', this.filterArray.toString());
          }
          if(item === 'Status'){
              this.filterArray = [];
              for (var i = 0; i < this.statusFiltersList.length; i++) {
                this.statusFiltersList[i].selected = this.selectedStatusAll;
                if(event.target.checked){
                    this.filterArray.push(this.statusFiltersList[i].id.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('StatusFilter', this.filterArray.toString());
          }
          if(item === 'Approver'){
              this.filterArray = [];
              for (var i = 0; i < this.approvedUserFiltersList.length; i++) {
                this.approvedUserFiltersList[i].selected = this.selectedsiteAll;
                if(event.target.checked){
                    this.filterArray.push(this.approvedUserFiltersList[i].id.toString());
                } else {
                    this.filterArray = [];
                }
              }
              this.params.set('ApproverFilter', this.filterArray.toString());
          }
          /*if(item === 'Status'){
              for (var i = 0; i < this.statusGroups.length; i++) {

                 this.statusGroups[i].selected = this.selectedstatusAll;
               }
               this.params.set('StatusFilter', null);
            }
            if(item === 'Approver'){
              for (var i = 0; i < this.approverGroups.length; i++) {

                 this.approverGroups[i].selected = this.selectedapproverAll;
               }
               this.params.set('ApproverFilter', null);
         }*/
    }

    checkIfAllSelected(option, event, item) {
       if( item === 'Site'){
          this.selectedsiteAll = this.siteFiltersList.every(function(item:any) {
              return item.selected == true;
          })
          var key = event.target.value.toString();
          var index = this.checkedsite.indexOf(key);
          if(event.target.checked) {
            this.checkedsite.push(event.target.value);
          } else {
            this.checkedsite.splice(index, 1);
          }
          this.params.set('SiteFilter', this.checkedsite.toString());
        }
        if( item === 'POL'){
            this.selectedPolAll = this.polFiltersList.every(function(item:any) {
                return item.selected == true;
            })
            var key = event.target.value.toString();
            var index = this.checkedPol.indexOf(key);
            if(event.target.checked) {
              this.checkedPol.push(event.target.value);
            } else {
              this.checkedPol.splice(index, 1);
            }
            this.params.set('POLFilter', this.checkedPol.toString());
        }
        if( item === 'TankId'){
          this.selectedTankIdAll = this.storageTankFiltersList.every(function(item:any) {
              return item.selected == true;
          })
          var key = event.target.value.toString();
          var index = this.checkedTankId.indexOf(key);
          if(event.target.checked) {
            this.checkedTankId.push(event.target.value);
          } else {
             this.checkedTankId.splice(index, 1);
          }
          this.params.set('TankIdFilter', this.checkedTankId.toString());
        }
        if( item === 'TankName'){
          this.selectedTankNameAll = this.storageTankFiltersList.every(function(item:any) {
              return item.selected == true;
          })
          var key = event.target.value.toString();
          var index = this.checkedTankName.indexOf(key);
          if(event.target.checked) {
            this.checkedTankName.push(event.target.value);
          } else {
            this.checkedTankName.splice(index, 1);
          }
          this.params.set('TankNameFilter', this.checkedTankName.toString());
        }
        if( item === 'Status'){
          this.selectedStatusAll = this.statusFiltersList.every(function(item:any) {
            return item.selected == true;
          })
          var key = event.target.value.toString();
          var index = this.checkedStatus.indexOf(key);
          if(event.target.checked) {
            this.checkedStatus.push(event.target.value);
          } else {
            this.checkedStatus.splice(index, 1);
          }
          this.params.set('StatusFilter', this.checkedStatus.toString());
        }
        if( item === 'Approver'){
          this.selectedApproverAll = this.approvedUserFiltersList.every(function(item:any) {
            return item.selected == true;
          })
          var key = event.target.value.toString();
          var index = this.checkedApprover.indexOf(key);
          if(event.target.checked) {
            this.checkedApprover.push(event.target.value);
          } else {
            this.checkedApprover.splice(index, 1);
          }
          this.params.set('ApproverFilter', this.checkedApprover.toString());
        }
    }

    onSearchDateChanged(date, type) {
      let dateFromPicker = { year: date.date.year, month: date.date.month, day: date.date.day };
      if(date.date.month < 10 && date.date.day < 10) {
        date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      } else if(date.date.day < 10 && date.date.month >= 10) {
        date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
      } else if(date.date.day >= 10 && date.date.month < 10) {
        date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      } else {
        date = date.date.day + '/' + date.date.month + '/' + date.date.year;
      }
      if(type == 'fromDate') {
        let toDateOptionCopy = this.getCopyOfSearchToDatePickerOptions()
        dateFromPicker.day = dateFromPicker.day-1;
        toDateOptionCopy.disableUntil = dateFromPicker;
        this.searchToDatePickerOptions = toDateOptionCopy;
        this.params.set('FromDate', date);
        this.getAllPhysicalDips(this.params);
      } else if(type == 'toDate') {
        let fromDateOptionCopy = this.getCopyOfSearchFromDatePickerOptions();
        dateFromPicker.day = dateFromPicker.day+1;
        fromDateOptionCopy.disableSince = dateFromPicker;
        this.searchFromDatePickerOptions = fromDateOptionCopy;
        this.params.set('ToDate', date);
        this.getAllPhysicalDips(this.params);
      }
    }
    
    getCopyOfSearchToDatePickerOptions(): IMyOptions {
        return JSON.parse(JSON.stringify(this.searchToDatePickerOptions));
    }
    
    getCopyOfSearchFromDatePickerOptions(): IMyOptions {
        return JSON.parse(JSON.stringify(this.searchFromDatePickerOptions));
    }

    onDateChanged(event: any, type: any) {
      if (type == 'FromDate') {
        this.fromDate = event.formatted;
      }
      if (type == 'ToDate') {
        this.toDate = event.formatted;
      }
    }

    export() {
      let userId = localStorage.getItem('user_nameId');
      this.API_URL_Export = this.configuration.ServerWithApiUrl + 'PhysicalDip/ExportPhysicalDipFiles?FromDate=' + this.fromDate + '&ToDate=' + this.toDate + '&UserId=' + userId;
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
      }

}