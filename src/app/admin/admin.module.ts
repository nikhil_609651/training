import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../theme/nga.module';
import { PaginationModule, TabsModule, TooltipModule } from 'ng2-bootstrap';
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { TextMaskModule } from 'angular2-text-mask';
import { BreadcrumbsComponent } from '../shared/breadcrumbs.component';
import {Http, Response, Request, RequestMethod} from '@angular/http';
import {ChartsModule, Color} from 'ng2-charts';
import { routing }       from './admin.routing';
import { Admin } from './admin.component';
import { NoContentComponent } from './no-content';
import { DatePipe } from '@angular/common';
import {SelectModule} from 'angular2-select';
import { NgSpinKitModule } from 'ng-spin-kit'

import { SharedService }  from '../services/shared.service';

/*import { ChartsModule } from 'ng2-charts';*/
import { AgmCoreModule } from 'angular2-google-maps/core';
// import { CookieService } from 'angular2-cookie/services/cookies.service';
//import { AddOrderComponent } from './add-order/add-order.component';

import { BusinessUnitListComponent } from 'app/admin/master/business_unit/list/businessunit-list.component';
import { BusinessUnitFormService } from 'app/admin/master/business_unit/services/businessunit-form.service';
import { BusinessUnitAddComponent } from 'app/admin/master/business_unit/add/businessunit-add.component';
import { BusinessUnitComponent } from 'app/admin/master/business_unit/businessunit.component';
import { BusinessUnitFormComponent } from 'app/admin/master/business_unit/businessunit-form/businessunit-form.component';
import { BusinessUnitEditComponent } from 'app/admin/master/business_unit/edit/businessunit-edit.component';
import { CustomersComponent } from 'app/admin/master/customers/customers.component';
import { CustomersListComponent } from 'app/admin/master/customers/list/customers-list.component';
import { CustomersAddComponent } from 'app/admin/master/customers/add/customers-add.component';
import { CustomersFormService } from 'app/admin/master/customers/services/customers-form.service';
import { CustomersFormComponent } from 'app/admin/master/customers/customers-form/customers-form.component';
import { DischargeVehicleComponent } from 'app/admin/master/discharge_vehicle/dischargevehicle.component';
import { DischargeVehicleListComponent } from 'app/admin/master/discharge_vehicle/list/dischargevehicle-list.component';
import { DischargeVehicleFormService } from 'app/admin/master/discharge_vehicle/services/dischargevehicle-form.service';
import { DischargeVehicleAddComponent } from 'app/admin/master/discharge_vehicle/add/dischargevehicle-add.component';
import { DischargeVehicleFormComponent } from 'app/admin/master/discharge_vehicle/dischargevehicle-form/dischargevehicle-form.component';
import { DischargeVehicleEditComponent } from 'app/admin/master/discharge_vehicle/edit/dischargevehicle-edit.component';
import { DriverComponent } from 'app/admin/master/driver/driver.component';
import { DriverListComponent } from 'app/admin/master/driver/list/driver-list.component';
import { DriverAddComponent } from 'app/admin/master/driver/add/driver-add.component';
import { DriverFormComponent } from 'app/admin/master/driver/driver-form/driver-form.component';
import { DriverFormService } from 'app/admin/master/driver/services/driver-form.service';
import { DriverEditComponent } from 'app/admin/master/driver/edit/driver-edit.component';
import { CountryFormComponent } from 'app/admin/master/country/country-form/country-form.component';
import { CountryAddComponent } from 'app/admin/master/country/add/country-add.component';
import { CountryEditComponent } from 'app/admin/master/country/edit/country-edit.component';
import { CountryListComponent } from 'app/admin/master/country/list/country-list.component';
import { CountryComponent } from 'app/admin/master/country/country.component';
import { SectionMasterComponent } from 'app/admin/master/sectionmaster/section.component';
import { SectionEditComponent } from 'app/admin/master/sectionmaster/edit/section-edit.component';
import { SectionAddComponent } from 'app/admin/master/sectionmaster/add/section-add.component';
import { SectionListComponent } from 'app/admin/master/sectionmaster/list/section-list.component';
import { SectionFormComponent } from './master/sectionmaster/section-form/section-form.component';
import { SectionMasterFormService } from './master/sectionmaster/services/section-form.service';
import { OwnershipComponent } from 'app/admin/master/ownership/ownership.component';
import { OwnershipListComponent } from 'app/admin/master/ownership/list/ownership-list.component';
import { OwnershipAddComponent } from 'app/admin/master/ownership/add/ownership-add.component';
import { OwnershipEditComponent } from 'app/admin/master/ownership/edit/ownership-edit.component';
import { OwnershipFormService } from 'app/admin/master/ownership/services/ownership-form.service';
import { OwnershipFormComponent } from './master/ownership/ownership-form/ownership-form.component';
import { IssuerComponent } from 'app/admin/master/issuer/issuer.component';
import { IssuerListComponent } from 'app/admin/master/issuer/list/issuer-list.component';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';
import { IssuerAddComponent } from 'app/admin/master/issuer/add/issuer-add.component';
import { IssuerFormComponent } from 'app/admin/master/issuer/issuer-form/issuer-form.component';
import { IssuerEditComponent } from 'app/admin/master/issuer/edit/issuer-edit.component';
import { ReceiverComponent } from 'app/admin/master/receiver/receiver.component';
import { ReceiverListComponent } from 'app/admin/master/receiver/list/receiver-list.component';
import { ReceiverFormService } from 'app/admin/master/receiver/service/receiver.service';
import { ReceiverAddComponent } from 'app/admin/master/receiver/add/receiver-add.component';
import { ReceiverFormComponent } from 'app/admin/master/receiver/receiver-form/receiver-form.component';
import { ReceiverEditComponent } from 'app/admin/master/receiver/edit/receiver-edit.component';
import { SubUnitFormService } from 'app/admin/master/subunit/services/subunit-form.service';
import { SubUnitComponent } from 'app/admin/master/subunit/subunit.component';
import { SubUnitListComponent } from 'app/admin/master/subunit/list/subunit-list.component';
import { SubUnitEditComponent } from 'app/admin/master/subunit/edit/subunit-edit.component';
import { SubUnitFormComponent } from 'app/admin/master/subunit/subunit-form/subunit-form.component';
import { SubUnitAddComponent } from 'app/admin/master/subunit/add/subunit-add.component';
import { AirCraftTypeComponent } from 'app/admin/master/equipmentmaster/aircrafttype/aircrafttype.component';
import { AirCraftTypeListComponent } from 'app/admin/master/equipmentmaster/aircrafttype/list/aircrafttype-list.component';
import { AirCraftTypeFormService } from 'app/admin/master/equipmentmaster/aircrafttype/services/aircrafttype-form.service';
import { AirCraftTypeFormComponent } from 'app/admin/master/equipmentmaster/aircrafttype/aircrafttype-form/aircrafttype-form.component';
import { AirCraftTypeAddComponent } from 'app/admin/master/equipmentmaster/aircrafttype/add/aircrafttype-add.component';
import { AirCraftTypeEditComponent } from 'app/admin/master/equipmentmaster/aircrafttype/edit/aircrafttype-edit.component';
import { UomComponent } from 'app/admin/master/uom/uom.component';
import { UomListComponent } from 'app/admin/master/uom/list/uom-list.component';
import { UomAddComponent } from 'app/admin/master/uom/add/uom-add.component';
import { UomEditComponent } from 'app/admin/master/uom/edit/uom-edit.component';
import { UomFormComponent } from 'app/admin/master/uom/uom-form/uom-form.component';
import { UomFormService } from 'app/admin/master/uom/service/uom.service';
import { CustomersEditComponent } from 'app/admin/master/customers/edit/customers-edit.component';
import { UserManagementComponent } from 'app/admin/usermanagement/usermanagement.component';
import { UserManagementAddComponent } from 'app/admin/usermanagement/add/usermanagement-add.component';
import { UserManagementFormComponent } from 'app/admin/usermanagement/usermanagement-form/usermanagement-form.component';
import { UserManagementFormService } from 'app/admin/usermanagement/services/usermanagement.service';
import { UserManagementListComponent } from 'app/admin/usermanagement/list/usermanagement-list.component';
import { UserManagementEditComponent } from 'app/admin/usermanagement/edit/usermanagement-edit.component';
import { AircraftFormService } from 'app/admin/master/equipmentmaster/aircraft/services/aircraft-form.service';
import { AirCraftComponent } from 'app/admin/master/equipmentmaster/aircraft/aircraft.component';
import { AircraftListComponent } from 'app/admin/master/equipmentmaster/aircraft/list/aircraft-list.component';
import { AircraftFormComponent } from 'app/admin/master/equipmentmaster/aircraft/aircraft-form/aircraft-form.component';
import { AircraftAddComponent } from 'app/admin/master/equipmentmaster/aircraft/add/aircraft-add.component';
import { AircraftEditComponent } from 'app/admin/master/equipmentmaster/aircraft/edit/aircraft-edit.component';
import { GeneratorMakeModelComponent } from 'app/admin/master/equipmentmaster/generatormakemodel/generatormakemodel.component';
import { GeneratorMakeModelListComponent } from 'app/admin/master/equipmentmaster/generatormakemodel/list/generatormakemodel-list.component';
import { GeneratorMakeModelFormService } from 'app/admin/master/equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
import { GeneratorMakeModelAddComponent } from 'app/admin/master/equipmentmaster/generatormakemodel/add/generatormakemodel-add.component';
import { GeneratorMakeModelEditComponent } from 'app/admin/master/equipmentmaster/generatormakemodel/edit/generatormakemodel-edit.component';
import { GeneratorMakeModelFormComponent } from 'app/admin/master/equipmentmaster/generatormakemodel/generatormakemodel-form/generatormakemodel-form.component';
import { TruckComponent } from 'app/admin/master/truck/truck.component';
import { TruckListComponent } from 'app/admin/master/truck/list/truck-list.component';
import { TruckAddComponent } from 'app/admin/master/truck/add/truck-add.component';
import { TruckEditComponent } from 'app/admin/master/truck/edit/truck-edit.component';
import { TruckFormComponent } from 'app/admin/master/truck/truck-form/truck-form.component';
import { TruckFormService } from 'app/admin/master/truck/service/truck.service';
import { PolFormService } from 'app/admin/master/pol/service/pol-service';
import { PolFormComponent } from 'app/admin/master/pol/pol-form/pol-form.component';
import { PolComponent } from 'app/admin/master/pol/pol-component';
import { PolListComponent } from 'app/admin/master/pol/list/pol-list.component';
import { PolAddComponent } from 'app/admin/master/pol/add/pol-add.component';
import { PolEditComponent } from 'app/admin/master/pol/edit/pol-edit.component';
import { DepartmentFormService } from 'app/admin/master/department/service/department-form.service';
import { DepartmentComponent } from 'app/admin/master/department/department.component';
import { DepartmentListComponent } from 'app/admin/master/department/list/department-list.component';
import { DepartmentAddComponent } from 'app/admin/master/department/add/department-add.component';
import { DepartmentFormComponent } from 'app/admin/master/department/department-form/department-form.component';
import { DepartmentEditComponent } from 'app/admin/master/department/edit/department-edit.component';
import { VehicleTypeComponent } from 'app/admin/master/equipmentmaster/vehicletype/vehicletype.component';
import { VehicleTypeListComponent } from 'app/admin/master/equipmentmaster/vehicletype/list/vehicletype-list.component';
import { VehicleTypeFormComponent } from 'app/admin/master/equipmentmaster/vehicletype/vehicletype-form/vehicletype-form.component';
import { VehicleTypeAddComponent } from 'app/admin/master/equipmentmaster/vehicletype/add/vehicletype-add.component';
import { VehicleTypeEditComponent } from 'app/admin/master/equipmentmaster/vehicletype/edit/vehicletype-edit.component';
import { VehicleMakeModelListComponent } from 'app/admin/master/equipmentmaster/vehiclemakemodel/list/vehiclemakemodel-list.component';
import { VehicleMakeModelComponent } from 'app/admin/master/equipmentmaster/vehiclemakemodel/vehiclemakemodel.component';
import { VehicleMakeModelAddComponent } from 'app/admin/master/equipmentmaster/vehiclemakemodel/add/vehiclemakemodel-add.component';
import { VehicleMakeModelEditComponent } from 'app/admin/master/equipmentmaster/vehiclemakemodel/edit/vehiclemakemodel-edit.component';
import { VehicleMakeModelFormComponent } from 'app/admin/master/equipmentmaster/vehiclemakemodel/vehiclemakemodel-form/vehiclemakemodel-form.component';
import { VehicleTypeFormService } from 'app/admin/master/equipmentmaster/vehicletype/service/vehicletype-form.service';
import { VehicleMakeModelFormService } from 'app/admin/master/equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';
import { ListViewAddComponent } from 'app/admin/fuelissuance/ListView/add/listview-add.component';
import { ListViewComponent } from 'app/admin/fuelissuance/ListView/listview.component';
import { ListViewFormComponent } from 'app/admin/fuelissuance/ListView/listview-form/listview-form.component';
import { ListViewListComponent } from 'app/admin/fuelissuance/ListView/list/listview-list.component';
import { ListViewFormService } from 'app/admin/fuelissuance/ListView/services/listview.service';
import { SiteFormService } from 'app/admin/master/site/services/site-form.service';
import { SiteComponent } from 'app/admin/master/site/site.component';
import { SiteListComponent } from 'app/admin/master/site/list/site-list.component';
import { SiteAddComponent } from 'app/admin/master/site/add/site-add.component';
import { SiteEditComponent } from 'app/admin/master/site/edit/site-edit.component';
import { SiteFormComponent } from 'app/admin/master/site/site-form/site-form.component';
import { StoragetankComponent } from 'app/admin/master/storagetank/storagetank.component';
import { StoragetankListComponent } from 'app/admin/master/storagetank/list/storagetank-list.component';
import { StoragetankAddComponent } from 'app/admin/master/storagetank/add/storagetank-add.component';
import { StoragetankEditComponent } from 'app/admin/master/storagetank/edit/storagetank-edit.component';
import { StoragetankFormComponent } from 'app/admin/master/storagetank/storagetank-form/storagetank-form.component';
import { StoragetankFormService } from 'app/admin/master/storagetank/service/storagetank-form.service';
import { EquipmentTypeComponent } from 'app/admin/master/equipmentmaster/equipmenttype/equipmenttype.component';
import { EquipmentTypeEditComponent } from 'app/admin/master/equipmentmaster/equipmenttype/edit/equipmenttype-edit.component';
import { EquipmentTypeAddComponent } from 'app/admin/master/equipmentmaster/equipmenttype/add/equipmenttype-add.component';
import { EquipmentTypeFormComponent } from 'app/admin/master/equipmentmaster/equipmenttype/equipmenttype-form/equipmenttype-form.component';
import { EquipmentTypeListComponent } from 'app/admin/master/equipmentmaster/equipmenttype/list/equipmenttype-list.component';
import { EquipmentTypeFormService } from 'app/admin/master/equipmentmaster/equipmenttype/service/equipmenttype-form.service';
import { ListViewEditComponent } from 'app/admin/fuelissuance/ListView/edit/listview-edit.component';
//import { DateTimePickerModule } from 'ng-pick-datetime';
//import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line you need
import { StockAdjustmentService } from 'app/admin/stockadjustment/services/stockadjustment.service';
import { StockAdjustmentAddComponent } from 'app/admin/stockadjustment/add/stockadjustment-add.component';
import { StockAdjustmentListComponent } from 'app/admin/stockadjustment/list/stockadjustment-list.component';
import { StockAdjustmentFormComponent } from 'app/admin/stockadjustment/stockadjustment-form/stockadjustment-form.component';
import { StockAdjustmentComponent } from 'app/admin/stockadjustment/stockadjustment.component';
import { StockAdjustmentEditComponent } from 'app/admin/stockadjustment/edit/stockadjustment-edit.component';
import { GeneratorComponent } from 'app/admin/master/equipmentmaster/generator/generator.component';
import { GeneratorListComponent } from 'app/admin/master/equipmentmaster/generator/list/generator-list.component';
import { GeneratorFormComponent } from 'app/admin/master/equipmentmaster/generator/generator-form/generator-form.component';
import { GeneratorAddComponent } from 'app/admin/master/equipmentmaster/generator/add/generator-add.component';
import { GeneratorEditComponent } from 'app/admin/master/equipmentmaster/generator/edit/generator-edit.component';
import { EquipmentComponent } from 'app/admin/master/equipmentmaster/equipment/equipment.component';
import { VehicleAddComponent } from 'app/admin/master/equipmentmaster/vehicle/add/vehicle-add.component';
import { VehicleFormComponent } from 'app/admin/master/equipmentmaster/vehicle/vehicle-form/vehicle-form.component';
import { VehicleListComponent } from 'app/admin/master/equipmentmaster/vehicle/list/vehicle-list.component';
import { VehicleComponent } from 'app/admin/master/equipmentmaster/vehicle/vehicle.component';
import { VehicleEditComponent } from 'app/admin/master/equipmentmaster/vehicle/edit/vehicle-edit.component';
import { EquipmentFormComponent } from 'app/admin/master/equipmentmaster/equipment/equipment-form/equipment-form.component';
import { EquipmentEditComponent } from 'app/admin/master/equipmentmaster/equipment/edit/equipment-edit.component';
import { EquipmentAddComponent } from 'app/admin/master/equipmentmaster/equipment/add/equipment-add.component';
import { EquipmentListComponent } from 'app/admin/master/equipmentmaster/equipment/list/equipment-list.component';
import { PurchaseReceiptComponent } from 'app/admin/receiptandtransfer/purchase-receipt/purchasereceipt.component';
import { PurchaseReceiptListComponent } from './receiptandtransfer/purchase-receipt/list/purchasereceipt-list.component';
import { PurchaseReceiptAddComponent } from 'app/admin/receiptandtransfer/purchase-receipt/add/purchasereceipt-add.component';
import { PurchaseReceiptEditComponent } from 'app/admin/receiptandtransfer/purchase-receipt/edit/purchasereceipt-edit.component';
import { TransferRequestComponent } from 'app/admin/receiptandtransfer/transferrequest/transferrequest.component';
import { TransferRequestListComponent } from 'app/admin/receiptandtransfer/transferrequest/list/transferrequest-list.component';
import { TransferRequestAddComponent } from 'app/admin/receiptandtransfer/transferrequest/add/transferrequest-add.component';
import { TransferRequestEditComponent } from 'app/admin/receiptandtransfer/transferrequest/edit/transferrequest-edit.component';
import { TransferComponent } from 'app/admin/receiptandtransfer/transfer/transfer.component';
import { TransferListComponent } from 'app/admin/receiptandtransfer/transfer/list/transfer-list.component';
import { TransferAddComponent } from 'app/admin/receiptandtransfer/transfer/add/transfer-add.component';
import { TransferEditComponent } from 'app/admin/receiptandtransfer/transfer/edit/transfer-edit.component';
import { TransferReceiptComponent } from 'app/admin/receiptandtransfer/transferreceipt/transferreceipt.component';
import { TransferReceiptListComponent } from 'app/admin/receiptandtransfer/transferreceipt/list/transferreceipt-list.component';
import { TransferReceiptAddComponent } from 'app/admin/receiptandtransfer/transferreceipt/add/transferreceipt-add.component';
import { TransferReceiptEditComponent } from 'app/admin/receiptandtransfer/transferreceipt/edit/transferreceipt-edit.component';
import { PurchaseReceiptFormService } from 'app/admin/receiptandtransfer/purchase-receipt/service/purchasereceipt-form.service';
import { TransferFormService } from 'app/admin/receiptandtransfer/transfer/service/transfer.service';
import { TransferReceiptFormService } from 'app/admin/receiptandtransfer/transferreceipt/service/transferreceipt.service';
import { TransferRequestFormService } from 'app/admin/receiptandtransfer/transferrequest/service/transferrequest.service';
import { PurchaseReceiptFormComponent } from 'app/admin/receiptandtransfer/purchase-receipt/purchasereceipt-form/purchasereceipt-form.component';
import { TransferRequestFormComponent } from 'app/admin/receiptandtransfer/transferrequest/transferrequest-form/transferrequest-form.component';
import { TransferFormComponent } from 'app/admin/receiptandtransfer/transfer/transfer-form/transfer-form.component';
import { TransferReceiptFormComponent } from 'app/admin/receiptandtransfer/transferreceipt/transferreceipt-form/transferreceipt-form.component';
import { TransportCompanyComponent } from 'app/admin/master/transportcompany/transportcompany.component';
import { TransportCompanyListComponent } from 'app/admin/master/transportcompany/list/transportcompany-list.component';
import { TransportCompanyEditComponent } from 'app/admin/master/transportcompany/edit/transportcompany-edit.component';
import { TransportCompanyAddComponent } from 'app/admin/master/transportcompany/add/transportcompany-add.component';
import { TransportCompanyFormComponent } from 'app/admin/master/transportcompany/transportcompany-form/transportcompany-form.component';
import { TransportCompanyFormService } from 'app/admin/master/transportcompany/service/transportcompany.service';
import { TankPolAvailabilityFormService } from 'app/admin/receiptandtransfer/tankpolavailability/service/tankpolavailability.service';
import { LubricantAvailabilityFormService } from 'app/admin/receiptandtransfer/lubricantavailability/service/lubricantavailability.service';
import { LubricantAvailabilityComponent } from 'app/admin/receiptandtransfer/lubricantavailability/lubricantavailability.component';
import { LubricantAvailabilityListComponent } from 'app/admin/receiptandtransfer/lubricantavailability/list/lubricantavailability-list.component';
import { TankPolAvailabilityComponent } from 'app/admin/receiptandtransfer/tankpolavailability/tankpolavailability.component';
import { TankPolAvailabilityListComponent } from 'app/admin/receiptandtransfer/tankpolavailability/list/tankpolavailability-list.component';
import { VehicleFormService } from 'app/admin/master/equipmentmaster/vehicle/service/vehicle-form.service';
import { GeneratorFormService } from 'app/admin/master/equipmentmaster/generator/service/generator-form.service';
import { EquipmentFormService } from 'app/admin/master/equipmentmaster/equipment/service/equipment-form.service';
import { RolesComponent } from 'app/admin/rolesandprivilages/roles.component';
import { RolesAddComponent } from 'app/admin/rolesandprivilages/add/roles-add.component';
import { RolesListComponent } from 'app/admin/rolesandprivilages/list/roles-list.component';
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';
import { RolesFormComponent } from 'app/admin/rolesandprivilages/roles-form/roles-form.component';
import { RolesEditComponent } from 'app/admin/rolesandprivilages/edit/roles-edit.component';
import { RolesViewComponent } from 'app/admin/rolesandprivilages/view/roles-view.component';
import { PhysicalDipComponent } from 'app/admin/physical-dip/physical-dip.component';
import { PasswordResetComponent } from 'app/admin/password-reset/password-reset.component';
import { PasswordResetComponentService } from 'app/admin/password-reset/password-reset.component.service';
import { PhysicalDipListComponent } from 'app/admin/physical-dip/list/physical-dip-list.component';
import { PhysicalDipComponentService } from 'app/admin/physical-dip/physical-dip.component.service';
import { PhysicalDipAddComponent } from 'app/admin/physical-dip/add/physical-dip-add.component';
import { PhysicalDipFormComponent } from 'app/admin/physical-dip/form/physical-dip-form.component';
import { PhysicalDipEditComponent } from 'app/admin/physical-dip/edit/physical-dip-edit.component';
import { PhysicalDipViewComponent } from 'app/admin/physical-dip/view/physical-dip-view.component';
import { ReportsComponent } from 'app/admin/reports/reports.component';
import { ReportsListComponent } from 'app/admin/reports/list/reports-list.component';
//import { AmazingTimePickerService, AmazingTimePickerModule } from 'amazing-time-picker';
//import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
//import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RefuellingHistoryListComponent } from 'app/admin/refuellinghistory/list/refuellinghistory-list.component';
import { RefuellingHistoryComponent } from 'app/admin/refuellinghistory/refuellinghistory.component';
import { RefuellingHistoryService } from 'app/admin/refuellinghistory/service/refuellinghistory.service';
import { SystemPreferenceFormComponent } from 'app/admin/systempreference/systempreference-form/systempreference-form.component';
import { SystemPreferenceComponent } from 'app/admin/systempreference/systempreference.component';
import { SystemPreferenceAddComponent } from 'app/admin/systempreference/add/systempreference-add.component';
//import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SystemPreferenceFormService } from 'app/admin/systempreference/systempreference.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReportsComponentService } from './reports/services/reports.component.service';
import { StockSummaryListComponent } from './fuelissuance/stock-summary/list/stock-summary-list.component';
import { StockSummaryComponent } from './fuelissuance/stock-summary/stock-summary.component';


@NgModule({
  imports: [
    CommonModule,
    NgaModule,
    TextMaskModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TabsModule,
    TooltipModule,
    DatePickerModule,
    MyDatePickerModule,
    routing,
    SelectModule,
    ChartsModule,
    AgmCoreModule,
    NgSpinKitModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  entryComponents:    [ ],
  declarations: [
    Admin,
    PasswordResetComponent,
    NoContentComponent,
    BreadcrumbsComponent,
    UserManagementListComponent,UserManagementEditComponent,
    UserManagementComponent,UserManagementAddComponent ,UserManagementFormComponent,
    StockAdjustmentAddComponent,StockAdjustmentComponent,
    StockAdjustmentListComponent,StockAdjustmentFormComponent,StockAdjustmentEditComponent,
    RolesComponent, RolesAddComponent, RolesListComponent, RolesFormComponent, RolesEditComponent, RolesViewComponent,
    PhysicalDipComponent, PhysicalDipListComponent, PhysicalDipAddComponent, PhysicalDipFormComponent, PhysicalDipEditComponent, PhysicalDipViewComponent,
    RefuellingHistoryListComponent,RefuellingHistoryComponent,
    SystemPreferenceFormComponent,SystemPreferenceComponent,
    SystemPreferenceAddComponent,
    ReportsComponent,
    ReportsListComponent
  ],

  exports: [],
  providers: [
    SharedService,
    DatePipe,
    UserManagementFormService,
    StockAdjustmentService,
    RolesComponentService,
    PasswordResetComponentService,
    PhysicalDipComponentService,
    RefuellingHistoryService,
    SystemPreferenceFormService,
    ReportsComponentService
   // AmazingTimePickerService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export default class AdminModule {
}