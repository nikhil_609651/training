import { Component, ViewChild } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { PasswordResetComponentService } from 'app/admin/password-reset/password-reset.component.service';

@Component({
  selector: 'password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent {
  passwordtoken: any;
  @ViewChild('ShowPassword') ShowPassword;
  @ViewChild('ShowConfirm') ShowConfirm;

  public resetForm : FormGroup;
  public show_password = true;
  public hide_password = false;
  public show_confirm_password = true;
  public hide_confirm_password = false;
  public passwordPattern = ".{7,20}";
  isNotValid : boolean = false;
  error : any;
  token : any;
  resetToken : any;
  userId : any;
  isResetTokenValid : boolean = true;//false;
  successMsg : string = '';

	constructor(private _router: Router, fb : FormBuilder, private _resetService : PasswordResetComponentService, private _activatedRoute : ActivatedRoute) {
    this.resetForm = fb.group({
      password   :  new FormControl('', [ Validators.required, Validators.pattern(this.passwordPattern) ] ),
      confirm   :  new FormControl('', [ Validators.required, Validators.pattern(this.passwordPattern) ] ),
    });

    this._activatedRoute.params.subscribe( params => this.token = params['token']);
    //parse the token to get pwd reset token and user id
    this.resetToken = '';
    this.userId = '';

    //this.checkPwdResetToken();
  }
  ngOnInit() {
    this._activatedRoute.params.forEach((params: Params) => {
            this.passwordtoken = params['token'];
          });
  }

  showHidePassword(value, element ) {
    if( element == 'password'){
      if (this.ShowPassword.nativeElement.type = 'password') {
        this.show_password = true;
        this.hide_password = false;
      } else {
        this.show_password = false;
        this.hide_password = true;
      }

      if (value === 'show') {
        this.show_password = false;
        this.hide_password = true;
        this.ShowPassword.nativeElement.type = 'text';
      } else {
        this.show_password = true;
        this.hide_password = false;
        this.ShowPassword.nativeElement.type = 'password';
      }
    } else if( element == 'confirmPassword') {
      if (this.ShowConfirm.nativeElement.type = 'password') {
        this.show_confirm_password = true;
        this.hide_confirm_password = false;
      } else {
        this.show_confirm_password = false;
        this.hide_confirm_password = true;
      }

      if (value === 'show') {
        this.show_confirm_password = false;
        this.hide_confirm_password = true;
        this.ShowConfirm.nativeElement.type = 'text';
      } else {
        this.show_confirm_password = true;
        this.hide_confirm_password = false;
        this.ShowConfirm.nativeElement.type = 'password';
      }
    }
  }

  isNotSame = false;
  onSubmit(form){
    if( form.controls['password'].value !== form.controls['confirm'].value ) {
      this.isNotValid = false;
      this.isNotSame = true;
    } else if( form.valid && form.controls['password'].value === form.controls['confirm'].value ) {
      this.isNotValid = false;
      this.isNotSame = false;
      this.resetPwd( form.controls['password'].value );
    } else {
      this.isNotValid = true;
    }
  }

  checkPwdResetToken() {
    this._resetService.checkPwdResetToken( { resetToken : this.resetToken, userId : this.userId } ).then(response =>  {
      this.isResetTokenValid = true;
      }).catch(response =>  {
        this.handleError(response);
    });
  }

  resetPwd( password ) {
    this._resetService.resetPwd( { userId :  this.passwordtoken, password : password } ).then(response =>  {      
      this.successMsg = 'Password changed successfully';
            jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
            setTimeout(function() {
                this.successMsg = '';
                this._router.navigate(['./login']);
            }.bind(this), 3000);
      }).catch(response =>  {
        this.handleError(response);
    });
  }

  private handleError(e: any) {
    this.error = e;
    console.log('error', this.error);
    let detail = e.detail
    if(e.status == '401') {
      this._router.navigate(['./login']);
    } else if(detail && detail == 'Signature has expired.') {
      this._router.navigate(['./login']);
    }
  }
}
