import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class PasswordResetComponentService{

    checkPwdResetTokenUrl = '';
    resetPwdUrl = '';
    constructor( private _authHttp : AuthHttp, private _config : Configuration ){
        this.checkPwdResetTokenUrl = _config.ServerWithApiUrl + '';
        this.resetPwdUrl = _config.ServerWithApiUrl + 'UserManagement/ChangePassword';
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }

    resetPwd(params): Promise<any> {
        return this._authHttp.get(this.resetPwdUrl+'?UserId= '+ params.userId + '&Password=' + params.password)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    checkPwdResetToken(params): Promise<any> {
        return this._authHttp.post(this.checkPwdResetTokenUrl, params)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

}