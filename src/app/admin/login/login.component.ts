import { Component, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { tokenNotExpired, JwtHelper, AuthHttp } from 'angular2-jwt';
import { LocalStorageService } from 'ng2-webstorage';
import { CustomValidator } from '../../shared/custom-validator';
import { CookieService } from 'angular2-cookie/core';
import { Configuration } from 'app/app.constants';

@Component({
  selector: 'login',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./login.scss')],
  template: require('./login.html'),
})
export class Login implements OnInit {
  successMsg: string;
  messageList: any = [];
  @ViewChild('ShowPassword') ShowPassword;
  @ViewChild('resetEmail') resetEmail;

  public form:FormGroup;
  public email:AbstractControl;
  public password:AbstractControl;
  public remember :AbstractControl;
  public error = '';
  public user="";
  public pass="";
  public rem=""
  public status ="";
  public show_password = true;
  public hide_password = false;
  public emailPattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}";
  //public passwordPattern = "(?=[^\d_].*?\d)\w(\w|[!@#$%]){7,20}";
  public passwordPattern = ".{7,20}";
  public hasError : boolean = false;
  userEmail : string = '';
  isMailSend : boolean = false;
  emailSendError : boolean = false;
  url : any;
  isInvalidMailId : boolean = false;
  isEmailRequired : boolean = false;
  jwtHelper: JwtHelper = new JwtHelper();

  constructor( fb:FormBuilder, private storage:LocalStorageService, private AuthService: AuthService, private router: Router, private _cookieService:CookieService,
               private configuration : Configuration
    ) {
    this.url = 'http://localhost:3000/#/forgetPassword';//configuration.applicationUrl;
    this.form = fb.group({
      'email'   :  ['', [ Validators.required, Validators.pattern(this.emailPattern) ] ],
      'password':  ['', [ Validators.required, Validators.pattern(this.passwordPattern) ]],
      'remember':  [''],
    });

    this.email     =  this.form.controls['email'];
    this.password  =  this.form.controls['password'];
    this.remember  =  this.form.controls['remember'];

    if(this._cookieService.get('remember')){
      this.form.controls['email'].setValue(this._cookieService.get('username'));
      this.form.controls['password'].setValue(this._cookieService.get('password'));
      this.form.controls['remember'].setValue(this._cookieService.get('remember'));
    }
  }

  ngOnInit() {
    this.getMessageList();
    // reset login status
    this.AuthService.logout();
    this.checkError();

  }

  isLoggedIn: boolean = false;
  onSubmit(values: any) {
    this.checkError();
    if (this.form.valid) {
      this.isLoggedIn = true;
      let tt = this;
      var result = this.AuthService.login(values.email, values.password).then(response =>  {
        const user = this.jwtHelper.decodeToken(localStorage.getItem('id_token'));
        if(this.form.get('remember').value) {
          this._cookieService.put('username',values.email);
          this._cookieService.put('password',values.password);
          this._cookieService.put('remember',values.remember);
        } else {
          this._cookieService.removeAll();
        }
        this.router.navigate(['/home']).then(response=>{
          this.isLoggedIn = false;
        });
      })
      .catch(response =>  {
        this.handleError(response);
    });
    } else {
      this.hasError = true;
      this.isLoggedIn = false;
    }

  }

  private handleError(e: any) {
    this.error = e;
    this.isLoggedIn = false;
    console.log('errorhandle', this.error);
    let detail = e.detail
    if(e.status == '401') {
      this.hasError = true;
    } else if(detail && detail == 'Signature has expired.') {
      this.hasError = true;
    }
  }

  showHidePassword(value){
    if (this.ShowPassword.nativeElement.type = 'password') {
      this.show_password = true;
      this.hide_password = false;
    } else {
      this.show_password = false;
      this.hide_password = true;
    }

    if (value === 'show') {
      this.show_password = false;
      this.hide_password = true;
      this.ShowPassword.nativeElement.type = 'text';
    } else {
      this.show_password = true;
      this.hide_password = false;
      this.ShowPassword.nativeElement.type = 'password';
    }
  }

  checkError() {
    if((this.form.get('email').touched || this.form.get('password').touched) && (this.form.get('email').hasError('pattern') || this.form.get('password').hasError('pattern'))){
      this.hasError = true;
      this.isLoggedIn = false;
    }
  }

  sendEmail() {
    this.checkError();
    if(this.userEmail != '') {
      this.isEmailRequired = false;
      if( this.resetEmail.valid ) {
        var url = this.url + '';
        this.AuthService.sendResetPwdMail( this.userEmail ).then( response =>  {
          //this.successMsg = 'Mail Send Successfully';
          //jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
          if(response.iSsend) {
            this.isMailSend = true;
            this.emailSendError = false;
          } else {
            this.isMailSend = false;
            this.emailSendError = true;
          }
        })
        .catch(e => {
          console.log(e)
          this.error = e;
        });
      }
    } else {
      this.isEmailRequired = true;
    }
  }

  onTextChange(event) {
    if( this.resetEmail.errors && this.resetEmail.errors.pattern ) {
      this.isInvalidMailId = true;
    }
    if( event.target.value == '') {
      this.emailSendError = false;
      this.isInvalidMailId = false;
    }
    if(this.resetEmail.touched && this.resetEmail.errors && this.resetEmail.errors.required) {
      this.isEmailRequired = true;
    }
  }

  onKeyUp(event) {
    this.emailSendError = false;
  }

  closeBtn() {
    //console.log('this.resetEmail', this.resetEmail.control.value);
    this.isMailSend = false;
    this.emailSendError = false;
    this.isInvalidMailId = false;
    this.isEmailRequired = false;
    //this.resetEmail.control.setValue('');
    this.userEmail = '';
  }

  onKeyPress() {
    this.hasError = false;
  }

  getMessageList() {
    this.AuthService.getMessageList().then(response =>  {
      response.forEach(message => {
        if (message.onOrOff){
          this.messageList.push(message);
        }
      });
      jQuery('.carousel').carousel({
        interval: 2000
      })
      }).catch(response =>  {
        this.handleError(response);
      });
  }
}
