import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormBuilder } from '@angular/forms';
import { IMyOptions } from 'mydatepicker';
import { RefuellingHistoryService } from 'app/admin/refuellinghistory/service/refuellinghistory.service';
@Component({
    selector: 'refuellinghistory-list',
    encapsulation: ViewEncapsulation.None,
    template: require('./refuellinghistory-list.html')
})
export class RefuellingHistoryListComponent implements OnInit {
    refuellinghistory: any = [];
    API_URL_Export: string;
    public columns: Array<any> = [

        { title: 'ID', name: 'Id', sort: true },
        { title: 'Vehicle Number', name: 'VehicleNumber', sort: true },
        { title: 'Date of Previous Issue', name: 'Date', sort: true },
        { title: 'Volume of Previous Issue', name: 'volumeissue', sort: true },
        { title: 'Section', name: 'Section', sort: true, filter: true  },
    ];
    public config:any = {
        paging: true,
        sorting: {columns: this.columns},
        filtering: {filterString: ''},
        className: ['table-bordered']
      };
    params: URLSearchParams = new URLSearchParams();

    public page : number = 1;
    public length : number = 0;
    public next : string = '';
    public itemsPerPage : number = 10;
    public start : number = 1;
    public maxSize : number = 5;
    public numPages : number = 2;
    selectedAllVolFilter : any;
    selectedAllSectionFilter : any;
    filterArray : any = [];
    volIssuedFiltersList : any;
    sectionFiltersList : any;
    checkedVol: string[] = [];

    public loading: boolean = false;
    private currentdate = new Date();
    public rows:Array<any> = [];
    private myDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        //disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}

      };
    private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    fromDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    toDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    constructor(private router: Router,
        private fb: FormBuilder,
        private Service: RefuellingHistoryService,
        private configuration: Configuration) {
        this.itemsPerPage = configuration.itemsPerPage;
        this.params.set('limit', configuration.itemsPerPage.toString());
        this.params.set('SectionFilter', '');
      //  this.params.set('ordering', 'desc');
        this.rows = configuration.rows;
        if(new Date().getDate()< 10){
            this.toDate = '0'+new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
            this.fromDate = '0'+new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
         }
    }

    ngOnInit() {
        this.getRefuellingHistoryList(this.params);
        //this.totalfeilds = this.columns.length;
        this.API_URL_Export = this.configuration.ServerWithApiUrl + 'FuelIssuance/ExportRefuellingHistory?FromDate=' + this.fromDate + '&ToDate=' + this.toDate;
        this.getSectionFilter();

    }
    getRefuellingHistoryList(params: any) {
        this.loading = true;
        this.Service.getRefuellingHistoryList(params).then(response => {
            this.refuellinghistory =  [];
            this.volIssuedFiltersList = [];
            this.loading = false;
            response['result'].forEach( result => {
                if(result.vehicleId) {
                    this.refuellinghistory.push(result);
                    this.volIssuedFiltersList.push({id : result.quantityIssued});
                }
            });
            this.length = this.refuellinghistory.length;
            if (this.refuellinghistory.length > 0) {
                this.next = response['next'];
            } else {
                this.page = 1;
                this.getAllLists(this.params);
            }
        }).catch(r => {
            this.handleError(r);
        });
    }



    getAllLists(params: any) {
        //this.loading = true;
        this.Service.getRefuellingHistoryList(params).then(response => {
            this.refuellinghistory = response['results']
            this.length = this.refuellinghistory.length;
            this.next = response['next'];
            this.loading = false;

        }).catch(r => {
            this.handleError(r);
        });
    }

    public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
        let params = this.params;
        let start = (page.page - 1) * page.itemsPerPage;
        this.start = start + 1;
        params.set('limit', page.itemsPerPage);
        params.set('offset',start.toString());
        var sortParam = '';
        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
          params.set(this.config.filtering.name, this.config.filtering.filterString);
        }
        var sortParam = '';
        this.config.sorting.columns.forEach( function( col, key ) {
          if(col.sort){
            if(col.sort == 'desc') {
              sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
            } else if(col.sort == 'asc') {
              sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
            }
          }
        });
        if(sortParam != '') {
          params.set('ordering', sortParam);
        }
        this.getRefuellingHistoryList(this.params);
    }
    // Export to Excel List 

    onDateChanged(event: any, type: any) {
        if (type == 'FromDate') {
            this.fromDate = event.formatted;
        }
        if (type == 'ToDate') { }
        this.toDate = event.formatted;
    }

    export() {

        this.API_URL_Export = this.configuration.ServerWithApiUrl + 'FuelIssuance/ExportRefuellingHistory?FromDate=' + this.fromDate + '&ToDate=' + this.toDate;
    }

    sort(param, order) {
        this.params.set('ordering', param);
        if(order === 'desc'){
          this.params.set('ordering', '-'+param);
        }
        if(order === 'asc'){
          this.params.set('ordering', param);
        }
        this.getRefuellingHistoryList(this.params);
    }

    selectAll(item, event) {
        if(item === 'VolumeIssued'){
            this.filterArray = [];
            for (var i = 0; i < this.volIssuedFiltersList.length; i++) {
              this.volIssuedFiltersList[i].selected = this.selectedAllVolFilter;
              if(event.target.checked) {
                  this.filterArray.push(this.volIssuedFiltersList[i].id.toString());
              } else {
                  this.filterArray = [];
              }
            }
             this.params.set('VolumeIssued', this.filterArray.toString());
        }
        if(item === 'Section'){
            this.filterArray = [];
            for (var i = 0; i < this.sectionFiltersList.length; i++) {
              this.sectionFiltersList[i].selected = this.selectedAllSectionFilter;
              if(event.target.checked) {
                  this.filterArray.push(this.sectionFiltersList[i].sectionId.toString());
              } else {
                  this.filterArray = [];
              }
            }
             this.params.set('SectionFilter', this.filterArray.toString());
        }
    }

    checkIfAllSelected(option, event, item) {
        if( item === 'VolumeIssued') {
           this.selectedAllVolFilter = this.volIssuedFiltersList.every(function(item:any) {
               return item.selected == true;
             })
             var key = event.target.value.toString();
             var index = this.checkedVol.indexOf(key);
             if(event.target.checked) {
               this.checkedVol.push(event.target.value);
             } else {
               this.checkedVol.splice(index, 1);
             }
             this.params.set('VolumeIssued', this.checkedVol.toString());
        }
        if( item === 'Section') {
            this.selectedAllSectionFilter = this.sectionFiltersList.every(function(item:any) {
                return item.selected == true;
              })
              var key = event.target.value.toString();
              var index = this.checkedVol.indexOf(key);
              if(event.target.checked) {
                this.checkedVol.push(event.target.value);
              } else {
                this.checkedVol.splice(index, 1);
              }
              this.params.set('SectionFilter', this.checkedVol.toString());
         }
    }

    apply(item) {
        this.getRefuellingHistoryList(this.params);
        if(item === 'VolumeIssued'){
            setTimeout(function () {
                jQuery('#ViewVolIssueModal').modal('toggle');
            }.bind(this) , 0);
        }
        if(item === 'Section'){
            setTimeout(function () {
                jQuery('#ViewSectionModal').modal('toggle');
            }.bind(this) , 0);
        }
        jQuery('ViewVolIssueModal').modal('hide');
        jQuery('ViewSectionModal').modal('hide');
        jQuery('body').removeClass('modal-open');
        jQuery('.modal-backdrop').remove();
    }

    getSectionFilter() {
        this.Service.getSectionFilter().then(response =>  {
            this.sectionFiltersList = response;
            this.loading = false;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    private handleError(e: any) {
        let detail = e.detail
        /*console.log(detail)*/
        if (detail && detail == 'Signature has expired.') {
            this.router.navigate(['./']);
        }
    }
}