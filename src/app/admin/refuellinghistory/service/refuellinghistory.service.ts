import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class RefuellingHistoryService{
    public listUrl = '';
    public exportUrl='';
    public sectionFilterUrl='';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'FuelIssuance/RefuellingHistory';
        this.sectionFilterUrl = _configuration.ServerWithApiUrl+'FuelIssuance/GetSectionFilter';
    }

    // List API for Refuelling History
    getRefuellingHistoryList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getSectionFilter(): Promise<any> {
        return this.authHttp.get(this.sectionFilterUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }
    
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}