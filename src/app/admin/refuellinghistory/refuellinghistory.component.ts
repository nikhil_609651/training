import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'refuelling-history',
    template: `<router-outlet></router-outlet>`
})
export class RefuellingHistoryComponent {
	constructor(private router: Router) {
	}
}
