import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'uom',
    template: `<router-outlet></router-outlet>`
})
export class UomComponent {
	constructor(private router: Router) {
	}
}
