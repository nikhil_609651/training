import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class UomFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public classdropdownUrl = '';
    public classFilterUrl = '';
    public geturl = '';
    public downloadUrl = '';
    public deleteattachmentUrl = '';
    public attachmenturl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/UOM/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveUOM';
        //this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateUOM';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteUOMByID?id=';
        this.classdropdownUrl = _configuration.ServerWithApiUrl+'Masters/ClassUnit';
        this.classFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetUomClassFilter';
        this.geturl = _configuration.ServerWithApiUrl+'Masters/GetUOMByID?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadUOMFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteUOMAttachments?Id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllUOMAttachments?id=';
    }
    
    // List API for UOM
    getUomList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getClassList(): Promise<any> {
        return this.authHttp.get(this.classdropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getClassFilterList(): Promise<any> {
        return this.authHttp.get(this.classFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUOMList(): Promise<any> {
        return this.authHttp.get(this.listUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getalluomattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //Save API for UOM
    Save(uom: any): Promise<any>  {
        if(uom.id == 0){
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, uom)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
        } else{
            let headers = new Headers({});
            let options = new RequestOptions({ headers });
            return this.authHttp
                .post(this.saveUrl, uom)
                .toPromise()
                .then(response => response.json().data)
                .catch(this.handleError);  
        }
    }

    private post(uom: any): Promise<any> {
        const formData = new FormData();
        formData.append('id', uom.id);
        formData.append('name', uom.name);
        formData.append('abbrevation', uom.abbrevation);
        formData.append('convertionFactor', uom.convertionFactor);
        formData.append('classUnitId', uom.classUnitId);
        formData.append('classUnit', "");
        formData.append('uomId', uom.uomId);
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deleteuom(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getuom(id: string) {
        return this.authHttp.get(this.geturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }
    
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
