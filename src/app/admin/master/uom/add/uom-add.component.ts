import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UomFormService } from 'app/admin/master/uom/service/uom.service';

@Component({
  selector: 'add',
  template: require('./uom-add.html')
})
export class UomAddComponent implements OnInit{
 
  classList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private uomService: UomFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';

  }
  getSiteList(){
    
        //this.loading = true;
        this.uomService.getClassList().then(r => {
          this.classList = r;
          console.log( 'class', this.classList)
          console.log(r)
          // this.loading = false;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  onSave(uom: any) {
    if(uom.uomId=="")
    {
      uom.uomId='0';
    }
    let files = uom.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('id', '0');
    formData.append('name', uom.name);
    formData.append('abbrevation', uom.abbrevation);
    formData.append('convertionFactor', uom.convertionFactor);
    formData.append('classUnitId', uom.classUnitId);
    formData.append('classUnit', "");
    formData.append('uomId', uom.uomId);
    formData.append('isDeleted',"");
    console.log('values',uom);
    this.uomService.Save(formData).then(r =>  {
      console.log('r',r)
      this.success = 'UOM Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/uom']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
