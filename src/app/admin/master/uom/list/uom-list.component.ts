import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { UomFormService } from 'app/admin/master/uom/service/uom.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'uom-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./uom-list.html')
})
export class UomListComponent implements OnInit{
  uomName: any;
  attachment_deleted_id: any;
  selectedAll: any;
  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  uomId: any;
  classGroups: any;
  public deleteattachmentmessage='';
  public uom: any=[];
  error: any;
  checked: string[] = [];
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'UOM Name', name: 'UOMName', sort: true },
    { title: 'Abbreviation', name: 'abbreviation', sort: true },
    { title: 'Class', name: 'ClassName', sort: true , filter:true},
    // { title: 'Attachments', name: 'attachments', sort: false },
    { title: 'Actions', name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public uom_deleted_id = '';
  public deletemessage='';
  viewUomForm: FormGroup;
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router, private fb: FormBuilder,private _sharedService : SharedService,
    private uomService: UomFormService, private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('ClassUnitFilter', '');
      this.rows = configuration.rows;
      this.viewUomForm = fb.group({
                  'bUomId': [''],
                  'bUom': [''],
                  'bAbbreviation': [''],
                  'bClass': [''],
                  'bConversionValue': [''],
                  'bConversionUnit': [''],
              });
      var vForm = this.viewUomForm;
  }

  ngOnInit() {
    this.getuomList(this.params);
    this.getclass();
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getclass(){
    this.uomService.getClassFilterList().then(r =>  {
        this.classGroups = r;
        this.loading = false;
      });
  }

   // Filteration
   selectAll(event) {
    this.checkArr = [];
    for (var i = 0; i < this.classGroups.length; i++) {
      this.classGroups[i].selected = this.selectedAll;
      if(event.target.checked){
          this.checkArr.push(this.classGroups[i].id.toString());
      } else {
          this.checkArr = [];
      }
    }
     this.params.set('ClassUnitFilter', this.checkArr.toString());
  }

  checkArr = [];
  checkIfAllSelected(option, event,item) {
    this.selectedAll = this.classGroups.every(function(item:any) {
      return item.selected == true;
    })
    var key = event.target.value.toString();
    var index = this.checkArr.indexOf(key);
    if(event.target.checked) {
      this.checkArr.push(event.target.value);
    } else {
      this.checkArr.splice(index,1);
    }
    this.params.set('ClassUnitFilter', this.checkArr.toString());
  }

  // For Applying Filtering to List
  apply() {
    this.getuomList(this.params);
    setTimeout(function () {
      jQuery('#ViewClassModal').modal('toggle');
    }.bind(this) , 0);
    jQuery('ViewClassModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  // Filteration
  getuomList(params: any) {
    this.loading = true;
    this.uomService.getUomList(params).then(response => {
      this.uom = response['result']
      if(this.uom.length>0) {
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else{
        this.page=1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  view(data) {
    if (data.id != 0) {
      this.uomName=data.uomName;
      this.viewUomForm.controls['bUom'].setValue(data.name);
      this.viewUomForm.controls['bAbbreviation'].setValue(data.abbrevation);
      this.viewUomForm.controls['bClass'].setValue(data.classUnit.name);
      this.viewUomForm.controls['bConversionValue'].setValue(data.convertionFactor);
      this.viewUomForm.controls['bConversionUnit'].setValue(this.uomName);
    }
  }

  viewclose(){
    setTimeout(function () {
        jQuery('#View').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }

  getAllLists(params: any) {
    this.loading = true;
    this.uomService.getUomList(params).then(response => {
      this.uom = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getuomList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getuomList(this.params);
  }

  adduom() {
    this.router.navigate(['./home/master/uom/add']);
  }

  edituom(id) {

    this.router.navigate(['./home/master/uom/edit/', id]);
  }
  deletemessageclear(){
    this.deletemessage = '';
  }
  deleteuom(id){



    this.uom_deleted_id = id;
  }

  deleteuomconfirm(item){

    //this.uom_deleted_id= item.id;
          this.uomService.deleteuom(this.uom_deleted_id).then(r =>  {
            this.getuomList(this.params);
            this.deletemessage = 'UOM Deleted Successfully';
          }).catch(r =>  {
            this.handleError(r);
            this.deletemessage = r.name[0];
          });
      }
  deleteattachment(id){
   this.attachment_deleted_id = id;
 }
 deleteattachmentmessageclear(){
   this.deleteattachmentmessage = '';
 }

 deleteattachmentconfirm(id){


  this.uomService.deleteattachment(id).then(r =>  {
    this.attachment(this.uomId);
       this.deleteattachmentmessage = "Attachment Deleted Successfully";

     }).catch(r =>  {
        // console.log();
       this.handleError(r);
       this.deleteattachmentmessage = r.name[0];
     })
 }


  attachment(id){
    this.uomId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadUOMFiles?FileId=';
this.uomService.getalluomattachments(id).then(response => {
  this.attachments=response;
  if(this.attachments.length > 0){
     var LongArray = [];
     for(var i = 0; i < this.attachments.length; i++) {
       let ext=this.attachments[i].location;
       var Obj = {
        id :this.attachments[i].id,
        referenceId:this.attachments[i].referenceId,
        shortFileName : this.attachments[i].shortFileName,
        fileName:this.attachments[i].fileName,
        createdDate: this.attachments[i].createdDate,
         ext :  ext.substr(ext.lastIndexOf('.') + 1),

       };
       LongArray.push(Obj);
       this.attachmentGroup=LongArray;
      }
  }
  else{
    this.getuomList(this.params);
    this.attachmentmessage='No Attachments Found'
    // setTimeout(function () {
    //   jQuery('#ViewAttachmentModal').modal('toggle');
    // }.bind(this), 1000);
  }
  console.log('Attachments', LongArray);
  return this.attachmentGroup;

});

  }


  Sort(param,order){
    //this.params.set('ordering', sortParam);

    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getuomList(this.params);



      }
  private handleError(e: any) {
      let detail = e.detail
      /*console.log(detail)*/
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
