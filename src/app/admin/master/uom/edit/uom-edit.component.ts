import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UomFormService } from 'app/admin/master/uom/service/uom.service';

@Component({
  selector: 'uom-edit',
  template: require('./uom-edit.html')
})
export class UomEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public uom: any;
  public page = 'edit';

public uomdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private uomService: UomFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.uom = this.uomService.getuom(this.id);
                console.log('DATA',this.uom)
      });
 
	}

  onSave(uom: any) {
    let files = uom.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('id', this.id);
    formData.append('name', uom.name);
    formData.append('abbrevation', uom.abbrevation);
    formData.append('convertionFactor', uom.convertionFactor);
    formData.append('classUnitId', uom.classUnitId);
    formData.append('classUnit', "");
    formData.append('uomId', uom.uomId);
    formData.append('isDeleted',"");
    this.uomService.Save(formData).then(r =>  {
      this.success = 'UOM Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/uom']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
