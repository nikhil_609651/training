import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { UomFormService } from 'app/admin/master/uom/service/uom.service';

@Component({
  selector: 'uom-form',
  template: require('./uom-form.html')
})
export class UomFormComponent {
  savebutton: boolean = true;

  classReq: string;
  nameReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() uom;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  uomForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public classList: Array<string>;
  public UOMList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public uom_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public uomGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private uomService: UomFormService, private router: Router,
    private route: ActivatedRoute) {
    //this.getbusinessunits();

    this.getClassList();
    this.getUOMList();

    this.uomForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'id': [''],
      'abbrevation': [''],
      'classUnitId': ['', Validators.compose([Validators.required])],
      'uomId': [''],
      'convertionFactor': [''],
      'FileType':[''],
    });
    var cForm = this.uomForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.uom_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
  }


  onselected(){
    this.options = this.optionss;
    //console.log(this.options)
  }

  getClassList(){

        //this.loading = true;
        this.uomService.getClassList().then(r => {
          this.classList = r;
          console.log( 'class', this.classList)
          console.log(r)
          // this.loading = false;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  getUOMList(){

    //this.loading = true;
    this.uomService.getUOMList().then(r => {
      this.UOMList = r.result;
      console.log( 'UOM', this.UOMList)
      console.log(r)
      // this.loading = false;
  })
      .catch(r => {
          this.handleError(r);
      })

}
getuom(params){
  this.uomService.getUomList(params).then(r =>  {
      this.uomGroups = r.result;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('uom_id',this.uom_id);
  this.params.set('list','list');
  this.uomService.getUomList(params).then(r =>  {
    this.uomGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.uomGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.uomGroups.length; i++) {
      var Obj = {
        label : this.uomGroups[i].name,
        value :  +this.uomGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.uomGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    console.log('abc',this.uomForm.controls);
  //this.options=this.optionss;
    this.loading = this.page == 'add' ? false : true;
    if (change.uom && change.uom.currentValue) {
   //this.onselectednew(change.businessunit.currentValue.city.id);
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.uomForm.controls['id'].setValue(change.uom.currentValue.id);
      this.uomForm.controls['name'].setValue(change.uom.currentValue.name);
      this.uomForm.controls['abbrevation'].setValue(change.uom.currentValue.abbrevation);
      this.uomForm.controls['convertionFactor'].setValue(change.uom.currentValue.convertionFactor);
      this.uomForm.controls['classUnitId'].setValue(change.uom.currentValue.classUnitId);
      this.uomForm.controls['uomId'].setValue(change.uom.currentValue.uomId);
      this.uomForm.controls['FileType'].setValue(this.filetype);

      //console.log(change.businessunit.currentValue.manager[0].id);
     // var result = change.businessunit.currentValue.manager.map(a => a.id);
      //console.log(result);

      console.log(this.uomForm.controls);

    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="UOM Name is required";
    this.classReq="You must select a class";
    this.clearerror();
    this.formSubmited = true;
    if(this.uomForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);

    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
   // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  clearerror(){
   // alert('hai');
    this.error="";
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.uomForm.controls['name'].hasError('required') || (this.uomForm.controls['name'].touched))
    {
      this.nameReq="";
    }
    if(this.uomForm.controls['classUnitId'].hasError('required') || (this.uomForm.controls['classUnitId'].touched))
    {
      this.classReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}