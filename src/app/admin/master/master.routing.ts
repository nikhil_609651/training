import { Routes, RouterModule }  from '@angular/router';
import { BusinessUnitComponent } from './business_unit/businessunit.component';
import { BusinessUnitListComponent } from './business_unit/list/businessunit-list.component';
import { BusinessUnitAddComponent } from './business_unit/add/businessunit-add.component';
import { BusinessUnitEditComponent } from './business_unit/edit/businessunit-edit.component';
import { DepartmentComponent } from './department/department.component';
import { DepartmentListComponent } from './department/list/department-list.component';
import { DepartmentAddComponent } from './department/add/department-add.component';
import { DepartmentEditComponent } from './department/edit/department-edit.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomersListComponent } from './customers/list/customers-list.component';
import { CustomersAddComponent } from './customers/add/customers-add.component';
import { CustomersEditComponent } from './customers/edit/customers-edit.component';
import { DischargeVehicleComponent } from './discharge_vehicle/dischargevehicle.component';
import { DischargeVehicleListComponent } from './discharge_vehicle/list/dischargevehicle-list.component';
import { DischargeVehicleAddComponent } from './discharge_vehicle/add/dischargevehicle-add.component';
import { DischargeVehicleEditComponent } from './discharge_vehicle/edit/dischargevehicle-edit.component';
import { DriverComponent } from './driver/driver.component';
import { DriverListComponent } from './driver/list/driver-list.component';
import { DriverAddComponent } from './driver/add/driver-add.component';
import { DriverEditComponent } from './driver/edit/driver-edit.component';
import { AirCraftComponent } from './equipmentmaster/aircraft/aircraft.component';
import { AircraftListComponent } from './equipmentmaster/aircraft/list/aircraft-list.component';
import { AircraftAddComponent } from './equipmentmaster/aircraft/add/aircraft-add.component';
import { AircraftEditComponent } from './equipmentmaster/aircraft/edit/aircraft-edit.component';
import { GeneratorComponent } from './equipmentmaster/generator/generator.component';
import { GeneratorListComponent } from './equipmentmaster/generator/list/generator-list.component';
import { GeneratorAddComponent } from './equipmentmaster/generator/add/generator-add.component';
import { GeneratorEditComponent } from './equipmentmaster/generator/edit/generator-edit.component';
import { VehicleComponent } from './equipmentmaster/vehicle/vehicle.component';
import { VehicleListComponent } from './equipmentmaster/vehicle/list/vehicle-list.component';
import { VehicleAddComponent } from './equipmentmaster/vehicle/add/vehicle-add.component';
import { VehicleEditComponent } from './equipmentmaster/vehicle/edit/vehicle-edit.component';
import { EquipmentComponent } from './equipmentmaster/equipment/equipment.component';
import { EquipmentListComponent } from './equipmentmaster/equipment/list/equipment-list.component';
import { EquipmentAddComponent } from './equipmentmaster/equipment/add/equipment-add.component';
import { EquipmentEditComponent } from './equipmentmaster/equipment/edit/equipment-edit.component';
import { AirCraftTypeComponent } from './equipmentmaster/aircrafttype/aircrafttype.component';
import { AirCraftTypeListComponent } from './equipmentmaster/aircrafttype/list/aircrafttype-list.component';
import { AirCraftTypeAddComponent } from './equipmentmaster/aircrafttype/add/aircrafttype-add.component';
import { AirCraftTypeEditComponent } from './equipmentmaster/aircrafttype/edit/aircrafttype-edit.component';
import { VehicleTypeComponent } from './equipmentmaster/vehicletype/vehicletype.component';
import { VehicleTypeListComponent } from './equipmentmaster/vehicletype/list/vehicletype-list.component';
import { VehicleTypeAddComponent } from './equipmentmaster/vehicletype/add/vehicletype-add.component';
import { VehicleTypeEditComponent } from './equipmentmaster/vehicletype/edit/vehicletype-edit.component';
import { EquipmentTypeComponent } from './equipmentmaster/equipmenttype/equipmenttype.component';
import { EquipmentTypeListComponent } from './equipmentmaster/equipmenttype/list/equipmenttype-list.component';
import { EquipmentTypeAddComponent } from './equipmentmaster/equipmenttype/add/equipmenttype-add.component';
import { EquipmentTypeEditComponent } from './equipmentmaster/equipmenttype/edit/equipmenttype-edit.component';
import { GeneratorMakeModelComponent } from './equipmentmaster/generatormakemodel/generatormakemodel.component';
import { GeneratorMakeModelListComponent } from './equipmentmaster/generatormakemodel/list/generatormakemodel-list.component';
import { GeneratorMakeModelAddComponent } from './equipmentmaster/generatormakemodel/add/generatormakemodel-add.component';
import { GeneratorMakeModelEditComponent } from './equipmentmaster/generatormakemodel/edit/generatormakemodel-edit.component';
import { VehicleMakeModelComponent } from './equipmentmaster/vehiclemakemodel/vehiclemakemodel.component';
import { VehicleMakeModelListComponent } from './equipmentmaster/vehiclemakemodel/list/vehiclemakemodel-list.component';
import { VehicleMakeModelAddComponent } from './equipmentmaster/vehiclemakemodel/add/vehiclemakemodel-add.component';
import { VehicleMakeModelEditComponent } from './equipmentmaster/vehiclemakemodel/edit/vehiclemakemodel-edit.component';
import { IssuerComponent } from './issuer/issuer.component';
import { IssuerListComponent } from './issuer/list/issuer-list.component';
import { IssuerAddComponent } from './issuer/add/issuer-add.component';
import { IssuerEditComponent } from './issuer/edit/issuer-edit.component';
import { OwnershipComponent } from './ownership/ownership.component';
import { OwnershipListComponent } from './ownership/list/ownership-list.component';
import { OwnershipAddComponent } from './ownership/add/ownership-add.component';
import { OwnershipEditComponent } from './ownership/edit/ownership-edit.component';
import { PolComponent } from './pol/pol-component';
import { PolListComponent } from './pol/list/pol-list.component';
import { PolAddComponent } from './pol/add/pol-add.component';
import { PolEditComponent } from './pol/edit/pol-edit.component';
import { ReceiverComponent } from './receiver/receiver.component';
import { ReceiverListComponent } from './receiver/list/receiver-list.component';
import { ReceiverAddComponent } from './receiver/add/receiver-add.component';
import { ReceiverEditComponent } from './receiver/edit/receiver-edit.component';
import { SectionMasterComponent } from './sectionmaster/section.component';
import { SectionListComponent } from './sectionmaster/list/section-list.component';
import { SectionAddComponent } from './sectionmaster/add/section-add.component';
import { SectionEditComponent } from './sectionmaster/edit/section-edit.component';
import { SiteComponent } from './site/site.component';
import { SiteListComponent } from './site/list/site-list.component';
import { SiteAddComponent } from './site/add/site-add.component';
import { SiteEditComponent } from './site/edit/site-edit.component';
import { StoragetankComponent } from './storagetank/storagetank.component';
import { StoragetankListComponent } from './storagetank/list/storagetank-list.component';
import { StoragetankAddComponent } from './storagetank/add/storagetank-add.component';
import { StoragetankEditComponent } from './storagetank/edit/storagetank-edit.component';
import { SubUnitComponent } from './subunit/subunit.component';
import { SubUnitListComponent } from './subunit/list/subunit-list.component';
import { SubUnitAddComponent } from './subunit/add/subunit-add.component';
import { SubUnitEditComponent } from './subunit/edit/subunit-edit.component';
import { TransportCompanyComponent } from './transportcompany/transportcompany.component';
import { TransportCompanyListComponent } from './transportcompany/list/transportcompany-list.component';
import { TransportCompanyAddComponent } from './transportcompany/add/transportcompany-add.component';
import { TransportCompanyEditComponent } from './transportcompany/edit/transportcompany-edit.component';
import { TruckComponent } from './truck/truck.component';
import { TruckListComponent } from './truck/list/truck-list.component';
import { TruckAddComponent } from './truck/add/truck-add.component';
import { TruckEditComponent } from './truck/edit/truck-edit.component';
import { UomComponent } from './uom/uom.component';
import { UomListComponent } from './uom/list/uom-list.component';
import { UomAddComponent } from './uom/add/uom-add.component';
import { UomEditComponent } from './uom/edit/uom-edit.component';

const routes: Routes = [
      {
       path: '',
       //component: TransactionComponent,
       children: [
        {
          path: 'business-unit',
          component: BusinessUnitComponent,
          children: [
            { path: '', component: BusinessUnitListComponent },
            { path: 'list', component: BusinessUnitListComponent },
            { path: 'add', component: BusinessUnitAddComponent },
            { path: 'edit/:id', component: BusinessUnitEditComponent },
          ]
        },
        {
          path: 'department',
          component: DepartmentComponent,
          children: [
            { path: '', component: DepartmentListComponent },
            { path: 'list', component: DepartmentListComponent },
            { path: 'add', component: DepartmentAddComponent },
            { path: 'edit/:id', component: DepartmentEditComponent },
          ]
        },  
        {
          path: 'customer',
          component: CustomersComponent,
          children: [
            { path: '', component: CustomersListComponent },
            { path: 'list', component: CustomersListComponent },
            { path: 'add', component: CustomersAddComponent },
            { path: 'edit/:id', component: CustomersEditComponent },
          ]
        },
        {
          path: 'discharge-vehicle',
          component: DischargeVehicleComponent,
          children: [
            { path: '', component: DischargeVehicleListComponent },
            { path: 'list', component: DischargeVehicleListComponent },
            { path: 'add', component: DischargeVehicleAddComponent },
            { path: 'edit/:id', component: DischargeVehicleEditComponent },
          ]
        },
        {
          path: 'driver',
          component: DriverComponent,
          children: [
            { path: '', component: DriverListComponent },
            { path: 'list', component: DriverListComponent },
            { path: 'add', component: DriverAddComponent },
            { path: 'edit/:id', component: DriverEditComponent },
          ]
        },
        {
          path: 'aircraft',
          component: AirCraftComponent,
          children: [
            { path: '', component: AircraftListComponent },
            { path: 'list', component: AircraftListComponent },
            { path: 'add', component: AircraftAddComponent },
            { path: 'edit/:id', component: AircraftEditComponent },
          ]
        },
        {
          path: 'generator',
          component: GeneratorComponent,
          children: [
            { path: '', component: GeneratorListComponent },
            { path: 'list', component: GeneratorListComponent },
            { path: 'add', component: GeneratorAddComponent },
            { path: 'edit/:id', component: GeneratorEditComponent },
          ]
        },  
        {
          path: 'vehicle',
          component: VehicleComponent,
          children: [
            { path: '', component: VehicleListComponent },
            { path: 'list', component: VehicleListComponent },
            { path: 'add', component: VehicleAddComponent },
            { path: 'edit/:id', component: VehicleEditComponent },
          ]
        },
        {
          path: 'equipment',
          component: EquipmentComponent,
          children: [
            { path: '', component: EquipmentListComponent },
            { path: 'list', component: EquipmentListComponent },
            { path: 'add', component: EquipmentAddComponent },
            { path: 'edit/:id', component: EquipmentEditComponent },
          ]
        },
        {
          path: 'aircraft-type',
          component: AirCraftTypeComponent,
          children: [
            { path: '', component: AirCraftTypeListComponent },
            { path: 'list', component: AirCraftTypeListComponent },
            { path: 'add', component: AirCraftTypeAddComponent },
            { path: 'edit/:id', component: AirCraftTypeEditComponent },
          ]
        },
        {
          path: 'vehicle-type',
          component: VehicleTypeComponent,
          children: [
            { path: '', component: VehicleTypeListComponent },
            { path: 'list', component: VehicleTypeListComponent },
            { path: 'add', component: VehicleTypeAddComponent },
            { path: 'edit/:id', component: VehicleTypeEditComponent },
          ]
        },
        {
          path: 'equipment-type',
          component: EquipmentTypeComponent,
          children: [
            { path: '', component: EquipmentTypeListComponent },
            { path: 'list', component: EquipmentTypeListComponent },
            { path: 'add', component: EquipmentTypeAddComponent },
            { path: 'edit/:id', component: EquipmentTypeEditComponent },
          ]
        },
        {
          path: 'generator-make-model',
          component: GeneratorMakeModelComponent,
          children: [
            { path: '', component: GeneratorMakeModelListComponent },
            { path: 'list', component: GeneratorMakeModelListComponent },
            { path: 'add', component: GeneratorMakeModelAddComponent },
            { path: 'edit/:id', component: GeneratorMakeModelEditComponent },
          ]
        },
        {
          path: 'vehicle-make-model',
          component: VehicleMakeModelComponent,
          children: [
            { path: '', component: VehicleMakeModelListComponent },
            { path: 'list', component: VehicleMakeModelListComponent },
            { path: 'add', component: VehicleMakeModelAddComponent },
            { path: 'edit/:id', component: VehicleMakeModelEditComponent },
          ]
        },
        {
          path: 'issuer',
          component: IssuerComponent,
          children: [
            { path: '', component: IssuerListComponent },
            { path: 'list', component: IssuerListComponent },
            { path: 'add', component: IssuerAddComponent },
            { path: 'edit/:id', component: IssuerEditComponent },
          ]
        },
        {
          path: 'ownership-info',
          component: OwnershipComponent,
          children: [
            { path: '', component: OwnershipListComponent },
            { path: 'list', component: OwnershipListComponent },
            { path: 'add', component: OwnershipAddComponent },
            { path: 'edit/:id', component: OwnershipEditComponent },
            ]
        },
        {
          path: 'pol',
          component: PolComponent,
          children: [
            { path: '', component: PolListComponent },
            { path: 'list', component: PolListComponent },
            { path: 'add', component: PolAddComponent },
            { path: 'edit/:id', component: PolEditComponent },
          ]
        },
        {
          path: 'receiver',
          component: ReceiverComponent,
          children: [
            { path: '', component: ReceiverListComponent },
            { path: 'list', component: ReceiverListComponent },
            { path: 'add', component: ReceiverAddComponent },
            { path: 'edit/:id', component: ReceiverEditComponent },
          ]
        },
        {
          path: 'section',
          component: SectionMasterComponent,
          children: [
            { path: '', component: SectionListComponent },
            { path: 'list', component: SectionListComponent },
            { path: 'add', component: SectionAddComponent },
            { path: 'edit/:id', component: SectionEditComponent },
          ]
        },
        {
          path: 'site',
          component: SiteComponent,
          children: [
            { path: '', component: SiteListComponent },
            { path: 'list', component: SiteListComponent },
            { path: 'add', component: SiteAddComponent },
            { path: 'edit/:id', component: SiteEditComponent },
          ]
        },
        {
          path: 'storage-tank',
          component: StoragetankComponent,
          children: [
            { path: '', component: StoragetankListComponent },
            { path: 'list', component: StoragetankListComponent },
            { path: 'add', component: StoragetankAddComponent },
            { path: 'edit/:id', component: StoragetankEditComponent },
          ]
        },
        {
          path: 'sub-unit',
          component: SubUnitComponent,
          children: [
            { path: '', component: SubUnitListComponent },
            { path: 'list', component: SubUnitListComponent },
            { path: 'add', component: SubUnitAddComponent },
            { path: 'edit/:id', component: SubUnitEditComponent },
          ]
        },
        {
          path: 'transport-company',
          component: TransportCompanyComponent,
          children: [
            { path: '', component: TransportCompanyListComponent },
            { path: 'list', component: TransportCompanyListComponent },
            { path: 'add', component: TransportCompanyAddComponent },
            { path: 'edit/:id', component: TransportCompanyEditComponent },
          ]
        },
        {
          path: 'truck',
          component: TruckComponent,
          children: [
            { path: '', component: TruckListComponent },
            { path: 'list', component: TruckListComponent },
            { path: 'add', component: TruckAddComponent },
            { path: 'edit/:id', component: TruckEditComponent },
          ]
        },
        {
          path: 'uom',
          component: UomComponent,
          children: [
            { path: '', component: UomListComponent },
            { path: 'list', component: UomListComponent },
            { path: 'add', component: UomAddComponent },
            { path: 'edit/:id', component: UomEditComponent },
          ]
        },
       ]
      }
];

export const routing = RouterModule.forChild(routes);
