import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'add',
  template: require('./issuer-add.html')
})
export class IssuerAddComponent implements OnInit{
  siteList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private issuerService: IssuerFormService) {
  }
  ngOnInit() {
    this.page = 'add';

  }
  // getSiteList(){
  //       this.issuerService.getSiteList().then(r => {
  //         this.siteList = r;
  //     })
  //         .catch(r => {
  //             this.handleError(r);
  //         });

  // }
  onSave(issuer: any) {
    let files = issuer.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('issuerId','0');
    formData.append('firstName', issuer.firstName);
    formData.append('lastName', issuer.lastName);
    formData.append('unidNo', issuer.unidNo);
    formData.append('siteId', issuer.siteId);
    formData.append('site', "");
    formData.append('isDeleted',"");
    this.issuerService.Save(formData).then(r =>  {
      this.success = 'Issuer Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/issuer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
