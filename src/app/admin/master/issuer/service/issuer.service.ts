import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class IssuerFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public sitedropdownUrl = '';
    public geturl = '';
    public sitesUrl = '';
    public sitesFilterUrl = '';
    public attachmenturl ="";
    public downloadUrl = "";
    public deleteattachmentUrl = "";

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Issuer/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveIssuer';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateIssuer';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteIssuerByID?id=';
        this.sitedropdownUrl = _configuration.ServerWithApiUrl+'UserManagement/GetSitebyUserId?userid=';
        this.geturl = _configuration.ServerWithApiUrl+'Masters/GetIssuerByID?id=';
        this.sitesUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.sitesFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetIssuerSiteFilter/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllIssuerAttachments?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadIssuerFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteIssuerAttachments?Id=';
    }

    // List API for Issuer
    getIssuerList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getsites(): Promise<any> {
        return this.authHttp.get(this.sitesUrl+'?UserId='+localStorage.getItem('user_nameId'))
                .toPromise()
                .then(response => response.json())
                .catch(this.handleError);
    }

    getFilterSites(userId): Promise<any> {
        return this.authHttp.get(this.sitesFilterUrl+ '?UserId=' + userId)
                .toPromise()
                .then(response => response.json())
                .catch(this.handleError);
    }

    // Save API for Issuer
    Save(issuer: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, issuer)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(issuer: any): Promise<any> {
        const formData = new FormData();
        formData.append('issuerId', issuer.issuerId);
        formData.append('firstName', issuer.firstName);
        formData.append('lastName', issuer.lastName);
        formData.append('unidNo', issuer.unidNo);
        formData.append('siteId', issuer.siteId);
        formData.append('site', "");
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deleteissuer(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getallissuerattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getissuer(id: string) {
        return this.authHttp.get(this.geturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteList(userid: any): Promise<any> {
        return this.authHttp.get(this.sitedropdownUrl + userid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
