import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'issuer-form',
  template: require('./issuer-form.html')
})
export class IssuerFormComponent {
  userId: string;
  savebutton: boolean = true;

  unidReq: string;
  lnameReq: string;
  siteReq: string;
  nameReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @Input() issuer;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  issuerForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public siteList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  // tslint:disable-next-line:variable-name
  public issuer_id = '';
  public issuerGroups =[];
   params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
    private issuerService: IssuerFormService, private router: Router,
    private route: ActivatedRoute) {
      this.userId=localStorage.getItem('user_nameId');
      console.log(' this.userId', this.userId)
    this.getSiteList();
    this.issuerForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'issuerId': [''],
      'lastName': ['', ],
      'siteId': ['', Validators.compose([Validators.required])],
      'unidNo': ['',Validators.compose([Validators.required])],
      'FileType':[''],
    });
    var cForm = this.issuerForm;
  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.issuer_id = params['id'];
      });


  }

// for getting SiteList
  getSiteList(){
        this.issuerService.getSiteList(this.userId).then(r => {
console.log('r',r)
          this.siteList = r;
          console.log(' this.siteList', this.siteList)
      })
          .catch(r => {
              this.handleError(r);
          });

  }
  // for getting Issuer List
getissuer(params){
  this.issuerService.getIssuerList(params).then(r =>  {
      this.issuerGroups = r.result;
    });
}

  ngOnChanges(change) {
    console.log('abc',this.issuerForm.controls);
    this.loading = this.page == 'add' ? false : true;
    if (change.issuer && change.issuer.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.issuerForm.controls['issuerId'].setValue(change.issuer.currentValue.issuerId);
      this.issuerForm.controls['firstName'].setValue(change.issuer.currentValue.firstName);
      this.issuerForm.controls['lastName'].setValue(change.issuer.currentValue.lastName);
      this.issuerForm.controls['unidNo'].setValue(change.issuer.currentValue.unidNo);
      this.issuerForm.controls['siteId'].setValue(change.issuer.currentValue.siteId);
      this.issuerForm.controls['FileType'].setValue(this.filetype);
    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="First Name is required";
    this.lnameReq="Last Name is required";
    this.siteReq="You must select a site";
    this.unidReq="UNID No: is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.issuerForm.valid && this.ErrorList.length=== 0) {
     validPost.uploadedFile = this.filetype;
     //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error='';
  }
  // for Uploading Files
  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.issuerForm.controls['firstName'].hasError('required') || (this.issuerForm.controls['firstName'].touched))
    {
      this.nameReq="";
    }
    if(this.issuerForm.controls['lastName'].hasError('required') || (this.issuerForm.controls['lastName'].touched))
    {
      this.lnameReq="";
    }
    if(this.issuerForm.controls['siteId'].hasError('required') || (this.issuerForm.controls['siteId'].touched))
    {
      this.siteReq="";
    }
    if(this.issuerForm.controls['unidNo'].hasError('required') || (this.issuerForm.controls['unidNo'].touched))
    {
      this.unidReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}