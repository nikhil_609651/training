import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'issuer',
    template: `<router-outlet></router-outlet>`
})
export class IssuerComponent {
	constructor(private router: Router) {
	}
}
