import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'issuer-edit',
  template: require('./issuer-edit.html')
})
export class IssuerEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public issuer: any;
  public page = 'edit';
  public issuerdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private issuerService: IssuerFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.issuer = this.issuerService.getissuer(this.id);
      });
    }

  onSave(issuer: any) {
    let files = issuer.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('issuerId',this.id);
    formData.append('firstName', issuer.firstName);
    formData.append('lastName', issuer.lastName);
    formData.append('unidNo', issuer.unidNo);
    formData.append('siteId', issuer.siteId);
    formData.append('site', "");
    formData.append('isDeleted',"");
    this.issuerService.Save(formData).then(r =>  {
      this.success = 'Issuer Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/issuer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
