import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'issuer-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./issuer-list.html')
})
export class IssuerListComponent implements OnInit{
  selectedsiteAll: any;
  bSite: any;
  bUNID: any;
  bLastName: any;
  bFirstName: any;
  attachment_deleted_id: any;
  selectedAll: any;
  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  issuerId: any;
  checkedList: any;
  siteGroups: any;
  public deleteattachmentmessage='';
  public issuer: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];

  checked: string[] = [];
  // For Headers
  public columns: Array<any> = [
    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'First Name', name: 'FirstName', sort: true },
    { title: 'Last Name', name: 'LastName', sort: true },
    { title: 'UNID', name: 'UNID', sort: true },
    { title: 'Site', name: 'Site', sort: true, filter:true},
    { title: 'Actions', name: 'actions', sort: false }
  ];
  sitevalue: any;
  public options= [];
  public optionss= [];
  public selectedValue: any;
  public totalfeilds = 0;
  public page: number = 1;
  public itemsPerPage:number = 3;
  public maxSize: number = 5;
  public numPages: number = 2;
  public length: number = 5;
  public next = '';
  viewIssuerForm: FormGroup;
  public issuer_deleted_id = '';
  public deletemessage= '';
  public config: any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,private fb: FormBuilder,
    private issuerService: IssuerFormService, private configuration: Configuration, private _sharedService : SharedService) {
    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
      this.privileges = privileges;
      this.showListPage = true;
    });
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('sitefilter', '');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.rows = configuration.rows;

    this.viewIssuerForm = fb.group({
                  'bIssuerId': [''],
                  'bFirstName': [''],
                  'bLastName': [''],
                  'bUNID': [''],
                  'bSite': [''],
              });
    var vForm = this.viewIssuerForm;
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_nameId');
    this.getissuerList(this.params);
    this.getSites(userId);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

   // Filteration
   selectAll(event) {
    this.checkArr = [];
    for (var i = 0; i < this.siteGroups.length; i++) {
      this.siteGroups[i].selected = this.selectedsiteAll;
      if(event.target.checked){
          this.checkArr.push(this.siteGroups[i].siteId.toString());
      } else {
          this.checkArr = [];
      }
    }
     this.params.set('sitefilter', this.checkArr.toString());
  }

  checkArr = [];
  checkIfAllSelected(option, event,item) {
    this.selectedsiteAll = this.siteGroups.every(function(item:any) {
      return item.selected == true;
    })
    var key = event.target.value.toString();
    var index = this.checkArr.indexOf(key);
    if(event.target.checked) {
      this.checkArr.push(event.target.value);
    } else {
      this.checkArr.splice(index,1);
    }
    this.params.set('sitefilter', this.checkArr.toString());
  }

  // For Applying Filtering to List
  apply() {
    this.getissuerList(this.params);
      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this) , 0);
      jQuery('ViewSiteModal').modal('hide');
      jQuery('body').removeClass('modal-open');
      jQuery('.modal-backdrop').remove();
  }

  // Filteration
  getissuerList(params: any) {
    this.loading = true;
    this.issuerService.getIssuerList(params).then(response => {
      this.issuer = response['result'];
      if (this.issuer.length > 0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else{
        this.page = 1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  //For getting List of Sites inside Filtering Modal/Popup
  getSites(userId) {
    this.issuerService.getFilterSites(userId).then(r =>  {
        this.siteGroups = r;
        this.loading = false;
      });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.issuerService.getIssuerList(params).then(response => {
      this.issuer = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)) {
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getissuerList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getissuerList(this.params);
  }

  view(data){
    if (data.issuerId != 0) {
      this.bFirstName = data.firstName;
      this.bLastName = data.lastName;
      this.bUNID = data.unidNo;
      this.bSite = data.siteMaster.name;
      this.viewIssuerForm.controls['bFirstName'].setValue(this.bFirstName);
      this.viewIssuerForm.controls['bLastName'].setValue(this.bLastName);
      this.viewIssuerForm.controls['bUNID'].setValue(this.bUNID);
      this.viewIssuerForm.controls['bSite'].setValue(this.bSite);
    }
  }

  viewclose() {
    setTimeout(function () {
        jQuery('#ViewIssuer').modal('toggle');
      }.bind(this), 0);
  }

  viewcloseattachment() {
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }

  addissuer() {
    this.router.navigate(['./home/master/issuer/add']);
  }

  editissuer(id) {
    this.router.navigate(['./home/master/issuer/edit/', id]);
  }

  deleteissuer(issuer){
    this.issuer_deleted_id = issuer;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  // For Deleting Issuer
  deleteissuerconfirm(issuer){
      this.issuer_deleted_id= issuer.issuerId;
      this.issuerService.deleteissuer(  this.issuer_deleted_id).then(r =>  {
        this.getissuerList(this.params);
        this.deletemessage = 'Issuer Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }

  // For File Attachment
  attachment(id){
    this.issuerId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadIssuerFiles?FileId=';
    this.issuerService.getallissuerattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
            id :this.attachments[i].id,
            referenceId:this.attachments[i].referenceId,
            shortFileName : this.attachments[i].shortFileName,
            fileName:this.attachments[i].fileName,
            createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
          }
      } else{
        this.getissuerList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    });
  }

  // For Deleting Attachment
  deleteattachment(id) {
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear() {
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.issuerService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.issuerId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  Sort(param,order,sortstatus){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getissuerList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
