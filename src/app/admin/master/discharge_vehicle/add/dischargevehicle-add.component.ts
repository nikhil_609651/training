import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DischargeVehicleFormService } from 'app/admin/master/discharge_vehicle/services/dischargevehicle-form.service';

@Component({
  selector: 'add',
  template: require('./dischargevehicle-add.html')
})
export class DischargeVehicleAddComponent implements OnInit{
  companyList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private dischargevehicleService: DischargeVehicleFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';

  }
  getCompanyList(){
    this.dischargevehicleService.getCompanyList().then(r => {
      this.companyList = r;
  })
      .catch(r => {
          this.handleError(r);
      })

}
  onSave(dv: any) {
    dv.dischargeVehicleId=0;
    let files = dv.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('dischargeVehicleId', '0');
    formData.append('name', dv.name);
    formData.append('companyId', dv.companyId);
    formData.append('address', dv.address);
    formData.append('isDeleted',"");
    this.dischargevehicleService.Save(formData).then(r =>  {
      this.success = 'Discharge Vehicle Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/discharge-vehicle']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
