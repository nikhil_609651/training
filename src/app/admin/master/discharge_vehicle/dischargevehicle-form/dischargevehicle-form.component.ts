import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { DischargeVehicleFormService } from 'app/admin/master/discharge_vehicle/services/dischargevehicle-form.service';

@Component({
  selector: 'dischargevehicle-form',
  template: require('./dischargevehicle-form.html')
})
export class DischargeVehicleFormComponent {
  savebutton: boolean = true;
  companyReq: string;
  nameReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() dischargevehicle;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  dischargevehicleForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public companyList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public dischargevehicle_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public dischargevehicleGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private dischargevehicleService: DischargeVehicleFormService, private router: Router,
    private route: ActivatedRoute) {
   this.getCompanyList();
    this.dischargevehicleForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'dischargeVehicleId': [''],
      'companyId': ['', Validators.compose([Validators.required])],
      'address':[''],
      'FileType':[''],
    });
    var cForm = this.dischargevehicleForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.dischargevehicle_id = params['id'];
      });
  }



getdischargevehicles(params){
  this.dischargevehicleService.getdischargevehicleList(params).then(r =>  {
      this.dischargevehicleGroups = r.result;
      this.loading = false;
    })
}
getCompanyList(){
  this.dischargevehicleService.getCompanyList().then(r => {
    this.companyList = r.result;
})
    .catch(r => {
        this.handleError(r);
    })

}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.dischargevehicle && change.dischargevehicle.currentValue) {

      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.dischargevehicleForm.controls['dischargeVehicleId'].setValue(change.dischargevehicle.currentValue.dischargeVehicleId);
      this.dischargevehicleForm.controls['name'].setValue(change.dischargevehicle.currentValue.name);
      this.dischargevehicleForm.controls['address'].setValue(change.dischargevehicle.currentValue.address);
      this.dischargevehicleForm.controls['companyId'].setValue(change.dischargevehicle.currentValue.companyId);
      this.dischargevehicleForm.controls['FileType'].setValue(this.filetype);
    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="Registration Number is required";
    this.companyReq="You must select a company";
    this.clearerror();
    this.formSubmited = true;
    if(this.dischargevehicleForm.valid && this.ErrorList.length==0) {
        validPost.uploadedFile = this.filetype;
        //this.savebutton = false;
        this.saved.emit(validPost);
      } else{
        jQuery('form').find(':input.ng-invalid:first').focus();
      }
  }
  clearerror(){
    this.error="";
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.dischargevehicleForm.controls['name'].hasError('required') || (this.dischargevehicleForm.controls['name'].touched))
    {
      this.nameReq="";
    }
    if(this.dischargevehicleForm.controls['companyId'].hasError('required') || (this.dischargevehicleForm.controls['companyId'].touched))
    {
      this.companyReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}