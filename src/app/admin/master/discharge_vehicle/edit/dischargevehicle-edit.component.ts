import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DischargeVehicleFormService } from 'app/admin/master/discharge_vehicle/services/dischargevehicle-form.service';

@Component({
  selector: 'dischargevehicle-edit',
  template: require('./dischargevehicle-edit.html')
})
export class DischargeVehicleEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public dischargevehicle: any;
  public page = 'edit';
public dischargevehicledata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private dischargevehicleService: DischargeVehicleFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

          this.id = params['id'];
                this.dischargevehicle = this.dischargevehicleService.getdischargevehicle(this.id );
      });
 

	}
  onSave(dv: any) {
    console.log()
    let files = dv.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('dischargeVehicleId', this.id);
    formData.append('name', dv.name);
    formData.append('companyId', dv.companyId);
    formData.append('address', dv.address);
    formData.append('isDeleted',"");
    this.dischargevehicleService.Save(formData).then(r =>  {
      this.success = 'Discharge Vehicle Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/discharge-vehicle']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
