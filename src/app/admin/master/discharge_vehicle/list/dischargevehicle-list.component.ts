import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { DischargeVehicleFormService } from 'app/admin/master/discharge_vehicle/services/dischargevehicle-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'dischargevehicle-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./dischargevehicle-list.html')
})
export class DischargeVehicleListComponent implements OnInit{

  selectedcompanyAll: any;
  address: any;
  companyName: any;
  regNo: any;
  attachment_deleted_id: any;
  selectedAll: any;
  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  dischargeVehicleId: any;
  companyGroups: any;

 public dischargevehicle: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  checked: string[] = [];
  public columns:Array<any> = [


    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'Registration No', name: 'RegistrationNo', sort: true },
    { title: 'Transport Company Name', name: 'CompanyName', sort: true,filter:true },
    { title: 'Address', name: 'Address',sort: false  },
    { title: 'Actions',  name: 'actions' ,sort: false}
  ];
  viewDischargeVehicleForm: FormGroup;
  public totalfeilds = 0;
  public page:number = 1;
  public items:any;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public dischargevehicle_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
  private dischargevehicleService: DischargeVehicleFormService,
  private fb: FormBuilder,
  private configuration: Configuration,
  private _sharedService : SharedService) {
    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
      this.privileges = privileges;
      this.showListPage = true;
    });
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('CompanyFilter', '');
    this.rows = configuration.rows;
    this.viewDischargeVehicleForm = fb.group({

      'regNo': [''],
      'dischargeVehicleId': [''],
      'companyName': [''],
      'address': [''],
  });
  var vForm = this.viewDischargeVehicleForm;
  }

  ngOnInit() {
    this.getdischargevehicleList(this.params);
    this.getclasslist();
    this.items = this.rows[0];
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }
  
  getclasslist(){
    this.dischargevehicleService.getCompanyFilterList().then(r =>  {
        this.companyGroups = r;
        this.loading = false;
      });

  }
  getdischargevehicleList(params: any) {
    this.loading = true;
    this.dischargevehicleService.getdischargevehicleList(params).then(response => {
      this.dischargevehicle = response['result']
      if(this.dischargevehicle.length>0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else {
        this.page=1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

   getAllLists(params: any) {
    this.loading = true;
    this.dischargevehicleService.getdischargevehicleList(params).then(response => {
      this.dischargevehicle = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  // Filteration
  selectAll(event) {
    this.checkArr = [];
    for (var i = 0; i < this.companyGroups.length; i++) {
      this.companyGroups[i].selected = this.selectedcompanyAll;
      if(event.target.checked){
          this.checkArr.push(this.companyGroups[i].id.toString());
      } else {
          this.checkArr = [];
      }
    }
     this.params.set('CompanyFilter', this.checkArr.toString());
}
checkArr = [];
checkIfAllSelected(option, event,item) {

this.selectedcompanyAll = this.companyGroups.every(function(item:any) {
return item.selected == true;
})
var key = event.target.value.toString();
var index = this.checkArr.indexOf(key);
if(event.target.checked) {
this.checkArr.push(event.target.value);
} else {
this.checkArr.splice(index,1);
}
this.params.set('CompanyFilter', this.checkArr.toString());
}

// For Applying Filtering to List
apply(){
this.getdischargevehicleList(this.params);
  setTimeout(function () {
    jQuery('#ViewCompanyModal').modal('toggle');
  }.bind(this) , 0);
    jQuery('ViewCompanyModal').modal('hide');
      jQuery('body').removeClass('modal-open');
      jQuery('.modal-backdrop').remove();
}
// Filteration
  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)

        if(this.next || (changedValue < this.itemsPerPage)){
          this.itemsPerPage =  event.target.value;
          let params = this.params;
          params.set('limit', event.target.value);
          this.getdischargevehicleList(params);
        }
  }
  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {

    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);


    }
    this.getdischargevehicleList(params);
  }


  adddischargevehicle() {
    this.router.navigate(['./home/master/discharge-vehicle/add']);
  }

  editdischargevehicle(id) {

    this.router.navigate(['./home/master/discharge-vehicle/edit/', id]);
  }

  deletedischargevehicle(dischargevehicle){



    this.dischargevehicle_deleted_id = dischargevehicle;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deletedischargevehicleconfirm(dischargevehicle){
  //alert(deleted);
    // if (confirm) {
      this.dischargevehicle_deleted_id= dischargevehicle.dischargeVehicleId;
      this.dischargevehicleService.deletedischargevehicle(  this.dischargevehicle_deleted_id).then(r =>  {
        this.getdischargevehicleList(this.params);
        this.deletemessage = "Discharge Vehicle Deleted Successfully";

      }).catch(r =>  {
         // console.log();
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  view(data){
    if (data.dischargeVehicleId != 0) {
        this.regNo = data.name;
        this.dischargeVehicleId = data.dischargeVehicleId;
        this.companyName = data.transportCompany.name;
        this.address = data.address;
        this.viewDischargeVehicleForm.controls['regNo'].setValue(this.regNo);
        this.viewDischargeVehicleForm.controls['dischargeVehicleId'].setValue(this.dischargeVehicleId);
        this.viewDischargeVehicleForm.controls['companyName'].setValue(this.companyName);
        this.viewDischargeVehicleForm.controls['address'].setValue(this.address);
    }
}
viewclose(){
  setTimeout(function () {
      jQuery('#ViewDischargeVehicle').modal('toggle');
    }.bind(this), 0);
 }
 viewcloseattachment(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
}

// For File Attachments
attachment(id){
  this.dischargeVehicleId=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadDischargeVehicleFiles?FileId=';
  this.dischargevehicleService.getalldischargevehicleattachments(id).then(response => {
  this.attachments=response;
  if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getdischargevehicleList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
  //alert(deleted);
    // if (confirm) {

      this.dischargevehicleService.deleteattachment(id).then(r =>  {
        this.attachment(this.dischargeVehicleId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";

      }).catch(r =>  {
         // console.log();
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }
  Sort(param,order,sortstatus){

    //this.params.set('ordering', sortParam);

    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getdischargevehicleList(this.params);




  }
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
