import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class DischargeVehicleFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public dischargevehicleurl = '';
    public companydropdownurl='';
    public companyFilterurl='';
    public attachmenturl ="";
    public downloadUrl = "";
    public deleteattachmentUrl = "";
    
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/DischargeVehicle/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveDischargeVehicle';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDischargeVehicleByID?id=';
        this.dischargevehicleurl = _configuration.ServerWithApiUrl+'Masters/GetDischargeVehicleByID?id=';
        this.companydropdownurl = _configuration.ServerWithApiUrl+'Masters/TransportCompany/';
        this.companyFilterurl = _configuration.ServerWithApiUrl+'Masters/GetDischargeVehicleCompanyFilter/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllDischargeVehicleAttachments?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadDischargeVehicleFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDischargeVehicleAttachments?Id=';
    }

    // List API for dischargevehicle
    getdischargevehicleList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getCompanyList(): Promise<any> {
        return this.authHttp.get(this.companydropdownurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCompanyFilterList(): Promise<any> {
        return this.authHttp.get(this.companyFilterurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //   Save API for dischargevehicle
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();

        formData.append('dischargeVehicleId', bu.dischargeVehicleId);
        formData.append('name', bu.name);
        formData.append('companyId', bu.companyId);
        formData.append('address', bu.address);
        formData.append('isDeleted',"");
        formData.append('FileType',bu.uploadedFile);
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);

    }

    deletedischargevehicle(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getdischargevehicle(id: string) {
        return this.authHttp.get(this.dischargevehicleurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getdownload(id): Promise<any>{
        return this.authHttp.get(this.downloadUrl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getalldischargevehicleattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
