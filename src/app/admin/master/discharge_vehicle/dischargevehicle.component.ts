import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'dischargevehicle',
    template: `<router-outlet></router-outlet>`
})
export class DischargeVehicleComponent {
	constructor(private router: Router) {
	}
}
