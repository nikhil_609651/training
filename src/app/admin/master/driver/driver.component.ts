import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'driver',
    template: `<router-outlet></router-outlet>`
})
export class DriverComponent {
	constructor(private router: Router) {
	}
}
