import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class DriverFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public companydropdownurl = '';
    public budropdownurl = '';
    public driverurl = '';
    public attachmenturl ="";
    public downloadUrl = "";
    public deleteattachmentUrl = "";

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Driver/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveDriver';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateDriver';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDriverByID?id=';
        this.driverurl = _configuration.ServerWithApiUrl+'Masters/GetDriverByID?id=';
        this.companydropdownurl = _configuration.ServerWithApiUrl+'Masters/TransportCompany/';
        this.budropdownurl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllDriverAttachments?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadDriverFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDriverAttachments?Id=';
    }
    
    // List API for driver
    getdriverList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getdownload(id): Promise<any>{
        return this.authHttp.get(this.downloadUrl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }
    
    getCompanyList(): Promise<any> {
        return this.authHttp.get(this.companydropdownurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBUList(): Promise<any> {
        return this.authHttp.get(this.budropdownurl +'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Save API for driver
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();
        formData.append('driverId', bu.driverId);
        formData.append('driverName', bu.driverName);
        formData.append('telephoneNo', bu.telephoneNo);
        formData.append('drivingLicenceNo', bu.drivingLicenceNo);
        formData.append('companyId', bu.companyId);
        formData.append('isDeleted'," ");
        formData.append('fileURL', " ");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);

    }

    deletedriver(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getalldriverattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getdriver(id: string) {
        return this.authHttp.get(this.driverurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
