import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { DriverFormService } from 'app/admin/master/driver/services/driver-form.service';

@Component({
  selector: 'driver-form',
  template: require('./driver-form.html')
})
export class DriverFormComponent {
  buReq: string;
  savebutton: boolean = true;
  buList: any;

  companyReq: string;
  licenceReq: string;
  mobReq: string;
  nameReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @Input() driver;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  driverForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public companyList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public driver_id = '';
  public driverGroups =[];
  params: URLSearchParams = new URLSearchParams();


  constructor(private fb: FormBuilder,
    private driverService: DriverFormService, private router: Router,
    private route: ActivatedRoute) {
    this.getCompanyList();
    this.getBUList();
    this.driverForm = fb.group({
      'driverId': [''],
      'driverName': ['', Validators.compose([Validators.required])],
      'telephoneNo': [''],
      'drivingLicenceNo': [''],
      'businessunitnameId':['',Validators.compose([Validators.required])],
      'IsInternalUse' : [''],
      'companyId': ['', Validators.compose([Validators.required])],
      'remarks':[''],
      'FileType':[''],
    });
    var cForm = this.driverForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.driver_id = params['id'];
      });
  }
  _keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  getdrivers(params){
    this.driverService.getdriverList(params).then(r =>  {
        this.driverGroups = r.result;
        this.loading = false;
      })
  }

  getCompanyList(){
    this.driverService.getCompanyList().then(r => {
      this.companyList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })

  }

  getBUList(){
    this.driverService.getBUList().then(r => {
      this.buList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })

  }

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.driver && change.driver.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.driverForm.controls['driverName'].setValue(change.driver.currentValue.driverName);
      //this.driverForm.controls['businessunitnameId'].setValue(change.driver.currentValue.buName);
      this.driverForm.controls['telephoneNo'].setValue(change.driver.currentValue.telephoneNo);
      this.driverForm.controls['drivingLicenceNo'].setValue(change.driver.currentValue.drivingLicenceNo);
      this.driverForm.controls['companyId'].setValue(change.driver.currentValue.companyId);
      this.driverForm.controls['businessunitnameId'].setValue(change.driver.currentValue.buId1);
      this.driverForm.controls['remarks'].setValue(change.driver.currentValue.remarks);
      this.driverForm.controls['FileType'].setValue(this.filetype);
    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="Driver Name is required";
    this.mobReq="Telephone No: is required";
    this.licenceReq="Licence No: is required";
    this.companyReq="You must select a company";
    this.buReq="You must select a Busines Unit";
   this.clearerror();
    this.formSubmited = true;
    if(this.driverForm.valid && this.ErrorList.length==0) {
        validPost.uploadedFile = this.filetype;
        //this.savebutton = false;
        this.saved.emit(validPost);
      } else{
        jQuery('form').find(':input.ng-invalid:first').focus();
      }
  }

  clearerror(){
    this.error="";
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  clearmsgs(){
    if(this.driverForm.controls['driverName'].hasError('required') || (this.driverForm.controls['driverName'].touched))
    {
      this.nameReq="";
    }
    if(this.driverForm.controls['businessunitnameId'].hasError('required') || (this.driverForm.controls['businessunitnameId'].touched))
    {
      this.buReq="";
    }
    if(this.driverForm.controls['telephoneNo'].hasError('required') || (this.driverForm.controls['telephoneNo'].touched))
    {
      this.mobReq="";
    }
    if(this.driverForm.controls['drivingLicenceNo'].hasError('required') || (this.driverForm.controls['drivingLicenceNo'].touched))
    {
      this.licenceReq="";
    }
    if(this.driverForm.controls['companyId'].hasError('required') || (this.driverForm.controls['companyId'].touched))
    {
      this.companyReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}