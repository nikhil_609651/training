import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DriverFormService } from 'app/admin/master/driver/services/driver-form.service';
@Component({
  selector: 'driver-edit',
  template: require('./driver-edit.html')
})
export class DriverEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public driver: any;
  public page = 'edit';
public driverdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private driverService: DriverFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.driver = this.driverService.getdriver(this.id);
      });
 

	}

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('driverId',this.id);
        formData.append('DriverName', bu.driverName);
        formData.append('BUId', bu.businessunitnameId);
        formData.append('TelephoneNo', bu.telephoneNo);
        formData.append('DrivingLicenceNo', bu.drivingLicenceNo);
        formData.append('companyId', bu.companyId);
        formData.append('Remarks', bu.remarks);
        formData.append('isDeleted', bu.isDeleted);
    this.driverService.Save(formData).then(r =>  {
      this.success = 'Driver Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/driver']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
