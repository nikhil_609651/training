import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DriverFormService } from 'app/admin/master/driver/services/driver-form.service';
@Component({
  selector: 'add',
  template: require('./driver-add.html')
})
export class DriverAddComponent implements OnInit{
  companyList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private driverService: DriverFormService) {

  }
  ngOnInit() {
    this.page = 'add';
}
  getCompanyList(){
    this.driverService.getCompanyList().then(r => {
      this.companyList = r;
  })
      .catch(r => {
          this.handleError(r);
      })

}
  onSave(bu: any) {
    bu.driverId=0;
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('driverId','0');
        formData.append('DriverName', bu.driverName);
        formData.append('BUId', bu.businessunitnameId);
        formData.append('TelephoneNo', bu.telephoneNo);
        formData.append('DrivingLicenceNo', bu.drivingLicenceNo);
        formData.append('companyId', bu.companyId);
        formData.append('Remarks', bu.remarks);
        formData.append('isDeleted', bu.isDeleted);
    this.driverService.Save(formData).then(r =>  {
      this.success = 'Driver Created successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/driver']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
