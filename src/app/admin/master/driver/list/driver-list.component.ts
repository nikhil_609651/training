import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { DriverFormService } from 'app/admin/master/driver/services/driver-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'driver-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./driver-list.html')
})
export class DriverListComponent implements OnInit{
  licenceNo: any;
  mobile: any;
  dName:any;
  attachment_deleted_id: any;
  attachmentmessage: string;
  driverId: any;
  API_URL: string;
  attachmentGroup: any;
  attachments: any;
  public deleteattachmentmessage='';
 public driver: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'Driver Name', name: 'Name', sort: true },
    { title: 'Telephone No', name: 'MobileNo', sort: false },
    { title: 'Driving License Number', name: 'drivingLicenceNo', sort: false },
    { title: 'Actions',  name: 'actions', sort: false }
  ];
  viewDriverForm: FormGroup;
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public driver_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
		isAdd: false,
		isDelete : false,
		isEdit : false,
		isView : false
	}
	privilegeSubscription : Subscription;
	showListPage : boolean = false;

  constructor(private router: Router,
    private driverService: DriverFormService, 
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {

      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.rows = configuration.rows;
      this.viewDriverForm = fb.group({
        'driverName': [''],
        'telephoneNo': [''],
        'licenceNo': [''],
      });
      var vForm = this.viewDriverForm;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  ngOnInit() {
    this.getdriverList(this.params);
    this.totalfeilds = this.columns.length;
  }
 
  getdriverList(params: any) {
    this.loading = true;
    this.driverService.getdriverList(params).then(response => {
      this.driver = response['result'];
      if(this.driver.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
   getAllLists(params: any) {
    this.loading = true;
    this.driverService.getdriverList(params).then(response => {
      this.driver = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getdriverList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getdriverList(this.params);
  }

  adddriver() {
    this.router.navigate(['./home/master/driver/add']);
  }
  editdriver(id) {
    this.router.navigate(['./home/master/driver/edit/', id]);
  }
  
  deletedriver(driver){
    this.driver_deleted_id = driver;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deletedriverconfirm(driver){
    
      this.driver_deleted_id= driver.driverId;
      this.driverService.deletedriver(  this.driver_deleted_id).then(r =>  {
        this.getdriverList(this.params);
        this.deletemessage = "Driver Deleted Successfully";
       
      }).catch(r =>  {
         
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }
  view(data){
    if (data.driverId != 0) {
        this.dName = data.driverName;
        this.mobile = data.telephoneNo;
        this.licenceNo = data.drivingLicenceNo;
       this.viewDriverForm.controls['driverName'].setValue(this.dName);
       this.viewDriverForm.controls['telephoneNo'].setValue(this.mobile);
       this.viewDriverForm.controls['licenceNo'].setValue(this.licenceNo);
    }
  }
  viewclose(){
  setTimeout(function () {
      jQuery('#ViewDriver').modal('toggle');
    }.bind(this), 0);
 }

 viewClose(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
 }
// For File Attachments
attachment(id){
  this.attachmentmessage='';
  this.driverId=id;
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadDriverFiles?FileId=';
this.driverService.getalldriverattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getdriverList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
  //alert(deleted);
    // if (confirm) {
     
      this.driverService.deleteattachment( this.attachment_deleted_id).then(r =>  {
        this.attachment(this.driverId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
       
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }
Sort(param,order,sortstatus){
  //this.params.set('ordering', sortParam);
 
  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getdriverList(this.params);

    }
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
