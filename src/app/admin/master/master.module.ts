import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { PaginationModule, TabsModule, TooltipModule } from 'ng2-bootstrap';
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';

import { Ng2SimplePageScrollModule } from 'ng2-simple-page-scroll';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { routing } from './master.routing';
import { BusinessUnitListComponent } from './business_unit/list/businessunit-list.component';
import { BusinessUnitAddComponent } from './business_unit/add/businessunit-add.component';
import { BusinessUnitComponent } from './business_unit/businessunit.component';
import { BusinessUnitFormComponent } from './business_unit/businessunit-form/businessunit-form.component';
import { BusinessUnitFormService } from './business_unit/services/businessunit-form.service';
import { BusinessUnitEditComponent } from './business_unit/edit/businessunit-edit.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomersListComponent } from './customers/list/customers-list.component';
import { CustomersAddComponent } from './customers/add/customers-add.component';
import { CustomersFormComponent } from './customers/customers-form/customers-form.component';
import { CustomersEditComponent } from './customers/edit/customers-edit.component';
import { DischargeVehicleComponent } from './discharge_vehicle/dischargevehicle.component';
import { DischargeVehicleListComponent } from './discharge_vehicle/list/dischargevehicle-list.component';
import { DischargeVehicleAddComponent } from './discharge_vehicle/add/dischargevehicle-add.component';
import { DischargeVehicleFormComponent } from './discharge_vehicle/dischargevehicle-form/dischargevehicle-form.component';
import { DischargeVehicleEditComponent } from './discharge_vehicle/edit/dischargevehicle-edit.component';
import { DriverComponent } from './driver/driver.component';
import { DriverListComponent } from './driver/list/driver-list.component';
import { DriverAddComponent } from './driver/add/driver-add.component';
import { DriverFormComponent } from './driver/driver-form/driver-form.component';
import { DriverEditComponent } from './driver/edit/driver-edit.component';
import { SectionMasterComponent } from './sectionmaster/section.component';
import { SectionAddComponent } from './sectionmaster/add/section-add.component';
import { SectionEditComponent } from './sectionmaster/edit/section-edit.component';
import { SectionListComponent } from './sectionmaster/list/section-list.component';
import { SectionFormComponent } from './sectionmaster/section-form/section-form.component';
import { OwnershipAddComponent } from './ownership/add/ownership-add.component';
import { OwnershipComponent } from './ownership/ownership.component';
import { OwnershipEditComponent } from './ownership/edit/ownership-edit.component';
import { OwnershipFormComponent } from './ownership/ownership-form/ownership-form.component';
import { OwnershipListComponent } from './ownership/list/ownership-list.component';
import { IssuerComponent } from './issuer/issuer.component';
import { IssuerListComponent } from './issuer/list/issuer-list.component';
import { IssuerAddComponent } from './issuer/add/issuer-add.component';
import { IssuerFormComponent } from './issuer/issuer-form/issuer-form.component';
import { IssuerEditComponent } from './issuer/edit/issuer-edit.component';
import { ReceiverComponent } from './receiver/receiver.component';
import { ReceiverListComponent } from './receiver/list/receiver-list.component';
import { ReceiverAddComponent } from './receiver/add/receiver-add.component';
import { ReceiverFormComponent } from './receiver/receiver-form/receiver-form.component';
import { ReceiverEditComponent } from './receiver/edit/receiver-edit.component';
import { SubUnitComponent } from './subunit/subunit.component';
import { SubUnitListComponent } from './subunit/list/subunit-list.component';
import { SubUnitEditComponent } from './subunit/edit/subunit-edit.component';
import { SubUnitFormComponent } from './subunit/subunit-form/subunit-form.component';
import { SubUnitAddComponent } from './subunit/add/subunit-add.component';
import { AirCraftTypeComponent } from './equipmentmaster/aircrafttype/aircrafttype.component';
import { AirCraftTypeListComponent } from './equipmentmaster/aircrafttype/list/aircrafttype-list.component';
import { AirCraftTypeFormComponent } from './equipmentmaster/aircrafttype/aircrafttype-form/aircrafttype-form.component';
import { AirCraftTypeAddComponent } from './equipmentmaster/aircrafttype/add/aircrafttype-add.component';
import { AirCraftTypeEditComponent } from './equipmentmaster/aircrafttype/edit/aircrafttype-edit.component';
import { UomComponent } from './uom/uom.component';
import { UomAddComponent } from './uom/add/uom-add.component';
import { UomEditComponent } from './uom/edit/uom-edit.component';
import { UomListComponent } from './uom/list/uom-list.component';
import { UomFormComponent } from './uom/uom-form/uom-form.component';
import { OwnershipFormService } from './ownership/services/ownership-form.service';
import { IssuerFormService } from './issuer/service/issuer.service';
import { ReceiverFormService } from './receiver/service/receiver.service';
import { CustomersFormService } from './customers/services/customers-form.service';
import { DischargeVehicleFormService } from './discharge_vehicle/services/dischargevehicle-form.service';
import { DriverFormService } from './driver/services/driver-form.service';
import { SectionMasterFormService } from './sectionmaster/services/section-form.service';
import { SubUnitFormService } from './subunit/services/subunit-form.service';
import { AirCraftTypeFormService } from './equipmentmaster/aircrafttype/services/aircrafttype-form.service';
import { UomFormService } from './uom/service/uom.service';
import { AircraftFormService } from './equipmentmaster/aircraft/services/aircraft-form.service';
import { GeneratorMakeModelFormService } from './equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
import { TruckFormService } from './truck/service/truck.service';
import { PolFormService } from './pol/service/pol-service';
import { DepartmentFormService } from './department/service/department-form.service';
import { VehicleTypeFormService } from './equipmentmaster/vehicletype/service/vehicletype-form.service';
import { VehicleMakeModelFormService } from './equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';
import { SiteFormService } from './site/services/site-form.service';
import { EquipmentTypeFormService } from './equipmentmaster/equipmenttype/service/equipmenttype-form.service';
import { StoragetankFormService } from './storagetank/service/storagetank-form.service';
import { TransportCompanyFormService } from './transportcompany/service/transportcompany.service';
import { GeneratorFormService } from './equipmentmaster/generator/service/generator-form.service';
import { EquipmentFormService } from './equipmentmaster/equipment/service/equipment-form.service';
import { DepartmentComponent } from './department/department.component';
import { DepartmentListComponent } from './department/list/department-list.component';
import { DepartmentFormComponent } from './department/department-form/department-form.component';
import { DepartmentAddComponent } from './department/add/department-add.component';
import { DepartmentEditComponent } from './department/edit/department-edit.component';
import { AirCraftComponent } from './equipmentmaster/aircraft/aircraft.component';
import { AircraftAddComponent } from './equipmentmaster/aircraft/add/aircraft-add.component';
import { AircraftEditComponent } from './equipmentmaster/aircraft/edit/aircraft-edit.component';
import { AircraftListComponent } from './equipmentmaster/aircraft/list/aircraft-list.component';
import { AircraftFormComponent } from './equipmentmaster/aircraft/aircraft-form/aircraft-form.component';
import { GeneratorMakeModelListComponent } from './equipmentmaster/generatormakemodel/list/generatormakemodel-list.component';
import { GeneratorMakeModelComponent } from './equipmentmaster/generatormakemodel/generatormakemodel.component';
import { GeneratorMakeModelAddComponent } from './equipmentmaster/generatormakemodel/add/generatormakemodel-add.component';
import { GeneratorMakeModelEditComponent } from './equipmentmaster/generatormakemodel/edit/generatormakemodel-edit.component';
import { GeneratorMakeModelFormComponent } from './equipmentmaster/generatormakemodel/generatormakemodel-form/generatormakemodel-form.component';
import { TruckAddComponent } from './truck/add/truck-add.component';
import { TruckComponent } from './truck/truck.component';
import { TruckEditComponent } from './truck/edit/truck-edit.component';
import { TruckListComponent } from './truck/list/truck-list.component';
import { TruckFormComponent } from './truck/truck-form/truck-form.component';
import { PolComponent } from './pol/pol-component';
import { PolListComponent } from './pol/list/pol-list.component';
import { PolAddComponent } from './pol/add/pol-add.component';
import { PolEditComponent } from './pol/edit/pol-edit.component';
import { PolFormComponent } from './pol/pol-form/pol-form.component';
import { VehicleTypeComponent } from './equipmentmaster/vehicletype/vehicletype.component';
import { VehicleTypeListComponent } from './equipmentmaster/vehicletype/list/vehicletype-list.component';
import { VehicleTypeFormComponent } from './equipmentmaster/vehicletype/vehicletype-form/vehicletype-form.component';
import { VehicleTypeAddComponent } from './equipmentmaster/vehicletype/add/vehicletype-add.component';
import { VehicleTypeEditComponent } from './equipmentmaster/vehicletype/edit/vehicletype-edit.component';
import { VehicleMakeModelListComponent } from './equipmentmaster/vehiclemakemodel/list/vehiclemakemodel-list.component';
import { VehicleMakeModelComponent } from './equipmentmaster/vehiclemakemodel/vehiclemakemodel.component';
import { VehicleMakeModelAddComponent } from './equipmentmaster/vehiclemakemodel/add/vehiclemakemodel-add.component';
import { VehicleMakeModelEditComponent } from './equipmentmaster/vehiclemakemodel/edit/vehiclemakemodel-edit.component';
import { VehicleMakeModelFormComponent } from './equipmentmaster/vehiclemakemodel/vehiclemakemodel-form/vehiclemakemodel-form.component';
import { SiteComponent } from './site/site.component';
import { SiteListComponent } from './site/list/site-list.component';
import { SiteAddComponent } from './site/add/site-add.component';
import { SiteFormComponent } from './site/site-form/site-form.component';
import { SiteEditComponent } from './site/edit/site-edit.component';
import { EquipmentTypeComponent } from './equipmentmaster/equipmenttype/equipmenttype.component';
import { EquipmentTypeListComponent } from './equipmentmaster/equipmenttype/list/equipmenttype-list.component';
import { EquipmentTypeFormComponent } from './equipmentmaster/equipmenttype/equipmenttype-form/equipmenttype-form.component';
import { EquipmentTypeAddComponent } from './equipmentmaster/equipmenttype/add/equipmenttype-add.component';
import { EquipmentTypeEditComponent } from './equipmentmaster/equipmenttype/edit/equipmenttype-edit.component';
import { GeneratorComponent } from './equipmentmaster/generator/generator.component';
import { GeneratorListComponent } from './equipmentmaster/generator/list/generator-list.component';
import { GeneratorFormComponent } from './equipmentmaster/generator/generator-form/generator-form.component';
import { GeneratorAddComponent } from './equipmentmaster/generator/add/generator-add.component';
import { GeneratorEditComponent } from './equipmentmaster/generator/edit/generator-edit.component';
import { EquipmentComponent } from './equipmentmaster/equipment/equipment.component';
import { EquipmentListComponent } from './equipmentmaster/equipment/list/equipment-list.component';
import { EquipmentAddComponent } from './equipmentmaster/equipment/add/equipment-add.component';
import { EquipmentFormComponent } from './equipmentmaster/equipment/equipment-form/equipment-form.component';
import { EquipmentEditComponent } from './equipmentmaster/equipment/edit/equipment-edit.component';
import { VehicleComponent } from './equipmentmaster/vehicle/vehicle.component';
import { VehicleListComponent } from './equipmentmaster/vehicle/list/vehicle-list.component';
import { VehicleFormComponent } from './equipmentmaster/vehicle/vehicle-form/vehicle-form.component';
import { VehicleAddComponent } from './equipmentmaster/vehicle/add/vehicle-add.component';
import { VehicleEditComponent } from './equipmentmaster/vehicle/edit/vehicle-edit.component';
import { StoragetankComponent } from './storagetank/storagetank.component';
import { StoragetankListComponent } from './storagetank/list/storagetank-list.component';
import { StoragetankAddComponent } from './storagetank/add/storagetank-add.component';
import { StoragetankEditComponent } from './storagetank/edit/storagetank-edit.component';
import { StoragetankFormComponent } from './storagetank/storagetank-form/storagetank-form.component';
import { TransportCompanyComponent } from './transportcompany/transportcompany.component';
import { TransportCompanyListComponent } from './transportcompany/list/transportcompany-list.component';
import { TransportCompanyEditComponent } from './transportcompany/edit/transportcompany-edit.component';
import { TransportCompanyAddComponent } from './transportcompany/add/transportcompany-add.component';
import { TransportCompanyFormComponent } from './transportcompany/transportcompany-form/transportcompany-form.component';
import { VehicleFormService } from './equipmentmaster/vehicle/service/vehicle-form.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    PaginationModule,
    TabsModule,
    TooltipModule,
    DatePickerModule,
    MyDatePickerModule,
    routing,
    Ng2SimplePageScrollModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    BusinessUnitListComponent,
    BusinessUnitAddComponent,
    BusinessUnitComponent,
    BusinessUnitFormComponent,
    BusinessUnitEditComponent,
    CustomersComponent,
    CustomersListComponent,
    CustomersAddComponent,
    CustomersFormComponent,
    CustomersEditComponent,
    DischargeVehicleComponent,
    DischargeVehicleListComponent,
    DischargeVehicleAddComponent,
    DischargeVehicleFormComponent,
    DischargeVehicleEditComponent,
    DriverComponent,
    DriverListComponent,
    DriverAddComponent,
    DriverFormComponent,
    DriverEditComponent,
    SectionMasterComponent,
    SectionAddComponent,
    SectionEditComponent,
    SectionListComponent,
    SectionFormComponent,
    OwnershipAddComponent,
    OwnershipComponent,
    OwnershipEditComponent,
    OwnershipFormComponent,
    OwnershipListComponent,
    IssuerComponent,
    IssuerListComponent,
    IssuerAddComponent,
    IssuerFormComponent,
    IssuerEditComponent,
    ReceiverComponent,
    ReceiverListComponent,
    ReceiverAddComponent,
    ReceiverFormComponent,
    ReceiverEditComponent,
    SubUnitComponent,
    SubUnitListComponent,
    SubUnitEditComponent,
    SubUnitFormComponent,
    SubUnitAddComponent,
    AirCraftTypeComponent,
    AirCraftTypeListComponent,
    AirCraftTypeFormComponent,
    AirCraftTypeAddComponent,
    AirCraftTypeEditComponent,
    UomComponent,
    UomAddComponent,
    UomEditComponent,
    UomListComponent,
    UomFormComponent,
    DepartmentComponent,
    DepartmentListComponent,
    DepartmentFormComponent,
    DepartmentAddComponent,
    DepartmentEditComponent,
    AirCraftComponent,
    AircraftAddComponent,
    AircraftEditComponent,
    AircraftListComponent,
    AircraftFormComponent,
    GeneratorMakeModelListComponent,
    GeneratorMakeModelComponent,
    GeneratorMakeModelAddComponent,
    GeneratorMakeModelEditComponent,
    GeneratorMakeModelFormComponent,
    TruckAddComponent,
    TruckComponent,
    TruckEditComponent,
    TruckListComponent,
    TruckFormComponent,
    PolComponent,
    PolListComponent,
    PolAddComponent,
    PolEditComponent,
    PolFormComponent,
    VehicleTypeComponent,
    VehicleTypeListComponent,
    VehicleTypeFormComponent,
    VehicleTypeAddComponent,
    VehicleTypeEditComponent,
    VehicleMakeModelListComponent,
    VehicleMakeModelComponent,
    VehicleMakeModelAddComponent,
    VehicleMakeModelEditComponent,
    VehicleMakeModelFormComponent,
    SiteComponent,
    SiteListComponent,
    SiteAddComponent,
    SiteFormComponent,
    SiteEditComponent,
    EquipmentTypeComponent,
    EquipmentTypeListComponent,
    EquipmentTypeFormComponent,
    EquipmentTypeAddComponent,
    EquipmentTypeEditComponent,
    GeneratorComponent,
    GeneratorListComponent,
    GeneratorFormComponent,
    GeneratorAddComponent,
    GeneratorEditComponent,
    EquipmentComponent,
    EquipmentListComponent,
    EquipmentAddComponent,
    EquipmentFormComponent,
    EquipmentEditComponent,
    VehicleComponent,
    VehicleListComponent,
    VehicleFormComponent,
    VehicleAddComponent,
    VehicleEditComponent,
    SiteComponent,
    SiteAddComponent,
    SiteListComponent,
    SiteEditComponent,
    SiteFormComponent,
    StoragetankComponent,
    StoragetankListComponent,
    StoragetankAddComponent,
    StoragetankEditComponent,
    StoragetankFormComponent,
    TransportCompanyComponent,
    TransportCompanyListComponent,
    TransportCompanyEditComponent,
    TransportCompanyAddComponent,
    TransportCompanyFormComponent,
  ],
  entryComponents:    [ ],
  providers: [
    BusinessUnitFormService,
    OwnershipFormService,
    IssuerFormService,
    ReceiverFormService,
    CustomersFormService,
    DischargeVehicleFormService,
    DriverFormService,
    SectionMasterFormService,
    SubUnitFormService,
    AirCraftTypeFormService,
    UomFormService,
    AircraftFormService,
    GeneratorMakeModelFormService,
    TruckFormService,
    PolFormService,
    DepartmentFormService,
    VehicleTypeFormService,
    VehicleMakeModelFormService,
    SiteFormService,
    EquipmentTypeFormService,
    StoragetankFormService,
    TransportCompanyFormService,
    GeneratorFormService,
    EquipmentFormService,
    VehicleFormService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export default class MasterModule {}
