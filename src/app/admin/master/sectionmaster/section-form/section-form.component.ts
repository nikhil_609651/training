import { Component, EventEmitter, Input, Output , OnInit, ViewChild} from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { SectionMasterFormService } from 'app/admin/master/sectionmaster/services/section-form.service';


@Component({
  selector: 'section-form',
  template: require('./section-form.html')
})
export class SectionFormComponent {
  savebutton: boolean=true;

  nameReq: string;
  filelength: number;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() section;
  @Input() error;
  @Input() page:string;
  @Output() saved = new EventEmitter();
  sectionForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public owner: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public section_id = '';
  image= '';
  userSettingsParams: URLSearchParams = new URLSearchParams();


  public sectionGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private sectionService: SectionMasterFormService, private router: Router, private route: ActivatedRoute) {
    //this.getareamanagers();
   // this.getCityGroups();
    //this.onselectednew();
    this.sectionForm = fb.group({
      'sectionId': [''],
      'sectionName':['', Validators.compose([Validators.required])],
      'description':[''],
      'FileType':[],

    });
    var cForm = this.sectionForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.section_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
  }

  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  onselected(){
    this.options = this.optionss;
    //console.log(this.options)
  }


getsections(params){
  this.sectionService.getsections(params).then(r =>  {
      this.sectionGroups = r.results;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('section_id',this.section_id);
  this.params.set('list','list');
  this.sectionService.getsections(params).then(r =>  {
    this.sectionGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.sectionGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.sectionGroups.length; i++) {
      var Obj = {
        label : this.sectionGroups[i].name,
        value :  +this.sectionGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.sectionGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}
onChange(event) {
  this.ErrorList=[];
  this.filelist = [];
  this.filetype = [];
  this.filelist = <Array<File>>event.target.files;
  for(let i = 0; i <this.filelist.length; i++) {
    let fSize = this.filelist[i].size;
    let fName = this.filelist[i].name;
    let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
    if(fSize > 2097152)
    {
      this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
    }
    else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="csv" && extnsion!="txt")
    {
      this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
    }
    else
    {
      this.ErrorList=[];
      this.filetype.push(this.filelist[i]);
      this.filelength= this.filetype.length;
    }
  }
}

  ngOnChanges(change) {
    console.log('abc',this.sectionForm.controls);
  //this.options=this.optionss;
    this.loading = this.page == 'add' ? false : true;
    if (change.section && change.section.currentValue) {
      this.loading = false;
      this.sectionForm.controls['sectionId'].setValue(change.section.currentValue.sectionId);
      this.sectionForm.controls['sectionName'].setValue(change.section.currentValue.sectionName);
      this.sectionForm.controls['description'].setValue(change.section.currentValue.description);

      console.log(this.sectionForm.controls);

    } else{
      this.loading = false;
    }
  }

  onSubmit(validPost) {
    this.nameReq="Section Name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.sectionForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.sectionForm.controls['sectionName'].hasError('required') || (this.sectionForm.controls['sectionName'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      console.log('error',e)
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}