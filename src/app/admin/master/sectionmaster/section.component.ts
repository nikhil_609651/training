import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'sectionmaster',
    template: `<router-outlet></router-outlet>`
})
export class SectionMasterComponent {
	constructor(private router: Router) {
	}
}