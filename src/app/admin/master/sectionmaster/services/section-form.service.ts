import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../../app.constants';

@Injectable()
export class SectionMasterFormService {

    private managerUrl = '';
    private userGroupUrl = '';
    private locality ='';
    private sublocality ='';
    private geturl='';
    private saveurl='';
    private editurl='';
    private updateurl='';
    private deleteurl='';
    private attachmenturl='';
    private deleteattachmentUrl='';
    
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.geturl = _configuration.ServerWithApiUrl+'Masters/Section';
        this.saveurl   =_configuration.ServerWithApiUrl+'Masters/SaveSection';
        this.updateurl   =_configuration.ServerWithApiUrl+'Masters/UpdateSection';
        this.deleteurl = _configuration.ServerWithApiUrl+'Masters/DeleteSectionByID?id=';
        this.editurl = _configuration.ServerWithApiUrl+'Masters/GetSectionByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllSectionAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteSectionAttachments?Id=';
    }

    getSectionList(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getsections(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
   
    getsection(id: string) {
        return this.authHttp.get(this.editurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    Save(section: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl, section)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(section: any): Promise<any> {
        const formData = new FormData();
        formData.append('sectionId', section.sectionId);
        formData.append('sectionName', section.sectionName);
        formData.append('description', section.description);
        formData.append('IsDeleted', "");
        formData.append('FileURL', null);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletesection(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteurl;
        return this.authHttp
            .post(this.deleteurl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }
    
    getallsectionattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}