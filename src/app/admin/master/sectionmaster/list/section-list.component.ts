import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../../../app.constants';
import { SectionMasterFormService } from 'app/admin/master/sectionmaster/services/section-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'sectionmaster-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./section-list.html')
})
export class SectionListComponent implements OnInit{

  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  attachmentGroup: any;
  sectionId(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  attachment_deleted_id(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  deleteattachmentmessage: any;
  viewSectionForm: FormGroup;
  description: any;
  name: any;
  Id: any;
  public sections: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: 'ID', name: 'Id', sort: true, },
    {title: 'Section', name: 'SectionName',sort: true},
    {title: 'Description', name: 'description', sort: false},
    {title: 'Actions', className: ['text-center'], name: 'actions', sort: false}
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public section_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private sectionService: SectionMasterFormService, 
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.rows = configuration.rows;
      this.viewSectionForm = fb.group({
        
        'name': [''],
        'Id': [''],
        'description': [''],
    });
    var vForm = this.viewSectionForm;
  }

  ngOnInit() {
    this.getsectionList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }
 
  getsectionList(params: any) {
    this.loading = true;
    this.sectionService.getSectionList(params).then(response => {
      this.sections = response['result']
      if(this.sections.length>0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else{
        this.page=1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
     
   getAllLists(params: any) {
    this.loading = true;
    this.sectionService.getSectionList(params).then(response => {
      this.sections = response['result']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getsectionList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getsectionList(this.params);
  }

  addsection() {
    this.router.navigate(['./home/master/section/add']);
  }

  editsection(id) {
    this.router.navigate(['./home/master/section/edit/', id]);
  }
  
  deletesection(section){
    this.section_deleted_id = section;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletesectionconfirm(section){
      this.section_deleted_id= section.sectionId;
      this.sectionService.deletesection(  this.section_deleted_id).then(r =>  {
        this.getsectionList(this.params);
        this.deletemessage = "Section Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  view(data){
    if (data.sectionId != 0) {
        this.name = data.sectionName;
        this.Id = data.sectionId;
        this.description = data.description;
        this.viewSectionForm.controls['name'].setValue(this.name);
        this.viewSectionForm.controls['description'].setValue(this.description);
    }
  }

  viewclose(){
    setTimeout(function () {
      jQuery('#ViewSection').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment(){
    setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }

  attachment(id){
    this.Id=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadSectionFiles?FileId=';
    this.sectionService.getallsectionattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
            id :this.attachments[i].id,
            referenceId:this.attachments[i].referenceId,
            shortFileName : this.attachments[i].shortFileName,
            fileName:this.attachments[i].fileName,
            createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else {
        this.getsectionList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    })
  }

  // For Deleting Attachments
  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(){
      this.sectionService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.Id);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  Sort(param,order,sortstatus){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getsectionList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
