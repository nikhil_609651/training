import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SectionMasterFormService } from 'app/admin/master/sectionmaster/services/section-form.service';

@Component({
  selector: 'add',
  template: require('./section-add.html')
})
export class SectionAddComponent implements OnInit{

  public error = {};
  public errorpass = '';
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private sectionService: SectionMasterFormService) {

  }
  ngOnInit() {
    this.page = 'add';
  }
  onSave(section: any) {
    section.sectionId=0;
      let files = section.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
          formData.append("FileType", files[i], files[i]['name']);
      }
      formData.append('sectionId', '0');
      formData.append('sectionName', section.sectionName);
      formData.append('description', section.description);
      formData.append('IsDeleted', "");
      formData.append('FileURL', "");
      console.log("values",section);
    this.sectionService.Save(formData).then(r =>  {
      this.success = 'Section Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/section']);
         // this.router.navigate('./admin/area-manager');
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    /*console.log(e)*/
    this.error = e;
      let detail = e.detail;
      console.log(detail);
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
