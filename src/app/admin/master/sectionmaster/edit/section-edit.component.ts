import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SectionMasterFormService } from 'app/admin/master/sectionmaster/services/section-form.service';

@Component({
  selector: 'section-edit',
  template: require('./section-edit.html')
})
export class SectionEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public section: any;
  public page = 'edit';
  

// public localityGroups: any;
// public usersublocality: any;
public sectiondata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private sectionService: SectionMasterFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
        
    	  this.section = this.sectionService.getsection(this.id)
      });
 
	}

  onSave(section: any) {
    let files = section.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('sectionId', this.id);
      formData.append('sectionName', section.sectionName);
      formData.append('description', section.description);
      formData.append('IsDeleted', "");
      formData.append('FileURL', "");
    this.sectionService.Save(formData).then(r =>  {
      this.success = 'Section Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/section']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
