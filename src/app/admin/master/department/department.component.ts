import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'department',
    template: `<router-outlet></router-outlet>`
})
export class DepartmentComponent {
	constructor(private router: Router) {
	}
}
