import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DepartmentFormService } from '../service/department-form.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'department-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./department-list.html')
})
export class DepartmentListComponent implements OnInit{
  Description: any;
  selectedAll: any;
  attachment_deleted_id: any;
  countryStatus: number;
  nameStatus: number;
  attachmentmessage: string;
  API_URL: string;
  attachmentGroup: any[];
  attachments: any;
  countryGroups: any;
  deptname: any;
  Id: any;
  checked: string[] = [];
  checkedall: string[] = [];
 public department: any=[];
  error: any;
  viewDepartmentForm: FormGroup;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  // For headers
  public columns:Array<any> = [
 

    { title: 'ID', name: 'Id', sort: true, },
    { title: 'Department', name: 'Name', sort: true, status:0 },
    { title: 'Description', name: 'Description', sort: false, filter:false, status:0 },
    { title: 'Actions', name: 'actions', sort: false }
  ];

  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public dept_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  sortstatus =2;
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
    private deptService: DepartmentFormService,
    private fb: FormBuilder,private _sharedService : SharedService,
    private configuration: Configuration) {

              /**user privileges**/	
              this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
                this.privileges = privileges;
              });
              this.itemsPerPage = configuration.itemsPerPage;
              this.params.set('limit', configuration.itemsPerPage.toString());
              this.rows = configuration.rows;
              this.viewDepartmentForm = fb.group({
                  'name': [''],
                  'description': [''],
              });
              var vForm = this.viewDepartmentForm;
  }

  ngOnInit() {
    this.getDepartmentList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  // For getting Department List
  getDepartmentList(params: any) {
    this.loading = true;
    this.deptService.getDepartmentList(params).then(response => {
      this.department = response['result'];
      if(this.department.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

   getAllLists(params: any) {
    this.loading = true;
    this.deptService.getDepartmentList(params).then(response => {
      this.department = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getDepartmentList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
  
    
    if(config.status ==0){
    config.status=1;
    }
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';


    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    this.getDepartmentList(this.params);
  }

  adddepartment() {
    this.router.navigate(['./home/master/department/add']);
  }

  editdepartment(id) {

    this.router.navigate(['./home/master/department/edit/', id]);
  }

  deletedepartment(dept){
    this.dept_deleted_id = dept;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }
// For Deleting Department
  deletedepartmentconfirm(dept){
      this.dept_deleted_id = dept.id;
      this.deptService.deletedepartment(  this.dept_deleted_id).then(r =>  {
        this.getDepartmentList(this.params);
        this.deletemessage = 'Department Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }
  view(data){
     if (data.id != 0) {
         this.deptname = data.name;
         this.Id = data.id;
         if(data.description!="null")
         {
          this.Description = data.description;
         }
         else
         {
          this.Description="";
         }
         this.viewDepartmentForm.controls['name'].setValue(this.deptname);
         this.viewDepartmentForm.controls['description'].setValue(this.Description);
     }
 }
 viewclose(){
  setTimeout(function () {
      jQuery('#ViewDepartment').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
}
// For File Attachments
attachment(id){
  this.Id=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadDepartmentFiles?FileId=';
this.deptService.getalldepartmentattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getDepartmentList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}
// For Deleting Attachments



deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){

    this.deptService.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.Id);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";
     
    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
Sort(param,order,sortstatus){
  //this.params.set('ordering', sortParam);
 
  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getDepartmentList(this.params);

}
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
