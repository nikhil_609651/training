import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { DepartmentFormService } from 'app/admin/master/department/service/department-form.service';

@Component({
  selector: 'department-form',
  template: require('./department-form.html')
})
export class DepartmentFormComponent {
  savebutton: boolean = true;

  nameReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() department;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  departmentForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public filesToUpload: Array<File>;
  public selectedFileNames: string[] = [];

  public file = '';
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public dept_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public departmentGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private deptService: DepartmentFormService, private router: Router,
    private route: ActivatedRoute) {

    this.departmentForm = fb.group({
      'name': ['', Validators.compose([Validators.required])],
      'id': [''],
      'description': [''],
      'FileType':[],
    });
    var cForm = this.departmentForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.dept_id = params['id'];
      });
  }
  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  // For getting BusinessUnit List
getdepartments(params){
  this.deptService.getDepartmentList(params).then(r =>  {
      this.departmentGroups = r.result;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.department && change.department.currentValue) {

      this.loading = false;
      this.departmentForm.controls['id'].setValue(change.department.currentValue.id);
      this.departmentForm.controls['name'].setValue(change.department.currentValue.name);
      if(change.department.currentValue.description!="null")
      {
        this.departmentForm.controls['description'].setValue(change.department.currentValue.description);
      }
      else
      {
        this.departmentForm.controls['description'].setValue("");
      }
      this.departmentForm.controls['FileType'].setValue(this.filetype);

    } else{
      this.loading = false;
    }
  }


  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="Department Name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.departmentForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.departmentForm.controls['name'].hasError('required') || (this.departmentForm.controls['name'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}