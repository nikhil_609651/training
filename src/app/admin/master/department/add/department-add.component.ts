import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DepartmentFormService } from 'app/admin/master/department/service/department-form.service';

@Component({
  selector: 'add',
  template: require('./department-add.html')
})
export class DepartmentAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private deptService: DepartmentFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';
  }

  onSave(dept: any) {
    dept.id=0;
    let files = dept.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('id', '0');
    formData.append('name', dept.name);
    formData.append('description', dept.description);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.deptService.Save(formData).then(r =>  {
      this.success = 'Department Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/department']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
