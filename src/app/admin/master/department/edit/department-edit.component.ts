import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DepartmentFormService } from 'app/admin/master/department/service/department-form.service';

@Component({
  selector: 'department-edit',
  template: require('./department-edit.html')
})
export class DepartmentEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public department: any;
  public page = 'edit';
 public deptdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private deptService: DepartmentFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
        this.department = this.deptService.getdepartment(this.id);
      });
	}

  onSave(dept: any) {   
    let files = dept.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('id', this.id);
    formData.append('name', dept.name);
    formData.append('description', dept.description);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.deptService.Save(formData).then(r =>  {
      this.success = 'Department Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/department']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
