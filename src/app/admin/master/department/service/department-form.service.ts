import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class DepartmentFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public countrydropdownUrl = '';
    public depturl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Department/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveDepartment';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/SaveDepartment';
       this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDepartmentByID?id=';
        //this.countrydropdownUrl = _configuration.ServerWithApiUrl+'Masters/Country';
        this.depturl = _configuration.ServerWithApiUrl+'Masters/GetDepartmentByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllDepartmentAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteDepartmentAttachments?Id=';
        
    }
    
   
// List API for Department
getDepartmentList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

//   Save API for Department
Save(dept: any): Promise<any>  {
  
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, dept)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}

private post(dept: any): Promise<any> {
    const formData = new FormData();
    formData.append('Id', dept.id);
    formData.append('Name', dept.name);
    formData.append('Description', dept.description);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    //let headers = new Headers({});

    let body = JSON.stringify({ 'foo': 'bar' });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    //let options = new RequestOptions({ headers: headers });
    
    headers.set('Upload-Content-Type',dept.FileType);
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deletedepartment(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
getalldepartmentattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getdepartment(id: string) {
    return this.authHttp.get(this.depturl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

private handleError(error: any) {
    return Promise.reject(error.json());
}
}
