import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SiteFormService } from 'app/admin/master/site/services/site-form.service';

@Component({
  selector: 'add',
  template: require('./site-add.html')
})
export class SiteAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private siteService: SiteFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';

  }
  
  onSave(site: any) {
    let files = site.uploadedFile;
    let stUnitList=site.SiteUnitDetails;
    let formData = new FormData();
    
    let calibrationList =  JSON.stringify(stUnitList);
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append("SiteUnitDetails",calibrationList);
    formData.append('siteId', '0');
    formData.append('BUId', site.BUId);
    formData.append('Name', site.Name);
    formData.append('Lattitude', site.Lattitude);
    formData.append('Longitude', site.Longitude);
    formData.append('SiteInchargeId', site.SiteInchargeId);
    formData.append('IsSfr',site.IsSfr);
    formData.append('UNID', site.UNID);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");

    this.siteService.Save(formData).then(r =>  {
      this.success = 'Site Created Successfully!';
     
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/site']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
