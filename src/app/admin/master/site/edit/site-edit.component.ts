import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SiteFormService } from 'app/admin/master/site/services/site-form.service';

@Component({
  selector: 'site-edit',
  template: require('./site-edit.html')
})
export class SiteEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public site: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private siteService: SiteFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.site = this.siteService.getsite(this.id);
                console.log('DATA',this.site)
      });


	}

  onSave(site: any) {
    console.log("Data",site);
    let files = site.uploadedFile;
    let stUnitList=site.SiteUnitDetails;
    let formData = new FormData();

    let calibrationList =  JSON.stringify(stUnitList);
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append("SiteUnitDetails",calibrationList);
    formData.append('siteId', this.id);
    formData.append('BUId', site.BUId);
    formData.append('Name', site.Name);
    formData.append('Lattitude', site.Lattitude);
    formData.append('Longitude', site.Longitude);
    formData.append('SiteInchargeId', site.SiteInchargeId);
    formData.append('IsSfr',site.IsSfr);
    formData.append('UNID', site.UNID);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.siteService.Save(formData).then(r =>  {
      this.success = 'Site Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/site']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
