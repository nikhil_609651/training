import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class SiteFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public siteurl = '';
    public businessunitUrl = '';
    public businessunitFilterUrl = '';
    public polUrl = '';
    public userdropdownUrl = '';
    public userFilterUrl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    public getSiteInchargeDetailsUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveSite';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateSite';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteSiteByID?id=';
        this.siteurl = _configuration.ServerWithApiUrl+'Masters/GetsiteByID?id=';
        this.businessunitUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
        this.businessunitFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetBusinessUnitFilter/';
        this.polUrl = _configuration.ServerWithApiUrl+'Masters/POL/';
        this.userdropdownUrl = _configuration.ServerWithApiUrl+'UserManagement/User';
        this.userFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetInchargeFilter';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllSiteAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteSiteAttachments?Id=';
        this.getSiteInchargeDetailsUrl = _configuration.ServerWithApiUrl+'UserManagement/GetUserByID?id=';
    }
    
    // List API for site
    getsiteList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getpolList(): Promise<any> {
        return this.authHttp.get(this.polUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUserList(): Promise<any> {
        return this.authHttp.get(this.userdropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUserFilterList(): Promise<any> {
        return this.authHttp.get(this.userFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBusinessUnitList(): Promise<any> {
        return this.authHttp.get(this.businessunitUrl +'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBusinessUnitFilterList(): Promise<any> {
        return this.authHttp.get(this.businessunitFilterUrl + '?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //Save API for site
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getSiteInchargeDetails(id: string) {
        return this.authHttp.get(this.getSiteInchargeDetailsUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();
        formData.append('siteId', bu.siteId);
        formData.append('name', bu.name);
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletesite(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getsite(id: string) {
        return this.authHttp.get(this.siteurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getallsiteattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
