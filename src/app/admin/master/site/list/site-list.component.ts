import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { SiteFormService } from 'app/admin/master/site/services/site-form.service';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'site-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./site-list.html')
})
export class SiteListComponent implements OnInit{
  selecteduserAll: any;
  selectedbuAll: any;
  selectedBUAll: any;
  selectedSiteInchargeAll: any;

  userGroups: any;
  buGroups: any;
  selectedAll: any;
  checkedbu: string[] = [];
  checkedsi: string[] = [];
  SiteIncharge: any;
  unId: any;
  Longitude: any;
  Lattitude: any;
  LocalReserve: any;
  BusinessUnit: any;
  siteName: any;
  viewSiteForm: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  sId: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  public site: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'id', sort: false },
    { title: 'Name', name: 'Name', sort: true },
    { title: 'Business Unit', name: 'BusinessUnit', sort: true, filter:true },
    { title: 'Site Incharge', name: 'Incharge', sort: true, filter:true },
    { title: 'UNID', name: 'UNID', sort: true },
    { title: 'Contact', name: 'Contact', sort: false },
    { title: 'Map View', name: 'MapView', sort: false },
    { title: 'Actions', className: ['text-center'], name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public site_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private fb: FormBuilder,
    private siteService: SiteFormService,
    private configuration: Configuration,
    private _sharedService : SharedService) {
      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('BusinessUnitFilter', '');
      this.params.set('SiteInchargeFilter', '');
      this.params.set('SiteInchargeFilter', '');
      this.params.set('UserId', localStorage.getItem('user_nameId'));
      this.rows = configuration.rows;
      this.viewSiteForm = fb.group({
        'Site': [''],
        'BusinessUnit': [''],
        'LocalReserve': [''],
        'Lattitude': [''],
        'Longitude': [''],
        'SiteIncharge': [''],
        'UNID': [''],
        'Role': [''],
        'ContactNo': [''],
        'Email': [''],
    });
    var vForm = this.viewSiteForm;
  }

  ngOnInit() {
    this.getsiteList(this.params);
    this.totalfeilds = this.columns.length;
    this.getbusinessunit();
    this.getSiteIncharge();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getsiteList(params: any) {
    this.loading = true;
    this.siteService.getsiteList(params).then(response => {
      this.site = response['result'];
      if(this.site.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getbusinessunit(){
    this.siteService.getBusinessUnitFilterList().then(r =>  {
        this.buGroups = r;
        this.loading = false;
      });
  }

  getSiteIncharge(){
    this.siteService.getUserFilterList().then(r =>  {
        this.userGroups = r;
        this.loading = false;
      });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getsiteList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    this.getsiteList(this.params);
  }

  view(data) {
    if (data.siteId != 0) {
        this.siteName = data.name;
        this.BusinessUnit = data.businessunit.buName;
        this.LocalReserve = data.siteUnitDetails.localReserve;
        this.Lattitude = data.lattitude;
        this.Longitude = data.longitude;
        this.SiteIncharge = data.userMaster.firstName;
        this.unId = data.userMaster.unid;
        this.viewSiteForm.controls['Site'].setValue(data.name);
        this.viewSiteForm.controls['BusinessUnit'].setValue(data.businessunit.buName);
        this.viewSiteForm.controls['LocalReserve'].setValue(this.LocalReserve);
        this.viewSiteForm.controls['Lattitude'].setValue(this.Lattitude);
        this.viewSiteForm.controls['Longitude'].setValue(this.Longitude);
        this.viewSiteForm.controls['SiteIncharge'].setValue(this.SiteIncharge);
        this.viewSiteForm.controls['UNID'].setValue(this.unId);
    }
  }

  addsite() {
    this.router.navigate(['./home/master/site/add']);
  }

  editsite(id) {
    this.router.navigate(['./home/master/site/edit/', id]);
  }

  deletesite(site){
    this.site_deleted_id = site;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletesiteconfirm(site){
      this.site_deleted_id= site.siteId;
      this.siteService.deletesite(this.site_deleted_id).then(r =>  {
        this.getsiteList(this.params);
        this.deletemessage = "Site Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  // For File Attachments
  attachment(id){
    this.sId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadSiteFiles?FileId=';
    this.siteService.getallsiteattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
            id :this.attachments[i].id,
            referenceId:this.attachments[i].referenceId,
            shortFileName : this.attachments[i].shortFileName,
            fileName:this.attachments[i].fileName,
            createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else{
        this.getsiteList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    })
  }

  viewclose(){
    setTimeout(function () {
        jQuery('#ViewSite').modal('toggle');
      }.bind(this), 0);
  }

  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }

  // For Deleting Attachments
  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.siteService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.sId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  // For Sorting
  Sort(param,order){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getsiteList(this.params);
  }

  // Filteration
  selectAll(item, event) {
    if(item === 'BusinessUnit'){
        this.checkArr = [];
        for (var i = 0; i < this.buGroups.length; i++) {
          this.buGroups[i].selected = this.selectedbuAll;
          if(event.target.checked){
              this.checkArr.push(this.buGroups[i].buId.toString());
          } else {
              this.checkArr = [];
          }
        }
        this.params.set('BusinessUnitFilter', this.checkArr.toString());
    }
    if(item === 'SiteIncharge'){
      this.checkUserArr=[]
      for (var i = 0; i < this.userGroups.length; i++) {
        this.userGroups[i].selected = this.selecteduserAll;
        if(event.target.checked){
            this.checkUserArr.push(this.userGroups[i].id.toString());
        } else {
            this.checkUserArr = [];
        }
      }
      this.params.set('SiteInchargeFilter', this.checkUserArr.toString());
    }
  }

  checkArr = [];
  checkUserArr=[];
  checkIfAllSelected(option, event,item) {
    if(item=='BusinessUnit') {
      this.selectedbuAll = this.buGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkArr.indexOf(key);
      if(event.target.checked) {
        this.checkArr.push(event.target.value);
      } else {
        this.checkArr.splice(index,1);
      }
      this.params.set('BusinessUnitFilter', this.checkArr.toString());
    }
    if(item === 'SiteIncharge') {
      this.selecteduserAll = this.userGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkUserArr.indexOf(key);
      if(event.target.checked) {
        this.checkUserArr.push(event.target.value);
      } else {
        this.checkUserArr.splice(index,1);
      }
      this.params.set('SiteInchargeFilter', this.checkUserArr.toString());
    }
  }

  // For Applying Filtering to List
  apply(item) {
    this.getsiteList(this.params);
    if(item === 'BusinessUnit'){
      setTimeout(function () {
        jQuery('#ViewBUModal').modal('toggle');
      }.bind(this) , 0);
      jQuery('ViewBUModal').modal('hide');
      jQuery('body').removeClass('modal-open');
      jQuery('.modal-backdrop').remove();
    }
    if(item === 'SiteIncharge') {
      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this) , 0);
      jQuery('ViewSiteModal').modal('hide');
      jQuery('body').removeClass('modal-open');
      jQuery('.modal-backdrop').remove();
    }
  }

  // Filteration
  getAllLists(params: any) {
    this.loading = true;
    this.siteService.getsiteList(params).then(response => {
      this.site = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
