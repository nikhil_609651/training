import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'site',
    template: `<router-outlet></router-outlet>`
})
export class SiteComponent {
	constructor(private router: Router) {
	}
}
