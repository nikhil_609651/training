
import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SiteFormService } from 'app/admin/master/site/services/site-form.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'site-form',
  templateUrl: './site-form.html',
  styleUrls: ['./site-form.component.scss']

})
export class SiteFormComponent implements OnInit,  OnChanges {
  email: any;
  contactnumber: any;
  CLength: number;
  polList = [];
  polId=0;
  idalertmessage: string='';
  nullalertmessage: string='';
  savebutton: boolean = true;
  sitearray: any[];
  inchargeRequired: string;
  siteRequired: string;
  buRequired:string;
  unid: any;
  uom: any;
  dummyvalue: number = 0;;
  OS = 0;
  SFR=0;
  LR=0;
  totalQty=0;
  isSFRSection=false;
  SFRUse=false;
  userList: any;
  pollist: any;
  businessunitList: any;
  @Input() site;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  siteForm: FormGroup;

  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];

  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';

  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public site_id = '';
  public text = '';
  public drop = '';
  items: any;
  public addRes: any[] = [];

  public siteGroups =[];

   //params: URLSearchParams = new URLSearchParams()
  constructor( private siteService: SiteFormService, private router: Router,
    private route: ActivatedRoute) {

      this.getBusinessUnitList();
      this.getPolList();
      this.getUserList();
  }

  ngOnInit() {
    if(this.page != "add"){
    this.siteForm = new FormGroup({
      siteId:new FormControl(''),
      Name: new FormControl('',Validators.required),
      SFRcheckbox:new FormControl(''),
      BUId: new FormControl('',Validators.required),
      Lattitude: new FormControl(''),
      Longitude: new FormControl(''),
      SiteInchargeId: new FormControl('',Validators.required),
      UNID:new FormControl(''),
      FileType:new FormControl(''),
      SiteUnitDetails: new FormArray([

      ]),
    });
  }

  if(this.page == "add"){
    this.siteForm = new FormGroup({
      siteId:new FormControl(''),
      Name: new FormControl('',Validators.required),
      SFRcheckbox:new FormControl(''),
      BUId: new FormControl('',Validators.required),
      Lattitude: new FormControl(''),
      Longitude: new FormControl(''),
      SiteInchargeId: new FormControl('',Validators.required),
      UNID:new FormControl(''),
      FileType:new FormControl(''),
      SiteUnitDetails: new FormArray([
        this.initSection([]),
      ]),
    });
  }
    this.route.params.forEach((params: Params) => {
      this.site_id = params['siteId'];
  });
  }

  initSection(data = []) {
    let localreserve = data['localReserve']  ? data['localReserve'] : '';
    let polid = data['polId']  ? data['polId'] : '';
    this.polId=polid;
    let sfr = data['sfr']  ? data['sfr'] : 0;
    let operationstock = data['operationStock']  ? data['operationStock'] : '';
    let totalquantity = data['localReserve'] + data['sfr'] ? data['localReserve'] + data['sfr'] : '';
    let monthlyrequirement = data['monthlyRequirement']  ? data['monthlyRequirement'] : '';
    let alert = data['alert']  ? data['alert'] : '';
    return new FormGroup({
      LocalReserve: new FormControl(localreserve),
      POLId: new FormControl(polid),
      SFR: new FormControl(sfr),
      OperationStock: new FormControl(operationstock),
      totalquantity: new FormControl(totalquantity),
      MonthlyRequirement: new FormControl(monthlyrequirement),
      Alert: new FormControl(alert),
    });
  }
  onSfrSelect(event) {
    if(event.target.checked)
    {
      this.SFRUse=true;
      this.isSFRSection=true;
    }
    else
    {
      this.isSFRSection=false;
      this.SFRUse=false;
    }
  }

  polAddedError = '';
  polDetaiilsList = [];
  isShowAddBtn = [];
  addSection(item, i) {
    console.log('itemitem', item);
    let itemValue = item.value; 
    if(this.nullalertmessage=="" && this.idalertmessage=="" && this.polId>0) {
      const control = <FormArray>this.siteForm.get('SiteUnitDetails');
      if(this.pollist.length != control.length) {
        control.push(this.initSection());
        this.isShowAddBtn[i] = true;
      } else {
        this.polAddedError ="All POLs are added";
      }
      this.polId=0;
    } else if(this.idalertmessage=="" && this.polAddedError =="") {
      this.nullalertmessage ="Please Select POL!"
    }
  }

  getLocalreserve(form) {
    return form.controls.SiteUnitDetails.controls;
  }

  removeSection(i){
   const control = <FormArray>this.siteForm.get('SiteUnitDetails');
   control.removeAt(i);
   this.polList.splice(i, 1);
   this.isShowAddBtn.splice(i, 1);
   this.idalertmessage='';
   this.nullalertmessage='';
   this.polAddedError = '';
   this.polId=1;
  }

  onOSChange(event: any, item: any) {
    this.OS=event;
    this.CalTotalQty(item);
  };
  onSFRChange(event: any, item: any) {
    this.SFR=event;
    this.CalTotalQty(item);
  };
  onLRChange(event: any,item: any) {
    this.LR=event;
    this.CalTotalQty(item);
  };
  CalTotalQty(item: any)
  {
    item.controls['totalquantity'].setValue(
      (item.controls['OperationStock'].value === '' ? 0 :Number( item.controls['OperationStock'].value))
      + (item.controls['LocalReserve'].value === '' ? 0 :Number( item.controls['LocalReserve'].value))
      + (item.controls['SFR'].value === '' ? 0 : Number(item.controls['SFR'].value)));
  }

  polErrorMsg = [];
  onSubmit(validPost) {
    if(validPost.SiteUnitDetails.length >= 1) {
      let len = validPost.SiteUnitDetails.length;
      let polDetails = validPost.SiteUnitDetails[len-1];
      if(polDetails['POLId'] == "") {
        validPost.SiteUnitDetails.splice(len-1, 1);
      }
      let i = 0;
      this.polErrorMsg = [];
      validPost.SiteUnitDetails.forEach(polDetails => {
        if(polDetails['POLId'] != "" && (polDetails['LocalReserve'] == "" || polDetails['OperationStock'] == "" || 
         polDetails['MonthlyRequirement'] == "" || polDetails['Alert'] == "")) {
          this.polErrorMsg[i] = 'Please add details for the selected POL';
        }
        i++;
      });
    }
    this.clearerror();
    this.polAddedError = '';
    this.formSubmited = true;
    this.siteRequired="Site Name is required";
    this.inchargeRequired="Site Incharge Name is required";
    this.buRequired="Business Unit is required";
    if(this.siteForm.valid && this.ErrorList.length==0 && this.polErrorMsg.length == 0) {
      validPost.uploadedFile = this.filetype;
      validPost.IsSfr=this.SFRUse;
      validPost.UNID=this.unid;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  polNotRequired = [];
  getPolDetails(id, index) {
    this.nullalertmessage="";
    this.idalertmessage="";
    this.polAddedError = "";
    this.polId=id;
    let isPresent = false;
    id ? this.polNotRequired[index] = true : this.polNotRequired[index] = false;
    for(let i = 0; i < this.polList.length+1; i++) {
      if(this.polList[i]==id) {
        this.polList[index] = 0;
        this.idalertmessage ="POL is already selected.Choose another one!";
        isPresent = true;
      }
    }
    if(!isPresent){
      //this.polList.push(id);
      this.polList[index] = id;
    }
    console.log('this.polList', this.polList);
  }

  clearerror(){
    this.error="";
  }
  clearmsgs() {
    this.polAddedError = "";
    this.nullalertmessage="";
    this.polErrorMsg = [];
    this.polNotRequired = [];
    if(this.siteForm.controls['SiteInchargeId'].hasError('required') || (this.siteForm.controls['SiteInchargeId'].touched))
    {
      this.inchargeRequired="";
    }
    if(this.siteForm.controls['Name'].hasError('required') || (this.siteForm.controls['Name'].touched))
    {
      this.siteRequired="";
    }
    if(this.siteForm.controls['BUId'].hasError('required') || (this.siteForm.controls['BUId'].touched))
    {
      this.buRequired="";
    }

    this.error="";
    this.ErrorList=[];

    const control = <FormArray>this.siteForm.get('SiteUnitDetails');
    this.CLength = control.length;
        while (  this.CLength !== 1) {
          for(let i =1 ; i<= this.CLength; i++){
            control.removeAt(i);
            this.CLength = this.CLength - 1;
         }
         this.clearmsgs();
        }
  }

  getsites(params) {
    this.siteService.getsiteList(params).then(r =>  {
        this.siteGroups = r.result;
        this.loading = false;
      })
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  keyPress(event: any) {
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    // Allow non-printable keys
    if (!charCode || charCode == 8 /* Backspace */ ) {
        return;
    }
    let typedChar = String.fromCharCode(charCode);
    // Allow numeric characters
    if (/\d/.test(typedChar)) {
        return;
    }
    // Allow the minus sign (-) if the user enters it first
    if (typedChar == "-" && event.target.value == "") {
        return;
    }
    // Allow the decimal point
    if (typedChar == ".") {
      return;
    }
    // In all other cases, suppress the event
    return false;
  }

  ngOnChanges(change) {
    //console.log('change.site.currentValue', change.site.currentValue);
    this.loading = this.page === 'add' ? false : true;
    if (change.site && change.site.currentValue) {
      this.loading = false;
      this.siteForm.controls['siteId'].setValue(change.site.currentValue.siteId);
      this.siteForm.controls['SiteInchargeId'].setValue(change.site.currentValue.siteInchargeId);
      this.siteForm.controls['Name'].setValue(change.site.currentValue.name);
      this.siteForm.controls['BUId'].setValue(change.site.currentValue.buId);
      this.siteForm.controls['Lattitude'].setValue(change.site.currentValue.lattitude);
      this.siteForm.controls['Longitude'].setValue(change.site.currentValue.longitude);
      //this.siteForm.controls['UNID'].setValue(change.site.currentValue.unid);
      this.siteForm.controls['SFRcheckbox'].setValue(change.site.currentValue.isSfr);
      this.siteForm.controls['FileType'].setValue(this.filetype);
      let siteValue = change.site.currentValue.siteUnitDetails;

      if(siteValue !=null ) {
        // let valuchanged = [];
        let items = this.siteForm.get('SiteUnitDetails') as FormArray;
        siteValue.forEach(element => {
          // valuchanged.push(this.initSection(element));
          items.push(this.initSection(element));

        });
      } else {
        let siteUnitDetails = this.siteForm.get('SiteUnitDetails') as FormArray;
        siteUnitDetails.push(this.initSection());
      }
      this.getSiteInchargeDetails(change.site.currentValue.siteInchargeId);
      let siteControl = (<FormArray>this.siteForm.get('SiteUnitDetails')).controls;
      siteControl.forEach(control => {
        this.CalTotalQty(control);
      });
    } else {
      this.loading = false;
    }
  }

  getBusinessUnitList(){

        this.siteService.getBusinessUnitList().then(r => {
          this.businessunitList = r.result;
      })
          .catch(r => {
              this.handleError(r);
          })

  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  getPolList(){

        this.siteService.getpolList().then(r => {
          this.pollist = r.result;
      })
          .catch(r => {
              this.handleError(r);
          })

  }

  getUserList(){

    //this.loading = true;
    this.siteService.getUserList().then(r => {
      this.userList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })

  }

  getSiteInchargeDetails(id){
    this.siteService.getSiteInchargeDetails(id).then(response=>{
      this.unid=response.unid;
      this.contactnumber=response.mobileNumber;
      this.email=response.email;

    })
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}