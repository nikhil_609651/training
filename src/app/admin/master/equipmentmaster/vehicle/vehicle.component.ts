import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'vehicle',
    template: `<router-outlet></router-outlet>`
})
export class VehicleComponent {
	constructor(private router: Router) {
	}
}
