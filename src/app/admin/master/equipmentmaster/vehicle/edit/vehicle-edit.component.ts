import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { VehicleFormService } from 'app/admin/master/equipmentmaster/vehicle/service/vehicle-form.service';


@Component({
  selector: 'vehicle-edit',
  template: require('./vehicle-edit.html')
})
export class VehicleEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public vehicle: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private commonService: VehicleFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.vehicle = this.commonService.getvehicle(this.id);
      });
    }

  onSave(g: any) {
    let files = g.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id',g.Id);
    formData.append('vehicleId',g.vehicleId);
    formData.append('startingOdometer', g.startingOdometer);
    formData.append('MakeAndModelId',g.makemodelId);
    formData.append('vehicleTypeId',g.vehicleTypeId);
    formData.append('maxfuelCapacity',g.maxfuelfillcapacity);
    formData.append('customerId',g.customer);
    formData.append('DepartmentId',g.department);
    formData.append('siteId',g.basestation);
    formData.append('sectionId',g.section);
    formData.append('ownershipId',g.ownershipinfo);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    this.commonService.Save(formData).then(r =>  {
      this.success = 'Vehicle Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
