import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleFormService } from 'app/admin/master/equipmentmaster/vehicle/service/vehicle-form.service';

@Component({
  selector: 'add',
  template: require('./vehicle-add.html')
})
export class VehicleAddComponent implements OnInit{
    makeList: any;
    ownershipList: any;
    customerList: any;
    departmentList: any;
    sectionList: any;
  siteList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private commonService: VehicleFormService) {
  }
  ngOnInit() {
    this.page = 'add';
  }
  getSiteList(){
        this.commonService.getBaseStationList().then(r => {
          this.siteList = r;
      })
          .catch(r => {
              this.handleError(r);
          });

  }
  getSectionList(){
    this.commonService.getSectionList().then(r => {
      this.sectionList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getDepartmentList(){
    this.commonService.getDepartmentList().then(r => {
      this.departmentList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getCustomerList(){
    this.commonService.getCustomerList().then(r => {
      this.customerList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getOwnershipList(){
    this.commonService.getOwnershipInfoList().then(r => {
      this.ownershipList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getMakeList(){
    this.commonService.getMakeModelList().then(r => {
      this.makeList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
  onSave(g: any) {
    let files = g.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id','0');
    formData.append('vehicleId',g.vehicleId);
    formData.append('startingOdometer', g.startingOdometer);
    formData.append('MakeAndModelId',g.makemodelId);
    formData.append('vehicleTypeId',g.vehicleTypeId);
    formData.append('maxfuelCapacity',g.maxfuelfillcapacity);
    formData.append('customerId',g.customer);
    formData.append('DepartmentId',g.department);
    formData.append('siteId',g.basestation);
    formData.append('sectionId',g.section);
    formData.append('ownershipId',g.ownershipinfo);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    this.commonService.Save(formData).then(r =>  {
      this.success = 'Vehicle Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
