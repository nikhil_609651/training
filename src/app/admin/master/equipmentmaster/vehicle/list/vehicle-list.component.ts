import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VehicleFormService } from 'app/admin/master/equipmentmaster/vehicle/service/vehicle-form.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';


@Component({
    selector: 'vehicle-list',
    encapsulation: ViewEncapsulation.None,
    template: require('./vehicle-list.html')
  })
  export class VehicleListComponent implements OnInit{
  selectedmakemodelAll: any;
  selectedcustomerAll: any;
  selectedsiteAll: any;
  siteGroups: any;
  selectedAllCustFilter: any;
  customerGroups: any;
  vehicleid: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  vehicletypeGroups: any;
  makemodelGroups: any;
  selectedAll: any;
  attachmentGroup: any[];
  API_URL: string;
  attachments: any;
  attachmentmessage: string;
  deletemessage: string;
  vehicle_deleted_id: any;

  checkedsite: string[] = [];
  checkedcust: string[] = [];
  checkedmake: string[] = [];

    viewForm: FormGroup;

    public loading: boolean;
    public rows:Array<any> = [];
    public vehicle: any= [];
    public totalfeilds = 0;
    public page: number = 1;
    public itemsPerPage: number = 3;
    public maxSize: number = 5;
    public numPages: number = 2;
    public length: number = 5;
    public next = '';
    public start: number = 1;
    checked: string[] = [];

        // For headers
  public columns:Array<any> = [


       { title: 'ID', name: 'Id', sort: true,  },
       { title: 'Vehicle ID', name: 'VehicleId', sort: true,  },
       { title: 'Max-Fuel Fill Capacity', name: 'MaxFuelCapacity', sort: true, },
       { title: 'Vehicle Make & Model', name: 'MakeAndModel', sort: false,filter:false },
       { title: 'Site', name: 'Site', sort: true,filter:true },
       { title: 'Customer', name: 'Customer', sort: true,filter:true },
       { title: 'Actions', name: 'actions', sort: false }
     ];
     public config:any = {
      paging: true,
      sorting: {columns: this.columns},
      filtering: {filterString: ''},
      className: ['table-bordered']
    };
     params: URLSearchParams = new URLSearchParams();

     privileges : any = {
      isAdd: false,
      isDelete : false,
      isEdit : false,
      isView : false
    }
    privilegeSubscription : Subscription;
    
     constructor(private router: Router,
        private Service: VehicleFormService,
        private fb: FormBuilder, private _sharedService : SharedService,
        private configuration: Configuration) {

          /**user privileges**/	
          this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	          this.privileges = privileges;
          });
          this.itemsPerPage = configuration.itemsPerPage;
          this.params.set('limit', configuration.itemsPerPage.toString());
          this.params.set('MakeAndModelFilter', '');
          this.params.set('CustomerFilter', '');
          this.params.set('SiteFilter', '');
          this.params.set('UserId', localStorage.getItem('user_nameId'));
          this.rows = configuration.rows;
          this.viewForm = fb.group({
                        'vehicleId': [''],
                        'startingOdometer': [''],
                        'makemodel':[''],
                        'vehicletype': [''],
                        'maxfuelfillcapacity': [''],
                        'customer': [''],
                        'department': [''],
                        'basestation': [''],
                        'section': [''],
                        'ownershipinfo': [''],
          });
          var vForm = this.viewForm;
    }

    ngOnInit(){
      this.getVehicleList(this.params);
      this.getVehicleMakeModelList();
      this.getCustomerList();
      let userId = localStorage.getItem('user_nameId');
      this.getSiteList(userId);
    }

    ngOnDestroy() {
      this.privilegeSubscription.unsubscribe();
    }

       // For getting Vehicle List
    getVehicleList(params: any) {
    this.loading = true;
    this.Service.getVehicleList(params).then(response => {
      this.vehicle = response['result'];
      if(this.vehicle.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.Service.getVehicleList(params).then(response => {
      this.vehicle = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }
// For listing parameters for Filteration
  getVehicleMakeModelList(){
    this.Service.getMakeModelList().then(r =>  {
      this.makemodelGroups = r['result'];
     // this.loading = false;
    });
  }
  getCustomerList(){
    this.Service.getCustomerFilterList().then(r =>  {
      this.customerGroups = r;
     // this.loading = false;
    });
  }
  getSiteList(userId){
    this.Service.getSiteFilterList(userId).then(r =>  {
      this.siteGroups = r;
     // this.loading = false;
    });
  }
  addvehicle() {
    this.router.navigate(['./home/master/vehicle/add']);
  }

  edit(id) {

    this.router.navigate(['./home/master/vehicle/edit/', id]);
  }

// View Details
  view(data){
    console.log(data)
    if (data.id != 0) {
      this.viewForm.controls['vehicletype'].setValue(data.vehicleType.vehicleTypeName);
      this.viewForm.controls['vehicleId'].setValue(data.vehicleId);
      //this.viewForm.controls['makemodel'].setValue(data.vehicleMakeAndModelId);
      this.viewForm.controls['startingOdometer'].setValue(data.startingOdoMeter);
      this.viewForm.controls['maxfuelfillcapacity'].setValue(data.maxFuelCapacity);
      this.viewForm.controls['customer'].setValue(data.customer.firstName + ' ' +  data.customer.expansion);
      this.viewForm.controls['department'].setValue(data.department.name);
      this.viewForm.controls['basestation'].setValue(data.site.name);
      this.viewForm.controls['section'].setValue(data.section.sectionName);
      this.viewForm.controls['ownershipinfo'].setValue(data.ownership.name);
    }
}


// For Deleting Vehicle
deletevehicle(id){
  this.vehicle_deleted_id = id;
}
deletemessageclear(id){
  this.deletemessage = '';
  this.vehicle_deleted_id = id;
}

deletevehicleconfirm(data){
    this.Service.delete(  this.vehicle_deleted_id).then(r =>  {
      this.getVehicleList(this.params);
      this.deletemessage = 'Vehicle Deleted Successfully';
    }).catch(r =>  {
      this.handleError(r);
      this.deletemessage = r.name[0];
    });
}
// For Listing Attachments
attachment(id){
this.vehicleid=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadVehicleFiles?FileId=';

  this.Service.getallattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){

  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
   console.log( this.attachmentGroup)
}
else{
 this.getVehicleList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}

viewclose(){
  setTimeout(function () {
      jQuery('#ViewDetails').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
    jQuery('#ViewAttachmentModal').modal('toggle');
  }.bind(this), 0);

}
public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {


    if(config.status ==0){
    config.status=1;
    }
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());
    var sortParam = '';

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getVehicleList(this.params);
  }

// Filteration

selectAll(item, event) {
  if(item === 'Site'){
      this.checkArr = [];
      for (var i = 0; i < this.siteGroups.length; i++) {
        this.siteGroups[i].selected = this.selectedsiteAll;
        if(event.target.checked){
            this.checkArr.push(this.siteGroups[i].siteId.toString());
        } else {
            this.checkArr = [];
        }
      }
       this.params.set('SiteFilter', this.checkArr.toString());
  }

  if(item === 'Customer'){
    this.checkCustomerArr=[]
    for (var i = 0; i < this.customerGroups.length; i++) {
      this.customerGroups[i].selected = this.selectedcustomerAll;
      if(event.target.checked){
          this.checkCustomerArr.push(this.customerGroups[i].customerId.toString());
      } else {
          this.checkCustomerArr = [];
      }
    }
     this.params.set('CustomerFilter', this.checkCustomerArr.toString());
  }

  if(item === 'MakeModel'){
    this.checkMakeModelArr=[]
    for (var i = 0; i < this.makemodelGroups.length; i++) {
      this.makemodelGroups[i].selected = this.selectedmakemodelAll;
      if(event.target.checked){
          this.checkMakeModelArr.push(this.makemodelGroups[i].id.toString());
      } else {
          this.checkMakeModelArr = [];
      }
    }
     this.params.set('MakeModelFilter', this.checkMakeModelArr.toString());
  }
}
checkArr = [];
checkCustomerArr=[];
checkMakeModelArr=[];
checkIfAllSelected(option, event,item) {
if(item=='Site')
{
this.selectedsiteAll = this.siteGroups.every(function(item:any) {
  return item.selected == true;
})
var key = event.target.value.toString();
var index = this.checkArr.indexOf(key);
if(event.target.checked) {
  this.checkArr.push(event.target.value);
} else {
  this.checkArr.splice(index,1);
}
  this.params.set('SiteFilter', this.checkArr.toString());
}
if(item === 'Customer'){
this.selectedcustomerAll = this.customerGroups.every(function(item:any) {
  return item.selected == true;
})
var key = event.target.value.toString();
var index = this.checkCustomerArr.indexOf(key);
if(event.target.checked) {
  this.checkCustomerArr.push(event.target.value);
} else {
  this.checkCustomerArr.splice(index,1);
}
  this.params.set('CustomerFilter', this.checkCustomerArr.toString());
}
if(item === 'MakeModel'){
  this.selectedmakemodelAll = this.makemodelGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkMakeModelArr.indexOf(key);
  if(event.target.checked) {
    this.checkMakeModelArr.push(event.target.value);
  } else {
    this.checkMakeModelArr.splice(index,1);
  }
    this.params.set('MakeModelFilter', this.checkMakeModelArr.toString());
  }
}
 // For Applying Filtering to List
 apply(item){
  this.getVehicleList(this.params);
   if(item === 'Site'){
    setTimeout(function () {
      jQuery('#ViewSiteModal').modal('toggle');
    }.bind(this) , 0);
   }
   if(item === 'Customer'){
    setTimeout(function () {
      jQuery('#ViewCustomerModal').modal('toggle');
    }.bind(this) , 0);
   }
   if(item === 'MakeModel'){
    setTimeout(function () {
      jQuery('#ViewMakeModal').modal('toggle');
    }.bind(this) , 0);
   }
   jQuery('ViewSiteModal').modal('hide');
   jQuery('ViewCustomerModal').modal('hide');
   jQuery('body').removeClass('modal-open');
   jQuery('.modal-backdrop').remove();
  }
// Filteration


// For Deleting Attachments



deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){

    this.Service.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.vehicleid);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}


// For Sorting
Sort(param,order){
  //this.params.set('ordering', sortParam);

  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getVehicleList(this.params);

    }


  private handleError(e: any) {
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
}
  }