import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';
@Injectable()

export class VehicleFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public attachmenturl ="";
    public downloadUrl = "";
    public deleteattachmentUrl = "";
    // for dropdowns
    public customerUrl = '';
    public customerFilterUrl = '';
    public departmentUrl = '';
    public basestationUrl = '';
    public siteFilterUrl = '';
    public sectionUrl = '';
    public ownershipinfoUrl = '';
    public vehicletypeUrl= '';
    public viewurl = '';
    public makemodelurl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

          this.listUrl = _configuration.ServerWithApiUrl + 'Masters/Vehicle/';
          this.saveUrl = _configuration.ServerWithApiUrl + 'Masters/SaveVehicle';
         this.deleteUrl = _configuration.ServerWithApiUrl + 'Masters/DeleteVehicleByID?id=';
          // tslint:disable-next-line:max-line-length
          this.attachmenturl = _configuration.ServerWithApiUrl + 'Masters/GetAllVehicleAttachments?id=';
          this.downloadUrl = _configuration.ServerWithApiUrl + 'Masters/DownloadVehicleFiles?FileId=';
          this.viewurl = _configuration.ServerWithApiUrl+'Masters/GetVehicleByID?id=';
          // tslint:disable-next-line:max-line-length
          this.deleteattachmentUrl = _configuration.ServerWithApiUrl + 'Masters/DeleteVehicleAttachments?Id=';
          this.customerUrl = _configuration.ServerWithApiUrl + 'Masters/Customer/';
          this.customerFilterUrl = _configuration.ServerWithApiUrl + 'Masters/GetVehicleCustomerFilter/';
          this.departmentUrl = _configuration.ServerWithApiUrl + 'Masters/Department/';
          this.basestationUrl = _configuration.ServerWithApiUrl + 'Masters/Site/';
          this.siteFilterUrl = _configuration.ServerWithApiUrl + 'Masters/GetVehicleSiteFilter/';
          this.sectionUrl = _configuration.ServerWithApiUrl + 'Masters/Section/';
          this.ownershipinfoUrl = _configuration.ServerWithApiUrl + 'Masters/Ownership/';
          this.vehicletypeUrl = _configuration.ServerWithApiUrl + 'Masters/VehicleType/';
          this.makemodelurl = _configuration.ServerWithApiUrl+'Masters/VehicleMakeAndModel/';
        }

        // List API for Vehicle List
    getVehicleList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}


// List API's for Dropdowns
getVehicleTypeList(): Promise<any> {
    return this.authHttp.get(this.vehicletypeUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getCustomerList(): Promise<any> {
    return this.authHttp.get(this.customerUrl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getCustomerFilterList(): Promise<any> {
    return this.authHttp.get(this.customerFilterUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getDepartmentList(): Promise<any> {
    return this.authHttp.get(this.departmentUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getBaseStationList(): Promise<any> {
    return this.authHttp.get(this.basestationUrl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getSiteFilterList(userId): Promise<any> {
    return this.authHttp.get(this.siteFilterUrl+ '?UserId=' + userId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getSectionList(): Promise<any> {
    return this.authHttp.get(this.sectionUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getOwnershipInfoList(): Promise<any> {
    return this.authHttp.get(this.ownershipinfoUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getMakeModelList(): Promise<any> {
    return this.authHttp.get(this.makemodelurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
//   Save API for Aircraft
Save(bu: any): Promise<any>  {

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
    }

// For Delating Aircraft
    delete(id: any) {

        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    // For getting all attachments
    getallattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    // For View Deatils
    getvehicle(id: string) {
        return this.authHttp.get(this.viewurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    // For deleting attachments
    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
private handleError(error: any) {
    return Promise.reject(error.json());
}
}