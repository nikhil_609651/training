import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { VehicleFormService } from 'app/admin/master/equipmentmaster/vehicle/service/vehicle-form.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
    selector: 'vehicle-form',
    template: require('./vehicle-form.html')
  })
  export class VehicleFormComponent {
  userId: string;
  savebutton: boolean = true;
  makemodelList: any;
  makemodelReq: string;
  odometerReq: string;
  vehicleIdReq: string;
  aircrafttypeReq: string;
  ownershipReq: string;
  sectionReq: string;
  basestationReq: string;
  departmentReq: string;
  customerReq: string;
  capacityReq: string;
  callsignReq: string;
  flightReq: string;
  vehicleTypeReq: string = "";
  ownershipInfoReq: string = "";
    public loading: boolean;
    customerList: any;
    departmentList: any;
    basestationList: any;
    sectionList: any;
    ownershipinfoList: any;
    vehicletypeList: any;
    filelength: number;
    ErrorList=[];
    filetype= [];
    filelist= [];
    @ViewChild('fileInput') fileInputVariable: any;
    @Input() vehicle;
    @Input() error;
    @Input() page: string;
    @Output() saved = new EventEmitter();
    vehicleForm:FormGroup;
  public formSubmited = false;
  public vehicle_id = '';
  image= '';
  params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
    private vehicleService: VehicleFormService, private router: Router,
    private issuerService: IssuerFormService,
    private route: ActivatedRoute) {
      this.userId=localStorage.getItem('user_nameId');
        this.vehicleForm = fb.group({
            'Id': [''],
            'vehicleId': ['',Validators.compose([Validators.required])],
            'vehicleTypeId': ['',Validators.compose([Validators.required])],
            'startingOdometer': ['',Validators.compose([Validators.required])],
            'makemodelId': ['',Validators.compose([Validators.required])],
            'maxfuelfillcapacity': ['',Validators.compose([Validators.required])],
            'customer': ['',Validators.compose([Validators.required])],
            'department': ['',Validators.compose([Validators.required])],
            'basestation': ['',Validators.compose([Validators.required])],
            'section': ['',Validators.compose([Validators.required])],
            'ownershipinfo': ['',Validators.compose([Validators.required])],
            'BarCode': [''],
            'FileType':[],
          });
          var cForm = this.vehicleForm;
    }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.vehicle_id = params['id'];
      });
      this.getCustomerList();
      this.getDepartmentList();
      this.getBaseStationList();
      this.getSectionList();
      this.getOwnershipInfoList();
      this.getVehicleTypeList();
      this.getMakeModelList();
  }
    ngOnChanges(change) {
      this.loading = this.page == 'add' ? false : true;
      if (change.vehicle && change.vehicle.currentValue) {
        this.loading = false;
        this.vehicleForm.controls['Id'].setValue(change.vehicle.currentValue.id);
        this.vehicleForm.controls['vehicleId'].setValue(change.vehicle.currentValue.vehicleId);
        this.vehicleForm.controls['vehicleTypeId'].setValue(change.vehicle.currentValue.typeId);
        this.vehicleForm.controls['makemodelId'].setValue(change.vehicle.currentValue.makeAndModelId);
        this.vehicleForm.controls['startingOdometer'].setValue(change.vehicle.currentValue.startingOdoMeter);
        this.vehicleForm.controls['maxfuelfillcapacity'].setValue(change.vehicle.currentValue.maxFuelCapacity);
        this.vehicleForm.controls['customer'].setValue(change.vehicle.currentValue.customerId);
        this.vehicleForm.controls['department'].setValue(change.vehicle.currentValue.departmentId);
        this.vehicleForm.controls['basestation'].setValue(change.vehicle.currentValue.siteId);
        this.vehicleForm.controls['section'].setValue(change.vehicle.currentValue.sectionId);
        this.vehicleForm.controls['ownershipinfo'].setValue(change.vehicle.currentValue.ownershipId);
        this.vehicleForm.controls['BarCode'].setValue(change.vehicle.currentValue.barCode);
        this.vehicleForm.controls['FileType'].setValue(this.filetype);
      } else{
        this.loading = false;
      }
    }


    updated($event, control) {
        const files = $event.target.files || $event.srcElement.files;
        var reader: FileReader = new FileReader();
        var file: File = files[0];

        reader.onloadend = (e) => {
          this.image = btoa(reader.result);
          control.setValue(btoa(reader.result));
        }
        reader.readAsBinaryString(file);
      }
      onChange(event) {
        this.ErrorList=[];
        this.filelist = [];
        this.filetype = [];
        this.filelist = <Array<File>>event.target.files;
        for(let i = 0; i <this.filelist.length; i++) {
          let fSize = this.filelist[i].size;
          let fName = this.filelist[i].name;
          let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
          if(fSize > 2097152)
          {
            this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
          }
          else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt")
          {
            this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
          }
          else
          {
            this.ErrorList=[];
            this.filetype.push(this.filelist[i]);
            this.filelength= this.filetype.length;
          }
        }
      }
      onRemoveFile(event) {
        this.filelist = [];
        this.filetype = [];
        this.ErrorList= [];
      }

      onSubmit(validPost) {
        this.vehicleIdReq="Vehicle Id is required";
        this.odometerReq="Starting odometer is required";
        this.makemodelReq="Make and Model is required";
        this.capacityReq="Fuel capacity is required";
        this.basestationReq="Site is required";
        this.customerReq="Customer is required";
        this.departmentReq="Department is required";
        this.sectionReq="Section is required";
        this.vehicleTypeReq = "Vehicle Type is required";
        this.ownershipInfoReq = "Ownership Info is required";
        this.clearerror();
        this.formSubmited = true;
        if(this.vehicleForm.valid && this.ErrorList.length==0) {
          validPost.uploadedFile = this.filetype;
          //this.savebutton = false;
          this.saved.emit(validPost);
        } else{
          jQuery('form').find(':input.ng-invalid:first').focus();
        }
      }
      clearerror(){
        this.error="";
      }
      clearmsgs(){
        if(this.vehicleForm.controls['vehicleId'].hasError('required') || (this.vehicleForm.controls['vehicleId'].touched))
        {
          this.vehicleIdReq="";
        }
        if(this.vehicleForm.controls['makemodelId'].hasError('required') || (this.vehicleForm.controls['makemodelId'].touched))
        {
          this.makemodelReq="";
        }
        if(this.vehicleForm.controls['startingOdometer'].hasError('required') || (this.vehicleForm.controls['startingOdometer'].touched))
        {
          this.odometerReq="";
        }
        if(this.vehicleForm.controls['maxfuelfillcapacity'].hasError('required') || (this.vehicleForm.controls['maxfuelfillcapacity'].touched))
        {
          this.capacityReq="";
        }
        if(this.vehicleForm.controls['customer'].hasError('required') || (this.vehicleForm.controls['customer'].touched))
        {
          this.customerReq="";
        }
        if(this.vehicleForm.controls['department'].hasError('required') || (this.vehicleForm.controls['department'].touched))
        {
          this.departmentReq="";
        }
        if(this.vehicleForm.controls['basestation'].hasError('required') || (this.vehicleForm.controls['basestation'].touched))
        {
          this.basestationReq="";
        }
        if(this.vehicleForm.controls['section'].hasError('required') || (this.vehicleForm.controls['section'].touched))
        {
          this.sectionReq="";
        }
        if(this.vehicleForm.controls['vehicleTypeId'].hasError('required') || (this.vehicleForm.controls['vehicleTypeId'].touched))
        {
          this.vehicleTypeReq="";
        }
        if(this.vehicleForm.controls['ownershipinfo'].hasError('required') || (this.vehicleForm.controls['ownershipinfo'].touched))
        {
          this.ownershipInfoReq="";
        }
        this.error="";
        this.ErrorList=[];
      }
      // For getting Customer List
   getCustomerList(){
    this.vehicleService.getCustomerList().then(r =>  {
        this.customerList = r['result'];
      });
  }
  //For getting Department List
  getDepartmentList(){
    this.vehicleService.getDepartmentList().then(r =>  {
        this.departmentList = r['result'];
      });
  }

  //For getting BaseStation List
  getBaseStationList(){
    // this.vehicleService.getBaseStationList().then(r =>  {
    //     this.basestationList = r['result'];
    //   });

      this.issuerService.getSiteList(this.userId).then(r => {
                  this.basestationList = r;
              })
                  .catch(r => {
                      this.handleError(r);
                  });
  }

  //For getting Section List
  getSectionList(){
    this.vehicleService.getSectionList().then(r =>  {
        this.sectionList = r['result'];
      });
  }

  //For getting OwnershipInfo List
  getOwnershipInfoList(){
    this.vehicleService.getOwnershipInfoList().then(r =>  {
        this.ownershipinfoList = r['result'];
      });
  }

  getVehicleTypeList(){
    this.vehicleService.getVehicleTypeList().then(r =>  {
      this.vehicletypeList = r['result'];
    });
  }

  getMakeModelList(){
    this.vehicleService.getMakeModelList().then(r =>  {
      this.makemodelList = r['result'];
    });
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
   // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
      handleError(e: any) {
          this.error = e;
          let detail = e.detail;
          if(detail && detail == 'Signature has expired.'){
            this.router.navigate(['./']);
          }
      }
  }