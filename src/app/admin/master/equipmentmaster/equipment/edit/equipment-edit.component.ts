import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EquipmentFormService } from 'app/admin/master/equipmentmaster/equipment/service/equipment-form.service';

@Component({
    selector: 'equipment-edit',
    template: require('./equipment-edit.html')
  })
  export class EquipmentEditComponent implements OnInit{
  id: any;
  public error = {};
  public success = '';
  public equipment: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private service: EquipmentFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

          this. id = params['id'];
                this.equipment = this.service.getdetails(this.id);
      });
    }
    
    onSave(g: any) {
      let files = g.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
          formData.append('FileType', files[i], files[i]['name']);
      }
    formData.append('Id',this.id);
    formData.append('equipmentId',g.equipmentId);
    formData.append('equipmenttypeId',g.equipmenttypeId);
    formData.append('regNo',g.regNo);
    formData.append('equipmentName',g.name);
    formData.append('StartingOdometer', g.StartingOdoMeter);
    formData.append('maxfuelCapacity',g.maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('polId',g.polId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId',g.ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");

      this.service.Save(formData).then(r =>  {
        this.success = 'Equipment Updated Successfully!';
        jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
        setTimeout(function() {
           this.success = '';
           this.router.navigate(['./home/master/equipment']);
        }.bind(this), 3000);
      }).catch(r =>  {
        this.handleError(r);
      })
      }
    private handleError(e: any) {
        this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
    }
  }