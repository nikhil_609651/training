import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class EquipmentFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public equipmenturl = '';
    public customerurl = '';
    public customerFilterurl = '';
    public siteurl = '';
    public siteFilterurl = '';
    public departmenturl = '';
    public polurl = '';
    public ownershipurl = '';
    public sectionurl = '';
    public equipmenttypeurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Equipment/';
        this.customerurl = _configuration.ServerWithApiUrl+'Masters/Customer/';
        this.customerFilterurl = _configuration.ServerWithApiUrl+'Masters/GetEquipmentCustomerFilter/';
        this.siteurl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.siteFilterurl = _configuration.ServerWithApiUrl+'Masters/GetEquipmentSiteFilter/';
        this.departmenturl = _configuration.ServerWithApiUrl+'Masters/Department/';
        this.sectionurl = _configuration.ServerWithApiUrl+'Masters/Section/';
      //  this.polurl = _configuration.ServerWithApiUrl+'Masters/POL/';
      this.polurl = _configuration.ServerWithApiUrl+'Masters/GetLubricantPOL?Lubricant=';

        this.ownershipurl = _configuration.ServerWithApiUrl+'Masters/Ownership/';
        this.equipmenttypeurl = _configuration.ServerWithApiUrl+'Masters/EquipmentType/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveEquipment';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateEquipment';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteEquipmentByID?id=';
        this.equipmenturl = _configuration.ServerWithApiUrl+'Masters/GetEquipmentByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllEquipmentAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteEquipmentAttachments?Id=';
    }
    
   
// List API for Equipment
getequipmentList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

getCustomerList(params): Promise<any> {
    return this.authHttp.get(this.customerurl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getCustomerFilterList(): Promise<any> {
    return this.authHttp.get(this.customerFilterurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSiteList(): Promise<any> {
    return this.authHttp.get(this.siteurl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSiteFilterList(userId): Promise<any> {
    return this.authHttp.get(this.siteFilterurl+ '?UserId=' + userId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSectionList(): Promise<any> {
    return this.authHttp.get(this.sectionurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getDepartmentList(): Promise<any> {
    return this.authHttp.get(this.departmenturl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getPolList(lubricant): Promise<any> {
    return this.authHttp.get(this.polurl + lubricant)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getEquipmentTypeList(): Promise<any> {
    return this.authHttp.get(this.equipmenttypeurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getOwnershipList(): Promise<any> {
    return this.authHttp.get(this.ownershipurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
//   Save API for generator
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

private post(g: any): Promise<any> {
    const formData = new FormData();
    formData.append('Id', g.Id);
    formData.append('equipmentId', g.equipmentId);
    formData.append('equipmentType', g.equipmenttypeId);
    formData.append('regNo', g.regNo);
    formData.append('name', g.name);
    formData.append('StartingOdometer', g.StartingOdoMeter);
    formData.append('polId',g.polId);
    formData.append('maxfuelCapacity',g.maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId',g.ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deleteequipment(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getallequipmentattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getdetails(id: string) {
    return this.authHttp.get(this.equipmenturl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

private handleError(error: any) {
    return Promise.reject(error.json());
}
}
