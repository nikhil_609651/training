import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { EquipmentFormService } from 'app/admin/master/equipmentmaster/equipment/service/equipment-form.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'equipment-form',
  template: require('./equipment-form.html')
})

export class EquipmentFormComponent {
  userId: string;
  lubricant: boolean;
  equipmenttypeIdReq: string;
  savebutton: boolean = true;
  nameReq: string;
  regnoReq: string;
  polList: any;
  equipmenttypeList: any = [];
  makeList: any;
  equipmentIdReq: string;
  odometerReq:string;
  maxfuelcapacityReq:string;
  makeReq: string;
  siteReq: string;
  customerReq: string;
  departmentReq: string;
  sectionReq: string;
  ownershipReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @Input() equipment;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  equipmentForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public siteList: Array<string>;
  public sectionList: Array<string>;
  public departmentList: Array<string>;
  public ownershipList: Array<string>;
  public customerList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  // tslint:disable-next-line:variable-name
  public equipment_id = '';
  public equipmentGroups =[];
   params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
      private equipmentService: EquipmentFormService, private router: Router,
      private issuerService: IssuerFormService,
      private route: ActivatedRoute) {
    this.userId=localStorage.getItem('user_nameId');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.getSiteList();
    this.getSectionList();
    this.getDepartmentList();
    this.getOwnershipList();
    this.getCustomerList(this.params);
    this.getPolList();
    this.getEquipmenttypeList();
    this.equipmentForm = fb.group({
      'Id': [''],
      'equipmentId': ['', Validators.compose([Validators.required])],
      'name': ['',Validators.compose([Validators.required])],
      'regNo': ['',Validators.compose([Validators.required])],
      'StartingOdoMeter':  [0],
      'maxfuelCapacity': [0],
      'siteId': ['', Validators.compose([Validators.required])],
      'customerId': ['',Validators.compose([Validators.required])],
      'departmentId': ['', Validators.compose([Validators.required])],
      'ownershipId': ['',Validators.compose([Validators.required])],
      'equipmenttypeId': ['',Validators.compose([Validators.required])],
      'polId':[""],
      'sectionId': ['',Validators.compose([Validators.required])],
      'BarCode': [''],
      'FileType':[''],
    });
    var cForm = this.equipmentForm;
  }

  ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.equipment_id = params['id'];
      });
  }

  // for getting SiteList
  getSiteList(){
      this.issuerService.getSiteList(this.userId).then(r => {
        this.siteList = r;
      })
      .catch(r => {
        this.handleError(r);
      });
  }

  getSectionList() {
    this.equipmentService.getSectionList().then(r => {
      this.sectionList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      });

  }

  getPolList() {
    this.lubricant = false;
      this.equipmentService.getPolList( this.lubricant).then(r => {
          this.polList = r;
    })
        .catch(r => {
            this.handleError(r);
        });

  }

  getEquipmenttypeList() {
      this.equipmentService.getEquipmentTypeList().then(r => {
        let equipments = r.result;
        equipments.forEach(equipment => {
          if(equipment.name.toUpperCase() != 'VEHICLE' && equipment.name.toUpperCase() != 'AIRCRAFT' && equipment.name.toUpperCase() != 'GENERATOR') {
            this.equipmenttypeList.push(equipment);
          }
        });
    })
        .catch(r => {
            this.handleError(r);
        });
  }

  getDepartmentList() {
      this.equipmentService.getDepartmentList().then(r => {
        this.departmentList = r.result;
    })
        .catch(r => {
            this.handleError(r);
        });

  }

  getCustomerList(params) {
    this.equipmentService.getCustomerList(params).then(r => {
      this.customerList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      });

  }

  getOwnershipList() {
      this.equipmentService.getOwnershipList().then(r => {
        this.ownershipList = r.result;
    })
        .catch(r => {
            this.handleError(r);
        });

  }

  // for getting equipment List
  getequipment(params){
    this.equipmentService.getequipmentList(params).then(r =>  {
        this.equipmentGroups = r.result;
      });
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
  // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.equipment && change.equipment.currentValue) {
      this.loading = false;
      this.equipmentForm.controls['Id'].setValue(change.equipment.currentValue.id);
      this.equipmentForm.controls['equipmentId'].setValue(change.equipment.currentValue.equipmentId);
      this.equipmentForm.controls['regNo'].setValue(change.equipment.currentValue.registrationNo);
      this.equipmentForm.controls['name'].setValue(change.equipment.currentValue.equipmentName);
      this.equipmentForm.controls['equipmenttypeId'].setValue(change.equipment.currentValue.typeId);
      this.equipmentForm.controls['StartingOdoMeter'].setValue(change.equipment.currentValue.startingOdoMeter);
      this.equipmentForm.controls['maxfuelCapacity'].setValue(change.equipment.currentValue.maxFuelCapacity);
      this.equipmentForm.controls['siteId'].setValue(change.equipment.currentValue.siteId);
      if(change.equipment.currentValue.polId) {
        this.equipmentForm.controls['polId'].setValue(change.equipment.currentValue.polId);
      }
      this.equipmentForm.controls['customerId'].setValue(change.equipment.currentValue.customerId);
      this.equipmentForm.controls['sectionId'].setValue(change.equipment.currentValue.sectionId);
      this.equipmentForm.controls['ownershipId'].setValue(change.equipment.currentValue.ownershipId);
      this.equipmentForm.controls['departmentId'].setValue(change.equipment.currentValue.departmentId);
      this.equipmentForm.controls['BarCode'].setValue(change.equipment.currentValue.barCode);
      this.equipmentForm.controls['FileType'].setValue(this.filetype);

    } else {
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.equipmentIdReq="Equipment Id is required";
    this.equipmenttypeIdReq="Equipment type is Required";
    this.regnoReq="Reg No is required";
    this.nameReq="Equipment Name is required";
    this.maxfuelcapacityReq="Max Fuel Capacity is required";
    this.odometerReq="Odometer is required";
    this.siteReq="Site is required";
    this.sectionReq="Section is required";
    this.departmentReq="Department is required";
    this.ownershipReq="Ownership Info is required";
    this.customerReq="Customer is required";
    this.makeReq="Make and Model is required";
    if(validPost.ownershipId === '' || validPost.ownershipId === null) {

    }
    this.clearerror();
    this.formSubmited = true;
    if(!validPost.StartingOdoMeter) {
      validPost.StartingOdoMeter = 0;
    }
    if(!validPost.maxfuelCapacity) {
      validPost.maxfuelCapacity = 0;
    }
    if(this.equipmentForm.valid && this.ErrorList.length=== 0) {
     validPost.uploadedFile = this.filetype;
     //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror() {
    this.error='';
  }

  // for Uploading Files
  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152) {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      } else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt") {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      } else {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  clearmsgs() {
    if(this.equipmentForm.controls['equipmentId'].hasError('required') || (this.equipmentForm.controls['equipmentId'].touched)) {
      this.equipmentIdReq="";
    }
    if(this.equipmentForm.controls['regNo'].hasError('required') || (this.equipmentForm.controls['regNo'].touched)) {
      this.regnoReq="";
    }
    if(this.equipmentForm.controls['maxfuelCapacity'].hasError('required') || (this.equipmentForm.controls['maxfuelCapacity'].touched)) {
      this.maxfuelcapacityReq="";
    }
    if(this.equipmentForm.controls['StartingOdoMeter'].hasError('required') || (this.equipmentForm.controls['StartingOdoMeter'].touched)) {
      this.odometerReq="";
    }
    if(this.equipmentForm.controls['equipmenttypeId'].hasError('required') || (this.equipmentForm.controls['equipmenttypeId'].touched)) {
      this.equipmenttypeIdReq='';
    }
    if(this.equipmentForm.controls['siteId'].hasError('required') || (this.equipmentForm.controls['siteId'].touched)) {
      this.siteReq="";
    }
    if(this.equipmentForm.controls['sectionId'].hasError('required') || (this.equipmentForm.controls['sectionId'].touched)) {
      this.sectionReq="";
    }
    if(this.equipmentForm.controls['customerId'].hasError('required') || (this.equipmentForm.controls['customerId'].touched)) {
      this.customerReq="";
    }
    if(this.equipmentForm.controls['name'].hasError('required') || (this.equipmentForm.controls['name'].touched)) {
      this.nameReq="";
    }
    if(this.equipmentForm.controls['ownershipId'].hasError('required') || (this.equipmentForm.controls['ownershipId'].touched)) {
      this.ownershipReq="";
    }
    this.error="";
    this.ErrorList=[];
  }

  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}