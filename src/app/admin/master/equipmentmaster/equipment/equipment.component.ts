import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'equipment',
    template: `<router-outlet></router-outlet>`
})
export class EquipmentComponent {
	constructor(private router: Router) {
	}
}
