import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EquipmentFormService } from 'app/admin/master/equipmentmaster/equipment/service/equipment-form.service';

@Component({
  selector: 'add',
  template: require('./equipment-add.html')
})
export class EquipmentAddComponent implements OnInit{
    equipmenttypeList: any;
    polList: any;
    makeList: any;
    ownershipList: any;
    customerList: any;
    departmentList: any;
    sectionList: any;
  siteList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private commonService: EquipmentFormService) {
  }
  ngOnInit() {
    this.page = 'add';
  }
  getSiteList(){
        this.commonService.getSiteList().then(r => {
          this.siteList = r;
      })
          .catch(r => {
              this.handleError(r);
          });

  }
//   getPolList(){
//     this.commonService.getPolList(true).then(r => {
//       this.polList = r;
//   })
//       .catch(r => {
//           this.handleError(r);
//       });

// }
getEquipmentTypeList(){
    this.commonService.getEquipmentTypeList().then(r => {
      this.equipmenttypeList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
  getSectionList(){
    this.commonService.getSectionList().then(r => {
      this.sectionList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getDepartmentList(){
    this.commonService.getDepartmentList().then(r => {
      this.departmentList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
/*getCustomerList(){
    this.commonService.getCustomerList().then(r => {
      this.customerList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}*/
getOwnershipList(){
    this.commonService.getOwnershipList().then(r => {
      this.ownershipList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}

  onSave(g: any) {
    let files = g.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id','0');
    formData.append('equipmentId',g.equipmentId);
    formData.append('equipmenttypeId',g.equipmenttypeId);
    formData.append('regNo',g.regNo);
    formData.append('equipmentName',g.name);
    formData.append('StartingOdometer', g.StartingOdoMeter);
    formData.append('maxfuelCapacity',g.maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('polId',g.polId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId',g.ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    this.commonService.Save(formData).then(r =>  {
      this.success = 'Equipment Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/equipment']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
