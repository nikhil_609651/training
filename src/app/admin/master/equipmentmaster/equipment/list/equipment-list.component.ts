import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormBuilder } from '@angular/forms';
import { EquipmentFormService } from 'app/admin/master/equipmentmaster/equipment/service/equipment-form.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'equipment-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./equipment-list.html')
})
export class EquipmentListComponent implements OnInit{
  selectedcustomerAll: any;
  selectedsiteAll: any;

    regNo: any;
    equipmentname: any;
    polGroups: any;
    equipmenttypeGroups: any;
  checked: string[] = [];
  checkedsite: string[] = [];
  checkedcust: string[] = [];
  departmentName: any;
  siteName: any;
  customerName: any;
  makemodel: any;
  ownership: any;
  section: any;
  odometer: any;
  fuelcapacity: any;
    ownershipGroups: any;
    siteGroups: any;
    sectionGroups: any;
    departmentGroups: any;
    makemodelGroups: any;
    customerGroups: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  viewForm: any;
  Id: any;
  name: any;
  selectedAll: any;
  public equipment: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [

    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'Equipment Id', name: 'equipmentId', sort: true },
    { title: 'Equipment Name', name: 'name', sort: true },
    { title: 'Max Fuel Fill Capacity', name: 'maxFuelCapacity', sort: true },
    { title: 'Site', name: 'site', sort: true,filter:true, },
    { title: 'Customer', name: 'customer', sort: true,filter:true },
    { title: 'Actions', name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
  private Service: EquipmentFormService,
  private fb: FormBuilder,private _sharedService : SharedService,
  private configuration: Configuration) {

    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	    this.privileges = privileges;
    });
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('SiteFilter', '');
    this.params.set('CustomerFilter','');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.rows = configuration.rows;
    this.viewForm = fb.group({
      'equipmentId': [''],
      'Id': [''],
      'name':[''],
      'regNo':[''],
      'maxfuelCapacity': [''],
      'polId': [''],
      'equipmenttypeId': [''],
      'siteId': [''],
      'customerName': [''],
      'departmentId': [''],
      'sectionId': [''],
      'ownershipId': [''],
      'odometer':[''],
    });
    var vForm = this.viewForm;
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_nameId');
    this.getequipmentList(this.params);
    this.getcustomer();
    this.getsite(userId);
    this.getdepartment();
    //this.getpol();
    this.getequipmenttype();
    this.getsection();
    this.getownership();
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getequipmentList(params: any) {
    this.loading = true;
    this.Service.getequipmentList(params).then(response => {
      this.equipment = response['result'];
      if(this.equipment.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
  getcustomer(){
    this.Service.getCustomerFilterList().then(r =>  {
        this.customerGroups = r;
        this.loading = false;
      });
  }
  /*getpol(){
    this.Service.getPolList().then(r =>  {
        this.polGroups = r['result'];
        this.loading = false;
      });
  }*/
  getsite(userId){
    this.Service.getSiteFilterList(userId).then(r =>  {
        this.siteGroups = r;
        this.loading = false;
      });
  }
  getsection(){
    this.Service.getSectionList().then(r =>  {
        this.sectionGroups = r['result'];
        this.loading = false;
      });
  }
  getdepartment(){
    this.Service.getDepartmentList().then(r =>  {
        this.departmentGroups = r['result'];
        this.loading = false;
      });
  }
  getequipmenttype(){
    this.Service.getEquipmentTypeList().then(r =>  {
        this.equipmenttypeGroups = r['result'];
        this.loading = false;
      });
  }
  getownership(){
    this.Service.getOwnershipList().then(r =>  {
        this.ownershipGroups = r['result'];
        this.loading = false;
      });
  }
   getAllLists(params: any) {
    this.loading = true;
    this.Service.getequipmentList(params).then(response => {
      this.equipment = response['results'];
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getequipmentList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  this.start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getequipmentList(this.params);
  }

  // Filteration

  selectAll(item, event) {
    if(item === 'Site'){
        this.checkArr = [];
        for (var i = 0; i < this.siteGroups.length; i++) {
          this.siteGroups[i].selected = this.selectedsiteAll;
          if(event.target.checked){
              this.checkArr.push(this.siteGroups[i].siteId.toString());
          } else {
              this.checkArr = [];
          }
        }
         this.params.set('SiteFilter', this.checkArr.toString());
    }

    if(item === 'Customer'){
      this.checkCustomerArr=[]
      for (var i = 0; i < this.customerGroups.length; i++) {
        this.customerGroups[i].selected = this.selectedcustomerAll;
        if(event.target.checked){
            this.checkCustomerArr.push(this.customerGroups[i].customerId.toString());
        } else {
            this.checkCustomerArr = [];
        }
      }
       this.params.set('CustomerFilter', this.checkCustomerArr.toString());
    }
  }
  checkArr = [];
  checkCustomerArr=[];
  checkIfAllSelected(option, event,item) {
  if(item=='Site')
  {
  this.selectedsiteAll = this.siteGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkArr.indexOf(key);
  if(event.target.checked) {
    this.checkArr.push(event.target.value);
  } else {
    this.checkArr.splice(index,1);
  }
    this.params.set('SiteFilter', this.checkArr.toString());
  }
  if(item === 'Customer'){
  this.selectedcustomerAll = this.customerGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkCustomerArr.indexOf(key);
  if(event.target.checked) {
    this.checkCustomerArr.push(event.target.value);
  } else {
    this.checkCustomerArr.splice(index,1);
  }
    this.params.set('CustomerFilter', this.checkCustomerArr.toString());
  }
  }
   // For Applying Filtering to List
   apply(item){
    this.getequipmentList(this.params);
     if(item === 'Site'){
      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this) , 0);
     }
     if(item === 'Customer'){
      setTimeout(function () {
        jQuery('#ViewCustomerModal').modal('toggle');
      }.bind(this) , 0);
     }
     jQuery('ViewSiteModal').modal('hide');
     jQuery('ViewCustomerModal').modal('hide');
     jQuery('body').removeClass('modal-open');
     jQuery('.modal-backdrop').remove();
    }
  // Filteration


  addequipment() {
    this.router.navigate(['./home/master/equipment/add']);
  }

  editequipment(id) {

    this.router.navigate(['./home/master/equipment/edit/', id]);
  }

  deleteequipment(equipment){

    this.deleted_id = equipment;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deleteequipmentconfirm(equipment){
      this.deleted_id= equipment.id;
      this.Service.deleteequipment(this.deleted_id).then(r =>  {
        this.getequipmentList(this.params);
        this.deletemessage = "Equipment Deleted Successfully";

      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }
  view(data){
    if (data.id != 0) {
        this.name = data.equipmentId;
        this.odometer=data.startingOdoMeter;
        this.fuelcapacity=data.maxFuelCapacity;
        this.siteName=data.site.name;
        this.equipmentname=data.name;
        this.customerName=data.customer.firstName;
        this.section=data.section.sectionName;
        this.ownership=data.ownership.name;
        this.departmentName=data.department.name;
        this.regNo=data.regNo;
        this.viewForm.controls['equipmentId'].setValue(this.name);
        this.viewForm.controls['name'].setValue(this.equipmentname);
        this.viewForm.controls['regNo'].setValue(this.regNo);
        this.viewForm.controls['maxfuelCapacity'].setValue(this.fuelcapacity);
        this.viewForm.controls['odometer'].setValue(this.odometer);
        this.viewForm.controls['siteId'].setValue(this.siteName);
        this.viewForm.controls['customerName'].setValue(this.customerName);
        this.viewForm.controls['sectionId'].setValue(this.section);
        this.viewForm.controls['departmentId'].setValue(this.departmentName);
        this.viewForm.controls['ownershipId'].setValue(this.ownership);
    }
}

// For File Attachments
attachment(id){
  this.Id=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadEquipmentFiles?FileId=';
this.Service.getallequipmentattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getequipmentList(this.params);
 this.attachmentmessage='No Attachments Found'
 setTimeout(function () {
   jQuery('#ViewAttachmentModal').modal('toggle');
 }.bind(this), 1000);
}
return this.attachmentGroup;
})

}
// For Deleting Attachments

deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){
    this.Service.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.Id);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
Sort(param,order,sortstatus){
  //this.params.set('ordering', sortParam);

  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getequipmentList(this.params);
}
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
