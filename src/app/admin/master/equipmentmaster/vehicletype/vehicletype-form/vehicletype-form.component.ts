import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { VehicleTypeFormService } from 'app/admin/master/equipmentmaster/vehicletype/service/vehicletype-form.service';

@Component({
  selector: 'vehicletype-form',
  template: require('./vehicletype-form.html')
})
export class VehicleTypeFormComponent {
  savebutton: boolean = true;

  nameReq: string;
  image: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() vehicletype;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  vehicletypeForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public vehicletype_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public vehicletypeGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private vehicletypeService: VehicleTypeFormService, private router: Router,
    private route: ActivatedRoute) {
    this.vehicletypeForm = fb.group({
      'vehicleTypeName': ['', Validators.compose([Validators.required])],
      'vehicleTypeId': [''],
      'FileType':[],
    });
    var cForm = this.vehicletypeForm;
  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.vehicletype_id = params['id'];
      });
  }

getvehicletypes(params){
  this.vehicletypeService.getvehicletypeList(params).then(r =>  {
      this.vehicletypeGroups = r.result;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.vehicletype && change.vehicletype.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.vehicletypeForm.controls['vehicleTypeId'].setValue(change.vehicletype.currentValue.vehicleTypeId);
      this.vehicletypeForm.controls['vehicleTypeName'].setValue(change.vehicletype.currentValue.vehicleTypeName);

    } else{
      this.loading = false;
    }
  }

  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="Vehicle type name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.vehicletypeForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.vehicletypeForm.controls['vehicleTypeName'].hasError('required') || (this.vehicletypeForm.controls['vehicleTypeName'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}