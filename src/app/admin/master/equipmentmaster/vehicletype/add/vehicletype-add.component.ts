import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleTypeFormService } from 'app/admin/master/equipmentmaster/vehicletype/service/vehicletype-form.service';
@Component({
  selector: 'add',
  template: require('./vehicletype-add.html')
})
export class VehicleTypeAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private vehicletypeService: VehicleTypeFormService) {


  }
  ngOnInit() {
    this.page = 'add';

  }
  
  onSave(vt: any) {
    vt.vehicleTypeId=0;
    let files = vt.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('vehicleTypeId','0');
    formData.append('vehicleTypeName', vt.vehicleTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.vehicletypeService.Save(formData).then(r =>  {
      this.success = 'Vehicle Type Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
