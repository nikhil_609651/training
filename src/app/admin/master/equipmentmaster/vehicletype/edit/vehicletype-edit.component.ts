import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { VehicleTypeFormService } from 'app/admin/master/equipmentmaster/vehicletype/service/vehicletype-form.service';
@Component({
  selector: 'vehicletype-edit',
  template: require('./vehicletype-edit.html')
})
export class VehicleTypeEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public vehicletype: any;
  public page = 'edit';
  public vehicletypedata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private vehicletypeService: VehicleTypeFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id= params['id'];
                this.vehicletype = this.vehicletypeService.getvehicletype(this.id);                
      });
	}

  onSave(vt: any) {
    let files = vt.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('vehicleTypeId',this.id);
    formData.append('vehicleTypeName', vt.vehicleTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.vehicletypeService.Save(formData).then(r =>  {
      this.success = 'Vehicle Type Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
