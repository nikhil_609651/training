import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class VehicleTypeFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public vehicletypeurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/VehicleType/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveVehicleType';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateVehicleType';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteVehicleTypeByID?id=';
        this.vehicletypeurl = _configuration.ServerWithApiUrl+'Masters/GetVehicleTypeByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllVehicleTypeAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteVehicleTypeAttachments?Id=';
    }
    
   
// List API for aircrafttype
getvehicletypeList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

//   Save API for vehicletype
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

private post(bu: any): Promise<any> {
    const formData = new FormData();
    formData.append('vehicleTypeId', bu.vehicleTypeId);
    formData.append('vehicleTypeName', bu.vehicleTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    console.log('append',formData)
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    console.log(formData);
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deletevehicletype(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getvehicletype(id: string) {
    return this.authHttp.get(this.vehicletypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getallvehicletypeattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getvehicletypes(id: string) {
    return this.authHttp.get(this.vehicletypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
private handleError(error: any) {
    return Promise.reject(error.json());
}
}
