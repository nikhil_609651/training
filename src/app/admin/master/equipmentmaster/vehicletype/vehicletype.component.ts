import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'vehicletype',
    template: `<router-outlet></router-outlet>`
})
export class VehicleTypeComponent {
	constructor(private router: Router) {
	}
}
