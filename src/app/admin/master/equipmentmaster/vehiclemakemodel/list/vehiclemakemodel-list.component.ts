import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VehicleMakeModelFormService } from 'app/admin/master/equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'vehiclemakemodel-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./vehiclemakemodel-list.html')
})
export class VehicleMakeModelListComponent implements OnInit{
    Id: any;
  selectedAll: any;
  attachment_deleted_id: any;
  attachmentmessage: string;
  API_URL: string;
  attachmentGroup: any[];
  attachments: any;
  makeGroups: any;
  vehiclemakemodelname: any;
  checked: string[] = [];
 public vehiclemakemodel: any=[];
  error: any;
  viewForm: FormGroup;
  public start: number = 1;
  public loading: boolean;
  public rows: Array<any> = [];
  // For headers
  public columns: Array<any> = [
    { title: 'ID', name: 'Id', sort: true,filter:false },
    { title: 'Vehicle Make', name: 'Make', sort: true,filter:false },
    { title: 'Vehicle Model', name: 'Model', sort: true, filter:false},
    { title: 'Actions', name: 'actions', sort: false,filter:false }
  ];

  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public vehiclemakemodel_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
    private service: VehicleMakeModelFormService,
    private fb: FormBuilder, private _sharedService : SharedService,
    private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	      this.privileges = privileges;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      // this.params.set('makefilter', '');
      this.rows = configuration.rows;
      this.viewForm = fb.group({
            'id': [''],
            'make': [''],
            'model': [''],
      });
      var vForm = this.viewForm;
  }

  ngOnInit() {
    this.getvehiclemakemodelList(this.params);
    this.totalfeilds = this.columns.length;
  }
  
  // For getting vehiclemakemodel List
  getvehiclemakemodelList(params: any) {
    this.loading = true;
    this.service.getvehiclemakemodelList(params).then(response => {
      this.vehiclemakemodel = response['result'];
      if(this.vehiclemakemodel.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
  
   getAllLists(params: any) {
    this.loading = true;
    this.service.getvehiclemakemodelList(params).then(response => {
      this.vehiclemakemodel = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getvehiclemakemodelList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
  
    
    if(config.status ==0){
    config.status=1;
    }
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  this.start.toString());
    var sortParam = '';

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getvehiclemakemodelList(this.params);
  }

  addvehiclemakemodel() {
    this.router.navigate(['./home/master/vehicle-make-model/add']);
  }

  editvehiclemakemodel(id) {

    this.router.navigate(['./home/master/vehicle-make-model/edit/', id]);
  }

  deletevehiclemakemodel(vehiclemakemodel){
    this.vehiclemakemodel_deleted_id = vehiclemakemodel;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }
// For Deleting generatormakemodel
  deletevehiclemakemodelconfirm(vehiclemakemodel){

      this.vehiclemakemodel_deleted_id = vehiclemakemodel.id;
      this.service.deletevehiclemakemodel(  this.vehiclemakemodel_deleted_id).then(r =>  {
        this.getvehiclemakemodelList(this.params);
        this.deletemessage = 'Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }
  view(data){
     if (data.id != 0) {
         this.viewForm.controls['make'].setValue(data.make);
         this.viewForm.controls['model'].setValue(data.model);
     }
 }
 viewclose(){
  setTimeout(function () {
      jQuery('#ViewModal').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
}
// For File Attachments
attachment(id){
  this.Id=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadGeneratorMakeAndModelFiles?FileId=';
this.service.getallvehiclemakemodelattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getvehiclemakemodelList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}

// For Deleting Attachments
deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){
   
    this.service.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.Id);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";
     
    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
// For Sorting
Sort(param,order){
 
  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getvehiclemakemodelList(this.params);


}


  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
