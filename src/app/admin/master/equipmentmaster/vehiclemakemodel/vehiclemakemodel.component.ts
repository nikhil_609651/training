import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'vehiclemakemodel',
    template: `<router-outlet></router-outlet>`
})
export class VehicleMakeModelComponent {
	constructor(private router: Router) {
	}
}
