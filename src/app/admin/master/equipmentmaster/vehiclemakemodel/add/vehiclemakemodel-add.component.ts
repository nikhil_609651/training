import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleMakeModelFormService } from 'app/admin/master/equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';

@Component({
  selector: 'add',
  template: require('./vehiclemakemodel-add.html')
})
export class VehicleMakeModelAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private vehiclemakemodelService: VehicleMakeModelFormService) {
  }
  
  ngOnInit() {
    this.page = 'add';
  }

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('id', '0');
    formData.append('make', bu.make);
    formData.append('model', bu.model);
    formData.append('description',bu.description);
    this.vehiclemakemodelService.Save(formData).then(r =>  {
      this.success = 'Vehicle Make & Model Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle-make-model']);
      }.bind(this), 1500);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
