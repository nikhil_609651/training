import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { VehicleMakeModelFormService } from 'app/admin/master/equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';
@Component({
  selector: 'vehiclemakemodel-edit',
  template: require('./vehiclemakemodel-edit.html')
})
export class VehicleMakeModelEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public vehiclemakemodel: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private vehiclemakemodelService: VehicleMakeModelFormService) {

  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
        this.vehiclemakemodel = this.vehiclemakemodelService.getvehiclemakemodel(this.id);
      });
	}

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('id', this.id);
    formData.append('make', bu.make);
    formData.append('model', bu.model);
    formData.append('description', bu.description);
    this.vehiclemakemodelService.Save(formData).then(r =>  {
      this.success = 'Vehicle Make & Model Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/vehicle-make-model']);
      }.bind(this), 1500);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}
