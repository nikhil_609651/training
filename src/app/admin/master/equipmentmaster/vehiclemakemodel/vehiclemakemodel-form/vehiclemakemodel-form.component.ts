import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { VehicleMakeModelFormService } from 'app/admin/master/equipmentmaster/vehiclemakemodel/service/vehiclemakemodel-form.service';
@Component({
  selector: 'vehiclemakemodel-form',
  template: require('./vehiclemakemodel-form.html')
})
export class VehicleMakeModelFormComponent {
  savebutton: boolean = true;

  makeReq: string;
  modelReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() vehiclemakemodel;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  vehiclemakemodelForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public filesToUpload: Array<File>;
  public selectedFileNames: string[] = [];

  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public vehiclemakemodel_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public vehiclemakemodelGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private vehiclemakemodelService: VehicleMakeModelFormService, private router: Router,
    private route: ActivatedRoute) {

    this.vehiclemakemodelForm = fb.group({
      'make': ['', Validators.compose([Validators.required])],
      'id': [''],
      'model': [''],
      'description':[''],
      'FileType':[],
    });
    var cForm = this.vehiclemakemodelForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.vehiclemakemodel_id = params['id'];
      });
  }
  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }


  // For getting vehiclemakemodel List
getvehiclemakemodels(params){
  this.vehiclemakemodelService.getvehiclemakemodelList(params).then(r =>  {
      this.vehiclemakemodelGroups = r.result;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.vehiclemakemodel && change.vehiclemakemodel.currentValue) {
      this.loading = false;
      this.vehiclemakemodelForm.controls['id'].setValue(change.vehiclemakemodel.currentValue.id);
      this.vehiclemakemodelForm.controls['model'].setValue(change.vehiclemakemodel.currentValue.model);
      this.vehiclemakemodelForm.controls['make'].setValue(change.vehiclemakemodel.currentValue.make);
      this.vehiclemakemodelForm.controls['description'].setValue(change.vehiclemakemodel.currentValue.description);
      this.vehiclemakemodelForm.controls['FileType'].setValue(this.filetype);

    } else{
      this.loading = false;
    }
  }


  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.makeReq="Vehicle Make is required";
    this.modelReq="Vehicle Model is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.vehiclemakemodelForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.vehiclemakemodelForm.controls['model'].hasError('required') || (this.vehiclemakemodelForm.controls['model'].touched))
    {
      this.modelReq="";
    }
    if(this.vehiclemakemodelForm.controls['make'].hasError('required') || (this.vehiclemakemodelForm.controls['make'].touched))
    {
      this.makeReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}