import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class VehicleMakeModelFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public vehiclemakemodel = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/VehicleMakeAndModel/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveVehicleMakeAndModel';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteVehicleMakeAndModelByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllVehicleMakeAndModelAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteVehicleMakeAndModelAttachments?Id=';
        this.vehiclemakemodel = _configuration.ServerWithApiUrl+'Masters/GetVehicleMakeAndModelByID?Id=';
        
    }
    
   
// List API for VMM
getvehiclemakemodelList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}


//   Save API for VMM
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
    
}


// tslint:disable-next-line:member-ordering

deletevehiclemakemodel(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
getallvehiclemakemodelattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getvehiclemakemodel(id: string) {
    return this.authHttp.get(this.vehiclemakemodel +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

private handleError(error: any) {
    return Promise.reject(error.json());
}
}
