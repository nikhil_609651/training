import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'aircrafttype',
    template: `<router-outlet></router-outlet>`
})
export class AirCraftTypeComponent {
	constructor(private router: Router) {
	}
}
