import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AirCraftTypeFormService } from 'app/admin/master/equipmentmaster/aircrafttype/services/aircrafttype-form.service';
@Component({
  selector: 'add',
  template: require('./aircrafttype-add.html')
})
export class AirCraftTypeAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private aircrafttypeService: AirCraftTypeFormService) {


  }
  ngOnInit() {
    this.page = 'add';

  }
  
  onSave(at: any) {
    at.aircraftTypeId=0;
    let files = at.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('aircraftTypeId','0');
    formData.append('aircraftTypeName', at.aircraftTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.aircrafttypeService.Save(formData).then(r =>  {
      this.success = 'Aircraft Type Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/aircraft-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
