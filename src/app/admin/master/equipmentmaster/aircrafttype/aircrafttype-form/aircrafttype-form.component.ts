import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { AirCraftTypeFormService } from 'app/admin/master/equipmentmaster/aircrafttype/services/aircrafttype-form.service';

@Component({
  selector: 'aircrafttype-form',
  template: require('./aircrafttype-form.html')
})
export class AirCraftTypeFormComponent {
  savebutton: boolean = true;

  nameReq: string;
  image: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() aircrafttype;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  aircrafttypeForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public aircrafttype_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public aircrafttypeGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private aircrafttypeService: AirCraftTypeFormService, private router: Router,
    private route: ActivatedRoute) {
    this.aircrafttypeForm = fb.group({
      'aircraftTypeName': ['', Validators.compose([Validators.required])],
      'aircraftTypeId': [''],
      'FileType':[],
    });
    var cForm = this.aircrafttypeForm;
  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.aircrafttype_id = params['id'];
      });
  }

getaircrafttypes(params){
  this.aircrafttypeService.getaircrafttypeList(params).then(r =>  {
      this.aircrafttypeGroups = r.result;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.aircrafttype && change.aircrafttype.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.aircrafttypeForm.controls['aircraftTypeId'].setValue(change.aircrafttype.currentValue.aircraftTypeId);
      this.aircrafttypeForm.controls['aircraftTypeName'].setValue(change.aircrafttype.currentValue.aircraftTypeName);

    } else{
      this.loading = false;
    }
  }

  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="Aircraft type name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.aircrafttypeForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.aircrafttypeForm.controls['aircraftTypeName'].hasError('required') || (this.aircrafttypeForm.controls['aircraftTypeName'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}