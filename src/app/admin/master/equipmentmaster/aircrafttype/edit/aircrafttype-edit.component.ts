import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AirCraftTypeFormService } from 'app/admin/master/equipmentmaster/aircrafttype/services/aircrafttype-form.service';
@Component({
  selector: 'aircrafttype-edit',
  template: require('./aircrafttype-edit.html')
})
export class AirCraftTypeEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public aircrafttype: any;
  public page = 'edit';
public aircrafttypedata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private aircrafttypeService: AirCraftTypeFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.aircrafttype = this.aircrafttypeService.getaircrafttype(this.id);
                
      });
 

	}

  onSave(at: any) {
    let files = at.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('aircraftTypeId',this.id);
    formData.append('aircraftTypeName', at.aircraftTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.aircrafttypeService.Save(formData).then(r =>  {
      this.success = 'Aircraft Type Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/aircraft-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
