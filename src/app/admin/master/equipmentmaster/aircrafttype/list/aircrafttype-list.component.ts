import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { AirCraftTypeFormService } from 'app/admin/master/equipmentmaster/aircrafttype/services/aircrafttype-form.service';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'aircrafttype-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./aircrafttype-list.html')
})
export class AirCraftTypeListComponent implements OnInit{

  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  viewAircraftTypeForm: any;
  Id: any;
  name: any;
  public aircrafttype: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'Aircraft Type', name: 'AircraftTypeName', sort: true },
    { title: 'Actions', name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public aircrafttype_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
  private aircrafttypeService: AirCraftTypeFormService, 
  private fb: FormBuilder, private _sharedService : SharedService,
  private configuration: Configuration) {
    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	    this.privileges = privileges;
    });
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.rows = configuration.rows;
    this.viewAircraftTypeForm = fb.group({
      'name': [''],
      'Id': [''],
    });
    var vForm = this.viewAircraftTypeForm;
  }

  ngOnInit() {
    this.getaircrafttypeList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }
 
  getaircrafttypeList(params: any) {
    this.loading = true;
    this.aircrafttypeService.getaircrafttypeList(params).then(response => {
      this.aircrafttype = response['result'];
      if(this.aircrafttype.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
     
   getAllLists(params: any) {
    this.loading = true;
    this.aircrafttypeService.getaircrafttypeList(params).then(response => {
      this.aircrafttype = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
     
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getaircrafttypeList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getaircrafttypeList(this.params);
  }

  addaircrafttype() {
    this.router.navigate(['./home/master/aircraft-type/add']);
  }

  editaircrafttype(id) {
    
    this.router.navigate(['./home/master/aircraft-type/edit/', id]);
  }
  
  deleteaircrafttype(aircrafttype){
    
    this.aircrafttype_deleted_id = aircrafttype;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deleteaircrafttypeconfirm(aircrafttype){
      this.aircrafttype_deleted_id= aircrafttype.aircraftTypeId;
      this.aircrafttypeService.deleteaircrafttype(  this.aircrafttype_deleted_id).then(r =>  {
        this.getaircrafttypeList(this.params);
        this.deletemessage = "Aircraft Type Deleted Successfully";
       
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }
  view(data){
    if (data.aircraftTypeId != 0) {
        this.name = data.aircraftTypeName;
        this.Id = data.aircraftTypeId;
        this.viewAircraftTypeForm.controls['name'].setValue(this.name);
        this.viewAircraftTypeForm.controls['Id'].setValue(this.Id);
    }
}
viewclose(){
 setTimeout(function () {
     jQuery('#ViewAircraftType').modal('toggle');
   }.bind(this), 0);
}
viewcloseattachment(){
 setTimeout(function () {
     jQuery('#ViewAttachmentModal').modal('toggle');
   }.bind(this), 0);
}
// For File Attachments
attachment(id){
  this.Id=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadAircraftTypeFiles?FileId=';
this.aircrafttypeService.getallaircrafttypeattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getaircrafttypeList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}
// For Deleting Attachments



deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){
//alert(deleted);
  // if (confirm) {
   
    this.aircrafttypeService.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.Id);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";
     
    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
Sort(param,order,sortstatus){
  //this.params.set('ordering', sortParam);
 
  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getaircrafttypeList(this.params);
}
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
