import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class AirCraftTypeFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public aircrafttypeurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/AircraftType/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveAircraftType';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateAircraftType';
       this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteAircraftTypeByID?id=';
        this.aircrafttypeurl = _configuration.ServerWithApiUrl+'Masters/GetAircraftTypeByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllAircraftTypeAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteAircraftTypeAttachments?Id=';
    }
    
   
// List API for aircrafttype
getaircrafttypeList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

//   Save API for aircrafttype
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

private post(bu: any): Promise<any> {
    const formData = new FormData();
    formData.append('aircraftTypeId', bu.aircraftTypeId);
    formData.append('aircraftTypeName', bu.aircraftTypeName);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deleteaircrafttype(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getaircrafttype(id: string) {
    return this.authHttp.get(this.aircrafttypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getallaircrafttypeattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getaircrafttypes(id: string) {
    return this.authHttp.get(this.aircrafttypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
private handleError(error: any) {
    return Promise.reject(error.json());
}
}
