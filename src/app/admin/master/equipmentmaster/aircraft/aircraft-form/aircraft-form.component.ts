import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { AircraftFormService } from 'app/admin/master/equipmentmaster/aircraft/services/aircraft-form.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
    selector: 'aircraft-form',
    template: require('./aircraft-form.html')
  })
  export class AircraftFormComponent {
  userId: string;
  savebutton: boolean = true;
  aircrafttypeReq: string;
  ownershipReq: string;
  sectionReq: string;
  basestationReq: string;
  departmentReq: string;
  customerReq: string;
  capacityReq: string;
  callsignReq: string;
  flightReq: string;
    public loading: boolean;
    customerList: any;
    departmentList: any;
    basestationList: any;
    sectionList: any;
    ownershipinfoList: any;
    aircrafttypeList: any;
    filelength: number;
    ErrorList=[];
    filetype= [];
    filelist= [];
    @ViewChild('fileInput') fileInputVariable: any;
    @Input() aircraft;
    @Input() error;
    @Input() page: string;
    @Output() saved = new EventEmitter();
    aircraftForm:FormGroup;
  public formSubmited = false;
  public aircraft_id = '';
  image= '';
  params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
    private aircraftService: AircraftFormService, private router: Router,
    private issuerService: IssuerFormService,
    private route: ActivatedRoute) {
      this.userId=localStorage.getItem('user_nameId');
        this.aircraftForm = fb.group({
            'flightno': ['',Validators.compose([Validators.required])],
            'aircraftTypeId': ['',Validators.compose([Validators.required])],
            'callsign': ['',Validators.compose([Validators.required])],
            'maxfuelfillcapacity': ['',Validators.compose([Validators.required])],
            'customer': ['',Validators.compose([Validators.required])],
            'department': ['',Validators.compose([Validators.required])],
            'basestation': ['',Validators.compose([Validators.required])],
            'section': ['',Validators.compose([Validators.required])],
            'ownershipinfo': ['',Validators.compose([Validators.required])],
            'FileType':[],
          });
          var cForm = this.aircraftForm;
    }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.aircraft_id = params['id'];
      });
      this.getCustomerList();
      this.getDepartmentList();
      this.getBaseStationList();
      this.getSectionList();
      this.getOwnershipInfoList();
      this.getAircraftTypeList();

  }
    ngOnChanges(change) {
      this.loading = this.page == 'add' ? false : true;
      if (change.aircraft && change.aircraft.currentValue) {
        this.loading = false;
        this.aircraftForm.controls['flightno'].setValue(change.aircraft.currentValue.flightNo);
        this.aircraftForm.controls['aircraftTypeId'].setValue(change.aircraft.currentValue.aircraftTypeId);
        this.aircraftForm.controls['callsign'].setValue(change.aircraft.currentValue.callsign);
        this.aircraftForm.controls['maxfuelfillcapacity'].setValue(change.aircraft.currentValue.maxFuelCapacity);
        this.aircraftForm.controls['customer'].setValue(change.aircraft.currentValue.customerId);
        this.aircraftForm.controls['department'].setValue(change.aircraft.currentValue.departmentId);
        this.aircraftForm.controls['basestation'].setValue(change.aircraft.currentValue.baseStationId);
        this.aircraftForm.controls['section'].setValue(change.aircraft.currentValue.sectionId);
        this.aircraftForm.controls['ownershipinfo'].setValue(change.aircraft.currentValue.ownershipId);
        this.aircraftForm.controls['FileType'].setValue(this.filetype);
      } else{
        this.loading = false;
      }
    }


    updated($event, control) {
        const files = $event.target.files || $event.srcElement.files;
        var reader: FileReader = new FileReader();
        var file: File = files[0];

        reader.onloadend = (e) => {
          this.image = btoa(reader.result);
          control.setValue(btoa(reader.result));
        }
        reader.readAsBinaryString(file);
      }
      onChange(event) {
        this.ErrorList=[];
        this.filelist = [];
        this.filetype = [];
        this.filelist = <Array<File>>event.target.files;
        for(let i = 0; i <this.filelist.length; i++) {
          let fSize = this.filelist[i].size;
          let fName = this.filelist[i].name;
          let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
          if(fSize > 2097152)
          {
            this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
          }
          else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt")
          {
            this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
          }
          else
          {
            this.ErrorList=[];
            this.filetype.push(this.filelist[i]);
            this.filelength= this.filetype.length;
          }
        }
      }
      onRemoveFile(event) {
        this.filelist = [];
        this.filetype = [];
        this.ErrorList= [];
      }
      onSubmit(validPost) {
        this.flightReq="Flight No: is required";
        this.aircrafttypeReq="Aircraft type is required";
        this.callsignReq="Call sign is required";
        this.capacityReq="Fuel capacity is required";
        this.basestationReq="Base station is required";
        this.customerReq="Customer is required";
        this.departmentReq="Department is required";
        this.sectionReq="Section is required";
        this.ownershipReq="Ownership is required";
        this.clearerror();
        this.formSubmited = true;
        if(this.aircraftForm.valid && this.ErrorList.length==0) {
          validPost.uploadedFile = this.filetype;
          //this.savebutton = false;
          this.saved.emit(validPost);
        } else{
          jQuery('form').find(':input.ng-invalid:first').focus();
        }
      }
      clearerror(){
        this.error="";
      }
      clearmsgs(){
        if(this.aircraftForm.controls['flightno'].hasError('required') || (this.aircraftForm.controls['flightno'].touched))
        {
          this.flightReq="";
        }
        if(this.aircraftForm.controls['aircraftTypeId'].hasError('required') || (this.aircraftForm.controls['aircraftTypeId'].touched))
        {
          this.aircrafttypeReq="";
        }
        if(this.aircraftForm.controls['callsign'].hasError('required') || (this.aircraftForm.controls['callsign'].touched))
        {
          this.callsignReq="";
        }
        if(this.aircraftForm.controls['maxfuelfillcapacity'].hasError('required') || (this.aircraftForm.controls['maxfuelfillcapacity'].touched))
        {
          this.capacityReq="";
        }
        if(this.aircraftForm.controls['customer'].hasError('required') || (this.aircraftForm.controls['customer'].touched))
        {
          this.customerReq="";
        }
        if(this.aircraftForm.controls['department'].hasError('required') || (this.aircraftForm.controls['department'].touched))
        {
          this.departmentReq="";
        }
        if(this.aircraftForm.controls['basestation'].hasError('required') || (this.aircraftForm.controls['basestation'].touched))
        {
          this.basestationReq="";
        }
        if(this.aircraftForm.controls['section'].hasError('required') || (this.aircraftForm.controls['section'].touched))
        {
          this.sectionReq="";
        }
        if(this.aircraftForm.controls['ownershipinfo'].hasError('required') || (this.aircraftForm.controls['ownershipinfo'].touched))
        {
          this.ownershipReq="";
        }
        this.error="";
        this.ErrorList=[];
      }
      // For getting Customer List
   getCustomerList(){
    this.aircraftService.getCustomerList().then(r =>  {
        this.customerList = r['result'];
       // this.loading = false;
      });
  }
  //For getting Department List
  getDepartmentList(){
    this.aircraftService.getDepartmentList().then(r =>  {
        this.departmentList = r['result'];
        //this.loading = false;
      });
  }

  //For getting BaseStation List
  getBaseStationList(){
      this.issuerService.getSiteList(this.userId).then(r => {
                  this.basestationList = r;
              })
                  .catch(r => {
                      this.handleError(r);
                  });
  }

  //For getting Section List
  getSectionList(){
    this.aircraftService.getSectionList().then(r =>  {
        this.sectionList = r['result'];
       // this.loading = false;
      });
  }

  //For getting OwnershipInfo List
  getOwnershipInfoList(){
    this.aircraftService.getOwnershipInfoList().then(r =>  {
        this.ownershipinfoList = r['result'];
       // this.loading = false;
      });
  }

  getAircraftTypeList(){
    this.aircraftService.getAircraftTypeList().then(r =>  {
      this.aircrafttypeList = r['result'];
     // this.loading = false;
    });
  }
  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
   // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
      handleError(e: any) {
          this.error = e;
          let detail = e.detail;
          if(detail && detail == 'Signature has expired.'){
            this.router.navigate(['./']);
          }
      }
  }