import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AircraftFormService } from 'app/admin/master/equipmentmaster/aircraft/services/aircraft-form.service';
@Component({
    selector: 'aircraft-edit',
    template: require('./aircraft-edit.html')
  })
  export class AircraftEditComponent implements OnInit{
  id: any;
  public error = {};
	public success = '';
  public aircraft: any;
  public page = 'edit';
  public aircraftdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private service: AircraftFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

          this. id = params['id'];
                this.aircraft = this.service.getdetails(this.id);

      });
    }
    
    onSave(bu: any) {
      console.log(bu)
      let files = bu.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
          formData.append('FileType', files[i], files[i]['name']);
      }
      formData.append('Id',this.id);
      formData.append('FlightNo', bu.flightno);
      formData.append('AircraftTypeId', bu.aircraftTypeId);
      formData.append('Callsign', bu.callsign);
      formData.append('MaxFuelCapacity',bu.maxfuelfillcapacity);
      formData.append('CustomerId',bu.customer);
      formData.append('DepartmentId',bu.department);
  
      formData.append('BaseStationId', bu.basestation);
      formData.append('SectionId',bu.section);
      formData.append('OwnerInfo',bu.ownershipinfo);
  
  
  
  
      this.service.Save(formData).then(r =>  {
        this.success = 'Aircraft Updated Successfully!';
        jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
        setTimeout(function() {
           this.success = '';
           this.router.navigate(['./home/master/aircraft']);
        }.bind(this), 3000);
      }).catch(r =>  {
        this.handleError(r);
      })
      }
    private handleError(e: any) {
        this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
    }
  }