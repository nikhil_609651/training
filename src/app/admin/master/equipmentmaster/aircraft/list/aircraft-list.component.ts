import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AircraftFormService } from 'app/admin/master/equipmentmaster/aircraft/services/aircraft-form.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
    selector: 'aircraft-list',
    encapsulation: ViewEncapsulation.None,
    template: require('./aircraft-list.html')
  })
  export class AircraftListComponent implements OnInit{
  selectedcustomerAll: any;
  selectedaircraftAll: any;
  selectedAllCustFilter: any;
  customerGroups: any;
  aircraftid: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  aircrafttypeGroups: any;
  selectedAll: any;
  attachmentGroup: any[];
  API_URL: string;
  attachments: any;
  attachmentmessage: string;
  deletemessage: string;
  aircraft_deleted_id: any;

    viewForm: FormGroup;

    public loading: boolean;
    public rows:Array<any> = [];
    public aircraft: any= [];
    public totalfeilds = 0;
    public page: number = 1;
    public itemsPerPage: number = 3;
    public maxSize: number = 5;
    public numPages: number = 2;
    public length: number = 5;
    public next = '';
    public start: number = 1;
    checked: string[] = [];
    checkedcust: string[] = [];
    checkedaircraft: string[] = [];
    // For headers
  public columns:Array<any> = [
       { title: 'ID', name: 'Id', sort: true,  },
       { title: 'Call Sign', name: 'CallSign', sort: true },
       { title: 'Flight Number', name: 'FlightNo', sort: true,  },
       { title: 'Aircraft Type', name: 'AircraftTypeName', sort: true,filter:true },
       { title: 'Customer', name: 'Customer', sort: true,filter:true },
       { title: 'Max-Fuel Fill Capacity', name: 'MaxFuelCapacity', sort: true,filter:true },
       { title: 'Actions', name: 'actions', sort: false }
     ];
     public config:any = {
      paging: true,
      sorting: {columns: this.columns},
      filtering: {filterString: ''},
      className: ['table-bordered']
    };
     params: URLSearchParams = new URLSearchParams();
     paramsCustomer: URLSearchParams = new URLSearchParams();

     privileges : any = {
      isAdd: false,
      isDelete : false,
      isEdit : false,
      isView : false
    }
    privilegeSubscription : Subscription;
    
     constructor(private router: Router,
        private aircraftService: AircraftFormService,
        private fb: FormBuilder,private _sharedService : SharedService,
         private configuration: Configuration) {
          /**user privileges**/	
          this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	          this.privileges = privileges;
          });
          this.itemsPerPage = configuration.itemsPerPage;
          this.params.set('limit', configuration.itemsPerPage.toString());
          this.params.set('AircraftTypeFilter', '');
          this.params.set('CustomerFilter', '');
          this.params.set('UserId', localStorage.getItem('user_nameId'));
          this.paramsCustomer.set('UserId', localStorage.getItem('user_nameId'));
          this.rows = configuration.rows;
          this.viewForm = fb.group({
                        'flightno': [''],
                        'aircrafttype': [''],
                        'callsign': [''],
                        'maxfuelfillcapacity': [''],
                        'customer': [''],
                        'department': [''],
                        'basestation': [''],
                        'section': [''],
                        'ownershipinfo': [''],
                    });
                    var vForm = this.viewForm;
        }
      ngOnInit(){
        this.getAircraftList(this.params);
        this.getAircraftTypeList();
        this.getCustomerList();

      }

      ngOnDestroy() {
        this.privilegeSubscription.unsubscribe();
      }

       // For getting Aircraft List
       getAircraftList(params: any) {
    this.loading = true;
    this.aircraftService.getAircraftList(params).then(response => {
      this.aircraft = response['result'];
      if(this.aircraft.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.aircraftService.getAircraftList(params).then(response => {
      this.aircraft = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }
// For listing parameters for Filteration
  getAircraftTypeList(){
    this.aircraftService.getAircraftTypeFilterList().then(r =>  {
      this.aircrafttypeGroups = r;
     // this.loading = false;
    });
  }
  getCustomerList(){
    this.aircraftService.getCustomerFilterList().then(r =>  {
      this.customerGroups = r;
     // this.loading = false;
    });
  }
  addnew() {
    this.router.navigate(['./home/master/aircraft/add']);
  }

  edit(id) {

    this.router.navigate(['./home/master/aircraft/edit/', id]);
  }

  // View Details
  view(data) {
    if (data.id != 0) {
      this.viewForm.controls['flightno'].setValue(data.flightNo);
      this.viewForm.controls['aircrafttype'].setValue(data.aircrafttype.aircraftTypeName);
      this.viewForm.controls['callsign'].setValue(data.callsign);
      this.viewForm.controls['maxfuelfillcapacity'].setValue(data.maxFuelCapacity);
      this.viewForm.controls['customer'].setValue(data.customer.firstName + ' ' + data.customer.expansion);
      this.viewForm.controls['department'].setValue(data.department.name);
      this.viewForm.controls['basestation'].setValue(data.site.name);
      this.viewForm.controls['section'].setValue(data.section.sectionName);
      this.viewForm.controls['ownershipinfo'].setValue(data.ownership.name);
    }
  }

// For Deleting Aircraft
deleteaircraft(id){
  this.aircraft_deleted_id = id;
}
deletemessageclear(){
  this.deletemessage = '';
}

deleteaircraftconfirm(data){

    this.aircraftService.delete(  this.aircraft_deleted_id).then(r =>  {
      this.getAircraftList(this.params);
      this.deletemessage = 'Aircraft Deleted Successfully';
    }).catch(r =>  {
      this.handleError(r);
      this.deletemessage = r.name[0];
    });
}
// For Listing Attachments
attachment(id){
this.aircraftid=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadAircraftTypeFiles?FileId=';

  this.aircraftService.getallattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){

  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getAircraftList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}

viewclose(){
  setTimeout(function () {
      jQuery('#ViewDetails').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
    jQuery('#ViewAttachmentModal').modal('toggle');
  }.bind(this), 0);

}
public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {


    if(config.status ==0){
    config.status=1;
    }
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  this.start.toString());
    var sortParam = '';

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getAircraftList(this.params);
  }

  // Filteration

 selectAll(item, event) {
  if(item === 'Aircraft'){
      this.checkArr = [];
      for (var i = 0; i < this.aircrafttypeGroups.length; i++) {
        this.aircrafttypeGroups[i].selected = this.selectedaircraftAll;
        if(event.target.checked){
            this.checkArr.push(this.aircrafttypeGroups[i].aircraftTypeId.toString());
        } else {
            this.checkArr = [];
        }
      }
       this.params.set('AircraftTypeFilter', this.checkArr.toString());
  }

  if(item === 'Customer'){
    this.checkCustomerArr=[]
    for (var i = 0; i < this.customerGroups.length; i++) {
      this.customerGroups[i].selected = this.selectedcustomerAll;
      if(event.target.checked){
          this.checkCustomerArr.push(this.customerGroups[i].customerId.toString());
      } else {
          this.checkCustomerArr = [];
      }
    }
     this.params.set('CustomerFilter', this.checkCustomerArr.toString());
  }
}
checkArr = [];
checkCustomerArr=[];
checkIfAllSelected(option, event,item) {
if(item=='Aircraft')
{
this.selectedaircraftAll = this.aircrafttypeGroups.every(function(item:any) {
  return item.selected == true;
})
var key = event.target.value.toString();
var index = this.checkArr.indexOf(key);
if(event.target.checked) {
  this.checkArr.push(event.target.value);
} else {
  this.checkArr.splice(index,1);
}
  this.params.set('AircraftTypeFilter', this.checkArr.toString());
}
if(item === 'Customer'){
this.selectedcustomerAll = this.customerGroups.every(function(item:any) {
  return item.selected == true;
})
var key = event.target.value.toString();
var index = this.checkCustomerArr.indexOf(key);
if(event.target.checked) {
  this.checkCustomerArr.push(event.target.value);
} else {
  this.checkCustomerArr.splice(index,1);
}
  this.params.set('CustomerFilter', this.checkCustomerArr.toString());
}
}
 // For Applying Filtering to List
 apply(item){
  this.getAircraftList(this.params);
   if(item === 'Aircraft'){
    setTimeout(function () {
      jQuery('#ViewAircraftTypeModal').modal('toggle');
    }.bind(this) , 0);
   }
   if(item === 'Customer'){
    setTimeout(function () {
      jQuery('#ViewCustomerModal').modal('toggle');
    }.bind(this) , 0);
   }
   jQuery('ViewAircraftTypeModal').modal('hide');
   jQuery('ViewCustomerModal').modal('hide');
   jQuery('body').removeClass('modal-open');
   jQuery('.modal-backdrop').remove();
  }
// Filteration


// For Deleting Attachments

deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){

    this.aircraftService.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this. aircraftid);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}


// For Sorting
Sort(param,order){
  //this.params.set('ordering', sortParam);

  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getAircraftList(this.params);


    }


  private handleError(e: any) {
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
}
  }