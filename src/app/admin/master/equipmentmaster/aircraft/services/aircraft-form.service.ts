import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';
@Injectable()

export class AircraftFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public attachmenturl ="";
    public downloadUrl = "";
    public deleteattachmentUrl = "";
    // for dropdowns
    public customerUrl = '';
    public customerFilterUrl = '';
    public departmentUrl = '';
    public basestationUrl = '';
    public sectionUrl = '';
    public ownershipinfoUrl = '';
    public aircraftUrl= '';
    public aircraftFilterUrl= '';
    public viewurl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

          this.listUrl = _configuration.ServerWithApiUrl + 'Masters/Aircraft/';
          this.saveUrl = _configuration.ServerWithApiUrl + 'Masters/SaveAircraft';
          this.updateUrl = _configuration.ServerWithApiUrl + 'Masters/UpdateAircraft';
         this.deleteUrl = _configuration.ServerWithApiUrl + 'Masters/DeleteAircraftByID?id=';
          // tslint:disable-next-line:max-line-length
          this.attachmenturl = _configuration.ServerWithApiUrl + 'Masters/GetAllAircraftAttachments?id=';
          this.downloadUrl = _configuration.ServerWithApiUrl + 'Masters/DownloadAircraftFiles?FileId=';
          this.viewurl = _configuration.ServerWithApiUrl+'Masters/GetAircraftByID?id=';
          // tslint:disable-next-line:max-line-length
          this.deleteattachmentUrl = _configuration.ServerWithApiUrl + 'Masters/DeleteAircraftAttachments?Id=';
          this.customerUrl = _configuration.ServerWithApiUrl + 'Masters/Customer/';
          this.customerFilterUrl = _configuration.ServerWithApiUrl + 'Masters/GetAircraftCustomerFilter/';
          this.departmentUrl = _configuration.ServerWithApiUrl + 'Masters/Department/';
          this.basestationUrl = _configuration.ServerWithApiUrl + 'Masters/Site/';
          this.sectionUrl = _configuration.ServerWithApiUrl + 'Masters/Section/';
          this.ownershipinfoUrl = _configuration.ServerWithApiUrl + 'Masters/Ownership/';
          this.aircraftUrl = _configuration.ServerWithApiUrl + 'Masters/AircraftType/';
          this.aircraftFilterUrl = _configuration.ServerWithApiUrl + 'Masters/GetAircraftTypeFilter/';
        }

        // List API for Aircraft List
    getAircraftList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}


// List API's for Dropdowns
getAircraftTypeList(): Promise<any> {
    return this.authHttp.get(this.aircraftUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getAircraftTypeFilterList(): Promise<any> {
    return this.authHttp.get(this.aircraftFilterUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getCustomerList(): Promise<any> {
    return this.authHttp.get(this.customerUrl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getCustomerFilterList(): Promise<any> {
    return this.authHttp.get(this.customerFilterUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getDepartmentList(): Promise<any> {
    return this.authHttp.get(this.departmentUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getBaseStationList(): Promise<any> {
    return this.authHttp.get(this.basestationUrl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getSectionList(): Promise<any> {
    return this.authHttp.get(this.sectionUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getOwnershipInfoList(): Promise<any> {
    return this.authHttp.get(this.ownershipinfoUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
//   Save API for Aircraft
Save(bu: any): Promise<any>  {
    // if(bu.buId == 0){
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
    }

// For Delating Aircraft
    delete(id: any) {
       
        const formData = new FormData();
        formData.append('id', id);
    
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
    
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    // For getting all attachments
    getallattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    // For View Deatils
    getdetails(id: string) {
        return this.authHttp.get(this.viewurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    // For deleting attachments
    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
    
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
private handleError(error: any) {
    return Promise.reject(error.json());
}
}