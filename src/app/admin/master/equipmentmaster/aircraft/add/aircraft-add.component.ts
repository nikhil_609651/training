import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AircraftFormService } from 'app/admin/master/equipmentmaster/aircraft/services/aircraft-form.service';
@Component({
  selector: 'add',
  template: require('./aircraft-add.html')
})
export class AircraftAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public aircraft: any;
  public page = '';

  constructor(private router: Router,
    private aircraftService: AircraftFormService) {
  }

  ngOnInit() {
    this.page = 'add';
  }

  // For Saving Datas
  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id', '0');
    formData.append('FlightNo', bu.flightno);
    formData.append('AircraftTypeId', bu.aircraftTypeId);
    formData.append('Callsign', bu.callsign);
    formData.append('MaxFuelCapacity',bu.maxfuelfillcapacity);
    formData.append('CustomerId',bu.customer);
    formData.append('DepartmentId',bu.department);

    formData.append('BaseStationId', bu.basestation);
    formData.append('SectionId',bu.section);
    formData.append('OwnerInfo',bu.ownershipinfo);
    this.aircraftService.Save(formData).then(r =>  {
      this.success = 'Aircraft Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/aircraft']);
      }.bind(this), 2000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
