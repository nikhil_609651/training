import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'aircraft',
    template: `<router-outlet></router-outlet>`
})
export class AirCraftComponent {
	constructor(private router: Router) {
	}
}
