import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { GeneratorMakeModelFormService } from 'app/admin/master/equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
@Component({
  selector: 'generatormakemodel-form',
  template: require('./generatormakemodel-form.html')
})
export class GeneratorMakeModelFormComponent {
  savebutton: boolean = true;
  kvReq: string;
  makeReq: string;
  modelReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() generatormakemodel;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  generatormakemodelForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public filesToUpload: Array<File>;
  public selectedFileNames: string[] = [];
  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public generatormakemodel_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public generatormakemodelGroups =[];
  params: URLSearchParams = new URLSearchParams()

  constructor(private fb: FormBuilder,
    private generatormakemodelService: GeneratorMakeModelFormService, private router: Router,
    private route: ActivatedRoute) {
    this.generatormakemodelForm = fb.group({
      'make': ['', Validators.compose([Validators.required])],
      'id': [''],
      'model': ['', Validators.compose([Validators.required])],
      'kv': ['',Validators.compose([Validators.required])],
      'description':[''],
      'FileType':[],
    });
    var cForm = this.generatormakemodelForm;
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
        this.generatormakemodel_id = params['id'];
    });
  }

  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  // For getting generatormakemodel List
  getgeneratormakemodels(params){
    this.generatormakemodelService.getgeneratormakemodelList(params).then(r =>  {
        this.generatormakemodelGroups = r.result;
        this.loading = false;
      })
  }

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.generatormakemodel && change.generatormakemodel.currentValue) {
      this.loading = false;
      this.generatormakemodelForm.controls['id'].setValue(change.generatormakemodel.currentValue.id);
      this.generatormakemodelForm.controls['model'].setValue(change.generatormakemodel.currentValue.model);
      this.generatormakemodelForm.controls['make'].setValue(change.generatormakemodel.currentValue.make);
      this.generatormakemodelForm.controls['kv'].setValue(change.generatormakemodel.currentValue.kv);
      this.generatormakemodelForm.controls['FileType'].setValue(this.filetype);
      this.generatormakemodelForm.controls['description'].setValue(change.generatormakemodel.currentValue.description);
    } else{
      this.loading = false;
    }
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" &&  extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }

  onSubmit(validPost) {
    this.makeReq="Generator Make is required";
    this.modelReq="Generator Model is required";
    this.kvReq="KV is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.generatormakemodelForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror(){
    this.error="";
  }

  clearmsgs(){
    if(this.generatormakemodelForm.controls['model'].hasError('required') || (this.generatormakemodelForm.controls['model'].touched))
    {
      this.modelReq="";
    }
    if(this.generatormakemodelForm.controls['make'].hasError('required') || (this.generatormakemodelForm.controls['make'].touched))
    {
      this.makeReq="";
    }
    if(this.generatormakemodelForm.controls['kv'].hasError('required') || (this.generatormakemodelForm.controls['kv'].touched))
    {
      this.kvReq="";
    }
    this.error="";
    this.ErrorList=[];
  }

  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}