import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GeneratorMakeModelFormService } from 'app/admin/master/equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'generatormakemodel-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./generatormakemodel-list.html')
})
export class GeneratorMakeModelListComponent implements OnInit{
  selectedAll: any;
  attachment_deleted_id: any;
  attachmentmessage: string;
  buId: any;
  API_URL: string;
  attachmentGroup: any[];
  attachments: any;
  makeGroups: any;
  generatormakemodelname: any;
  checked: string[] = [];
  public generatormakemodel: any=[];
  error: any;
  viewForm: FormGroup;
  public start: number = 1;
  public loading: boolean;
  public rows: Array<any> = [];
  // For headers
  public columns: Array<any> = [
    { title: 'ID', name: 'Id', sort: true,filter:false },
    { title: 'Generator Make', name: 'Make', sort: true,filter:false },
    { title: 'Generator Model', name: 'Model', sort: true, filter:false},
    { title: 'KV', name: 'KV', sort: true, filter:false},
    { title: 'Actions', name: 'actions', sort: false,filter:false }
  ];

  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public generatormakemodel_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  params: URLSearchParams = new URLSearchParams();

  constructor(private router: Router,
    private service: GeneratorMakeModelFormService,
    private fb: FormBuilder, private _sharedService : SharedService,
    private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	      this.privileges = privileges;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      // this.params.set('makefilter', '');
      this.rows = configuration.rows;
      this.viewForm = fb.group({
                  'id': [''],
                  'make': [''],
                  'model': [''],
                  'kv' : [''],
      });
      var vForm = this.viewForm;
  }

  ngOnInit() {
    this.getgeneratormakemodelList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  // For getting generatormakemodel List
  getgeneratormakemodelList(params: any) {
    this.loading = true;
    this.service.getgeneratormakemodelList(params).then(response => {
      this.generatormakemodel = response['result'];
      if(this.generatormakemodel.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
   // Filtering
//    selectAll() {
//     for (var i = 0; i < this.makeGroups.length; i++) {

//       this.makeGroups[i].selected = this.selectedAll;
//     }
//     this.params.set('makefilter', null);

//   }
//   checkIfAllSelected(option, event) {
//     this.selectedAll = this.makeGroups.every(function(item:any) {
//         return item.selected == true;
//       })

//        var index = this.checked.indexOf(option.id);
//     if(event.target.checked) {
//       if(index === -1) {
//         this.checked.push(option.id);

//       }
//     }
//     else {
//       if(index !== -1) {
//         this.checked.splice(index, 1);
//       }
//     }
// this.params.set('makefilter', this.checked.toString());
//   }

//   // for listing generatormakemodel list after filtering by using apply button
//     apply(){
//       this.getgeneratormakemodelList(this.params);
//       setTimeout(function () {
//         jQuery('#ViewModal').modal('toggle');
//       }.bind(this), 0);
//     }

   getAllLists(params: any) {
    this.loading = true;
    this.service.getgeneratormakemodelList(params).then(response => {
      this.generatormakemodel = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getgeneratormakemodelList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {


    if(config.status ==0){
    config.status=1;
    }
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  this.start.toString());
    var sortParam = '';

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getgeneratormakemodelList(this.params);
  }

  addgeneratormakemodel() {
    this.router.navigate(['./home/master/generator-make-model/add']);
  }

  editgeneratormakemodel(id) {

    this.router.navigate(['./home/master/generator-make-model/edit/', id]);
  }

  deletegeneratormakemodel(generatormakemodel){
    this.generatormakemodel_deleted_id = generatormakemodel;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }
// For Deleting generatormakemodel
  deletegeneratormakemodelconfirm(generatormakemodel){

      this.service.deletegeneratormakemodel(this.generatormakemodel_deleted_id).then(r =>  {
        this.getgeneratormakemodelList(this.params);
        this.deletemessage = 'Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }
  view(data){
     if (data.id != 0) {
         this.viewForm.controls['make'].setValue(data.make);
         this.viewForm.controls['model'].setValue(data.model);
         this.viewForm.controls['kv'].setValue(data.kv);
     }
 }
 viewclose(){
  setTimeout(function () {
      jQuery('#ViewModal').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
}
// For File Attachments
attachment(id){
  this.buId=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadGeneratorMakeAndModelFiles?FileId=';
this.service.getallgeneratormakemodelattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getgeneratormakemodelList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}
// For Deleting Attachments



deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){

    this.service.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.buId);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
// For Sorting
Sort(param,order){

  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getgeneratormakemodelList(this.params);


}


  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
