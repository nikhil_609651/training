import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class GeneratorMakeModelFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public generatormakemodel = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/GeneratorMakeAndModel/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveGeneratorMakeAndModel';
       this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteGeneratorMakeAndModelByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllGeneratorMakeAndModelAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteGeneratorMakeAndModelAttachments?Id=';
        this.generatormakemodel = _configuration.ServerWithApiUrl+'Masters/GetGeneratorMakeAndModelByID?Id=';
        
    }
    
   
// List API for GMM
getgeneratormakemodelList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}



//   Save API for GMM
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
    
}


// tslint:disable-next-line:member-ordering

deletegeneratormakemodel(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
getallgeneratormakemodelattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getgeneratormakemodel(id: string) {
    return this.authHttp.get(this.generatormakemodel +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

private handleError(error: any) {
    return Promise.reject(error.json());
}
}
