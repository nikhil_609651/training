import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'generatormakemodel',
    template: `<router-outlet></router-outlet>`
})
export class GeneratorMakeModelComponent {
	constructor(private router: Router) {
	}
}
