import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneratorMakeModelFormService } from 'app/admin/master/equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
@Component({
  selector: 'add',
  template: require('./generatormakemodel-add.html')
})
export class GeneratorMakeModelAddComponent implements OnInit{
  countryList: any;
  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private generatormakemodelService: GeneratorMakeModelFormService) {
  }

  ngOnInit() {
    this.page = 'add';
  }

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('id', '0');
    formData.append('make', bu.make);
    formData.append('model', bu.model);
    formData.append('kv', bu.kv);
    formData.append('description',bu.description);
    this.generatormakemodelService.Save(formData).then(r =>  {
      this.success = 'Generator Make & Model Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/generator-make-model']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
