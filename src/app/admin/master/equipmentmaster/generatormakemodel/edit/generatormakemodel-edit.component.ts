import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GeneratorMakeModelFormService } from 'app/admin/master/equipmentmaster/generatormakemodel/services/generatormakemodel-form.service';
@Component({
  selector: 'generatormakemodel-edit',
  template: require('./generatormakemodel-edit.html')
})
export class GeneratorMakeModelEditComponent implements OnInit{
  id: any;
  public error = {};
	public success = '';
  public generatormakemodel: any;
  public page = 'edit';
  public generatormakemodeldata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private generatormakemodelService: GeneratorMakeModelFormService) {
  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
        this.generatormakemodel = this.generatormakemodelService.getgeneratormakemodel(this.id);
      });
	}

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('id', this.id);
    formData.append('make', bu.make);
    formData.append('model', bu.model);
    formData.append('kv', bu.kv);
    formData.append('description',bu.description);
    this.generatormakemodelService.Save(formData).then(r =>  {
      this.success = 'Generator Make & Model Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/generator-make-model']);
      }.bind(this), 1500);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
