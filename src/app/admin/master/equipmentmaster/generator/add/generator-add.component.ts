import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneratorFormService } from 'app/admin/master/equipmentmaster/generator/service/generator-form.service';

@Component({
  selector: 'add',
  template: require('./generator-add.html')
})
export class GeneratorAddComponent implements OnInit{
    makeList: any;
    ownershipList: any;
    customerList: any;
    departmentList: any;
    sectionList: any;
  siteList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private commonService: GeneratorFormService) {
  }
  ngOnInit() {
    this.page = 'add';
  }
  getSiteList(){
        this.commonService.getSiteList().then(r => {
          this.siteList = r;
      })
          .catch(r => {
              this.handleError(r);
          });

  }
  getSectionList(){
    this.commonService.getSectionList().then(r => {
      this.sectionList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getDepartmentList(){
    this.commonService.getDepartmentList().then(r => {
      this.departmentList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
/*getCustomerList(){
    this.commonService.getCustomerList().then(r => {
      this.customerList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}*/
getOwnershipList(){
    this.commonService.getOwnershipList().then(r => {
      this.ownershipList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getMakeList(){
    this.commonService.getMakeModelList().then(r => {
      this.makeList = r;
  })
      .catch(r => {
          this.handleError(r);
      });

}
  onSave(g: any) {
    let files = g.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    let startingOdoMeter = g.StartingOdoMeter ? g.StartingOdoMeter : 0;
    let maxfuelCapacity = g.maxfuelCapacity ? g.maxfuelCapacity : 0;
    let ownershipId = g.ownershipId ? g.ownershipId : 0;
    formData.append('Id','0');
    formData.append('generatorId',g.generatorId);
    formData.append('StartingOdometer', startingOdoMeter);
    formData.append('MakeAndModelId',g.MakeAndModelId);
    formData.append('maxfuelCapacity', maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId', ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    this.commonService.Save(formData).then(r =>  {
      this.success = 'Generator Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/generator']);
      }.bind(this), 2000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
