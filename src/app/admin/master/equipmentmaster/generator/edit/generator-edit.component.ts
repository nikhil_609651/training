import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GeneratorFormService } from 'app/admin/master/equipmentmaster/generator/service/generator-form.service';

@Component({
  selector: 'generator-edit',
  template: require('./generator-edit.html')
})
export class GeneratorEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public generator: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private commonService: GeneratorFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.generator = this.commonService.getgenerator(this.id);
      });
    }

  onSave(g: any) {
    let files = g.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id',g.Id);
    formData.append('generatorId',g.generatorId);
    formData.append('StartingOdometer', g.StartingOdoMeter);
    formData.append('MakeAndModelId',g.MakeAndModelId);
    formData.append('maxfuelCapacity',g.maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId',g.ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    this.commonService.Save(formData).then(r =>  {
      this.success = 'Generator Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/generator']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
