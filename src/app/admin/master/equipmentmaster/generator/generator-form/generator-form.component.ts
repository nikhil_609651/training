import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { GeneratorFormService } from 'app/admin/master/equipmentmaster/generator/service/generator-form.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'generator-form',
  template: require('./generator-form.html')
})
export class GeneratorFormComponent {
  userId: string;
  savebutton: boolean = true;

  makeList: any;
  generatorIdReq: string;
  odometerReq:string;
  maxfuelcapacityReq:string;
  makeReq: string;
  siteReq: string;
  customerReq: string;
  departmentReq: string;
  sectionReq: string;
  ownershipReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @Input() generator;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  generatorForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public siteList: Array<string>;
  public sectionList: Array<string>;
  public departmentList: Array<string>;
  public ownershipList: Array<string>;
  public customerList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  // tslint:disable-next-line:variable-name
  public generator_id = '';
  public generatorGroups =[];
   params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
    private generatorService: GeneratorFormService, private router: Router,
    private issuerService: IssuerFormService,
    private route: ActivatedRoute) {

    this.userId=localStorage.getItem('user_nameId');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.getSiteList();
    this.getSectionList();
    this.getDepartmentList();
    this.getOwnershipList();
    this.getCustomerList(this.params);
    this.getMakeList();
    this.generatorForm = fb.group({
      'Id': [''],
      'generatorId': ['', Validators.compose([Validators.required])],
      'StartingOdoMeter':  [''],
      'MakeAndModelId': ['', Validators.compose([Validators.required])],
      'maxfuelCapacity': [''],
      'siteId': ['', Validators.compose([Validators.required])],
      'customerId': ['',Validators.compose([Validators.required])],
      'departmentId': ['',Validators.compose([Validators.required])],
      'ownershipId': ['', Validators.compose([Validators.required])],
      'sectionId': ['',Validators.compose([Validators.required])],
      'BarCode': [''],
      'FileType':[''],
    });
    var cForm = this.generatorForm;
  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.generator_id = params['id'];
      });
  }

// for getting SiteList
  getSiteList(){
          this.issuerService.getSiteList(this.userId).then(r => {
                      this.siteList = r;
                  })
                      .catch(r => {
                          this.handleError(r);
                      });

  }
  getSectionList(){
    this.generatorService.getSectionList().then(r => {
      this.sectionList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getDepartmentList(){
    this.generatorService.getDepartmentList().then(r => {
      this.departmentList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      });

}
getMakeList(){
  this.generatorService.getMakeModelList().then(r => {
    this.makeList = r.result;
})
    .catch(r => {
        this.handleError(r);
    });

}
getCustomerList(params){
  this.generatorService.getCustomerList(params).then(r => {
    this.customerList = r.result;
})
    .catch(r => {
        this.handleError(r);
    });

}
getOwnershipList(){
    this.generatorService.getOwnershipList().then(r => {
      this.ownershipList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      });

}
  // for getting Generator List
getgenerator(params){
  this.generatorService.getgeneratorList(params).then(r =>  {
      this.generatorGroups = r.result;
    });
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.generator && change.generator.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.generatorForm.controls['Id'].setValue(change.generator.currentValue.id);
      this.generatorForm.controls['generatorId'].setValue(change.generator.currentValue.generatorId);
      this.generatorForm.controls['StartingOdoMeter'].setValue(change.generator.currentValue.startingOdoMeter);
      this.generatorForm.controls['MakeAndModelId'].setValue(change.generator.currentValue.makeAndModelId);
      this.generatorForm.controls['maxfuelCapacity'].setValue(change.generator.currentValue.maxFuelCapacity);
      this.generatorForm.controls['siteId'].setValue(change.generator.currentValue.siteId);
      this.generatorForm.controls['customerId'].setValue(change.generator.currentValue.customerId);
      this.generatorForm.controls['sectionId'].setValue(change.generator.currentValue.sectionId);
      this.generatorForm.controls['ownershipId'].setValue(change.generator.currentValue.ownershipId);
      this.generatorForm.controls['departmentId'].setValue(change.generator.currentValue.departmentId);
      this.generatorForm.controls['BarCode'].setValue(change.generator.currentValue.barCode);
      this.generatorForm.controls['FileType'].setValue(this.filetype);

    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.generatorIdReq="Generator Id is required";
    this.maxfuelcapacityReq="Max Fuel Capacity is required";
    this.odometerReq="Odometer is required";
    this.siteReq="Site is required";
    this.maxfuelcapacityReq="UNID No: is required";
    this.sectionReq="Section is required";
    this.departmentReq="Department is required";
    this.ownershipReq="Ownership Info is required";
    this.customerReq="Customer is required";
    this.makeReq="Make and Model is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.generatorForm.valid && this.ErrorList.length=== 0) {
     validPost.uploadedFile = this.filetype;
     //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error='';
  }
  // for Uploading Files
  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.generatorForm.controls['generatorId'].hasError('required') || (this.generatorForm.controls['generatorId'].touched))
    {
      this.generatorIdReq="";
    }
    if(this.generatorForm.controls['maxfuelCapacity'].hasError('required') || (this.generatorForm.controls['maxfuelCapacity'].touched))
    {
      this.maxfuelcapacityReq="";
    }
    if(this.generatorForm.controls['StartingOdoMeter'].hasError('required') || (this.generatorForm.controls['StartingOdoMeter'].touched))
    {
      this.odometerReq="";
    }
    if(this.generatorForm.controls['siteId'].hasError('required') || (this.generatorForm.controls['siteId'].touched))
    {
      this.siteReq="";
    }
    if(this.generatorForm.controls['sectionId'].hasError('required') || (this.generatorForm.controls['sectionId'].touched))
    {
      this.sectionReq="";
    }
    if(this.generatorForm.controls['customerId'].hasError('required') || (this.generatorForm.controls['customerId'].touched))
    {
      this.customerReq="";
    }
    if(this.generatorForm.controls['departmentId'].hasError('required') || (this.generatorForm.controls['departmentId'].touched))
    {
      this.departmentReq="";
    }
    if(this.generatorForm.controls['ownershipId'].hasError('required') || (this.generatorForm.controls['ownershipId'].touched))
    {
      this.ownershipReq="";
    }
    if(this.generatorForm.controls['MakeAndModelId'].hasError('required') || (this.generatorForm.controls['MakeAndModelId'].touched))
    {
      this.makeReq="";
    }
    this.error="";
    this.ErrorList=[];
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}