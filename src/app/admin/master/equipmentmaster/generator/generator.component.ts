import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'generator',
    template: `<router-outlet></router-outlet>`
})
export class GeneratorComponent {
	constructor(private router: Router) {
	}
}
