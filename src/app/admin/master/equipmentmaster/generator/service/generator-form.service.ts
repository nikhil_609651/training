import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class GeneratorFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public generatoreurl = '';
    public customerurl = '';
    public customerFilterurl = '';
    public siteurl = '';
    public siteFilterurl = '';
    public departmenturl = '';
    public makemodelurl = '';
    public ownershipurl = '';
    public sectionurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Generator/';
        this.customerurl = _configuration.ServerWithApiUrl+'Masters/Customer/';
        this.customerFilterurl = _configuration.ServerWithApiUrl+'Masters/GetGeneratorCustomerFilter/';
        this.siteurl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.siteFilterurl = _configuration.ServerWithApiUrl+'Masters/GetGeneratorSiteFilter/';
        this.departmenturl = _configuration.ServerWithApiUrl+'Masters/Department/';
        this.sectionurl = _configuration.ServerWithApiUrl+'Masters/Section/';
        this.makemodelurl = _configuration.ServerWithApiUrl+'Masters/GeneratorMakeAndModel/';
        this.ownershipurl = _configuration.ServerWithApiUrl+'Masters/Ownership/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveGenerator';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateGenerator';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteGeneratorByID?id=';
        this.generatoreurl = _configuration.ServerWithApiUrl+'Masters/GetGeneratorByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllGeneratorAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteGeneratorAttachments?Id=';
    }
    
   
// List API for generator
getgeneratorList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

getCustomerList(params): Promise<any> {
    return this.authHttp.get(this.customerurl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getCustomerFilterList(): Promise<any> {
    return this.authHttp.get(this.customerFilterurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSiteList(): Promise<any> {
    return this.authHttp.get(this.siteurl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSiteFilterList(userId): Promise<any> {
    return this.authHttp.get(this.siteFilterurl+ '?UserId=' + userId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSectionList(): Promise<any> {
    return this.authHttp.get(this.sectionurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getDepartmentList(): Promise<any> {
    return this.authHttp.get(this.departmenturl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getMakeModelList(): Promise<any> {
    return this.authHttp.get(this.makemodelurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getOwnershipList(): Promise<any> {
    return this.authHttp.get(this.ownershipurl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
//   Save API for generator
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

private post(g: any): Promise<any> {
    const formData = new FormData();
    formData.append('Id', g.Id);
    formData.append('generatorId', g.generatorId);
    formData.append('StartingOdometer', g.StartingOdoMeter);
    formData.append('MakeAndModelId',g.makeId);
    formData.append('maxfuelCapacity',g.maxfuelCapacity);
    formData.append('customerId',g.customerId);
    formData.append('DepartmentId',g.departmentId);
    formData.append('siteId',g.siteId);
    formData.append('sectionId',g.sectionId);
    formData.append('ownershipId',g.ownershipId);
    formData.append('BarCode',g.BarCode);
    formData.append('fileURL',"");
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deletegenerator(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getallgeneratorattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getgenerator(id: string) {
    return this.authHttp.get(this.generatoreurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
private handleError(error: any) {
    return Promise.reject(error.json());
}
}
