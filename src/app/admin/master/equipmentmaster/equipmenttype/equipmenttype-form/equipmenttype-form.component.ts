import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { EquipmentTypeFormService } from 'app/admin/master/equipmentmaster/equipmenttype/service/equipmenttype-form.service';

@Component({
  selector: 'equipmenttype-form',
  template: require('./equipmenttype-form.html')
})
export class EquipmentTypeFormComponent {
  savebutton: boolean = true;

  nameReq: string;
  image: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() equipmenttype;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  equipmenttypeForm:FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public equipmenttype_id = '';
  public IsBulk=false;
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public equipmenttypeGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private equipmenttypeService: EquipmentTypeFormService, private router: Router,
    private route: ActivatedRoute) {
    this.equipmenttypeForm = fb.group({
      'equipmentTypeName': ['', Validators.compose([Validators.required])],
      'equipmentTypeId': [''],
     // 'IsBulk': [''],
      'FileType':[],
    });
    var cForm = this.equipmenttypeForm;
  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.equipmenttype_id = params['id'];
      });
  }

getequipmenttypes(params){
  this.equipmenttypeService.getequipmenttypeList(params).then(r =>  {
      this.equipmenttypeGroups = r.result;
      this.loading = false;
    })
}
selectAll(event) {
    this.IsBulk=event.target.checked;
  }
  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.equipmenttype && change.equipmenttype.currentValue) {
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.equipmenttypeForm.controls['equipmentTypeId'].setValue(change.equipmenttype.currentValue.id);
      this.equipmenttypeForm.controls['equipmentTypeName'].setValue(change.equipmenttype.currentValue.name);
      //this.equipmenttypeForm.controls['IsBulk'].setValue(change.equipmenttype.currentValue.isBulk);
    } else{
      this.loading = false;
    }
  }

  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="Equipment type name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.equipmenttypeForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //validPost.IsBulk=this.IsBulk;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.equipmenttypeForm.controls['equipmentTypeName'].hasError('required') || (this.equipmenttypeForm.controls['equipmentTypeName'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}