import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class EquipmentTypeFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public equipmenttypeurl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
      
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/EquipmentType/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveEquipmentType';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateEquipmentType';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteEquipmentTypeByID?id=';
        this.equipmenttypeurl = _configuration.ServerWithApiUrl+'Masters/GetEquipmentTypeByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllEquipmentTypeAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteEquipmentTypeAttachments?Id=';
    }
    
   
// List API for equipment type
getequipmenttypeList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}

//   Save API for equipment type
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

private post(bu: any): Promise<any> {
    const formData = new FormData();
    formData.append('equipmentTypeId', bu.equipmentTypeId);
    formData.append('equipmentTypeName', bu.equipmentTypeName);
    formData.append('IsBulk',bu.IsBulk);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl,formData,options)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);

}
// tslint:disable-next-line:member-ordering

deleteequipmenttype(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getequipmenttype(id: string) {
    return this.authHttp.get(this.equipmenttypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getallequipmenttypeattachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getequipmenttypes(id: string) {
    return this.authHttp.get(this.equipmenttypeurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}
private handleError(error: any) {
    return Promise.reject(error.json());
}
}
