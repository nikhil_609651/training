import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { EquipmentTypeFormService } from 'app/admin/master/equipmentmaster/equipmenttype/service/equipmenttype-form.service';
@Component({
  selector: 'equipmenttype-edit',
  template: require('./equipmenttype-edit.html')
})
export class EquipmentTypeEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public equipmenttype: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private equipmenttypeService: EquipmentTypeFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
        this.equipmenttype = this.equipmenttypeService.getequipmenttype(this.id);
      });


	}

  onSave(at: any) {
    let files = at.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('equipmentTypeId',at.equipmentTypeId);
    formData.append('equipmentTypeName', at.equipmentTypeName);
   // formData.append('IsBulk', at.IsBulk);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.equipmenttypeService.Save(formData).then(r =>  {
      this.success = 'Equipment Type Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/equipment-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
