import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'equipmenttype',
    template: `<router-outlet></router-outlet>`
})
export class EquipmentTypeComponent {
	constructor(private router: Router) {
	}
}
