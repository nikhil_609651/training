import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EquipmentTypeFormService } from 'app/admin/master/equipmentmaster/equipmenttype/service/equipmenttype-form.service';
@Component({
  selector: 'add',
  template: require('./equipmenttype-add.html')
})
export class EquipmentTypeAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private equipmenttypeService: EquipmentTypeFormService) {

  }
  ngOnInit() {
    this.page = 'add';

  }

  onSave(at: any) {
    at.equipmentTypeId=0;
    let files = at.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('equipmentTypeId','0');
    formData.append('equipmentTypeName', at.equipmentTypeName);
   // formData.append('IsBulk', at.IsBulk);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.equipmenttypeService.Save(formData).then(r =>  {
      this.success = 'Equipment Type Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/equipment-type']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
