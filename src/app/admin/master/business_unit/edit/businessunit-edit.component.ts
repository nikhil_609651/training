import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BusinessUnitFormService } from 'app/admin/master/business_unit/services/businessunit-form.service';

@Component({
  selector: 'businessunit-edit',
  template: require('./businessunit-edit.html')
})
export class BusinessUnitEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public businessunit: any;
  public page = 'edit';
  public businessunitdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private businessunitService: BusinessUnitFormService) {
  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.id = params['id'];
          this.businessunit = this.businessunitService.getbusinessunit(this.id);
      });
	}

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('buId', this.id);
    formData.append('IsInternal', bu.IsInternal);
    formData.append('buName', bu.buName);
    formData.append('buAddress', bu.buAddress);
    formData.append('countryId', bu.countryId);
    formData.append('country', "");
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    formData.append('buCode', bu.buCode);
    this.businessunitService.Save(formData).then(r =>  {
      this.success = 'Business Unit Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/business-unit']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
