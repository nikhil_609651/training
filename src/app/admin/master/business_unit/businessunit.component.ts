import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'businessunit',
    template: `<router-outlet></router-outlet>`
})
export class BusinessUnitComponent {
	constructor(private router: Router) {
	}
}
