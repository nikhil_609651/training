import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { BusinessUnitFormService } from 'app/admin/master/business_unit/services/businessunit-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'businessunit-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./businessunit-list.html')
})
export class BusinessUnitListComponent implements OnInit{
  selectedcountryAll: any;
  pagedummy: number;
  selectedAll: any;
  attachment_deleted_id: any;
  attachmentmessage: string;
  buId: any;
  API_URL: string;
  attachmentGroup: any[];
  attachments: any;
  countryGroups: any;
  businessunitname: any;
  CountryName: any;
  CountryId: any;
  Address: any;
  Id: any;
  checked: string[] = [];
 public businessunit: any=[];
  error: any;
  viewBusinessUnitForm: FormGroup;
  public start: number = 1;
  public loading: boolean;
  public rows: Array<any> = [];
  // For headers
  public columns: Array<any> = [
    { title: 'ID', name: 'Id', sort: true, },
    { title: 'Business Unit Name', name: 'BuName', sort: true, status:0 },
    { title: 'Business Unit Code', name: 'BuCode', sort: false },
    { title: 'Country', name: 'CountryName', sort: true, filter:true, status:0 },
    { title: 'Actions', name: 'actions', sort: false }
  ];

  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number =10;
  public maxSize:number = 5;
  public numPages:number = 0;
  public length:number = 5;
  public next = '';
  public businessunit_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams()
  
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;
  
  constructor(private router: Router,
        private businessunitService: BusinessUnitFormService,
        private fb: FormBuilder,
        private configuration: Configuration,
        private _sharedService : SharedService) {

          /**user privileges**/	
          this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
            this.privileges = privileges;
            this.showListPage = true;
          });
          this.itemsPerPage = configuration.itemsPerPage;
          this.params.set('limit', configuration.itemsPerPage.toString());
          this.params.set('countryfilter', '');
          this.params.set('UserId', localStorage.getItem('user_nameId'));
          this.rows = configuration.rows;
          this.viewBusinessUnitForm = fb.group({
                  'bBusinessUnit': [''],
                  'bBusinessUnitId': [''],
                  'bAddress': [''],
                  'bCountry': [''],
                  'buCode': ['']
              });
              var vForm = this.viewBusinessUnitForm;
  }

  ngOnInit() {
    this.getbusinessunitList(this.params);
    this.getcountries();
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  // For getting Country List
  getcountries(){
    this.businessunitService.getCountryFilterList().then(r =>  {
        this.countryGroups = r;
        this.loading = false;
      });
  }
  // For getting BusinessUnit List
  getbusinessunitList(params: any) {
    this.loading = true;
    this.businessunitService.getBusinessUnitList(params).then(response => {
      this.businessunit = response['result'];
      if(this.businessunit.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
  // Filteration

  selectAll(event) {
        this.checkArr = [];
        for (var i = 0; i < this.countryGroups.length; i++) {
          this.countryGroups[i].selected = this.selectedcountryAll;
          if(event.target.checked){
              this.checkArr.push(this.countryGroups[i].countryId.toString());
          } else {
              this.checkArr = [];
          }
        }
         this.params.set('countryfilter', this.checkArr.toString());
    }
 checkArr = [];
 checkIfAllSelected(option, event,item) {

  this.selectedcountryAll = this.countryGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkArr.indexOf(key);
  if(event.target.checked) {
    this.checkArr.push(event.target.value);
  } else {
    this.checkArr.splice(index,1);
  }
    this.params.set('countryfilter', this.checkArr.toString());
}

   // For Applying Filtering to List
   apply(){
    this.getbusinessunitList(this.params);
      setTimeout(function () {
        jQuery('#ViewCountryModal').modal('toggle');
      }.bind(this) , 0);
      jQuery('ViewCountryModal').modal('hide');
      jQuery('body').removeClass('modal-open');
      jQuery('.modal-backdrop').remove();
    }
// Filteration
   getAllLists(params: any) {
    this.loading = true;
    this.businessunitService.getBusinessUnitList(params).then(response => {
      this.businessunit = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getbusinessunitList(params);

    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {

    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());

    var sortParam = '';
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    this.getbusinessunitList(this.params);
  }

  addbusinessunit() {
    this.router.navigate(['./home/master/business-unit/add']);
  }

  editbusinessunit(id) {

    this.router.navigate(['./home/master/business-unit/edit/', id]);
  }

  deletebusinessunit(businessunit){
    this.businessunit_deleted_id = businessunit;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  // For Deleting BusinessUnit
  deletebusinessunitconfirm(businessunit){
      this.businessunit_deleted_id = businessunit.buId;
      this.businessunitService.deletebusinessunit(  this.businessunit_deleted_id).then(r =>  {
        this.getbusinessunitList(this.params);
        this.deletemessage = 'Business Unit Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }

  view(data){
     if (data.buId != 0) {
         this.businessunitname = data.buName;
         this.Id = data.buId;
         this.Address = data.buAddress;
         this.CountryId = data.countryId;
         this.CountryName = data.country.name;
         this.viewBusinessUnitForm.controls['bBusinessUnit'].setValue(this.businessunitname);
         this.viewBusinessUnitForm.controls['bBusinessUnitId'].setValue(this.Id);
         this.viewBusinessUnitForm.controls['bAddress'].setValue(this.Address);
         this.viewBusinessUnitForm.controls['bCountry'].setValue(this.CountryName);
         this.viewBusinessUnitForm.controls['buCode'].setValue(data.buCode);
     }
  }

  // For File Attachments
  attachment(id){
    this.buId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadBusinessUnitFiles?FileId=';
    this.businessunitService.getallbusinessunitattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0) {
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
          id :this.attachments[i].id,
          referenceId:this.attachments[i].referenceId,
          shortFileName : this.attachments[i].shortFileName,
          fileName:this.attachments[i].fileName,
          createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else{
        this.getbusinessunitList(this.params);
        this.attachmentmessage='No Attachments Found'
        /*setTimeout(function () {
          jQuery('#ViewAttachmentModal').modal('toggle');
        }.bind(this), 1000);*/
      }
      return this.attachmentGroup;
    })
  }

  // For Deleting Attachments
  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.businessunitService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.buId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  // For Sorting
  Sort(param,order){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getbusinessunitList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
