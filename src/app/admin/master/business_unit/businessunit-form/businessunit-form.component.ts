import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { BusinessUnitFormService } from 'app/admin/master/business_unit/services/businessunit-form.service';

@Component({
  selector: 'businessunit-form',
  template: require('./businessunit-form.html')
})
export class BusinessUnitFormComponent {
  savebutton: boolean =true;
  IsInternalUse = false;

  addressReq: string;
  countryReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() businessunit;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  businessunitForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public filesToUpload: Array<File>;
  public selectedFileNames: string[] = [];

  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public businessunit_id = '';
  public nameReq;
  public buCodeReq = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public businessunitGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private businessunitService: BusinessUnitFormService, private router: Router,
    private route: ActivatedRoute) {

    this.getCountryList();
    this.businessunitForm = fb.group({
      'buName': ['', Validators.compose([Validators.required])],
      'buId': [''],
      'buAddress': [''],
      'IsInternal': [''],
      'countryId': ['',Validators.compose([Validators.required])],
      'FileType':[],
      'buCode' : ['', Validators.required]
    });
    var cForm = this.businessunitForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.businessunit_id = params['id'];
      });
  }
  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  // For getting Country List
  getCountryList(){
        this.businessunitService.getCountryList().then(result => {
          this.countryList = result;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  // For getting BusinessUnit List
getbusinessunits(params){
  this.businessunitService.getBusinessUnitList(params).then(r =>  {
      this.businessunitGroups = r.result;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.businessunit && change.businessunit.currentValue) {
      this.loading = false;
      this.businessunitForm.controls['buId'].setValue(change.businessunit.currentValue.buId);
      this.businessunitForm.controls['buName'].setValue(change.businessunit.currentValue.buName);
      this.businessunitForm.controls['buAddress'].setValue(change.businessunit.currentValue.buAddress);
      this.businessunitForm.controls['countryId'].setValue(change.businessunit.currentValue.countryId);
      this.businessunitForm.controls['IsInternal'].setValue(change.businessunit.currentValue.isInternal);
      this.businessunitForm.controls['FileType'].setValue(this.filetype);
      this.businessunitForm.controls['buCode'].setValue(change.businessunit.currentValue.buCode);

    } else{
      this.loading = false;
    }
  }


  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  selectAll(event) {
    if(event.target.checked)
    {
      this.IsInternalUse=true;
    }
    else
    {
      this.IsInternalUse=false;
    }
  }

  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="Business Unit Name is Required";
    this.addressReq="Business Unit Address is Required";
    this.countryReq="Country is Required";
    if(this.buCodeErrorMsg == '') {
      this.buCodeReq = "Business Unit Code is Required"
    }
    this.clearerror();
    this.formSubmited = true;
    if(this.businessunitForm.valid && this.ErrorList.length==0 && this.buCodeErrorMsg == '') {
      validPost.uploadedFile = this.filetype;
      validPost.IsInternal=this.IsInternalUse;
      //this.savebutton = false;
      this.saved.emit(validPost);
      //this.savebutton = false;
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror(){
    this.error="";
  }

  clearmsgs(){
    if(this.businessunitForm.controls['buName'].hasError('required') || (this.businessunitForm.controls['buName'].touched))
    {
      this.nameReq="";
    }
    if(this.businessunitForm.controls['countryId'].hasError('required') || (this.businessunitForm.controls['countryId'].touched))
    {
      this.countryReq="";
    }
    if(this.businessunitForm.controls['buAddress'].hasError('required') || (this.businessunitForm.controls['buAddress'].touched))
    {
      this.addressReq="";
    }
    if(this.businessunitForm.controls['buCode'].hasError('required') || (this.businessunitForm.controls['buCode'].touched))
    {
      this.buCodeReq="";
    }
    this.error="";
    this.ErrorList=[];
  }

  buCodeErrorMsg = '';
  onBUcodeChange(value) {
    var expression = /^([0-9]|[a-z])+([0-9a-z]+)$/i;
    if(value.match(expression)) {
      this.buCodeReq = '';
      this.buCodeErrorMsg = '';
      this.businessunitService.checkBusinessUnitCode(value.toUpperCase()).then(response => {
        response ? this.buCodeErrorMsg = 'Code already exists' : this.buCodeErrorMsg = '';
      })
    } else {
      this.buCodeErrorMsg = 'Please enter only alphanumeric characters';
      return false;
    }
  }

  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}