import {Injectable} from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class BusinessUnitFormService {
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public countrydropdownUrl = '';
    public countryFilterUrl = '';
    public businessuniturl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    public checkBUcodeUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveBusinessUnit';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/SaveBusinessUnit';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteBusinessUnitByID?id=';
        this.countrydropdownUrl = _configuration.ServerWithApiUrl+'Masters/GetAllCountries';
        this.countryFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetBusinessUnitCountryFilter';
        this.businessuniturl = _configuration.ServerWithApiUrl+'Masters/GetBusinessUnitByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllBusinessUnitAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteBusinessUnitAttachments?Id='; 
        this.checkBUcodeUrl = _configuration.ServerWithApiUrl+'Masters/CheckBusinessUnitCode?BUCode='; 
    }
    
    // List API for BusinessUnit
    getBusinessUnitList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getCountryList(): Promise<any> {
        return this.authHttp.get(this.countrydropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCountryFilterList(): Promise<any> {
        return this.authHttp.get(this.countryFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //Save API for BusinessUnit
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();
        formData.append('buId', bu.buId);
        formData.append('buName', bu.buName);
        formData.append('buAddress', bu.buAddress);
        formData.append('countryId', bu.countryId);
        formData.append('country', "");
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let body = JSON.stringify({ 'foo': 'bar' });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.set('Upload-Content-Type',bu.FileType);
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletebusinessunit(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getallbusinessunitattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getbusinessunit(id: string) {
        return this.authHttp.get(this.businessuniturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    checkBusinessUnitCode(buCode) {
        return this.authHttp.get(this.checkBUcodeUrl +  buCode)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
