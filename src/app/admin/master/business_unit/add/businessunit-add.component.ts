import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BusinessUnitFormService } from 'app/admin/master/business_unit/services/businessunit-form.service';

@Component({
  selector: 'add',
  template: require('./businessunit-add.html')
})
export class BusinessUnitAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private businessunitService: BusinessUnitFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';
  }
  getCountryList(){
        this.businessunitService.getCountryList().then(r => {
          this.countryList = r;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  onSave(bu: any) {
    bu.buId=0;
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('buId', '0');
    formData.append('IsInternal', bu.IsInternal);
    formData.append('buName', bu.buName);
    formData.append('buAddress', bu.buAddress);
    formData.append('countryId', bu.countryId);
    formData.append('buCode', bu.buCode);
    formData.append('country', "");
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.businessunitService.Save(formData).then(r =>  {
      this.success = 'Business Unit Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/business-unit']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
