import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OwnershipFormService } from 'app/admin/master/ownership/services/ownership-form.service';

@Component({
  selector: 'add',
  template: require('./ownership-add.html')
})
export class OwnershipAddComponent implements OnInit{

  public error = {};
  public errorpass = '';
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private ownershipService: OwnershipFormService) {

  }
  ngOnInit() {
    this.page = 'add';
  }
  onSave(owner: any) {
    owner.ownershipId=0;
      let files = owner.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
          formData.append("FileType", files[i], files[i]['name']);
      }
      formData.append('ownershipId', '0');
      formData.append('name', owner.name);
      formData.append('description', owner.description);
      formData.append('IsDeleted', "");
    this.ownershipService.Save(formData).then(r =>  {
      this.success = 'Ownership Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/ownership-info']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
