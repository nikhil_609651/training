import { Component, EventEmitter, Input, Output , OnInit, ViewChild} from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { OwnershipFormService } from 'app/admin/master/ownership/services/ownership-form.service';

@Component({
  selector: 'ownership-form',
  template: require('./ownership-form.html')
})
export class OwnershipFormComponent {
  savebutton: boolean = true;
  nameReq: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() ownership;
  @Input() error;
  @Input() page:string;
  @Output() saved = new EventEmitter();
  ownershipForm:FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public owner: Array<string>;
  public ownership_id = '';
  public ownershipGroups =[];
  params: URLSearchParams = new URLSearchParams();
  constructor(private fb: FormBuilder,
    private ownershipService: OwnershipFormService, private router: Router, private route: ActivatedRoute) {
    this.ownershipForm = fb.group({
       'ownershipId': [''],
      'name':['', Validators.compose([Validators.required])],
      'description':[''],
      'FileType':[''],

    });
    var cForm = this.ownershipForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.ownership_id = params['id'];
      });
  }
getownerships(params){
  this.ownershipService.getownerships(params).then(r =>  {
      this.ownershipGroups = r.results;
      this.loading = false;
    })
}

  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.ownership && change.ownership.currentValue) {
      this.loading = false;
      this.ownershipForm.controls['ownershipId'].setValue(change.ownership.currentValue.ownershipId);
      this.ownershipForm.controls['name'].setValue(change.ownership.currentValue.name);
      this.ownershipForm.controls['description'].setValue(change.ownership.currentValue.description);
      this.ownershipForm.controls['FileType'].setValue(this.filetype);
      console.log(this.ownershipForm.controls);

    } else{
      this.loading = false;
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="Ownership Name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.ownershipForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);

    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
   // alert('hai');
    this.error="";
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.ownershipForm.controls['name'].hasError('required') || (this.ownershipForm.controls['name'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}