import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../../../app.constants';
import { OwnershipFormService } from 'app/admin/master/ownership/services/ownership-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'ownership-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./ownership-list.html')
})
export class OwnershipListComponent implements OnInit{
  description: any;
  ownership: any;
  attachment_deleted_id: any;
  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  ownershipId: any;
  viewOwnershipForm: FormGroup;

    Id: any;
  public ownerships: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: 'ID', name: 'Id', sort: true, },
    {title: 'Name', name: 'Name',sort: true},
    {title: 'Description', name: 'description', sort: false},
    {title: 'Actions',  name: 'actions', sort: false}
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public ownership_deleted_id = '';
  public deleteattachmentmessage='';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private ownershipService: OwnershipFormService,
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.rows = configuration.rows;
      this.viewOwnershipForm = fb.group({
        'ownership': [''],
        'ownershipId': [''],
        'description': [''],
      });
      var vForm = this.viewOwnershipForm;
  }

  ngOnInit() {
    this.getownershipList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getownershipList(params: any) {
    this.loading = true;
    this.ownershipService.getOwnershipList(params).then(response => {
      this.ownerships = response['result'];
      if(this.ownerships.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.ownershipService.getOwnershipList(params).then(response => {
      this.ownerships = response['result']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getownershipList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getownershipList(this.params);
  }

  addownership() {
    this.router.navigate(['./home/master/ownership-info/add']);
  }

  editownership(id) {
    this.router.navigate(['./home/master/ownership-info/edit/', id]);
  }

  deleteownership(ownership){
    this.ownership_deleted_id = ownership;
  }

  deletemessageclear() {
    this.deletemessage = '';
  }

  deleteownershipconfirm(ownership) {
      this.ownership_deleted_id= ownership.ownershipId;
      this.ownershipService.deleteownership(  this.ownership_deleted_id).then(r =>  {
        this.getownershipList(this.params);
        this.deletemessage = "Ownership Info Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  view(data) {
    if (data.ownershipId != 0) {
        this.ownership = data.name;
        this.ownershipId = data.ownershipId;
        this.description = data.description;
        this.viewOwnershipForm.controls['ownership'].setValue(this.ownership);
        this.viewOwnershipForm.controls['ownershipId'].setValue(this.ownershipId);
        this.viewOwnershipForm.controls['description'].setValue(this.description);
    }
  }

  viewclose(){
    setTimeout(function () {
      jQuery('#ViewOwnership').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment() {
    setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }

  attachment(id) {
    this.ownershipId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadOwnershipFiles?FileId=';
    this.ownershipService.getallownershipattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
          id :this.attachments[i].id,
          referenceId:this.attachments[i].referenceId,
          shortFileName : this.attachments[i].shortFileName,
          fileName:this.attachments[i].fileName,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else {
        this.getownershipList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    });
  }

  deleteattachment(id) {
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear() {
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id) {
      this.ownershipService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.ownershipId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  Sort(param,order,sortstatus){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc') {
      this.params.set('ordering', param);
    }
    this.getownershipList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
