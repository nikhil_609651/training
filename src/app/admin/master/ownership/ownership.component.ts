import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'ownership',
    template: `<router-outlet></router-outlet>`
})
export class OwnershipComponent {
	constructor(private router: Router) {
	}
}