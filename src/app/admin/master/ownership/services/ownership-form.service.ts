import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../../app.constants';

@Injectable()
export class OwnershipFormService {

    private managerUrl = '';
    private userGroupUrl = '';
    private locality ='';
    private sublocality ='';
    private ownershipurl='';
    private saveurl='';
    private geturl='';
    private updateurl='';
    private deleteurl='';
    private attachmenturl='';
    private downloadUrl='';
    private deleteattachmentUrl;

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.ownershipurl = _configuration.ServerWithApiUrl+'Masters/Ownership';
        this.geturl   =_configuration.ServerWithApiUrl+'Masters/GetOwnershipByID?id=';
        this.saveurl   =_configuration.ServerWithApiUrl+'Masters/SaveOwnership';
        this.deleteurl = _configuration.ServerWithApiUrl+'Masters/DeleteOwnershipByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllOwnershipAttachments?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadOwnershipFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteOwnershipAttachments?Id=';
    }

    getOwnershipList(params: any): Promise<any> {
        return this.authHttp.get(this.ownershipurl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getownerships(params: any): Promise<any> {
        return this.authHttp.get(this.ownershipurl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
   
    getownership(id: string) {
        return this.authHttp.get(this.geturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    Save(ownership: any): Promise<any>  {
        console.log('Ownership',ownership);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl, ownership)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(ownership: any): Promise<any> {
        const formData = new FormData();
        formData.append('ownershipId', ownership.ownershipId);
        formData.append('name', ownership.name);
        formData.append('description', ownership.description);
        formData.append('IsDeleted', "");
        formData.append('FileURL', null);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);

    }
    
    deleteownership(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteurl;
        return this.authHttp
        .post(this.deleteurl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getallownershipattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }
}