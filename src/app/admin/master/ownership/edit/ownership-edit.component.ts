import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { OwnershipFormService } from 'app/admin/master/ownership/services/ownership-form.service';

@Component({
  selector: 'edit',
  template: require('./ownership-edit.html')
})
export class OwnershipEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public ownership: any;
  public page = 'edit';
public ownershipdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private ownershipService: OwnershipFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
    			this.ownership = this.ownershipService.getownership(this.id)
      });

	}

  onSave(owner: any) {
    let files = owner.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('ownershipId',this.id);
    formData.append('name', owner.name);
    formData.append('description', owner.description);
    formData.append('IsDeleted', "");
    this.ownershipService.Save(formData).then(r =>  {
      this.success = 'Ownership Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/ownership-info']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
    console.log('this.error', this.error);
    let detail = e.detail
    if(e.status == '401'){
      this.router.navigate(['./']);
    }else
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
