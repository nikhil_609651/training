import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PolFormService } from 'app/admin/master/pol/service/pol-service';

@Component({
  selector: 'pol-edit',
  template: require('./pol-edit.html')
})
export class PolEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public pol: any;
  public page = 'edit';
public businessunitdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private polService: PolFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
                this.pol = this.polService.getpol(this.id);
      });
	}

  onSave(pol: any) {
    console.log(pol)
    let files = pol.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append('FileType', files[i], files[i]['name']);
    }
    formData.append('Id', this.id);
    formData.append('Name', pol.Name);
    formData.append('Grade', pol.Grade);
    formData.append('UOMId', pol.UOMId);
    formData.append('IsLubricant', pol.IsLubricant)
    formData.append('UOM', "");
    formData.append('PackSize', pol.PackSize)
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.polService.Save(formData).then(r =>  {
      this.success = 'POL Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/pol']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
