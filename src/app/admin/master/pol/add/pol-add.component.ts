import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PolFormService } from 'app/admin/master/pol/service/pol-service';

@Component({
  selector: 'add',
  template: require('./pol-add.html')
})
export class PolAddComponent implements OnInit{
  uomList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private polService: PolFormService) {
     

  }
  ngOnInit() {
    this.page = 'add';
  }
  getUomList(){
    this.polService.getUomList().then(r => {
      this.uomList = r;
      console.log("uom",this.uomList)
  })
      .catch(r => {
          this.handleError(r);
      })

}
  onSave(pol: any) {
    pol.Id=0;
    let files = pol.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Name', pol.Name);
    formData.append('Grade', pol.Grade);
    formData.append('UOMId', pol.UOMId);
    formData.append('IsLubricant', pol.IsLubricant)
    formData.append('UOM', "");
    formData.append('PackSize', pol.PackSize)
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.polService.Save(formData).then(r =>  {
      this.success = 'POL Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/pol']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
