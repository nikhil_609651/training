import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class PolFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public uomdropdownUrl = '';
    public polurl = '';
    public attachmenturl='';
    public deleteattachmentUrl='';
    
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/POL/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SavePOL';
        //this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdatePOL';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeletePOLByID?id=';
        this.uomdropdownUrl = _configuration.ServerWithApiUrl+'Masters/UOM';
        this.polurl = _configuration.ServerWithApiUrl+'Masters/GetPOLByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllPOLAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeletePOLAttachments?Id=';
    }
    
    // List API for POL
    getPolList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getUomList(): Promise<any> {
        return this.authHttp.get(this.uomdropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Save API for POL
    Save(pol: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, pol)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(pol: any): Promise<any> {
        const formData = new FormData();
        formData.append('Id', pol.Id);
        formData.append('Name', pol.Name);
        formData.append('Grade', pol.Grade);
        formData.append('UOMId', pol.UOMId);
        formData.append('UOM', "");
        formData.append('IsLubricant', pol.IsLubricant);
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let body = JSON.stringify({ 'foo': 'bar' });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.set('Upload-Content-Type',pol.FileType);
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletepol(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getpol(id: string) {
        return this.authHttp.get(this.polurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getallpolattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
