import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'pol',
    template: `<router-outlet></router-outlet>`
})
export class PolComponent {
	constructor(private router: Router) {
	}
}
