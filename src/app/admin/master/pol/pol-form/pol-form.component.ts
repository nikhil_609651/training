import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { PolFormService } from 'app/admin/master/pol/service/pol-service';

@Component({
  selector: 'pol-form',
  template: require('./pol-form.html')
})
export class PolFormComponent {
  savebutton: boolean =true;

  isLubricantSection= false;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() pol;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  polForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public filesToUpload: Array<File>;
  public selectedFileNames: string[] = [];

  public file = '';
  public uomList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public pol_id = '';
  public nameReq;
  public uomReq;
  public packSizeReq = '';
  public IsLubricant=false;
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public polGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private polService: PolFormService, private router: Router,
    private route: ActivatedRoute) {

    this.getUomList();

    this.polForm = fb.group({
      'Name': ['', Validators.compose([Validators.required])],
      'Id': [''],
      'Grade': [''],
      'UOMId': ['', Validators.compose([Validators.required])],
      'PackSize': [''],
      'IsLubricant': [''],
      'FileType':[],
    });
    var cForm = this.polForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.pol_id = params['id'];
      });
  }
  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  // For getting uom List
  getUomList(){
        this.polService.getUomList().then(r => {
          this.uomList = r.result;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  // For getting POL List
getpol(params){
  this.polService.getPolList(params).then(r =>  {
      this.polGroups = r.result;
      this.loading = false;
    })
}
_keyPress(event: any) {
  const pattern = /[0-9\.\ ]/;
  let inputChar = String.fromCharCode(event.charCode);

  if (!pattern.test(inputChar)) {
    // invalid character, prevent input
    event.preventDefault();
  }
}
  ngOnChanges(change) {
   //console.log('abc',change.pol.currentValue)
    this.loading = this.page == 'add' ? false : true;
    if (change.pol && change.pol.currentValue) {
      this.loading = false;
      this.polForm.controls['Id'].setValue(change.pol.currentValue.id);
      this.polForm.controls['Name'].setValue(change.pol.currentValue.name);
      this.polForm.controls['Grade'].setValue(change.pol.currentValue.grade);
      this.polForm.controls['UOMId'].setValue(change.pol.currentValue.uomId);

        this.polForm.controls['IsLubricant'].setValue(change.pol.currentValue.isLubricant);
        this.IsLubricant=change.pol.currentValue.isLubricant;
        if(change.pol.currentValue.isLubricant==true)
        {
          this.isLubricantSection=true;
        }
        else
        {
          this.isLubricantSection=false;
        }

      //this.polForm.controls['IsLubricant'].setValue(change.pol.currentValue.isLubricant)
      this.polForm.controls['PackSize'].setValue(change.pol.currentValue.packSize)
      this.polForm.controls['FileType'].setValue(this.filetype);

    } else{
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  selectAll(event) {
    this.IsLubricant=event.target.checked;

    if(event.target.checked) {
      this.IsLubricant=true;
      this.isLubricantSection=true;
    } else {
      this.isLubricantSection=false;
      this.IsLubricant=false;
    }
  }

  onSubmit(validPost) {
    this.formSubmited = true;
    this.nameReq="POL Name is required";
    this.uomReq="UOM is required";
    if(this.IsLubricant) {
      validPost.PackSize == '' ? this.packSizeReq = "Pack Size is required" : this.packSizeReq = '';
    }
    this.clearerror();
    if(this.polForm.valid && this.ErrorList.length==0 && this.packSizeReq == '') {
      validPost.uploadedFile = this.filetype;
      validPost.IsLubricant=this.IsLubricant;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror(){
    this.error="";
  }

  clearmsgs() {
    if(this.polForm.controls['Name'].hasError('required') || (this.polForm.controls['Name'].touched)) {
      this.nameReq="";
    }
    if(this.polForm.controls['UOMId'].hasError('required') || (this.polForm.controls['UOMId'].touched)) {
      this.uomReq="";
    }
    this.packSizeReq = "";
    this.isLubricantSection=false;
    this.IsLubricant=false;
    this.error="";
    this.ErrorList=[];
  }

  handleError(e: any) {
      this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}