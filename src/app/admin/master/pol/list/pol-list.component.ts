import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PolFormService } from 'app/admin/master/pol/service/pol-service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'pol-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./pol-list.html')
})
export class PolListComponent implements OnInit{
  isLubricant: boolean=false;
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  attachmentGroup: any;
  attachment_deleted_id: any;
  deleteattachmentmessage: any;
  PackSize: any;
  UOMId: any;
  grade: any;
  name: any;
  uomName: any;
  polname: any;
  UomName: any;
  uomId: any;
  Grade: any;
  Id: any;

  public pol: any=[];
  error: any;
  viewPolForm: FormGroup;

  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [

    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'POL', name: 'Name', sort: true },
    { title: 'Fuel Type', name: 'Name', sort: false },
    { title: 'Grade', name: 'Grade', sort: true },
    { title: 'UOM', name: 'uom', sort: false },
    { title: 'Actions', className: ['text-center'], name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public pol_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private polService: PolFormService,
    private fb: FormBuilder,
    private _sharedService : SharedService,
    private configuration: Configuration) {

    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
      this.privileges = privileges;
      this.showListPage = true;
    });
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.rows = configuration.rows;
    this.viewPolForm = fb.group({
                  'Name': [''],
                  //'Id': [''],
                  'grade': [''],
                  'UomName': [''],
                  'PackSize': [''],
              });
    var vForm = this.viewPolForm;
  }

  ngOnInit() {
    this.getPolList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }
 
  getPolList(params: any) {
    this.loading = true;
    this.polService.getPolList(params).then(response => {
      this.pol = response['result']
      if(this.pol.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
     
   getAllLists(params: any) {
    this.loading = true;
    this.polService.getPolList(params).then(response => {
      this.pol = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getPolList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getPolList(this.params);
  }

  addpol() {
    this.router.navigate(['./home/master/pol/add']);
  }

  editpol(id) {
    this.router.navigate(['./home/master/pol/edit/', id]);
  }
  
  deletepol(pol){
    this.pol_deleted_id = pol;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletepolconfirm(pol){
      this.pol_deleted_id= pol.id;
      this.polService.deletepol(  this.pol_deleted_id).then(r =>  {
        this.getPolList(this.params);
        this.deletemessage = "POL Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  view(data) {
    if (data.id != 0) {
        this.name = data.name;
        this.Id = data.id;
        this.grade = data.grade;
        this.UOMId = data.uomId;
        this.UomName = data.uom.name;
        this.PackSize=data.packSize;
        this.isLubricant=data.isLubricant;
        this.viewPolForm.controls['Name'].setValue(this.name);
        this.viewPolForm.controls['grade'].setValue(this.grade);
        this.viewPolForm.controls['UomName'].setValue(this.UomName);
        this.viewPolForm.controls['PackSize'].setValue(this.PackSize);
    }
  }

  viewclose() {
  setTimeout(function () {
      jQuery('#ViewPOL').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment() {
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }

  attachment(id) {
    this.Id=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadPOLFiles?FileId=';
    this.polService.getallpolattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0) {
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
          id :this.attachments[i].id,
          referenceId:this.attachments[i].referenceId,
          shortFileName : this.attachments[i].shortFileName,
          fileName:this.attachments[i].fileName,
          createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else {
        this.getPolList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    });
  }

  // For Deleting Attachments
  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
    this.polService.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.Id);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";
    }).catch(r =>  {
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
  }

  Sort(param,order,sortstatus){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getPolList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
