import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'truck',
    template: `<router-outlet></router-outlet>`
})
export class TruckComponent {
	constructor(private router: Router) {
	}
}