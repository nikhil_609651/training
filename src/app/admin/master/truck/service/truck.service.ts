import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../../app.constants';

@Injectable()
export class TruckFormService {

    private geturl='';
    private saveurl='';
    private editurl='';
    private updateurl='';
    private deleteurl='';
    private companydropdownurl='';
    private companyFilterurl='';
    private downloadUrl='';
    private deleteattachmentUrl='';
    private attachmenturl='';
    
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.geturl = _configuration.ServerWithApiUrl+'Masters/Truck';
        this.saveurl   =_configuration.ServerWithApiUrl+'Masters/SaveTruck';
        this.updateurl   =_configuration.ServerWithApiUrl+'Masters/UpdateTruck';
        this.deleteurl = _configuration.ServerWithApiUrl+'Masters/DeleteTruckByID?id=';
        this.editurl = _configuration.ServerWithApiUrl+'Masters/GetTruckByID?id=';
        this.companydropdownurl = _configuration.ServerWithApiUrl+'Masters/TransportCompany/';
        this.companyFilterurl = _configuration.ServerWithApiUrl+'Masters/GetTruckCompanyFilter/';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadTruckFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteTruckAttachments?Id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllTruckAttachments?id=';
    }

    getTruckList(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    gettrucks(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCompanyList(): Promise<any> {
        return this.authHttp.get(this.companydropdownurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCompanyFilterList(): Promise<any> {
        return this.authHttp.get(this.companyFilterurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
   
    gettruck(id: string) {
        return this.authHttp.get(this.editurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    Save(truck: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl, truck)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(truck: any): Promise<any> {
        const formData = new FormData();
        formData.append('truckId', truck.truckId);
        formData.append('regNo', truck.regNo);
        formData.append('CompanyId', truck.regName);
        formData.append('Address', truck.address);
        formData.append('IsDeleted', "");
        formData.append('FileURL', null);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getalltruckattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deletetruck(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteurl;
        return this.authHttp
            .post(this.deleteurl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}