import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TruckFormService } from 'app/admin/master/truck/service/truck.service';

@Component({
  selector: 'add',
  template: require('./truck-add.html')
})
export class TruckAddComponent implements OnInit{

  public error = {};
  public errorpass = '';
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private truckService: TruckFormService) {

  }
  ngOnInit() {
    this.page = 'add';
  }
  onSave(truck: any) {
    truck.truckId=0;
      let files = truck.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
      }
      formData.append('truckId', '0');
      formData.append('Name', truck.regNo);
      formData.append('companyId', truck.companyId);
      formData.append('Address', truck.Address);
      formData.append('IsDeleted', "");
    this.truckService.Save(formData).then(r =>  {
      this.success = 'Truck Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/truck']);
         // this.router.navigate('./admin/area-manager');
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    /*console.log(e)*/
    this.error = e;
      let detail = e.detail;
      console.log(detail);
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
