import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TruckFormService } from 'app/admin/master/truck/service/truck.service';

@Component({
  selector: 'truck-edit',
  template: require('./truck-edit.html')
})
export class TruckEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public truck: any;
  public page = 'edit';


// public localityGroups: any;
// public usersublocality: any;
public truckdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private truckService: TruckFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

         this.id = params['id'];
          console.log( this.id )
    	  this.truck = this.truckService.gettruck( this.id )
      });

	}

  onSave(truck: any) {
    let files = truck.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
      }
    formData.append('truckId',  this.id );
    formData.append('Name', truck.regNo);
    formData.append('companyId', truck.companyId);
    formData.append('Address', truck.Address);
    formData.append('IsDeleted', "");
    this.truckService.Save(formData).then(r =>  {
      this.success = 'Truck Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/truck']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
