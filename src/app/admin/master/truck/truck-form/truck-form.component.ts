import { Component, EventEmitter, Input, Output , OnInit} from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { TruckFormService } from 'app/admin/master/truck/service/truck.service';

@Component({
  selector: 'truck-form',
  template: require('./truck-form.html')
})
export class TruckFormComponent {
  savebutton: boolean =true;
  companyList: any;
  companyReq: string;
  nameReq: string;
  image: string;
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() truck;
  @Input() error;
  @Input() page:string;
  @Output() saved = new EventEmitter();
  truckForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public owner: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public truck_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();


  public truckGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private truckService: TruckFormService, private router: Router, private route: ActivatedRoute) {
    //this.getareamanagers();
   // this.getCityGroups();
    //this.onselectednew();
    this.getCompanyList();
    this.truckForm = fb.group({
      'truckId': [''],
      'regNo':['', Validators.compose([Validators.required])],
      'companyId':['',Validators.compose([Validators.required])],
      'Address':[''],
      'FileType':[],

    });
    var cForm = this.truckForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.truck_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
  }


  onselected(){
    this.options = this.optionss;
    //console.log(this.options)
  }
  getCompanyList(){
    this.truckService.getCompanyList().then(r => {
      this.companyList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })

  }

gettrucks(params){
  this.truckService.gettrucks(params).then(r =>  {
      this.truckGroups = r.results;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('truck_id',this.truck_id);
  this.params.set('list','list');
  this.truckService.gettrucks(params).then(r =>  {
    this.truckGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.truckGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.truckGroups.length; i++) {
      var Obj = {
        label : this.truckGroups[i].name,
        value :  +this.truckGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.truckGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.truck && change.truck.currentValue) {
      this.loading = false;
      this.truckForm.controls['truckId'].setValue(change.truck.currentValue.truckId);
      this.truckForm.controls['regNo'].setValue(change.truck.currentValue.regNo);
      this.truckForm.controls['companyId'].setValue(change.truck.currentValue.companyId);
      this.truckForm.controls['Address'].setValue(change.truck.currentValue.address);
      this.truckForm.controls['FileType'].setValue(this.filetype);

    } else{
      this.loading = false;
    }
  }

  updated($event,control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  onSubmit(validPost) {
    this.nameReq="Registration Number is required";
    this.companyReq="You must select a company";
    this.clearerror();
    this.formSubmited = true;
    if(this.truckForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);

    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearerror(){
    this.error="";
  }
  msgClear(event) {
    if(event.keyCode == 8){
      this.nameReq="";
      this.error="";
    }
  }
  clearmsgs(){
    if(this.truckForm.controls['regNo'].hasError('required') || (this.truckForm.controls['regNo'].touched))
    {
      this.nameReq="";
    }
    if(this.truckForm.controls['companyId'].hasError('required') || (this.truckForm.controls['companyId'].touched))
    {
      this.companyReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}