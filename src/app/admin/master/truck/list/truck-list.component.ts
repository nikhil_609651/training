import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../../../app.constants';
import { TruckFormService } from 'app/admin/master/truck/service/truck.service';
import { FormBuilder } from '@angular/forms/src/form_builder';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'truck-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./truck-list.html')
})
export class TruckListComponent implements OnInit{

  selectedcompanyAll: any;
  address: any;
  companyName: any;
  truck: any;
  viewTruckForm: any;
  attachment_deleted_id: any;
  deleteattachmentmessage: any;
  truckId: any;
  API_URL: string;
  attachments: any;
  attachmentmessage: string;
  attachmentGroup: any;
  Id: any;
  public trucks: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: 'ID', name: 'Id', sort: true, },
    {title: 'Registration No', name: 'regno',sort: true},
    {title: 'Transport Company Name', name: 'regname', sort: true, filter:true},
    {title: 'Address', name: 'address',sort: true},
    {title: 'Actions', className: ['text-center'], name: 'actions', sort: false}
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public truck_deleted_id = '';
  public deletemessage='';
  checked: string[] = [];
  selectedAll: any;
  companyGroups: any;
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,private fb: FormBuilder,private _sharedService : SharedService,
    private truckService: TruckFormService, private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('CompanyFilter', '');
      this.rows = configuration.rows;
      this.viewTruckForm = fb.group({
        'regNo': [''],
        'companyId': [''],
        'address': [''],
      });
      var vForm = this.viewTruckForm;
  }

  ngOnInit() {
    this.gettruckList(this.params);
    this.getcompany();
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  gettruckList(params: any) {
    this.loading = true;
    this.truckService.getTruckList(params).then(response => {
      this.trucks = response['result']
      console.log('truck',response)
      if(this.trucks.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.truckService.getTruckList(params).then(response => {
      this.trucks = response['result']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.gettruckList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.gettruckList(this.params);
  }

  getcompany(){
    this.truckService.getCompanyFilterList().then(r =>  {
        this.companyGroups = r;
        this.loading = false;
      });
  }

   // Filteration
   selectAll(event) {
    this.checkArr = [];
    for (var i = 0; i < this.companyGroups.length; i++) {
      this.companyGroups[i].selected = this.selectedcompanyAll;
      if(event.target.checked){
          this.checkArr.push(this.companyGroups[i].id.toString());
      } else {
          this.checkArr = [];
      }
    }
    this.params.set('CompanyFilter', this.checkArr.toString());
  }

  checkArr = [];
  checkIfAllSelected(option, event,item) {
    this.selectedcompanyAll = this.companyGroups.every(function(item:any) {
      return item.selected == true;
    })
    var key = event.target.value.toString();
    var index = this.checkArr.indexOf(key);
    if(event.target.checked) {
      this.checkArr.push(event.target.value);
    } else {
      this.checkArr.splice(index,1);
    }
    this.params.set('CompanyFilter', this.checkArr.toString());
  }

  // For Applying Filtering to List
  apply(){
    this.gettruckList(this.params);
    setTimeout(function () {
      jQuery('#ViewCompanyModal').modal('toggle');
    }.bind(this) , 0);
    jQuery('ViewCompanyModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  // Filteration
  view(data){
    if (data.truckId != 0) {
      this.truck = data.regNo;
      this.companyName = data.transportCompany.name;
      this.address = data.address;      ;
      this.viewTruckForm.controls['regNo'].setValue(this.truck);
      this.viewTruckForm.controls['companyId'].setValue(this.companyName);
      this.viewTruckForm.controls['address'].setValue(this.address);
    }
  }

  viewclose(){
    setTimeout(function () {
        jQuery('#View').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }

  addtruck() {
    this.router.navigate(['./home/master/truck/add']);
  }

  edittruck(id) {
    this.router.navigate(['./home/master/truck/edit/', id]);
  }

  deletetruck(truck){
    this.truck_deleted_id = truck.truckId;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletetruckconfirm(truck){
      this.truck_deleted_id= truck;
      this.truckService.deletetruck(  this.truck_deleted_id).then(r =>  {
        this.gettruckList(this.params);
        this.deletemessage = "Truck Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
   this.truckService.deleteattachment(id).then(r =>  {
      this.attachment(this.truckId);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";
    }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
    })
  }

  attachment(id){
     this.truckId=id;
     this.attachmentmessage='';
     this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadTruckFiles?FileId=';
      this.truckService.getalltruckattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
          var LongArray = [];
          for(var i = 0; i < this.attachments.length; i++) {
            let ext=this.attachments[i].location;
            var Obj = {
              id :this.attachments[i].id,
              referenceId:this.attachments[i].referenceId,
              shortFileName : this.attachments[i].shortFileName,
              fileName:this.attachments[i].fileName,
              createdDate: this.attachments[i].createdDate,
              ext :  ext.substr(ext.lastIndexOf('.') + 1),
            };
            LongArray.push(Obj);
            this.attachmentGroup=LongArray;
          }
      } else{
        this.gettruckList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    });
  }

  Sort(param,order){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.gettruckList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
