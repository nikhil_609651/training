import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'receiver',
    template: `<router-outlet></router-outlet>`
})
export class ReceiverComponent {
	constructor(private router: Router) {
	}
}
