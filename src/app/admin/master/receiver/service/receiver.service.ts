import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class ReceiverFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public sitedropdownUrl = '';
    public customerdropdownUrl = '';
    public sectiondropdownUrl = '';
    public departmentdropdownUrl = '';
    public geturl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Receiver/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveReceiver';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateReceiver';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteReceiverByID?id=';
        this.sitedropdownUrl = _configuration.ServerWithApiUrl+'Masters/Site';
        this.customerdropdownUrl = _configuration.ServerWithApiUrl+'Masters/Customer';
        this.sectiondropdownUrl = _configuration.ServerWithApiUrl+'Masters/Section';
        this.departmentdropdownUrl = _configuration.ServerWithApiUrl+'Masters/Department';
        this.geturl = _configuration.ServerWithApiUrl+'Masters/GetReceiverByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllReceiverAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteReceiverAttachments?Id=';
    }   
   
    // List API for Receiver
    getReceiverList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getSiteList(): Promise<any> {
        return this.authHttp.get(this.sitedropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSectionList(): Promise<any> {
        return this.authHttp.get(this.sectiondropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCustomerList(): Promise<any> {
        return this.authHttp.get(this.customerdropdownUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDepartmentList(): Promise<any> {
        return this.authHttp.get(this.departmentdropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // Save API for Receiver
    Save(receiver: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, receiver)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(receiver: any): Promise<any> {
        const formData = new FormData();
        formData.append('receiverId', receiver.receiverId);
        formData.append('firstName', receiver.firstName);
        formData.append('lastName', receiver.lastName);
        formData.append('unidNo', receiver.unidNo);
        formData.append('siteId', receiver.siteId);
        formData.append('site', "");
        formData.append('sectionId', receiver.sectionId);
        formData.append('section', "");
        formData.append('customerId', receiver.customerId);
        formData.append('customer', "");
        formData.append('departmentId', receiver.departmentId);
        formData.append('department', "");
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletereceiver(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getreceiver(id: string) {
        return this.authHttp.get(this.geturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getallreceiverattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getReceiver(id: string) {
        return this.authHttp.get(this.listUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
