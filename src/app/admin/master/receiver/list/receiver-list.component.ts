import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { ReceiverFormService } from 'app/admin/master/receiver/service/receiver.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'receiver-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./receiver-list.html')
})
export class ReceiverListComponent implements OnInit{

  siteGroups: any;
  selectedsiteAll: any;
  department: any;
  API_URL: string;
  attachments: any;
  attachmentmessage: string;
  attachmentGroup: any;
  receiverId(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  attachment_deleted_id(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  deleteattachmentmessage: any;
  viewReceiverForm: FormGroup;
  section: any;
  customer: any;
  site: any;
  unid: any;
  lname: any;
  fname: any;
  public receiver: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true,  },
    { title: 'First Name', name: 'FirstName', sort: true },
    { title: 'Last Name', name: 'LastName', sort: true },
    { title: 'UNID', name: 'UNID', sort: true },
    { title: 'Site', name: 'Site', sort: true,filter:true },
    { title: 'Customer', name: 'Customer', sort: true },
    { title: 'Section', name: 'Section', sort: true },
    { title: 'Actions',  name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public receiver_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private receiverService: ReceiverFormService,
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {
      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('ordering', null);
      this.params.set('UserId', localStorage.getItem('user_nameId'));
      this.rows = configuration.rows;
      this.viewReceiverForm = fb.group({
        'firstname': [''],
        'lname': [''],
        'unid': [''],
        'site': [''],
        'customer': [''],
        'section': [''],
        'department': [''],
    });
    var vForm = this.viewReceiverForm;
  }

  ngOnInit() {
    this.getreceiverList(this.params);
    this.getSites();
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getreceiverList(params: any) {
    this.loading = true;
    this.receiverService.getReceiverList(params).then(response => {
      this.receiver = response['result']
      if(this.receiver.length>0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else{
        this.page=1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getSites(){
    this.receiverService.getSiteList().then(r =>  {
        this.siteGroups = r.result;
        this.loading = false;
      });
  }
   
  getAllLists(params: any) {
    this.loading = true;
    this.receiverService.getReceiverList(params).then(response => {
      this.receiver = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getreceiverList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getreceiverList(this.params);
  }

  addreceiver() {
    this.router.navigate(['./home/master/receiver/add']);
  }

  editreceiver(id) {
    this.router.navigate(['./home/master/receiver/edit/', id]);
  }

  deletereceiver(receiver){
    this.receiver_deleted_id = receiver;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletereceiverconfirm(receiver){
      this.receiver_deleted_id= receiver.receiverId;
      this.receiverService.deletereceiver(this.receiver_deleted_id).then(r =>  {
        this.getreceiverList(this.params);
        this.deletemessage = "Receiver Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  // Filteration
  selectAll(event) {
    this.checkArr = [];
    for (var i = 0; i < this.siteGroups.length; i++) {
      this.siteGroups[i].selected = this.selectedsiteAll;
      if(event.target.checked){
          this.checkArr.push(this.siteGroups[i].siteId.toString());
      } else {
          this.checkArr = [];
      }
    }
    this.params.set('sitefilter', this.checkArr.toString());
  }

  checkArr = [];
  checkIfAllSelected(option, event,item) {
    this.selectedsiteAll = this.siteGroups.every(function(item:any) {
      return item.selected == true;
    })
    var key = event.target.value.toString();
    var index = this.checkArr.indexOf(key);
    if(event.target.checked) {
      this.checkArr.push(event.target.value);
    } else {
      this.checkArr.splice(index,1);
    }
    this.params.set('sitefilter', this.checkArr.toString());
  }

  // For Applying Filtering to List
  apply() {
  this.getreceiverList(this.params);
    setTimeout(function () {
      jQuery('#ViewSiteModal').modal('toggle');
    }.bind(this) , 0);
    jQuery('ViewSiteModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  // Filteration
  view(data){
    if (data.receiverId != 0) {
        this.fname = data.firstName;
        this.lname = data.lastName;
        this.unid = data.unidNo;
        this.site = data.sitemaster.name;
        this.customer = data.customermaster.firstName;
        this.section = data.section.sectionName;
        this.department = data.department.name;
        this.viewReceiverForm.controls['firstname'].setValue(this.fname);
        this.viewReceiverForm.controls['lname'].setValue(this.lname);
        this.viewReceiverForm.controls['unid'].setValue(this.unid);
        this.viewReceiverForm.controls['site'].setValue(this.site);
        this.viewReceiverForm.controls['customer'].setValue(this.customer);
        this.viewReceiverForm.controls['section'].setValue(this.section);
        this.viewReceiverForm.controls['department'].setValue(this.department);
    }
  }

  viewclose() {
    setTimeout(function () {
      jQuery('#ViewReceiver').modal('toggle');
    }.bind(this), 0);
  }

  viewcloseattachment() {
    setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }

  // For File Attachments
  attachment(id) {
    this.receiverId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadReceiverFiles?FileId=';
    this.receiverService.getallreceiverattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
          id :this.attachments[i].id,
          referenceId:this.attachments[i].referenceId,
          shortFileName : this.attachments[i].shortFileName,
          fileName:this.attachments[i].fileName,
          createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else{
        this.getreceiverList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    })
  }

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.receiverService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.receiverId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  Sort(param,order,sortstatus){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getreceiverList(this.params);
  }

  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
