import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ReceiverFormService } from 'app/admin/master/receiver/service/receiver.service';

@Component({
  selector: 'receiver-edit',
  template: require('./receiver-edit.html')
})
export class ReceiverEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public receiver: any;
  public page = 'edit';
public receiverdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private receiverService: ReceiverFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
                this.receiver = this.receiverService.getreceiver(this.id);
      });
 
	}

  onSave(receiver: any) {
    let files = receiver.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('receiverId', this.id);
    formData.append('firstName', receiver.firstName);
    formData.append('lastName', receiver.lastName);
    formData.append('unidNo', receiver.unidNo);
    formData.append('siteId', receiver.siteId);
    formData.append('site', "");
    formData.append('sectionId', receiver.sectionId);
    formData.append('section', "");
    formData.append('customerId', receiver.customerId);
    formData.append('customer', "");
    formData.append('departmentId', receiver.departmentId);
    formData.append('department', "");
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.receiverService.Save(formData).then(r =>  {
      this.success = 'Receiver Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/receiver']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
