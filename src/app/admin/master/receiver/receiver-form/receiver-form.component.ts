import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { ReceiverFormService } from 'app/admin/master/receiver/service/receiver.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';

@Component({
  selector: 'receiver-form',
  template: require('./receiver-form.html')
})
export class ReceiverFormComponent {
  userId: string;
  savebutton: boolean = true;

  customerReq: string;
  sectionReq: string;
  departmentReq: string;
  unidReq: string;
  siteReq: string;
  lnameReq: string;
  nameReq: string;
  departmentList: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() receiver;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();

  receiverForm:FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public siteList: Array<string>;
  public sectionList: Array<string>;
  public customerList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public receiver_id = '';
  image= '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public receiverGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,private issuerService: IssuerFormService,
    private receiverService: ReceiverFormService, private router: Router,
    private route: ActivatedRoute) {

      this.userId=localStorage.getItem('user_nameId');
    this.getSiteList();
    this.getSectionList();
    this.getCustomerList();
    this.getDepartmentList();

    this.receiverForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'receiverId': [''],
      'lastName': [''],
      'siteId': ['', Validators.compose([Validators.required])],
      'customerId': ['', Validators.compose([Validators.required])],
      'sectionId': ['', Validators.compose([Validators.required])],
      'departmentId': ['', Validators.compose([Validators.required])],
      'unidNo': ['', Validators.compose([Validators.required])],
      'siteName': [''],
      'customerName': [''],
      'sectionName': [''],
      'FileType':[],
    });
    var cForm = this.receiverForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.receiver_id = params['id'];
      });
  }


  onselected(){
    this.options = this.optionss;
  }

  getSiteList(){

    this.issuerService.getSiteList(this.userId).then(r => {
      console.log('r',r)
                this.siteList = r;
                console.log(' this.siteList', this.siteList)
            })
                .catch(r => {
                    this.handleError(r);
                });
  }
  getDepartmentList(){

    this.receiverService.getDepartmentList().then(r => {
      this.departmentList = r.result;
      console.log(r)
  })
      .catch(r => {
          this.handleError(r);
})
}
  getSectionList(){

    //this.loading = true;
    this.receiverService.getSectionList().then(r => {
      this.sectionList = r.result;
      console.log( 'section', this.sectionList)
      // this.loading = false;
  })
      .catch(r => {
          this.handleError(r);
      })
}
getCustomerList(){
    //this.loading = true;
    this.receiverService.getCustomerList().then(r => {
      this.customerList = r.result;
      console.log( 'customer', this.customerList)
      console.log(r)
      // this.loading = false;
  })
      .catch(r => {
          this.handleError(r);
      })
}
getreceiver(params){
  this.receiverService.getReceiverList(params).then(r =>  {
      this.receiverGroups = r.result;
      this.loading = false;
    })
}
updated($event, control) {
  const files = $event.target.files || $event.srcElement.files;
  var reader: FileReader = new FileReader();
  var file: File = files[0];

  reader.onloadend = (e) => {
    this.image = btoa(reader.result);
    console.log('image', this.image)
    control.setValue(btoa(reader.result));
  }
  reader.readAsBinaryString(file);
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('receiver_id',this.receiver_id);
  this.params.set('list','list');
  this.receiverService.getReceiverList(params).then(r =>  {
    this.receiverGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.receiverGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.receiverGroups.length; i++) {
      var Obj = {
        label : this.receiverGroups[i].name,
        value :  +this.receiverGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.receiverGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.receiver && change.receiver.currentValue) {
      this.loading = false;
      //console.log("set value=",change.receiver.currentValue.firstName);
      this.receiverForm.controls['receiverId'].setValue(change.receiver.currentValue.receiverId);
      this.receiverForm.controls['firstName'].setValue(change.receiver.currentValue.firstName);
      this.receiverForm.controls['lastName'].setValue(change.receiver.currentValue.lastName);
      this.receiverForm.controls['unidNo'].setValue(change.receiver.currentValue.unidNo);
      this.receiverForm.controls['siteId'].setValue(change.receiver.currentValue.siteId);
      this.receiverForm.controls['customerId'].setValue(change.receiver.currentValue.customerId);
      this.receiverForm.controls['sectionId'].setValue(change.receiver.currentValue.sectionId);
      this.receiverForm.controls['departmentId'].setValue(change.receiver.currentValue.departmentId);

      //console.log(change.businessunit.currentValue.manager[0].id);
     // var result = change.businessunit.currentValue.manager.map(a => a.id);
      //console.log(result);

      console.log(this.receiverForm.controls);

    } else{
      this.loading = false;
    }
  }

  onChange(event) {

    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onRemoveFile(event) {
    //this.filetype.removeFromQueue();
    this.filelist = [];
    this.filetype = [];
    this.ErrorList=[];
  }
  onSubmit(validPost) {
    this.nameReq="First Name is required";
    this.siteReq="You must select a site";
    this.unidReq="UNID No: is required";
    this.customerReq="You must select a customer";
    this.departmentReq="You must select a department";
    this.sectionReq="You must select a section";
    this.clearerror();
    this.formSubmited = true;
    if(this.receiverForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  OnUnidChange(event){

    console.log(event)
    if(event = ''){
      this.error="";
    }
  }
  clearmsgs(){
    this.receiverForm.reset();
    if(this.receiverForm.controls['firstName'].hasError('required') || (this.receiverForm.controls['firstName'].touched))
    {
      this.nameReq="";
    }
    if(this.receiverForm.controls['lastName'].hasError('required') || (this.receiverForm.controls['lastName'].touched))
    {
      this.lnameReq="";
    }
    if(this.receiverForm.controls['siteId'].hasError('required') || (this.receiverForm.controls['siteId'].touched))
    {
      this.siteReq="";
    }
    if(this.receiverForm.controls['unidNo'].hasError('required') || (this.receiverForm.controls['unidNo'].touched))
    {
      this.unidReq="";
    }
    if(this.receiverForm.controls['customerId'].hasError('required') || (this.receiverForm.controls['customerId'].touched))
    {
      this.customerReq="";
    }
    if(this.receiverForm.controls['sectionId'].hasError('required') || (this.receiverForm.controls['sectionId'].touched))
    {
      this.sectionReq="";
    }
    if(this.receiverForm.controls['departmentId'].hasError('required') || (this.receiverForm.controls['departmentId'].touched))
    {
      this.departmentReq="";
    }
    this.error="";
    this.ErrorList=[];
  }

  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}