import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReceiverFormService } from 'app/admin/master/receiver/service/receiver.service';

@Component({
  selector: 'add',
  template: require('./receiver-add.html')
})
export class ReceiverAddComponent implements OnInit{
  siteList: any;
  sectionList: any;
  customerList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
  private receiverService: ReceiverFormService) {
  }
  ngOnInit() {
    this.page = 'add';

  }
  getSectionList(){
        this.receiverService.getSectionList().then(r => {
          this.sectionList = r;
      })
          .catch(r => {
              this.handleError(r);
          })

  }
  getCustomerList(){
    this.receiverService.getCustomerList().then(r => {
      this.sectionList = r;
  })
      .catch(r => {
          this.handleError(r);
      })

}
getSiteList(){
    this.receiverService.getSiteList().then(r => {
      this.siteList = r;
  })
      .catch(r => {
          this.handleError(r);
      })

}
  onSave(receiver: any) {
    receiver.receiverId=0;
    console.log(receiver)
    let files = receiver.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('receiverId', '0');
    formData.append('firstName', receiver.firstName);
    formData.append('lastName', receiver.lastName);
    formData.append('unidNo', receiver.unidNo);
    formData.append('siteId', receiver.siteId);
    formData.append('site', "");
    formData.append('sectionId', receiver.sectionId);
    formData.append('section', "");
    formData.append('customerId', receiver.customerId);
    formData.append('customer', "");
    formData.append('departmentId', receiver.departmentId);
    formData.append('department', "");
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.receiverService.Save(formData).then(r =>  {
      this.success = 'Receiver Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/receiver']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
