import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { StoragetankFormService } from 'app/admin/master/storagetank/service/storagetank-form.service';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'storagetank-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./storagetank-list.html')
})
export class StoragetankListComponent implements OnInit{
  selectedsiteAll: any;
  selectedPolAll: any;
  selectedtankAll: any;
  tankDetails: any;
  viewStorageTankForm: any;
  typeGroups: any;
  polGroups: any;
  checkedsite: string[] = [];
  checkedpol: string[] = [];
  checkedtank: string[] = [];
  selectedAll: any;
  siteGroups: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  sId: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  public st: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'id', sort: true },
    { title: 'Site', name: 'Site', sort: true,filter:true },
    { title: 'Tank No', name: 'TankNo', sort: true },
    { title: 'Tank Name', name: 'TankName', sort: true },
    { title: 'Tank Capacity', name: 'TankCapacity', sort: true },
    { title: 'POL', name: 'POL', sort: false,filter:true },
    { title: 'Type of Tank', name: 'tankType', sort: false,filter:true},
    { title: 'Actions', className: ['text-center'], name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public st_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private fb: FormBuilder,private _sharedService : SharedService,
    private stService: StoragetankFormService, private configuration: Configuration) {

      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
        this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('SiteFilter', '');
      this.params.set('POLFilter', '');
      this.params.set('TankTypeFilter', '');
      this.params.set('UserId', localStorage.getItem('user_nameId'));
      this.rows = configuration.rows;
      this.viewStorageTankForm = fb.group({
        'TankNo': [''],
        'TankName': [''],
        'Site': [''],
        'TankCapacity': [''],
        'HeightOrLength': [''],
        'POL': [''],
        'Diameter': [''],
        'Radius': [''],
        'TankMfg': [''],
        'Remarks': [''],
    });
    var vForm = this.viewStorageTankForm;
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_nameId');
    this.getstoragetankList(this.params);
    this.totalfeilds = this.columns.length;
    this.getSites(userId);
    this.getPOL();
    this.getTankType();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getstoragetankList(params: any) {
    this.loading = true;
    this.stService.getStorageTankList(params).then(response => {
      this.st = response['result']
      if(this.st.length>0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      } else{
        this.page=1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

   getAllLists(params: any) {
    this.loading = true;
    this.stService.getStorageTankList(params).then(response => {
      this.st = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getstoragetankList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getstoragetankList(this.params);
  }

  view(data){
    if (data.storageTankId != 0) {
        this.viewStorageTankForm.controls['TankNo'].setValue(data.tankNo);
        this.viewStorageTankForm.controls['TankName'].setValue(data.tankName);
        this.viewStorageTankForm.controls['Site'].setValue(data.site.name);
        this.viewStorageTankForm.controls['TankCapacity'].setValue(data.capacity);
        this.viewStorageTankForm.controls['HeightOrLength'].setValue(data.heightOrLength);
        this.viewStorageTankForm.controls['POL'].setValue(data.pol.name);
        this.viewStorageTankForm.controls['Diameter'].setValue(data.diameter);
        this.viewStorageTankForm.controls['Radius'].setValue(data.radius);
        this.viewStorageTankForm.controls['TankMfg'].setValue(data.tankMfg);
        this.viewStorageTankForm.controls['Remarks'].setValue(data.remarks);
        //for displaying Calibration Chart
        if(data.tankCaliberationChart.length>0)
        {
          this.tankDetails=data.tankCaliberationChart;
        }
    }
  }

  addstoragetank() {
    this.router.navigate(['./home/master/storage-tank/add']);
  }

  editstoragetank(id) {
    this.router.navigate(['./home/master/storage-tank/edit/', id]);
  }

  deletestoragetank(st){
    this.st_deleted_id = st;
  }

  deletemessageclear(){
    this.deletemessage = '';
  }

  deletestoragetankconfirm(st){
      this.st_deleted_id= st.storageTankId;
      this.stService.deletestoragetank(this.st_deleted_id).then(r =>  {
        this.getstoragetankList(this.params);
        this.deletemessage = "Storage Tank Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  // For File Attachments
  attachment(id){
    this.sId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadStorageTankFiles?FileId=';
    this.stService.getallstoragetankattachments(id).then(response => {
      this.attachments=response;
      if(this.attachments.length > 0){
        var LongArray = [];
        for(var i = 0; i < this.attachments.length; i++) {
          let ext=this.attachments[i].location;
          var Obj = {
            id :this.attachments[i].id,
            referenceId:this.attachments[i].referenceId,
            shortFileName : this.attachments[i].shortFileName,
            fileName:this.attachments[i].fileName,
            createdDate: this.attachments[i].createdDate,
            ext :  ext.substr(ext.lastIndexOf('.') + 1),
          };
          LongArray.push(Obj);
          this.attachmentGroup=LongArray;
        }
      } else{
        this.getstoragetankList(this.params);
        this.attachmentmessage='No Attachments Found'
      }
      return this.attachmentGroup;
    })
  }

  viewclose() {
    setTimeout(function () {
        jQuery('#ViewSTModal').modal('toggle');
      }.bind(this), 0);
  }

  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }

  // For Deleting Attachments
  deleteattachment(id){
    this.attachment_deleted_id = id;
  }

  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.stService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.sId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  // For Sorting
  Sort(param,order){
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getstoragetankList(this.params);
  }
  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

  // Filteration
  selectAll(item, event) {
    if(item === 'Site'){
        this.checkArr = [];
        for (var i = 0; i < this.siteGroups.length; i++) {
          this.siteGroups[i].selected = this.selectedsiteAll;
          if(event.target.checked){
              this.checkArr.push(this.siteGroups[i].siteId.toString());
          } else {
              this.checkArr = [];
          }
        }
        this.params.set('SiteFilter', this.checkArr.toString());
    }
    if(item === 'Pol'){
      this.checkPolArr=[]
      for (var i = 0; i < this.polGroups.length; i++) {
        this.polGroups[i].selected = this.selectedPolAll;
        if(event.target.checked){
            this.checkPolArr.push(this.polGroups[i].id.toString());
        } else {
            this.checkPolArr = [];
        }
      }
      this.params.set('POLFilter', this.checkPolArr.toString());
    }
    if(item === 'Tank'){
      this.checkTankArr=[]
      for (var i = 0; i < this.typeGroups.length; i++) {
        this.typeGroups[i].selected = this.selectedtankAll;
        if(event.target.checked){
            this.checkTankArr.push(this.typeGroups[i].id.toString());
        } else {
            this.checkTankArr = [];
        }
      }
      this.params.set('TankTypeFilter', this.checkTankArr.toString());
    }
 }
  checkArr = [];
  checkPolArr=[];
  checkTankArr=[];
  checkIfAllSelected(option, event,item) {
    if(item=='Site') {
      this.selectedsiteAll = this.siteGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkArr.indexOf(key);
      if(event.target.checked) {
        this.checkArr.push(event.target.value);
      } else {
        this.checkArr.splice(index,1);
      }
      this.params.set('SiteFilter', this.checkArr.toString());
    }

    if(item === 'Pol'){
      this.selectedPolAll = this.polGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkPolArr.indexOf(key);
      if(event.target.checked) {
        this.checkPolArr.push(event.target.value);
      } else {
        this.checkPolArr.splice(index,1);
      }
      this.params.set('POLFilter', this.checkPolArr.toString());
    }

    if(item === 'Tank'){
      this.selectedtankAll = this.typeGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkTankArr.indexOf(key);
      if(event.target.checked) {
        this.checkTankArr.push(event.target.value);
      } else {
        this.checkTankArr.splice(index,1);
      }
        this.params.set('TankTypeFilter', this.checkTankArr.toString());
    }

  }

  //For Applying Filtering to List
  apply(item){
    this.getstoragetankList(this.params);
    if(item === 'Site'){
      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this) , 0);
    }
    if(item === 'Pol'){
      setTimeout(function () {
        jQuery('#ViewPOLModal').modal('toggle');
      }.bind(this) , 0);
    }

    if(item === 'Tank'){
      setTimeout(function () {
        jQuery('#ViewTankTypeModal').modal('toggle');
      }.bind(this) , 0);
    }
    jQuery('ViewSiteModal').modal('hide');
    jQuery('ViewPOLModal').modal('hide');
    jQuery('ViewTankTypeModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }

  // Filteration
  getSites(userId){
    this.stService.getSiteFilterList(userId).then(r =>  {
        this.siteGroups = r;
        this.loading = false;
    });
  }

  getPOL(){
    this.stService.getPolFilterList().then(r =>  {
        this.polGroups = r;
        this.loading = false;
    });
  }

  // For Applying Filtering to List
  applyType(){
    this.getstoragetankList(this.params);
  }

  getTankType(){
    this.stService.getTypeList().then(r =>  {
        this.typeGroups = r;
        this.loading = false;
      });
  }
}
