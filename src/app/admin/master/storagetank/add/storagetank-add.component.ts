import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoragetankFormService } from 'app/admin/master/storagetank/service/storagetank-form.service';

@Component({
  selector: 'add',
  template: require('./storagetank-add.html')
})
export class StoragetankAddComponent implements OnInit{
  typeList: any;
  siteList: any;
  polList: any;
  stUnitList= [];
  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router, private stService: StoragetankFormService) {
  }

  ngOnInit() {
    this.page = 'add';
  }

  getPolList() {
    this.stService.getPolList().then(r => {
      this.polList = r;
    }).catch(r => {
      this.handleError(r);
    })
  }

  getSiteList(){ 
    this.stService.getSiteList().then(r => {
      this.siteList = r;
    })
    .catch(r => {
      this.handleError(r);
    })
  }

  getTypeList(){ 
    this.stService.getTypeList().then(r => {
      this.typeList = r;
    })
    .catch(r => {
      this.handleError(r);
    })
  }

  onSave(st: any) {
    let files = st.uploadedFile;
    let stUnitList=st.StorageTankUnitDetails;
    let formData = new FormData();
    let calibrationList =  JSON.stringify(stUnitList);
    let diameter = st.diameter ? st.diameter : 0 ;
    let radius = st.radius ? st.radius : 0 ;
    let heightLength = st.heightLength ? st.heightLength : 0 ;
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append("StorageTankUnitDetails",calibrationList);
    formData.append('stId', '0');
    formData.append('TankNo', st.tankNo);
    formData.append('TankName', st.tankName);
    formData.append('Capacity', st.tankCapacity);
    formData.append('HeightOrLength', heightLength);
    formData.append('Radius', radius);
    formData.append('Diameter', diameter);
    formData.append('TankMfg', st.tankMfg);
    formData.append('TankTypeId', st.typeId);
    formData.append('Remarks', st.remarks);
    formData.append('SiteId', st.siteId);
    formData.append('POLId', st.polId);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.stService.Save(formData).then(r =>  {
      console.log('r',r)
      this.success = 'Storage Tank Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/storage-tank']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}