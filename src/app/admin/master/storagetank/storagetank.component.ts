import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'storagetank',
    template: `<router-outlet></router-outlet>`
})
export class StoragetankComponent {
	constructor(private router: Router) {
	}
}
