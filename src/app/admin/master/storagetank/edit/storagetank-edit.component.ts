import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StoragetankFormService } from 'app/admin/master/storagetank/service/storagetank-form.service';


@Component({
  selector: 'storagetank-edit',
  template: require('./storagetank-edit.html')
})
export class StoragetankEditComponent implements OnInit{

  public error = {};
	public success = '';
  public st: any;
  public page = 'edit';
  public stdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private stService: StoragetankFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
          let id = params['id'];
          console.log('ID',id)
                this.st = this.stService.getstoragetank(id);
                console.log('St',this.st)
      });
 
	}

  onSave(st: any) {
    let files = st.uploadedFile;
    let stUnitList=st.StorageTankUnitDetails;
    let formData = new FormData();
    
    let calibrationList =  JSON.stringify(stUnitList);
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    let heightLength = st.heightLength ? st.heightLength : 0 ;
    let diameter = st.diameter ? st.diameter : 0 ;
    let radius = st.radius ? st.radius : 0 ;
    formData.append("StorageTankUnitDetails",calibrationList);
    formData.append('stId', st.stId);
    formData.append('TankNo', st.tankNo);
    formData.append('TankName', st.tankName);
    formData.append('Capacity', st.tankCapacity);
    formData.append('HeightOrLength', heightLength);
    formData.append('Radius', radius);
    formData.append('Diameter', diameter);
    formData.append('TankMfg', st.tankMfg);
    formData.append('TankTypeId', st.typeId);
    formData.append('Remarks', st.remarks);
    formData.append('SiteId', st.siteId);
    formData.append('POLId', st.polId);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.stService.Save(formData).then(r =>  {
      this.success = 'Storage Tank Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/storage-tank']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
