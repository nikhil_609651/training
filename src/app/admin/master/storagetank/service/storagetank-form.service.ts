import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class StoragetankFormService{
    public IsLubricant=false;
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public sitedropdownUrl = '';
    public siteFilterUrl = '';
    public polFilterUrl = '';
    public poldropdownUrl = '';
    public typedropdownUrl = '';
    public geturl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/StorageTank/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveStorageTank';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateStorageTank';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteStorageTankByID?id=';
        this.sitedropdownUrl = _configuration.ServerWithApiUrl+'Masters/Site';
        this.siteFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetStorageTankSiteFilter';
        this.polFilterUrl = _configuration.ServerWithApiUrl+'Masters/GetPOLFilter';
        this.poldropdownUrl = _configuration.ServerWithApiUrl+'Masters/GetLubricantPOL?Lubricant=';
        this.typedropdownUrl = _configuration.ServerWithApiUrl+'Masters/TankType';
        this.geturl = _configuration.ServerWithApiUrl+'Masters/GetStorageTankByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllStorageTankAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteStorageTankAttachments?Id=';
    }
   
    // List API for Storage tank
    getStorageTankList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getSiteList(): Promise<any> {
        return this.authHttp.get(this.sitedropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteFilterList(userId): Promise<any> {
        return this.authHttp.get(this.siteFilterUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolFilterList(): Promise<any> {
        return this.authHttp.get(this.polFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolList(): Promise<any> {
        return this.authHttp.get(this.poldropdownUrl+this.IsLubricant)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTypeList(): Promise<any> {
        return this.authHttp.get(this.typedropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //Save API for Storage tank
    Save(st: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, st)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(st: any): Promise<any> {
        const formData = new FormData();
        formData.append('stId', st.stId);
        formData.append('tankNo', st.tankNo);
        formData.append('tankName', st.tankName);
        formData.append('tankCapacity', st.tankCapacity);
        formData.append('heightLength', st.heightLength);
        formData.append('siteId', st.siteId);
        formData.append('site', "");
        formData.append('polId', st.polId);
        formData.append('typeId', st.typeId);
        formData.append('pol', "");
        formData.append('diameter', st.diameter);
        formData.append('tankmfg', st.tankmfg);
        formData.append('radius', st.radius);
        formData.append('remarks', st.remarks);
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletestoragetank(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getstoragetank(id: string) {
        return this.authHttp.get(this.geturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getallstoragetankattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
