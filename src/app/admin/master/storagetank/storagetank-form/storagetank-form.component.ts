import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { StoragetankFormService } from 'app/admin/master/storagetank/service/storagetank-form.service';
import { IssuerFormService } from 'app/admin/master/issuer/service/issuer.service';


@Component({
  selector: 'storagetank-form',
  template: require('./storagetank-form.html')
})
export class StoragetankFormComponent {
  userId: string;
  CLength: number;
  heightFlag: boolean = false;
  siteReq: string;
  vol=0;
  height=0;
  nullalertmessage: string = '';
  savebutton: boolean = true;

  polReq: string;
  capacityReq: string;
  nameReq: string;
  noReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() st;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();

  stForm:FormGroup;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public siteList: Array<string>;
  public polList: Array<string>;
  public typeList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public st_id = '';
  public tempHeightArray = [];
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public stGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,private issuerService: IssuerFormService,
    private stService: StoragetankFormService, private router: Router,
    private route: ActivatedRoute) {
      this.userId=localStorage.getItem('user_nameId');
    this.getSiteList();
    this.getPolList();
    this.getTypeList();

    }

  ngOnInit() {
    this.heightFlag = false;
      this.route.params.forEach((params: Params) => {
          this.st_id = params['id'];
      });

    if(this.page === 'add'){
    this.stForm = new FormGroup({
      stId:new FormControl(''),
      tankNo: new FormControl('',Validators.required),
      tankName: new FormControl('',Validators.required),
      tankCapacity: new FormControl('',Validators.required),
      siteId: new FormControl('',Validators.required),
      polId:  new FormControl('',Validators.required),
      typeId: new FormControl(''),
      siteName: new FormControl(''),
      customerName: new FormControl(''),
      sectionName: new FormControl(''),
      typeName: new FormControl(''),
      heightLength: new FormControl(''),
      diameter: new FormControl(''),
      radius: new FormControl(''),
      tankMfg: new FormControl(''),
      remarks: new FormControl(''),
      FileType:new FormControl(''),
      StorageTankUnitDetails: new FormArray([
        this.initSection(),
      ]),
    });
    var cForm = this.stForm;

  }
if(this.page === 'edit'){
  this.stForm = new FormGroup({
    stId:new FormControl(''),
    tankNo: new FormControl('',Validators.required),
    tankName: new FormControl('',Validators.required),
    tankCapacity: new FormControl('',Validators.required),
    siteId: new FormControl('',Validators.required),
    polId:  new FormControl('',Validators.required),
    typeId: new FormControl(''),
    siteName: new FormControl(''),
    customerName: new FormControl(''),
    sectionName: new FormControl(''),
    typeName: new FormControl(''),
    heightLength: new FormControl(''),
    diameter: new FormControl(''),
    radius: new FormControl(''),
    tankMfg: new FormControl(''),
    remarks: new FormControl(''),
    FileType:new FormControl(''),
    StorageTankUnitDetails: new FormArray([
    //this.initSection(),
    ]),
  });
  var cForm = this.stForm;
    }
  }

  initSection(data = []) {
     let height = data['height']  ? data['height'] : 0;
     let vol = data['volume']  ? data['volume'] : 0;
     this.height=height;
     this.vol=vol;
    return new FormGroup({
      Height: new FormControl(height),
      Volume:new FormControl(vol),
    });
  }

  addSection(i) {
    this.heightFlag = false;
    const control = (<FormGroup>(<FormArray>this.stForm.get('StorageTankUnitDetails')).controls[i]).value;
    let height = control['Height'];
    let volume = control['Volume'];
    if ((height <= 0 || volume <= 0) &&  this.heightErrorMsg == "") {
      this.nullalertmessage = "Height or Volume Can't be Zero!";
    } else if (i!=0) {
      this.nullalertmessage = "";
      const calibControl = this.stForm.get('StorageTankUnitDetails').value;
      this.tempHeightArray.forEach(height => {
        if (height == this.height) {
          this.heightFlag = true;
        }
      });
      if (this.nullalertmessage == "" && this.heightFlag && this.heightErrorMsg == "") {
        this.nullalertmessage = "Caliberation is already added for this Height!";
      } else if (!this.heightFlag && this.heightErrorMsg == "") {
        const control = <FormArray>this.stForm.get('StorageTankUnitDetails');
        control.push(this.initSection());
        this.tempHeightArray.push(height);
        this.heightFlag = false;
      }
    } else {
      const control = <FormArray>this.stForm.get('StorageTankUnitDetails');
      control.push(this.initSection());
    }
  }

  getCalibrationChart(form) {
    return form.controls.StorageTankUnitDetails.controls;
  }

  removeSection(i) {
   const control = <FormArray>this.stForm.get('StorageTankUnitDetails');
   control.removeAt(i);
   this.height = 0;
   this.nullalertmessage = '';
   this.heightErrorMsg = '';
   this.tempHeightArray.slice(i, 1);
  }

  remove(i,j){
    const control =  <FormArray>this.stForm.get(['StorageTankUnitDetails',i,'questions',j,'options']);
    control.removeAt(0);
    control.controls = [];
  }

  /*onHeightChange(event: any) {
    this.nullalertmessage="";
    this.height=event;
  }*/

  onVolChange(event: any) {
   // console.log('eventevent', event)
    this.nullalertmessage="";
    this.vol=event;
  }

  totalVol = 0;
  totalHeight = 0;
  volErrorMsg = '';
  heightErrorMsg = '';
  calculateTotalVol(event: any, i) {
    this.vol = event;
    this.totalVol= 0;
    const control = (<FormGroup>(<FormArray>this.stForm.get('StorageTankUnitDetails')).controls[i]).value;
    let controlValue = control.value;
    if (Number(control['Volume']) > Number(this.stForm.get('tankCapacity').value)) {
      this.volErrorMsg = "Volume would not exceed the Tank Capacity!"
    } else {
      this.volErrorMsg = "";
    }
  }

  onHeightChange(event: any, i) {
    this.nullalertmessage="";
    this.height = event;
    this.totalHeight= 0;
    const control = (<FormGroup>(<FormArray>this.stForm.get('StorageTankUnitDetails')).controls[i]).value;
    console.log('control', control);

    if (Number(control['Height']) > Number(this.stForm.get('heightLength').value)) {
      this.heightErrorMsg = "Height would not exceed the Height/Length!";
    } else {
      this.heightErrorMsg = "";
    }
  }

  onHeightLengthChange(value) {
    this.heightErrorMsg = "";
    const controls = (<FormArray>this.stForm.get('StorageTankUnitDetails')).value;
    for( let control of controls ) {
      if((Number(control['Height']) > Number(value))) {
        this.heightErrorMsg = "Height would not exceed the Height/Length!";
        break;
      }
    }
  }

  onselected(){
    this.options = this.optionss;
  }

  getSiteList(){

    //     this.stService.getSiteList().then(r => {
    //       this.siteList = r.result;
    //   })
    //       .catch(r => {
    //           this.handleError(r);
    // });

    this.issuerService.getSiteList(this.userId).then(r => {
      console.log('r',r)
                this.siteList = r;
                console.log(' this.siteList', this.siteList)
            })
                .catch(r => {
                    this.handleError(r);
                });
  }
  getPolList(){

    //this.loading = true;
    this.stService.getPolList().then(r => {
      this.polList = r;
  })
      .catch(r => {
          this.handleError(r);
      })
}
getTypeList(){

  //this.loading = true;
  this.stService.getTypeList().then(r => {
    this.typeList = r;
})
    .catch(r => {
        this.handleError(r);
    })
}

getstoragetank(params){
  this.stService.getStorageTankList(params).then(r =>  {
      this.stGroups = r.result;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('st_id',this.st_id);
  this.params.set('list','list');
  this.stService.getStorageTankList(params).then(r =>  {
    this.stGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.stGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.stGroups.length; i++) {
      var Obj = {
        label : this.stGroups[i].name,
        value :  +this.stGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.stGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}
  storageTankCapacity : any = 0;
  storageTankPolId : any = 0;
  storageTankId : any = 0;
  ngOnChanges(change) {
  //this.options=this.optionss;
    this.loading = this.page == 'add' ? false : true;
    if (change.st && change.st.currentValue) {
      this.loading = false;
      this.stForm.controls['stId'].setValue(change.st.currentValue.storageTankId);
      this.storageTankId = change.st.currentValue.storageTankId;
      this.stForm.controls['tankNo'].setValue(change.st.currentValue.tankNo);
      this.stForm.controls['tankName'].setValue(change.st.currentValue.tankName);
      this.stForm.controls['tankCapacity'].setValue(change.st.currentValue.capacity);
      this.storageTankCapacity = change.st.currentValue.capacity;
      this.storageTankPolId = change.st.currentValue.polId;
      if(change.st.currentValue.heightOrLength > 0) {
        this.stForm.controls['heightLength'].setValue(change.st.currentValue.heightOrLength);
      }
      if(change.st.currentValue.diameter < 0) {
        this.stForm.controls['diameter'].setValue('');
      } else {
        this.stForm.controls['diameter'].setValue(change.st.currentValue.diameter);
      }
      if(change.st.currentValue.radius < 0) {
        this.stForm.controls['radius'].setValue('');
      } else {
        this.stForm.controls['radius'].setValue(change.st.currentValue.radius);
      }
      this.stForm.controls['tankMfg'].setValue(change.st.currentValue.tankMfg);
      this.stForm.controls['remarks'].setValue(change.st.currentValue.remarks);
      this.stForm.controls['siteId'].setValue(change.st.currentValue.siteId);
      if(change.st.currentValue.tankTypeId) {
        this.stForm.controls['typeId'].setValue(change.st.currentValue.tankTypeId);
      }
      this.stForm.controls['polId'].setValue(change.st.currentValue.polId);
      this.stForm.controls['FileType'].setValue(this.filetype);

       let calibrationValue = change.st.currentValue.tankCaliberationChart;
       if(calibrationValue != null){
        // const control =  <FormArray>this.stForm.get(['StorageTankUnitDetails']);
        // let valuchanged = []
        // calibrationValue.forEach(element => {
        //   valuchanged.push(this.initSection(element));
        // });
        // control.controls = valuchanged;


        let items = this.stForm.get('StorageTankUnitDetails') as FormArray;
        calibrationValue.forEach(element => {
         // valuchanged.push(this.initSection(element));
          items.push(this.initSection(element));

        });
       }


    } else{
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }
  
  onSubmit(validPost) {
    /***no need to show validation for the first element in the array*/
    for( let i = 1; i < validPost.StorageTankUnitDetails.length; i++ ) {
      if(validPost.StorageTankUnitDetails[i].Height == "" || validPost.StorageTankUnitDetails[i].Volume == "" || validPost.StorageTankUnitDetails[i].Height == 0 || validPost.StorageTankUnitDetails[i].Volume == 0) {
        this.nullalertmessage = "Height or Volume Can't be Zero!";
        break;
      }
    }
    this.clearerror();
    this.formSubmited = true;
    this.siteReq="You must select a Site";
    this.noReq="Tank No is required";
    this.nameReq="Tank name is required";
    this.capacityReq="Tank Capacity is required";
    this.polReq="You must select a POL";
    validPost.StorageTankUnitDetails.forEach(chartInfo => {
      if(chartInfo['Height'] == "") {
        chartInfo['Height'] = 0;
      }
      if(chartInfo['Volume'] == "") {
        chartInfo['Volume'] = 0;
      }
    });
    if(this.stForm.valid && this.ErrorList.length==0 && this.heightErrorMsg == "" && this.nullalertmessage == "") {
      validPost.uploadedFile = this.filetype;
     // validPost.StorageTankUnitDetails=this.StorageTankUnitDetails;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }

  clearmsgs() {
    this.heightErrorMsg = "";
    this.nullalertmessage = "";
    if (this.stForm.controls['siteId'].hasError('required') || (this.stForm.controls['siteId'].touched)) {
      this.siteReq = "";
    }
    if (this.stForm.controls['tankNo'].hasError('required') || (this.stForm.controls['tankNo'].touched)) {
      this.noReq = "";
    }
    if (this.stForm.controls['tankName'].hasError('required') || (this.stForm.controls['tankName'].touched)) {
      this.nameReq = "";
    }
    if (this.stForm.controls['tankCapacity'].hasError('required') || (this.stForm.controls['tankCapacity'].touched)) {
      this.capacityReq = "";
    }
    if (this.stForm.controls['polId'].hasError('required') || (this.stForm.controls['polId'].touched)) {
      this.polReq = "";
    }
    this.error = "";
    this.ErrorList = [];
    this.stForm = new FormGroup({
      stId:new FormControl(this.storageTankId),
      tankNo: new FormControl('', Validators.required),
      tankName: new FormControl('', Validators.required),
      tankCapacity: new FormControl(this.storageTankCapacity, Validators.required),
      siteId: new FormControl('', Validators.required),
      polId:  new FormControl(this.storageTankPolId, Validators.required),
      typeId: new FormControl(''),
      siteName: new FormControl(''),
      customerName: new FormControl(''),
      sectionName: new FormControl(''),
      typeName: new FormControl(''),
      heightLength: new FormControl(''),
      diameter: new FormControl(''),
      radius: new FormControl(''),
      tankMfg: new FormControl(''),
      remarks: new FormControl(''),
      FileType:new FormControl(''),
      StorageTankUnitDetails: new FormArray([
        this.initSection(),
      ]),
    });
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }

    var number=event.target.value;
    var wholePart = Math.floor(number);
    var wholePartLength = wholePart.toString().length;
    var fractionPart = number % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
   // console.log('noOfDigits', noOfDigits);
    if(noOfDigits >= 4){
        return false;
    } else {
        return true;
    }
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}