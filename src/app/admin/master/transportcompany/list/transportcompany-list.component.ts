import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../../../app.constants';
import { TransportCompanyFormService } from 'app/admin/master/transportcompany/service/transportcompany.service';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'transportcompany-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./transportcompany-list.html')
})
export class TransportCompanyListComponent implements OnInit{

  viewTransportCompanyForm: any;
  address: any;
  name: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  Id: any;
  public tc: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [

    {title: 'ID', name: 'Id', sort: true, },
    {title: 'Transport Company', name: 'Name',sort: true},
    {title: 'Address', name: 'description', sort: false},
    {title: 'Actions', className: ['text-center'], name: 'actions', sort: false}

  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public tc_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
   private tcService: TransportCompanyFormService,
   private fb: FormBuilder, private _sharedService : SharedService,
   private configuration: Configuration) {
      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
        this.privileges = privileges;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.rows = configuration.rows;
      this.viewTransportCompanyForm = fb.group({
        'name': [''],
        'Id': [''],
        'address': [''],
      });
      var vForm = this.viewTransportCompanyForm;
  }

  ngOnInit() {
    this.gettcList(this.params);
    this.totalfeilds = this.columns.length;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  gettcList(params: any) {
    this.loading = true;
    this.tcService.getTransportCompanyList(params).then(response => {
      this.tc = response['result']
      console.log('tc',response)
      if(this.tc.length>0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;}
      else{
        this.page=1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

   getAllLists(params: any) {
    this.loading = true;
    this.tcService.getTransportCompanyList(params).then(response => {
      this.tc = response['result']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value)

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.gettcList(params);
    }
  }
  Sort(param,order,sortstatus){
    //this.params.set('ordering', sortParam);

    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.gettcList(this.params);

  }
  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  this.start.toString());
    var sortParam = '';

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    var sortParam = '';
    this.config.sorting.columns.forEach(function(col, key) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.gettcList(this.params);
  }

  addtc() {
    this.router.navigate(['./home/master/transport-company/add']);
  }

  edittc(id) {

    this.router.navigate(['./home/master/transport-company/edit/', id]);
  }

deletetc(tc){

    this.tc_deleted_id = tc;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deletetcconfirm(tc){

      this.tc_deleted_id= tc.id;
      this.tcService.deletetc(  this.tc_deleted_id).then(r =>  {
        this.gettcList(this.params);
        this.deletemessage = "Transport Company Deleted Successfully";

      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })

  }
  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }

  view(data){
    if (data.buId != 0) {
        this.name = data.name;
        this.Id = data.id;
        this.address = data.address;
        this.viewTransportCompanyForm.controls['bBusinessUnit'].setValue(this.name);
        this.viewTransportCompanyForm.controls['bBusinessUnitId'].setValue(this.Id);
        this.viewTransportCompanyForm.controls['bAddress'].setValue(this.address);
    }
}
viewclose(){
 setTimeout(function () {
     jQuery('#ViewTransportCompany').modal('toggle');
   }.bind(this), 0);
}

  // For File Attachments
  attachment(id){
    this.Id=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadTransportCompanyFiles?FileId=';
  this.tcService.getalltransportcompanyattachments(id).then(response => {
  this.attachments=response;
  if(this.attachments.length > 0){
    var LongArray = [];
    for(var i = 0; i < this.attachments.length; i++) {
      let ext=this.attachments[i].location;
      var Obj = {
       id :this.attachments[i].id,
       referenceId:this.attachments[i].referenceId,
       shortFileName : this.attachments[i].shortFileName,
       fileName:this.attachments[i].fileName,
       createdDate: this.attachments[i].createdDate,
        ext :  ext.substr(ext.lastIndexOf('.') + 1),

      };
      LongArray.push(Obj);
      this.attachmentGroup=LongArray;
     }
     console.log( this.attachmentGroup)
  }
  else{
   this.gettcList(this.params);
   this.attachmentmessage='No Attachments Found'
  //  setTimeout(function () {
  //    jQuery('#ViewAttachmentModal').modal('toggle');
  //  }.bind(this), 1000);
  }
  return this.attachmentGroup;
  })

  }

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
      this.tcService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.Id);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";

      }).catch(r =>  {
         // console.log();
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }
  private handleError(e: any) {
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
