import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransportCompanyFormService } from 'app/admin/master/transportcompany/service/transportcompany.service';

@Component({
  selector: 'edit',
  template: require('./transportcompany-edit.html')
})
export class TransportCompanyEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public tc: any;
  public page = 'edit';
  

// public localityGroups: any;
// public usersublocality: any;
public ownershipdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private tcService: TransportCompanyFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
    			this.tc = this.tcService.gettransportcompany(this.id)
      });

	}

  onSave(tc: any) {
    let files = tc.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    } 
    formData.append('id',this.id);
    formData.append('name', tc.name);
    formData.append('address', tc.address);
    formData.append('IsDeleted', "");
    formData.append('BUId', tc.BUId);
    this.tcService.Save(formData).then(r =>  {
      this.success = 'Transport Company Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/transport-company']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
