import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'transportcompany',
    template: `<router-outlet></router-outlet>`
})
export class TransportCompanyComponent {
	constructor(private router: Router) {
	}
}