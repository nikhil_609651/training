import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransportCompanyFormService } from 'app/admin/master/transportcompany/service/transportcompany.service';

@Component({
  selector: 'add',
  template: require('./transportcompany-add.html')
})
export class TransportCompanyAddComponent implements OnInit{

  public error = {};
  public errorpass = '';
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private tcService: TransportCompanyFormService) {

  }
  ngOnInit() {
    this.page = 'add';
  }
  onSave(tc: any) {
      console.log("values",tc);
      let files = tc.uploadedFile;
      let formData = new FormData();
      for(let i =0; i < files.length; i++) {
          formData.append("FileType", files[i], files[i]['name']);
      } 
      formData.append('id', '0');
      formData.append('name', tc.name);
      formData.append('address', tc.address);
      formData.append('BUId', tc.BUId);
      formData.append('IsDeleted', "");
    this.tcService.Save(formData).then(r =>  {
      this.success = 'Transport Company Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/transport-company']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    /*console.log(e)*/
    this.error = e;
      let detail = e.detail;
      console.log(detail);
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
