import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../../app.constants';

@Injectable()
export class TransportCompanyFormService {

    private userGroupUrl = '';
    private locality ='';
    private sublocality ='';
    private geturl='';
    private saveurl='';
    private editurl='';
    private updateurl='';
    private deleteurl='';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    public businessunitUrl ='';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.geturl = _configuration.ServerWithApiUrl+'Masters/TransportCompany';
        this.saveurl   =_configuration.ServerWithApiUrl+'Masters/SaveTransportCompany';
        //this.updateurl   =_configuration.ServerWithApiUrl+'Masters/UpdateTransportCompany';
        this.deleteurl = _configuration.ServerWithApiUrl+'Masters/DeleteTransportCompanyByID?id=';
        this.editurl = _configuration.ServerWithApiUrl+'Masters/GetTransportCompanyByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllTransportCompanyAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteTransportCompanyAttachments?Id=';
        this.businessunitUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
    }

    getTransportCompanyList(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    gettransportcompanies(params: any): Promise<any> {
        return this.authHttp.get(this.geturl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBusinessUnitList(): Promise<any> {
        return this.authHttp.get(this.businessunitUrl +'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    gettransportcompany(id: string) {
        return this.authHttp.get(this.editurl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }


    Save(tc: any): Promise<any>  {
        console.log('tc',tc);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveurl, tc)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);

    }

    private post(tc: any): Promise<any> {
        const formData = new FormData();
        formData.append('Id', tc.Id);
        formData.append('Name', tc.name);
        formData.append('Address', tc.address);
        formData.append('IsDeleted', "");
        formData.append('FileURL', null);

        console.log(tc);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        console.log(formData);
        return this.authHttp
            .post(this.saveurl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);

    }
    getalltransportcompanyattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .get(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    deletetc(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteurl;

        return this.authHttp
        .post(this.deleteurl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}