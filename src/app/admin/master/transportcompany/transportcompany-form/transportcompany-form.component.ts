import { Component, EventEmitter, Input, Output , OnInit, ViewChild} from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { TransportCompanyFormService } from 'app/admin/master/transportcompany/service/transportcompany.service';

@Component({
  selector: 'transportcompany-form',
  template: require('./transportcompany-form.html')
})
export class TransportCompanyFormComponent {
  buReq: string;
  businessunitList: any;
  savebutton: boolean = true;
  nameReq: string;
  image: string;
  filetype= [];
  file: any;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filelist= [];
  @ViewChild("fileInput") fileInputVariable: any;
  @Input() tc;
  @Input() error;
  @Input() page:string;
  @Output() saved = new EventEmitter();
  transportcompanyForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public owner: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public company_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();


  public tcGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private tcService: TransportCompanyFormService, private router: Router, private route: ActivatedRoute) {
    //this.getareamanagers();
   // this.getCityGroups();
    //this.onselectednew();
    this.transportcompanyForm = fb.group({
       'id': [''],
       'BUId':['',Validators.compose([Validators.required])],
      'name':['', Validators.compose([Validators.required])],
      'address':[''],
      'FileType':[''],

    });
    var cForm = this.transportcompanyForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.company_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
      this.getBusinessUnitList();
  }


  onselected(){
    this.options = this.optionss;
    //console.log(this.options)
  }


gettranportcompanies(params){
  this.tcService.gettransportcompanies(params).then(r =>  {
      this.tcGroups = r.results;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('company_id',this.company_id);
  this.params.set('list','list');
  this.tcService.gettransportcompanies(params).then(r =>  {
    this.tcGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.tcGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.tcGroups.length; i++) {
      var Obj = {
        label : this.tcGroups[i].name,
        value :  +this.tcGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.tcGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    console.log('abc',this.transportcompanyForm.controls);
  //this.options=this.optionss;
    this.loading = this.page == 'add' ? false : true;
    if (change.tc && change.tc.currentValue) {
      this.loading = false;
      this.transportcompanyForm.controls['id'].setValue(change.tc.currentValue.id);
      this.transportcompanyForm.controls['name'].setValue(change.tc.currentValue.name);
      this.transportcompanyForm.controls['address'].setValue(change.tc.currentValue.address);
      this.transportcompanyForm.controls['BUId'].setValue(change.tc.currentValue.buId1);
      this.transportcompanyForm.controls['FileType'].setValue(this.filetype);
      console.log(this.transportcompanyForm.controls);

    } else{
      this.loading = false;
    }
  }

  updated($event,control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }
  getBusinessUnitList(){

            this.tcService.getBusinessUnitList().then(r => {
              this.businessunitList = r.result;
              console.log(this.businessunitList)
          })
              .catch(r => {
                  this.handleError(r);
              })

      }
  onSubmit(validPost) {
    this.nameReq="Company Name is required";
    this.buReq="Please select Business Unit"
    this.clearerror();
    this.formSubmited = true;
    if(this.transportcompanyForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);

    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
   // alert('hai');
    this.error="";
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.transportcompanyForm.controls['name'].hasError('required') || (this.transportcompanyForm.controls['name'].touched))
    {
      this.nameReq="";
    }
    if(this.transportcompanyForm.controls['BUId'].hasError('required') || (this.transportcompanyForm.controls['BUId'].touched))
    {
      this.buReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}