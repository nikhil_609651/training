import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { subunitFormService } from 'app/admin/master/business_unit/services/subunit-form.service';
import { SubUnitFormService } from 'app/admin/master/subunit/services/subunit-form.service';

@Component({
  selector: 'subunit-edit',
  template: require('./subunit-edit.html')
})
export class SubUnitEditComponent implements OnInit{
  id: any;

  public error = {};
	public success = '';
  public subunit: any;
  public page = 'edit';


// public localityGroups: any;
// public usersublocality: any;
public subunitdata :any;

	constructor(private router: Router, private route: ActivatedRoute,
    private subunitService: SubUnitFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
          console.log('ID',this.id)
                this.subunit = this.subunitService.getsubunit(this.id);
                console.log('DATA',this.subunit)
      });


	}

  onSave(su: any) {
    let files = su.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('subUnitId',this.id);
    formData.append('subUnitName', su.subUnitName);
    this.subunitService.Save(formData).then(r =>  {
      this.success = 'Sub Unit Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/sub-unit']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
