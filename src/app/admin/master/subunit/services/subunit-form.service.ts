import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class SubUnitFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public countrydropdownUrl = '';
    public subuniturl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/SubUnit/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveSubUnit';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateSubUnit';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteSubUnitByID?id=';
        this.subuniturl = _configuration.ServerWithApiUrl+'Masters/GetSubUnitByID?id=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllSubUnitAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteSubUnitAttachments?Id=';
    }
   
    // List API for subunit
    getsubunitList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    //Save API for subunit
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();
        formData.append('subUnitId', bu.subUnitId);
        formData.append('subUnitName', bu.subUnitName);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletesubunit(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getsubunit(id: string) {
        return this.authHttp.get(this.subuniturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deletedepartment(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getallsubunitatattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getsubunits(id: string) {
        return this.authHttp.get(this.subuniturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
