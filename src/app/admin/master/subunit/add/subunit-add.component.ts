import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubUnitFormService } from 'app/admin/master/subunit/services/subunit-form.service';

@Component({
  selector: 'add',
  template: require('./subunit-add.html')
})
export class SubUnitAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
  private subunitService: SubUnitFormService) {

  }
  ngOnInit() {
    this.page = 'add';

  }
  onSave(su: any) {
    console.log('values',su);
    su.subUnitId=0;
    let files = su.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('subUnitId','0');
    formData.append('subUnitName', su.subUnitName);
    this.subunitService.Save(formData).then(r =>  {
      console.log('r',r)
      this.success = 'Sub Unit Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/sub-unit']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
