import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'subunit',
    template: `<router-outlet></router-outlet>`
})
export class SubUnitComponent {
	constructor(private router: Router) {
	}
}
