import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { SubUnitFormService } from 'app/admin/master/subunit/services/subunit-form.service';

@Component({
  selector: 'subunit-form',
  template: require('./subunit-form.html')
})
export class SubUnitFormComponent {
  savebutton: boolean = true;

  nameReq: string;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  @Input() subunit;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  subunitForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public subunit_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();
  image= '';
  public subunitGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private subunitService: SubUnitFormService, private router: Router,
    private route: ActivatedRoute) {
    //this.getsubunits();


    this.subunitForm = fb.group({
      'subUnitName': ['', Validators.compose([Validators.required])],
      'subUnitId': [''],
      'FileType':[],

    });
    var cForm = this.subunitForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.subunit_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
  }
  updated($event, control) {
    const files = $event.target.files || $event.srcElement.files;
    var reader: FileReader = new FileReader();
    var file: File = files[0];

    reader.onloadend = (e) => {
      this.image = btoa(reader.result);
      console.log('image', this.image)
      control.setValue(btoa(reader.result));
    }
    reader.readAsBinaryString(file);
  }

  onselected(){
    this.options = this.optionss;
    //console.log(this.options)
  }


getsubunits(params){
  this.subunitService.getsubunitList(params).then(r =>  {
      this.subunitGroups = r.result;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;
  this.params.set('city_id',id);
  this.params.set('subunit_id',this.subunit_id);
  this.params.set('list','list');
  this.subunitService.getsubunitList(params).then(r =>  {
    this.subunitGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.subunitGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.subunitGroups.length; i++) {
      var Obj = {
        label : this.subunitGroups[i].name,
        value :  +this.subunitGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.subunitGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    console.log('abc',this.subunitForm.controls);
  //this.options=this.optionss;
    this.loading = this.page == 'add' ? false : true;
    if (change.subunit && change.subunit.currentValue) {
   //this.onselectednew(change.subunit.currentValue.city.id);
      this.loading = false;
      // tslint:disable-next-line:max-line-length
      this.subunitForm.controls['subUnitId'].setValue(change.subunit.currentValue.subUnitId);
      this.subunitForm.controls['subUnitName'].setValue(change.subunit.currentValue.subUnitName);


    } else{
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  onSubmit(validPost) {
    this.nameReq="Sub Unit Name is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.subunitForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
      //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
    this.error="";
  }
  clearmsgs(){
    if(this.subunitForm.controls['subUnitName'].hasError('required') || (this.subunitForm.controls['subUnitName'].touched))
    {
      this.nameReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}