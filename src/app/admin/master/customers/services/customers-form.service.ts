import {Injectable} from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class CustomersFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public countrydropdownUrl = '';
    public budropdownUrl = '';
    public geturl;
    private attachmenturl='';
    private downloadUrl='';
    private deleteattachmentUrl;

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'Masters/Customer/';
        this.saveUrl = _configuration.ServerWithApiUrl+'Masters/SaveCustomer';
        this.updateUrl = _configuration.ServerWithApiUrl+'Masters/UpdateCustomer';
        this.deleteUrl = _configuration.ServerWithApiUrl+'Masters/DeleteCustomerByID?id=';
        this.geturl   =_configuration.ServerWithApiUrl+'Masters/GetCustomerByID?id=';
        this.budropdownUrl   =_configuration.ServerWithApiUrl+'Masters/BusinessUnit/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'Masters/GetAllCustomerAttachments?id=';
        this.downloadUrl = _configuration.ServerWithApiUrl+'Masters/DownloadCustomerFiles?FileId=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'Masters/DeleteCustomerAttachments?Id=';
    }
    
    // List API for Customers
    getCustomersList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getcustomer(id: string) {
        return this.authHttp.get(this.geturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBUList() {
        return this.authHttp.get(this.budropdownUrl +'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //   Save API for Customers
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private post(bu: any): Promise<any> {
        const formData = new FormData();
        formData.append('Id', bu.customerId);
        formData.append('firstName', bu.firstName);
        formData.append('lastName', bu.lastName);
        formData.append('unidNo', bu.unidNo);
        formData.append('IsInternal', bu.IsInternal);
        formData.append('isDeleted',"");
        formData.append('fileURL',"");
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl,formData,options)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletecustomers(id: any) {
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getallcustomerattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}