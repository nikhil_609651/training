import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { CustomersFormService } from 'app/admin/master/customers/services/customers-form.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SharedService } from 'app/services/shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'customers-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./customers-list.html')
})
export class CustomersListComponent implements OnInit{
  unidNo: any;
  lname: any;
  fname: any;
  attachment_deleted_id: any;

  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  customerId: any;
  public deleteattachmentmessage='';
 public customers: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true },
    { title: 'First Name', name: 'FirstName', sort: true },
    { title: 'Expansion', name: 'LastName', sort: true },
    { title: 'Address', name: 'Address', sort: true },
    { title: 'Actions', name: 'actions', sort: false }

  ];
  viewCustomerForm: FormGroup;
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public customers_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();
  
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;

  constructor(private router: Router,
    private customersService: CustomersFormService,
    private fb: FormBuilder,
    private configuration: Configuration,
    private _sharedService : SharedService) {
      /**user privileges**/	
      this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	      this.privileges = privileges;
	      this.showListPage = true;
      });
      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('UserId', localStorage.getItem('user_nameId'));
      this.rows = configuration.rows;
      this.viewCustomerForm = fb.group({
        'fname': [''],
        'customerId': [''],
        'lname': [''],
        'unidNo': [''],
      });
      var vForm = this.viewCustomerForm;
  }

  ngOnInit() {
    this.getcustomersList(this.params);
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getcustomersList(params: any) {
    this.loading = true;
    this.customersService.getCustomersList(params).then(response => {
      this.customers = response['result']
      if(this.customers.length>0){
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false; }
        else{
          this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

   getAllLists(params: any) {
    this.loading = true;
    this.customersService.getCustomersList(params).then(response => {
      this.customers = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;

      params.set('limit', event.target.value);
      this.getcustomersList(params);
    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {

    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';


    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getcustomersList(this.params);
  }


  addcustomer() {
    this.router.navigate(['./home/master/customer/add']);
  }

  editcustomer(id) {

    this.router.navigate(['./home/master/customer/edit/', id]);
  }

  deletecustomer(customers){
    this.customers_deleted_id = customers.customerId;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deletecustomersconfirm(id){
      this.customersService.deletecustomers(  this.customers_deleted_id).then(r =>  {
        this.getcustomersList(this.params);
        this.deletemessage = "Customer Deleted Successfully";
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }
  view(data){
    if (data.customerId != 0) {
        this.fname = data.firstName;
        this.customerId = data.customerId;
        this.lname = data.expansion;
        this.unidNo = data.address;
    }
}
viewclose(){
 setTimeout(function () {
     jQuery('#ViewCustomer').modal('toggle');
   }.bind(this), 0);
}
// For File Attachments
attachment(id){
  this.customerId=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'Masters/DownloadCustomerFiles?FileId=';
  this.customersService.getallcustomerattachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
     createdDate: this.attachments[i].createdDate,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getcustomersList(this.params);
 this.attachmentmessage='No Attachments Found'
//  setTimeout(function () {
//    jQuery('#ViewAttachmentModal').modal('toggle');
//  }.bind(this), 1000);
}
return this.attachmentGroup;
})

}


  deleteattachment(id){
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
  //alert(deleted);
    // if (confirm) {

      this.customersService.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.customerId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";

      }).catch(r =>  {
         // console.log();
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }
  viewcloseattachment(){
    setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }
  Sort(param,order,sortstatus){
    //this.params.set('ordering', sortParam);

    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getcustomersList(this.params);



      }
  private handleError(e: any) {
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
