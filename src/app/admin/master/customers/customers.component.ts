import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'customers',
    template: `<router-outlet></router-outlet>`
})
export class CustomersComponent {
	constructor(private router: Router) {
	}
}
