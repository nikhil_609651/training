import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomersFormService } from 'app/admin/master/customers/services/customers-form.service';
@Component({
  selector: 'add',
  template: require('./customers-add.html')
})
export class CustomersAddComponent implements OnInit{
  countryList: any;
  public error = {};
  public success = '';
  public customer: any;
  public page = '';
  constructor(private router: Router,
    private customerService: CustomersFormService) {
  }
  ngOnInit() {
    this.page = 'add';

  }

  onSave(bu: any) {
    bu.customerId=0;
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('customerId', '0');
    formData.append('firstName', bu.firstName);
    formData.append('lastName', bu.lastName);
    formData.append('unidNo', bu.unidNo);
    formData.append('BUId', bu.businessunitnameId);
    formData.append('isDeleted',"");
    this.customerService.Save(formData).then(r =>  {
      this.success = 'Customer Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/customer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
