import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { CustomValidator } from '../../../shared/custom-validator';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { URLSearchParams } from '@angular/http';
import {SelectModule} from 'angular2-select';
import { CustomersFormService } from 'app/admin/master/customers/services/customers-form.service';
@Component({
  selector: 'customers-form',
  template: require('./customers-form.html')
})
export class CustomersFormComponent {
  businessunitReq: string;
  savebutton: boolean = true;;
  buList: any;
  isInternalSection=false;
  unidReq: string;
  lnameReq: string;
  nameReq: string;
  IsInternalUse= false;
  filelength: number;
  sizeError: string;
  ErrorList=[];
  filetype= [];
  filelist= [];
  file: any;

  @ViewChild("fileInput") fileInputVariable: any;
  @Input() customer;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  customerForm:FormGroup;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public countryList: Array<string>;
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public customer_id = '';
  userSettingsParams: URLSearchParams = new URLSearchParams();

  public customerGroups =[];

   params: URLSearchParams = new URLSearchParams()


  constructor(private fb: FormBuilder,
    private customerService: CustomersFormService, private router: Router,
    private route: ActivatedRoute) {
      this.getBUList();


    this.customerForm = fb.group({
      'customerId': [''],
      'firstName': ['', Validators.compose([Validators.required])],
      'lastName': [''],
      'unidNo': [''],
     // 'IsInternalUse': [''],
      'businessunitnameId':['',Validators.compose([Validators.required])],
      'FileType':[''],

    });
    var cForm = this.customerForm;

  }
    ngOnInit() {
      this.route.params.forEach((params: Params) => {
          this.customer_id = params['id'];
          // this.citymanager = this.citymanagerService.getcitymanager(id)
      });
  }


  onselected(){
    this.options = this.optionss;
  }

getBUList(){
    this.customerService.getBUList().then(r => {
      this.buList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
}

getcustomers(params){
  this.customerService.getCustomersList(params).then(r =>  {
      this.customerGroups = r.result;
      this.loading = false;
    })
}
onselectednew(id){
  var self = this;
  let params = this.params;

  this.params.set('customer_id',this.customer_id);
  this.params.set('list','list');
  this.customerService.getCustomersList(params).then(r =>  {
    this.customerGroups = r.results;
    this.loading = false;
     var LongArray = [];
     if(this.customerGroups.length==0){
       this.optionss=[];
       this.onselected();
     }
    for(var i = 0; i < this.customerGroups.length; i++) {
      var Obj = {
        label : this.customerGroups[i].name,
        value :  +this.customerGroups[i].id,
      }
      LongArray.push(Obj);
      if(i == (this.customerGroups.length - 1)) {
        this.optionss = LongArray;
        this.onselected();
      }
    }
  })
}


  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.customer && change.customer.currentValue) {
      this.loading = false;
      this.customerForm.controls['customerId'].setValue(change.customer.currentValue.customerId);
      this.customerForm.controls['firstName'].setValue(change.customer.currentValue.firstName);
      this.customerForm.controls['lastName'].setValue(change.customer.currentValue.expansion);
      this.customerForm.controls['unidNo'].setValue(change.customer.currentValue.address);
     // this.customerForm.controls['IsInternalUse'].setValue(change.customer.currentValue.isInternalUse);
      this.customerForm.controls['businessunitnameId'].setValue(change.customer.currentValue.buId1);
      this.customerForm.controls['FileType'].setValue(this.filetype);
    } else {
      this.loading = false;
    }
  }

  // selectAll(event) {
  //   if(event.target.checked)
  //   {
  //     this.IsInternalUse=true;
  //     this.isInternalSection=true;
  //     this.getInternalBU(this.IsInternalUse);
  //   }
  //   else
  //   {
  //     this.isInternalSection=false;
  //     this.IsInternalUse=false;
  //   }
  // }

  // getInternalBU(IsInternalUse)
  // {
  // this.customerService.getBUList(IsInternalUse).then(response=>{
  //   this.buList = response.result;
  // })
  //     .catch(r => {
  //         this.handleError(r);
  //     })
  // }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onSubmit(validPost) {
    this.nameReq="First Name is required";
   this.businessunitReq="BusinessUnit is required";
    this.unidReq="UNID No: is required";
    this.clearerror();
    this.formSubmited = true;
    if(this.customerForm.valid && this.ErrorList.length==0) {
      validPost.uploadedFile = this.filetype;
     // validPost.IsInternalUse=this.IsInternalUse;
     //this.savebutton = false;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }
  clearerror(){
   // alert('hai');
    this.error="";
  }
  onChange(event) {
    this.ErrorList=[];
    this.filelist=[];
    this.filetype=[];
    this.filelist = <Array<File>>event.target.files;
    for(let i =0; i <this.filelist.length; i++) {
      let fSize= this.filelist[i].size;
      let fName=this.filelist[i].name;
      let extnsion=fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize>2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  clearmsgs(){
    if(this.customerForm.controls['firstName'].hasError('required') || (this.customerForm.controls['firstName'].touched))
    {
      this.nameReq="";
    }
    if(this.customerForm.controls['businessunitnameId'].hasError('required') || (this.customerForm.controls['businessunitnameId'].touched))
    {
      this.businessunitReq="";
    }
    if(this.customerForm.controls['unidNo'].hasError('required') || (this.customerForm.controls['unidNo'].touched))
    {
      this.unidReq="";
    }
    this.error="";
    this.ErrorList=[];
  }
  handleError(e: any) {
      this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}