import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { CustomersFormService } from 'app/admin/master/customers/services/customers-form.service';
@Component({
  selector: 'edit',
  template: require('./customers-edit.html')
})
export class CustomersEditComponent implements OnInit{
  id: any;
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router, private route: ActivatedRoute,
    private customerService: CustomersFormService) {

  }
  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.customer = this.customerService.getcustomer(this.id);
    });

  }

  onSave(bu: any) {
    let files = bu.uploadedFile;
    let formData = new FormData();
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', this.id);
    formData.append('firstName', bu.firstName);
    formData.append('lastName', bu.lastName);
    formData.append('unidNo', bu.unidNo);
    formData.append('BUId', bu.businessunitnameId);
    formData.append('isDeleted',"");
    this.customerService.Save(formData).then(r =>  {
      this.success = 'Customer Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/master/customer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail;
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
