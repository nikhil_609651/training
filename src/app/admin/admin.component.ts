import {Component, ViewEncapsulation} from '@angular/core';
import {
  Router,
  // import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router'

@Component({
  selector: 'admin',
  encapsulation: ViewEncapsulation.None,
  styles: [],
  template: `
    <ba-sidebar></ba-sidebar>
    <ba-page-top></ba-page-top>
    <div class="al-main container-fluid" id="pre-mode">

      <div class="al-content row">
        <div class="col-md-12">
          <breadcrumbs></breadcrumbs>
        </div>

        <router-outlet>
          <div *ngIf="loading">
            <div class="ng-busy-default-wrapper">
              <div class="ng-busy-default-sign">
                  <div>
                    <sk-three-bounce></sk-three-bounce>
                  </div>
              </div>
            </div>
            <div class="ng-busy-backdrop"></div>
          </div>
        </router-outlet>

      </div>

    </div>
    <footer class="al-footer clearfix">
      <div class="al-footer-main clearfix">
         <div class="al-copy">&copy; TRISTAR 2018</div>
      </div>
    </footer>
    <ba-back-top position="200"></ba-back-top>
    `
})
export class Admin {

  loading = true;

  constructor(private router: Router) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true
    }
    if (event instanceof NavigationEnd) {
      this.loading = false
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false
    }
    if (event instanceof NavigationError) {
      this.loading = false
    }
  }

  private handleError(e: any) {

  }

}
