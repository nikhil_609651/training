import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'stockadjustment',
    template: `<router-outlet></router-outlet>`
})
export class StockAdjustmentComponent {
	constructor(private router: Router) {
	}
}
