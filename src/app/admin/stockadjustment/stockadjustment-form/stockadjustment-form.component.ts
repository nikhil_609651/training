import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IMyOptions } from 'mydatepicker';
import { StockAdjustmentService } from 'app/admin/stockadjustment/services/stockadjustment.service';

@Component({
  selector: 'stockadjustment-form',
  template: require('./stockadjustment-form.html')
})
export class StockAdjustmentFormComponent implements OnInit {
  afteralert: string;
  negativevalue: number;
  valuealert: string= '';
  stockTankName = 'Select';
  quantityalert: string= '';
  capacity: any;
  nullalertmessage: string= '';
  idalertmessage: string= '';
  bId: any = 0;
  adjustmentTypeReq: string;
  adjustmentReq: string;
  batchReq: string;
  polReq: string;
  tankReq: string;
  siteReq: string;
  item: any;
  total: number= 0;
  tankid: any;
  polid: any;
  siteid: any;
  savebutton: boolean = true;
  quantityafteriteration: number = 0;
  quantityafteradjustment: number= 0;
  tankname: any;
  adjustedquantity: number = 0;
  quantityfuelafteradjustment: number= 0;

  quantityoffuel: any = [];
  quantitybefore: number= 0;
  batchGroups: any;
  public loading: boolean;
  ErrorList = [];
  filetype = [];
  filelist = [];
  batchList = [];
  filelength: number;
  tankdropdownlist: any;
  poldropdownlist: any;
  sitetoadjustdropdownlist: any;
  adjustmenttypedropdownlist: any;
  AdjustmentValue: number = 0;
  BatchName: string = '';
  BatchId: any;
  isAddedAllBatches = false;
  showAddBtn = [];
  capacityError : any = "";
  public formSubmited = false;
  constructor(private fb: FormBuilder, private service: StockAdjustmentService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  @Input() stockadjustment;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  stockadjustmentForm: FormGroup;
  private currentdate = new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    /** disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()} */
    /** disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1} */

  };
  private TransactionDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  public file = '';
  batchDetailsEmptyMsg = '';
  disableControl = false;
  ngOnInit() {
    this.stockadjustmentForm = new FormGroup({
      AdjustmentTypeId: new FormControl('', Validators.required),
      TransactionDate: new FormControl('', Validators.required),
      AdjustSiteId: new FormControl('', Validators.required),
      PolId: new FormControl('', Validators.required),
      TankId: new FormControl('', Validators.required),
      TankName: new FormControl(''),
      BatchId: new FormControl(''),
      Adjustment: new FormControl(''),
      QuantityofFuel: new FormControl(''),
      QuantityBeforeAdjusted: new FormControl(0),
      QuantityAfterAdjusted: new FormControl(0),
      StockAdjustmentBatchDetails: new FormArray([
       this.initSection([]),
      ]),
      Remarks: new FormControl(''),
      FileType: new FormControl(''),
    });

    this.getadjustmenttypelist();
    this.getsitelist();
    this.getpollist();
    //this.storagetanklist();
    if (this.page == 'edit') {
      this.disableControl = true;
      this.stockadjustmentForm.disable();
      const batchControls = (<FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails')).controls;
      batchControls.forEach(control => {
        control.get('PurchaseReceiptId').disable();
        control.get('AdjustedQuantity').disable();
      });
    }
  }

  initSection(data = []) {
    let batch = data['purchaseReceiptId'] ? data['purchaseReceiptId'] : '';
    let volume = data['adjustedQuantity'] ? data['adjustedQuantity'] : '';
    let BatchQuantityAfterAdjustment = data['batchQuantityAfterAdjustment'] ? data['batchQuantityAfterAdjustment'] : '';
    this.quantityoffuel.push(Number(BatchQuantityAfterAdjustment)- Number(volume));
    this.quantityfuelafteradjustment = BatchQuantityAfterAdjustment;

    // let BatchQuantityAfterAdjustment = data['batchQuantityAfterAdjustment'] ? data['batchQuantityAfterAdjustment'] : '';
    // let PurchaseReceiptId = data['purchaseReceiptId'] ? data['purchaseReceiptId'] : '';
    // let PurchaseReceipt = data['batchNumber'] ? data['batchNumber'] : '';
    // let BatchQuantityBeforeAdjustment = data['batchQuantityBeforeAdjustment'] ? data['batchQuantityBeforeAdjustment'] : '';
    // let AdjustedQuantity = data['adjustedQuantity'] ? data['adjustedQuantity'] :'';
    // let BatchQuantityAfterAdjustment = data['batchQuantityAfterAdjustment'] ? data['batchQuantityAfterAdjustment'] : '';

    if (this.page == 'edit') {
      this.batchList.push(batch);
      this.AdjustmentValue  = volume;
      this.bId = batch;
    }
    return new FormGroup({
      PurchaseReceiptId: new FormControl(batch),
      // BatchNumber: new FormControl(PurchaseReceipt),
      // BatchQuantityBeforeAdjustment: new FormControl(BatchQuantityBeforeAdjustment),
      AdjustedQuantity: new FormControl(volume),
      BatchQuantityAfterAdjustment: new FormControl(BatchQuantityAfterAdjustment)
    });
  }

  getBatchDetails(form) {
    return form.controls.StockAdjustmentBatchDetails.controls;
  }

  removeSection(i, item) {
    const control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
    control.removeAt(i);
    this.nullalertmessage = "";
    this.idalertmessage = '';
    this.batchList.splice(i, 1);
    this.quantityoffuel.splice(i, 1);
    this.showAddBtn[control.length-1] = false;
    let qtyAfterAdjusted = this.stockadjustmentForm.controls['QuantityAfterAdjusted'].value;
    let adjustedQty = item.value['volume'];
    this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(qtyAfterAdjusted - adjustedQty);
    //this.calculation();
    if(this.batchList.length == this.batchGroups.length) {
      this.isAddedAllBatches = true;
    } else {
      this.isAddedAllBatches = false;
    }
  }

  totalquantityadjustment(){
    this.total = 0;
        let control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
        let contrlarray = [];
        contrlarray = control.value;
        for (let i = 0; i < contrlarray.length; i++) {
          this.total = Number(this.total) + Number(contrlarray[i].Adjustment);
          this.item.push(contrlarray[i].Adjustment);
        }
        this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(this.total);
  }

  // For getting Type of Adjustment List
  getadjustmenttypelist() {
    this.service.getStockAdjustmentTypeList().then(r => {
      this.adjustmenttypedropdownlist = r;
    });
  }

  // For getting Site List
  getsitelist() {
    this.service.getSiteList().then(r => {
      this.sitetoadjustdropdownlist = r['result'];
    });
  }

  getpollist() {
    this.service.getPolList().then(r => {
      this.poldropdownlist = r.result;
    })
      .catch(r => {
        this.handleError(r);
      });
  }

  // For getting Tank List
  /*storagetanklist() {
    this.service.getTankList().then(response => {
      this.tankdropdownlist = response['result'];
    });
  }*/

  transactionDateView: any;
  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.stockadjustment && change.stockadjustment.currentValue) {
      this.loading = false;
      //this.PId=change.pr.currentValue.id;
      this.stockadjustmentForm.controls['AdjustmentTypeId'].setValue(change.stockadjustment.currentValue.adjustmentTypeId);
      this.stockadjustmentForm.controls['TransactionDate'].setValue(change.stockadjustment.currentValue.transactionDate);
      let transactionDate = new Date(change.stockadjustment.currentValue.transactionDate);
      this.transactionDateView = transactionDate.getDate() + '-' + Number(transactionDate.getMonth()+1) + '-' + transactionDate.getFullYear();
      this.stockadjustmentForm.controls['AdjustSiteId'].setValue(change.stockadjustment.currentValue.siteId);
      this.stockadjustmentForm.controls['PolId'].setValue(change.stockadjustment.currentValue.polId);
      this.stockadjustmentForm.controls['TankId'].setValue(change.stockadjustment.currentValue.storageTankId);
      // this.stockadjustmentForm.controls['TankName'].setValue(change.stockadjustment.currentValue.batchNumber);
      // this.stockadjustmentForm.controls['BatchId'].setValue(change.stockadjustment.currentValue.batchId);
      this.stockadjustmentForm.controls['Adjustment'].setValue(0);
      this.stockadjustmentForm.controls['QuantityofFuel'].setValue(change.stockadjustment.currentValue.totalAdjustedQuantity);
      // this.stockadjustmentForm.controls['AdjustedQuantityofFuel'].setValue(change.stockadjustment.currentValue.totalAdjustedQuantity);
      this.stockadjustmentForm.controls['QuantityBeforeAdjusted'].setValue(change.stockadjustment.currentValue.totalBatchQuantityBeforeAdjustment);
      this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(change.stockadjustment.currentValue.totalBatchQuantityAfterAdjustment);
      this.stockadjustmentForm.controls['Remarks'].setValue(change.stockadjustment.currentValue.remarks);
      this.stockadjustmentForm.controls['FileType'].setValue(this.filetype);

      let siteValue =  change.stockadjustment.currentValue.adjustmentBatchDetails;
      let valuchanged = [];
      let items = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
      siteValue.forEach(element => {
        //valuchanged.push(this.initSection(element));
        items.push(this.initSection(element));
        //this.initSection(element);
      });
      items.removeAt(0);
      this.quantityoffuel.splice(0, 1);
      console.log('this.stockadjustmentForm', this.stockadjustmentForm);
      //this.getAdjustSite(change.stockadjustment.currentValue.siteId);
      //this.getPol(change.stockadjustment.currentValue.polId);
      //this. onTankChange(change.stockadjustment.currentValue.storageTankId);
      this.service.getTankNameById(change.stockadjustment.currentValue.storageTankId).then(response => {
        this.tankname = response['tankName'];
        this.capacity = response['capacity'];
      });
      this.service.getBatchList(change.stockadjustment.currentValue.storageTankId).then(r => {
        this.batchGroups = r;
      });
      this.calculation();
    } else {
      this.loading = false;
    }
  }

  // Date Change
  onDateChanged(event: any) {
    this.TransactionDate = event.formatted;
  }

  getAdjustmentType(id) {
  }

  getAdjustSite(id) {
    this.idalertmessage = "";
    this.nullalertmessage = "";
    this.siteid = id;
    this.stockadjustmentForm.controls['QuantityBeforeAdjusted'].setValue(0);
    this.stockadjustmentForm.controls['TankName'].setValue('');
    this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(0);
    this.service.getPolListFromSiteById(id).then(response => {
      this.poldropdownlist = response;
      this.stockadjustmentForm.controls['PolId'].setValue('');
      this.stockadjustmentForm.controls['TankId'].setValue('');
      const control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
      for (let i = 1; i < control.length; i++) {
        control.removeAt(i);
        this.quantityoffuel.pop();
      }
      const batchControl = (<FormGroup>control.controls[0]).controls;
      batchControl['PurchaseReceiptId'].setValue('');
      batchControl['AdjustedQuantity'].setValue('');
      batchControl['BatchQuantityAfterAdjustment'].setValue('');
      this.quantityoffuel[0] = 0;
      this.batchList = [];
      this.showAddBtn = [];
      this.batchGroups = [];
      this.tankdropdownlist = [];
    });
    if (this.polid !== undefined) {
      this.getPol(this.polid);
    }
  }

  getPol(id) {
    this.idalertmessage = "";
    this.nullalertmessage = "";
    this.polid = id;
    this.stockadjustmentForm.controls['QuantityBeforeAdjusted'].setValue(0);
    this.stockadjustmentForm.controls['TankName'].setValue('');
    this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(0);
    this.service.getStorageTankListFromSiteById(this.siteid,  this.polid).then(response => {
      this.tankdropdownlist = response;
      this.stockadjustmentForm.controls['TankId'].setValue('');
      const control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
      for (let i = 1; i < control.length; i++) {
        control.removeAt(i);
        this.quantityoffuel.pop();
      }
      const batchControl = (<FormGroup>control.controls[0]).controls;
      batchControl['PurchaseReceiptId'].setValue('');
      batchControl['AdjustedQuantity'].setValue('');
      batchControl['BatchQuantityAfterAdjustment'].setValue('');
      this.quantityoffuel[0] = 0;
      this.batchList = [];
      this.batchGroups = [];
    });
  }

  /******************************************
   * Get the tank details from tank id
   * Reinitialise the BatchDetails FormArray
   * Get Batch details for the selected tank
   ******************************************/
  onTankChange(id) {
    this.tankname = "";
    this.idalertmessage = "";
    this.nullalertmessage = "";
    this.stockadjustmentForm.controls['TankName'].setValue('');
    this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(0);
    this.showAddBtn = [];
    this.isAddedAllBatches =false;
    this.capacityError = '';
    this.service.getTankNameById(id).then(response => {
      this.tankname = response['tankName'];
      this.capacity = response['capacity'];
    });
    const control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
      for (let i = 1; i < control.length; i++) {
        control.removeAt(i);
        this.quantityoffuel.pop();
      }
      const batchControl = (<FormGroup>control.controls[0]).controls;
      batchControl['PurchaseReceiptId'].setValue('');
      batchControl['AdjustedQuantity'].setValue('');
      batchControl['BatchQuantityAfterAdjustment'].setValue('');
      this.quantityoffuel[0] = 0;
      this.batchList = [];
      this.batchGroups = [];
      this.tankid = id;
      this.service.getTankDetailsById(id).then(response => {
        this.quantitybefore = response['availableQuantity'];
        this.stockadjustmentForm.controls['QuantityBeforeAdjusted'].setValue(this.quantitybefore);
        if (this.quantitybefore == 0) {
          this.batchDetailsEmptyMsg = 'There is no fuel in the tank!'
        } else {
          this.batchDetailsEmptyMsg = '';
        }
      });

      this.service.getBatchList(id).then(r => {
        this.batchGroups = r;
      });
  }

  /***************************************************************************
   * On each selection change check whether the batch is not choosen already.
   * Also checks other validations
   ***************************************************************************/
  getBatchNoDetails(id, section, index) {
    this.nullalertmessage = '';
    this.idalertmessage = '';
    this.BatchId = id;
    this.bId = id;
    this.isAddedAllBatches = false;
    let batchPresent = false;
    //this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(0);
    this.quantityfuelafteradjustment = 0;
    //this.batchList[index] = id;
    this.batchList.forEach(batch=>{
      if(batch == id) {
        batchPresent = true;
      }
    });
    if(batchPresent) {
      this.idalertmessage = 'Batch Id is already selected.Choose another one!';
      this.batchList[index] = id;
      /*if(this.batchList.length > index-1) {
        if(this.batchList[index] == id) {
          this.idalertmessage = '';
        }
      }*/
    } else {
      if(this.batchList.length+1 == this.batchGroups.length) {
        this.batchList[index] = id;
      }
      this.idalertmessage = '';
      this.service.getPurchaseReceipt(id).then(response => {
        this.BatchName = response.batchNumber;
      });
       this.service.getBatchNoDetailsById(id, this.tankid).then(response => {
         this.AdjustmentValue = response.quantity;
         section.controls['PurchaseReceiptId'].setValue( this.BatchId);
         this.quantityoffuel[index] = (this.AdjustmentValue);
         section.controls['AdjustedQuantity'].enable();
         section.controls['AdjustedQuantity'].setValue('');
         (<FormGroup>(<FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails')).
         controls[index]).controls['BatchQuantityAfterAdjustment'].setValue(0);
         this.quantityfuelafteradjustment = 0;
         this.calculation();
      });
    }
  }

  onAdjustmentChange(value, section, index) {
    this.total = 0;
    this.AdjustmentValue = value;
    this.negativevalue = -(Number(this.quantityoffuel[index] + Number(1)));
    if(Number(this.AdjustmentValue)  <= Number(this.negativevalue)) {
      this.valuealert = 'Quantity of fuel greater than adjusted quantity';
    } else {
      this.valuealert = '';
    }
    let control = (<FormGroup>(<FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails')).
                      controls[index]).controls['BatchQuantityAfterAdjustment'];
    if(typeof this.quantityoffuel[index] != 'undefined' && this.quantityoffuel[index] != 0 ) {
      control.setValue(Number(this.AdjustmentValue) + Number(this.quantityoffuel[index]));
    }
    // this.qtyadjust = Number(this.qtyadjust)+ section.controls['totalquantity'].setValue(value);
    // this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue();
    this.calculation();
  }

  calculation() {

    let control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails').value;
    let totalQty = 0;
    for(let i = 0; i < control.length; i++) {
      totalQty = totalQty + Number(control[i]['AdjustedQuantity']);
    }
    let QuantityBeforeAdjusted = Number(this.stockadjustmentForm.controls['QuantityBeforeAdjusted'].value)+ Number(totalQty);
    this.stockadjustmentForm.controls['QuantityAfterAdjusted'].setValue(Math.round(QuantityBeforeAdjusted*100)/100);
    if(QuantityBeforeAdjusted > this.capacity) {
      this.capacityError = 'Adjusted Quantity is greater than Tank Capacity';
    } else {
      this.capacityError = '';
    }
  }

  quantitycheck() {
    if (this.quantityafteradjustment > this.capacity){
      this.quantityalert = 'Total Adjustment Quantity should be less than Tank Capacity';
    } else {
      this.quantityalert = '';
    }
  }

  addSection(i) {
    let control = (<FormGroup>(<FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails')).controls[i]).value;
    if(control['PurchaseReceiptId']  == "" || control['AdjustedQuantity']  == "") {
      this.nullalertmessage = "Batch Id or Quantity Can't be Empty!";
    } else if(this.idalertmessage == '') {
      this.nullalertmessage = "";
      if(this.batchList.length == 0) {
        this.batchList[i] = control['PurchaseReceiptId'];
        if(this.batchList.length == this.batchGroups.length) {
          this.isAddedAllBatches = true;
        } else {
          this.isAddedAllBatches = false;
        }
        if(!this.isAddedAllBatches) {
          const batchControl = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
          batchControl.push(this.initSection());
          this.showAddBtn[i] = true;
        }
      } else {
        let isBatchPresent = false;
        this.batchList.forEach(batch => {
          if(batch == this.BatchId) {
            isBatchPresent = true;
          }
        });
        //if(!isBatchPresent) {
          this.batchList[i] = control['PurchaseReceiptId'];
          this.showAddBtn[i] = true;
          if(this.batchList.length == this.batchGroups.length) {
            this.isAddedAllBatches = true;
          } else {
            this.isAddedAllBatches = false;
          }
          if(!this.isAddedAllBatches) {
            const batchControl = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
            batchControl.push(this.initSection());
            this.showAddBtn[i] = true;
          }
          this.idalertmessage = '';
        /*} else {
          this.idalertmessage = 'Batch Id is already selected.Choose another one!';
          if(this.batchList.length == this.batchGroups.length) {
            this.idalertmessage = '';
          } else if(this.batchList.length == i+1) {
            this.showAddBtn[i] = true;
            this.idalertmessage = '';
            const batchControl = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
            batchControl.push(this.initSection());
            this.showAddBtn[i] = true;
          }
        }*/
      }
    }
  }

  clearerror() {
    this.error = '';
  }

  onChange(event) {
    this.ErrorList = [];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for (let i = 0; i < this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if (fSize > 2097152) {
        this.ErrorList.push('The file ' + this.filelist[i].name + ' is too large. Maximum file size permitted is 2 Mb');
      }
      else if (extnsion != 'pdf' && extnsion != 'doc' && extnsion != 'docx' && extnsion != 'xls' && extnsion != 'xlsx' && extnsion != 'png' && extnsion != 'jpg' && extnsion != 'jpeg' && extnsion != 'gif' &&  extnsion != 'txt') {
        this.ErrorList.push('The selected file ' + this.filelist[i].name + ' is a an unsupported file');
      }
      else {
        this.ErrorList = [];
        this.filetype.push(this.filelist[i]);
        this.filelength = this.filetype.length;
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  clearmsgs() {
    this.quantityfuelafteradjustment = 0;
    this.quantityoffuel = [];
    this.idalertmessage = '';
		this.nullalertmessage = '';
    this.quantityalert = '';
    this.batchList = [];
    this.batchGroups = [];
    this.tankdropdownlist = [];
    this.poldropdownlist = [];
    this.capacityError = '';
    this.showAddBtn = [];
    if (this.stockadjustmentForm.controls['AdjustSiteId'].hasError('required') || (this.stockadjustmentForm.controls['AdjustSiteId'].touched)) {
      this.siteReq = '';
    }
    if (this.stockadjustmentForm.controls['TankId'].hasError('required') || (this.stockadjustmentForm.controls['TankId'].touched)) {
      this.tankReq = '';
    }
    if (this.stockadjustmentForm.controls['PolId'].hasError('required') || (this.stockadjustmentForm.controls['PolId'].touched)) {
      this.polReq = '';
    }
    if (this.stockadjustmentForm.controls['BatchId'].hasError('required') || (this.stockadjustmentForm.controls['BatchId'].touched)) {
      this.batchReq = '';
    }
    if (this.stockadjustmentForm.controls['Adjustment'].hasError('required') || (this.stockadjustmentForm.controls['Adjustment'].touched)) {
      this.adjustmentReq = '';
    }
    if (this.stockadjustmentForm.controls['AdjustmentTypeId'].hasError('required') || (this.stockadjustmentForm.controls['AdjustmentTypeId'].touched)) {
      this.adjustmentTypeReq = '';
    }
    const control = <FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails');
    for (let i = 1; i <= control.length; i++) {
      control.removeAt(i);
    }
    this.stockadjustmentForm.reset();
    this.stockadjustmentForm = new FormGroup({
      AdjustmentTypeId: new FormControl('', Validators.required),
      TransactionDate: new FormControl('', Validators.required),
      AdjustSiteId: new FormControl('', Validators.required),
      PolId: new FormControl('', Validators.required),
      TankId: new FormControl('', Validators.required),
      TankName: new FormControl(''),
      BatchId: new FormControl(''),
      Adjustment: new FormControl(''),
      QuantityofFuel: new FormControl(''),
      // AdjustedQuantityofFuel: new FormControl(''),
      QuantityBeforeAdjusted: new FormControl(0),
      QuantityAfterAdjusted: new FormControl(0),
      StockAdjustmentBatchDetails: new FormArray([
       this.initSection([]),
      ]),
      Remarks: new FormControl(''),
      FileType: new FormControl(''),
    });
    let TransactionDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.stockadjustmentForm.controls['TransactionDate'].setValue(TransactionDate);

    this.getadjustmenttypelist();
    this.getsitelist();
  }

  onSubmit(validPost) {
    let after= validPost.QuantityAfterAdjusted;
    if(this.capacity < after) {
      this.afteralert="QuantityAfterAdjusted must be less than or equal to QuantityBeforeAdjusted"
    } else {
      this.afteralert ='';
    }

    this.clearerror();
    this.quantitycheck();
    this.formSubmited = true;
    this.siteReq = 'Site Name is required';
    this.tankReq = 'Tank Name is required';
    this.polReq = 'Pol is required';
    this.batchReq = 'Batch is required';
    this.adjustmentReq = 'Adjustment is required';
    this.adjustmentTypeReq = 'Adjustment Type is required';
    let control = (<FormArray>this.stockadjustmentForm.get('StockAdjustmentBatchDetails')).controls;
    for( let batch of control ) {
      if(batch.value['PurchaseReceiptId']  == "" || batch.value['AdjustedQuantity']  == "") {
        this.nullalertmessage = "Batch Id or Quantity Can't be Empty!";
        break;
      }
    }
    console.log('validPost',validPost)
    if(this.stockadjustmentForm.valid && this.ErrorList.length == 0 && this.quantityalert == ''  && this.valuealert == '' && this.capacityError == '' && this.nullalertmessage == "") {
      validPost.uploadedFile = this.filetype;
      validPost['BatchDetails'] = [];
      validPost.StockAdjustmentBatchDetails.forEach(batch => {
        let batchArray = {
          batch : batch.PurchaseReceiptId,
          volume : batch.AdjustedQuantity
        }
        validPost['BatchDetails'].push(batchArray);
      });
      this.saved.emit(validPost);
    } else {
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  _keyPress(event){
    const pattern = /[0-9\.\+\- ]/;
    let inputChar = String.fromCharCode(event.charCode);
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      //event.preventDefault();
      return false;
    }
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail;
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}
