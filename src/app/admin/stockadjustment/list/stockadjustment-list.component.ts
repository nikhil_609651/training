import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormGroup, FormBuilder } from '@angular/forms';
import { StockAdjustmentService } from 'app/admin/stockadjustment/services/stockadjustment.service';
import { IMyOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
    selector: 'stockadjustment-list',
    encapsulation: ViewEncapsulation.None,
    template: require('./stockadjustment-list.html')
})
export class StockAdjustmentListComponent implements OnInit {
    selectedapproverAll: any;
    selectedstatusAll: any;
    selectedsiteAll: any;
    selectedbatchAll: any;
    selectedadjAll: any;
    selectedAll: any;
    batchGroups: any;
    attachmentGroup: any[];
    attachments: any;
    API_URL: string;
    attachmentmessage: string;
    saId: any;
    attachment_deleted_id: any;
    approverGroups: any;
    siteGroups: any;
    adjustmentGroups: any;
    statusGroups: any;
    stockadjustmentlist: any;
    checkedapprover: string[] = [];
    checkedsite: string[] = [];
    checkedbatch: string[] = [];
    checkedstatus: string[] = [];
    checkedadjustmentype: string[] = [];
    viewStockAdjustmentForm: FormGroup;
    public start: number = 1;
    public totalfeilds = 0;
    public page: number = 1;
    public itemsPerPage: number = 10;
    public maxSize: number = 5;
    public numPages: number = 0;
    public length: number = 5;
    public next = '';
    public stockadjustment_deleted_id = '';
    public deletemessage = '';
    public deleteattachmentmessage = '';
    public loading: boolean;
    public rows: Array<any> = [];
    private currentdate= new Date();
    API_URL_Export: string;
    private myDatePickerOptions: IMyOptions = {
        dateFormat: 'dd/mm/yyyy',
        //disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}

      };
      private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
      private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
      fromDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      toDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    // For headers
    public columns: Array<any> = [


        { title: 'ID', name: 'Id', sort: true, filter: false },
        { title: 'Type of Adjustment', name: 'AdjustmentType', sort: true, filter: true },
        { title: 'Site', name: 'Site', sort: true, filter: true },
        { title: 'Tank', name: 'Tank', sort: false, filter: false },
        { title: 'Quantity Before Adjustment', name: 'qtybefore', sort: false, filter: false },
        { title: 'Quantity After Adjustment', name: 'qtyafter', sort: false, filter: false },
        { title: 'Quantity Adjusted', name: 'qtyadjusted', sort: false, filter: false },
       // { title: 'Batch', name: 'Batch', sort: true, filter: true },
        { title: 'Date', name: 'Date', sort: true, filter: true },
        { title: 'Status', name: 'Status', sort: false, filter: true },
        { title: 'Approver', name: 'Approver', sort: true, filter: true },
        { title: 'Actions', name: 'actions', sort: false, filter: false }
    ];
    public config: any = {
        paging: true,
        sorting: { columns: this.columns },
        filtering: { filterString: '' },
        className: ['table-bordered']
    };
    params: URLSearchParams = new URLSearchParams();
    privileges : any = {
        isAdd: false,
        isDelete : false,
        isEdit : false,
        isView : false
    }
    privilegeSubscription : Subscription;
    
    constructor(private router: Router,
        private service: StockAdjustmentService,
        private fb: FormBuilder, private _sharedService : SharedService,
        private configuration: Configuration) {
            /**user privileges**/	
            this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	            this.privileges = privileges;
            });
            this.itemsPerPage = configuration.itemsPerPage;
            this.params.set('limit', configuration.itemsPerPage.toString());
            this.params.set('SiteFilter', null);
            this.params.set('AdjustmentTypeFilter', null);
            this.params.set('BatchFilter', null);
            this.params.set('ApproverFilter', null);
            this.params.set('StatusFilter', null);
            this.params.set('UserId', localStorage.getItem('user_nameId'));
            this.rows = configuration.rows;
            this.viewStockAdjustmentForm = fb.group({

                'id': [''],
                'adjustmenttype': [''],
                'site': [''],
                'batch': [''],
                'date': [''],
                'status': [''],
                'approver': [''],
            });
            var vForm = this.viewStockAdjustmentForm;
            if ((new Date().getDate() < 10) && (new Date().getMonth() > 10) ) {
                this.toDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
                this.fromDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
            }

            if ((new Date().getDate() < 10) && (new Date().getMonth() < 10) ) {
                this.toDate = '0' + new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
                this.fromDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
            }

            if ((new Date().getDate() > 10) && (new Date().getMonth() < 10) ) {
                this.toDate =  new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
                this.fromDate =  new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
            }

    }
    ngOnInit() {
        let userId = localStorage.getItem('user_nameId');
        this.API_URL_Export = this.configuration.ServerWithApiUrl + 'StockAdjustment/ExportStockAdjustmentFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
        this.getstockadjustmentlist(this.params);
        this.totalfeilds = this.columns.length;
        this.getadjustmenttypelist();
        this.getsitelist(userId);
        this.getstatuslist();
        this.getapproverlist();
    }

    ngOnDestroy() {
        this.privilegeSubscription.unsubscribe();
    }

    // For getting StockAdjustment List
    getstockadjustmentlist(params: any) {
        console.log('params',params)
        this.loading = true;
        this.service.getStockAdjustmentList(params).then(response => {
            this.stockadjustmentlist = response['result'];
            console.log('this.stockadjustmentlist',this.stockadjustmentlist)
            if (this.stockadjustmentlist.length > 0) {
                this.length = response['count'];
                this.next = response['next'];
                this.loading = false;
            }
            else {
                this.page = 1;
                this.getAllLists(this.params);
            }
        }).catch(r => {
            this.handleError(r);
        });
    }

    getAllLists(params: any) {
        this.loading = true;
        this.service.getStockAdjustmentList(params).then(response => {
            this.stockadjustmentlist = response['results']
            this.length = response['count'];
            this.next = response['next'];
            this.loading = false;

        }).catch(r => {
            this.handleError(r);
        });
    }

    // For getting Type of Adjustment List
    getadjustmenttypelist() {
        this.service.getStockAdjustmentTypeFilterList().then(r => {
            this.adjustmentGroups = r;
            console.log(r,'r')
            this.loading = false;
        });
    }

    // For getting Site List
    getsitelist(siteId) {
        this.service.getSiteFilterList(siteId).then(r => {
            this.siteGroups = r;

        });
    }

    // // For getting Batch List
    // getbatchlist() {
    //     this.service.getBatchFilterList().then(r => {
    //         this.batchGroups = r['result'];
    //     });
    // }
    public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

        let params = this.params;

        console.log(' page.page', page.page)
        console.log(' page.itemsPerPage', page.itemsPerPage)


        let start = (page.page - 1) * page.itemsPerPage;
        this.start = start + 1;
        console.log(' let start', start)
        console.log(' this.start', this.start)
        params.set('limit', page.itemsPerPage);
        params.set('offset', start.toString());

        var sortParam = '';
        if (config.filtering) {
            Object.assign(this.config.filtering, config.filtering);
            params.set(this.config.filtering.name, this.config.filtering.filterString);
        }
        this.getstockadjustmentlist(this.params);
    }
    // For getting Approver List
    getapproverlist() {
        this.service.getApproverList().then(r => {
            this.approverGroups = r;
            console.log('approverGroups',this.approverGroups)


        });
    }
    // For getting Status List
    getstatuslist() {
        this.service.getStatusList().then(r => {
            this.statusGroups = r;
            console.log('statusGroups',this.statusGroups)


        });
    }
    add() {
        this.router.navigate(['./home/stock-adjustment/add']);
    }

    edit(id) {

        this.router.navigate(['./home/stock-adjustment/view/', id]);
    }

    delete(data) {
        this.stockadjustment_deleted_id = data.id;
    }
    deletemessageclear() {
        this.deletemessage = '';
    }
    // For Deleting Stock Adjustment
    deleteconfirm(data) {


        this.service.delete(this.stockadjustment_deleted_id).then(r => {
            this.getstockadjustmentlist(this.params);
            this.deletemessage = 'Stock Adjustment Deleted Successfully';
        }).catch(r => {
            this.handleError(r);
            this.deletemessage = r.name[0];
        });
    }
    view(data) {
        console.log('data',data)
        if (data.id != 0) {

            this.viewStockAdjustmentForm.controls['id'].setValue(data.id);
            this.viewStockAdjustmentForm.controls['adjustmenttype'].setValue(data.adjustmentType.name);
            this.viewStockAdjustmentForm.controls['site'].setValue(data.site.name);
           // this.viewStockAdjustmentForm.controls['batch'].setValue(data.batch);
            this.viewStockAdjustmentForm.controls['date'].setValue(data.date);
            this.viewStockAdjustmentForm.controls['status'].setValue(data.approvedStatus.name);
            this.viewStockAdjustmentForm.controls['approver'].setValue(data.users1.firstName + data.users1.lastName);
        }
    }

    //For File Attachments
    attachment(id) {
        this.saId = id;
        this.attachmentmessage = '';
        this.API_URL = this.configuration.ServerWithApiUrl + 'StockAdjustment/DownloadStockAdjustmentFiles?FileId=';
        this.service.getallStockAttachmentattachments(id).then(response => {
            this.attachments = response;
            if (this.attachments.length > 0) {
                var LongArray = [];
                for (var i = 0; i < this.attachments.length; i++) {
                    let ext = this.attachments[i].location;
                    var Obj = {
                        id: this.attachments[i].id,
                        referenceId: this.attachments[i].referenceId,
                        shortFileName: this.attachments[i].shortFileName,
                        fileName: this.attachments[i].fileName,
                        ext: ext.substr(ext.lastIndexOf('.') + 1),

                    };
                    LongArray.push(Obj);
                    this.attachmentGroup = LongArray;
                }
                console.log(this.attachmentGroup)
            }
            else {
                this.getstockadjustmentlist(this.params);
                this.attachmentmessage = 'No Attachments Found'
                // setTimeout(function () {
                //     jQuery('#ViewAttachmentModal').modal('toggle');
                // }.bind(this), 1000);
            }
            return this.attachmentGroup;
        })

    }

    // For Deleting Attachments



    deleteattachment(id) {
        this.attachment_deleted_id = id;
    }
    deleteattachmentmessageclear() {
        this.deleteattachmentmessage = '';
    }

    deleteattachmentconfirm(id) {
        //alert(deleted);
        if (confirm) {

        this.service.deleteattachment(this.attachment_deleted_id).then(r => {
            this.attachment(this.saId);
            this.deleteattachmentmessage = "Attachment Deleted Successfully";

        }).catch(r => {
            // console.log();
            this.handleError(r);
            this.deleteattachmentmessage = r.name[0];
        })
    }
}

    // For Sorting
    Sort(param, order) {
        //this.params.set('ordering', sortParam);

        if (order === 'desc') {
            this.params.set('ordering', '-' + param);
        }
        if (order === 'asc') {
            this.params.set('ordering', param);
        }
        this.getstockadjustmentlist(this.params);
    }

    apply(item:any){
        this.getstockadjustmentlist(this.params);
        if(item === 'AdjustmentType'){
          setTimeout(function () {
            jQuery('#ViewAdjustmentModal').modal('toggle');
          }.bind(this), 0);
          jQuery('ViewAdjustmentModal').modal('hide');
          jQuery('body').removeClass('modal-open');
          jQuery('.modal-backdrop').remove();

        }


        if(item === 'Batch'){
          setTimeout(function () {
            jQuery('#ViewBatchModal').modal('toggle');
          }.bind(this), 0);

        }

        if(item === 'Site'){


                  setTimeout(function () {
                    jQuery('#ViewSiteModal').modal('toggle');
                  }.bind(this) , 0);
                 }
                 if(item === 'Status'){
                  setTimeout(function () {
                    jQuery('#ViewStatusModal').modal('toggle');
                  }.bind(this) , 0);
                 }

                 if(item === 'Approver'){

                  setTimeout(function () {
                    jQuery('#ViewApproverModal').modal('toggle');
                  }.bind(this) , 0);
                 }


                 jQuery('ViewAdjustmentModal').modal('hide');
                 jQuery('ViewBatchModal').modal('hide');
                 jQuery('ViewSiteModal').modal('hide');
                 jQuery('ViewApproverModal').modal('hide');
                 jQuery('ViewStatusModal').modal('hide');

                 jQuery('body').removeClass('modal-open');
                 jQuery('.modal-backdrop').remove();

      }

   // Filtering
    selectAll(item) {

        if(item === 'AdjustmentType'){


                  for (var i = 0; i < this.adjustmentGroups.length; i++) {

                    this.adjustmentGroups[i].selected = this.selectedadjAll;
                  }
                  this.params.set('AdjustmentTypeFilter', null);
                  }


      if(item === 'Batch'){


          for (var i = 0; i < this.batchGroups.length; i++) {

            this.batchGroups[i].selected = this.selectedbatchAll;
          }
          this.params.set('BatchFilter', null);
          }
          if(item === 'Site'){
            for (var i = 0; i < this.siteGroups.length; i++) {

                  this.siteGroups[i].selected = this.selectedsiteAll;
                }
                this.params.set('SiteFilter', null);
           }

           if(item === 'Status'){
             for (var i = 0; i < this.statusGroups.length; i++) {

                this.statusGroups[i].selected = this.selectedstatusAll;
              }
              this.params.set('StatusFilter', null);
           }
           if(item === 'Approver'){
             for (var i = 0; i < this.approverGroups.length; i++) {

                this.approverGroups[i].selected = this.selectedapproverAll;
              }
              this.params.set('ApproverFilter', null);
        }
        }

    checkIfAllSelected(option, event,filteritem) {



      if(filteritem === 'Batch'){
        this.selectedAll = this.batchGroups.every(function(item:any) {
            return item.selected == true;
          })

           var index = this.checkedbatch.indexOf(option.batchId);
        if(event.target.checked) {
          if(index === -1) {
            this.checkedbatch.push(option.batchId);

          }
        }
        else {
          if(index !== -1) {
            this.checkedbatch.splice(index, 1);
          }
        }
      this.params.set('BatchFilter', this.checkedbatch.toString());
        }



          if(filteritem === 'AdjustmentType'){
            this.selectedAll = this.adjustmentGroups.every(function(item:any) {
                return item.selected == true;
              })

               var index = this.checkedadjustmentype.indexOf(option.id);
            if(event.target.checked) {
              if(index === -1) {
                this.checkedadjustmentype.push(option.id);

              }
            }
            else {
              if(index !== -1) {
                this.checkedadjustmentype.splice(index, 1);
              }
            }
          this.params.set('AdjustmentTypeFilter', this.checkedadjustmentype.toString());
            }




            if(filteritem === 'Site'){
                this.selectedsiteAll = this.siteGroups.every(function(item:any) {
                    return item.selected == true;
                  })

                   var index = this.checkedsite.indexOf(option.siteId);
                if(event.target.checked) {
                  if(index === -1) {
                    this.checkedsite.push(option.siteId);

                  }
                } else {
                  if(index !== -1) {
                    this.checkedsite.splice(index, 1);
                  }
                }
              this.params.set('SiteFilter', this.checkedsite.toString());
              }

              if(filteritem === 'Status'){
               this.selectedstatusAll = this.statusGroups.every(function(item:any) {
                 return item.selected == true;
               })

                var index = this.checkedstatus.indexOf(option.id);
             if(event.target.checked) {
               if(index === -1) {
                 this.checkedstatus.push(option.id);

               }
             } else {
               if(index !== -1) {
                 this.checkedstatus.splice(index, 1);
               }
             }
             this.params.set('StatusFilter', this.checkedstatus.toString());

              }

              if(filteritem === 'Approver'){
               this.selectedapproverAll = this.approverGroups.every(function(item:any) {
                 return item.selected == true;
               })

                var index = this.checkedapprover.indexOf(option.id);
             if(event.target.checked) {
               if(index === -1) {
                 this.checkedapprover.push(option.id);

               }
             } else {
               if(index !== -1) {
                 this.checkedapprover.splice(index, 1);
               }
             }
             this.params.set('ApproverFilter', this.checkedapprover.toString());
              }

    }

    // Export to Excel List

    onDateChanged(event:any, type: any) {
        if(type == 'FromDate') {
            this.fromDate = event.formatted;
        }
        if(type == 'ToDate'){
            this.toDate = event.formatted;
        }

    }

    export() {
        let userId = localStorage.getItem('user_nameId');
        this.API_URL_Export = this.configuration.ServerWithApiUrl + 'StockAdjustment/ExportStockAdjustmentFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
    }

    closeExport() {
        setTimeout(function () {
            jQuery('#Export').modal('toggle');
        }.bind(this), 0);
    }

    private handleError(e: any) {
        let detail = e.detail;
        if (detail && detail == 'Signature has expired.') {
            this.router.navigate(['./']);
        }
    }
}