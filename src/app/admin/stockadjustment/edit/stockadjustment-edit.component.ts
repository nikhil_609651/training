import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StockAdjustmentService } from 'app/admin/stockadjustment/services/stockadjustment.service';
@Component({
  selector: 'stockadjustment-edit',
  template: require('./stockadjustment-edit.html')
})
export class StockAdjustmentEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public stockadjustment: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private service: StockAdjustmentService) {

  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {
        this.id = params['id'];
        this.getStockAdjustmentById();
      });
  }
  
  getStockAdjustmentById() {
    this.service.getStockAdjustment(this.id).then(response => {
      this.stockadjustment = response;
      console.log('this.stockadjustment', this.stockadjustment);
    });
  }

 onSave(data: any) {
  let files = data.uploadedFile;
  let BatchDetails = data.BatchDetails;
  let bDetails;
  let formData = new FormData();

  bDetails = JSON.stringify(BatchDetails);
  let TransactionDate = data.TransactionDate;

  if (typeof TransactionDate == 'object') {
    TransactionDate = TransactionDate.date.day + '/' + TransactionDate.date.month + '/' + TransactionDate.date.year;

  } else {
    TransactionDate = data.TransactionDate;
  }

  for (let i = 0; i < files.length; i++) {
    formData.append("FileType", files[i], files[i]['name']);
  }

  formData.append('Id', this.id);
  formData.append('AdjustmentTypeId', data.AdjustmentTypeId);
  formData.append('TransactionDate', TransactionDate);
  formData.append('SiteId', data.AdjustSiteId);
  formData.append('POLId', data.PolId);
  formData.append('StorageTankId', data.TankId);
  formData.append('TotalBatchQuantityBeforeAdjustment', data.QuantityBeforeAdjusted);
  formData.append('TotalBatchQuantityAfterAdjustment', data.QuantityAfterAdjusted);
  formData.append('Remarks', data.Remarks);
  formData.append('StockAdjustmentBatchDetails', bDetails);
 // formData.append('AdjustedQuantityofFuel', data.AdjustedQuantityofFuel);

  //formData.append('Adjustment', data.Adjustment);

  //formData.append('BatchId', data.BatchId);

 // formData.append('QuantityAfterAdjusted', data.QuantityAfterAdjusted);
  //formData.append('QuantityofFuel', data.QuantityofFuel);


  formData.append('isDeleted', "");
  formData.append('fileURL', "");
  console.log('formData', formData)
  this.service.Save(formData).then(r => {
    this.success = 'Stock Adjustment Created Successfully!';

    jQuery('html, body').animate({ scrollTop: 0 }, { duration: 1000 });
    setTimeout(function () {
      this.success = '';
      this.router.navigate(['./home/stock-adjustment/']);
    }.bind(this), 3000);
  }).catch(r => {
    this.handleError(r);
  })

   }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
