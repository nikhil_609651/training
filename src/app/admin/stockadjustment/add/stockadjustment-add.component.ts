import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockAdjustmentService } from 'app/admin/stockadjustment/services/stockadjustment.service';

@Component({
  selector: 'add',
  template: require('./stockadjustment-add.html')
})
export class StockAdjustmentAddComponent implements OnInit {

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private service: StockAdjustmentService) {

  }
  ngOnInit() {
    this.page = 'add';
  }

  onSave(data: any) {
    let files = data.uploadedFile;
    let bDetails;
    let formData = new FormData();

    bDetails = JSON.stringify(data.StockAdjustmentBatchDetails);
    let TransactionDate = data.TransactionDate;

    if(typeof TransactionDate == 'object') {
      if(TransactionDate.date.month < 10 && TransactionDate.date.day < 10) {
        TransactionDate = '0' + TransactionDate.date.day + '/' + '0' + TransactionDate.date.month + '/' + TransactionDate.date.year;
      }else if(TransactionDate.date.day < 10 && TransactionDate.date.month >= 10) {
        TransactionDate = '0' + TransactionDate.date.day + '/' + TransactionDate.date.month + '/' + TransactionDate.date.year;
      }else if(TransactionDate.date.day >= 10 && TransactionDate.date.month < 10) {
        TransactionDate = TransactionDate.date.day + '/' + '0' + TransactionDate.date.month + '/' + TransactionDate.date.year;
      } else {
        TransactionDate = TransactionDate.date.day + '/' + TransactionDate.date.month + '/' + TransactionDate.date.year;
      }
    }
    for (let i = 0; i < files.length; i++) {
      formData.append("FileType", files[i], files[i]['name']);
    }

    formData.append('Id', '0');
    formData.append('AdjustmentTypeId', data.AdjustmentTypeId);
    formData.append('TransactionDate', TransactionDate);
    formData.append('SiteId', data.AdjustSiteId);
    formData.append('POLId', data.PolId);
    formData.append('StorageTankId', data.TankId);
    formData.append('TotalBatchQuantityBeforeAdjustment', data.QuantityBeforeAdjusted);
    formData.append('TotalBatchQuantityAfterAdjustment', data.QuantityAfterAdjusted);
    formData.append('Remarks', data.Remarks);
    formData.append('StockAdjustmentBatchDetails', bDetails);
    //formData.append('AdjustedQuantityofFuel', data.AdjustedQuantityofFuel);

    //formData.append('Adjustment', data.Adjustment);

    //formData.append('BatchId', data.BatchId);

   // formData.append('QuantityAfterAdjusted', data.QuantityAfterAdjusted);
    //formData.append('QuantityofFuel', data.QuantityofFuel);


    formData.append('isDeleted', "");
    formData.append('fileURL', "");
    formData.append('BatchDetails', JSON.stringify(data.BatchDetails));
    this.service.Save(formData).then(r => {
      this.success = 'Stock Adjustment Created Successfully!';

      jQuery('html, body').animate({ scrollTop: 0 }, { duration: 1000 });
      setTimeout(function () {
        this.success = '';
        this.router.navigate(['./home/stock-adjustment/']);
      }.bind(this), 3000);
    }).catch(r => {
      this.handleError(r);
    })
  }

private handleError(e: any) {
  console.log(e)
  this.error = e;
  let detail = e.detail
  if (detail && detail == 'Signature has expired.') {
    this.router.navigate(['./']);
  }
}
}