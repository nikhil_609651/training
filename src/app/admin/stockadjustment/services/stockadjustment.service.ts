import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../app.constants';

@Injectable()
export class StockAdjustmentService {
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public stockattachmenturl = '';
    public adjustmenttypeUrl = '';
    public dvUrl = '';
    public polUrl = '';
    public getPolUrl = '';
    public getSTUrl = '';
    public getDriverUrl = '';
    public getCompanyUrl = '';
    public siteUrl = '';
    public storagetankUrl = '';
    public driverdropdownUrl = '';
    public attachmenturl = '';
    public statusUrl = '';
    public batchUrl = '';
    public approverUrl = '';
    public deleteattachmentUrl = '';
    public stockadjustmentbyid ='';
    public sitefilterUrl = '';
    public getTankDetailsByIdUrl = '';
    public getBatchDetailsByIdUrl ='';
    public getTankNameByIdUrl ='';
    public getSTfromPolUrl= '';
    public getPolfromSiteUrl= '';
    public purchasereceipturl ='';
    public adjustmenttypefilterUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'StockAdjustment/StockAdjustment/';
        this.saveUrl = _configuration.ServerWithApiUrl+'StockAdjustment/SaveStockAdjustment';
        this.stockadjustmentbyid=_configuration.ServerWithApiUrl+'StockAdjustment/GetStockAdjustmentByID?id=';
        this.deleteUrl = _configuration.ServerWithApiUrl+'StockAdjustment/DeleteStockAdjustmentByID?id=';
        this.adjustmenttypeUrl = _configuration.ServerWithApiUrl+'StockAdjustment/AdjustmentType';
        this.adjustmenttypefilterUrl = _configuration.ServerWithApiUrl+'StockAdjustment/GetAdjustmentTypeFilter';
        this.polUrl = _configuration.ServerWithApiUrl+'Masters/POL/';
        this.siteUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.batchUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetBatchDetailsbyStorageTank?TankID=';
        this.storagetankUrl = _configuration.ServerWithApiUrl+'Masters/StorageTank';
        this.attachmenturl = _configuration.ServerWithApiUrl+'StockAdjustment/GetAllStockAdjustmentAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'StockAdjustment/DeleteStockAdjustmentAttachments?Id=';
        this.statusUrl = _configuration.ServerWithApiUrl+'StockAdjustment/GetStockAdjustmentApprovedStatusFilter';
        this.approverUrl = _configuration.ServerWithApiUrl+'StockAdjustment/GetNewStockAdjustmentApprovedUserFilter';
        this.sitefilterUrl=_configuration.ServerWithApiUrl+'StockAdjustment/GetStockAdjustmentSiteFilter'
        this.getTankDetailsByIdUrl=_configuration.ServerWithApiUrl + 'ReceiptAndTransfer/GetAvailableQuantityByTankId?TankId=';
        this.getBatchDetailsByIdUrl=_configuration.ServerWithApiUrl + 'ReceiptAndTransfer/GetBatchTankQuantityByTankId?PurchaseReceiptId=';
        this.getTankNameByIdUrl=_configuration.ServerWithApiUrl + 'Masters/GetStorageTankByID?id=';
        this.getPolfromSiteUrl = _configuration.ServerWithApiUrl + 'Masters/getAllPOLBySite?SiteId=';
        this.getSTfromPolUrl = _configuration.ServerWithApiUrl + 'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.purchasereceipturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseReceiptByID?id=';
    }
    
    getStockAdjustmentList(params: any): Promise<any> {
        return this.authHttp.get( this.listUrl,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getSiteList(): Promise<any> {
        return this.authHttp.get(this.siteUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getSiteFilterList(userId): Promise<any> {
        return this.authHttp.get(this.sitefilterUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getTankList(): Promise<any> {
        return this.authHttp.get(this.storagetankUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getPolList(): Promise<any> {
        return this.authHttp.get(this.polUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getStorageTankListFromSiteById(siteid: string,polid: string) {
        return this.authHttp.get(this.getSTfromPolUrl +  siteid+ '&POLId=' + polid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getPolListFromSiteById(id: string) {
        return this.authHttp.get(this.getPolfromSiteUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getBatchList(tankid:any): Promise<any> {
        return this.authHttp.get(this.batchUrl + tankid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getStockAdjustmentTypeList(): Promise<any> {
        return this.authHttp.get(this.adjustmenttypeUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getStockAdjustmentTypeFilterList(): Promise<any> {
        return this.authHttp.get(this.adjustmenttypefilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getApproverList(): Promise<any> {
        return this.authHttp.get(this.approverUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getStatusList(): Promise<any> {
        return this.authHttp.get(this.statusUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteStockAttachment(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteattachmentUrl;
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    delete(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }
    getStockAttachment(id: string) {
        return this.authHttp.get(this.stockattachmenturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getallStockAttachmentattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getBatchNoDetailsById(id: string,tankid : any) {
        return this.authHttp.get(this.getBatchDetailsByIdUrl + id + '&TankId=' + tankid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getTankDetailsById(id: string) {
        return this.authHttp.get(this.getTankDetailsByIdUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getTankNameById(id: string) {
        return this.authHttp.get(this.getTankNameByIdUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }


    //   Save API for Stock adjustment
Save(bu: any): Promise<any>  {

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

getStockAdjustment(id: string) {
    return this.authHttp.get(this.stockadjustmentbyid +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getPurchaseReceipt(id: string) {
    return this.authHttp.get(this.purchasereceipturl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}