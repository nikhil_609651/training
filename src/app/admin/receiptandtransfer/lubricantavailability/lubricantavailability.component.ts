import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'stock-summary-lube',
    template: `<router-outlet></router-outlet>`
})
export class LubricantAvailabilityComponent {
	constructor(private router: Router) {
	}
}
