import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class LubricantAvailabilityFormService{
    public listUrl = '';
    public siteUrl = '';
    public polcatUrl = '';
    public siteByUserUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.listUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/LubricantAvailibility/';
        this.siteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetLubricantAvailibilitySiteFilter/';
        this.polcatUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetLubricantAvailibilityPOLFilter/';
        this.siteByUserUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
    }

    getSiteList(userId): Promise<any> {
        return this.authHttp.get(this.siteUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getpolListcat(): Promise<any> {
        return this.authHttp.get(this.polcatUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    // List API for Lubricant Availability
    getLubricantAvailabilityList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getSiteListByUser(): Promise<any> {
        return this.authHttp.get(this.siteByUserUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
