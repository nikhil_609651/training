import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormBuilder } from '@angular/forms';
import { LubricantAvailabilityFormService } from 'app/admin/receiptandtransfer/lubricantavailability/service/lubricantavailability.service';

@Component({
  selector: 'lubricantavailability-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./lubricantavailability-list.html')
})
export class LubricantAvailabilityListComponent implements OnInit{

  checkedsite: string[] =[];
  checkedpol:string[]=[];
  selectedpolAll: any;
  selectedsiteAll: any;
  approverGroups: any;
  statusGroups: any;
  TankList: string;
  TankDetails: any;
  siteGroups: any;
  polGroups: any;
  selectedAll: any;
  checked: string[] = [];
  SiteIncharge: any;
  viewPRForm: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;

  public polavailability: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true },
    { title: 'Site', name: 'Sitename', sort: true,filter:true },
    { title: 'POL', name: 'POL', sort: true, filter:true },
    { title: 'Available No Of Packets', name: 'POLAvailableQuantity', sort: false,filter:true },
    { title: 'Pack Size', name: 'PackSize', sort: true, filter:true },
    { title: 'Volume', name: 'PurchasedVolume', sort: true, filter:true },
    //{ title: 'Actions', className: ['text-center'], name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public pr_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  API_URL_Export: string;

  constructor(private router: Router,
  private fb: FormBuilder,
  private lubService: LubricantAvailabilityFormService,
  private configuration: Configuration) {
    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('SiteFilter', '');
    this.params.set('POLFilter', '');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.rows = configuration.rows;
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  ngOnInit() {
    this.getLubricantAvailabilityList(this.params);
    this.totalfeilds = this.columns.length;
    this.getPOL();
    let userId = localStorage.getItem('user_nameId');
    this.getSite(userId);
    this.getSiteList();
  }

  getLubricantAvailabilityList(params: any) {
    this.loading = true;
    this.lubService.getLubricantAvailabilityList(params).then(response => {
      this.polavailability = response['result'];
      if(this.polavailability.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);
    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getLubricantAvailabilityList(params);

    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());
    var sortParam = '';
    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getLubricantAvailabilityList(this.params);
  }

  // Filteration
  selectAll(item, event) {
    if(item === 'Site'){
        this.checkArr = [];
        for (var i = 0; i < this.siteGroups.length; i++) {
          this.siteGroups[i].selected = this.selectedsiteAll;
          if(event.target.checked){
              this.checkArr.push(this.siteGroups[i].siteId.toString());
          } else {
              this.checkArr = [];
          }
        }
         this.params.set('SiteFilter', this.checkArr.toString());
    }

    if(item === 'Pol'){
      this.checkPolArr=[]
      for (var i = 0; i < this.polGroups.length; i++) {
        this.polGroups[i].selected = this.selectedpolAll;
        if(event.target.checked){
            this.checkPolArr.push(this.polGroups[i].id.toString());
        } else {
            this.checkPolArr = [];
        }
      }
       this.params.set('POLFilter', this.checkPolArr.toString());
    }
  }

  checkArr = [];
  checkPolArr=[];
  checkIfAllSelected(option, event,item) {
    if(item=='Site')
    {
      this.selectedsiteAll = this.siteGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkArr.indexOf(key);
      if(event.target.checked) {
        this.checkArr.push(event.target.value);
      } else {
        this.checkArr.splice(index,1);
      }
        this.params.set('SiteFilter', this.checkArr.toString());
    }

    if(item === 'Pol'){
      this.selectedpolAll = this.polGroups.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkPolArr.indexOf(key);
      if(event.target.checked) {
        this.checkPolArr.push(event.target.value);
      } else {
        this.checkPolArr.splice(index,1);
      }
        this.params.set('POLFilter', this.checkPolArr.toString());
      }
  }

  // For Applying Filtering to List
  apply(item){
      this.getLubricantAvailabilityList(this.params);
      if(item === 'Pol'){
            setTimeout(function () {
              jQuery('#ViewPOLModal').modal('toggle');
            }.bind(this) , 0);
          }
          jQuery('ViewPOLModal').modal('hide');
          jQuery('body').removeClass('modal-open');
          jQuery('.modal-backdrop').remove();
      if(item === 'Site'){
        setTimeout(function () {
          jQuery('#ViewSiteModal').modal('toggle');
        }.bind(this) , 0);

        jQuery('ViewSiteModal').modal('hide');
        jQuery('body').removeClass('modal-open');
        jQuery('.modal-backdrop').remove();
      }
  }

  Sort(param,order){
    //this.params.set('ordering', sortParam);
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getLubricantAvailabilityList(this.params);
  }

  getSite(siteId){
    this.lubService.getSiteList(siteId).then(r =>  {
        this.siteGroups = r;
        this.loading = false;
      });
  }

  getPOL(){
    this.lubService.getpolListcat().then(r =>  {
        this.polGroups = r;
        console.log( this.polGroups)
        this.loading = false;
      });

  }

  getAllLists(params: any) {
    this.loading = true;
    this.lubService.getLubricantAvailabilityList(params).then(response => {
      this.polavailability = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  siteList : any = [];
  selectedSiteItems = [];
  dropdownSettings = {};
  siteIdList : any = [];
  exportErrorMsg = '';
  getSiteList() {
    this.lubService.getSiteListByUser().then(response =>{
      let siteDetails = response.result;
      siteDetails.forEach(site => {
          this.siteList.push({
              item_id: site.siteId , item_text: site.name
          })
      });
      console.log('siteList', this.siteList);
  }).catch(e => {
      this.handleError(e);
  })
  }

  onItemSelect(event) {
    this.exportErrorMsg = '';
    let isIdPresent = false;
    if(this.siteIdList.length == 0) {
      this.siteIdList.push(event.item_id);
    } else {
      this.siteIdList.forEach(id=> {
          if(id == event.item_id) {
              isIdPresent = true;
          }
      });
      if(!isIdPresent) {
          this.siteIdList.push(event.item_id);
      }
    }
  }

  onItemDeSelect(event) {
    this.exportErrorMsg = '';
    let index = this.siteIdList.indexOf(event.item_id);
    if(index > -1) {
      this.siteIdList.splice(index, 1);
    }
  }

  onSelectAll(event) { 
    this.siteIdList = [];
    this.exportErrorMsg = '';
    event.forEach(id => {
        this.siteIdList.push(id.item_id);
    });
  }

  onDeSelectAll(event) {
    this.exportErrorMsg = '';
    this.siteIdList = [];
  }

  export() {
    if(this.selectedSiteItems.length == 0) {
      this.exportErrorMsg = 'Please select site';
    } else {
      this.exportErrorMsg = '';
      let userId = localStorage.getItem('user_nameId');
      this.API_URL_Export = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/ExportStockSummaryLube?SiteString=' + this.siteIdList.toString() + '&UserId=' + userId;
      var exportList =  document.getElementById('export');
      exportList.setAttribute("href", this.API_URL_Export);
    }
  }

  modalClose() {
    this.selectedSiteItems = [];
    this.siteIdList = [];
    this.exportErrorMsg = '';
  }

  private handleError(e: any) {
      let detail = e.detail
      /*console.log(detail)*/
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
