import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'stock-summary-tank',
    template: `<router-outlet></router-outlet>`
})
export class TankPolAvailabilityComponent {
	constructor(private router: Router) {
	}
}
