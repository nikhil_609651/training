import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class TankPolAvailabilityFormService {
    public listUrl = '';
    public siteUrl = '';
    public tankUrl = '';
    public polcatUrl = '';
    public siteByUserUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.listUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/TankPOLAvailibility/';
        this.siteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPOLInventorySiteFilter/';
        this.tankUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPOLInventoryTankFilter/';
        this.polcatUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPOLInventoryPOLFilter/';
        this.siteByUserUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
    }

    // List API for Purchase Receipt
    getTankPolAvailabilityList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getSiteList(userId): Promise<any> {
        return this.authHttp.get(this.siteUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankList(): Promise<any> {
        return this.authHttp.get(this.tankUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getpolListcat(): Promise<any> {
        return this.authHttp.get(this.polcatUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteListByUser(): Promise<any> {
        return this.authHttp.get(this.siteByUserUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
