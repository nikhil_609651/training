import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransferFormService } from 'app/admin/receiptandtransfer/transfer/service/transfer.service';


@Component({
  selector: 'add',
  template: require('./transfer-add.html')
})
export class TransferAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private prService: TransferFormService) {
  }
  ngOnInit() {
    this.page = 'add';

  }
  onSave(tr: any) {
    let files = tr.uploadedFile;
    let formData = new FormData();

    let compartmentDetails;
    let transferLubeDetails;
    let batchDetails =JSON.stringify(tr.BatchDetails);
    let tankId;
    let transportCompanyId =  tr.Transporter? tr.Transporter : 0;
    let truckId =  tr.Truck? tr.Truck : 0;
    let driverId =  tr.Driver? tr.Driver : "null";
    if(tr.IsLubricant==false)
    {
      compartmentDetails =  JSON.stringify(tr.CompartmentDetails);
      transferLubeDetails=null;
    }
    else
    {
       transferLubeDetails =  JSON.stringify(tr.TransferLubeDetails);
       compartmentDetails=null;
    }
    let date = tr.DateLoaded;
    if(typeof date == 'object'){
      if(date.date.month < 10 && date.date.day < 10) {
        date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      } else if(date.date.day < 10 && date.date.month >= 10) {
        date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
      } else if(date.date.day >= 10 && date.date.month < 10) {
        date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      } else {
        date = date.date.day + '/' + date.date.month + '/' + date.date.year;
      }
    } else {
      date= tr.DateLoaded;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', '0');
    formData.append('IsLubricant', tr.IsLubricant);
    formData.append('StorageTankID', tr.Tank);
    formData.append('IsTransferReceipt', tr.IsTransferReceipt);
    formData.append('TransferRequestId', tr.TransferNoId);
    formData.append('DateLoaded', date);
    formData.append('TransportCompanyId', transportCompanyId);
    formData.append('LoadingTerminal', tr.LoadingTerminal);
    formData.append('ReceivingTerminal', "");
    formData.append('TruckId', truckId);
    formData.append('DriverId', driverId);
    formData.append('TruckOdoMeterReading', tr.TruckOdometerReading);
    formData.append('ConsigmentNo', tr.ConsignmentNo);
    formData.append('TransferCompartmentDetails', compartmentDetails);
    formData.append('TransferLubricantDetails', transferLubeDetails);
    formData.append('BatchDetails', batchDetails);
    formData.append('Remarks', tr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.prService.Save(formData).then(r =>  {
      this.success = 'Transfer Created Successfully!';

      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/transfer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
