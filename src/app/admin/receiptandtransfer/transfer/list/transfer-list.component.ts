import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormBuilder } from '@angular/forms';
import { TransferFormService } from 'app/admin/receiptandtransfer/transfer/service/transfer.service';
import { IMyOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'transfer-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./transfer-list.html')
})
export class TransferListComponent implements OnInit{

  compartmentDetails=[];
  transferLubeDetails=[];
  approverGroups: any;
  statusGroups: any;
  TankList: string;
  TankDetails: any;
  siteGroups: any;
  polGroups: any;

  selectedapproverAll: any;
  selectedstatusAll: any;
  selectedsiteAll: any;
  selectedpolAll: any;

  checkedapprover: string[] = [];
  checkedsite: string[] =[];
  checkedstatus:string[]=[];
  checkedpol:string[]=[];
  SiteIncharge: any;
  siteName: any;
  viewTRForm: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  sId: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  public transferrequest: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  private currentdate= new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    //disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}
  };
  private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  fromDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();

  private searchFromDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };

  private searchToDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };

  private initialSearchFromDate: Object = { date: { day: 1, month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialSearchToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  toDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
  public columns:Array<any> = [

    { title: 'Sl.No', name: 'Id', sort: false },
    { title: 'Transfer Id', name: 'TransferId', sort: false },
    { title: 'Site', name: 'FromSite', sort: true,filter:true },
    { title: 'Transferred to', name: 'ToSite', sort: false,filter:true },
    { title: 'Volume', name: 'Volume', sort: true },
    { title: 'Date Loaded', name: 'DateLoaded', sort: true, filter:true },
    { title: 'POL', name: 'POL', sort: true, filter:true },
    { title: 'Storage Tank', name: 'StorageTank', sort: false },
    { title: 'Transporter', name: 'Transporter', sort: false },
    { title: 'Loading Terminal', name: 'LoadingTerminal', sort: false },
    { title: 'Truck', name: 'Truck', sort: false },
    { title: 'Driver', name: 'Driver', sort: false },
     { title: 'Consignment Note No', name: 'ConsignmentNo', sort: true },
    //{ title: 'Consgn No', name: 'ConsignmentNo', sort: true },
    { title: 'Status', name: 'Status', sort: false,filter:true },
    { title: 'Approver', name: 'ApprovedBy', sort: true,filter:true },
    { title: 'Actions', className: ['text-center'], name: 'actions', sort: false }

  ];
  API_URL_Export: string;
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  public tr_deleted_id = '';
  public deletemessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  params: URLSearchParams = new URLSearchParams();

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
  private fb: FormBuilder, private _sharedService : SharedService,
  private trService: TransferFormService,
  private configuration: Configuration) {

    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	    this.privileges = privileges;
    });
    if ((new Date().getDate() < 10) && (new Date().getMonth() > 10) ) {
      this.toDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    if ((new Date().getDate() < 10) && (new Date().getMonth() < 10) ) {
      this.toDate = '0' + new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    if ((new Date().getDate() > 10) && (new Date().getMonth() < 10) ) {
      this.toDate =  new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate =  new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    let searchToDate = '';
    if(Number(new Date().getMonth()+1) < 10 && new Date().getDate() < 10) {
      searchToDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if(new Date().getDate() < 10 && Number(new Date().getMonth()+1) >= 10) {
      searchToDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if(new Date().getDate() >= 10 && Number(new Date().getMonth()+1) < 10) {
      searchToDate = new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else {
      searchToDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    }
    let searchFromDate = '';
    if(Number(new Date().getMonth()+1) < 10) {
      searchFromDate = '01' + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if( Number(new Date().getMonth()+1) >= 10) {
      searchFromDate = '01' + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else {
      searchFromDate = '01/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    }
    this.params.set('FromDate', searchFromDate);
    this.params.set('ToDate', searchToDate);

    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('SiteFilter', '');
    this.params.set('StatusFilter', '');
    this.params.set('ApproverFilter', '');
    this.params.set('POLFilter', '');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.rows = configuration.rows;
    this.viewTRForm = fb.group({

      'TransferNo': [''],
      'FromSite': [''],
      'ToSite': [''],
      'DateLoaded': [''],
      'Tank': [''],
      'Transporter': [''],
      'LoadingTerminal': [''],
      'Truck': [''],
      'Driver': [''],
      'TruckOdometerReading': [''],
      'ConsignmentNo': [''],
      'Remarks': [''],
      'SiteIncharge': [''],
  });
  var vForm = this.viewTRForm;
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_nameId');
    this.API_URL_Export = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/ExportTransferDetailsFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
    this.getTransferList(this.params);
    this.totalfeilds = this.columns.length;
    this.getPOL();
    this.getSite(userId);
    this.getStatus();
    this.getApprover();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getTransferList(params: any) {
    this.loading = true;
    this.transferrequest= [];
    this.trService.getTransferList(params).then(response => {
      this.transferrequest = response['result'];
      if(this.transferrequest.length > 0){
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false; }
      else{
        this.page = 1;
      this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }
  getSite(userId){
    this.trService.getFilterSiteList(userId).then(r =>  {
        this.siteGroups = r;
        this.loading = false;
      });
  }
  getStatus(){
    this.trService.getApprovedStatusList().then(r =>  {
        this.statusGroups = r;
        this.loading = false;
      });
  }
  getApprover(){
    this.trService.getApproverList().then(r =>  {
        this.approverGroups = r;
        this.loading = false;
      });
  }
  getPOL(){
    this.trService.getFilterPolList().then(r =>  {
        this.polGroups = r;
        this.loading = false;
      });

  }

  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);

    if(this.next || (changedValue < this.itemsPerPage)){
      this.itemsPerPage =  event.target.value;
      let params = this.params;

      params.set('limit', event.target.value);
      this.getTransferList(params);

    }
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {

    let params = this.params;

    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',  start.toString());

    var sortParam = '';

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getTransferList(this.params);
  }
  view(data){
        this.transferLubeDetails=[];
        this.compartmentDetails=[];
        console.log(data);
        this.SiteIncharge = data.users1.firstName;
        this.viewTRForm.controls['TransferNo'].setValue(data.transferRequest.transferId);
        this.viewTRForm.controls['FromSite'].setValue(data.transferRequest.fromSite.name);
        this.viewTRForm.controls['ToSite'].setValue(data.transferRequest.requestedSite.name);
        this.viewTRForm.controls['DateLoaded'].setValue(data.loadDate);
        if(data .storageTank!=null)
        {
          this.viewTRForm.controls['Tank'].setValue(data .storageTank.tankName);
        }
        if(data .driver!=null)
        {
          this.viewTRForm.controls['Driver'].setValue(data.driver.driverName);
        }
        if(data .transportCompany!=null)
        {
          this.viewTRForm.controls['Transporter'].setValue(data.transportCompany.name);
        }
        this.viewTRForm.controls['LoadingTerminal'].setValue(data.loadingTerminal);
        if(data .truck!=null)
        {
          this.viewTRForm.controls['Truck'].setValue(data.truck.regNo);
        }
        this.viewTRForm.controls['TruckOdometerReading'].setValue(data.truckOdoMeterReading);
        this.viewTRForm.controls['ConsignmentNo'].setValue(data.consigmentNo);
        this.viewTRForm.controls['Remarks'].setValue(data.remarks);
        this.viewTRForm.controls['SiteIncharge'].setValue(this.SiteIncharge);

        if(data.transferCompartmentDetails.length>0)
        {
          for(let i=0;i<data.transferCompartmentDetails.length;i++)
          {
            if(data.transferCompartmentDetails[i].isDeleted==0)
            {
              this.compartmentDetails.push(data.transferCompartmentDetails[i]);
            }
          }
        }

        if(data.transferLubricantDetails.length>0)
        {
          for(let i=0;i<data.transferLubricantDetails.length;i++)
          {
            if(data.transferLubricantDetails[i].isDeleted==0)
            {
              this.transferLubeDetails.push(data.transferLubricantDetails[i]);
            }
          }
        }

}
  addtransfer() {
    this.router.navigate(['./home/receipt-and-transfer/transfer/add']);
  }

  edittransfer(id) {

    this.router.navigate(['./home/receipt-and-transfer/transfer/view/', id]);
  }

  deletetransfer(tr){

    this.tr_deleted_id = tr;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }

  deletetrconfirm(tr){
      this.tr_deleted_id= tr.id;
      this.trService.deleteTransfer(this.tr_deleted_id).then(r =>  {
        this.getTransferList(this.params);
        this.deletemessage = "Transfer Deleted Successfully";

      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      })
  }

  //Filteration
  selectAll(item, event) {
    if(item === 'Site'){
        this.checkArr = [];
        for (var i = 0; i < this.siteGroups.length; i++) {
          this.siteGroups[i].selected = this.selectedsiteAll;
          if(event.target.checked){
              this.checkArr.push(this.siteGroups[i].siteId.toString());
          } else {
              this.checkArr = [];
          }
        }
         this.params.set('SiteFilter', this.checkArr.toString());
    }

    if(item === 'Status'){
      this.checkStatusArr=[]
      for (var i = 0; i < this.statusGroups.length; i++) {
        this.statusGroups[i].selected = this.selectedstatusAll;
        if(event.target.checked){
            this.checkStatusArr.push(this.statusGroups[i].id.toString());
        } else {
            this.checkStatusArr = [];
        }
      }
       this.params.set('StatusFilter', this.checkStatusArr.toString());
    }
    if(item === 'Approver'){
      this.checkApproverArr=[]
      for (var i = 0; i < this.approverGroups.length; i++) {
        this.approverGroups[i].selected = this.selectedapproverAll;
        if(event.target.checked){
            this.checkApproverArr.push(this.approverGroups[i].id.toString());
        } else {
            this.checkApproverArr = [];
        }
      }
       this.params.set('ApproverFilter', this.checkApproverArr.toString());
    }
    if(item === 'Pol'){
      this.checkPolArr=[]
      for (var i = 0; i < this.polGroups.length; i++) {
        this.polGroups[i].selected = this.selectedpolAll;
        if(event.target.checked){
            this.checkPolArr.push(this.polGroups[i].id.toString());
        } else {
            this.checkPolArr = [];
        }
      }
       this.params.set('POLFilter', this.checkPolArr.toString());
    }
}
 checkArr = [];
 checkStatusArr=[];
 checkApproverArr=[];
 checkPolArr=[];
 checkIfAllSelected(option, event,item) {
if(item=='Site')
{
  this.selectedsiteAll = this.siteGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkArr.indexOf(key);
  if(event.target.checked) {
    this.checkArr.push(event.target.value);
  } else {
    this.checkArr.splice(index,1);
  }
    this.params.set('SiteFilter', this.checkArr.toString());
}

 if(item === 'Status'){
  this.selectedstatusAll = this.statusGroups.every(function(item:any) {
    return item.selected == true;
  })

  var key = event.target.value.toString();
  var index = this.checkStatusArr.indexOf(key);
  if(event.target.checked) {
    this.checkStatusArr.push(event.target.value);
  } else {
    this.checkStatusArr.splice(index,1);
  }
    this.params.set('StatusFilter', this.checkStatusArr.toString());
}

 if(item === 'Approver'){
  this.selectedapproverAll = this.approverGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkApproverArr.indexOf(key);
  if(event.target.checked) {
    this.checkApproverArr.push(event.target.value);
  } else {
    this.checkApproverArr.splice(index,1);
  }
    this.params.set('ApproverFilter', this.checkApproverArr.toString());
}

if(item === 'Pol'){
  this.selectedpolAll = this.polGroups.every(function(item:any) {
    return item.selected == true;
  })
  var key = event.target.value.toString();
  var index = this.checkPolArr.indexOf(key);
  if(event.target.checked) {
    this.checkPolArr.push(event.target.value);
  } else {
    this.checkPolArr.splice(index,1);
  }
    this.params.set('POLFilter', this.checkPolArr.toString());
  }
}
 // For Applying Filtering to List
 apply(item){
  this.getTransferList(this.params);
  if(item === 'Pol'){


        setTimeout(function () {
          jQuery('#ViewPOLModal').modal('toggle');
        }.bind(this) , 0);
       }
   if(item === 'Site'){


    setTimeout(function () {
      jQuery('#ViewSiteModal').modal('toggle');
    }.bind(this) , 0);
   }
   if(item === 'Status'){
    setTimeout(function () {
      jQuery('#ViewStatusModal').modal('toggle');
    }.bind(this) , 0);
   }

   if(item === 'Approver'){

    setTimeout(function () {
      jQuery('#ViewApproverModal').modal('toggle');
    }.bind(this) , 0);
   }
   jQuery('ViewPOLModal').modal('hide');
   jQuery('ViewSiteModal').modal('hide');
   jQuery('ViewStatusModal').modal('hide');
   jQuery('ViewApproverModal').modal('hide');
   jQuery('body').removeClass('modal-open');
   jQuery('.modal-backdrop').remove();
   }


// For File Attachments
attachment(id){
  this.sId=id;
  this.attachmentmessage='';
  this.API_URL = this.configuration.ServerWithApiUrl +'ReceiptAndTransfer/DownloadTransferDetailsFiles?FileId=';
this.trService.getallTransferAttachments(id).then(response => {
this.attachments=response;
if(this.attachments.length > 0){
  var LongArray = [];
  for(var i = 0; i < this.attachments.length; i++) {
    let ext=this.attachments[i].location;
    var Obj = {
     id :this.attachments[i].id,
     referenceId:this.attachments[i].referenceId,
     shortFileName : this.attachments[i].shortFileName,
     fileName:this.attachments[i].fileName,
      ext :  ext.substr(ext.lastIndexOf('.') + 1),

    };
    LongArray.push(Obj);
    this.attachmentGroup=LongArray;
   }
}
else{
 this.getTransferList(this.params);
 this.attachmentmessage='No Attachments Found'
 setTimeout(function () {
   jQuery('#ViewAttachmentModal').modal('toggle');
 }.bind(this), 1000);
}
return this.attachmentGroup;
})

}
viewclose(){
  setTimeout(function () {
      jQuery('#ViewTransfer').modal('toggle');
    }.bind(this), 0);
}
viewcloseattachment(){
  setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
}
// For Deleting Attachments

deleteattachment(id){
  this.attachment_deleted_id = id;
}
deleteattachmentmessageclear(){
  this.deleteattachmentmessage = '';
}

deleteattachmentconfirm(id){
  // if (confirm) {

    this.trService.deleteattachment(this.attachment_deleted_id).then(r =>  {
      this.attachment(this.sId);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r =>  {
       // console.log();
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
}
// For Sorting
Sort(param,order){
  //this.params.set('ordering', sortParam);

  if(order === 'desc'){
    this.params.set('ordering', '-'+param);
  }
  if(order === 'asc'){
    this.params.set('ordering', param);
      }
      this.getTransferList(this.params);
}

  getAllLists(params: any) {
    this.loading = true;
    this.trService.getTransferList(params).then(response => {
      this.transferrequest = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }
  onDateChanged(event:any, type: any) {
    if(type == 'FromDate'){
      this.fromDate = event.formatted;
    }
    if(type == 'ToDate'){
      this.toDate = event.formatted;

    }
  }

  export(){
    let userId = localStorage.getItem('user_nameId');
    this.API_URL_Export = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/ExportTransferDetailsFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
  }

  onSearchDateChanged(date, type) {
    let dateFromPicker = { year: date.date.year, month: date.date.month, day: date.date.day };
    if(date.date.month < 10 && date.date.day < 10) {
      date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else if(date.date.day < 10 && date.date.month >= 10) {
      date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
    } else if(date.date.day >= 10 && date.date.month < 10) {
      date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else {
      date = date.date.day + '/' + date.date.month + '/' + date.date.year;
    }
    if(type == 'fromDate') {
      let toDateOptionCopy = this.getCopyOfSearchToDatePickerOptions()
      dateFromPicker.day = dateFromPicker.day-1;
      toDateOptionCopy.disableUntil = dateFromPicker;
      this.searchToDatePickerOptions = toDateOptionCopy;
      this.params.set('FromDate', date);
      this.getTransferList(this.params);
    } else if(type == 'toDate') {
      let fromDateOptionCopy = this.getCopyOfSearchFromDatePickerOptions();
      dateFromPicker.day = dateFromPicker.day+1;
      fromDateOptionCopy.disableSince = dateFromPicker;
      this.searchFromDatePickerOptions = fromDateOptionCopy;
      this.params.set('ToDate', date);
      this.getTransferList(this.params);
    }
  }

  getCopyOfSearchToDatePickerOptions(): IMyOptions {
      return JSON.parse(JSON.stringify(this.searchToDatePickerOptions));
  }

  getCopyOfSearchFromDatePickerOptions(): IMyOptions {
      return JSON.parse(JSON.stringify(this.searchFromDatePickerOptions));
  }

  private handleError(e: any) {
      let detail = e.detail
      /*console.log(detail)*/
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
