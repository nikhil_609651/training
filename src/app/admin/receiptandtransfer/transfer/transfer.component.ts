import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'transfer',
    template: `<router-outlet></router-outlet>`
})
export class TransferComponent {
	constructor(private router: Router) {
	}
}
