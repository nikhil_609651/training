import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransferFormService } from 'app/admin/receiptandtransfer/transfer/service/transfer.service';

@Component({
  selector: 'transfer-edit',
  template: require('./transfer-edit.html')
})
export class TransferEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public pr: any;
  public page = 'edit';

	constructor(private router: Router, private route: ActivatedRoute,
    private prService: TransferFormService) {

	}
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
        this.pr = this.prService.getTransfer(this.id);
      });
	}

  onSave(tr: any) {
    let files = tr.uploadedFile;
    let formData = new FormData();

    let compartmentDetails;
    let transferLubeDetails;
    if(tr.Truck == null){
      tr.Truck = '';
    }
    if(tr.Driver == null){
      tr.Driver = '';
    }
    if(tr.Transporter == null){
      tr.Transporter = '';
    }
    if(tr.IsLubricant==false)
    {
      compartmentDetails =  JSON.stringify(tr.CompartmentDetails);
      transferLubeDetails=null;
    }
    else
    {
       transferLubeDetails =  JSON.stringify(tr.TransferLubeDetails);
       compartmentDetails=null;
    }
    let date = tr.DateLoaded;
    if(typeof date == 'object'){
      date=date.date.day + '/' + date.date.month + '/' + date.date.year;

    } else {
      date= tr.DateLoaded;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', tr.TId);
    formData.append('StorageTankID', tr.Tank);
    formData.append('IsLubricant', tr.IsLubricant);
    formData.append('IsTransferReceipt', tr.IsTransferReceipt);
    formData.append('TransferRequestId', tr.TransferNoId);
    formData.append('DateLoaded', date);
    formData.append('TransportCompanyId', tr.Transporter);
    formData.append('LoadingTerminal', tr.LoadingTerminal);
    formData.append('ReceivingTerminal', "");
    formData.append('TruckId', tr.Truck);
    formData.append('DriverId', tr.Driver);
    formData.append('TruckOdoMeterReading', tr.TruckOdometerReading);
    formData.append('ConsigmentNo', tr.ConsignmentNo);
    formData.append('TransferCompartmentDetails', compartmentDetails);
    formData.append('TransferLubricantDetails', transferLubeDetails);
    formData.append('Remarks', tr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.prService.Save(formData).then(r =>  {
      this.success = 'Transfer Updated Successfully!';

      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/transfer']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }
  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }

}
