
import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { IMyOptions } from 'mydatepicker';
import { TransferFormService } from 'app/admin/receiptandtransfer/transfer/service/transfer.service';
import { TransferReceiptFormService } from '../../transferreceipt/service/transferreceipt.service';

@Component({
  selector: 'transfer-form',
  templateUrl: './transfer-form.html',
})
export class TransferFormComponent implements OnInit,  OnChanges {
  userId: string;
  TransferLubeDetails: AbstractControl;
  CompartmentDetails: AbstractControl;
  CLength: number;
  Temperature = 0;
  temperatureMessage: string='';
  Density=0;
  densityMessage: string='';
  batchErrorMessage: string='';
  notificationMeaasge: string='';
  CompartmentPolVolume=0;
  tankAvailableQtyErrorMsg: string='';
  totalAvailableQty=0;
  LubeErrorMsg: string='';
  totalNoOfPacks=0;
  polId: any;
  fromSiteId: any;
  polErrorMsg: string='';
  totalPolVolume=0;
  reqQuantity=0;
  savebutton: boolean =true;
  requestedSiteId: any;
  tankId: any;
  PRId: any;
  BatchNo: any;
  polVolume: any;
  prList: any;
  transferReqId: any;
  POL: any;
  volume=0;
  packNo: any;
  package: any;
  packsize=0;
  transporterList: any;
  siteList: any;
  typeReq: string;
  transferList: any;
  // LoadingDate: Date;
  selectedDate: Date;
  Quantity: any;
  printedvalue=0;
  secondaryvalue=0;
  lastvalue=0;
  totalReceivedQuantity=0;
  receivedQuantity=0;
  finalvalue=0;
  initialvalue=0;
  companyName: any;
  mobNo="";
  toSite="";
  fromSite="";
  isLubricantSection=false;
  IsLubricant=false;
  tankList: any;
  loadingDate: string;
  truckList: any;
  driverList: any;
  pollist: any;
  @Input() pr;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  trForm: FormGroup;

  filelength: number;
  sizeError: string;
  ErrorList=[];
  CapacityErrorList=[];
  StorageTankList=[];
  filetype= [];
  filelist= [];

  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public pr_id = '';
  public PId:any;
  public text = '';
  public drop = '';
  items: any;
  public addRes: any[] = [];
  dateError : any = "";
  createdDate : any;
  tankReq : any = "";
  compartmentLimit: number = 5;
  isCompartmentLimitReached: boolean = false;
  transporterReq = "";
  truckReq = "";
  driverReq = "";
  public siteGroups =[];
  loadingDateViewPage : any;
  transferId : string = '';
  requestedQuantity = 0;
  compBatchList = [];
  batchDetailsList = [];
  batchList = [];
  batchListClone = [];
  compartmentVolume = [];
  arrangedBatchList = [];
  private currentdate= new Date();
  //private startDate: Object = { date: { year: 2008, month: 01, day: 01 }    };
  private myDatePickerOptions: IMyOptions = {
  dateFormat: 'dd/mm/yyyy',
  //disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()-1}

};
  //private LoadingDate :Date;
  private DateLoaded: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
  //params: URLSearchParams = new URLSearchParams()

  constructor( private trService: TransferFormService, private router: Router,
    private route: ActivatedRoute, private transferReceiptFormService: TransferReceiptFormService ) {
      this.userId=localStorage.getItem('user_nameId');
      this.getPolList();
      this.getSiteList();
      this.getRequestList();
      this.getDriverList();
      this.getTruckList();
      this.getTransporterList();
  }

  readOnly: boolean = false;
  ngOnInit() {

    if(this.page === 'add'){
    this.trForm = new FormGroup({
      TId:new FormControl(''),
      TransferNoId:new FormControl('', Validators.required),
      DateLoaded:new FormControl(''),
      Tank: new FormControl('', Validators.required),
      Transporter: new FormControl('', Validators.required),
      LoadingTerminal: new FormControl(''),
      Truck: new FormControl('', Validators.required),
      Driver: new FormControl('', Validators.required),
      TruckOdometerReading: new FormControl(''),
      ConsignmentNo:new FormControl(''),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),

      FromSite: new FormControl(''),
      ToSite: new FormControl(''),
      POLId:new FormControl(''),

      CompartmentDetails: new FormArray([
        this.initSection([]),
      ]),

      TransferLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),

    });

    this.route.params.forEach((params: Params) => {
      this.pr_id = params['TRId'];
  });
}
  if (this.page === 'edit') {
    this.trForm = new FormGroup({
      TId: new FormControl(''),
      TransferNoId: new FormControl('',Validators.required),
      DateLoaded:new FormControl(''),
      Tank: new FormControl(''),
      Transporter: new FormControl(''),
      LoadingTerminal: new FormControl(''),
      Truck: new FormControl('', Validators.required),
      Driver: new FormControl('', Validators.required),
      TruckOdometerReading: new FormControl(''),
      ConsignmentNo:new FormControl(''),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),

      FromSite: new FormControl(''),
      ToSite: new FormControl(''),

      CompartmentDetails: new FormArray([
        //this.initSection([]),
      ]),

      TransferLubeDetails: new FormArray([
        //this.initLubeSection([]),
      ]),

    });
    if( this.page == 'edit') {
      this.trForm.disable();
      this.CompartmentDetails = this.trForm.get('CompartmentDetails');
      this.TransferLubeDetails = this.trForm.get('TransferLubeDetails');
      this.CompartmentDetails.disable();
      this.TransferLubeDetails.disable();
      this.readOnly = true;
      const tankControls = (<FormArray>this.trForm.get('CompartmentDetails')).controls;
      tankControls.forEach(control => {
        (<FormGroup>control).controls['POL'].disabled = true;
        control.get('BatchNo').disabled = true;
        (<FormGroup>control).controls['Volume'].disable();
        //control.get('Volume').disabled = true;
        control.get('ObservedDensity').disabled = true;
        control.get('ObservedTemprature').disabled = true;
        control.get('SealNo').disabled = true;
        control.get('Conductivity').disabled = true;
        control.get('Visual').disabled = true;
        control.get('PurchaseReceiptId').disabled = true;
      });

      const lubControls = (<FormArray>this.trForm.get('TransferLubeDetails')).controls;
      lubControls.forEach(control => {
        control.get('POLId').disabled = true;
        control.get('Package').disabled = true;
        control.get('PackSize').disabled = true;
        control.get('NumberOfPackets').disabled = true;
        control.get('Volume').disabled = true;
      });
    }

      this.trForm.get('DateLoaded').disable();
    }
    this.route.params.forEach((params: Params) => {
      this.pr_id = params['TRId'];
  });
}

  initSection(data = []) {
    let Quantity = data['volume']  ? data['volume'] : '';
    let BatchNo = data['batchNumber']  ? data['batchNumber'] : this.BatchNo;
    let SealNo = data['sealNo']  ? data['sealNo'] : 0;
    let ObservedDensity = data['observedDensity']  ? data['observedDensity'] : 0;
    let ObservedTemperature = data['observedTemprature']  ? data['observedTemprature'] :0;
    let Conductivity = data['conductivity']  ? data['conductivity'] : 0;
    let Visual = data['visual']  ? data['visual'] : '';
    this.totalPolVolume+=Quantity;
    return new FormGroup({
      POL: new FormControl(),
      BatchNo: new FormControl(this.BatchNo),
      Volume: new FormControl(Quantity),
      ObservedDensity: new FormControl(ObservedDensity),
      ObservedTemprature: new FormControl(ObservedTemperature),
      SealNo: new FormControl(SealNo),
      Conductivity: new FormControl(Conductivity),
      Visual: new FormControl(Visual),
      PurchaseReceiptId:new FormControl(this.PRId)
    });
  }
  initLubeSection(data = []) {
    let Product = data['polId']  ? data['polId'] : '';
    let Package = data['uomName']  ? data['uomName'] : this.package;
    let PackSize = data['packSize']  ? data['packSize'] : this.packsize;
    let NoOfPacks = data['numberOfPackets']  ? data['numberOfPackets'] : '';
    this.totalNoOfPacks=NoOfPacks;
    let Volume = data['volume']  ? data['volume'] : this.volume;
    this.packsize=PackSize;
    this.package=Package;
    this.packNo=NoOfPacks;
    this.volume=Volume;
    return new FormGroup({
      POLId: new FormControl(Product),
      Package: new FormControl(Package),
      PackSize : new FormControl(PackSize),
      NumberOfPackets : new FormControl(NoOfPacks),
      Volume : new FormControl(Volume),
    });
  }

  compartmentDetailsReq = "";
  addSection(i, section) {
    if(section.value['Volume'] == "" || typeof(section.value['Volume']) == 'undefined') {
      this.compartmentDetailsReq = "Compartment Details can't be empty";
    } else {
      this.polErrorMsg='';
      this.densityMessage ="";
      this.temperatureMessage ="";
      this.batchErrorMessage="";
      this.compartmentDetailsReq ="";
      const control = <FormArray>this.trForm.get('CompartmentDetails');
      //this.batchList = JSON.parse(JSON.stringify(this.batchListClone[i+1]));
      control.push(this.initSection());
      if(control.length == this.compartmentLimit ) {
        this.isCompartmentLimitReached = true;
      } else {
        this.isCompartmentLimitReached = false;
      }
    }
  }

  addLubeSection() {
    this.LubeErrorMsg='';
    this.batchErrorMessage="";
    const control = <FormArray>this.trForm.get('TransferLubeDetails');
    control.push(this.initLubeSection());
  }

  getCompartmentDetails(form) {
    return form.controls.CompartmentDetails.controls;
  }

  getTransferLubeDetails(form) {
    return form.controls.TransferLubeDetails.controls;
  }

  removeSection(i, item){
    this.densityMessage ="";
    this.temperatureMessage ="";
    this.polErrorMsg="";
    this.batchErrorMessage="";
    this.compartmentDetailsReq="";
    this.tankAvailableQtyErrorMsg="";
    const control = <FormArray>this.trForm.get('CompartmentDetails');
    this.totalPolVolume=Number(this.totalPolVolume)- Number(item._value.Volume);
    this.compartmentErrorMsg.splice(i, 1);
    control.removeAt(i);
    this.compBatchList.pop();
    this.batchListClone.pop();
    //this.batchList = JSON.parse(JSON.stringify(this.batchListClone[i]));
    //this.batchList = this.batchListClone[i-1];
    if(control.length == this.compartmentLimit ) {
      this.isCompartmentLimitReached = true;
    } else {
      this.isCompartmentLimitReached = false;
    }
   }
   
  onPackChange(event: any, i) {
    if(event=='') {
      event=0;
    }
    this.LubeErrorMsg="";
    this.batchErrorMessage="";
    this.packNo=event;
    let lubeControl = (<FormGroup>(<FormArray>this.trForm.get('TransferLubeDetails')).controls[i]).controls;
    let volume= Number(lubeControl['NumberOfPackets'].value)* Number(lubeControl['PackSize'].value);
    lubeControl['Volume'].setValue(volume);
    this.totalNoOfPacks=Number(this.packNo);
  };

  onTextChange(event: any) {
    this.tankAvailableQtyErrorMsg="";
    this.CompartmentPolVolume=event;
    this.polErrorMsg="";
    this.batchErrorMessage="";

    this.totalPolVolume = 0;
    let control = <FormArray>this.trForm.get('CompartmentDetails');
    let controlvalue = control.value;
    for (let i = 0; i < controlvalue.length; i++) {
      this.totalPolVolume = Number(this.totalPolVolume) + Number(controlvalue[i].Volume)
    }
    //console.log('totalPolVolume', this.totalPolVolume)
  }

  onQtyChange(event: any) {
    this.tankAvailableQtyErrorMsg="";
    this.batchErrorMessage="";
    this.BatchNo="";
    this.polErrorMsg="";
    this.polVolume=event;
    const control = <FormArray>this.trForm.get('CompartmentDetails');
	  if(control.length == this.compartmentLimit ) {
      this.isCompartmentLimitReached = true;
    } else {
      this.isCompartmentLimitReached = false;
    }
    //this.getBatchAndPol(this.transferReqId,this.tankId,0);
    // this.totalPolVolume+=Number(this.polVolume);
    // console.log("totalPolVolume",this.totalPolVolume);
  };

  getBatchAndPol(traId,tankId,id) {
    this.trService.getTransferRequestBatchNo(traId,tankId).then(response=>{
      this.prList=response['purchaseReceipt'];
      this.batchList = response['purchaseReceipt'];
      if(id==0) {
          for(let i=0;i<this.prList.length;i++) {
            if(Number(this.polVolume)<=this.prList[i].quantity) {
                this.BatchNo=this.prList[i].batchNumber;
                this.PRId=this.prList[i].purchaseReceiptId;
            }
          }
          /*if(this.BatchNo=="") {
              this.batchErrorMessage="Volume is Greater than Requested Quantity.So Batch No Can't be displayed!"
          }*/
      } else {
          for(let i=0;i<this.prList.length;i++) {
            this.BatchNo=this.prList[i].batchNumber;
            this.PRId=this.prList[i].purchaseReceiptId;
          }
      }
      /*if(this.BatchNo=="") {
        this.batchErrorMessage="Volume is Greater than Requested Quantity.So Batch No Can't be displayed!"
      }*/
    })
  }

onDateChanged(event:any, type: any) {
  let createdDate = new Date(this.createdDate);
  createdDate.setHours(0, 0, 0);
  let createdDateFormatted = createdDate.getTime();
  let selectedDate = event.date.year+'-'+event.date.month+'-'+event.date.day;
  let selectedDateFormatted = new Date(selectedDate);
  selectedDateFormatted.setHours(0, 0, 0);
  let currentDate = new Date();
  currentDate.setHours(0, 0, 0);
  let currentDateFormatted = currentDate.getTime();
  if(Math.floor(createdDateFormatted/Math.pow(10, 3)) > Math.floor(selectedDateFormatted.getTime()/Math.pow(10, 3))) {
    this.dateError = "Date Loaded should be greater than or equal to Transfer request date";
  } else if(Math.floor(currentDateFormatted/Math.pow(10, 3)) < Math.floor(selectedDateFormatted.getTime()/Math.pow(10, 3))) {
    this.dateError = "Date Loaded should be less than or equal to current date";
  } else {
    this.DateLoaded = event.formatted;
    this.dateError = "";
  }
}
  compartmentErrorMsg = [];
  onSubmit(validPost) {
    this.clearerror();
    let batchArray = [];
    this.batchDetailsList.forEach(outer=>{
        outer.forEach(inner => {
          batchArray.push(inner);
        });
    });
    validPost['BatchDetails'] = batchArray;
    this.typeReq="Transfer type is required";
    this.tankReq = "Tank is required";
    this.transporterReq = "Transporter is Required";
    this.truckReq = "Truck is Required";
    this.driverReq = "Driver is Required";
    if (this.IsLubricant==false) {
      /*if (this.reqQuantity!=this.totalPolVolume) {
        this.polErrorMsg="Total Compartment Volume and Total Requested Volume are not Same!"
      }
      if (this.totalAvailableQty<this.totalPolVolume) {
        this.tankAvailableQtyErrorMsg="Volume Exceeds Available Quantity of this Tank."
      }*/

    } else if(this.reqQuantity!=this.totalNoOfPacks) {
        this.LubeErrorMsg="Total No of Packs and Total Requested Volume are not Same!"
    }
    if(this.IsLubricant) {
      this.trForm.get('Tank').setErrors(null);
    }
    if(!this.IsLubricant) {
      let i = 0;
      this.compartmentErrorMsg = [];
      validPost.CompartmentDetails.forEach(compartment => {
        if(compartment.Volume == "" ) {
          this.compartmentErrorMsg[i] = "Volume can't be empty";
        } else {
          this.compartmentErrorMsg[i] = "";
        }
        i++;
      });
    }
    this.formSubmited = true;
    if(this.trForm.valid && this.ErrorList.length==0 && this.polErrorMsg=="" && this.LubeErrorMsg=="" && this.tankAvailableQtyErrorMsg=="" && this.batchErrorMessage==""
      && this.temperatureMessage=="" && this.densityMessage=="" && this.compartmentDetailsReq=="") {
          validPost.uploadedFile = this.filetype;
          validPost.ReceivedQuantity=this.printedvalue;
          validPost.IsLubricant=this.IsLubricant;
          validPost.IsTransferReceipt=false;
          for(let i = 0; i < validPost['CompartmentDetails'].length; i++) {
            validPost['CompartmentDetails'][i].BatchNo = this.batchDetailsList[i];
          }
          let batchArray = [];
          this.batchDetailsList.forEach(outer=>{
            outer.forEach(inner => {
              batchArray.push(inner);
            });
          });
          validPost['BatchDetails'] = batchArray;

          //this.savebutton = false;
          this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror(){
    this.error="";
  }
  clearmsgs() {
    //this.trForm.reset();
    this.error="";
    this.notificationMeaasge="";
    this.densityMessage ="";
    this.temperatureMessage ="";
    this.batchErrorMessage="";
    this.compartmentDetailsReq="";
    this.polErrorMsg="";
    this.LubeErrorMsg="";
    this.tankAvailableQtyErrorMsg="";
    this.ErrorList=[];
    this.typeReq="";
    this.tankReq="";
    this.dateError ="";
    this.requestedQuantity = 0;
    this.POL = "";
    this.fromSite = "";
    this.toSite = "";
    this.transporterReq="";
    this.truckReq="";
    this.driverReq="";
    this.compartmentErrorMsg = [];
    this.trForm = new FormGroup({
      TId:new FormControl(''),
      TransferNoId:new FormControl('', Validators.required),
      DateLoaded:new FormControl(''),
      Tank: new FormControl('', Validators.required),
      Transporter: new FormControl('', Validators.required),
      LoadingTerminal: new FormControl(''),
      Truck: new FormControl('', Validators.required),
      Driver: new FormControl('', Validators.required),
      TruckOdometerReading: new FormControl(''),
      ConsignmentNo:new FormControl(''),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),
      FromSite: new FormControl(''),
      ToSite: new FormControl(''),
      POLId:new FormControl(''),
      CompartmentDetails: new FormArray([
        this.initSection([]),
      ]),
      TransferLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),

    });
    this.DateLoaded={ date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
    this.trForm.controls['DateLoaded'].setValue(this.DateLoaded);
  }

  getTransferRequests(params){
    this.trService.getTransferList(params).then(r =>  {
        this.siteGroups = r.result;
        this.loading = false;
      })
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      return false;
      //event.preventDefault();
    }

    var number=event.target.value;
    var wholePart = Math.floor(number);
    var wholePartLength = wholePart.toString().length;
    var fractionPart = number % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    if(noOfDigits >= 4) {
        return false;
    } else {
        return true;
    }
  }

  onDensityChange(value){
    this.densityMessage ="";
      if(value == ''){
        value=0;
      }
      let no = Number(value);
      if((no < 0.670)  || (no > 1.06) || (value == '.') || value == ''  ){
        this.densityMessage = "Density should be between 0.670 and 1.06";
      }
      else{
       this.densityMessage ="";
       this.Density = value;
    }
  }

  onTemperatureChange(value){
    this.temperatureMessage="";
    if(value == ''){
      value=0;
    }
    let no = Number(value);
        if(no > 50 || (value == '.') || value == ''){
          this.temperatureMessage = "Temperature should be between 0 and 50";
        }
        else{
         this.temperatureMessage ="";
         this.Temperature = value;
        }
    }

  ngOnChanges(change) {
    this.loading = this.page === 'add' ? false : true;
    if (change.pr && change.pr.currentValue) {
      this.transferPrintData = change.pr.currentValue;
      this.loading = false;
      this.IsLubricant=change.pr.currentValue.transferRequest.islubricant;
      this.reqQuantity=change.pr.currentValue.transferRequest.requestedQuantity;
      this.trForm.controls['TId'].setValue(change.pr.currentValue.id);
      this.trForm.controls['TransferNoId'].setValue(change.pr.currentValue.transferRequestId);
      this.transferId = change.pr.currentValue.transferRequest.transferId;
      this.getTransferRequestDetails(change.pr.currentValue.transferRequestId);
       let date = new Date(change.pr.currentValue.dateLoaded);
       this.loadingDateViewPage = date.getDate() + '-' + Number(date.getMonth()+1) + '-' + date.getFullYear();
       this.trForm.controls['Tank'].setValue(change.pr.currentValue.storageTankId);
       this.trForm.controls['Transporter'].setValue(change.pr.currentValue.transportCompanyId);
       this.trForm.controls['LoadingTerminal'].setValue(change.pr.currentValue.loadingTerminal);
       this.trForm.controls['Truck'].setValue(change.pr.currentValue.truckId);
       this.trForm.controls['Driver'].setValue(change.pr.currentValue.driverId);
       this.trForm.controls['TruckOdometerReading'].setValue(change.pr.currentValue.truckOdoMeterReading);
       this.trForm.controls['ConsignmentNo'].setValue(change.pr.currentValue.consigmentNo);
       this.trForm.controls['Remarks'].setValue(change.pr.currentValue.remarks);
       this.trForm.controls['FileType'].setValue(this.filetype);
       if(this.IsLubricant==false) {
          //this.getBatchAndPol(change.pr.currentValue.transferRequestId,change.pr.currentValue.storageTankId,1);
          this.getTankDetails(change.pr.currentValue.storageTankId);
       }
       if(change.pr.currentValue.transferCompartmentDetails!=null) {
         let compartmentDetails=change.pr.currentValue.transferCompartmentDetails;
         compartmentDetails.forEach(element => {
            //this.totalPolVolume+=element.volume;
        });
         const control =  <FormArray>this.trForm.get(['CompartmentDetails']);
         control.removeAt(0);
         compartmentDetails.forEach(element => {
          control.push(this.initSection(element));
          // this.totalPolVolume+=compartmentDetails.toSiteTankDetails.quantity;
       });
       } else if(change.pr.currentValue.transferLubricantDetails!=null) {
          let lubeValue = change.pr.currentValue.transferLubricantDetails;
          let items = this.trForm.get('TransferLubeDetails') as FormArray;
          if(this.page == 'edit') {
            items.removeAt(0);
          }
          //const control =  <FormArray>this.trForm.get(['TransferLubeDetails']);
          //let valuchanged = []
          lubeValue.forEach(element => {
            //valuchanged.push(this.initLubeSection(element));
            items.push(this.initLubeSection(element));
            this.getPolDetails(element.polId);
       });
          //control.controls = valuchanged;
     }
     this.createdDate = change.pr.currentValue.createdDate;
     let loadedDate = new Date(change.pr.currentValue.dateLoaded);
     this.DateLoaded = { date: {day: loadedDate.getDate(),month: loadedDate.getMonth() +1 , year: loadedDate.getFullYear()} };
     this.trForm.controls['DateLoaded'].setValue(this.DateLoaded);
     if(this.page == 'edit') {
       if(!this.IsLubricant) {
        let transferCompartmentDetails = change.pr.currentValue.transferCompartmentDetails;
        let i = 0;
        transferCompartmentDetails.forEach(transferCompartment => {
          this.compBatchList[i] = [];
          transferCompartment.compartment.forEach(compartment => {
            this.compBatchList[i].push(compartment.batchName);
          });
          i++;
        });
       }
     }
    } else {
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" &&  extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  getPolList(){
        this.trService.getpolList().then(r => {
          this.pollist = r;
      })
          .catch(r => {
              this.handleError(r);
          })
  }
  
  getTransporterList(){
    this.trService.getTransporterList().then(r => {
      this.transporterList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  getRequestList(){
    this.trService.getTransferRequestList().then(r => {
      this.transferList = r;
    }).catch(r => {
      this.handleError(r);
    })
  }

  getSiteList(){
    this.transferReceiptFormService.getSiteListByUser(this.userId).then(r => {
      this.siteList = r;
      })
      .catch(r => {
        this.handleError(r);
      });
  }

  getDriverList(){
    this.trService.getDriverList().then(r => {
      this.driverList = r.result;
    }).catch(r => {
      this.handleError(r);
    })
  }

  getTruckList(){
    this.trService.getTruckList().then(r => {
      this.truckList = r.result;
    }).catch(r => {
      this.handleError(r);
    })
  }

  getTankList(siteId,polId){
    this.trService.getTankList(siteId,polId).then(r => {
      this.tankList = r;
    }).catch(r => {
      this.handleError(r);
    })
  }

  getTransferRequestDetails(id) {
    this.batchErrorMessage="";
    this.tankReq="";
    this.polErrorMsg="";
    this.tankAvailableQtyErrorMsg="";
    this.tankList = [];
    this.clearmsgs();
    this.trForm.controls['TransferNoId'].setValue(id);
    this.trService.getTransferRequestById(id).then(response => {
      this.requestedQuantity = response.requestedQuantity;
      this.fromSite=response.fromSite.name;
      this.fromSiteId=response.fromSite.siteId;
      this.transferReqId=id;
      this.polId=response.polId;
      this.getTankList(this.fromSiteId,this.polId);
      this.reqQuantity=response.requestedQuantity;
      this.toSite=response.requestedSite.name;
      this.IsLubricant=response.islubricant;
      if(this.IsLubricant==true) {
        this.isLubricantSection=true;
        this.getPolDetails(response.polId, 0);
        let control = (<FormGroup>(<FormArray>this.trForm.get('TransferLubeDetails')).controls[0]).controls;
        control['POLId'].setValue(response.polId);
      } else {
        this.isLubricantSection=false;
      }
    })
  }

  getPolDetails(id, i = 0) {
    let control = (<FormGroup>(<FormArray>this.trForm.get('TransferLubeDetails')).controls[i]).controls;
    control['NumberOfPackets'].setValue(this.packNo);
    control['PackSize'].setValue(0);
    control['Package'].setValue('');
    control['Volume'].setValue(0);
    this.trService.getPolDetailsById(id).then(response=>{
      this.package=response.uom.name;
      control['Package'].setValue(response.uom.name);
      this.packsize=response.packSize;
      control['PackSize'].setValue(response.packSize);
    })
  }

  getTankDetails(id) {
      this.tankId=id;
      this.batchErrorMessage = "";
      this.tankAvailableQtyErrorMsg = "";
      this.compBatchList = [];
      this.batchDetailsList = [];
      this.trService.getTankDetailsById(id).then(response=> {
        this.POL=response.pol.name;
        this.getBatchAndPol(this.transferReqId, this.tankId, 0);
        this.trService.getTankAvailableQuantity(id).then(response=> {
          this.totalAvailableQty=response.availableQuantity;
          this.batchDetailsList = [];
          this.notificationMeaasge="Available Quantity : " +this.totalAvailableQty;
        });
      });
      const control = <FormArray>this.trForm.get('CompartmentDetails');
      for(let i = 1; i < control.controls.length; i++) {
        control.removeAt(i);
      }
      //(<FormGroup>control.controls[0]).controls['POL'].setValue('');
      (<FormGroup>control.controls[0]).controls['BatchNo'].setValue('');
      (<FormGroup>control.controls[0]).controls['Volume'].setValue(0);
      (<FormGroup>control.controls[0]).controls['ObservedDensity'].setValue(0);
      (<FormGroup>control.controls[0]).controls['ObservedTemprature'].setValue(0);
      (<FormGroup>control.controls[0]).controls['SealNo'].setValue(0);
      (<FormGroup>control.controls[0]).controls['Conductivity'].setValue(0);
      (<FormGroup>control.controls[0]).controls['Visual'].setValue('');
      (<FormGroup>control.controls[0]).controls['PurchaseReceiptId'].setValue('');
  }

  setBatchDetails(volume, index) {
    this.compartmentDetailsReq="";
    let control = (<FormArray>this.trForm.get('CompartmentDetails'));
    if(volume == '' || volume == null || volume == 0) {
      this.compBatchList[index] = [];
      return;
    }
    let controlLength = control.length;
    if(typeof(this.prList) != 'undefined') {
      this.batchListClone[0] = JSON.parse(JSON.stringify(this.prList));
      this.batchListClone[index+1] = JSON.parse(JSON.stringify(this.batchListClone[index]));
      let compBatch = [];
      let requiredVolOnNextBatch = 0;
      let quantityLeftOnBatch = 0;
      this.compBatchList[index] = [];
      this.batchDetailsList[index] = [];
      this.compartmentVolume[index] = volume;
      for( let batch of this.batchListClone[index+1]) {
        if(index + 1 === controlLength) {
          if(volume == batch.quantity && volume != 0) {
              this.compBatchList[index].push(batch.batchNumber);
              let batchDetails = {
                'batch' : batch.purchaseReceiptId,
                'volume' : Number(volume)
              }
              this.batchDetailsList[index].push(batchDetails);
              batch.quantity = 0;
              break;
          } else if(volume > batch.quantity && batch.quantity != 0) {
              this.compBatchList[index].push(batch.batchNumber);
              requiredVolOnNextBatch = volume - batch.quantity;
              volume = requiredVolOnNextBatch;
              let batchDetails = {
                'batch' : batch.purchaseReceiptId,
                'volume' : Number(batch.quantity)
              }
              this.batchDetailsList[index].push(batchDetails);
              batch.quantity = 0;
              //break;
          } else if(volume < batch.quantity) {
              this.compBatchList[index].push(batch.batchNumber);
              quantityLeftOnBatch = batch.quantity - volume;
              batch.quantity = quantityLeftOnBatch;
              let batchDetails = {
                'batch' : batch.purchaseReceiptId,
                'volume' : Number(volume)
              }
              this.batchDetailsList[index].push(batchDetails);
              break;
          }
        }
      }
      if( index + 1 < this.getCompartmentDetails(this.trForm).length) {
        let control = this.getCompartmentDetails(this.trForm);
        for(let j = index+1; j < this.getCompartmentDetails(this.trForm).length; j++) {
          control.pop();
        }
        this.setBatchDetails(volume, index);
      }
    }
  }

  compartmentDetails = [];
  transferLubeDetails = [];
  transferPrintData : any = {
    transferRequest : {
      fromSite : {},
      transferCompartmentDetails : {}
    }
  };
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html lang="en">
      <head>
      <script src='https://printjs-4de6.kxcdn.com/print.min.js'></script>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
      <style></style>
      </head>
      <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
    popupWin.document.close();
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}