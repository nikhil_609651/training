import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TransferRequestFormService } from 'app/admin/receiptandtransfer/transferrequest/service/transferrequest.service';

@Component({
  selector: 'add',
  template: require('./transferrequest-add.html')
})
export class TransferRequestAddComponent implements OnInit{
  loading: boolean;
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';
  showLoader : boolean = false;

  constructor(private router: Router,
    private prService: TransferRequestFormService) {
  }
  ngOnInit() {
    this.page = 'add';

  }
  onSave(tr: any) {
    this.showLoader = true;
    this.loading = true;
    let files = tr.uploadedFile;
    let formData = new FormData();
    let date = tr.Date;
    if(typeof date == 'object') {
      if(date.date.month < 10 && date.date.day < 10) {
        date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      }else if(date.date.day < 10 && date.date.month >= 10) {
        date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
      }else if(date.date.day >= 10 && date.date.month < 10) {
        date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
      } else {
        date = date.date.day + '/' + date.date.month + '/' + date.date.year;
      }
    } else {
      date= tr.Date;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', '0');
    formData.append('Islubricant', tr.IsLubricant);
    formData.append('Date', date);
    formData.append('POLId', tr.POLId);
    formData.append('TransferTypeId', tr.TransferTypeId);
    formData.append('FromSiteId', tr.FromSiteId);
    formData.append('RequestedSiteId', tr.RequestSiteId);
    formData.append('RequestedQuantity', tr.RequestedQuantity);
    formData.append('Remarks', tr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    let toTank = '0';
    let fromTank = '0';
    tr.TransferTypeId == '3' ? toTank = tr.toTank : toTank = '0';
    tr.TransferTypeId == '3' ? fromTank = tr.fromTank : fromTank = '0';
    formData.append('FromTankId', fromTank);
    formData.append('ToTankId', toTank);
    this.prService.Save(formData).then(r =>  {
      this.showLoader = false;
      this.success = 'Transfer Request Created Successfully!';
      this.loading = false;
      jQuery('html, body').animate({scrollTop:0}, {duration: 500});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/transfer-request']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
