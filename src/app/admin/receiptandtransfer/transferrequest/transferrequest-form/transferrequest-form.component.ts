import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import { IMyOptions } from 'mydatepicker';
import { TransferRequestFormService } from 'app/admin/receiptandtransfer/transferrequest/service/transferrequest.service';
import { TransferReceiptFormService } from '../../transferreceipt/service/transferreceipt.service';

@Component({
  selector: 'transferrequest-form',
  templateUrl: './transferrequest-form.html',

})
export class TransferRequestFormComponent implements OnInit,  OnChanges {
  fromsiteList: any;
  userId: string;
  notificationMessage: string;
  nullalertmessage: string='';
  fromsiteReq: string;
  polReq: string;
  siteReq: string;
  savebutton: boolean = true;

  availableQty: any;
  polId=0;
  trId=0;
  qtyalertmessage: string='';
  reqsiteError=[];
  siteError=[];
  fromSiteId=0;
  reqSiteId: any;
  isEdit=false;
  transferId="";
  typeReq: string;
  typeList: any;
 // LoadingDate: Date;
  Quantity: any;
  printedvalue=0;
  secondaryvalue=0;
  lastvalue=0;
  totalReceivedQuantity=0;
  receivedQuantity=0;
  finalvalue=0;
  reqQuantity=0;
  companyName: any;
  mobNo="";
  packsize="";
  package="";
  isLubricantSection=false;
  IsLubricant:boolean=false;
  tankList: any = [];
  loadingDate: string;
  siteList: any;
  truckList: any;
  driverList: any;
  pollist: any;
  @Input() pr;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  trForm: FormGroup;

  filelength: number;
  sizeError: string;
  ErrorList=[];
  CapacityErrorList=[];
  StorageTankList=[];
  filetype= [];
  filelist= [];

  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';

  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public pr_id = '';
  public PId:any;
  public text = '';
  public drop = '';
  items: any;
  public addRes: any[] = [];
  dateError : any = "";

  public siteGroups =[];private currentdate= new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    //disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()}

  };
  private Date: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };

  constructor( private trService: TransferRequestFormService, private router: Router,
    private route: ActivatedRoute,private transferReceiptFormService: TransferReceiptFormService) {
      this.userId=localStorage.getItem('user_nameId');
      this.getPolList();
      this.getSiteList();
      this.getRequestTypeList();
  }

  ngOnInit() {
    this.trForm = new FormGroup({
      TRId:new FormControl(''),
      Date:new FormControl(''),
      FromSiteId: new FormControl('', Validators.required),
      RequestSiteId: new FormControl('',Validators.required),
      TransferTypeId: new FormControl('',Validators.required),
      RequestedQuantity: new FormControl('0'),
      //NoOfPacks: new FormControl(''),
      POLId: new FormControl('',Validators.required),
      Remarks:new FormControl(''),
      IsLubricant:new FormControl(''),
      FileType:new FormControl(''),
      fromTank:new FormControl(''),
      toTank:new FormControl(''),
    });

    this.route.params.forEach((params: Params) => {
      this.pr_id = params['TRId'];
    });

    if(this.page == 'edit') {
      this.trForm.disable();
    }
  }

  onDateChanged(event:any) {
    let selectedDate = event.date.year+'-'+event.date.month+'-'+event.date.day;
    let selectedDateFormatted = new Date(selectedDate);
    selectedDateFormatted.setHours(0, 0, 0);
    let currentDate = new Date();
    currentDate.setHours(0, 0, 0);
    let currentDateFormatted = currentDate.getTime();
    if(Math.floor(currentDateFormatted/Math.pow(10, 3)) < Math.floor(selectedDateFormatted.getTime()/Math.pow(10, 3))){
      this.dateError = "Date should be less than or equal to current date";
    } else {
      this.Date = event.formatted;
      this.dateError = "";
    }
  }

  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
    var number=event.target.value;
    var wholePart = Math.floor(number);
    var wholePartLength = wholePart.toString().length;
    var fractionPart = number % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    if(noOfDigits >= 4){
        return false;
    } else {
        return true;
    }
  }

  fromTankRequired = '';
  toTankRequired = '';
  onSubmit(validPost) {
    this.clearerror();
    this.typeReq="Transfer Type is required";
    this.siteReq="Requested Site is required";
    this.polReq="POL is required";
    this.fromsiteReq="Transfer Site is required";
    this.fromTankRequired = 'From Tank is Required';
    this.toTankRequired = 'To Tank is Required';
    this.formSubmited = true;
    if(this.reqQuantity==0) {
      if(this.isLubricantSection==true) {
        this.nullalertmessage="Number of Packs Can't be Zero!";
      } else {
        this.nullalertmessage="Requested Quantity Can't be Zero!";
      }
    }
    if(this.isTanktoTank) {
      validPost.fromTank ? this.fromTankRequired = '' : this.fromTankRequired = 'From Tank is Required';
      validPost.toTank ? this.toTankRequired = '' : this.toTankRequired = 'To Tank is Required';
    } else {
      this.fromTankRequired = '';
      this.toTankRequired = '';
    }
    if(this.trForm.valid && this.ErrorList.length==0 && this.CapacityErrorList.length==0 && this.reqsiteError.length==0 && this.siteError.length==0 
       && this.qtyalertmessage=='' && this.reqQuantity>0 && this.dateError == '' && this.toTankErrorMsg == '' && this.quantityUnavailableMessage == ''
       && this.fromTankRequired == '' && this.toTankRequired == '') {
      validPost.uploadedFile = this.filetype;
      validPost.ReceivedQuantity=this.printedvalue;
      validPost.IsLubricant = this.IsLubricant;
      this.saved.emit(validPost);
    } else{
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  selectAll(event) {
    this.trForm = new FormGroup({
      TRId:new FormControl(''),
      Date:new FormControl(''),
      FromSiteId: new FormControl('', Validators.required),
      RequestSiteId: new FormControl('',Validators.required),
      TransferTypeId: new FormControl('',Validators.required),
      RequestedQuantity: new FormControl('0'),
      POLId: new FormControl('',Validators.required),
      IsLubricant:new FormControl(event.target.checked),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),
    });
    let Date: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.trForm.controls['Date'].setValue(Date);
    this.notificationMessage="";
    this.reqQuantity=0;
    this.typeReq="";
    this.siteReq="";
    this.polReq="";
    this.fromsiteReq="";
    this.error="";
    this.notificationMessage="";
    this.qtyalertmessage='';
    this.nullalertmessage='';
    this.ErrorList=[];
    this.reqsiteError=[];
    this.siteError=[];
    if(event.target.checked)
    {
      this.IsLubricant=true;
      this.isLubricantSection=true;
      this.getPolList();
    }
    else
    {
      //this.prForm.controls['Batch'].setValue("A");
      //this.prForm.controls['POL'].setValue(2007);
      this.isLubricantSection=false;
      this.IsLubricant=false;
      this.getPolList();
    }
  }

  clearerror() {
    this.error="";
  }

  clearmsgs() {
    this.isLubricantSection = false;
    this.reqsiteError = [];
    this.siteError = [];
    this.reqQuantity = 0;
    this.typeReq="";
    this.siteReq="";
    this.polReq="";
    this.fromsiteReq="";
    this.error = "";
    this.notificationMessage = "";
    this.tankAvailabiltyMsg = "";
    this.trForm.reset();
    this.qtyalertmessage = '';
    this.nullalertmessage = '';
    this.ErrorList = [];
    this.reqsiteError = [];
    this.siteError = [];
    this.toTankErrorMsg = '';
    this.quantityUnavailableMessage = '';
    this.fromTankRequired = '';
    this.toTankRequired = '';
    this.isTanktoTank = false;
    this.dateError = "";
    this.trForm = new FormGroup({
      TRId:new FormControl(''),
      Date:new FormControl(''),
      FromSiteId: new FormControl('', Validators.required),
      RequestSiteId: new FormControl('',Validators.required),
      TransferTypeId: new FormControl('',Validators.required),
      RequestedQuantity: new FormControl('0'),
      POLId: new FormControl('',Validators.required),
      IsLubricant:new FormControl(false),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),
      fromTank:new FormControl(''),
      toTank:new FormControl(''),
    });
    let Date: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.trForm.controls['Date'].setValue(Date);
  }

  getTransferRequests(params) {
    this.trService.getTransferRequestList(params).then(r =>  {
        this.siteGroups = r.result;
        this.loading = false;
      })
  }

  dateViewPage: any;
  ngOnChanges(change) {
    this.loading = this.page === 'add' ? false : true;
    let userId = localStorage.getItem('user_nameId');
    if(change.pr && change.pr.currentValue) {
      this.transferRequestPrintData = change.pr.currentValue;
      this.loading = false;
      this.isEdit=true;
      this.polId=change.pr.currentValue.polId;
      this.fromSiteId=change.pr.currentValue.fromSiteId;
      this.trId=change.pr.currentValue.id;
      this.transferId=change.pr.currentValue.transferId;
      this.reqQuantity=change.pr.currentValue.requestedQuantity;
      this.trForm.controls['TRId'].setValue(change.pr.currentValue.id);
      this.trForm.controls['TransferTypeId'].setValue(change.pr.currentValue.transferTypeId);
      //this.trForm.controls['Date'].setValue(change.pr.currentValue.Date);
      let date = new Date(change.pr.currentValue.date);
      this.dateViewPage = date.getDate() + '-' + Number(date.getMonth()+1) + '-' + date.getFullYear();
      this.transferReceiptFormService.getSiteListByUser(userId).then(r => {
        this.siteList = r;
        this.trForm.controls['RequestSiteId'].setValue(change.pr.currentValue.requestedSiteId);
        this.trService.getSiteById(change.pr.currentValue.requestedSiteId).then(result => {
          this.trService.getSiteList(result.buId, userId).then(result => {
            this.fromsiteList = result;
            this.trForm.controls['FromSiteId'].setValue(change.pr.currentValue.fromSiteId);
          })
        })
      })
       this.trForm.controls['RequestedQuantity'].setValue(change.pr.currentValue.requestedQuantity);
       this.trForm.controls['POLId'].setValue(change.pr.currentValue.polId);
       this.trForm.controls['Remarks'].setValue(change.pr.currentValue.remarks);
       this.trForm.controls['IsLubricant'].setValue(change.pr.currentValue.islubricant);
       if(change.pr.currentValue.islubricant==true) {
          this.IsLubricant=change.pr.currentValue.islubricant;
          this.isLubricantSection=true;
          this.getPolList();
       } else {
          this.IsLubricant=change.pr.currentValue.islubricant;
          this.isLubricantSection=false;
          this.getPolList();
       }
       this.trForm.controls['FileType'].setValue(this.filetype);
       if(change.pr.currentValue.transferTypeId == 3) {
        this.isTanktoTank = true;
        this.trService.getTankListFromSiteAndPol(change.pr.currentValue.fromSiteId, change.pr.currentValue.polId).then( response => {
          this.tankList = response;
          this.trForm.controls['fromTank'].setValue(change.pr.currentValue.fromTankId);
          this.trForm.controls['toTank'].setValue(change.pr.currentValue.toTankId);
        })
      }
    } else {
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152) {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif" &&  extnsion!="txt") {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      } else {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  getPolList() {
    this.trService.getpolList(this.IsLubricant).then(r => {
      this.pollist = r;
    })
    .catch(r => {
      this.handleError(r);
    })
  }

  getRequestTypeList() {
    this.trService.getTransferTypeList().then(r => {
      this.typeList = r.model;
    })
    .catch(r => {
      this.handleError(r);
    })
  }

  getSiteList() {
    this.transferReceiptFormService.getSiteListByUser(this.userId).then(r => {
        this.siteList = r;
    }).catch(r => {
        this.handleError(r);
    });
  }

  getFromSiteList(buId) {
    let userId = localStorage.getItem('user_nameId');
    this.trService.getSiteList(buId, userId).then(result => {
      this.fromsiteList = result;
    })
    .catch(r => {
      this.handleError(r);
    });
  }

  getRequestedSite(id) {
      this.qtyalertmessage='';
      this.reqsiteError=[];
      this.siteError=[];
      this.nullalertmessage="";
      this.toTankErrorMsg = '';
      this.reqSiteId=id;
      if(this.isTanktoTank) {
        this.trForm.controls['FromSiteId'].setValue(id);
        this.tankList = [];
        this.tankAvailabiltyMsg = '';
        this.quantityUnavailableMessage = '';
        this.availableQuantity = 0;
        this.trForm.controls['toTank'].setValue('');
        this.trForm.controls['fromTank'].setValue('');
        this.trForm.controls['RequestedQuantity'].setValue('');
        this.trForm.controls['POLId'].setValue('');
      } else {
        if(this.reqSiteId==this.fromSiteId) {
          this.reqsiteError.push("Same site selected in 'Transfer requested by' and 'Transfer from'");
        } else {
          this.trService.getSiteById(id).then(result => {
            this.getFromSiteList(result.buId);
          }).catch(r => {
            this.handleError(r);
          });
        }
      }
  }

  getFromSite(id) {
      this.qtyalertmessage='';
      this.nullalertmessage="";
      this.siteError=[];
      this.reqsiteError=[];
      this.fromSiteId=id;
      //this.trForm.controls['RequestedQuantity'].setValue(0);
      //this.reqQuantity=0;
      this.checkQuantity(this.fromSiteId,this.polId,this.trId);
      if(this.reqSiteId==this.fromSiteId) {
        this.siteError.push("Same site selected in 'Transfer requested by' and  'Transfer from'");
      }
  }

  getPolId(id,section) {
    this.qtyalertmessage='';
    this.nullalertmessage="";
    this.polId=id;
    if(this.polId > 0) {
      this.trForm.controls['FromSiteId'].enable();
    }
    this.checkQuantity(this.fromSiteId,this.polId,this.trId);
    if(this.isTanktoTank) {
      this.tankList = [];
      this.tankAvailabiltyMsg = '';
      this.quantityUnavailableMessage = '';
      this.trForm.controls['toTank'].setValue('');
      this.trForm.controls['fromTank'].setValue('');
      this.trForm.controls['RequestedQuantity'].setValue('');
      this.trService.getTankListFromSiteAndPol(this.reqSiteId, this.polId).then( response => {
        this.tankList = response;
      }).catch(r => {
        this.handleError(r);
      });
    }
  }

  onTextChange(event: any) {
    this.nullalertmessage="";
    this.qtyalertmessage="";
    this.reqQuantity=event;
    if(this.reqQuantity==0) {
      if(this.isLubricantSection==true) {
        this.nullalertmessage="Number of Packs Can't be Zero!";
      } else {
        this.nullalertmessage="Requested Quantity Can't be Zero!";
      }
    } else {
      this.checkQuantity(this.fromSiteId,this.polId,this.trId);
    }
  };

  checkQuantity(siteId, polId, trId) {
    if(!this.isTanktoTank) {
      this.qtyalertmessage='';
      this.nullalertmessage="";
          this.trService.getPolQtyFromSite(siteId,polId,trId).then(r => {
            this.availableQty = r['availableQuantity'];
            this.notificationMessage="Available Quantity : " + this.availableQty;
            if(Number(this.reqQuantity) > Number(this.availableQty)){
              this.qtyalertmessage="The Requested Quantity is Greater than Available Quantity of this Site!";
            } else {
              this.qtyalertmessage='';
            }
        });
        //this.availableQty=0;
      if(siteId != 0 && polId !=0 ) {
        if(Number(this.reqQuantity) > Number(this.availableQty)){
          this.qtyalertmessage="The Requested Quantity is Greater than Available Quantity of this Site!";
        } else {
          this.qtyalertmessage='';
        }
      }
    }
  }

  numberOnly(event): boolean {
    var charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    // Allow non-printable keys
    if (!charCode || charCode == 8 /* Backspace */ ) {
        return;
    }
    var typedChar = String.fromCharCode(charCode);
    // Allow numeric characters
    if (/\d/.test(typedChar)) {
        return;
    }
    // Allow the minus sign (-) if the user enters it first
    if (typedChar == ".") {
        return;
    }
    // In all other cases, suppress the event
    return false;
  }

  transferRequestPrintData : any = {
    transferType: {},
    requestedSite: { },
    pol: { },
    fromSite: { }
  };

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html lang="en">
      <head>
      <script src='https://printjs-4de6.kxcdn.com/print.min.js'></script>
      <style></style>
      </head>
      <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
    popupWin.document.close();
  }

  isTanktoTank : boolean =  false;
  onTransferTypeChange(type) {
    type === '3' ? this.isTanktoTank = true : this.isTanktoTank = false;
    this.reqsiteError=[];
    this.siteError=[];
    this.reqQuantity=0;
    this.tankAvailabiltyMsg = '';
    this.toTankErrorMsg = '';
    this.quantityUnavailableMessage = '';
    if(this.trForm.controls['TransferTypeId'].hasError('required') || (this.trForm.controls['TransferTypeId'].touched)) {
      this.typeReq="";
    }
    this.siteReq="";
    this.polReq="";
    this.fromsiteReq="";
    this.error="";
    this.notificationMessage="";
    this.qtyalertmessage='';
    this.nullalertmessage='';
    this.ErrorList=[];
    this.reqsiteError=[];
    this.siteError=[];
    this.fromTankRequired = '';
    this.toTankRequired = '';
    this.dateError = "";
    this.trForm = new FormGroup({
      TRId:new FormControl(''),
      Date:new FormControl(''),
      FromSiteId: new FormControl('', Validators.required),
      RequestSiteId: new FormControl('',Validators.required),
      TransferTypeId: new FormControl('',Validators.required),
      RequestedQuantity: new FormControl('0'),
      POLId: new FormControl('',Validators.required),
      IsLubricant:new FormControl(false),
      Remarks:new FormControl(''),
      FileType:new FormControl(''),
      fromTank:new FormControl(''),
      toTank:new FormControl(''),
    });
    this.trForm.controls['TransferTypeId'].setValue(type);
    if(type == '3') {
      this.trForm.controls['IsLubricant'].setValue(false);
      this.IsLubricant = false;
      this.isLubricantSection = false;
      this.trService.getpolList(this.IsLubricant).then(r => {
        this.pollist = r;
      })
      .catch(r => {
        this.handleError(r);
      })
    } else {
      this.trForm.controls['IsLubricant'].setValue(this.IsLubricant);
    }
    let Date: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.trForm.controls['Date'].setValue(Date);
  }

  fromTankId : any = '';
  availableQuantity : any = '';
  tankAvailabiltyMsg : any = '';
  getFromTank(fromTank) {
    this.fromTankId = fromTank;
    this.nullalertmessage = '';
    this.trForm.controls['RequestedQuantity'].setValue('');
    this.tankAvailabiltyMsg = '';
    this.quantityUnavailableMessage = '';
    this.fromTankRequired = '';
    this.trService.getTankCapacity(fromTank).then(response => {
      this.availableQuantity = response.availableQuantity;
      this.tankAvailabiltyMsg = "Available Quantity : " + this.availableQuantity;
    });
  }

  toTankId : any = '';
  toTankErrorMsg : string = '';
  getToTank(toTank) {
    this.toTankRequired = '';
    if(this.fromTankId == toTank) {
      this.toTankErrorMsg = "To Tank and From Tank can't be same";
    } else {
      this.toTankErrorMsg = '';
      this.toTankId = toTank;
    }
  }

  quantityUnavailableMessage = '';
  quantityAvailabilityCheck(value) {
    if(this.isTanktoTank) {
      if(value > this.availableQuantity && value != '' && value != 0) {
        this.quantityUnavailableMessage = 'Entered Quantity is greater than available quantity in the tank';
      } else {
        this.quantityUnavailableMessage = '';
      }
    }
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}