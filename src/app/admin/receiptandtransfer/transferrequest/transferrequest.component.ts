import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'transfer-request',
    template: `<router-outlet></router-outlet>`
})
export class TransferRequestComponent {
	constructor(private router: Router) {
	}
}
