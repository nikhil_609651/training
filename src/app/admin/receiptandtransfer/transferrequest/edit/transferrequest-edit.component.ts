import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransferRequestFormService } from 'app/admin/receiptandtransfer/transferrequest/service/transferrequest.service';

@Component({
  selector: 'transferrequest-edit',
  template: require('./transferrequest-edit.html')
})
export class TransferRequestEditComponent implements OnInit{
  id: any;

  public error = {};
  public success = '';
  public pr: any;
  public page = 'edit';
  showLoader: boolean = false;

	constructor(private router: Router, private route: ActivatedRoute,
    private trService: TransferRequestFormService) {
  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
        this.pr = this.trService.getTransferRequest(this.id);     
      });
	}

  onSave(tr: any) {
    this.showLoader = true;
    let files = tr.uploadedFile;
    let formData = new FormData();

    let date = tr.Date;
    if(typeof date == 'object'){
      date=date.date.day + '/' + date.date.month + '/' + date.date.year;        
    }
     else {
      date= tr.Date;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', tr.TRId);
    formData.append('IsLubricant', tr.IsLubricant);
    formData.append('Date', date);
    formData.append('POLId', tr.POLId);
    formData.append('TransferTypeId', tr.TransferTypeId);
    formData.append('FromSiteId', tr.FromSiteId);
    formData.append('RequestedSiteId', tr.RequestSiteId);
    formData.append('RequestedQuantity', tr.RequestedQuantity);
    formData.append('Remarks', tr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.trService.Save(formData).then(r =>  {
      this.showLoader = false;
      this.success = 'Transfer Request Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/transfer-request']);
      }.bind(this), 3000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}
