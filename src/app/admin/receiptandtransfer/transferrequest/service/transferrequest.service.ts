import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class TransferRequestFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public transferrequesturl = '';
    public truckUrl = '';
    public requesttypeUrl = '';
    public polUrl = '';
    public siteUrl = '';
    public FromSiteFilterUrl = '';
    public polfilterUrl='';
    public attachmenturl = '';
    public checkQtyUrl = '';
    public deleteattachmentUrl = '';
    public statusUrl = '';
    public approverUrl ='';
    public getSiteByIDUrl = '';
    public getSiteByBuIDUrl = '';
    public tankListFromSitePolUrl = '';
    public getSTUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.listUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/TransferRequest/';
        this.saveUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/SaveTransferRequest';
        this.deleteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeleteTransferRequestByID?id=';
        this.transferrequesturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestByID?id=';
        this.requesttypeUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/TransferType/';
        this.polfilterUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestPOLFilter/';
        this.polUrl = _configuration.ServerWithApiUrl+'Masters/GetLubricantPOL?Lubricant=';
        this.checkQtyUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPolAvailableQuantityBySite?SiteID=';
        this.siteUrl = _configuration.ServerWithApiUrl+'Masters/GetAllSites';
        this.FromSiteFilterUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestFromSiteFilter/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetAllTransferRequestAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeleteTransferRequestAttachments?Id=';
        this.statusUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestApprovedStatusFilter';
        this.approverUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestApproverFilter';
        this.getSiteByIDUrl = _configuration.ServerWithApiUrl+'Masters/GetSiteByID?id=';
        this.getSiteByBuIDUrl = _configuration.ServerWithApiUrl+'Masters/SiteByBusinessUnitId?BusinessFilter=';
        this.tankListFromSitePolUrl = _configuration.ServerWithApiUrl+'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.getSTUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/GetAvailableQuantityByTankId?TankId=';
    }

    // List API for Purchase Receipt
    getTransferRequestList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getpolList(IsLubricant:boolean): Promise<any> {
        return this.authHttp.get(this.polUrl + IsLubricant)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getpolfilterList(): Promise<any> {
        return this.authHttp.get(this.polfilterUrl )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTransferTypeList(): Promise<any> {
        return this.authHttp.get(this.requesttypeUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteList(buId, userId): Promise<any> {
        return this.authHttp.get(this.getSiteByBuIDUrl+buId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getFromSiteFilterList(userId): Promise<any> {
        return this.authHttp.get(this.FromSiteFilterUrl+ '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getApprovedStatusList(): Promise<any> {
        return this.authHttp.get(this.statusUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getApproverList(): Promise<any> {
        return this.authHttp.get(this.approverUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //   Save API for Purchase Receipt
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deleteTransferRequest(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getTransferRequest(id: string) {
        return this.authHttp.get(this.transferrequesturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getallTransferRequestAttachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolQtyFromSite(siteId,polId,trId):Promise<any> {

        return this.authHttp.get(this.checkQtyUrl + siteId + '&POLID=' + polId + '&TransferRequestId=' + trId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getSiteById(id):Promise<any> {
        return this.authHttp.get(this.getSiteByIDUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteByBUId(id):Promise<any> {
        return this.authHttp.get(this.getSiteByBuIDUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankListFromSiteAndPol(site, pol):Promise<any> {
        return this.authHttp.get(this.tankListFromSitePolUrl + site + '&POLId=' + pol)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankCapacity(id: string) {
        return this.authHttp.get(this.getSTUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
