import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { PaginationModule, TabsModule, TooltipModule } from 'ng2-bootstrap';
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';

import { Ng2SimplePageScrollModule } from 'ng2-simple-page-scroll';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LubricantAvailabilityComponent } from './lubricantavailability/lubricantavailability.component';
import { LubricantAvailabilityListComponent } from './lubricantavailability/list/lubricantavailability-list.component';
import { LubricantAvailabilityFormService } from './lubricantavailability/service/lubricantavailability.service';
import { TransactionComponent } from './transaction.component';
import { routing } from './transaction.routing';
import { TankPolAvailabilityListComponent } from './tankpolavailability/list/tankpolavailability-list.component';
import { TankPolAvailabilityComponent } from './tankpolavailability/tankpolavailability.component';
import { TankPolAvailabilityFormService } from './tankpolavailability/service/tankpolavailability.service';
import { PurchaseReceiptComponent } from './purchase-receipt/purchasereceipt.component';
import { PurchaseReceiptListComponent } from './purchase-receipt/list/purchasereceipt-list.component';
import { PurchaseReceiptFormComponent } from './purchase-receipt/purchasereceipt-form/purchasereceipt-form.component';
import { PurchaseReceiptAddComponent } from './purchase-receipt/add/purchasereceipt-add.component';
import { PurchaseReceiptEditComponent } from './purchase-receipt/edit/purchasereceipt-edit.component';
import { TransferRequestComponent } from './transferrequest/transferrequest.component';
import { TransferRequestListComponent } from './transferrequest/list/transferrequest-list.component';
import { TransferRequestAddComponent } from './transferrequest/add/transferrequest-add.component';
import { TransferRequestFormComponent } from './transferrequest/transferrequest-form/transferrequest-form.component';
import { TransferRequestEditComponent } from './transferrequest/edit/transferrequest-edit.component';
import { TransferComponent } from './transfer/transfer.component';
import { TransferListComponent } from './transfer/list/transfer-list.component';
import { TransferAddComponent } from './transfer/add/transfer-add.component';
import { TransferFormComponent } from './transfer/transfer-form/transfer-form.component';
import { TransferEditComponent } from './transfer/edit/transfer-edit.component';
import { TransferReceiptComponent } from './transferreceipt/transferreceipt.component';
import { TransferReceiptListComponent } from './transferreceipt/list/transferreceipt-list.component';
import { TransferReceiptFormComponent } from './transferreceipt/transferreceipt-form/transferreceipt-form.component';
import { TransferReceiptAddComponent } from './transferreceipt/add/transferreceipt-add.component';
import { TransferReceiptEditComponent } from './transferreceipt/edit/transferreceipt-edit.component';
import { PurchaseReceiptFormService } from './purchase-receipt/service/purchasereceipt-form.service';
import { TransferRequestFormService } from './transferrequest/service/transferrequest.service';
import { TransferFormService } from './transfer/service/transfer.service';
import { TransferReceiptFormService } from './transferreceipt/service/transferreceipt.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    PaginationModule,
    TabsModule,
    TooltipModule,
    DatePickerModule,
    MyDatePickerModule,
    routing,
    Ng2SimplePageScrollModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    TransactionComponent, 
    LubricantAvailabilityComponent, 
    LubricantAvailabilityListComponent,
    TankPolAvailabilityComponent, 
    TankPolAvailabilityListComponent,
    PurchaseReceiptComponent, 
    PurchaseReceiptListComponent,
    PurchaseReceiptFormComponent, 
    PurchaseReceiptAddComponent, 
    PurchaseReceiptEditComponent,
    TransferRequestComponent, 
    TransferRequestListComponent, 
    TransferRequestAddComponent,
    TransferRequestFormComponent, 
    TransferRequestEditComponent,
    TransferComponent, 
    TransferListComponent, 
    TransferAddComponent,
    TransferFormComponent, 
    TransferEditComponent,
    TransferReceiptComponent, 
    TransferReceiptListComponent,
    TransferReceiptFormComponent, 
    TransferReceiptAddComponent, 
    TransferReceiptEditComponent,
  ],
  entryComponents:    [ ],
  providers: [
    LubricantAvailabilityFormService, 
    TankPolAvailabilityFormService, 
    PurchaseReceiptFormService,
    TransferRequestFormService,
    TransferFormService,
    TransferReceiptFormService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export default class TransactionModule {}
