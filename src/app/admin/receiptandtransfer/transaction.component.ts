import { Component } from "@angular/core";

@Component({
    selector: 'transfer',
    template: `<router-outlet></router-outlet>`
})

export class TransactionComponent {
    
}