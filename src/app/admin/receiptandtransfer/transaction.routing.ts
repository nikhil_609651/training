import { Routes, RouterModule }  from '@angular/router';
import { TransactionComponent } from './transaction.component';
import { LubricantAvailabilityListComponent } from './lubricantavailability/list/lubricantavailability-list.component';
import { TankPolAvailabilityListComponent } from './tankpolavailability/list/tankpolavailability-list.component';
import { PurchaseReceiptComponent } from './purchase-receipt/purchasereceipt.component';
import { PurchaseReceiptListComponent } from './purchase-receipt/list/purchasereceipt-list.component';
import { PurchaseReceiptAddComponent } from './purchase-receipt/add/purchasereceipt-add.component';
import { PurchaseReceiptEditComponent } from './purchase-receipt/edit/purchasereceipt-edit.component';
import { TransferRequestComponent } from './transferrequest/transferrequest.component';
import { TransferRequestListComponent } from './transferrequest/list/transferrequest-list.component';
import { TransferRequestAddComponent } from './transferrequest/add/transferrequest-add.component';
import { TransferRequestEditComponent } from './transferrequest/edit/transferrequest-edit.component';
import { TransferComponent } from './transfer/transfer.component';
import { TransferListComponent } from './transfer/list/transfer-list.component';
import { TransferAddComponent } from './transfer/add/transfer-add.component';
import { TransferEditComponent } from './transfer/edit/transfer-edit.component';
import { TransferReceiptComponent } from './transferreceipt/transferreceipt.component';
import { TransferReceiptListComponent } from './transferreceipt/list/transferreceipt-list.component';
import { TransferReceiptEditComponent } from './transferreceipt/edit/transferreceipt-edit.component';

const routes: Routes = [
      {
       path: '',
       component: TransactionComponent,
       children: [
          { path: 'stock-summary-lube', component: LubricantAvailabilityListComponent },
          { path: 'stock-summary-tank', component: TankPolAvailabilityListComponent },
          {
            path: 'purchase-receipt',
            component: PurchaseReceiptComponent,
            children: [
              { path: '', component: PurchaseReceiptListComponent },
              { path: 'list', component: PurchaseReceiptListComponent },
              { path: 'add', component: PurchaseReceiptAddComponent },
              { path: 'view/:id', component: PurchaseReceiptEditComponent },
            ]
          },
          {
            path: 'transfer-request',
            component: TransferRequestComponent,
            children: [
              { path: '', component: TransferRequestListComponent },
              { path: 'list', component: TransferRequestListComponent },
              { path: 'add', component: TransferRequestAddComponent },
              { path: 'view/:id', component: TransferRequestEditComponent },
            ]
          },
          {
            path: 'transfer',
            component: TransferComponent,
            children: [
              { path: '', component: TransferListComponent },
              { path: 'list', component: TransferListComponent },
              { path: 'add', component: TransferAddComponent },
              { path: 'view/:id', component: TransferEditComponent },
            ]
          },
          {
            path: 'transfer-receipt',
            component: TransferReceiptComponent,
            children: [
              { path: '', component: TransferReceiptListComponent },
              { path: 'list', component: TransferReceiptListComponent },
              { path: 'view/:id', component: TransferReceiptEditComponent },
              { path: 'edit/:id', component: TransferReceiptEditComponent },
            ]
          },
        ]
      },
];

export const routing = RouterModule.forChild(routes);
