
import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import { IMyOptions } from 'mydatepicker';
import { TransferReceiptFormService } from 'app/admin/receiptandtransfer/transferreceipt/service/transferreceipt.service';

@Component({

  selector: 'transferreceipt-form',
  templateUrl: './transferreceipt-form.html',

  // selector: 'tab-group-basic-example',
  // templateUrl: 'tab-group-basic-example.html',
 // styleUrls: ['tab-group-basic-example.css'],

})
export class TransferReceiptFormComponent implements OnInit,  OnChanges {
  userId: string;
  receivedQtyErrorMessage: string = '';
  totalPolVolume=0;
  polId=0;
  reqSiteId=0;
  stList=[];
  StId=0;
  idalertmessage: string='';
  nullalertmessage: string='';
  savebutton: boolean =true;

  tankId: any;
  PRId: any;
  BatchNo: any;
  polVolume: any;
  prList: any;
  transferReqId: any;
  POL: any;
  volume: any;
  packNo: any;
  package: any;
  packsize: any;
  transporterList: any;
  siteList: any;
  typeReq: string;
  transferList: any;
 // LoadingDate: Date;
  selectedDate: Date;
  Quantity: any;
  printedvalue=0;
  secondaryvalue=0;
  lastvalue=0;
  totalReceivedQuantity=0;
  receivedQuantity=0;
  finalvalue=0;
  initialvalue=0;
  companyName: any;
  mobNo="";
  toSite="";
  fromSite="";
  isLubricantSection=false;
  IsLubricant=false;
  tankList: any;
  loadingDate: string;
    truckList: any;
    driverList: any;
  pollist: any;
  @Input() pr;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  trForm: FormGroup;

  filelength: number;
  sizeError: string;
  ErrorList=[];
  CapacityErrorList=[];
  StorageTankList=[];
  filetype= [];
  filelist= [];

  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';

  public localityGroups=[];
  public usersublocality=[];
  public options= [];
  public optionss= [];
  public optionsnew=[];
  public pr_id = '';
  public PId:any;
  public text = '';
  public drop = '';
  items: any;
  public addRes: any[] = [];

  dateError : any = "";
  loadedDate: any;

  public siteGroups =[];
 private currentdate= new Date();
 //private startDate: Object = { date: { year: 2008, month: 01, day: 01 }    };
 private myDatePickerOptions: IMyOptions = {
  dateFormat: 'dd/mm/yyyy',
  disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()-1}

};
//private LoadingDate :Date;
private DateLoaded: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
isViewPage: boolean = false;
   //params: URLSearchParams = new URLSearchParams()
  constructor( private trService: TransferReceiptFormService, private router: Router, private route: ActivatedRoute,
               private formBuilder : FormBuilder) {
                this.userId=localStorage.getItem('user_nameId');
      this.getPolList();
      this.getSiteList();
      this.getRequestList();
      this.getDriverList();
      this.getTruckList();
      this.getTransporterList();
      //this.getTankList();
      if(this.route.snapshot.url[0].path == "view") {
        this.isViewPage = true;
      } else {
        this.isViewPage = false;
      }
  }

  ngOnInit() {
    if(this.isViewPage) {
      this.trForm = new FormGroup({
        TId:new FormControl(''),
        dtLoaded : new FormControl(''),
        TransferNoId:new FormControl(''),
        DateLoaded:new FormControl(''),
        Tank: new FormControl(''),
        Transporter: new FormControl(''),
        ReceivingTerminal: new FormControl(''),
        Truck: new FormControl(null),
        Driver: new FormControl(''),
        TruckOdometerReading: new FormControl(''),
        ConsignmentNo:new FormControl(''),
        Remarks:new FormControl(''),
        FileType:new FormControl(''),

        FromSite: new FormControl(''),
        ToSite: new FormControl(''),

        CompartmentDetails: new FormArray([
        //this.initSection([]),
        ]),

        TransferLubeDetails: new FormArray([
        // this.initLubeSection([]),
        ]),

      });

      this.route.params.forEach((params: Params) => {
        this.pr_id = params['TRId'];
      });
      this.trForm.disable();
    }
    if(this.page === 'edit' && !this.isViewPage) {
      this.isViewPage = false;
      this.trForm = new FormGroup({
        TId:new FormControl(''),
        dtLoaded : new FormControl(''),
        TransferNoId:new FormControl(''),
        DateLoaded:new FormControl(''),
        Tank: new FormControl(''),
        Transporter: new FormControl(''),
        ReceivingTerminal: new FormControl(''),
        Truck: new FormControl(null),
        Driver: new FormControl(''),
        TruckOdometerReading: new FormControl(''),
        ConsignmentNo:new FormControl(''),
        Remarks:new FormControl(''),
        FileType:new FormControl(''),

        FromSite: new FormControl(''),
        ToSite: new FormControl(''),

        CompartmentDetails: new FormArray([
        //this.initSection([]),
        ]),

        TransferLubeDetails: new FormArray([
        // this.initLubeSection([]),
        ]),

      });

      this.route.params.forEach((params: Params) => {
        this.pr_id = params['TRId'];
      });
    } 
  }

  initSection(data = []) {
    let Quantity = data['volume']  ? data['volume'] : '';
    let BatchNo = data['batchNumber']  ? data['batchNumber'] : this.BatchNo;
    let SealNo = data['sealNo']  ? data['sealNo'] : 0;
    let ObservedDensity = data['observedDensity']  ? data['observedDensity'] : 0;
    let ObservedTemprature = data['observedTemprature']  ? data['observedTemprature'] : 0;
    let Conductivity = data['conductivity']  ? data['conductivity'] : 0;
    let Visual = data['visual']  ? data['visual'] : '';
    let TransferDetailsId = data['transferDetailsId']  ? data['transferDetailsId'] : '';
    let id = data['id']  ? data['id'] : '';
    let tankDetails = new FormArray([]);
    let section = new FormGroup({
      id : new FormControl(id),
      TransferDetailsId : new FormControl(TransferDetailsId),
      CompartmentNo : new FormControl(),
      POL: new FormControl(),
      //BatchNo: new FormControl(this.BatchNo),
      Volume: new FormControl(Quantity),
      ObservedDensity: new FormControl(ObservedDensity),
      ObservedTemprature: new FormControl(ObservedTemprature),
      SealNo: new FormControl(SealNo),
      Conductivity: new FormControl(Conductivity),
      Visual: new FormControl(Visual),
      PurchaseReceiptId:new FormControl(this.PRId),
      ToSiteTank: tankDetails,
      /*ToSiteTank: new FormArray([
        this.initTankSection(data['toSiteTankDetails']),
      ])*/
    });
    //this.initTankSection(data['toSiteTankDetails']);
    return section;
  }

  initTankSection(tankDetails = [], i) {
    //let i = 0;
    const stControl = (<FormArray>(<FormGroup>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank'].controls;
    if(tankDetails.length > 0) {
      tankDetails.forEach(tank => {
        stControl.push(
          new FormGroup({
            StorageTankId: new FormControl(tank['storageTankId'], Validators.required),
            Quantity: new FormControl(tank['quantity'], Validators.required),
          })
        )
        i++;
      })
    } else {
      stControl.push(
        new FormGroup({
          StorageTankId: new FormControl('', Validators.required),
          Quantity: new FormControl('', Validators.required),
        })
      )
    }
    /*let StorageTankId = tank['storageTankId']  ? tank['storageTankId'] : '';
    let Quantity = tank['quantity']  ? tank['quantity'] : '';
    return new FormGroup({
        StorageTankId: new FormControl(StorageTankId, Validators.required),
        Quantity: new FormControl(Quantity, Validators.required),
    });*/
  }

  /*initTankSection(tank = []) {
    let tankData = JSON.parse(JSON.stringify(tank))
    console.log("tankData 1",tankData);
    // for(let i=1;i<tankData.length;i++)
    // {
      if(tankData.length === 0) {
        return new  FormArray([
          new FormGroup({
            StorageTankId: new FormControl('', Validators.required),
            Quantity: new FormControl('', Validators.required),
          })
        ]);
      } else {
          let formGroupArray = new FormArray([]);
          tankData.forEach(tank => {

            let StorageTankId = tank['storageTankId']  ? tank['storageTankId'] : '';
            let Quantity = tank['quantity']  ? tank['quantity'] : '';
            this.stList.push(StorageTankId);
            this.initialvalue=Quantity;
            this.StId=StorageTankId;
            this.totalPolVolume+=Number(this.initialvalue);
            console.log('StorageTankId', StorageTankId, Quantity);
            formGroupArray.push(new FormGroup({
              StorageTankId: new FormControl(StorageTankId),
              Quantity: new FormControl(Quantity),
            }))
          });
          console.log('formGroupArray', formGroupArray);
          return formGroupArray;
      }
  }*/

  initLubeSection(data = []) {
    let Product = data['polId']  ? data['polId'] : '';
    let Package = data['uomName']  ? data['uomName'] : this.package;
    let PackSize = data['packSize']  ? data['packSize'] : this.packsize;
    let NoOfPacks = data['numberOfPackets']  ? data['numberOfPackets'] : '';
    let Volume = data['volume']  ? data['volume'] : this.volume;
    this.packsize=PackSize;
    this.package=Package;
    this.packNo=NoOfPacks;
    this.volume=Volume;
    return new FormGroup({
      POLId: new FormControl(Product),
      Package: new FormControl(Package),
      PackSize : new FormControl(PackSize),
      NumberOfPackets : new FormControl(NoOfPacks),
      Volume : new FormControl(Volume),
    });
  }
  /*addSection() {
      const control = <FormArray>this.trForm.get('CompartmentDetails');
      control.push(this.initSection());
      this.stList.push(this.StId);
  }
  addLubeSection() {
    const control = <FormArray>this.trForm.get('TransferLubeDetails');
    control.push(this.initLubeSection());
  }*/
  /*addTankSection(form, j) {
    this.receivedQtyErrorMessage="";
    if(this.nullalertmessage=="" && this.idalertmessage=="" && this.initialvalue>0 && this.StId>0) {
        const control = form.controls.ToSiteTank.controls;//.get('ToSiteTank');
        control.push(
          new FormGroup({
            StorageTankId: new FormControl(''),
            Quantity: new FormControl(''),
          })
        );
        this.stList.push(this.StId);
        this.StId=0;
        this.initialvalue=0;
        this.idalertmessage='';
        this.nullalertmessage='';
    } else if(this.idalertmessage=="") {
        this.nullalertmessage ="Storage Tank or Quantity Can't be Empty!"
    }
  }*/

  isAddedAllTanks = false;
  showAddBtn = [];
  addTankSection(item, i, j) {

    let itemValue = item.value;
    const control = (<FormArray>(<FormArray>(<FormGroup>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank']).controls[j].value;
    //const control = (<FormGroup>(<FormArray>this.trForm.get('ToSiteTank')).controls[i]).value;
    if(itemValue.StorageTankId == "" || itemValue.Quantity == "") {
      //this.nullalertmessage = "Tank Details Can't be Empty!"
      this.compartmentQuantityError[i] = "Storage Tank Details can't be empty!";
    } else if(this.tankArrayError[i] == '') {
      this.nullalertmessage = "";
      this.compartmentQuantityError[i] = "";
      if(this.stList[i].length == 0) {
        this.stList[i] = [];
        this.stList[i][j] = control['StorageTankId'];
        if(this.stList[i].length == this.tankList.length) {
          this.isAddedAllTanks = true;
        } else {
          this.isAddedAllTanks = false;
        }
        if(!this.isAddedAllTanks) {
          const stControl = (<FormArray>(<FormGroup>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank'].controls;
          //stControl.push(this.initTankSection());
          this.initTankSection([], i);
          this.showAddBtn[i][j] = true;
        }
      } else {
          let isTankPresent = false;
          this.stList[i].forEach(tank => {
            if(tank == this.StId) {
              isTankPresent = true;
            }
          });
          //if(!isTankPresent) {
            this.stList[i][j] = control['StorageTankId'];
            this.showAddBtn[i][j] = true;
            if(this.stList[i].length == this.tankList.length) {
              this.isAddedAllTanks = true;
            } else {
              this.isAddedAllTanks = false;
            }
            if(!this.isAddedAllTanks) {
              const stControl = (<FormArray>(<FormGroup>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank'].controls;
              this.initTankSection([],i)
              //stControl.push(this.initTankSection([]));
              /*stControl.push(
                new FormGroup({
                  StorageTankId: new FormControl(''),
                  Quantity: new FormControl(''),
                })
              );*/
              this.showAddBtn[i][j] = true;
            }
            this.tankArrayError[i] = '';
          /*} else {
            this.tankArrayError = 'Tank is already selected.Choose another one!';
            if(this.stList[i].length == this.tankList.length) {
              this.tankArrayError = '';
            } else if(this.stList[i].length == i+1) {
              this.showAddBtn[i][j] = true;
              this.tankArrayError = '';
              const stList = (<FormArray>(<FormGroup>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank'].controls;
              //stList.push(this.initTankSection([]));
              this.initTankSection([], i)
              /*stList.push(
                new FormGroup({
                  StorageTankId: new FormControl(''),
                  Quantity: new FormControl(''),
                })
              );/*
              this.showAddBtn[i][j] = true;
            }
          }*/
      }
    }
	}


  getCompartmentDetails(form) {
    return form.controls.CompartmentDetails.controls;
  }

  getStorageTankDetails(form) {
    return form.controls.ToSiteTank.controls;
  }

  getTransferLubeDetails(form) {
    return form.controls.TransferLubeDetails.controls;
  }

  removeSection(i){
    const control = <FormArray>this.trForm.get('CompartmentDetails');
    control.removeAt(i);
   }
//    removeTankSection(i, j){
//      this.idalertmessage="";
//      this.receivedQtyErrorMessage="";
//     const control = <FormArray>this.trForm.get('CompartmentDetails').controls[i].get('ToSiteTank');
//     this.totalPolVolume=Number(this.totalPolVolume)- Number(control.controls[j].controls['Quantity'].value);
//     control.removeAt(j);
//     this.stList.splice(this.StId);
//     this.idalertmessage='';
//     this.nullalertmessage='';
//     this.StId=1;
//     this.initialvalue=1;
//  }
  //************************************************************* */

  removeTankSection(i, j) {

    const control = (<FormArray>this.trForm.get('CompartmentDetails')).controls[i].get('ToSiteTank');
    this.totalPolVolume=Number(this.totalPolVolume)- Number((<FormArray>(<FormGroup>control).controls[j]).controls['Quantity'].value);
    (<FormArray>control).removeAt(j);
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.tankArrayError[i]="";
    this.idalertmessage = '';
    this.receivedQtyErrorMessage="";
    this.StId = 1;
    this.initialvalue = 1;
    this.showAddBtn[i][(<FormArray>control).length-1] = false;
    this.stList[i].splice(j, 1);
    if(this.stList[i].length == this.tankList.length) {
      this.isAddedAllTanks = true;
    } else {
      this.isAddedAllTanks = false;
    }
  }

  onPackChange(event: any) {
    this.packNo=event;
    this.volume= Number(this.packNo)* Number(this.packsize);
  };

  getBatchAndPol(traId,tankId,id)
  {
    this.trService.getTransferRequestBatchNo(traId,tankId).then(response=>{
    this.prList=response['purchaseReceipt'];
    //this.setBatchDetails();
    /*if(id==0)
    {
        for(let i=0;i<this.prList.length;i++)
        {
          if(Number(this.polVolume)<this.prList[i].quantity)
          {
              this.BatchNo=this.prList[i].batchNumber;
              this.PRId=this.prList[i].purchaseReceiptId;
          }
        }
      }
      else
      {
        for(let i=0;i<this.prList.length;i++)
        {
          this.BatchNo=this.prList[i].batchNumber;
          this.PRId=this.prList[i].purchaseReceiptId;
        }
      }*/
    })
  }

  compartmentQuantityError = [];
  onSubmit(validPost) {
    this.clearerror();
    this.typeReq="Transfer type is required";
    this.formSubmited = true;
    /*if(this.totalReceivedQuantity!=this.totalPolVolume) {
      this.receivedQtyErrorMessage="Total Volume and Total Quantity Entered in the Tank are not same!"
    }*/
    let hasCompartmentQuantityError = false;
    let compartmentControl = (<FormArray>this.trForm.get('CompartmentDetails')).controls
    let i = 0;
    compartmentControl.forEach(compartment => {
      let tanks = (<FormArray>(<FormGroup>compartment).controls['ToSiteTank']).controls;
      let j = 0;
      validPost.CompartmentDetails[i].ToSiteTank = {tankDetails:[]};
      tanks.forEach(tank => {
        if(tank.value.StorageTankId == "" || tank.value.Quantity == "") {
          this.compartmentQuantityError[i] = "Storage Tank Details can't be empty!";
          hasCompartmentQuantityError = true;
        } else {
          this.compartmentQuantityError[i] = "";
          let tankObj = {
            StorageTankId : tank.value.StorageTankId,
            Quantity : tank.value.Quantity,
            BatchDetails : this.tankBatchDetails[i][j]
          }
          validPost.CompartmentDetails[i].ToSiteTank['tankDetails'].push(tankObj);
        }
        j++;
      });
      i++;
    });

    //this.compartmentQuantityError = [];
    /*let errorMsg = 'Quantity should not be greater than Compartment Volume';
    let compDetails = validPost.CompartmentDetails;
    let k = 0;
    compDetails.forEach(compartment => {
      let volume = compartment.Volume;
      let storageTank = compartment.ToSiteTank;
      if(storageTank.hasOwnProperty('tankDetails')) {
        storageTank = compartment.ToSiteTank.tankDetails;
      }
      let quantity = 0;
      storageTank.forEach(tank => {
        quantity = Number(quantity) + Number(tank.Quantity);
      });
      if(this.compartmentQuantityError[k] == '') {
        if(volume < quantity) {
          this.compartmentQuantityError[k] = errorMsg;
          hasCompartmentQuantityError = true;
        } else {
          this.compartmentQuantityError[k] = '';
        }
      }
      k++;
    });*/

    /*if (isStorageTankDetailsNotPresent) {
      this.receivedQtyErrorMessage = "Storage Tank Details can't be empty!";
    } else {
      this.receivedQtyErrorMessage = "";
    }*/
    if(this.ErrorList.length == 0 && this.CapacityErrorList.length == 0 && !hasCompartmentQuantityError) {
      validPost.uploadedFile = this.filetype;
      validPost.ReceivedQuantity=this.printedvalue;
      validPost.IsLubricant=this.IsLubricant;
      validPost.IsTransferReceipt=true;
      validPost.TransferNoId=this.transferID;
      this.saved.emit(validPost);
    } else {
      jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  clearerror() {
    this.error="";
  }

  clearmsgs() {
    this.receivedQtyErrorMessage = "";
    this.compartmentQuantityError = [];
    let compartmentControl = (<FormArray>this.trForm.controls['CompartmentDetails']).controls;
    for(let i=0; i<compartmentControl.length; i++) {
      const control = (<FormArray>(<FormGroup>compartmentControl[i]).controls['ToSiteTank']).controls;
      for(let j =1; j< control.length; j++) {
        control.pop();
      }
      control[0].get('Quantity').setValue('');
      control[0].get('StorageTankId').setValue('');
    }
    this.stList = [];
    if(this.trForm.controls['TransferNoId'].hasError('required') || (this.trForm.controls['TransferNoId'].touched)) {
      this.typeReq="";
    }
    let dateLoaded={ date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
    this.trForm.controls['DateLoaded'].setValue(dateLoaded);
    this.error="";
    this.dateError="";
    this.idalertmessage="";
    this.tankArrayError = [];
    this.ErrorList=[];
  }

  getTransferRequests(params){
    this.trService.getTransferList(params).then(r =>  {
        this.siteGroups = r.result;
        this.loading = false;
      })
  }
  transferID : any;
  dateViewPage : any;
  ngOnChanges(change) {
    //this.loading = this.page === 'add' ? false : true;
    if (change.pr && change.pr.currentValue) {
      this.transferPrintData = change.pr.currentValue;
      this.loading = false;
      this.IsLubricant=change.pr.currentValue.transferRequest.islubricant;
      this.totalReceivedQuantity=change.pr.currentValue.totalVolume;
      this.reqSiteId=change.pr.currentValue.transferRequest.requestedSiteId;
      this.polId=change.pr.currentValue.transferRequest.polId;
      this.getTankList(this.reqSiteId,this.polId);
      this.trForm.controls['TId'].setValue(change.pr.currentValue.id);
      this.trForm.controls['TransferNoId'].setValue(change.pr.currentValue.transferRequest.transferId);
      this.transferID = change.pr.currentValue.transferRequest.id;
      let date = new Date(change.pr.currentValue.loadDate);
      this.dateViewPage = date.getDate() + '-' + Number(date.getMonth()+1) + '-' + date.getFullYear();
      this.trForm.controls['dtLoaded'].setValue(change.pr.currentValue.loadDate);
       //this.trForm.controls['Date'].setValue(change.pr.currentValue.Date);
       this.trForm.controls['Tank'].setValue(change.pr.currentValue.storageTankId);
       this.trForm.controls['Transporter'].setValue(change.pr.currentValue.transportCompanyId);
       if(change.pr.currentValue.loadingTerminal!=null && change.pr.currentValue.loadingTerminal!="")
       {
          this.trForm.controls['ReceivingTerminal'].setValue(change.pr.currentValue.loadingTerminal);
       }
       else
       {
         this.trForm.controls['ReceivingTerminal'].setValue(change.pr.currentValue.receivingTerminal);
       }
       this.trForm.controls['Truck'].setValue(change.pr.currentValue.truckId);
       this.trForm.controls['Driver'].setValue(change.pr.currentValue.driverId);
       this.trForm.controls['TruckOdometerReading'].setValue(change.pr.currentValue.truckOdoMeterReading);
       this.trForm.controls['ConsignmentNo'].setValue(change.pr.currentValue.consigmentNo);
       this.trForm.controls['Remarks'].setValue(change.pr.currentValue.remarks);
       this.trForm.controls['FileType'].setValue(this.filetype);

       this.getTransferRequestDetails(change.pr.currentValue.transferRequestId);
       this.transferCompartmentDetails = change.pr.currentValue.transferCompartmentDetails;
       if(this.IsLubricant==false)
       {
          this.getBatchAndPol(change.pr.currentValue.transferRequestId,change.pr.currentValue.storageTankId,1);
          this.getTankDetails(change.pr.currentValue.storageTankId);
       }

       if(change.pr.currentValue.transferCompartmentDetails!=null)
       {
         let compartmentDetails=change.pr.currentValue.transferCompartmentDetails;
         let items = this.trForm.get('CompartmentDetails') as FormArray;
         let i = 0;
         compartmentDetails.forEach(element => {
            items.push(this.initSection(element));
            this.tankBatchDetails.push([]);
            this.compBatchList.push([]);
            this.initTankSection(element['toSiteTankDetails'], i);
            this.stList.push([]);
            this.showAddBtn.push([]);
            i++;
         });
         //if(typeof(this.prList) != 'undefined') {
            this.setBatchDetails();
         //}
        //  let compTankDetails=change.pr.currentValue.toSiteTankDetails;
        //  let aitems = this.trForm.get('ToSiteTank') as FormArray;

        //  compTankDetails.forEach(element => {
        //   items.push(this.initTankSection(element));
        //  });

       }

       else if(change.pr.currentValue.transferLubricantDetails!=null)
       {
          let lubeValue = change.pr.currentValue.transferLubricantDetails;
          const control =  <FormArray>this.trForm.get(['TransferLubeDetails']);
          let items = this.trForm.get('TransferLubeDetails') as FormArray;
          lubeValue.forEach(element => {
            items.push(this.initLubeSection(element));
            this.getPolDetails(element.polId);
       });
     }
     /*if(this.isViewPage) {
      let i = 0;
      if(this.transferCompartmentDetails) {
        this.transferCompartmentDetails.forEach(transferCompartment => {
          transferCompartment.compartment.forEach(batch => {
            this.compBatchList[i].push(batch.batchName);
          });
          i++;
        });
      }
     }*/
     this.loadedDate = change.pr.currentValue.dateLoaded;
    } else{
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList=[];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i <this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152)
      {
        this.ErrorList.push("The file "+this.filelist[i].name +" is too large. Maximum file size permitted is 2 Mb");
      }
      else if(extnsion!="pdf" && extnsion!="doc" && extnsion!="docx" && extnsion!="xls" && extnsion!="xlsx" && extnsion!="png" && extnsion!="jpg" && extnsion!="jpeg" && extnsion!="gif"  && extnsion!="txt")
      {
        this.ErrorList.push("The selected file "+this.filelist[i].name +" is a an unsupported file");
      }
      else
      {
        this.ErrorList=[];
        this.filetype.push(this.filelist[i]);
        this.filelength= this.filetype.length;
      }
    }
  }
  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  getPolList() {
    this.pollist = [];
    this.trService.getpolList().then(r => {
      r.result.forEach(pol => {
        if(pol.isLubricant) {
          this.pollist.push(pol);
        }
      });
    })
    .catch(r => {
      this.handleError(r);
    });
  }

  getTransporterList(){
    this.trService.getTransporterList().then(r => {
      this.transporterList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  getRequestList(){
    this.trService.getTransferRequestList().then(r => {
    this.transferList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  getSiteList(){
    this.trService.getSiteListByUser(this.userId).then(r => {
                this.siteList = r;
            })
                .catch(r => {
                    this.handleError(r);
                });
  }

  getDriverList(){
    this.trService.getDriverList().then(r => {
      this.driverList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  getTruckList(){
    this.trService.getTruckList().then(r => {
      this.truckList = r.result;
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  tankError: string = '';
  getTankList(siteId,polId){
    this.trService.getTankList(siteId,polId).then(r => {
      this.tankList = r;
      if(this.tankList.length == 0) {
        this.tankError = 'No Storage Tank in the Site';
      } else {
        this.tankError = '';
      }
  })
      .catch(r => {
          this.handleError(r);
      })
  }

  getTransferRequestDetails(id)
  {
    this.trService.getTransferRequestById(id).then(response=>{
      this.fromSite=response.fromSite.name;
      this.transferReqId=id;
      this.toSite=response.requestedSite.name;
      this.IsLubricant=response.islubricant;
      if(this.IsLubricant) {
        this.isLubricantSection=true;
      } else {
        this.isLubricantSection=false;
      }
    })
  }

  getPolDetails(id)
  {
      this.trService.getPolDetailsById(id).then(response=>{
      this.package=response.uom.name;
      this.packsize=response.packSize;
    })
  }

  getTankDetails(id)
  {
      this.tankId=id;
      this.trService.getTankDetailsById(id).then(response=>{
      this.POL=response.pol.name;
    })
  }

  /*getSTDetails(id) {
    this.nullalertmessage="";
    this.idalertmessage="";
    this.StId=id;
    console.log('stList', this.stList);
    for( let i = 0; i< this.stList.length+1; i++){
      if(this.stList[i]==id){
        this.idalertmessage ="Storage Tank is already selected.Choose another one!";
      }
    }
  }*/

 tankArrayError = [];
  getSTDetails(event, section, i, j) {

    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.tankArrayError[i] = "";
    this.StId = event;
    this.isAddedAllTanks = false;
    let isPresent = false;
    this.stList[i].forEach(tank=>{
      if(tank == event) {
        isPresent = true;
      }
    });
    if(isPresent) {
      this.tankArrayError[i] = "Tank is already selected.Choose another one!";
      this.stList[i][j] = event;
      /*if(this.stList[i].length > j-1) {
        if(this.stList[i][j] == event) {
          this.tankArrayError = '';
        }
      }*/
    } else {
      this.stList[i][j] = event;
      if(this.stList[i].length+1 == this.tankList.length) {
        this.stList[i][j] = event;
      } else {
        this.tankArrayError[i] = '';
      }
    }
 }

onTextChange(event: any, i) {
  this.receivedQtyErrorMessage="";
  // this.nullalertmessage="";
  // this.idalertmessage="";
  this.initialvalue=event;
  this.compartmentQuantityError[i] = null;
    //let control = <FormArray>this.trForm.get('CompartmentDetails').controls.get('ToSiteTank');
    const compartmentControl = (<FormArray>this.trForm.get('CompartmentDetails')).controls;
    const tankControl = (<FormGroup>(<FormArray>this.trForm.get('CompartmentDetails')).controls[i].get('ToSiteTank')).controls;
    let total = 0;
    for( let m = 0; m < compartmentControl.length; m++ ){
      for(let k=0; k < (<FormArray>compartmentControl[m].get('ToSiteTank')).controls.length; k++){
        total = total + Number((<FormGroup>(<FormArray>compartmentControl[m].get('ToSiteTank')).controls[k]).controls['Quantity'].value);
      }
    }
    // let controlvalue = control._value;
    // for (let i = 0; i < controlvalue.length; i++) {
    //   this.totalPolVolume = Number(this.totalPolVolume) + Number(controlvalue[i].Quantity)
    // }
    this.totalPolVolume=total;
}
// onQtyChange(event: any) {
//   this.polVolume=event;
//   this.totalPolVolume+=Number(this.polVolume);
// };


  onDateChanged(event:any, type: any) {
    let loadedDate = new Date(this.loadedDate);
    loadedDate.setHours(0, 0, 0);
    let loadedDateFormatted = loadedDate.getTime();
    let selectedDate = event.date.year+'-'+event.date.month+'-'+event.date.day;
    let selectedDateFormatted = new Date(selectedDate);
    selectedDateFormatted.setHours(0, 0, 0);
    let currentDate = new Date();
    currentDate.setHours(0, 0, 0);
    let currentDateFormatted = currentDate.getTime();
    if(Math.floor(loadedDateFormatted/Math.pow(10, 3)) > Math.floor(selectedDateFormatted.getTime()/Math.pow(10, 3))) {
      this.dateError = "Date Received should be greater than or equal to Transfer date";
    } else if(Math.floor(currentDateFormatted/Math.pow(10, 3)) < Math.floor(selectedDateFormatted.getTime()/Math.pow(10, 3))) {
      this.dateError = "Date Received should be less than or equal to current date";
    } else {
      this.DateLoaded = event.formatted;
      this.dateError = "";
    }
  }

  compBatchList = [];
  batchDetailsList = [];
  batchList = [];
  batchListClone = [];
  compartmentVolume = [];
  arrangedBatchList = [];
  tankBatchDetails = [];
  /*setBatchDetails() {
    console.log('transferCompartmentDetails', this.transferCompartmentDetails);
    let compartmentControls = (<FormArray>this.trForm.get('CompartmentDetails')).controls
    if(typeof(this.prList) != 'undefined') {
      for(let i = 0; i < compartmentControls.length; i++) {
        this.compBatchList[i] = [];
        this.batchDetailsList[i] = [];
        let volume = compartmentControls[i].value['Volume'];
        for( let batchList of this.prList ) {
          if (volume == batchList.quantity && volume != 0) {
            this.compBatchList[i].push(batchList.batchNumber);
            this.batchDetailsList[i].push({
              'batch' : batchList.purchaseReceiptId,
              'volume' : Number(volume)
            })
            volume = 0;
            break;
          } else if (volume > batchList.quantity &&  batchList.quantity != 0) {
            this.compBatchList[i].push(batchList.batchNumber);
            volume = volume - batchList.quantity;
            this.batchDetailsList[i].push({
              'batch' : batchList.purchaseReceiptId,
              'volume' : Number(batchList.quantity)
            })
          } else if (volume < batchList.quantity) {
            this.compBatchList[i].push(batchList.batchNumber);
            let quantityLeftOnBatch = batchList.quantity - volume;
            this.batchDetailsList[i].push({
              'batch' : batchList.purchaseReceiptId,
              'volume' : Number(volume)
            })
            volume = 0;
            break;
          }
        }
      }
    }
  }*/

  setBatchDetails() {
    let compartmentControls = (<FormArray>this.trForm.get('CompartmentDetails')).controls
    for(let i = 0; i < this.transferCompartmentDetails.length; i++) {
      this.compBatchList[i] = [];
      this.transferCompartmentDetails[i].compartment.forEach(compartment => {
        this.compBatchList[i].push(compartment.batchName);
      });
    }
  }
  /*
  setBatchToTanks(quantity, i, j) {
    this.tankBatchDetails[i][j] = [];
    for( let batch of this.batchDetailsList[i] ) {
      if(quantity == batch.volume && quantity != 0) {
        this.tankBatchDetails[i][j].push({
          batch : batch.batch,
          volume : Number(quantity)
        });
        quantity = 0;
        break;
      } else if(quantity > batch.volume) {
        this.tankBatchDetails[i][j].push({
          batch : batch.batch,
          volume : Number(batch.volume)
        });
      } else if(quantity < batch.volume) {
        this.tankBatchDetails[i][j].push({
          batch : batch.batch,
          volume : Number(quantity)
        });
        break;
      }
    }
  }*/
  
  /**set batch on quantity changes */
  transferCompartmentDetails = [];
  setBatchToTanks(val, i, j) {
    let compartmentTankControls = (<FormArray>(<FormGroup>(<FormArray>this.trForm.get('CompartmentDetails')).controls[i]).controls['ToSiteTank']).controls;
    let quantity = 0;
    for( let k = 0; k < compartmentTankControls.length; k++ ) {
      quantity = Number(quantity) + Number(compartmentTankControls[k].value['Quantity']);
    }
    this.tankBatchDetails[i][j] = [];
    for( let batch of this.transferCompartmentDetails[i].compartment ) {
      //this.compBatchList[i] = [];
      if(val == batch.quantity && val != 0) {
        let batchPresent = false;
        for( let compBatch of this.compBatchList[i] ) {
          if(compBatch == batch.batchName ) {
            batchPresent = true;
            break;
          }
        }
        if(!batchPresent) {
          this.compBatchList[i].push(batch.batchName);
        }
        this.tankBatchDetails[i][j].push({
          batch : batch.batchId,
          volume : Number(val)
        });
        batch.quantity = 0;
        val = 0;
        break;
      } else if(val > batch.quantity) {
        let batchPresent = false;
        for( let compBatch of this.compBatchList[i] ) {
          if(compBatch == batch.batchName ) {
            batchPresent = true;
            break;
          }
        }
        if(!batchPresent) {
          this.compBatchList[i].push(batch.batchName);
        }
        this.tankBatchDetails[i][j].push({
          batch : batch.batchId,
          volume : Number(batch.quantity)
        });
        batch.quantity = 0;
      } else if(val < batch.quantity) {
        let batchPresent = false;
        for( let compBatch of this.compBatchList[i] ) {
          if(compBatch == batch.batchName ) {
            batchPresent = true;
            break;
          }
        }
        if(!batchPresent) {
          this.compBatchList[i].push(batch.batchName);
        }
        this.tankBatchDetails[i][j].push({
          batch : batch.batchId,
          volume : Number(val)
        });
        batch.quantity = batch.quantity - val;
        break;
      }
    }
  }

  compartmentDetails = [];
  transferLubeDetails = [];
  transferPrintData : any = {
    transferRequest : {
      fromSite : {},
      transferCompartmentDetails : {}
    }
  };
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html lang="en">
      <head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
      <script src='https://printjs-4de6.kxcdn.com/print.min.js'></script>
      <style></style>
      </head>
      <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
    popupWin.document.close();
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}