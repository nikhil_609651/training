import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class TransferReceiptFormService{
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public transferurl = '';
    public truckUrl = '';
    public driverUrl = '';
    public requestUrl = '';
    public polUrl = '';
    public filterpolUrl = '';
    public getPolUrl = '';
    public getTankUrl = '';
    public tankUrl = '';
    public transportUrl = '';
    public TRUrl = '';
    public siteUrl = '';
    public filtersiteUrl = '';
    public attachmenturl = '';
    public statusUrl = '';
    public approverUrl = '';
    public getBatchUrl = '';
    public deleteattachmentUrl = '';
    public sitedropdownUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.listUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/TransferDetails/';
        this.saveUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/SaveTransferReceiptDetails';
        this.deleteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeleteTransferDetailsByID?id=';
        this.transferurl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferDetailsByID?id=';
        this.requestUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/TransferRequest/';
        this.TRUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestById?id=';
        //this.polUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestPOLFilter/';
        this.polUrl = _configuration.ServerWithApiUrl+'Masters/POL/';
        this.filterpolUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferDetailsPOLFilter/';
        this.getPolUrl = _configuration.ServerWithApiUrl+'Masters/GetPolById?id=';
        this.getTankUrl = _configuration.ServerWithApiUrl+'Masters/GetStorageTankById?id=';
        this.truckUrl = _configuration.ServerWithApiUrl+'Masters/Truck/';
        this.driverUrl = _configuration.ServerWithApiUrl+'Masters/Driver/';
        this.tankUrl = _configuration.ServerWithApiUrl+'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.transportUrl = _configuration.ServerWithApiUrl+'Masters/TransportCompany/';
        //this.siteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferRequestFromSiteFilter/';
        this.siteUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.filtersiteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferDetailsSiteFilter/';
        this.attachmenturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetAllTransferDetailsAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeleteTransferDetailsAttachments?Id=';
        this.statusUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferDetailsApprovedStatusFilter';
        this.approverUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTransferDetailsApproverFilter';
        this.getBatchUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetAllBatchDetailsByTransferAndTank?TransferRequestId=';
        this.sitedropdownUrl = _configuration.ServerWithApiUrl+'UserManagement/GetSitebyUserId?userid=';
    }


// List API for Transfer
getTransferList(params: any): Promise<any>{
    return this.authHttp.get(this.listUrl,{search: params})
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);

}
getpolList(): Promise<any> {
    return this.authHttp.get(this.polUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}

getpolFilterList(): Promise<any> {
    return this.authHttp.get(this.filterpolUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTransporterList(): Promise<any> {
    return this.authHttp.get(this.transportUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getDriverList(): Promise<any> {
    return this.authHttp.get(this.driverUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTruckList(): Promise<any> {
    return this.authHttp.get(this.truckUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTransferRequestList(): Promise<any> {
    return this.authHttp.get(this.requestUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getSiteList(): Promise<any> {
    return this.authHttp.get(this.siteUrl+'?UserId='+localStorage.getItem('user_nameId'))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getFilterSiteList(userId): Promise<any> {
    return this.authHttp.get(this.filtersiteUrl+ '?UserId=' + userId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTankList(id:any,polId): Promise<any> {
    return this.authHttp.get(this.tankUrl + id + '&POLId=' + polId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getApprovedStatusList(): Promise<any> {
    return this.authHttp.get(this.statusUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getApproverList(): Promise<any> {
    return this.authHttp.get(this.approverUrl)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTransferRequestById(id: string) {
    return this.authHttp.get(this.TRUrl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
//   Save API for Transfer Receipt
Save(bu: any): Promise<any>  {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
        .post(this.saveUrl, bu)
        .toPromise()
        .then(response => response.json().data)
        .catch(this.handleError);
}

deleteTransfer(id: any) {
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });

    let url =   this.deleteUrl;
    return this.authHttp
    .post(this.deleteUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getTransfer(id: string) {
    return this.authHttp.get(this.transferurl +  id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getallTransferAttachments(id):Promise<any> {
    return this.authHttp.get(this.attachmenturl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getPolDetailsById(id: string) {
    return this.authHttp.get(this.getPolUrl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTankDetailsById(id: string) {
    return this.authHttp.get(this.getTankUrl + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
getTransferRequestBatchNo(TRId: string,TankId:string) {
    return this.authHttp.get(this.getBatchUrl +  TRId + '&TankId=' + TankId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
deleteattachment(id:any){
    const formData = new FormData();
    formData.append('id', id);

    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.deleteattachmentUrl + id, options)
    .toPromise()
    .then(() => id)
    .catch(this.handleError);
}

getSiteListByUser(userid: any): Promise<any> {
    return this.authHttp.get(this.sitedropdownUrl + userid)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
}
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
