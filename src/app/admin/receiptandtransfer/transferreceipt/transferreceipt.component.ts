import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'transfer-receipt',
    template: `<router-outlet></router-outlet>`
})
export class TransferReceiptComponent {
	constructor(private router: Router) {
	}
}
