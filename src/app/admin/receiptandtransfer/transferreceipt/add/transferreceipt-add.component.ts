import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TransferReceiptFormService } from 'app/admin/receiptandtransfer/transferreceipt/service/transferreceipt.service';

@Component({
  selector: 'add',
  template: require('./transferreceipt-add.html')
})

export class TransferReceiptAddComponent implements OnInit{
  countryList: any;

  public error = {};
  public success = '';
  public customer: any;
  public page = '';
  public id = '';
  public pr: any;

  constructor(private router: Router, private route: ActivatedRoute,
    private prService: TransferReceiptFormService) {
  }

  ngOnInit() {
    this.page = 'view';
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.pr = this.prService.getTransfer(this.id);  
    });
  }
}
