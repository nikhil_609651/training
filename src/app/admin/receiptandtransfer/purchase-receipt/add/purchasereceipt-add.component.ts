import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PurchaseReceiptFormService } from 'app/admin/receiptandtransfer/purchase-receipt/service/purchasereceipt-form.service';

@Component({
  selector: 'add',
  template: require('./purchasereceipt-add.html')
})
export class PurchaseReceiptAddComponent implements OnInit{
  countryList: any;
  public error = {};
  public success = '';
  public customer: any;
  public page = '';

  constructor(private router: Router,
    private prService: PurchaseReceiptFormService) {
  }

  ngOnInit() {
    this.page = 'add';
  }

  onSave(pr: any) {
    let files = pr.uploadedFile;
    let PurchaseReceiptStorageTankDetails=pr.PurchaseReceiptDetails;
    let PurchaseReceiptLubricantDetails=pr.PurchaseReceiptLubeDetails;
    let prDetails;
    let prLubeDetails;
    let formData = new FormData();
    if(pr.IsLubricant==false)
    {
       prDetails =  JSON.stringify(PurchaseReceiptStorageTankDetails);
       prLubeDetails=null;
    }
    else
    {
       prLubeDetails =  JSON.stringify(PurchaseReceiptLubricantDetails);
       prDetails=null;
    }
    let loadingdate = pr.LoadingDate;
    let receivingdate=pr.ReceivingDate;
    let batchexpdate=pr.BatchExpiryDate;

    if(typeof loadingdate == 'object') {      
      if(loadingdate.date.month < 10 && loadingdate.date.day < 10) {
        loadingdate = '0' + loadingdate.date.day + '/' + '0' + loadingdate.date.month + '/' + loadingdate.date.year;
      } else if(loadingdate.date.day < 10 && loadingdate.date.month >= 10) {
        loadingdate = '0' + loadingdate.date.day + '/' + loadingdate.date.month + '/' + loadingdate.date.year;
      } else if(loadingdate.date.day >= 10 && loadingdate.date.month < 10) {
        loadingdate = loadingdate.date.day + '/' + '0' + loadingdate.date.month + '/' + loadingdate.date.year;
      } else {
        loadingdate = loadingdate.date.day + '/' + loadingdate.date.month + '/' + loadingdate.date.year;
      }
    } else {
      loadingdate= pr.LoadingDate;
    }

    if(typeof receivingdate == 'object'){
      if(receivingdate.date.month < 10 && receivingdate.date.day < 10) {
        receivingdate = '0' + receivingdate.date.day + '/' + '0' + receivingdate.date.month + '/' + receivingdate.date.year;
      } else if(receivingdate.date.day < 10 && receivingdate.date.month >= 10) {
        receivingdate = '0' + receivingdate.date.day + '/' + receivingdate.date.month + '/' + receivingdate.date.year;
      } else if(receivingdate.date.day >= 10 && receivingdate.date.month < 10) {
        receivingdate = receivingdate.date.day + '/' + '0' + receivingdate.date.month + '/' + receivingdate.date.year;
      } else {
        receivingdate = receivingdate.date.day + '/' + receivingdate.date.month + '/' + receivingdate.date.year;
      }
    } else {
      receivingdate= pr.ReceivingDate;
    }

    if(typeof batchexpdate == 'object') {
      if(batchexpdate.date.month < 10 && batchexpdate.date.day < 10) {
        batchexpdate = '0' + batchexpdate.date.day + '/' + '0' + batchexpdate.date.month + '/' + batchexpdate.date.year;
      } else if(batchexpdate.date.day < 10 && batchexpdate.date.month >= 10) {
        batchexpdate = '0' + batchexpdate.date.day + '/' + batchexpdate.date.month + '/' + batchexpdate.date.year;
      } else if(batchexpdate.date.day >= 10 && batchexpdate.date.month < 10) {
        batchexpdate = batchexpdate.date.day + '/' + '0' + batchexpdate.date.month + '/' + batchexpdate.date.year;
      } else {
        batchexpdate = batchexpdate.date.day + '/' + batchexpdate.date.month + '/' + batchexpdate.date.year;
      }
    } else {
      batchexpdate= pr.BatchExpiryDate;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', '0');
    formData.append('IsLubricant', pr.IsLubricant);
    formData.append('GRNNo', pr.GRNNo);
    formData.append('LoadingDate', loadingdate);
    formData.append('SiteId', pr.Site);
    formData.append('ReceivingDate', receivingdate);
    formData.append('BatchNumber', pr.Batch);
    formData.append('Temperature', pr.Temperature);
    formData.append('BatchExpiryDate',batchexpdate);
    formData.append('Density', pr.Density);
    formData.append('LoadedQuantity', pr.LoadedQuantity);
    formData.append('PurchaseOrderNo', pr.PurchaseOrder);
    formData.append('POLId', pr.POL);
    formData.append('LaboratorySampleNo', pr.LaboratorySampleNo);
    formData.append('DischargeVehicleId', pr.TruckNo);
    formData.append('TruckOdoMeterReading', pr.TruckOdometerReading);
    formData.append('DriverId', pr.Driver);
    formData.append('Variance', pr.Variance);
    formData.append('ReceivedQuantity', pr.ReceivedQuantity);
    formData.append('ReceivedQuantity15DegreeC', pr.ReceivedQuantity15DegreeC);
    formData.append('PurchaseReceiptStorageTankDetails', prDetails);
    formData.append('PurchaseReceiptLubricantDetails', prLubeDetails);
    formData.append('Remarks', pr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.prService.Save(formData).then(r =>  {
      this.success = 'Purchase Receipt Created Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/purchase-receipt']);
      }.bind(this), 2000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}
