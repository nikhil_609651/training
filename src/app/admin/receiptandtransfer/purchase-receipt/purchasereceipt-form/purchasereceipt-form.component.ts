import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PurchaseReceiptFormService } from 'app/admin/receiptandtransfer/purchase-receipt/service/purchasereceipt-form.service';
import { IMyOptions } from 'mydatepicker';
import { TransferReceiptFormService } from '../../transferreceipt/service/transferreceipt.service';

@Component({
  selector: 'purchasereceipt-form',
  templateUrl: './purchasereceipt-form.html',
})

export class PurchaseReceiptFormComponent implements OnInit, OnChanges {
  userId: string;
  qtyZero: string;
  variancealert: string = '';
  purchasereceiptalertmessagelub: string;
  purchasereceiptalertmessage: string;
  prValue = [];
  densityMessage: string='';
  temperatureMessage: string='';
  Density=0;
  Temperature=0;
  variance=0;
  detailsmessage: string;
  quantityissued15=0;
  storagetankreqalert: string = '';
  totallub=0;
  oldSiteId=0;
  prinitialvalue = 0
  QuantityMessage: string='';
  polbysitelist: any;
  datealertreceiving: string = '';
  loadedQty = 0;
  qtyErrorMsg: string = '';
  savebutton: boolean = true;
  nullalertmessage: string = '';
  lubealertmessage: string = '';
  lubeId: number = 0;
  lubeList = [];
  section: any;
  idalertmessage: string = '';
  stList = [];
  prList = [];
  StId: number = 0;
  datealertloading: string = '';
  truckmessage: string = '';
  odometerreading: any;
  previousodometerreading: any;
  storagetanklist: any = [];
  datealert: string = '';
  batchexpirycomparedate: Date;
  loadingcomparedate: Date;
  receivingcomparedate: Date;
  tankalertmessage: string = '';
  pollistcat: any;
  POLId: number = 0;
  SiteId: number = 0;
  polReq: string;
  qtyReq: string;
  batchReq: string;
  siteReq: string;
  capacity: any;
  volume = 0;
  packNo = 0;
  dvList: any;
  selectedDate: Date;
  Quantity: any;
  printedvalue = 0;
  secondaryvalue = 0;
  lastvalue = 0;
  totalReceivedQuantity = 0;
  receivedQuantity= 0;
  finalvalue = 0;
  initialvalue = 0;
  companyName: any;
  mobNo = "";
  packsize = "";
  package = "";
  isLubricantSection = false;
  IsLubricant = false;
  tankList: any;
  loadingDate: string;
  siteList: any;
  truckList: any;
  driverList: any;
  pollist: any;
  @Input() pr;
  @Input() error;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  prForm: FormGroup;
  filelength: number;
  sizeError: string;
  ErrorList = [];
  CapacityErrorList = [];
  StorageTankList = [];
  filetype = [];
  filelist = [];
  total = 0;
  public cityGroups = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
  public loading: boolean;
  public formSubmited = false;
  public file = '';
  public localityGroups = [];
  public usersublocality = [];
  public options = [];
  public optionss = [];
  public optionsnew = [];
  public pr_id = '';
  public PId: any = 0;
  public text = '';
  public drop = '';
  items: any;
  public addRes: any[] = [];
  item = [];
  public siteGroups = [];
  private currentdate = new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
   // disableUntil: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() }
  };
  public LoadingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  public ReceivingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  public BatchExpiryDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  showAddBtn = [];
  tempReq = "";
  densityReq = "";
  loadingDateViewPage: any;
  receivingDateViewPage: any;
  expiryDateViewPage: any;
  lubricantList = [];
  isAddedAllLubes = false;
  showAddLubeBtn = [];
  sTankList = [];
  isAddedAllTanks = false;
  lubeErrorMsg = "";
  isReadOnly = false;
  tankArrayError = "";
  selectedTankCapacity = 0;
  truckForm : FormGroup;
  driverForm : FormGroup;
  regNoReq;
  companyReq;
  truckFormSubmited =  false;
  truckError = '';
  driverReq;
  licenceReq;
  transportCompanyReq;
  buReq;
  driverFormSubmited = false;
  constructor(private prService: PurchaseReceiptFormService, private router: Router, private route: ActivatedRoute,
    private transferReceiptFormService: TransferReceiptFormService, private formBuilder: FormBuilder ) {

    this.userId=localStorage.getItem('user_nameId');
    this.getDriverList();
    this.getPolList();
    this.getTruckList();
    this.getSiteList();
    this.getDischargevehicleList();
    this.getCompanyList();
    this.getBUList();
  }

  ngOnInit() {
      this.prForm = new FormGroup({
        IsLubricant: new FormControl(''),
        PRId: new FormControl(''),
        GRNNo: new FormControl(''),
        LoadingDate: new FormControl(''),
        Site: new FormControl('', Validators.required),
        ReceivingDate: new FormControl(''),
        Batch: new FormControl('', [Validators.required]),
        // Batch: new FormControl(''),
        Temperature: new FormControl(0),
        BatchExpiryDate: new FormControl(''),
        Density: new FormControl(0),
        LoadedQuantity: new FormControl('', [Validators.required]),
        PurchaseOrder: new FormControl(''),
        POL: new FormControl('', [Validators.required]),
        //POL: new FormControl(''),
        LaboratorySampleNo: new FormControl(''),
        TruckNo: new FormControl(''),
        companyName: new FormControl(''),
        TruckOdometerReading: new FormControl(''),
        Driver: new FormControl(''),
        DriverMob: new FormControl(''),
        Remarks: new FormControl(''),
        ReceivedQuantity: new FormControl(0),
        ReceivedQuantityAt15: new FormControl(0),
        Variance: new FormControl(0),
        FileType: new FormControl(''),
        PurchaseReceiptDetails: new FormArray([
          this.initSection([]),
        ]),
        PurchaseReceiptLubeDetails: new FormArray([
          this.initLubeSection([]),
        ]),
      });
      this.truckForm = this.formBuilder.group({
        'truckId': [''],
        'regNo':['', Validators.required],
        'companyId':['', Validators.required],
        'Address':[''],
      });
      this.driverForm = this.formBuilder.group({
        'driverId': [''],
        'driverName': ['', Validators.compose([Validators.required])],
        'telephoneNo': [''],
        'drivingLicenceNo': [''],
        'businessunitnameId':['',Validators.compose([Validators.required])],
        'IsInternalUse' : [''],
        'companyId': ['', Validators.compose([Validators.required])],
        'remarks':[''],
        'FileType':[''],
      });
      this.route.params.forEach((params: Params) => {
        this.pr_id = params['PRId'];
      });
      if( this.page == 'edit') {
        this.prForm.disable();
        this.prForm.get('LoadingDate').disable();
        this.prForm.get('ReceivingDate').disable();
        this.prForm.get('BatchExpiryDate').disable();
      }
  }

  initSection(data = []) {
    let StorageTankId = data['storageTankId'] ? data['storageTankId'] : '';
    let Quantity = data['quantity'] ? data['quantity'] : 0;
    let Capacity = data['capacity'] ? data['capacity'] : '';
    if (this.page == 'edit') {
      if(StorageTankId != '') {
        this.stList.push(StorageTankId);
      }
      this.initialvalue = Quantity;
      this.StId = StorageTankId;
      this.receivedQuantity+=this.initialvalue;
    }
    return new FormGroup({
      StorageTankId: new FormControl(StorageTankId),
      Quantity: new FormControl(Quantity),
      Capacity: new FormControl(Capacity)
    });
  }

  initLubeSection(data = []) {
    let Product = data['polId'] ? data['polId'] : '';
    let Package = data['uomName'] ? data['uomName'] : '';
    let PackSize = data['packSize'] ? data['packSize'] : '';
    let NoOfPacks = data['numberOfPackets'] ? data['numberOfPackets'] : '';
    let Volume = data['volume'] ? data['volume'] : 0;
    this.totallub+=NoOfPacks;
    if (this.page == 'edit') {
      this.prList.push(Product);
      this.prinitialvalue = Volume;
      this.lubeId = Product;
    }
    return new FormGroup({
      POLId: new FormControl(Product),
      Package: new FormControl(Package),
      PackSize: new FormControl(PackSize),
      NumberOfPackets: new FormControl(NoOfPacks),
      Volume: new FormControl(Volume),
    });
  }

  selectAll(event) {
    this.prForm = new FormGroup({
      IsLubricant: new FormControl(event.target.checked),
      PRId: new FormControl(''),
      GRNNo: new FormControl(''),
      LoadingDate: new FormControl(''),
      Site: new FormControl('', Validators.required),
      ReceivingDate: new FormControl(''),
      Batch: new FormControl('', [Validators.required]),
      // Batch: new FormControl(''),
      Temperature: new FormControl(0),
      BatchExpiryDate: new FormControl(''),
      Density: new FormControl(0),
      LoadedQuantity: new FormControl('', [Validators.required]),
      PurchaseOrder: new FormControl(''),
      POL: new FormControl('', [Validators.required]),
      //POL: new FormControl(''),
      LaboratorySampleNo: new FormControl(''),
      TruckNo: new FormControl(''),
      companyName: new FormControl(''),
      TruckOdometerReading: new FormControl(''),
      Driver: new FormControl(''),
      DriverMob: new FormControl(''),
      Remarks: new FormControl(''),
      ReceivedQuantity: new FormControl(0),
      ReceivedQuantityAt15: new FormControl(0),
      Variance: new FormControl(0),
      FileType: new FormControl(''),
      PurchaseReceiptDetails: new FormArray([
        this.initSection([]),
      ]),
      PurchaseReceiptLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),
    });
    let Date: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.prForm.controls['LoadingDate'].setValue(Date);
    this.prForm.controls['ReceivingDate'].setValue(Date);
    this.prForm.controls['BatchExpiryDate'].setValue(Date);
    this.QuantityMessage = "";
    this.batchReq = "";
    this.siteReq = "";
    this.qtyReq = "";
    this.polReq = "";
    this.detailsmessage="";
    this.densityMessage="";
    this.temperatureMessage="";
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.qtyErrorMsg = "";
    this.stList = [];
    this.lubeList = [];
    this.receivedQuantity = 0;
    this.printedvalue = 0;
    this.totalReceivedQuantity = 0;
    this.initialvalue = 0;
    this.finalvalue = 0;
    this.error = "";
    this.datealert = '';
    this.datealertloading = '';
    this.ErrorList = [];
    this.idalertmessage = "";
    this.lubealertmessage = "";
    this.qtyErrorMsg = "";
    this.tankalertmessage="";
    this.nullalertmessage="";
    this.storagetankreqalert = "";
    this.datealertreceiving="";
    this.variancealert='';
    this.qtyZero = '';
    this.tempReq = '';
    this.densityReq = '';
    this.lubeErrorMsg = "";
    this.storagetanklist = [];
    if (event.target.checked) {
      this.IsLubricant = true;
       this.isLubricantSection = true;
       this.getPolList();
    } else {
      this.isLubricantSection=false;
      this.IsLubricant=false;
      this.getPolList();
    }
  }

  onTextChange(event: any, section) {
    this.nullalertmessage="";
    this.qtyErrorMsg="";
    this.lubeErrorMsg = "";
    this.detailsmessage="";
    this.initialvalue = event
    let capacity = section._value.Capacity;
    this.total = 0;
    let control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
    let controlvalue = control.value;
    for (let i = 0; i < controlvalue.length; i++) {
      this.total = Number(this.total) + Number(controlvalue[i].Quantity)
    }
    this.receivedQuantity = this.total;
    this.prForm.controls['ReceivedQuantity'].setValue(this.receivedQuantity);
    this.calculate15();
    this.prForm.controls['ReceivedQuantity'].setValue(this.total);
    this.prForm.controls['ReceivedQuantityAt15'].setValue(this.quantityissued15);
      this.prForm.controls['Variance'].setValue(this.variance);
      if(capacity != '') {
        if (Number(capacity) >= Number(event)) {
          this.tankalertmessage = '';
        } else {
          this.tankalertmessage = "The Entered Quantity is Greater than Tank Capacity!";
        }
      }
    /*if (this.total > this.loadedQty) {
      this.qtyErrorMsg = "Received Quantity is Greater than Loaded Quantity!"
    }*/
  }

  onPackChange(event: any, item) {
    this.nullalertmessage = "";
    this.prinitialvalue = event;
    this.totalReceivedQuantity = this.totalReceivedQuantity+Number(this.prinitialvalue);
    let capacity = item._value.Capacity;
    item.controls['Volume'].setValue(
      (item.controls['PackSize'].value === '' ? 0 : Number(item.controls['PackSize'].value))
      * (item.controls['NumberOfPackets'].value === '' ? 0 : Number(item.controls['NumberOfPackets'].value))
    );
    let control = (<FormArray>this.prForm.get('PurchaseReceiptLubeDetails')).controls;
    let totalPacks = 0;
    control.forEach(lubeDetails => {
      totalPacks = totalPacks + parseInt(lubeDetails.value['NumberOfPackets']);
    });
    let loadedQuantity = this.prForm.get('LoadedQuantity').value;
    if(totalPacks != loadedQuantity && this.IsLubricant) {
      this.lubeErrorMsg = 'Number of Packets should be equal to Loaded Quantity!'
    } else {
      this.lubeErrorMsg = "";
    }
  };

  onLoadedTextChange(event, section) {
    this.loadedQty = event;
    this.qtyZero = "";
    this.qtyErrorMsg = "";
    this.prForm.controls['LoadedQuantity'].setValue(event);
    /*if (this.receivedQuantity > this.loadedQty) {
      this.qtyErrorMsg = "Received Quantity is Greater than Loaded Quantity!"
    }*/
    if(this.IsLubricant) {
      let control = (<FormArray>this.prForm.get('PurchaseReceiptLubeDetails')).controls;
      let totalPacks = 0;
      control.forEach(lubeDetails => {
        totalPacks = totalPacks + parseInt(lubeDetails.value['NumberOfPackets']);
      });
      let loadedQuantity = this.prForm.get('LoadedQuantity').value;
      if(totalPacks != loadedQuantity && this.IsLubricant) {
        this.lubeErrorMsg = 'Number of Packets should be equal to Loaded Quantity!'
      } else {
        this.lubeErrorMsg = "";
      }
    }
  }

  onReceivedQuantityCalculation() {
    this.total = 0;
    let control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
    let contrlarray = [];
    contrlarray = control.value;
    for (let i = 0; i < contrlarray.length; i++) {
      this.total = Number(this.total) + Number(contrlarray[i].Quantity)
      this.item.push(contrlarray[i].Quantity);
    }
    this.prForm.controls['ReceivedQuantity'].setValue(this.total);
  }

  getPolDetails(event, section, index) {
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.lubeId = event;
    this.isAddedAllLubes = false;
    let isPresent = false;
    this.lubeErrorMsg = "";
    this.prList.forEach(lube=>{
      if(lube == event) {
        isPresent = true;
      }
    });
    if(isPresent) {
      this.idalertmessage = "Lubricant is already selected.Choose another one!";
      if(this.prList.length > index-1) {
        if(this.prList[index] == event) {
          this.idalertmessage = '';
        }
      }
    } else {
      if(this.prList.length+1 == this.pollist.length) {
        this.prList[index] = event;
      } else
      this.idalertmessage = '';
      this.prService.getPolDetailsById(this.lubeId).then(response => {
        section.controls['Package'].setValue(response.uom.name);
        section.controls['PackSize'].setValue(response.packSize);
       // item.controls['NumberOfPackets'].enable();
      });
    }
  }

  getSTDetails(event, section, index) {
    this.storagetanklist.forEach(tank => {
      if(tank.storageTankId == event) {
        this.selectedTankCapacity = tank.capacity;
      }
    });
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.tankArrayError = "";
    this.StId = event;
    this.isAddedAllTanks = false;
    let isPresent = false;
    this.stList.forEach(tank=>{
      if (tank == event) {
        isPresent = true;
      }
    });
    if (isPresent) {
      this.idalertmessage = "Tank is already selected.Choose another one!";
      this.stList[index] = event
      /*if (this.stList.length > index-1) {
        if (this.stList[index] == event) {
          this.idalertmessage = '';
        }
      }*/
    } else {
      this.stList[index] = event;
      if (this.stList.length+1 == this.storagetanklist.length) {
        this.stList[index] = event;
      } else
      this.idalertmessage = '';
      this.prService.getSTDetailsById(event, this.PId).then(response => {
        this.capacity = response.availableQuantity;
        section.controls['Capacity'].setValue(this.selectedTankCapacity);
        section.controls['Quantity'].enable();
      })
    }
  }

  addSection(i, item) {
    let itemValue = item.value;
    const control = (<FormGroup>(<FormArray>this.prForm.get('PurchaseReceiptDetails')).controls[i]).value;
    if (itemValue.StorageTankId == "" || itemValue.Quantity == "") {
      this.nullalertmessage = "Storage Details Can't be Empty!"
    } else if(this.idalertmessage == '') {
      this.nullalertmessage = "";
      if (this.stList.length == 0) {
        this.stList[i] = control['StorageTankId'];
        if (this.stList.length == this.storagetanklist.length) {
          this.isAddedAllTanks = true;
        } else {
          this.isAddedAllTanks = false;
        }
        if (!this.isAddedAllTanks) {
          const stControl = <FormArray>this.prForm.get('PurchaseReceiptDetails');
          stControl.push(this.initSection());
          this.showAddBtn[i] = true;
        }
      } else {
          let isTankPresent = false;
          this.stList.forEach(tank => {
            if (tank == this.StId) {
              isTankPresent = true;
            }
          });
          //if (!isTankPresent) {
            this.stList[i] = control['StorageTankId'];
            this.showAddBtn[i] = true;
            if (this.stList.length == this.storagetanklist.length) {
              this.isAddedAllTanks = true;
            } else {
              this.isAddedAllTanks = false;
            }
            if (!this.isAddedAllTanks) {
              const stControl = <FormArray>this.prForm.get('PurchaseReceiptDetails');
              stControl.push(this.initSection());
              this.showAddBtn[i] = true;
            }
            this.idalertmessage = '';
          /*} else {
            this.idalertmessage = 'Tank is already selected.Choose another one!';
            if (this.stList.length == this.storagetanklist.length) {
              this.idalertmessage = '';
            } else if (this.stList.length == i+1) {
              this.showAddBtn[i] = true;
              this.idalertmessage = '';
              const stList = <FormArray>this.prForm.get('PurchaseReceiptDetails');
              stList.push(this.initSection());
              this.showAddBtn[i] = true;
            }
          }*/
      }
    }
  }

  addLubeSection(item, i) {
    let itemValue = item.value;
    const control = (<FormGroup>(<FormArray>this.prForm.get('PurchaseReceiptLubeDetails')).controls[i]).value;
    if (itemValue.POLId == "" || itemValue.NumberOfPackets == "") {
      this.nullalertmessage = "Lubricant Details can't be Empty!"
    } else {
      this.nullalertmessage = "";
      if (this.prList.length == 0) {
        this.prList[i] = control['POLId'];
        if (this.prList.length == this.pollist.length) {
          this.isAddedAllLubes = true;
        } else {
          this.isAddedAllLubes = false;
        }
        if (!this.isAddedAllLubes) {
          const lubeControl = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
          lubeControl.push(this.initLubeSection());
          this.showAddLubeBtn[i] = true;
        }
      } else {
          let isLubePresent = false;
          this.prList.forEach(lube => {
            if (lube == this.lubeId) {
              isLubePresent = true;
            }
          });
          if (!isLubePresent) {
            this.prList[i] = control['POLId'];
            this.showAddLubeBtn[i] = true;
            if (this.prList.length == this.pollist.length) {
              this.isAddedAllLubes = true;
            } else {
              this.isAddedAllLubes = false;
            }
            if (!this.isAddedAllLubes) {
              const lubeControl = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
              lubeControl.push(this.initLubeSection());
              this.showAddLubeBtn[i] = true;
            }
            this.idalertmessage = '';
          } else {
            this.idalertmessage = 'Lubricant is already selected.Choose another one!';
            if(this.prList.length == this.pollist.length) {
              this.idalertmessage = '';
            } else if (this.prList.length == i+1) {
              this.showAddLubeBtn[i] = true;
              this.idalertmessage = '';
              const prList = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
              prList.push(this.initLubeSection());
              this.showAddLubeBtn[i] = true;
            }
          }
      }
    }
  }

  onLoadedQtyChange(event: any) {
    this.qtyErrorMsg = "";
    this.qtyZero = "";
    this.loadedQty = event;
    this.calculate15();
  };

  checkLoadedQty() {
    if (this.loadedQty >= this.printedvalue) {
      this.qtyErrorMsg = "";
    } else {
      this.qtyErrorMsg = "Received Quantity is Greater than Loaded Quantity!"
    }
  }

  getPurchaseReceiptDetails(form) {
    return form.controls.PurchaseReceiptDetails.controls;
  }

  getPurchaseReceiptLubeDetails(form) {
    return form.controls.PurchaseReceiptLubeDetails.controls;
  }

  removeSection(i, item) {
    const control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
    control.removeAt(i);
    this.onReceivedQuantityCalculation();
    this.printedvalue = this.totalReceivedQuantity;
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.tankArrayError="";
    this.tankalertmessage='';
    this.qtyErrorMsg="";
    this.StId = 1;
    this.initialvalue = 1;
    this.showAddBtn[control.length-1] = false;
    this.stList.splice(i, 1);
    if (this.stList.length == this.storagetanklist.length) {
      this.isAddedAllTanks = true;
    } else {
      this.isAddedAllTanks = false;
    }
    this.calculate15();
  }

  removeLubeSection(i) {
    const control = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
    control.removeAt(i);
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.tankalertmessage='';
    this.lubealertmessage="";
    this.lubeErrorMsg = "";
    this.showAddLubeBtn[control.length-1] = false;
    this.prList.splice(i, 1);
    if (this.prList.length == this.pollist.length) {
      this.isAddedAllLubes = true;
    } else {
      this.isAddedAllLubes = false;
    }
  }

  remove(i, j) {
    const control = <FormArray>this.prForm.get(['PurchaseReceiptDetails', i, 'questions', j, 'options']);
    control.removeAt(0);
    control.controls = [];
  }

  editSection(i, section) {
    section.controls['Quantity'].enable();
    section.controls['StorageTankId'].enable();
  }

  editLubeSection(i, section) {
    section.controls['NumberOfPackets'].enable();
  }

  onDateChanged(event: any, type: any) {
    if (type === 'LoadingDate') {
      this.LoadingDate = event.formatted;
      this.loadingcomparedate = event.jsdate;
    }
    if (type === 'ReceivingDate') {
      this.ReceivingDate = event.formatted;
      this.receivingcomparedate = event.jsdate;
    }
    if (type === 'BatchExpiryDate') {
      this.BatchExpiryDate = event.formatted;
      this.batchexpirycomparedate = event.jsdate;
    }

    if (this.loadingcomparedate > this.receivingcomparedate) {
      this.datealertloading = "Loading date should not be greater than the receiving date";
    } else {
      this.datealertloading = "";
    }
    if (this.batchexpirycomparedate > this.receivingcomparedate) {
      this.datealertreceiving = "Batch Expiry date should not be greater than the Receiving date";
    } else {
      this.datealertreceiving = "";
    }
  }

  clearerror() {
    this.error = "";
  }

  getPurchaseReceipts(params) {
    this.prService.getPurchaseReceiptList(params).then(r => {
      this.siteGroups = r.result;
      this.loading = false;
    })
  }

  ngOnChanges(change) {
    this.loading = this.page === 'add' ? false : true;
    if (change.pr && change.pr.currentValue) {
      this.loading = false;
      this.purchaseReceiptPrintData = change.pr.currentValue;
      this.PId = change.pr.currentValue.id;
      this.prForm.controls['IsLubricant'].setValue(change.pr.currentValue.isLubricant);
      this.prForm.controls['PRId'].setValue(change.pr.currentValue.id);
      if (change.pr.currentValue.grnNo != "null") {
        this.prForm.controls['GRNNo'].setValue(change.pr.currentValue.grnNo);
      }
      else {
        this.prForm.controls['GRNNo'].setValue("");
      }
      let loadingDate = new Date(change.pr.currentValue.loadingDate);
      this.loadingDateViewPage = loadingDate.getDate() + '-' + Number(loadingDate.getMonth()+1) + '-' + loadingDate.getFullYear();
      let receivingDate = new Date(change.pr.currentValue.receivingDate);
      this.receivingDateViewPage = receivingDate.getDate() + '-' + Number(receivingDate.getMonth()+1) + '-' + receivingDate.getFullYear();
      let expiryDate = new Date(change.pr.currentValue.batchExpiryDate);
      this.expiryDateViewPage = expiryDate.getDate() + '-' + Number(expiryDate.getMonth()+1) + '-' + expiryDate.getFullYear();
      this.oldSiteId=change.pr.currentValue.siteId;
      this.prForm.controls['Site'].setValue(change.pr.currentValue.siteId);
      this.prForm.controls['Batch'].setValue(change.pr.currentValue.batchNumber);
      this.prForm.controls['Temperature'].setValue(change.pr.currentValue.temprature);
      this.prForm.controls['Density'].setValue(change.pr.currentValue.density);
      this.prForm.controls['LoadedQuantity'].setValue(change.pr.currentValue.loadedQuantity);
      this.prForm.controls['PurchaseOrder'].setValue(change.pr.currentValue.purchaseOrderNo);
      this.prForm.controls['LaboratorySampleNo'].setValue(change.pr.currentValue.laboratorySampleNo);
      this.prForm.controls['TruckNo'].setValue(change.pr.currentValue.dischargeVehicleId);
      this.prForm.controls['Driver'].setValue(change.pr.currentValue.driverId);
      if(change.pr.currentValue.remarks!="null") {
        this.prForm.controls['Remarks'].setValue(change.pr.currentValue.remarks);
      } else {
        this.prForm.controls['Remarks'].setValue("");
      }
      this.prForm.controls['ReceivedQuantity'].setValue(change.pr.currentValue.receivedQuantity);
      this.prForm.controls['ReceivedQuantityAt15'].setValue(change.pr.currentValue.receivedQuantity15DegreeC);
      this.prForm.controls['Variance'].setValue(change.pr.currentValue.variance);
      this.prForm.controls['FileType'].setValue(this.filetype);
      this.lastvalue = change.pr.currentValue.receivedQuantity;
      this.loadedQty = change.pr.currentValue.loadedQuantity;
      this.receivedQuantity=this.total;
      if(change.pr.currentValue.isLubricant == true) {
        this.IsLubricant = true;
        this.getPolList();
        this.isLubricantSection = true;
        let lubDetails = change.pr.currentValue.purchaseReceiptLubricantDetails;
        let items = this.prForm.get('PurchaseReceiptLubeDetails') as FormArray;
        lubDetails.forEach(element => {
          items.push(this.initLubeSection(element));
        });
        items.removeAt(0);
      } else {
        this.prValue = change.pr.currentValue.purchaseReceiptStorageTankDetails;
        let items = this.prForm.get('PurchaseReceiptDetails') as FormArray;
        const control = <FormArray>this.prForm.get(['PurchaseReceiptDetails']);
        this.prValue.forEach(element => {
          items.push(this.initSection(element));
        });
        items.removeAt(0);
        this.onReceivedQuantityCalculation();
      }
      // this.getReceiverId(change.listview.currentValue.receiverId);
      if (change.pr.currentValue.siteId == null) {
        this.SiteId = 0;
      } else {
        this.SiteId = change.pr.currentValue.siteId;
      }
      if (change.pr.currentValue.polId == null) {
        this.POLId = 0
      } else {
        this.POLId = change.pr.currentValue.polId;
      }
      this.getPolListFromSite(change.pr.currentValue.siteId);
      this.prService.getPolListFromSiteById(change.pr.currentValue.siteId).then(response => {
        this.polbysitelist = response;
        this.prForm.controls['POL'].setValue(change.pr.currentValue.polId);
        this.getStorageTankListFromPol(change.pr.currentValue.polId);
      });
      if(change.pr.currentValue.dischargeVehicleId) {
        this.getCompanyDetails(change.pr.currentValue.dischargeVehicleId);
        this.prForm.controls['TruckOdometerReading'].setValue(change.pr.currentValue.truckOdoMeterReading);
      }
      if(change.pr.currentValue.driverId) {
        this.getDriverDetails(change.pr.currentValue.driverId);
      }
      if(this.page == 'edit') {
        const tankControls = (<FormArray>this.prForm.get('PurchaseReceiptDetails')).controls;
        tankControls.forEach(control => {
          control.get('StorageTankId').disable();
          control.get('Quantity').disable();
        });
      }
    } else {
      this.loading = false;
    }
  }

  onChange(event) {
    this.ErrorList = [];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for (let i = 0; i < this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if (fSize > 2097152) {
        this.ErrorList.push("The file " + this.filelist[i].name + " is too large. Maximum file size permitted is 2 Mb");
      } else if (extnsion != "pdf" && extnsion != "doc" && extnsion != "docx" && extnsion != "xls" && extnsion != "xlsx" && extnsion != "png" && extnsion != "jpg" && extnsion != "jpeg" && extnsion != "gif" && extnsion != "txt") {
        this.ErrorList.push("The selected file " + this.filelist[i].name + " is a an unsupported file");
      } else {
        this.ErrorList = [];
        this.filetype.push(this.filelist[i]);
        this.filelength = this.filetype.length;
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  getPolList() {
    this.prService.getpolList(this.IsLubricant).then(r => {
      this.pollist = r;
    })
      .catch(r => {
        this.handleError(r);
      });
  }

  getPolListCat() {
    this.prService.getpolListcat().then(r => {
      this.pollistcat = r.result;
    })
      .catch(r => {
        this.handleError(r);
      })
  }

  getDriverList() {
    this.prService.getDriverList().then(r => {
      this.driverList = r.result;
    })
      .catch(r => {
        this.handleError(r);
      })
  }

  getTruckList() {
    this.prService.getTruckList().then(r => {
      this.truckList = r.result;
    })
      .catch(r => {
        this.handleError(r);
      })
  }

  getDischargevehicleList() {
    this.prService.getDischargevehicleList().then(r => {
      this.dvList = r.result;
    })
      .catch(r => {
        this.handleError(r);
      })

  }

  getSiteList() {
      this.transferReceiptFormService.getSiteListByUser(this.userId).then(r => {
        this.siteList = r;
      })
      .catch(r => {
        this.handleError(r);
      });
  }

  getStorageTankList(id) {
    this.POLId = id;
    this.prService.getStorageTankList(this.POLId, this.SiteId).then(r => {
      this.tankList = r;
    })
      .catch(r => {
        this.handleError(r);
      })
  }

  getTankListFromSite(id) {
    this.SiteId = id;
  }

  getPolListFromSite(id) {
    this.SiteId = id;
    this.prForm.controls['POL'].setValue('');
    this.storagetanklist = [];
    this.prService.getPolListFromSiteById(id).then(response => {
      this.polbysitelist = response;
      if(this.page === 'add') {
        this.prForm.controls['LoadedQuantity'].setValue('');
        this.loadedQty=0;
          const control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
          let len = control.length;
          control.reset(
            new FormGroup({
              StorageTankId: new FormControl(0),
              Quantity: new FormControl(0),
              Capacity: new FormControl(0)
            })
          );
          for (let i = 0; i < len; i++) {
            control.removeAt(0);
          }
          this.stList=[];
          this.initialvalue=0;
          this.StId=0;
          this.total=0;
          this.receivedQuantity=0;
          this.prForm.controls['ReceivedQuantity'].setValue(this.total);
          if(control.length==0) {
            control.push(this.initSection());
          }
      }
      if(this.page === 'edit' && this.oldSiteId!=this.SiteId) {
        this.prForm.controls['LoadedQuantity'].setValue(0);
        this.loadedQty=0;
          const control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
          let len = control.length;
          control.reset(
            new FormGroup({
              StorageTankId: new FormControl(0),
              Quantity: new FormControl(0),
              Capacity: new FormControl(0)
            })
          );
          for (let i = 0; i < len; i++) {
            control.removeAt(0);
          }
          this.stList=[];
          this.initialvalue=0;
          this.StId=0;
          this.total=0;
          this.receivedQuantity=0;
          this.prForm.controls['ReceivedQuantity'].setValue(this.total);
          if(control.length==0)
          {
            control.push(this.initSection());
          }
      }
    })
    this.showAddBtn = [];
  }

  getLubeListFromSite(id) {
    this.SiteId = id;
    if(this.page === 'add'){
      const control = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
      let len = control.length;
      this.prForm.controls['LoadedQuantity'].setValue(0);
      this.loadedQty=0;
      control.reset(
        new FormGroup({
          POLId: new FormControl(0),
          Package: new FormControl(0),
          PackSize: new FormControl(0),
          NumberOfPackets: new FormControl(0),
          Volume: new FormControl(0),
        })
      );
      for (let i = 0; i < len; i++) {
        control.removeAt(0);
      }
      this.lubeList=[];
      this.lubeId=0;
      if(control.length==0)
      {
        control.push(this.initLubeSection());
      }
    }
    if(this.page === 'edit' && this.oldSiteId!=this.SiteId){
      this.prForm.controls['LoadedQuantity'].setValue(0);
      this.loadedQty=0;
      const control = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
      let len = control.length;
      control.reset(
        new FormGroup({
          POLId: new FormControl(0),
          Package: new FormControl(0),
          PackSize: new FormControl(0),
          NumberOfPackets: new FormControl(0),
          Volume: new FormControl(0),
        })
      );
      for (let i = 0; i < len; i++) {
        control.removeAt(0);
      }
      this.lubeList=[];
      this.lubeId=0;
      if(control.length==0)
      {
        control.push(this.initLubeSection());
      }
    }
  }

  getStorageTankListFromPol(id) {
    if (id == null) {
      id = 0;
    }
    this.POLId = id;
    this.prService.getStorageTankListFromSiteById(this.SiteId, this.POLId).then(response => {
      this.storagetanklist = response;
    });
    this.showAddBtn = [];
  }

  getDriverDetails(id) {
    this.prService.getDriverDetailsById(id).then(response => {
      this.prForm.controls['DriverMob'].setValue(response.telephoneNo);
    })
  }

  getCompanyDetails(id) {
    this.prForm.controls['TruckOdometerReading'].setValue('');
    this.truckmessage = '';
    this.prService.getCompanyDetailsById(id).then(response => {
      this.prForm.controls['companyName'].setValue(response.transportCompany.name);
    });
    this.prService.getTruckDetailsById(id).then(response => {
      this.previousodometerreading = response.odoMeterReading;
    });
  }

  onTruckOdometerChange(event) {
    this.truckmessage = '';
    this.odometerreading = event;
    if (Number(this.previousodometerreading) != 0) {
      if (Number(this.odometerreading) < Number(this.previousodometerreading)) {
        this.truckmessage = " Truck Odometer Reading must be greater than Previous Odometer Reading";
      }
      else {
        this.truckmessage = '';
      }
    } else {
      this.truckmessage = '';
    }
  }

  clearmsgs() {
    this.isLubricantSection = false;
    this.QuantityMessage = "";
    this.batchReq = "";
    this.siteReq = "";
    this.qtyReq = "";
    this.polReq = "";
    this.detailsmessage="";
    this.densityMessage="";
    this.temperatureMessage="";
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.qtyErrorMsg = "";
    this.stList = [];
    this.lubeList = [];
    this.receivedQuantity = 0;
    this.printedvalue = 0;
    this.totalReceivedQuantity = 0;
    this.initialvalue = 0;
    this.finalvalue = 0;
    this.error = "";
    this.datealert = '';
    this.datealertloading = '';
    this.ErrorList = [];
    this.idalertmessage = "";
    this.lubealertmessage = "";
    this.qtyErrorMsg = "";
    this.tankalertmessage="";
    this.nullalertmessage="";
    this.storagetankreqalert = "";
    this.datealertreceiving="";
    this.variancealert='';
    this.qtyZero='';
    this.tempReq='';
    this.densityReq='';
    this.isAddedAllTanks = false;
    this.isAddedAllLubes = false;
    this.showAddBtn = [];
    this.showAddLubeBtn = [];
    this.lubeErrorMsg = "";
    this.storagetanklist = [];
    this.batchIssueSlipErrorMsg = '';
    const control = <FormArray>this.prForm.get('PurchaseReceiptDetails');
    for (let i = 1; i <= control.length; i++) {
      control.removeAt(i);
    }
    const lubcontrol = <FormArray>this.prForm.get('PurchaseReceiptLubeDetails');
    for (let i = 1; i <= lubcontrol.length; i++) {
      lubcontrol.removeAt(i);
    }
    this.prForm = new FormGroup({
      IsLubricant: new FormControl(''),
      PRId: new FormControl(''),
      GRNNo: new FormControl(''),
      LoadingDate: new FormControl(''),
      Site: new FormControl('', Validators.required),
      ReceivingDate: new FormControl(''),
      Batch: new FormControl('', [Validators.required]),
      Temperature: new FormControl(0),
      BatchExpiryDate: new FormControl(''),
      Density: new FormControl(0),
      LoadedQuantity: new FormControl('', [Validators.required]),
      PurchaseOrder: new FormControl(''),
      POL: new FormControl('', [Validators.required]),
      LaboratorySampleNo: new FormControl(''),
      TruckNo: new FormControl(''),
      companyName: new FormControl(''),
      TruckOdometerReading: new FormControl(''),
      Driver: new FormControl(''),
      DriverMob: new FormControl(''),
      Remarks: new FormControl(''),
      ReceivedQuantity: new FormControl(0),
      ReceivedQuantityAt15: new FormControl(0),
      Variance: new FormControl(0),
      FileType: new FormControl(''),
      PurchaseReceiptDetails: new FormArray([
        this.initSection([]),
      ]),
      PurchaseReceiptLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),
    });
    let LoadingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    let ReceivingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    let BatchExpiryDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.prForm.controls['LoadingDate'].setValue(LoadingDate);
    this.prForm.controls['ReceivingDate'].setValue(ReceivingDate);
    this.prForm.controls['BatchExpiryDate'].setValue(BatchExpiryDate);
  }


  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    // const pattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    let inputChar = String.fromCharCode(event.charCode);
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      return false;
      //event.preventDefault();
    }
    var number=event.target.value;
    var wholePart = Math.floor(number);
    var wholePartLength = wholePart.toString().length;
    var fractionPart = number % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    if(noOfDigits >= 4){
        return false;
    } else {
        return true;
    }
  }

  _keyTempPress(event: any) {
    const pattern = /(\.[0]{1,4})?$/;
    // const pattern =/^(?!0\d|$)\d*(\.\d{1,4})?$/g
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
    var number=event.target.value;
    var wholePart = Math.floor(number);
    var wholePartLength = wholePart.toString().length;
    var fractionPart = number % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    if(noOfDigits >= 4){
        return false;
    } else {
        return true;
    }
  }

  onTemperatureChange(value) {
    this.tempReq = "";
    var number = value;
    let num = Number(number);
    if(num > 50 || (number == '.') || number == '') {
      this.temperatureMessage = "Temperature should be between 0 and 50";
    } else {
      this.temperatureMessage = "";
      this.Temperature = value;
      this.calculate15();
    }
  }

  onDensityChange(value) {
    this.densityReq = "";
    this.densityMessage ="";
    var number = value;
    let num = Number(number);
    if((num < 0.670)  || (num > 1.06) || (number == '.') || number == ''  ){
      this.densityMessage = "Density should be between 0.670 and 1.06";
    } else {
      this.densityMessage = "";
      this.Density = value;
    }
    this.calculate15();
  }

  calculate15() {
    this.prService.getCalculateQuantityAt15DegreeCelsius(this.Density.toString(),this.Temperature.toString(),this.total).then(response => {
      this.quantityissued15 = response.quantityAt15Degree;
      this.variance=this.loadedQty-this.total;
      this.prForm.controls['ReceivedQuantityAt15'].setValue(this.quantityissued15);
      this.prForm.controls['Variance'].setValue(this.variance);
    })
  }


  onSubmit(validPost) {
      /*if(Number(validPost.Variance) <0) {
        this.variancealert="Variance shouldn't be negative, please check loaded quantity";
      } else {
        this.variancealert=""
      }*/
      if(validPost.IsLubricant == false) {
        this.calculate15();
        validPost.ReceivedQuantity = this.receivedQuantity;
        validPost.ReceivedQuantity15DegreeC = this.quantityissued15;
        validPost.Variance = this.variance;
        for(let i=0; i<validPost.PurchaseReceiptDetails.length;i++) {
          if (validPost.PurchaseReceiptDetails[i].StorageTankId == "") {
            this.storagetankreqalert = 'StorageTank Details Required'
          } else {
            this.storagetankreqalert = '';
          }
        }
        this.qtyErrorMsg='';
      }
      if (this.IsLubricant == true) {
        this.prForm.get('Batch').setErrors(null);
        this.prForm.get('POL').setErrors(null);
        for(let i=0; i<validPost.PurchaseReceiptLubeDetails.length;i++) {
          if (validPost.PurchaseReceiptLubeDetails[i].POLId == "") {
            this.storagetankreqalert = 'Product Details Required';
          } else {
            this.storagetankreqalert = '';
          }
        }
      }

      /*if (this.receivedQuantity > this.loadedQty) {
        this.qtyErrorMsg = "Received Quantity is Greater than Loaded Quantity!"
      } else {
        this.qtyErrorMsg ="";
      }*/
      if(validPost.LoadedQuantity <=0) {
        this.qtyZero ="Please provide proper loaded quantity";
      } else {
        this.qtyZero ="";
      }
      if(validPost.Temperature <=0 && validPost.IsLubricant == false) {
        this.tempReq ="Temperature is Required";
      } else {
        this.tempReq ="";
      }
      if(validPost.Density <=0 && validPost.IsLubricant == false) {
        this.densityReq ="Density is Required";
      } else {
        this.densityReq ="";
      }
      let control = (<FormArray>this.prForm.get('PurchaseReceiptLubeDetails')).controls;
      let totalPacks = 0;
      control.forEach(lubeDetails => {
        totalPacks = totalPacks + parseInt(lubeDetails.value['NumberOfPackets']);
      });
      let loadedQuantity = this.prForm.get('LoadedQuantity').value;
      if(totalPacks != loadedQuantity && validPost.IsLubricant) {
        this.lubeErrorMsg = 'Number of Packets should be equal to Loaded Quantity!'
      } else {
        this.lubeErrorMsg = "";
      }
      this.clearerror();
      this.formSubmited = true;
      this.siteReq = "Site is required";
      this.batchReq = "Batch is required";
      this.qtyReq = "Loaded Quantity is required";
      this.polReq = "POL is required";
      if (this.prForm.valid && this.ErrorList.length == 0 && this.datealert == '' && this.truckmessage == ''
      && this.tankalertmessage == '' && this.idalertmessage == '' && this.QuantityMessage == '' && this.densityMessage==""
      && this.temperatureMessage=="" && this.variancealert == '' && this.storagetankreqalert == '' && this.qtyZero ==""
      && this.qtyErrorMsg =="" && this.tempReq == "" && this.densityReq == "" && this.lubeErrorMsg == "" ) {
        validPost.uploadedFile = this.filetype;
        validPost.IsLubricant = this.IsLubricant;

        this.saved.emit(validPost);
        // this.savebutton = true;
      } else {
        jQuery('form').find(':input.ng-invalid:first').focus();
      }
  }

  clearGrnErrorMsg(event) {
    if(event.keyCode == 8) {
      this.error ='';
    }
  }

  companyList: any;
  getCompanyList() {
    this.prService.getCompanyList().then(r => {
      this.companyList = r.result;
    })
    .catch(r => {
      this.handleError(r);
    })
  }

  onSaveTruck(truck) {
    this.regNoReq="Registration Number is required";
    this.companyReq="Company Name is Required";
    this.truckFormSubmited = true;
    if(this.truckForm.valid) {
      truck.truckId=0;
      let formData = new FormData();
      formData.append('dischargeVehicleId', '0');
      formData.append('name', truck.regNo);
      formData.append('companyId', truck.companyId);
      formData.append('address', truck.Address);
      formData.append('isDeleted', "");
      this.prService.saveDischargeVehicle(formData).then(r =>  {
        jQuery('.modal-backdrop').remove();
        this.modalclose('truck');
        this.getDischargevehicleList();
        setTimeout(function () {
          jQuery('#AddTruck').modal('toggle');
        }.bind(this), 0);
      }).catch(r =>  {
        this.truckError = r;
      })
    }
  }

  clearRegNoError() {
    this.truckError = '';
  }

  onSaveDriver(driver) {
    this.driverReq = "Driver Name is required";
    this.licenceReq = "Licence No: is required";
    this.transportCompanyReq = "Transport Company Name is required";
    this.buReq = "Busines Unit is required";
    this.driverFormSubmited = true;
    if(this.driverForm.valid) {
      driver.driverId = 0;
      let files = driver.uploadedFile;
      let formData = new FormData();
      formData.append('driverId','0');
      formData.append('DriverName', driver.driverName);
      formData.append('BUId', driver.businessunitnameId);
      formData.append('TelephoneNo', driver.telephoneNo);
      formData.append('DrivingLicenceNo', driver.drivingLicenceNo);
      formData.append('companyId', driver.companyId);
      formData.append('Remarks', driver.remarks);
      formData.append('isDeleted', driver.isDeleted);
      this.prService.saveDriver(formData).then(r =>  {
        jQuery('.modal-backdrop').remove();
        this.modalclose('driver');
        this.getDriverList();
        setTimeout(function () {
          jQuery('#AddDriver').modal('toggle');
        }.bind(this), 0);
      }).catch(r =>  {
        this.handleError(r);
      })
    }
  }

  buList: any;
  getBUList() {
    this.prService.getBUList().then(r => {
      this.buList = r.result;
    })
    .catch(r => {
       this.handleError(r);
    })
  }

  modalclose(item) {
    if (item == 'truck') {
      this.truckFormSubmited = false;
      this.truckForm = this.formBuilder.group({
        'truckId': [''],
        'regNo':['', Validators.required],
        'companyId':['', Validators.required],
        'Address':[''],
      });
    }
    if (item == 'driver') {
      this.driverFormSubmited = false;
      this.driverForm = this.formBuilder.group({
        'driverId': [''],
        'driverName': ['', Validators.compose([Validators.required])],
        'telephoneNo': [''],
        'drivingLicenceNo': [''],
        'businessunitnameId':['',Validators.compose([Validators.required])],
        'IsInternalUse' : [''],
        'companyId': ['', Validators.compose([Validators.required])],
        'remarks':[''],
        'FileType':[''],
      });
    }
  }

  purchaseReceiptPrintData = {};
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html lang="en">
      <head>
      <script src='https://printjs-4de6.kxcdn.com/print.min.js'></script>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
      <style></style>
      </head>
      <body >${printContents}</body>
    </html>`);
    popupWin.document.close();
  }

  batchIssueSlipErrorMsg = '';
  onBatchChange(event: any) {
      this.prService.checkBatchNumber(event.target.value).then(response => {
        this.batchIssueSlipErrorMsg = response;
      });
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}