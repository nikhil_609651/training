import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PurchaseReceiptFormService } from 'app/admin/receiptandtransfer/purchase-receipt/service/purchasereceipt-form.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'purchasereceipt-edit',
  template: require('./purchasereceipt-edit.html')
})
export class PurchaseReceiptEditComponent implements OnInit{
  lubeDetails: any;
  TankDetails: any;
  SiteIncharge: any;
  IsLubricant: any;
  id: any;

  public error = {};
  public success = '';
  public pr: any;
  public page = 'edit';
  viewPRForm: any;


	constructor(private router: Router, private route: ActivatedRoute,
    private prService: PurchaseReceiptFormService,private fb: FormBuilder,) {

      this.viewPRForm = fb.group({

                    'GRNNo': [''],
                    'LoadingDate': [''],
                    'Site': [''],
                    'ReceivingDate': [''],
                    'Batch': [''],
                    'Temperature': [''],
                    'BatchExpiryDate': [''],
                    'Density': [''],
                    'LoadedQuantity': [''],
                    'PurchaseOrder': [''],
                    'POL': [''],
                    'LaboratorySampleNo': [''],
                    'TruckNo': [''],
                    'CompanyName': [''],
                    'TruckOdometerReading': [''],
                    'Driver': [''],
                    'DriverMobileNo': [''],
                    'ReceivedQuantity': [''],
                    'ReceivedQuantity15C': [''],
                    'Varience': [''],
                    'Remarks': [''],
                    'SiteIncharge': [''],
                    'StorageTank':[''],
                    'Quantity':['']
                });
                var vForm = this.viewPRForm;

  }
  
	ngOnInit() {
      this.route.params.forEach((params: Params) => {

        this.id = params['id'];
        this.pr = this.prService.getPurchaseReceipt(this.id);
      });
  }

  onSave(pr: any) {
    let files = pr.uploadedFile;
    let PurchaseReceiptStorageTankDetails=pr.PurchaseReceiptDetails;
    let PurchaseReceiptLubricantDetails=pr.PurchaseReceiptLubricantDetails;
    let prDetails;
    let prLubeDetails;
    let formData = new FormData();
    let dischargeVehicleId = pr.TruckNo ? pr.TruckNo : 0;
    if(PurchaseReceiptStorageTankDetails!=undefined)
    {
       prDetails =  JSON.stringify(PurchaseReceiptStorageTankDetails);
    }
    else
    {
       prDetails=null;
    }
    if(PurchaseReceiptLubricantDetails!=undefined)
    {
       prLubeDetails =  JSON.stringify(PurchaseReceiptLubricantDetails);
    }
    else
    {
       prLubeDetails=null;
    }
    let loadingdate = pr.LoadingDate;
    let receivingdate=pr.ReceivingDate;
    let batchexpdate=pr.BatchExpiryDate;

    if(typeof loadingdate == 'object'){
      loadingdate=loadingdate.date.day + '/' + loadingdate.date.month + '/' + loadingdate.date.year;

    } else {
      loadingdate= pr.LoadingDate;
    }

    if(typeof receivingdate == 'object'){
      receivingdate=receivingdate.date.day + '/' + receivingdate.date.month + '/' + receivingdate.date.year;

    } else {
      receivingdate= pr.ReceivingDate;
    }

    if(typeof batchexpdate == 'object'){
      batchexpdate=batchexpdate.date.day + '/' + batchexpdate.date.month + '/' + batchexpdate.date.year;

    } else {
      batchexpdate= pr.BatchExpiryDate;
    }
    for(let i =0; i < files.length; i++) {
        formData.append("FileType", files[i], files[i]['name']);
    }
    formData.append('Id', pr.PRId);
    formData.append('IsLubricant', pr.IsLubricant);
    formData.append('GRNNo', pr.GRNNo);
    formData.append('LoadingDate', loadingdate);
    formData.append('SiteId', pr.Site);
    formData.append('ReceivingDate', receivingdate);
    formData.append('BatchNumber', pr.Batch);
    formData.append('Temperature', pr.Temperature);
    formData.append('BatchExpiryDate',batchexpdate);
    formData.append('Density', pr.Density);
    formData.append('LoadedQuantity', pr.LoadedQuantity);
    formData.append('PurchaseOrderNo', pr.PurchaseOrder);
    formData.append('POLId', pr.POL);
    formData.append('LaboratorySampleNo', pr.LaboratorySampleNo);
    formData.append('DischargeVehicleId', dischargeVehicleId);
    formData.append('TruckOdoMeterReading', pr.TruckOdometerReading);
    formData.append('DriverId', pr.Driver);
    formData.append('Variance', pr.Variance);
    formData.append('ReceivedQuantity', pr.ReceivedQuantity);
    formData.append('ReceivedQuantity15DegreeC', pr.ReceivedQuantity15DegreeC);
    formData.append('PurchaseReceiptStorageTankDetails', prDetails);
    formData.append('PurchaseReceiptLubricantDetails', prLubeDetails);
    formData.append('Remarks', pr.Remarks);
    formData.append('isDeleted',"");
    formData.append('fileURL',"");
    this.prService.Save(formData).then(r =>  {
      this.success = 'Purchase Receipt Updated Successfully!';
      jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
      setTimeout(function() {
         this.success = '';
         this.router.navigate(['./home/receipt-and-transfer/purchase-receipt']);
      }.bind(this), 2000);
    }).catch(r =>  {
      this.handleError(r);
    })
  }

  private handleError(e: any) {
  	this.error = e;
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}
