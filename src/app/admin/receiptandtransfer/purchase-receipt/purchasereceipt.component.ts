import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'purchase-receipt',
    template: `<router-outlet></router-outlet>`
})
export class PurchaseReceiptComponent {
	constructor(private router: Router) {
	}
}
