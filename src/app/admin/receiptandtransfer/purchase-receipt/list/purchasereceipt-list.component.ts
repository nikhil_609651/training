import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { Configuration } from 'app/app.constants';
import { FormBuilder } from '@angular/forms';
import { PurchaseReceiptFormService } from 'app/admin/receiptandtransfer/purchase-receipt/service/purchasereceipt-form.service';
import { IMyOptions } from 'mydatepicker';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
@Component({
  selector: 'purchasereceipt-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./purchasereceipt-list.html')
  // styleUrls: ['./purchasereceipt-list.css']
})
export class PurchaseReceiptListComponent implements OnInit {

  lubeDetails: any;
  IsLubricant: boolean;
  Export: string;
  selectedapproverAll: any;
  selectedstatusAll: any;
  selectedsiteAll: any;
  selectedPOLAll: any;
  API_URL_Export: string;
  exportmeassage: string;
  success: string;
  approverGroups: any;
  statusGroups: any;
  TankList: string;
  TankDetails: any;
  siteGroups: any;
  polGroups: any;
  selectedAll: any;
  checked: string[] = [];
  SiteIncharge: any;
  Longitude: any;
  Lattitude: any;
  LocalReserve: any;
  BusinessUnit: any;
  siteName: any;
  viewPRForm: any;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  attachmentmessage: string;
  sId: any;
  deleteattachmentmessage: string;
  attachment_deleted_id: any;
  private currentdate = new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  private initialSearchFromDate: Object = { date: { day: 1, month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialSearchToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  fromDate = new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
  toDate = new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
  public purchasereceipt: any = [];
  error: any;
  public start: number = 1;
  public loading: boolean;
  public rows: Array<any> = [];
  public columns: Array<any> = [
    { title: 'Sl.No', name: 'id', sort: false },
    { title: 'Date', name: 'LoadingDate', sort: true, filter: true },
    { title: 'Site', name: 'Site', sort: true, filter: true },
    { title: 'POL', name: 'POL', sort: true, filter:true },
    { title: 'BatchNumber', name: 'BatchNumber', sort: true, filter:true },
    { title: 'Loaded Quantity', name: 'Quantity', sort: true },
    { title: 'Received Quantity', name: 'ReceivedQuantity', sort: true },
    { title: 'Temperature', name: 'Temperature', sort: false },
    { title: 'Density', name: 'Density', sort: false },
    { title: 'Purchase Order', name: 'PurchaseOrder', sort: false },
    { title: 'Laboratory Sample No', name: 'LaboratorySampleNo', sort: false },
    { title: 'Truck No', name: 'TruckNo', sort: false },
    { title: 'Driver', name: 'Driver', sort: false },
    { title: 'Status', name: 'Status', sort: false, filter: true },
    { title: 'Approver', name: 'ApprovedBy', sort: true, filter: true },
    { title: 'Actions', className: ['text-center'], name: 'actions', sort: false }
  ];
  public totalfeilds = 0;
  public page: number = 1;
  public itemsPerPage: number = 3;
  public maxSize: number = 5;
  public numPages: number = 2;
  public length: number = 5;
  public next = '';
  public pr_deleted_id = '';
  public deletemessage = '';
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-bordered']
  };
  params: URLSearchParams = new URLSearchParams();
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  private searchFromDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };
  private searchToDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };
  constructor(private router: Router,
    private fb: FormBuilder, private _sharedService : SharedService,
    private prService: PurchaseReceiptFormService,
    private configuration: Configuration) {
    
      /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	    this.privileges = privileges;
    });
    if ((new Date().getDate() < 10) && (new Date().getMonth() > 10) ) {
      this.toDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    if ((new Date().getDate() < 10) && (new Date().getMonth() < 10) ) {
      this.toDate = '0' + new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    if ((new Date().getDate() > 10) && (new Date().getMonth() < 10) ) {
      this.toDate =  new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
      this.fromDate =  new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
    }

    this.itemsPerPage = configuration.itemsPerPage;
    this.params.set('limit', configuration.itemsPerPage.toString());
    this.params.set('SiteFilter', '');
    this.params.set('StatusFilter', '');
    this.params.set('ApproverFilter', '');
    this.params.set('POLFilter', '');
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    let searchToDate = '';
    if(Number(new Date().getMonth()+1) < 10 && new Date().getDate() < 10) {
      searchToDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if(new Date().getDate() < 10 && Number(new Date().getMonth()+1) >= 10) {
      searchToDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if(new Date().getDate() >= 10 && Number(new Date().getMonth()+1) < 10) {
      searchToDate = new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else {
      searchToDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    }
    let searchFromDate = '';
    if(Number(new Date().getMonth()+1) < 10) {
      searchFromDate = '01' + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else if( Number(new Date().getMonth()+1) >= 10) {
      searchFromDate = '01' + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    } else {
      searchFromDate = '01/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    }
    this.params.set('FromDate', searchFromDate);
    this.params.set('ToDate', searchToDate);
    this.rows = configuration.rows;
    this.viewPRForm = fb.group({

      'GRNNo': [''],
      'LoadingDate': [''],
      'Site': [''],
      'ReceivingDate': [''],
      'Batch': [''],
      'Temperature': [''],
      'BatchExpiryDate': [''],
      'Density': [''],
      'LoadedQuantity': [''],
      'PurchaseOrder': [''],
      'POL': [''],
      'LaboratorySampleNo': [''],
      'TruckNo': [''],
      'CompanyName': [''],
      'TruckOdometerReading': [''],
      'Driver': [''],
      'DriverMobileNo': [''],
      'ReceivedQuantity': [''],
      'ReceivedQuantity15C': [''],
      'Varience': [''],
      'Remarks': [''],
      'SiteIncharge': [''],
      'StorageTank': [''],
      'Quantity': ['']
    });
    var vForm = this.viewPRForm;
  }

  ngOnInit() {
    let userId = localStorage.getItem('user_nameId') ;
    this.API_URL_Export = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/ExportPurchaseReceiptFiles?FromDate=' + this.fromDate + '&ToDate=' + this.toDate + '&UserId=' + userId;
    this.getPurchaseReceiptList(this.params);
    this.totalfeilds = this.columns.length;
    this.getPOL();
    this.getSite(userId);
    this.getStatus();
    this.getApprover();
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  getPurchaseReceiptList(params: any) {
    this.loading = true;
    this.prService.getPurchaseReceiptList(params).then(response => {
      this.purchasereceipt = response['result'];
      if (this.purchasereceipt.length > 0) {
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false;
      }
      else {
        this.page = 1;
        this.getAllLists(this.params);
      }
    }).catch(r => {
      this.handleError(r);
    });
  }
  getSite(userId) {
    this.prService.getFilterSiteList(userId).then(r => {
      this.siteGroups = r;
      this.loading = false;
    });
  }
  getStatus() {
    this.prService.getApprovedStatusList().then(r => {
      this.statusGroups = r;
      this.loading = false;
    });
  }
  getApprover() {
    this.prService.getApproverList().then(r => {
      this.approverGroups = r;
      this.loading = false;
    });
  }
  getPOL() {
    this.prService.getpolListcat().then(r => {
      this.polGroups = r.result;
      this.loading = false;
    });
  }
  onSelectChange(event) {
    let changedValue = parseInt(event.target.value);
    if (this.next || (changedValue < this.itemsPerPage)) {
      this.itemsPerPage = event.target.value;
      let params = this.params;
      params.set('limit', event.target.value);
      this.getPurchaseReceiptList(params);
    }
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset', start.toString());

    var sortParam = '';

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }

    this.getPurchaseReceiptList(this.params);
  }
  view(data) {
    this.IsLubricant = data.isLubricant;
    this.SiteIncharge = data.users1.firstName;
    if (data.grnNo != "null") {
      this.viewPRForm.controls['GRNNo'].setValue(data.grnNo);
    }
    else {
      this.viewPRForm.controls['GRNNo'].setValue("");
    }
    this.viewPRForm.controls['LoadingDate'].setValue(data.loadDate);
    this.viewPRForm.controls['Site'].setValue(data.site.name);
    this.viewPRForm.controls['ReceivingDate'].setValue(data.receiveDate);
    if (data.isLubricant == false) {
      this.viewPRForm.controls['Batch'].setValue(data.batchNumber);
      this.viewPRForm.controls['Temperature'].setValue(data.temprature);
      this.viewPRForm.controls['Density'].setValue(data.density);
      this.viewPRForm.controls['POL'].setValue(data.pol.name);
    }
    this.viewPRForm.controls['BatchExpiryDate'].setValue(data.batchExpiry);
    this.viewPRForm.controls['LoadedQuantity'].setValue(data.loadedQuantity);
    this.viewPRForm.controls['PurchaseOrder'].setValue(data.purchaseOrderNo);
    this.viewPRForm.controls['LaboratorySampleNo'].setValue(data.laboratorySampleNo);
    this.viewPRForm.controls['TruckNo'].setValue(data.dischargeVehicle.name);
    this.viewPRForm.controls['TruckOdometerReading'].setValue(data.truckOdoMeterReading);
    this.viewPRForm.controls['Driver'].setValue(data.driver.driverName);
    this.viewPRForm.controls['SiteIncharge'].setValue(this.SiteIncharge);

    if (data.purchaseReceiptStorageTankDetails.length > 0) {
      this.TankDetails = data.purchaseReceiptStorageTankDetails;
    }

    if (data.purchaseReceiptLubricantDetails.length > 0) {
      this.lubeDetails = data.purchaseReceiptLubricantDetails;
    }

  }
  addpurchasereceipt() {
    this.router.navigate(['./home/receipt-and-transfer/purchase-receipt/add']);
  }

  editpurchasereceipt(id) {

    this.router.navigate(['./home/receipt-and-transfer/purchase-receipt/view/', id]);
  }
  deletepr(pr) {

    this.pr_deleted_id = pr;
  }
  deletemessageclear() {
    this.deletemessage = '';
  }

  deleteprconfirm(pr) {
    this.pr_deleted_id = pr.id;
    this.prService.deletePurchaseReceipt(this.pr_deleted_id).then(r => {
      this.getPurchaseReceiptList(this.params);
      this.deletemessage = "Purchase And Receipt Deleted Successfully";

    }).catch(r => {
      this.handleError(r);
      this.deletemessage = r.name[0];
    })
  }

  // Filteration

  selectAll(item, event) {
    if (item === 'Site') {
      this.checkArr = [];
      for (var i = 0; i < this.siteGroups.length; i++) {
        this.siteGroups[i].selected = this.selectedsiteAll;
        if (event.target.checked) {
          this.checkArr.push(this.siteGroups[i].siteId.toString());
        } else {
          this.checkArr = [];
        }
      }
      this.params.set('SiteFilter', this.checkArr.toString());
    }
    if (item === 'POL') {
      this.checkPOLArr = [];
      for (var i = 0; i < this.polGroups.length; i++) {
        this.polGroups[i].selected = this.selectedPOLAll;
        if (event.target.checked) {
          this.checkPOLArr.push(this.polGroups[i].id.toString());
        } else {
          this.checkPOLArr = [];
        }
      }
      this.params.set('POLFilter', this.checkPOLArr.toString());
    }
    if (item === 'Status') {
      this.checkStatusArr = []
      for (var i = 0; i < this.statusGroups.length; i++) {
        this.statusGroups[i].selected = this.selectedstatusAll;
        if (event.target.checked) {
          this.checkStatusArr.push(this.statusGroups[i].id.toString());
        } else {
          this.checkStatusArr = [];
        }
      }
      this.params.set('StatusFilter', this.checkStatusArr.toString());
    }
    if (item === 'Approver') {
      this.checkApproverArr = []
      for (var i = 0; i < this.approverGroups.length; i++) {
        this.approverGroups[i].selected = this.selectedapproverAll;
        if (event.target.checked) {
          this.checkApproverArr.push(this.approverGroups[i].id.toString());
        } else {
          this.checkApproverArr = [];
        }
      }
      this.params.set('ApproverFilter', this.checkApproverArr.toString());
    }
  }
  checkArr = [];
  checkStatusArr = [];
  checkPOLArr = [];
  checkApproverArr = [];
  checkIfAllSelected(option, event, item) {
    if (item == 'Site') {
      this.selectedsiteAll = this.siteGroups.every(function (item: any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkArr.indexOf(key);
      if (event.target.checked) {
        this.checkArr.push(event.target.value);
      } else {
        this.checkArr.splice(index, 1);
      }
      this.params.set('SiteFilter', this.checkArr.toString());
    }

    if (item == 'POL') {
      this.selectedPOLAll = this.polGroups.every(function (item: any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkPOLArr.indexOf(key);
      if (event.target.checked) {
        this.checkPOLArr.push(event.target.value);
      } else {
        this.checkPOLArr.splice(index, 1);
      }
      this.params.set('POLFilter', this.checkPOLArr.toString());
    }

    if (item === 'Status') {
      this.selectedstatusAll = this.statusGroups.every(function (item: any) {
        return item.selected == true;
      })

      var key = event.target.value.toString();
      var index = this.checkStatusArr.indexOf(key);
      if (event.target.checked) {
        this.checkStatusArr.push(event.target.value);
      } else {
        this.checkStatusArr.splice(index, 1);
      }
      this.params.set('StatusFilter', this.checkStatusArr.toString());
    }

    if (item === 'Approver') {
      this.selectedapproverAll = this.approverGroups.every(function (item: any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkApproverArr.indexOf(key);
      if (event.target.checked) {
        this.checkApproverArr.push(event.target.value);
      } else {
        this.checkApproverArr.splice(index, 1);
      }
      this.params.set('ApproverFilter', this.checkApproverArr.toString());
    }

  }
  // For Applying Filtering to List
  apply(item) {
    this.getPurchaseReceiptList(this.params);
    if (item === 'Site') {

      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this), 0);
    }
    if (item === 'POL') {

      setTimeout(function () {
        jQuery('#ViewPOLModal').modal('toggle');
      }.bind(this), 0);
    }
    if (item === 'Status') {
      setTimeout(function () {
        jQuery('#ViewStatusModal').modal('toggle');
      }.bind(this), 0);
    }

    if (item === 'Approver') {

      setTimeout(function () {
        jQuery('#ViewApproverModal').modal('toggle');
      }.bind(this), 0);
    }
    jQuery('ViewSiteModal').modal('hide');
    jQuery('ViewStatusModal').modal('hide');
    jQuery('ViewApproverModal').modal('hide');
    jQuery('ViewPOLModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();

  }
  // Filteration

  // For File Attachments
  attachment(id) {
    this.sId = id;
    this.attachmentmessage = '';
    this.API_URL = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/DownloadPurchaseReceiptFiles?FileId=';
    this.prService.getallPurchaseReceiptattachments(id).then(response => {
      this.attachments = response;
      if (this.attachments.length > 0) {
        var LongArray = [];
        for (var i = 0; i < this.attachments.length; i++) {
          let ext = this.attachments[i].location;
          var Obj = {
            id: this.attachments[i].id,
            referenceId: this.attachments[i].referenceId,
            shortFileName: this.attachments[i].shortFileName,
            fileName: this.attachments[i].fileName,
            createdDate: this.attachments[i].createdDate,
            ext: ext.substr(ext.lastIndexOf('.') + 1),

          };
          LongArray.push(Obj);
          this.attachmentGroup = LongArray;
        }
      }
      else {
        this.getPurchaseReceiptList(this.params);
        this.attachmentmessage = 'No Attachments Found'
        setTimeout(function () {
          jQuery('#ViewAttachmentModal').modal('toggle');
        }.bind(this), 1000);
      }
      return this.attachmentGroup;
    })

  }
  viewclose() {
    setTimeout(function () {
      jQuery('#ViewPurchaseReceipt').modal('toggle');
    }.bind(this), 0);
  }
  viewcloseattachment() {
    setTimeout(function () {
      jQuery('#ViewAttachmentModal').modal('toggle');
    }.bind(this), 0);
  }
  // For Deleting Attachments

  deleteattachment(id) {
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear() {
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id) {
    //alert(deleted);
    // if (confirm) {

    this.prService.deleteattachment(this.attachment_deleted_id).then(r => {
      this.attachment(this.sId);
      this.deleteattachmentmessage = "Attachment Deleted Successfully";

    }).catch(r => {
      this.handleError(r);
      this.deleteattachmentmessage = r.name[0];
    })
  }
  // For Sorting
  Sort(param, order) {
    if (order === 'desc') {
      this.params.set('ordering', '-' + param);
    }
    if (order === 'asc') {
      this.params.set('ordering', param);
    }
    this.getPurchaseReceiptList(this.params);
  }


  getAllLists(params: any) {
    this.loading = true;
    this.prService.getPurchaseReceiptList(params).then(response => {
      this.purchasereceipt = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r => {
      this.handleError(r);
    });
  }

  // Export to Excel List

  onDateChanged(event: any, type: any) {
    if (type == 'FromDate') {
      this.fromDate = event.formatted;
    }
    if (type == 'ToDate') {
      this.toDate = event.formatted;
    }
  }

  export() {
    let userId = localStorage.getItem('user_nameId');
    this.API_URL_Export = this.configuration.ServerWithApiUrl + 'ReceiptAndTransfer/ExportPurchaseReceiptFiles?FromDate=' + this.fromDate + '&ToDate=' + this.toDate + '&UserId=' + userId;

  }

  onSearchDateChanged(date, type) {
    let dateFromPicker = { year: date.date.year, month: date.date.month, day: date.date.day };
    if(date.date.month < 10 && date.date.day < 10) {
      date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else if(date.date.day < 10 && date.date.month >= 10) {
      date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
    } else if(date.date.day >= 10 && date.date.month < 10) {
      date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else {
      date = date.date.day + '/' + date.date.month + '/' + date.date.year;
    }
    if(type == 'fromDate') {
      let toDateOptionCopy = this.getCopyOfSearchToDatePickerOptions()
      dateFromPicker.day = dateFromPicker.day-1;
      toDateOptionCopy.disableUntil = dateFromPicker;
      this.searchToDatePickerOptions = toDateOptionCopy;
      this.params.set('FromDate', date);
      this.getPurchaseReceiptList(this.params);
    } else if(type == 'toDate') {
      let fromDateOptionCopy = this.getCopyOfSearchFromDatePickerOptions();
      dateFromPicker.day = dateFromPicker.day+1;
      fromDateOptionCopy.disableSince = dateFromPicker;
      this.searchFromDatePickerOptions = fromDateOptionCopy;
      this.params.set('ToDate', date);
      this.getPurchaseReceiptList(this.params);
    }
  }

  getCopyOfSearchToDatePickerOptions(): IMyOptions {
    return JSON.parse(JSON.stringify(this.searchToDatePickerOptions));
  }

  getCopyOfSearchFromDatePickerOptions(): IMyOptions {
    return JSON.parse(JSON.stringify(this.searchFromDatePickerOptions));
  }

  private handleError(e: any) {
    let detail = e.detail;
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}
