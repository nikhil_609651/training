import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class PurchaseReceiptFormService {
    public listUrl = '';
    public saveUrl = '';
    public updateUrl = '';
    public deleteUrl = '';
    public purchasereceipturl = '';
    public truckUrl = '';
    public dvUrl = '';
    public polUrl = '';
    public polcatUrl = '';
    public getPolUrl = '';
    public getSTUrl = '';
    public getPolfromSiteUrl = '';
    public getSTfromPolUrl = '';
    public getDriverUrl = '';
    public getCompanyUrl = '';
    public getTruckUrl = '';
    public siteUrl = '';
    public filtersiteUrl='';
    public storagetankUrl = '';
    public driverdropdownUrl = '';
    public attachmenturl = '';
    public statusUrl = '';
    public exportUrl = '';
    public approverUrl = '';
    public deleteattachmentUrl = '';
    public quantityUrl = '';
    public companyListUrl = '';
    public saveDischargeVehicleUrl = '';
    public saveDriverUrl = '';
    public buListUrl = '';
    public checkBatchUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.listUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/PurchaseReceipt/';
        this.saveUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/SavePurchaseReceipt';
        this.exportUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/ExportPurchaseReceiptFiles?FromDate=';
        this.deleteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeletePurchaseReceiptByID?id=';
        this.purchasereceipturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseReceiptByID?id=';
        this.truckUrl = _configuration.ServerWithApiUrl+'Masters/Truck/';
        this.dvUrl = _configuration.ServerWithApiUrl+'Masters/DischargeVehicle/';
        this.polcatUrl = _configuration.ServerWithApiUrl+'Masters/POL/';
        this.polUrl = _configuration.ServerWithApiUrl+'Masters/GetLubricantPOL?Lubricant=';
        this.getPolUrl = _configuration.ServerWithApiUrl+'Masters/GetPolById?id=';
        this.getSTUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetTankPolAvailableQuantity?StorageTankId=';
        this.getPolfromSiteUrl = _configuration.ServerWithApiUrl + 'Masters/getAllPOLBySite?SiteId=';
        this.getSTfromPolUrl = _configuration.ServerWithApiUrl + 'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.getDriverUrl = _configuration.ServerWithApiUrl+'Masters/GetDriverByID?id=';
        this.getCompanyUrl = _configuration.ServerWithApiUrl+'Masters/GetDischargeVehicleByID?id=';
        this.getTruckUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseDischargeVehiclePreviousOdoMeterReading?DischargeVehicleID=';
        this.filtersiteUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseReceiptSiteFilter/';
        this.siteUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.driverdropdownUrl = _configuration.ServerWithApiUrl+'Masters/Driver';
        this.storagetankUrl = _configuration.ServerWithApiUrl+'Masters/GetAllStorageTankByPOLIdAndSiteID?POLId=';
        this.attachmenturl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetAllPurchaseReceiptAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/DeletePurchaseReceiptAttachments?Id=';
        this.statusUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseReceiptApprovedStatusFilter';
        this.approverUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/GetPurchaseReceiptApproverFilter';
        this.quantityUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/CalculateQuantityAt15DegreeCelsius?Temprature='
        this.companyListUrl = _configuration.ServerWithApiUrl+'Masters/TransportCompany/';
        this.saveDischargeVehicleUrl = _configuration.ServerWithApiUrl+'Masters/SaveDischargeVehicle';
        this.saveDriverUrl = _configuration.ServerWithApiUrl+'Masters/SaveDriver';
        this.buListUrl = _configuration.ServerWithApiUrl+'Masters/BusinessUnit';
        this.checkBatchUrl = _configuration.ServerWithApiUrl+'ReceiptAndTransfer/CheckBatchNumber?BatchNo=';
    }

    // List API for Purchase Receipt
    getPurchaseReceiptList(params: any): Promise<any>{
        return this.authHttp.get(this.listUrl,{search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    ExportToExcel(fromDate, toDate, userId): Promise<any>{
        return this.authHttp.get(this.exportUrl+fromDate+'&ToDate='+toDate+ '&UserId='+userId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);

    }

    getpolList(Lubricant :boolean): Promise<any> {
        return this.authHttp.get(this.polUrl + Lubricant)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getpolListcat(): Promise<any> {
        return this.authHttp.get(this.polcatUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getSiteList(): Promise<any> {
        return this.authHttp.get(this.siteUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getFilterSiteList(userId): Promise<any> {
        return this.authHttp.get(this.filtersiteUrl+'?UserId='+userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getCalculateQuantityAt15DegreeCelsius(Density: string, Temperature:string,quantityissued) {
        return this.authHttp.get(this.quantityUrl + Temperature + '&Density=' + Density+ '&Quantity=' +quantityissued)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getApprovedStatusList(): Promise<any> {
        return this.authHttp.get(this.statusUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getApproverList(): Promise<any> {
        return this.authHttp.get(this.approverUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getDriverList(): Promise<any> {
        return this.authHttp.get(this.driverdropdownUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getTruckList(): Promise<any> {
        return this.authHttp.get(this.truckUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getDischargevehicleList(): Promise<any> {
        return this.authHttp.get(this.dvUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getStorageTankList(polId,siteId): Promise<any> {
        return this.authHttp.get(this.storagetankUrl+  polId + '&SiteId=' + siteId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolDetailsById(id: any) {
        return this.authHttp.get(this.getPolUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getSTDetailsById(id: string,prId:string) {
        return this.authHttp.get(this.getSTUrl +  id + '&PurchaseReceiptID=' + prId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getStorageTankListFromSiteById(siteid: any,polid: any) {
        return this.authHttp.get(this.getSTfromPolUrl +  siteid+ '&POLId=' + polid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getPolListFromSiteById(id: string) {
        return this.authHttp.get(this.getPolfromSiteUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getDriverDetailsById(id: string) {
        return this.authHttp.get(this.getDriverUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTruckDetailsById(id: string) {
        return this.authHttp.get(this.getTruckUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCompanyDetailsById(id: string) {
        return this.authHttp.get(this.getCompanyUrl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //   Save API for Purchase Receipt
    Save(bu: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    Export(fromDate,toDate,userId): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .get(this.exportUrl+  fromDate + '&ToDate=' + toDate + '&UserId='+userId)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    deletePurchaseReceipt(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url =   this.deleteUrl;
        return this.authHttp
        .post(this.deleteUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getPurchaseReceipt(id: string) {
        return this.authHttp.get(this.purchasereceipturl +  id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getallPurchaseReceiptattachments(id):Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteattachment(id:any){
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
        .post(this.deleteattachmentUrl + id, options)
        .toPromise()
        .then(() => id)
        .catch(this.handleError);
    }

    getCompanyList(): Promise<any> {
        return this.authHttp.get(this.companyListUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveDischargeVehicle(dischargeVehicle: any): Promise<any>  {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveDischargeVehicleUrl, dischargeVehicle)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    saveDriver(driver: any): Promise<any> {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveDriverUrl, driver)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getBUList(): Promise<any> {
        return this.authHttp.get(this.buListUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    checkBatchNumber(batchId): Promise<any> {
        return this.authHttp.get(this.checkBatchUrl+batchId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}
