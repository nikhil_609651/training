import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';
import { IMyOptions } from 'mydatepicker';

@Component({
  selector: 'reports',
    template: `<router-outlet></router-outlet>`,
})
export class ReportsComponent {
}
