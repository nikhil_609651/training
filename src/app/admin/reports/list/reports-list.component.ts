import { Component, ViewEncapsulation } from '@angular/core';
import { URLSearchParams } from "@angular/http";
import { Router } from '@angular/router';
import { IMyOptions } from 'mydatepicker';
import { ReportsComponentService } from '../services/reports.component.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedService } from 'app/services/shared.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Configuration } from 'app/app.constants';

@Component({
  selector: 'reports',
    templateUrl: './reports-list.component.html',
    styleUrls: ['./reports-list.component.css'],

})
export class ReportsListComponent {

    private currentdate = new Date();
    private myDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
      //disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}
  
    };
    private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

    public selectedTime: string;

    error : any;
    reportsList : any;
    reportName : any = '';

    public coptions = [];
    dropdownSettings = {};
    selectedSite = [];
    dropdownList = [];
    selectedSiteItems = [];
    selectedPolItems = [];
    siteIds = [];
    polIds = [];
    polList : any = [];
    polSelected: any = '';
    siteList : any = [];
    selectedBu : any = '';
    buList : any = [];
    params : URLSearchParams = new URLSearchParams();
    fromDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    toDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
    showWeeks : boolean = false;
    showDates : boolean = false;
    dates = Array(31).fill(0).map((x,i)=>i);
    polReq = "";
    buReq = "";
    siteReq = "";

    privileges : any = {
        isAdd: false,
        isDelete : false,
        isEdit : false,
        isView : false
    }
    privilegeSubscription : Subscription;
    scheduleReportForm : FormGroup;

    selectedCountry: any = [];
    selectedScheduledSite: any = [];

    public page : number = 1;
    public length : number = 0;
    public next : string = '';
    public itemsPerPage : number = 10;
    public start : number = 1;
    public maxSize : number = 5;
    public numPages : number = 2;
    public rows:Array<any> = [];

    public config:any = {
        paging: true,
        filtering: {filterString: ''},
        className: ['table-bordered']
    };

    constructor(private _router: Router, private reportService: ReportsComponentService, private _sharedService : SharedService, private formBuilder : FormBuilder,
                private configuration: Configuration
        ) {

        /**user privileges**/	
        this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	        this.privileges = privileges;
        });
        this.itemsPerPage = configuration.itemsPerPage;
        this.params.set('limit', configuration.itemsPerPage.toString());
        this.rows = configuration.rows;
        this.scheduleReportForm = new FormGroup({
            reportName : new FormControl('MEFAS'),
            frequency : new FormControl(''),
            time : new FormControl(),
            day : new FormControl(''),
            date : new FormControl(''),
            businessUnit : new FormControl('', Validators.required),
            site : new FormControl(),
            pol : new FormControl(''),
            email : new FormControl(),
        });

        this.getReports();
        this.getBUList();
        this.getPolList();
        this.getCountryList();
        this.listScheduledReports(this.params);
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };

        let initialFromDate = new Date();
        if(Number(initialFromDate.getMonth()+1) < 10 && initialFromDate.getDate() < 10) {
           this.fromDate = '0' + initialFromDate.getDate() + '/' + '0' + Number(initialFromDate.getMonth()+1) + '/' + initialFromDate.getFullYear();
        } else if(initialFromDate.getDate() < 10 && Number(initialFromDate.getMonth()+1) >= 10) {
           this.fromDate = '0' + initialFromDate.getDate() + '/' + Number(initialFromDate.getMonth()+1) + '/' + initialFromDate.getFullYear();
        } else if(initialFromDate.getDate() >= 10 && Number(initialFromDate.getMonth()+1) < 10) {
           this.fromDate = initialFromDate.getDate() + '/' + '0' + Number(initialFromDate.getMonth()+1) + '/' + initialFromDate.getFullYear();
        } else {
           this.fromDate = initialFromDate.getDate() + '/' + Number(initialFromDate.getMonth()+1) + '/' + initialFromDate.getFullYear();
        }

        let initialToDate = new Date();
        if(Number(initialToDate.getMonth()+1) < 10 && initialToDate.getDate() < 10) {
           this.toDate = '0' + initialToDate.getDate() + '/' + '0' + Number(initialToDate.getMonth()+1) + '/' + initialToDate.getFullYear();
        } else if(initialToDate.getDate() < 10 && Number(initialToDate.getMonth()+1) >= 10) {
           this.toDate = '0' + initialToDate.getDate() + '/' + Number(initialToDate.getMonth()+1) + '/' + initialToDate.getFullYear();
        } else if(initialToDate.getDate() >= 10 && Number(initialToDate.getMonth()+1) < 10) {
           this.toDate = initialToDate.getDate() + '/' + '0' + Number(initialToDate.getMonth()+1) + '/' + initialToDate.getFullYear();
        } else {
           this.toDate = initialToDate.getDate() + '/' + Number(initialToDate.getMonth()+1) + '/' + initialToDate.getFullYear();
        }

    }

    ngOnDestroy() {
        this.privilegeSubscription.unsubscribe();
    }

    onReportChange() {
        this.polSelected = '' ;
        this.selectedPolforDIR = '';
        this.selectedBu = '';
        this.siteIds = [];
        this.selectedBu = '';
        this.selectedSite = [];
        this.polSelected = '';
        this.siteSelectedForSchedule = '';
        this.siteList = [];
        this.scheduledSiteList = [];
        this.selectedSiteItems = [];
        if(this.reportName.toUpperCase() == 'LR') {
            this.reportService.getSiteByUserList().then(response =>{
                let siteDetails = response.result;
                siteDetails.forEach(site => {
                    this.scheduledSiteList.push({
                        item_id: site.siteId , item_text: site.name
                    })
                });
            }).catch(e => {
                this.handleError(e);
            })
        }
    }

    reportRequired = '';
    generateReport() {
       if(this.reportName == '') {
        this.reportRequired = 'Report Name is Required'
       } else {
        this.reportRequired = ''
       }
        if(this.polSelected == '' && this.selectedPolforDIR == '') {
            this.polReq = "POL is required";
        } else {
            this.polReq = "";
        }
        if(this.selectedBu == '') {
            this.buReq = "Business Unit is required";
        } else {
            this.buReq = "";
        }
        if(this.siteIds.length == 0) {
            this.siteReq = "Site is required";
        } else {
            this.siteReq = "";
        }
        /*if(this.selectedPolforDIR == '') {
            this.buReq = "Business Unit is required";
        } else {
            this.buReq = "";
        }*/
        //this.reportName = 'MEFAS'; /*remove this line */
        this.params.set('FromDate', this.fromDate);
        this.params.set('ToDate', this.toDate);
        if(this.reportName.toUpperCase() == 'MEFAS') {
            this.params.set('POL', this.polSelected);
            this.params.set('BU', this.selectedBu);
            this.params.set('Site', this.siteIds.toString());
            if(this.polSelected != '' && this.selectedBu != '' && this.siteIds.length != 0) {
                this.generateMEFASReport(this.params);
            }
        } else if (this.reportName.toUpperCase() == 'DIR') {
            this.params.set('PolId', this.selectedPolforDIR);
            this.params.set('Site', this.siteIds.toString());
            if(this.selectedPolforDIR != '' && this.siteIds.length != 0) {
                this.generateDIRReport(this.params);
            }
        } else if (this.reportName.toUpperCase() == 'LR') {
            this.params.set('Site', this.siteIds.toString());
            if(this.siteIds.length != 0) {
                this.generateLRReport(this.params);
            }
        }
    }

    polSelectedForSchedule = '';
    countrySelectedForSchedule = '';
    siteSelectedForSchedule = '';
    selectedBuForSchedule = '';
    polReqSchedule = '';
    buReqSchedule = '';
    siteReqSchedule = '';
    countryReqSchedule = '';
    formSubmited = false;
    saveSchedule() {
        this.formSubmited = true;
        //this.countryReqSchedule = "Country is required";
        this.buReqSchedule = "Business Unit is required";
        //this.polReqSchedule = "POL is required";
        //this.siteReqSchedule = "Site is required";
        /*if(this.countrySelectedForSchedule == '') {
            this.countryReqSchedule = "Country is required";
        } else {
            this.countryReqSchedule = "";
        }
        if(this.polSelectedForSchedule == '') {
            this.polReqSchedule = "POL is required";
        } else {
            this.polReqSchedule = "";
        }
        if(this.selectedBuForSchedule == '') {
            this.buReqSchedule = "Business Unit is required";
        } else {
            this.buReqSchedule = "";
        }
        if(this.siteIds.length == 0 || this.siteSelectedForSchedule == '') {
            this.siteReqSchedule = "Site is required";
        } else {
            this.siteReqSchedule = "";
        }
        if(this.polSelectedForSchedule != '' && this.selectedBuForSchedule != '' && this.siteIds.length != 0) {
            //schedule report
        }*/
        let scheduleData = this.scheduleReportForm.value;
        let siteList = [];
        scheduleData.site.forEach(site => {
            siteList.push(site.item_id);
        });
        let day = scheduleData.day[0] ? scheduleData.day[0] : 0;
        let date = scheduleData.date[0] ? scheduleData.date[0] : 0;
        let formData = new FormData();
        formData.append('ReportName', scheduleData.reportName);
        formData.append('Frequency', scheduleData.frequency);
        formData.append('Day', day);
        formData.append('Date', date);
        formData.append('Time', scheduleData.time.toString());
        formData.append('SiteLList', siteList.toString());
        formData.append('BUId', scheduleData.businessUnit);
        formData.append('emailList', scheduleData.email);
        formData.append('Pol', scheduleData.pol);
        this.reportService.saveReportSchedule(formData).then(response=>{
            this.modalClose('schedule');
            this.listScheduledReports(this.params);
            jQuery('.modal-backdrop').remove();
            jQuery('#ViewScheduleReport').modal('toggle');
        }).catch(response =>  {
            this.handleError(response);
        });
    }

    changeFrequency(event) {
        let frequency = event.target.value;
        if( frequency == '1') {
            this.showWeeks = false;
            this.showDates = false;
        } else if( frequency == '0'){
            this.showWeeks = false;
            this.showDates = false;
        } else if( frequency == '2') {
            this.showWeeks = true;
            this.showDates = false;
        } else if( frequency == '3') {
            this.showWeeks = false;
            this.showDates = true;
        }
    }

    onDateChanged(event, type) {
        if(type == 'from') {
            //this.fromDate = event.date.day + '/' + event.date.month + '/' + event.date.year;
            this.fromDate = event.formatted;
        } else if(type == 'to') {
            //this.toDate = event.date.day + '/' + event.date.month + '/' + event.date.year;
            this.toDate = event.formatted;
        }
    }

    getReports() {
        this.reportService.getReports().then(response =>  {
            this.reportsList = response;
            }).catch(response =>  {
              this.handleError(response);
            });
    }

    getBUList() {
        this.reportService.getBUList().then(response => {
            this.buList = response['result'];
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    scheduledSiteList = [];
    getSiteByBusinessUnit(id) {
        this.selectedBu = id;
        this.scheduledSiteList = [];
        this.selectedSiteItems = [];
        this.reportService.getSiteList(id).then(response => {
            let siteDetails = response;
            siteDetails.forEach(site => {
                this.scheduledSiteList.push({
                    item_id: site.siteId , item_text: site.name
                })
            });
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    countryList: any = [];
    getCountryList() {
        this.reportService.getCountryList().then(response => {
            let countryDetails = response.result;
            countryDetails.forEach(country => {
                this.countryList.push({
                    item_id: country.countryId , item_text: country.name
                })
            });
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    selectedPol(polId) {
        this.polSelected = polId;
    }

    getPolList() {
        this.reportService.getPolList().then(response => {
            this.polList = response;
        })
        .catch(r => {
            this.handleError(r);
        })
    }

    modalClose(modal) {
        this.scheduledSiteList = [];
        this.reportRequired = '';
        this.initialFromDate = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
        this.initialToDate = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
        if(modal == 'generate') {
            this.siteList = [];
            this.polList = [];
            this.selectedSiteItems = [];
            this.selectedBu = '';
            this.polSelected = '';
            this.buReq = '';
            this.polReq = '';
            this.siteReq = '';
            this.buList = [];
            this.reportName = '';
            this.selectedPolforDIR = '';
            this.getBUList();
            this.getPolList();
        }
        if(modal == 'schedule') {
            this.countryReqSchedule = '';
            this.polReqSchedule = '';
            this.buReqSchedule = '';
            this.siteReqSchedule = '';
            this.scheduleReportForm = new FormGroup({
                reportName : new FormControl('MEFAS'),
                frequency : new FormControl(''),
                time : new FormControl(),
                day : new FormControl(''),
                date : new FormControl(''),
                businessUnit : new FormControl('', Validators.required),
                site : new FormControl(),
                pol : new FormControl(''),
                email : new FormControl(),
            });
            //this.getBUList();
            //this.getPolList();
            this.selectedCountry = "";
            //this.scheduleReportForm.reset
        }
    }

    onItemSelect(event, item) {
        let isIdPresent = false;
        if(item === 'site') {
            if(this.siteIds.length == 0) {
                this.siteIds.push(event.item_id);
            } else {
                this.siteIds.forEach(id=> {
                    if(id == event.item_id) {
                        isIdPresent = true;
                    }
                });
                if(!isIdPresent) {
                    this.siteIds.push(event.item_id);
                }
           }
        } else if(item === 'pol') {
            if(this.polIds.length == 0) {
                this.polIds.push(event.item_id);
            } else {
                this.polIds.forEach(id=> {
                    if(id == event.item_id) {
                        isIdPresent = true;
                    }
                });
                if(!isIdPresent) {
                    this.polIds.push(event.item_id);
                }
           }
        }
    }

    onItemDeSelect(event, item) {
        if( item === 'site' ) {
            let index = this.siteIds.indexOf(event.item_id);
            if(index > -1) {
                this.siteIds.splice(index, 1);
            }
        } else if( item === 'pol' ) {
            let index = this.polIds.indexOf(event.item_id);
            if(index > -1) {
                this.polIds.splice(index, 1);
            }
        }
     }

    onSelectAll(event, item) {
       if(item === 'site') {
        this.siteIds = [];
        event.forEach(id => {
            this.siteIds.push(id.item_id);
        });
       } else if(item === 'pol') {
        event.forEach(id => {
            this.polIds.push(id.item_id);
        });
       }
    }
    onDeSelectAll(event, item) {
       if(item === 'site') {
            this.siteIds = [];
       } else if(item === 'pol') {
            this.polIds = [];
       }
    }

    generateMEFASReport(params) {
        this.reportService.generateMEFASReport(params).then(response =>{
            window.open(response.url);
        }).catch(e => {
            this.handleError(e);
        })
    }

    generateDIRReport(params) {
        this.reportService.generateDIRReport(params).then(response =>{
            window.open(response.url);
        }).catch(e => {
            this.handleError(e);
        })
    }

    generateLRReport(params) {
        this.reportService.generateLRReport(params).then(response =>{
            window.open(response.url);
        }).catch(e => {
            this.handleError(e);
        })
    }

    onCountryChange(country) {
        this.countrySelectedForSchedule = country;
    }

    onBUChange(bu) {
        this.selectedBuForSchedule = bu;
        this.getSiteByBusinessUnit(bu);
    }

    onSiteChange(site) {
        this.selectedBuForSchedule = site;
    }

    onPolChange(pol) {
        this.polSelectedForSchedule = pol;
    }

    selectedPolforDIR = '';
    getSiteByPol(pol) {
        this.siteList = [];
        this.selectedPolforDIR = pol;
        this.selectedSite = [];
        this.selectedSiteItems = [];
        this.reportService.getSiteByPol(pol).then(response =>{
            let siteDetails = response.model;
            siteDetails.forEach(site => {
                this.siteList.push({
                    item_id: site.siteId , item_text: site.name
                })
            });
        }).catch(e => {
            this.handleError(e);
        })
    }

    scheduledReportsList = [];
    frequency = {
        1: 'Daily',
        2: 'Weekly',
        3: 'Monthly'
    }
    listScheduledReports(params: any) {
        this.reportService.listScheduledReports(params).then(response =>{
            this.scheduledReportsList = response.result;
            this.length = response.count;
            this.next = response['next'];
        });
    }

    public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
        let params = this.params;
        let start = (page.page - 1) * page.itemsPerPage;
        this.start = start + 1;
        params.set('limit', page.itemsPerPage);
        params.set('offset',start.toString());
        var sortParam = '';
        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
          params.set(this.config.filtering.name, this.config.filtering.filterString);
        }
        this.listScheduledReports(this.params);
    }

    deleteMsg : string = '';
    deleteConfirm() {
        this.reportService.deleteSchedule(this.scheduleId).then(response =>  {
          if(response) {
            this.deleteMsg = 'Schedule successfully deleted';
            this.listScheduledReports(this.params);
          } else {
            this.deleteMsg = 'Failed to delete Schedule';
          }
          }).catch(response =>  {
            this.deleteMsg = 'Failed to delete Schedule';
            this.handleError(response);
        });
    }

    deleteMessageClear(){
        this.deleteMsg = '';
    }

    scheduleId = '';
    deleteSchedule(id) {
        this.scheduleId = id;
        this.deleteMsg = '';
    }

    cancelModal() {
        this._router.navigate(['./home/reports']);
    }

    private handleError(e: any) {
        this.error = e;
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
    }
}
