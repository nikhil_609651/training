import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../app.constants';

@Injectable()
export class ReportsComponentService {

    private getReportsUrl = '';
    private siteListUrl = '';
    private polListUrl = '';
    private mefasReportUrl = '';
    private buListUrl = '';
    private countryListUrl = '';
    public sitesByPolUrl = '';
    public dirReportUrl = '';
    public lrReportUrl = '';
    public siteByUserUrl = '';
    public saveScheduleUrl = '';
    public listScheduledReportsUrl = '';
    public deleteScheduleUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {
        this.getReportsUrl =_configuration.ServerWithApiUrl+'Reports/ReportType';
        this.siteListUrl = _configuration.ServerWithApiUrl + 'Masters/GetSiteByBusinessUnit';
        this.polListUrl = _configuration.ServerWithApiUrl + 'Masters/GetPOLFilter';
        this.mefasReportUrl = _configuration.ServerWithApiUrl + 'Reports/GenerateMEFASReport';
        this.buListUrl = _configuration.ServerWithApiUrl + 'Masters/BusinessUnit';
        this.countryListUrl = _configuration.ServerWithApiUrl + 'Masters/Country';
        this.sitesByPolUrl = _configuration.ServerWithApiUrl + 'Masters/GetSiteByPol';
        this.dirReportUrl = _configuration.ServerWithApiUrl + 'Reports/GenerateDailyInventoryReport';
        this.lrReportUrl = _configuration.ServerWithApiUrl + 'Reports/GenerateLubricantReport';
        this.siteByUserUrl = _configuration.ServerWithApiUrl+'Masters/Site/';
        this.saveScheduleUrl = _configuration.ServerWithApiUrl+'Reports/SaveReportSchedule';
        this.listScheduledReportsUrl = _configuration.ServerWithApiUrl+'Reports/ScheduledMEFASReports';
        this.deleteScheduleUrl = _configuration.ServerWithApiUrl+'Reports/DeleteScheduledMEFASReports?id=';
    }
    
    getReports(): Promise<any> {
        return this.authHttp.get(this.getReportsUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBUList(): Promise<any> {
        return this.authHttp.get(this.buListUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getSiteList(id): Promise<any> {
        return this.authHttp.get(this.siteListUrl + '?BusinessUnitID='+id +'&UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getPolList(): Promise<any> {
        return this.authHttp.get(this.polListUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCountryList(): Promise<any> {
        return this.authHttp.get(this.countryListUrl + '?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
    generateMEFASReport(params : any): Promise<any> {
        return this.authHttp.get(this.mefasReportUrl, { search : params })
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    getSiteByPol(pol : any): Promise<any> {
        return this.authHttp.get(this.sitesByPolUrl + '?PolID=' + pol)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    generateDIRReport(params : any): Promise<any> {
        return this.authHttp.get(this.dirReportUrl, { search : params })
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    generateLRReport(params : any): Promise<any> {
        return this.authHttp.get(this.lrReportUrl, { search : params })
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    getSiteByUserList(): Promise<any> {
        return this.authHttp.get(this.siteByUserUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    saveReportSchedule(formData): Promise<any> {
        return this.authHttp.post(this.saveScheduleUrl, formData)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    listScheduledReports(params): Promise<any> {
        return this.authHttp.get(this.listScheduledReportsUrl, { search: params })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteSchedule(id): Promise<any> {
        return this.authHttp.post(this.deleteScheduleUrl + id,'')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }
}