import { Routes, RouterModule }  from '@angular/router';

import { Dashboard } from './dashboard.component';

import { DashboardResolverService } from './dashboard-resolver.service';
import { DashboardShiftToTankComponent } from './shifttotank/shifttotank.component';
import { SiteChartComponent } from './siteChart/sitechart.component';
import { SiteViewComponent } from './siteView/siteview.component';
import { GraphChartComponent } from 'app/admin/dashboard/graphpv/graphpv.component';
import { TankPvComponent } from 'app/admin/dashboard/tankpv/tankpv.component';
import { SitePvComponent } from 'app/admin/dashboard/sitepv/sitepv.component';
import { mapviewComponent } from 'app/admin/dashboard/mapview/mapview.component';
import { bargraphComponent } from 'app/admin/dashboard/bargraph/bargraph.component';
/*import { DoughnutChart } from './doughnutChart';*/
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Dashboard,
    children: [
      { path: '', component: SiteChartComponent },
      { path: 'tank', component: DashboardShiftToTankComponent },
      { path: 'siteView', component: SiteViewComponent },
      { path: 'linegraph', component: GraphChartComponent },
      { path: 'mapview', component: mapviewComponent },
      { path:'tankviewpresentation',component:TankPvComponent },
      { path:'siteviewpresentation',component:SitePvComponent },
      { path:'bargraph',component:bargraphComponent }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
