import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import {ChartsModule, Color} from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit} from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { DashboardService } from "app/admin/dashboard/services/dashboard.service";

@Component({
  selector: 'sitepv',
  styles: [require('../siteView/siteview.component.scss')],
  template: require('./sitepv.component.html')
})
export class SitePvComponent  implements OnInit {

    error : any;
    siteDetails : any;
    countriesList : any;
    buList : any;
    siteList : any;
    polFiltersList : any = '';
    polCheckBoxFilters : any = [];
    countryCheckBoxFilters : any = [];
    businessUnitCheckBoxFilters : any = [];
    siteCheckBoxFilters : any = [];

    params: URLSearchParams = new URLSearchParams();

    constructor(private router: Router, private dashboardService: DashboardService) {
      this.params.set('UserId', localStorage.getItem('user_nameId'));
    }
    ngOnInit(){
        this.dashboardService.getSiteDetails(this.params).then(response =>  {
            this.siteDetails = response;
            //this.loading = false;
            }).catch(response =>  {
            this.handleError(response);
            });
    }



onclose(){
    //this.dashboardService.clearPresentationInterval();
    //document.exitFullscreen;
//     this.router.navigate(['./home/dashboard']);
//     jQuery(document).keyup(function(e) {
//       if (e.keyCode == 27) { // Esc
//           window.close(); // or whatever you want

//       }
//   });


  jQuery(".ba-sidebar").css("display", "block");
  jQuery(".al-sidebar").css("display", "block");
  jQuery(".page-top").css("display", "block");
  jQuery(".breadcrumb").css("display", "block");
  jQuery(".al-main").css("marginLeft", "200px");
  var element = document.getElementById("pre-mode");
  element.classList.add("mystyle");

  // document.getElementsByClassName("al-main")[0].className.marginLeft = "200";
  // var element = document.getElementsByClassName("al-main");
  // element.classList.add("mystyle");


}

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this.router.navigate(['./']);
        } else if(detail && detail == 'Signature has expired.') {
          this.router.navigate(['./']);
        }
      }
}
