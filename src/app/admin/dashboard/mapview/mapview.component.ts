// import { Component, OnInit,Input } from '@angular/core';
import { DashboardService } from "../services/dashboard.service";
import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { ChartsModule, Color } from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit, ElementRef, ViewChild, AfterViewInit  } from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef,
  DocumentRef, MapServiceFactory,
  BingMapAPILoaderConfig, BingMapAPILoader,
  GoogleMapAPILoader,  GoogleMapAPILoaderConfig, ILatLong
} from 'angular-maps';
declare const google: any;

@Component({
  selector: 'dashboard',
  styles: [require('../siteChart/sitechart.component.scss')],
  template: require('./mapview.component.html')
})
export class mapviewComponent  implements OnInit  {
  mapdetails: any;

  //@ViewChild('barChart') barChart: ElementRef;

  private currentdate= new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  //private LoadingDate :Date;
  private GraphADate: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
  private GraphBDate: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };

/** MAP START */
_markerTypeId = MarkerTypeId;
_options: IMapOptions = {
  disableBirdseye: false,
  disableStreetside: false,
  navigationBarMode: 1,
  zoom: 7
};

_box: IBox = {
  maxLatitude: 25,
  maxLongitude: 25,
  minLatitude: -10,
  minLongitude: 25
};

_iconInfo: IMarkerIconInfo = {
  markerType: MarkerTypeId.FontMarker,
  //size: { height: 50, width: 50 }, //RoundedImageMarker
  fontName: 'FontAwesome',
  fontSize: 24,
  color: 'red',
  markerOffsetRatio: { x: 0.5, y: 1 },
  text: '\uF276'
}

_markers: Array<ILatLong> = new Array<ILatLong>();

  error: any = '';
  polDetails : any;
  polFiltersList : any = '';
  date = new Date();
  latestDate : any;
  countriesList : any;
  buList : any;
  siteList : any = [];

  params: URLSearchParams = new URLSearchParams();
  polCheckBoxFilters : any = [];
  countryCheckBoxFilters : any = [];
  businessUnitCheckBoxFilters : any = [];
  siteCheckBoxFilters : any = [];
  checkPol = [];
  checkCountry = [];
  checkBU = [];
  checkSite = [];
  isChartDetails = false;
  //params : any  = [];

  constructor( private router: Router, private dashboardService: DashboardService, private _elementRef: ElementRef ) {
    let currentMonth = this.date.getMonth()+1;
    if(new Date().getMonth() < 10 && new Date().getDate() < 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() < 10 && new Date().getMonth() >= 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() >= 10 && new Date().getMonth() < 10) {
      this.latestDate = this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else {
      this.latestDate = this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    }
    this.params.set('date', this.latestDate);
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.getMapDetails();
  }

  ngOnInit() {
  }

  shifttotank() {
    this.router.navigate(['./home/dashboard/tank']);
   // this.dashboardService.clearPresentationInterval();
  }

  /* click action on the map */

  getMapDetails() {
    this.dashboardService.getSiteData().then(response =>  {
      this.mapdetails = response;
      let index = 0;
      if(this.mapdetails.length != 0) {
        this.mapdetails.forEach(element => {
          //this.barChartLabels.push(element.site.name);
          this._markers.push({latitude: element.site.lattitude, longitude: element.site.longitude});
          this._markers[index]['name'] = element.site.name;
          this._markers[index]['stockQuantityPetrol'] = 0;
          this._markers[index]['fuelIssuedQtyPetrol'] = 0;
          this._markers[index]['stockQuantityDiesel'] = 0;
          this._markers[index]['fuelIssuedQtyDiesel'] = 0;
          this._markers[index]['stockQuantityJet'] = 0;
          this._markers[index]['fuelIssuedQtyJet'] = 0;
          if(element['polData']) {
            element['polData'].forEach(pol => {
              pol.totalPolQty ? pol.totalPolQty : pol.totalPolQty = 0;
              pol.totalFuelissued ? pol.totalFuelissued : pol.totalFuelissued = 0;
              if(pol.polName.toUpperCase() == 'PETROL') {
                this._markers[index]['stockQuantityPetrol'] = pol.totalPolQty;
                this._markers[index]['fuelIssuedQtyPetrol'] = pol.totalFuelissued;
              }
              if(pol.polName.toUpperCase() == 'DIESEL') {
                this._markers[index]['stockQuantityDiesel'] = pol.totalPolQty;
                this._markers[index]['fuelIssuedQtyDiesel'] = pol.totalFuelissued;
              }
              if(pol.polName.toUpperCase() == 'JET A1' || pol.polName.toUpperCase() == 'JET A-1') {
                this._markers[index]['stockQuantityJet'] = pol.totalPolQty;
                this._markers[index]['fuelIssuedQtyJet'] = pol.totalFuelissued;
              }
            });
          }
          index++;
        })
      }
    });
  }

  private handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(e.status == '401') {
      this.router.navigate(['./']);
    } else if(detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }
}
