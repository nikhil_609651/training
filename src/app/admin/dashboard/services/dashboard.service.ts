import {Injectable} from '@angular/core';

// import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../../../app.constants';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
// import { AngularFireDatabase } from 'angularfire2/database';
// import { AngularFireAuth }     from 'angularfire2/auth';
// import * as firebase from 'firebase';
@Injectable()
export class DashboardService {
    public isStaus:boolean = true;
    /*private orderUrl ='';*/
    polDetailsBySiteUrl = '';
    polFilterUrl = '';
    countryFilterUrl = '';
    businessUnitFilterUrl = '';
    siteFilterUrl = '';
    polDetailByTankUrl = '';
    getSiteDetailsUrl = '';
    siteUrl='';
    businessunitByCountryUrl='';
    siteListUrl='';
    getAllSiteInCountryUrl = '';
    siteListFromBUUrl = '';
    constructor(private authHttp: AuthHttp, private configuration: Configuration) {
      /*this.orderUrl=configuration.ServerWithApiUrl+'dot_order?contact='; */
      this.polDetailsBySiteUrl = configuration.ServerWithApiUrl + 'Dashboard/GetPolDetailBySite';
      this.polFilterUrl = configuration.ServerWithApiUrl + 'Masters/GetPOLFilter';
      this.countryFilterUrl = configuration.ServerWithApiUrl + 'Masters/Country';
      this.businessUnitFilterUrl = configuration.ServerWithApiUrl + 'Masters/GetBusinessUnitFilter';
      this.siteFilterUrl = configuration.ServerWithApiUrl + 'Masters/Site';
      this.polDetailByTankUrl = configuration.ServerWithApiUrl + 'Dashboard/GetPolDetailByTank';
      this.getSiteDetailsUrl = configuration.ServerWithApiUrl + 'Dashboard/GetSiteDetails';
      this.siteUrl = configuration.ServerWithApiUrl + 'Dashboard/GetSiteforMap';
      this.businessunitByCountryUrl = configuration.ServerWithApiUrl+'Masters/BusinessUnitByCountryId?CountryFilter=';
      this.siteListUrl = configuration.ServerWithApiUrl + 'Masters/GetSiteByBusinessUnit';
      this.getAllSiteInCountryUrl = configuration.ServerWithApiUrl + 'Masters/GetAllSitesInCountryByUserId?countryString=';
      this.siteListFromBUUrl = configuration.ServerWithApiUrl + 'Masters/GetSiteByBusinessUnitString';
    }

    getDashboardData(): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'UserManagement/Menu')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteData(): Promise<any> {

        return this.authHttp.get(this.siteUrl +'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getMerchantData(): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'merchant/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getManagerData(): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'manager/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getManagerIdSingle(id : any): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'manager/'+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }
     getMerchant(): Promise<any> {
        return this.authHttp.get(this.configuration.ServerWithApiUrl+'merchant/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }


    getTable(id: any, params: any): Promise<any> {
       //alert(name);
       //alert(value);
        return this.authHttp.get(this.configuration.ServerWithApiUrl+'generateexcel?user_id='+id,{search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getmerchantTable(id: any, params: any): Promise<any> {
        //return this.authHttp.get('http://192.168.1.85:8000/api/v1/generateexcel')
        return this.authHttp.get(this.configuration.ServerWithApiUrl+'generatemerchant_excel?merchant_id='+id, {search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    ordernowbatch(){
        const formData = new FormData();
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
            return this.authHttp
            .post(this.configuration.ServerWithApiUrl+'batch', formData, options)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }


    getMerchantsingle(id: string): Promise<any> {
        return this.authHttp.get(this.configuration.ServerWithApiUrl+'merchant/'+id+'/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getLocality(id: string): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'locality?city_id='+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    insertbatch(datas: any): Promise<any>  {
        return this.posts(datas);
    }
    
     private posts(ordernow: any): Promise<any> {
           // const formData = new FormData();
           // formData.append('name', ordernow.name);
           let headers = new Headers({});
           let options = new RequestOptions({ headers });

        return this.authHttp
            .post(this.configuration.ServerWithApiUrl+'dot_order', ordernow, options)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
      }

        getMerchantdetailsid(id: string): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'merchant/?user_id='+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
   getAutoFill(contact: number): Promise<any> {

        return this.authHttp.get(this.configuration.ServerWithApiUrl+'dot_order?contact='+contact)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getpolDetailsBySite(params: any): Promise<any> {
        return this.authHttp.get(this.polDetailsBySiteUrl, {search : params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPOLListForFilter(): Promise<any> {
        return this.authHttp.get(this.polFilterUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCountryFilter(): Promise<any> {
        return this.authHttp.get(this.countryFilterUrl + '?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBusinessUnitFilter(): Promise<any> {
        return this.authHttp.get(this.businessUnitFilterUrl + '?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteFilter(): Promise<any> {
        return this.authHttp.get(this.siteFilterUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPolDetailByTank(params): Promise<any> {
        return this.authHttp.get(this.polDetailByTankUrl, { search: params })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    //Dashboard/GetSiteDetails
    getSiteDetails(params): Promise<any> {
        return this.authHttp.get(this.getSiteDetailsUrl, { search: params })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBusinessUnitListByCountryList(CountryFilter: string): Promise<any> {
        return this.authHttp.get(this.businessunitByCountryUrl + CountryFilter)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteList(id): Promise<any> {
        return this.authHttp.get(this.siteListUrl + '?BusinessUnitID='+id +'&UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getAllSiteInCountryFromSiteId(id): Promise<any> {
        return this.authHttp.get(this.getAllSiteInCountryUrl+id  +'&UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteListFromBU(id): Promise<any> {
        return this.authHttp.get(this.siteListFromBUUrl + '?countryString='+id +'&UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    setPresentationInterval(value : any){
        this.configuration.presentationTimerValue = value;
    }
    clearPresentationInterval(){
        window.clearInterval(this.configuration.presentationTimerValue);
    }
}



