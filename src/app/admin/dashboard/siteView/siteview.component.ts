import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import {ChartsModule, Color} from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit} from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { DashboardService } from "../services/dashboard.service";

@Component({
  selector: 'siteView',
  styles: [require('./siteview.component.scss')],
  template: require('./siteview.component.html')
})
export class SiteViewComponent  implements OnInit {
    
    error : any;
    siteDetails : any;
    countriesList : any;
    buList : any;
    siteList : any = [];
    polFiltersList : any = '';
    polCheckBoxFilters : any = [];
    countryCheckBoxFilters : any = [];
    businessUnitCheckBoxFilters : any = [];
    siteCheckBoxFilters : any = [];

    params: URLSearchParams = new URLSearchParams();

    loading : boolean = false;

    constructor(private router: Router, private dashboardService: DashboardService) {
        this.params.set('UserId', localStorage.getItem('user_nameId'));
    }
    ngOnInit(){
        this.getSiteDetails(this.params);
        this.getAllPolFilter();
        this.getCountryFilter();
        this.getBusinessUnitFilter();
        this.getSiteFilter();
    }

    shifttochart(){
        this.router.navigate(['./home/dashboard']);
    }

    shifttotank(){
        this.router.navigate(['./home/dashboard/tank']);
    }

    getSiteDetails(params: any) {
        this.loading = true;
        this.dashboardService.getSiteDetails(params).then(response =>  {
            this.siteDetails = response;
            this.loading = false;
            }).catch(response =>  {
            this.handleError(response);
            });
    }

    getAllPolFilter() {
        this.dashboardService.getPOLListForFilter().then(response =>  {
            this.polFiltersList = response;
            //this.loading = false;
        }).catch(response =>  {
            this.handleError(response);
        });
    }
    
    getCountryFilter() {
        this.dashboardService.getCountryFilter().then(response =>  {
            this.countriesList = response.result;
            //this.loading = false;
        }).catch(response =>  {
            this.handleError(response);
        });
    }
    
    getBusinessUnitFilter() {
        this.dashboardService.getBusinessUnitFilter().then(response =>  {
          this.buList = response;
          //this.loading = false;
        }).catch(response =>  {
           this.handleError(response);
        });
    }
    
    getSiteFilter() {
        this.dashboardService.getSiteFilter().then(response =>  {
          this.siteList = response.result;
          //this.loading = false;
        }).catch(response =>  {
           this.handleError(response);
        });
    }

    filterChart(type) {
      var checkboxes = document.getElementsByTagName('input');
      for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            if(type == 'pol') {
                  if(checkboxes[i].id == 'polFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('POLFilter');
                    this.polCheckBoxFilters = [];
                    sessionStorage.removeItem('POLFilter');
                  }
            } else if(type == 'country') {
                  if(checkboxes[i].id == 'countryFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('CountryFilter');
                    this.countryCheckBoxFilters = [];
                    sessionStorage.removeItem('CountryFilter');
                  }
                  if(checkboxes[i].id == 'buFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('BUFilter');
                    this.businessUnitCheckBoxFilters = [];
                    sessionStorage.removeItem('BUFilter');
                  }
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
                  this.getBusinessUnitFilter();
                  this.getSiteFilter();
            } else if(type == 'businessUnit') {
                  if(checkboxes[i].id == 'buFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('BUFilter');
                    this.businessUnitCheckBoxFilters = [];
                    sessionStorage.removeItem('BUFilter');
                  }
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
                  this.getSiteFilter();
            } else if(type == 'site') {
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
            }
          }
      }
      this.getSiteDetails(this.params);
    }
  
    selectedFilters(type, event) {
      var key = event.target.name.toString();
      if(type == 'pol') {
          var index = this.polCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.polCheckBoxFilters.push(event.target.name);
          } else {
              this.polCheckBoxFilters.splice(index, 1);
          }
          this.params.set('POLFilter', this.polCheckBoxFilters.toString());
          sessionStorage.setItem('POLFilter', this.polCheckBoxFilters);
          this.getSiteDetails(this.params);
      }
      if(type == 'country') {
          var index = this.countryCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.countryCheckBoxFilters.push(event.target.name);
          } else {
              this.countryCheckBoxFilters.splice(index, 1);
          }
          //this.filterChart('site');
          //this.filterChart('businessUnit');
          this.params.set('CountryFilter', this.countryCheckBoxFilters.toString());
          sessionStorage.setItem('CountryFilter', this.countryCheckBoxFilters);
          this.getBUfromCountry(this.countryCheckBoxFilters.toString());
          this.getAllSiteInCountryFromSiteId(this.countryCheckBoxFilters.toString())
          this.getSiteDetails(this.params);
      }
      if(type == 'businessUnit') {
          var index = this.businessUnitCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.businessUnitCheckBoxFilters.push(event.target.name);
          } else {
              this.businessUnitCheckBoxFilters.splice(index, 1);
          }
          //this.filterChart('site');
          this.params.set('BUFilter', this.businessUnitCheckBoxFilters.toString());
          sessionStorage.setItem('BUFilter', this.businessUnitCheckBoxFilters);
          this.getSiteFromBU(this.businessUnitCheckBoxFilters.toString());
          this.getSiteDetails(this.params);
      }
      if(type == 'site') {
          var index = this.siteCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.siteCheckBoxFilters.push(event.target.name);
          } else {
              this.siteCheckBoxFilters.splice(index, 1);
          }
          this.params.set('SiteFilter', this.siteCheckBoxFilters.toString());
          sessionStorage.setItem('SiteFilter', this.siteCheckBoxFilters);
          this.getSiteDetails(this.params);
      }
    }
  
    getBUfromCountry(countryId) {
      if(countryId != '') {
        this.dashboardService.getBusinessUnitListByCountryList(countryId).then(response =>  {
          this.buList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      } else {
        this.getBusinessUnitFilter();
      }
    }
  
    getSiteFromBU(buId) {
      if(buId != '') {
        this.dashboardService.getSiteListFromBU(buId).then(response =>  {
          this.siteList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      } else {
        this.getSiteFilter();
      }
    }
  
    getAllSiteInCountryFromSiteId(countryIdList) {
      if(countryIdList == '') {
        this.getSiteFilter();
      } else {
        this.dashboardService.getAllSiteInCountryFromSiteId(countryIdList).then(response =>  {
          this.siteList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      }
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this.router.navigate(['./']);
        } else if(detail && detail == 'Signature has expired.') {
          this.router.navigate(['./']);
        }
    }
}
