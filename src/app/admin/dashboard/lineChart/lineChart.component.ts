import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import { DashboardService } from "../services/dashboard.service";
import {BaThemeConfigProvider, colorHelper, layoutPaths} from '../../../theme';
import { Router } from '@angular/router';

@Component({
  selector: 'line-chart',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./lineChart.scss')],
  template: require('./lineChart.html')
})
export class LineChart implements OnInit {

  chartData:Object;
  public data: any;
  public user;

  constructor(private _baConfig:BaThemeConfigProvider, private dashboardService: DashboardService, private router: Router) {
    this.user = JSON.parse(sessionStorage.getItem('user'));
  }

  ngOnInit() {
     const data = sessionStorage.getItem('dashboard');
     if(!data){
      this.dashboardService.getDashboardData().then(response => {
        sessionStorage.setItem('dashboard', JSON.stringify(response));
          this.data = response
          this.initLineChart();
      }).catch(r =>  {
        this.handleError(r);
      });
    }
    else{
      this.data = JSON.parse(data);
      this.initLineChart();
    }

  }

  initLineChart() {
      if(this.data.years_data){
        var dt = new Date();
        const year = dt.getFullYear();
        const prevYear = year - 1;
        var temp = [];
        if(this.data.years_data[year] && this.data.years_data[year]['month_data']){
          for (var key in this.data.years_data[year]['month_data']) {
            if(this.user['groups'] && this.user['groups'][0] == 3){
              var total_amount = this.data.years_data[year]['month_data'][key]['completed_artwork'];
              var prev_total_amount = 0;
              if(this.data.years_data[prevYear] && this.data.years_data[prevYear]['month_data']){
                prev_total_amount = (this.data.years_data[prevYear]['month_data'][key] && this.data.years_data[prevYear]['month_data'][key]['completed_artwork']) ? this.data.years_data[prevYear]['month_data'][key]['completed_artwork'] : 0;
              }
            }
            else{
              var total_amount = this.data.years_data[year]['month_data'][key]['total_amount'];
              var prev_total_amount = 0;
              if(this.data.years_data[prevYear] && this.data.years_data[prevYear]['month_data']){
                prev_total_amount = (this.data.years_data[prevYear]['month_data'][key] && this.data.years_data[prevYear]['month_data'][key]['total_amount']) ? this.data.years_data[prevYear]['month_data'][key]['total_amount'] : 0;
              }
            }



            var obj = {
              date: new Date(year, parseInt(key)-1),
              value: total_amount,
              value0: prev_total_amount
            };
            temp.push(obj)
          }
        }

      }

    var layoutColors = this._baConfig.get().colors;

    this.chartData = {
                  type: 'serial',
                  theme: 'blur',
                  marginTop: 15,
                  marginRight: 15,
                  responsive: {
                    'enabled': true
                  },
                  dataProvider: temp,
                  categoryField: 'date',
                  categoryAxis: {
                    parseDates: true,
                    gridAlpha: 0,
                    color: '#000',
                    axisColor: '#005173'
                  },
                  valueAxes: [
                    {
                      minVerticalGap: 50,
                      gridAlpha: 0,
                      color: '#000',
                      axisColor: '#005173'
                    }
                  ],
                  graphs: [
                    {
                      id: 'g0',
                      bullet: 'none',
                      useLineColorForBulletBorder: true,
                      lineColor: colorHelper.hexToRgbA('#00abff', 0.15),
                      lineThickness: 1,
                      negativeLineColor: layoutColors.danger,
                      type: 'smoothedLine',
                      valueField: 'value0',
                      fillAlphas: 1,
                      fillColorsField: 'lineColor'
                    },
                    {
                      id: 'g1',
                      bullet: 'none',
                      useLineColorForBulletBorder: true,
                      lineColor: colorHelper.hexToRgbA('#00abff', 0.15),
                      lineThickness: 1,
                      negativeLineColor: layoutColors.danger,
                      type: 'smoothedLine',
                      valueField: 'value',
                      fillAlphas: 1,
                      fillColorsField: 'lineColor'
                    }
                  ],
                  chartCursor: {
                    categoryBalloonDateFormat: 'MM YYYY',
                    categoryBalloonColor: '#4285F4',
                    categoryBalloonAlpha: 0.7,
                    cursorAlpha: 0,
                    valueLineEnabled: true,
                    valueLineBalloonEnabled: true,
                    valueLineAlpha: 0.5
                  },
                  dataDateFormat: 'MM YYYY',
                  export: {
                    enabled: true
                  },
                  creditsPosition: 'bottom-right',
                  zoomOutButton: {
                    backgroundColor: '#000',
                    backgroundAlpha: 0
                  },
                  zoomOutText: '',
                  pathToImages: layoutPaths.images.amChart
                };
  }

  initChart(chart:any) {

    let zoomChart = () => {};

    chart.addListener('rendered', zoomChart);
    zoomChart();

    if (chart.zoomChart) {
      chart.zoomChart();
    }
  }

  private handleError(e: any) {
    let detail = e.detail
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }


}
