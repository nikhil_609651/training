// import { Component, OnInit,Input } from '@angular/core';
import { DashboardService } from "../services/dashboard.service";
import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { ChartsModule, Color } from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit, ElementRef, ViewChild, AfterViewInit  } from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef,
  DocumentRef, MapServiceFactory,
  BingMapAPILoaderConfig, BingMapAPILoader,
  GoogleMapAPILoader,  GoogleMapAPILoaderConfig, ILatLong
} from 'angular-maps';
declare const google: any;

@Component({
  selector: 'dashboard',
  styles: [require('./sitechart.component.scss')],
  template: require('./sitechart.component.html')
})
export class SiteChartComponent  implements OnInit, AfterViewInit  {
  mapdetails: any;

  //@ViewChild('barChart') barChart: ElementRef;

  private currentdate= new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };
  //private LoadingDate :Date;
  private GraphADate: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };
  private GraphBDate: Object = { date: {day: this.currentdate.getDate(),month: this.currentdate.getMonth() +1 , year: this.currentdate.getFullYear()} };

/** MAP START */
_markerTypeId = MarkerTypeId;
_options: IMapOptions = {
  disableBirdseye: false,
  disableStreetside: false,
  navigationBarMode: 1,
  zoom: 5
};

_box: IBox = {
  maxLatitude: 25,
  maxLongitude: 25,
  minLatitude: 0,
  minLongitude: 20
};

_iconInfo: IMarkerIconInfo = {
  markerType: MarkerTypeId.FontMarker,
  //size: { height: 50, width: 50 }, //RoundedImageMarker
  fontName: 'FontAwesome',
  fontSize: 24,
  color: 'red',
  markerOffsetRatio: { x: 0.5, y: 1 },
  text: '\uF276'
}

_markers: Array<ILatLong> = new Array<ILatLong>();


  private stockBarChart = [
  ];

  private barChartLabels = [];

  private options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    responsive: true
  };

  public color:Array<any> = [
    { // Green
      backgroundColor: '#008148',
      borderColor: '#008148',
      HoverBackgroundColor: '#008148',
      HoverBorderColor: '#008148'
    },
    { // Blue
      backgroundColor: '#0b1c25',
      borderColor: '#0b1c25',
      HoverBackgroundColor: '#0b1c25',
      HoverBorderColor: '#0b1c25'
    },
    { // Red
      backgroundColor: '#EB2834',
      borderColor: '#EB2834',
      pointBackgroundColor: 'EB2834',
      pointBorderColor: 'EB2834',
      pointHoverBackgroundColor: 'EB2834',
      pointHoverBorderColor: 'EB2834'
    },
    { // Yellow
      backgroundColor: '#cccc00',
      borderColor: '#cccc00',
      pointBackgroundColor: 'cccc00',
      pointBorderColor: 'cccc00',
      pointHoverBackgroundColor: 'cccc00',
      pointHoverBorderColor: 'cccc00'
    },
    { // violet
      backgroundColor: '#0000cc',
      borderColor: '#0000cc',
      pointBackgroundColor: '0000cc',
      pointBorderColor: '0000cc',
      pointHoverBackgroundColor: '0000cc',
      pointHoverBorderColor: '0000cc'
    },
    { // pink
      backgroundColor: '#cc00cc',
      borderColor: '#cc00cc',
      pointBackgroundColor: 'cc00cc',
      pointBorderColor: 'cc00cc',
      pointHoverBackgroundColor: 'cc00cc',
      pointHoverBorderColor: 'cc00cc'
    },
    { // grey
      backgroundColor: '#8c7373',
      borderColor: '#8c7373',
      pointBackgroundColor: '8c7373',
      pointBorderColor: '8c7373',
      pointHoverBackgroundColor: '8c7373',
      pointHoverBorderColor: '8c7373'
    },
    { // orange
      backgroundColor: '#ff6600',
      borderColor: '#ff6600',
      pointBackgroundColor: 'ff6600',
      pointBorderColor: 'ff6600',
      pointHoverBackgroundColor: 'ff6600',
      pointHoverBorderColor: 'ff6600'
    },
    { // l.green
      backgroundColor: '#99ff66',
      borderColor: '#99ff66',
      pointBackgroundColor: '99ff66',
      pointBorderColor: '99ff66',
      pointHoverBackgroundColor: '99ff66',
      pointHoverBorderColor: '99ff66'
    },
    { // l.blue
      backgroundColor: '#66ffcc',
      borderColor: '#66ffcc',
      pointBackgroundColor: '66ffcc',
      pointBorderColor: '66ffcc',
      pointHoverBackgroundColor: '66ffcc',
      pointHoverBorderColor: '66ffcc'
    },
    { // c1
      backgroundColor: '#9966ff',
      borderColor: '#9966ff',
      pointBackgroundColor: '9966ff',
      pointBorderColor: '9966ff',
      pointHoverBackgroundColor: '9966ff',
      pointHoverBorderColor: '9966ff'
    },
    { // c2
      backgroundColor: '#336699',
      borderColor: '#336699',
      pointBackgroundColor: '336699',
      pointBorderColor: '336699',
      pointHoverBackgroundColor: '336699',
      pointHoverBorderColor: '336699'
    },
    { // c3
      backgroundColor: '#6600ff',
      borderColor: '#6600ff',
      pointBackgroundColor: '6600ff',
      pointBorderColor: '6600ff',
      pointHoverBackgroundColor: '6600ff',
      pointHoverBorderColor: '6600ff'
    },


  ];

/************************second chart******************************* */

  private lineChartDataset = [
  ];

  private labels1 = ['Site A', 'Site B'];

  private options1 = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    responsive: true
  };

  public color1:Array<any> = [
    { // Green
      backgroundColor: '#008148',
      borderColor: '#008148',
      HoverBackgroundColor: '#008148',
      HoverBorderColor: '#008148'
    },
    { // Blue
      backgroundColor: '#0b1c25',
      borderColor: '#0b1c25',
      HoverBackgroundColor: '#0b1c25',
      HoverBorderColor: '#0b1c25'
    },
    { // Red
      backgroundColor: '#EB2834',
      borderColor: '#EB2834',
      pointBackgroundColor: 'EB2834',
      pointBorderColor: 'EB2834',
      pointHoverBackgroundColor: 'EB2834',
      pointHoverBorderColor: 'EB2834'
    },
    { // Yellow
      backgroundColor: '#cccc00',
      borderColor: '#cccc00',
      pointBackgroundColor: 'cccc00',
      pointBorderColor: 'cccc00',
      pointHoverBackgroundColor: 'cccc00',
      pointHoverBorderColor: 'cccc00'
    },
    { // violet
      backgroundColor: '#0000cc',
      borderColor: '#0000cc',
      pointBackgroundColor: '0000cc',
      pointBorderColor: '0000cc',
      pointHoverBackgroundColor: '0000cc',
      pointHoverBorderColor: '0000cc'
    },
    { // pink
      backgroundColor: '#cc00cc',
      borderColor: '#cc00cc',
      pointBackgroundColor: 'cc00cc',
      pointBorderColor: 'cc00cc',
      pointHoverBackgroundColor: 'cc00cc',
      pointHoverBorderColor: 'cc00cc'
    },
    { // grey
      backgroundColor: '#8c7373',
      borderColor: '#8c7373',
      pointBackgroundColor: '8c7373',
      pointBorderColor: '8c7373',
      pointHoverBackgroundColor: '8c7373',
      pointHoverBorderColor: '8c7373'
    },
    { // orange
      backgroundColor: '#ff6600',
      borderColor: '#ff6600',
      pointBackgroundColor: 'ff6600',
      pointBorderColor: 'ff6600',
      pointHoverBackgroundColor: 'ff6600',
      pointHoverBorderColor: 'ff6600'
    },
    { // l.green
      backgroundColor: '#99ff66',
      borderColor: '#99ff66',
      pointBackgroundColor: '99ff66',
      pointBorderColor: '99ff66',
      pointHoverBackgroundColor: '99ff66',
      pointHoverBorderColor: '99ff66'
    },
    { // l.blue
      backgroundColor: '#66ffcc',
      borderColor: '#66ffcc',
      pointBackgroundColor: '66ffcc',
      pointBorderColor: '66ffcc',
      pointHoverBackgroundColor: '66ffcc',
      pointHoverBorderColor: '66ffcc'
    },
    { // c1
      backgroundColor: '#9966ff',
      borderColor: '#9966ff',
      pointBackgroundColor: '9966ff',
      pointBorderColor: '9966ff',
      pointHoverBackgroundColor: '9966ff',
      pointHoverBorderColor: '9966ff'
    },
    { // c2
      backgroundColor: '#336699',
      borderColor: '#336699',
      pointBackgroundColor: '336699',
      pointBorderColor: '336699',
      pointHoverBackgroundColor: '336699',
      pointHoverBorderColor: '336699'
    },
    { // c3
      backgroundColor: '#6600ff',
      borderColor: '#6600ff',
      pointBackgroundColor: '6600ff',
      pointBorderColor: '6600ff',
      pointHoverBackgroundColor: '6600ff',
      pointHoverBorderColor: '6600ff'
    },
  ];

  error: any = '';
  polDetails : any = [];
  polFiltersList : any = '';
  date = new Date();
  latestDate : any;
  countriesList : any;
  buList : any;
  siteList : any = [];

  params: URLSearchParams = new URLSearchParams();
  polCheckBoxFilters : any = [];
  countryCheckBoxFilters : any = [];
  businessUnitCheckBoxFilters : any = [];
  siteCheckBoxFilters : any = [];
  checkPol = [];
  checkCountry = [];
  checkBU = [];
  checkSite = [];
  isChartDetails = false;
  //params : any  = [];

  constructor( private router: Router, private dashboardService: DashboardService, private _elementRef: ElementRef ){
    let currentMonth = this.date.getMonth()+1;
    if(new Date().getMonth() < 10 && new Date().getDate() < 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() < 10 && new Date().getMonth() >= 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() >= 10 && new Date().getMonth() < 10) {
      this.latestDate = this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else {
      this.latestDate = this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    }
    this.params.set('date', this.latestDate);
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.getMapDetails();
    this.loadDashboardFilters('POLFilter');
    this.loadDashboardFilters('CountryFilter');
    this.loadDashboardFilters('BUFilter');
    this.loadDashboardFilters('SiteFilter');
    this.getpolDetailsBySite(this.params);
  }

  ngOnInit() {
    this.getAllPolFilter();
    this.getCountryFilter();
  }

  loadDashboardFilters(filter) {
    let sessionString = sessionStorage.getItem(filter);
    if(sessionString) {
      this.polCheckBoxFilters = [];
      this.countryCheckBoxFilters = [];
      this.businessUnitCheckBoxFilters = [];
      this.siteCheckBoxFilters = [];
      let sessionStringtoArray = sessionString.split(',');
      if( filter == 'POLFilter') {
        sessionStringtoArray.forEach(item=> {
          this.checkPol[item] = true;
          this.polCheckBoxFilters.push(item);
        });
        this.params.set('POLFilter', this.polCheckBoxFilters.toString());
      }
      if( filter == 'CountryFilter') {
        sessionStringtoArray.forEach(item=> {
          this.checkCountry[item] = true;
          this.countryCheckBoxFilters.push(item);
        });
        this.params.set('CountryFilter', this.countryCheckBoxFilters.toString());
        this.getBUfromCountry(this.countryCheckBoxFilters.toString());
      }
      if( filter == 'BUFilter') {
        sessionStringtoArray.forEach(item=> {
          this.checkBU[item] = true;
          this.businessUnitCheckBoxFilters.push(item);
        });
        this.params.set('BUFilter', this.businessUnitCheckBoxFilters.toString());
        this.getSiteFromBU(this.businessUnitCheckBoxFilters.toString());
      }
      if( filter == 'SiteFilter') {
        sessionStringtoArray.forEach(item=> {
          this.checkSite[item] = true;
          this.siteCheckBoxFilters.push(item);
        });
        this.params.set('SiteFilter', this.siteCheckBoxFilters.toString());
      }
      this.getpolDetailsBySite(this.params);
    } else {
      this.getBusinessUnitFilter();
      this.getSiteFilter();
    }
  }

  onDateChanged(event:any, type: any) {
    let chartFilterByDate = event.formatted;
    if(chartFilterByDate == "") {
      this.isChartDetails = false;
    } else {
      this.params.set('date', chartFilterByDate);
      this.getpolDetailsBySite(this.params);
    }
  }

  shifttotank() {
    this.router.navigate(['./home/dashboard/tank']);
   // this.dashboardService.clearPresentationInterval();
  }

  /* click action on the map */
  _click(){
  }

  getpolDetailsBySite(params) {
    this.barChartLabels = [];
    this.dashboardService.getpolDetailsBySite(params).then(response =>  {//debugger
      this.polDetails = response;

      if(this.polDetails.length != 0) {
        this.polDetails[0].data.forEach(element => {
          this.barChartLabels.push(element.site.name);

        });
        if(this.polDetails[0].data.length == this.barChartLabels.length) {
          this.isChartDetails = true;
          this.plotStockBarChart();
        }
      }
    }).catch(response =>  {
        this.handleError(response);
    });
  }

  getMapDetails() {
    this.dashboardService.getSiteData().then(response =>  {
      this.mapdetails = response;
      let index = 0;
      if(this.mapdetails.length != 0) {
        this.mapdetails.forEach(element => {
          //this.barChartLabels.push(element.site.name);
          this._markers.push({latitude: element.site.lattitude, longitude: element.site.longitude});
          this._markers[index]['name'] = element.site.name;
          this._markers[index]['stockQuantityPetrol'] = 0;
          this._markers[index]['fuelIssuedQtyPetrol'] = 0;
          this._markers[index]['stockQuantityDiesel'] = 0;
          this._markers[index]['fuelIssuedQtyDiesel'] = 0;
          this._markers[index]['stockQuantityJet'] = 0;
          this._markers[index]['fuelIssuedQtyJet'] = 0;
          if(element['polData']) {
            element['polData'].forEach(pol => {
              pol.totalPolQty ? pol.totalPolQty : pol.totalPolQty = 0;
              pol.totalFuelissued ? pol.totalFuelissued : pol.totalFuelissued = 0;
              if(pol.polName.toUpperCase() == 'PETROL') {
                this._markers[index]['stockQuantityPetrol'] = pol.totalPolQty.toFixed(2);
                this._markers[index]['fuelIssuedQtyPetrol'] = pol.totalFuelissued.toFixed(2);
              }
              if(pol.polName.toUpperCase() == 'DIESEL') {
                this._markers[index]['stockQuantityDiesel'] = pol.totalPolQty.toFixed(2);
                this._markers[index]['fuelIssuedQtyDiesel'] = pol.totalFuelissued.toFixed(2);
              }
              if(pol.polName.toUpperCase() == 'JET A1' || pol.polName.toUpperCase() == 'JET A-1') {
                this._markers[index]['stockQuantityJet'] = pol.totalPolQty.toFixed(2);
                this._markers[index]['fuelIssuedQtyJet'] = pol.totalFuelissued.toFixed(2);
              }
            });
          }
          index++;
        })
      }
    });
  }

  plotStockBarChart() {
    let quantityBySites = [];
    let stockOnLineChart = [];
    let issuanceOnLineChart = [];
    let quantityByAvgStock = [];
    let index = 0;
    this.polDetails.forEach(chartData => {
      quantityBySites.push({ label : 'Stock Status for '+chartData.pol.name, data : [], type: "bar" });
      stockOnLineChart.push({ label : 'Stock Status for '+chartData.pol.name, data : [], type: "line", fill: "false" });
      issuanceOnLineChart.push({ label : 'Issuance for '+chartData.pol.name, data : [], type: "line", fill: "false" });
      quantityByAvgStock.push({ label : chartData.pol.name, data : [], type: "bar" });
      //quantityBySites.data = [];
      chartData.data.forEach(siteData => {
        quantityBySites[index]['data'].push(siteData.totalQty.toFixed(2));
        stockOnLineChart[index]['data'].push(siteData.totalQty.toFixed(2));
        issuanceOnLineChart[index]['data'].push(siteData.fuelIssuedQty.toFixed(2));
        quantityByAvgStock[index]['data'].push(siteData.avgQty.toFixed(2));
      });
      index++;
    });
    this.stockBarChart = [];
    this.stockBarChart = quantityBySites;
    this.lineChartDataset = [];
    this.lineChartDataset = stockOnLineChart;
    this.lineChartDataset.push(...issuanceOnLineChart);
    this.lineChartDataset.push(...quantityByAvgStock);
  }

  getAllPolFilter() {
    this.dashboardService.getPOLListForFilter().then(response =>  {
      this.polFiltersList = response;
      //this.loading = false;
    }).catch(response =>  {
      this.handleError(response);
    });
  }

  getCountryFilter() {
    this.dashboardService.getCountryFilter().then(response =>  {
      this.countriesList = response.result;
      //this.loading = false;
    }).catch(response =>  {
        this.handleError(response);
    });
  }

  getBusinessUnitFilter() {
    this.dashboardService.getBusinessUnitFilter().then(response =>  {
      this.buList = response;
      //this.loading = false;
    }).catch(response =>  {
        this.handleError(response);
    });
  }

  getSiteFilter() {
    this.dashboardService.getSiteFilter().then(response =>  {
      this.siteList = response.result;
      // this.loading = false;
    }).catch(response =>  {
        this.handleError(response);
    });
  }

  ngAfterViewInit() {
  }

  private handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(e.status == '401') {
      this.router.navigate(['./']);
    } else if(detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }

  filterChart(type) {
    var checkboxes = document.getElementsByTagName('input');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
          if(type == 'pol') {
                if(checkboxes[i].id == 'polFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('POLFilter');
                  this.polCheckBoxFilters = [];
                  sessionStorage.removeItem('POLFilter');
                }
          } else if(type == 'country') {
                if(checkboxes[i].id == 'countryFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('CountryFilter');
                  this.countryCheckBoxFilters = [];
                  sessionStorage.removeItem('CountryFilter');
                }
                if(checkboxes[i].id == 'buFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('BUFilter');
                  this.businessUnitCheckBoxFilters = [];
                  sessionStorage.removeItem('BUFilter');
                }
                if(checkboxes[i].id == 'siteFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('SiteFilter');
                  this.siteCheckBoxFilters = [];
                  sessionStorage.removeItem('SiteFilter');
                }
                this.getBusinessUnitFilter();
                this.getSiteFilter();
          } else if(type == 'businessUnit') {
                if(checkboxes[i].id == 'buFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('BUFilter');
                  this.businessUnitCheckBoxFilters = [];
                  sessionStorage.removeItem('BUFilter');
                }
                if(checkboxes[i].id == 'siteFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('SiteFilter');
                  this.siteCheckBoxFilters = [];
                  sessionStorage.removeItem('SiteFilter');
                }
                this.getSiteFilter();
          } else if(type == 'site') {
                if(checkboxes[i].id == 'siteFilterCB') {
                  checkboxes[i].checked = false;
                  this.params.delete('SiteFilter');
                  this.siteCheckBoxFilters = [];
                  sessionStorage.removeItem('SiteFilter');
                }
          }
        }
    }
    this.getpolDetailsBySite(this.params);
  }

  selectedFilters(type, event) {
    var key = event.target.name.toString();
    if(type == 'pol') {
        var index = this.polCheckBoxFilters.indexOf(key);
        if(event.target.checked){
            this.polCheckBoxFilters.push(event.target.name);
        } else {
            this.polCheckBoxFilters.splice(index, 1);
        }
        this.params.set('POLFilter', this.polCheckBoxFilters.toString());
        sessionStorage.setItem('POLFilter', this.polCheckBoxFilters);
        this.getpolDetailsBySite(this.params);
    }
    if(type == 'country') {
        var index = this.countryCheckBoxFilters.indexOf(key);
        if(event.target.checked){
            this.countryCheckBoxFilters.push(event.target.name);
        } else {
            this.countryCheckBoxFilters.splice(index, 1);
      }
      //this.filterChart('site');
      //this.filterChart('businessUnit');
      this.params.set('CountryFilter', this.countryCheckBoxFilters.toString());
      sessionStorage.setItem('CountryFilter', this.countryCheckBoxFilters);
      this.getBUfromCountry(this.countryCheckBoxFilters.toString());
      this.getAllSiteInCountryFromSiteId(this.countryCheckBoxFilters.toString())
      this.getpolDetailsBySite(this.params);
    }
    if(type == 'businessUnit') {
        var index = this.businessUnitCheckBoxFilters.indexOf(key);
        if(event.target.checked){
            this.businessUnitCheckBoxFilters.push(event.target.name);
        } else {
            this.businessUnitCheckBoxFilters.splice(index, 1);
        }
        this.filterChart('site');
        this.params.set('BUFilter', this.businessUnitCheckBoxFilters.toString());
        sessionStorage.setItem('BUFilter', this.businessUnitCheckBoxFilters);
        this.getSiteFromBU(this.businessUnitCheckBoxFilters.toString());
        this.getpolDetailsBySite(this.params);
    }
    if(type == 'site') {
        var index = this.siteCheckBoxFilters.indexOf(key);
        if(event.target.checked){
            this.siteCheckBoxFilters.push(event.target.name);
        } else {
            this.siteCheckBoxFilters.splice(index, 1);
        }
        this.params.set('SiteFilter', this.siteCheckBoxFilters.toString());
        sessionStorage.setItem('SiteFilter', this.siteCheckBoxFilters);
        this.getpolDetailsBySite(this.params);
    }
  }

  getBUfromCountry(countryId) {
    if(countryId != '') {
      this.dashboardService.getBusinessUnitListByCountryList(countryId).then(response =>  {
        this.buList = response;
      }).catch(response =>  {
          this.handleError(response);
      });
    } else {
      this.getBusinessUnitFilter();
    }
  }

  getSiteFromBU(buId) {
    if(buId != '') {
      this.dashboardService.getSiteListFromBU(buId).then(response =>  {
        this.siteList = response;
      }).catch(response =>  {
          this.handleError(response);
      });
    } else {
      this.getSiteFilter();
    }
  }

  getAllSiteInCountryFromSiteId(countryIdList) {
    if(countryIdList == '') {
      this.getSiteFilter();
    } else {
      this.dashboardService.getAllSiteInCountryFromSiteId(countryIdList).then(response =>  {
        this.siteList = response;
      }).catch(response =>  {
          this.handleError(response);
      });
    }
  }

  gotoTank() {
  }

  // Presentation Mode
  presentationmode() {
    let exit = false;
    let elem =  document.body;
    let methodToBeInvoked = elem.requestFullscreen ||
                            elem.webkitRequestFullScreen || elem['mozRequestFullscreen'] ||
                            elem['msRequestFullscreen'];
    if(methodToBeInvoked) methodToBeInvoked.call(elem);
    jQuery(".ba-sidebar").css("display", "none");
    jQuery(".al-sidebar").css("display", "none");
    jQuery(".page-top").css("display", "none");
    jQuery(".breadcrumb").css("display", "none");
    jQuery(".al-main").css("marginLeft", "0px");

    localStorage.setItem("PageNo", "1");
    this.router.navigate(['./home/dashboard/linegraph']);
    const PresentationInterval = setInterval(() => {
      if (exit) {
        clearInterval(PresentationInterval);
        this.router.navigate(['./home/dashboard/']);
      }
      this.pageSequenceChange();
    }, 30000);
    this.dashboardService.setPresentationInterval(PresentationInterval);
    var element = document.getElementById("pre-mode");
    element.classList.remove("mystyle");
    // this.dashboardService.clearPresentationInterval();
    // var esc= document.getElementById("esc");
    // esc.onkeydown = function(e)
    // {
    //   if(e.keyCode==27)
    //   {
    //     console.log(esc);
    //     jQuery(".ba-sidebar").css("display", "block");
    //     jQuery(".al-sidebar").css("display", "block");
    //     jQuery(".page-top").css("display", "block");
    //     jQuery(".breadcrumb").css("display", "block");
    //     jQuery(".al-main").css("marginLeft", "200px");
    //   }
    // }

      jQuery('body').keydown(function(e) {
        // alert(e.which);
        if(e.which == 27) {
              exit = true;
              jQuery(".ba-sidebar").css("display", "block");
              jQuery(".al-sidebar").css("display", "block");
              jQuery(".page-top").css("display", "block");
              jQuery(".breadcrumb").css("display", "block");
              jQuery(".al-main").css("marginLeft", "200px");
              jQuery(".left-menu").css("display", "block");
              jQuery(".al-sidebar-list").css("height" , "auto");
              clearInterval(PresentationInterval);
              this.router.navigate(['./home/dashboard']);
        }
      });
  }

  pageSequenceChange() {
    switch(localStorage.getItem("PageNo")){

      case "1":
      localStorage.setItem("PageNo", "2");
      this.router.navigate(['./home/dashboard/mapview']);
      break; 

      case "2":
        localStorage.setItem("PageNo", "3");
        this.router.navigate(['./home/dashboard/tankviewpresentation']);
      break;

      case "3":
        localStorage.setItem("PageNo", "4");
        this.router.navigate(['./home/dashboard/bargraph']);
      break;

      case "4":
        localStorage.setItem("PageNo", "5");
        this.router.navigate(['./home/dashboard/siteviewpresentation']);
      break;

      case "5":
        localStorage.setItem("PageNo", "1");
        this.router.navigate(['./home/dashboard/linegraph']);
      break;
    }
  }
}
