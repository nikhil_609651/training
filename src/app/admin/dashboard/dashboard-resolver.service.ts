import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AuthService } from "../../services/auth.service";
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class DashboardResolverService implements Resolve<any> {
    constructor(private authService: AuthService, ) {
    }

    public resolve(route: ActivatedRouteSnapshot) {
    	const token = this.authService.getDecodedToken();
	    return this.authService.getUserInfo(token.user_id).then(response => {
            sessionStorage.setItem('user', JSON.stringify(response));
        });
    }
}