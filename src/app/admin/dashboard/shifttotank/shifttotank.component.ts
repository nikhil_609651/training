import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import {ChartsModule, Color} from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit} from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { DashboardService } from "../services/dashboard.service";

@Component({
  selector: 'tank',
  styles: [require('./shifttotank.component.scss')],
  template: require('./shifttotank.component.html')
})
export class DashboardShiftToTankComponent  implements OnInit {
    
    public fillingdata ="M300, 300V2.5c0,0-0.6-0.1-1.1-0.1c0 , 0-25.5-2.3-40.5-2.4c-15 , 0-40.6,2.4-40.6,2.4 c-12.3,1.1-30.3,1.8-31.9 , 1.9c-2-0.1-19.7-0.8-32-1.9c0 , 0-25.8-2.3-40.8-2.4c-15 , 0-40.8 , 2.4-40.8 , 2.4c-12.3, 1.1-30.4 , 1.8-32,1.9 c-2-0.1-20-0.8-32.2-1.9c0 ,0-3.1-0.3-8.1-0.7V300H300z";
    error : any;
    tankDetails : any;
    countriesList : any;
    buList : any;
    siteList : any = [];
    polFiltersList : any;
    
    params: URLSearchParams = new URLSearchParams();

    loading : boolean = false;

    constructor(private router: Router, private dashboardService: DashboardService) {
        this.params.set('UserId', localStorage.getItem('user_nameId'));
    }
    
    /*getMyStyles(index) {
        let translate = '20%';//this.tankDetails[index]
        let myStyles = { 'transform': 'translate(0%,translate)'};
        return myStyles;
    }*/

    ngOnInit(){
        this.getPolDetailByTank(this.params);
        this.getAllPolFilter();
        this.getCountryFilter();
        this.getBusinessUnitFilter();
        this.getSiteFilter();
    }

    shifttochart(){
        this.router.navigate(['./home/dashboard']);
    }

    getPolDetailByTank(params: any) {
        this.loading = true;
        this.dashboardService.getPolDetailByTank(params).then(response =>  {
            this.tankDetails = response;
            this.loading = false;
        }).catch(response =>  {
            this.handleError(response);
        });
    }

    siteView(){
        this.router.navigate(['./home/dashboard/siteView']);
    }
    
    getAllPolFilter() {
        this.dashboardService.getPOLListForFilter().then(response =>  {
            this.polFiltersList = response;
            //this.loading = false;
        }).catch(response =>  {
              this.handleError(response);
        });
    }
    
    getCountryFilter() {
        this.dashboardService.getCountryFilter().then(response =>  {
            this.countriesList = response.result;
            //this.loading = false;
        }).catch(response =>  {
              this.handleError(response);
        });
    }
    
    getBusinessUnitFilter() {
        this.dashboardService.getBusinessUnitFilter().then(response =>  {
          this.buList = response;
          //this.loading = false;
          }).catch(response =>  {
            this.handleError(response);
          });
    }
    
    getSiteFilter() {
        this.dashboardService.getSiteFilter().then(response =>  {
          this.siteList = response.result;
          // this.loading = false;
        }).catch(response =>  {
            this.handleError(response);
        });
    }

    polCheckBoxFilters : any = [];
    countryCheckBoxFilters : any = [];
    businessUnitCheckBoxFilters : any = [];
    siteCheckBoxFilters : any = [];

    filterChart(type) {
      var checkboxes = document.getElementsByTagName('input');
      for (var i = 0; i < checkboxes.length; i++) {
          if (checkboxes[i].type == 'checkbox') {
            if(type == 'pol') {
                  if(checkboxes[i].id == 'polFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('POLFilter');
                    this.polCheckBoxFilters = [];
                    sessionStorage.removeItem('POLFilter');
                  }
            } else if(type == 'country') {
                  if(checkboxes[i].id == 'countryFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('CountryFilter');
                    this.countryCheckBoxFilters = [];
                    sessionStorage.removeItem('CountryFilter');
                  }
                  if(checkboxes[i].id == 'buFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('BUFilter');
                    this.businessUnitCheckBoxFilters = [];
                    sessionStorage.removeItem('BUFilter');
                  }
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
                  this.getBusinessUnitFilter();
                  this.getSiteFilter();
            } else if(type == 'businessUnit') {
                  if(checkboxes[i].id == 'buFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('BUFilter');
                    this.businessUnitCheckBoxFilters = [];
                    sessionStorage.removeItem('BUFilter');
                  }
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
                  this.getSiteFilter();
            } else if(type == 'site') {
                  if(checkboxes[i].id == 'siteFilterCB') {
                    checkboxes[i].checked = false;
                    this.params.delete('SiteFilter');
                    this.siteCheckBoxFilters = [];
                    sessionStorage.removeItem('SiteFilter');
                  }
            }
          }
      }
      this.getPolDetailByTank(this.params);
    }
  
    selectedFilters(type, event) {
      var key = event.target.name.toString();
      if(type == 'pol') {
          var index = this.polCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.polCheckBoxFilters.push(event.target.name);
          } else {
              this.polCheckBoxFilters.splice(index, 1);
          }
          this.params.set('POLFilter', this.polCheckBoxFilters.toString());
          sessionStorage.setItem('POLFilter', this.polCheckBoxFilters);
          this.getPolDetailByTank(this.params);
      }
      if(type == 'country') {
          var index = this.countryCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.countryCheckBoxFilters.push(event.target.name);
          } else {
              this.countryCheckBoxFilters.splice(index, 1);
          }
          //this.filterChart('site');
          //this.filterChart('businessUnit');
          this.params.set('CountryFilter', this.countryCheckBoxFilters.toString());
          sessionStorage.setItem('CountryFilter', this.countryCheckBoxFilters);
          this.getBUfromCountry(this.countryCheckBoxFilters.toString());
          this.getAllSiteInCountryFromSiteId(this.countryCheckBoxFilters.toString())
          this.getPolDetailByTank(this.params);
      }
      if(type == 'businessUnit') {
          var index = this.businessUnitCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.businessUnitCheckBoxFilters.push(event.target.name);
          } else {
              this.businessUnitCheckBoxFilters.splice(index, 1);
          }
          //this.filterChart('site');
          this.params.set('BUFilter', this.businessUnitCheckBoxFilters.toString());
          sessionStorage.setItem('BUFilter', this.businessUnitCheckBoxFilters);
          this.getSiteFromBU(this.businessUnitCheckBoxFilters.toString());
          this.getPolDetailByTank(this.params);
      }
      if(type == 'site') {
          var index = this.siteCheckBoxFilters.indexOf(key);
          if(event.target.checked){
              this.siteCheckBoxFilters.push(event.target.name);
          } else {
              this.siteCheckBoxFilters.splice(index, 1);
          }
          this.params.set('SiteFilter', this.siteCheckBoxFilters.toString());
          sessionStorage.setItem('SiteFilter', this.siteCheckBoxFilters);
          this.getPolDetailByTank(this.params);
      }
    }
  
    getBUfromCountry(countryId) {
      if(countryId != '') {
        this.dashboardService.getBusinessUnitListByCountryList(countryId).then(response =>  {
          this.buList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      } else {
        this.getBusinessUnitFilter();
      }
    }
  
    getSiteFromBU(buId) {
      if(buId != '') {
        this.dashboardService.getSiteListFromBU(buId).then(response =>  {
          this.siteList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      } else {
        this.getSiteFilter();
      }
    }
  
    getAllSiteInCountryFromSiteId(countryIdList) {
      if(countryIdList == '') {
        this.getSiteFilter();
      } else {
        this.dashboardService.getAllSiteInCountryFromSiteId(countryIdList).then(response =>  {
          this.siteList = response;
        }).catch(response =>  {
            this.handleError(response);
        });
      }
    }

    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this.router.navigate(['./']);
        } else if(detail && detail == 'Signature has expired.') {
          this.router.navigate(['./']);
        }
    }
}
