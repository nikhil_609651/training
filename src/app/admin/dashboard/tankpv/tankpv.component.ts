import { OrdersService } from "../orders/services/orders.service";
import { AuthService } from "../../services/auth.service";
import { SharedService } from "../../services/shared.service";
import { URLSearchParams } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import {ChartsModule, Color} from 'ng2-charts';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, Output , OnInit, Inject} from '@angular/core';
import { CustomValidator } from '../../../shared/custom-validator';
import { IMyOptions } from "mydatepicker";
import { DashboardService } from "app/admin/dashboard/services/dashboard.service";
import { SimplePageScrollService, SimplePageScrollConfig} from 'ng2-simple-page-scroll';
import { DOCUMENT } from "@angular/platform-browser";


@Component({
  selector: 'tankpv',
  styles: [require('../shifttotank/shifttotank.component.scss')],
  template: require('./tankpv.component.html')
})
export class TankPvComponent  implements OnInit {

    public fillingdata ="M300, 300V2.5c0,0-0.6-0.1-1.1-0.1c0 , 0-25.5-2.3-40.5-2.4c-15 , 0-40.6,2.4-40.6,2.4 c-12.3,1.1-30.3,1.8-31.9 , 1.9c-2-0.1-19.7-0.8-32-1.9c0 , 0-25.8-2.3-40.8-2.4c-15 , 0-40.8 , 2.4-40.8 , 2.4c-12.3, 1.1-30.4 , 1.8-32,1.9 c-2-0.1-20-0.8-32.2-1.9c0 ,0-3.1-0.3-8.1-0.7V300H300z";
    error : any;
    tankDetails : any;
    countriesList : any;
    buList : any;
    siteList : any;
    polFiltersList : any;

    params: URLSearchParams = new URLSearchParams();

    constructor(private router: Router, private dashboardService: DashboardService, private simplePageScrollService: SimplePageScrollService, @Inject(DOCUMENT) private document: Document) {
      this.params.set('UserId', localStorage.getItem('user_nameId'));
    }

    offset = 0;
    ngOnInit(){
        this.dashboardService.getPolDetailByTank(this.params).then(response =>  {
            this.tankDetails = response;
            var interval = setInterval(()=>{
              if(this.offset === 200){
                clearInterval(interval);
            }
              this.goToHead2();
            },1000);
            //this.goToHead2();
            }).catch(response =>  {
            this.handleError(response);
            });
    }

    public goToHead2(): void {
      this.simplePageScrollService.scrollToElement("#head2", this.offset );
      this.offset= this.offset+50;
      //this.simplePageScrollService.scrollToElement(elementReference, /* optional offset */);
  };


    private handleError(e: any) {
        this.error = e;
        console.log('error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this.router.navigate(['./']);
        } else if(detail && detail == 'Signature has expired.') {
          this.router.navigate(['./']);
        }
      }
}
