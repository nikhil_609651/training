import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { PaginationModule, TabsModule, TooltipModule } from 'ng2-bootstrap';
/*import { ChartsModule,Color } from 'ng2-charts/ng2-charts';*/
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';
import { AgmCoreModule } from 'angular2-google-maps/core';
import {MapsAPILoader} from 'angular2-google-maps/core';

import { Dashboard } from './dashboard.component';
import { routing }       from './dashboard.routing';

import { LineChart } from './lineChart';
/*import { DoughnutChart } from './doughnutChart';*/

import { DashboardService } from "./services/dashboard.service";
import { AuthService } from "../../services/auth.service";
import { DashboardResolverService } from './dashboard-resolver.service';
import { ChartsModule } from 'ng2-charts';
import {MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef,
  DocumentRef, MapServiceFactory,
  BingMapAPILoaderConfig, BingMapAPILoader,
  GoogleMapAPILoader,  GoogleMapAPILoaderConfig
} from 'angular-maps';

import { DashboardShiftToTankComponent } from 'app/admin/dashboard/shifttotank/shifttotank.component';
import { SiteChartComponent } from './siteChart/sitechart.component';
import { SiteViewComponent } from './siteView/siteview.component';
import { GraphChartComponent } from 'app/admin/dashboard/graphpv/graphpv.component';
import { TankPvComponent } from 'app/admin/dashboard/tankpv/tankpv.component';
import { SitePvComponent } from 'app/admin/dashboard/sitepv/sitepv.component';
import { mapviewComponent } from 'app/admin/dashboard/mapview/mapview.component';
import { bargraphComponent } from 'app/admin/dashboard/bargraph/bargraph.component';
import { Ng2SimplePageScrollModule } from 'ng2-simple-page-scroll';

const useBing = false;

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    PaginationModule,
    TabsModule,
    TooltipModule,
    ChartsModule,
    DatePickerModule,
    MyDatePickerModule,
    routing,
    AgmCoreModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD-oftP9oMjfzi9ZUw-woK91ik15yGsE6I'
    }),
    useBing ? MapModule.forRootBing() : MapModule.forRootGoogle(),
    Ng2SimplePageScrollModule.forRoot(),
  ],
  declarations: [
    Dashboard,
    LineChart,
    /*DoughnutChart,*/
    DashboardShiftToTankComponent,
    SiteChartComponent,
    SiteViewComponent,
    GraphChartComponent,
    TankPvComponent,
    SitePvComponent,
    mapviewComponent,
    bargraphComponent

  ],
  providers: [
    DashboardService,
    AuthService,
    DashboardResolverService,
    {
      provide: MapAPILoader, deps: [], useFactory: useBing ? BingMapServiceProviderFactory :  GoogleMapServiceProviderFactory
    },
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export default class DashboardModule {}
export function BingMapServiceProviderFactory(){
  let bc: BingMapAPILoaderConfig = new BingMapAPILoaderConfig();
  bc.apiKey ="Ap0AObt84NcDaUThCeWOj52ZqUHv6k4TJhjLibR-DghC-semgoj-0uPbIi8r0E4j";
    // replace with your bing map key
    // the usage of this key outside this plunker is illegal.
  bc.branch = "experimental";
    // to use the experimental bing brach. There are some bug fixes for
    // clustering in that branch you will need if you want to use
    // clustering.
  return new BingMapAPILoader(bc, new WindowRef(), new DocumentRef());
}

export function GoogleMapServiceProviderFactory(){
  const gc: GoogleMapAPILoaderConfig = new GoogleMapAPILoaderConfig();
  gc.apiKey = 'AIzaSyDe2QqXrbtaORvL-I0WHpiI72HxtfTz5Zo';
    // replace with your google map key
    // the usage of this key outside this plunker is illegal.
  gc.enableClustering = true;
  return new GoogleMapAPILoader(gc, new WindowRef(), new DocumentRef());
}
