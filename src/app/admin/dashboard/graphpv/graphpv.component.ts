import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Component, ElementRef  } from '@angular/core';
import { DashboardService } from "app/admin/dashboard/services/dashboard.service";

@Component({
  selector: 'graphpv',
  styles: [require('../siteChart/sitechart.component.scss')],
  template: require('./graphpv.component.html')
})
export class GraphChartComponent    {

  private barChartLabels = [];//['Site A', 'Site B', 'Site C', 'Site D', 'Site E', 'Site C'];

  public color:Array<any> = [
    { // Green
      backgroundColor: '#008148',
      borderColor: '#008148',
      HoverBackgroundColor: '#008148',
      HoverBorderColor: '#008148'
    },
    { // Blue
      backgroundColor: '#0b1c25',
      borderColor: '#0b1c25',
      HoverBackgroundColor: '#0b1c25',
      HoverBorderColor: '#0b1c25'
    },
    { // Red
      backgroundColor: '#EB2834',
      borderColor: '#EB2834',
      pointBackgroundColor: 'EB2834',
      pointBorderColor: 'EB2834',
      pointHoverBackgroundColor: 'EB2834',
      pointHoverBorderColor: 'EB2834'
    },
    { // Yellow
      backgroundColor: '#cccc00',
      borderColor: '#cccc00',
      pointBackgroundColor: 'cccc00',
      pointBorderColor: 'cccc00',
      pointHoverBackgroundColor: 'cccc00',
      pointHoverBorderColor: 'cccc00'
    },
    { // violet
      backgroundColor: '#0000cc',
      borderColor: '#0000cc',
      pointBackgroundColor: '0000cc',
      pointBorderColor: '0000cc',
      pointHoverBackgroundColor: '0000cc',
      pointHoverBorderColor: '0000cc'
    },
    { // pink
      backgroundColor: '#cc00cc',
      borderColor: '#cc00cc',
      pointBackgroundColor: 'cc00cc',
      pointBorderColor: 'cc00cc',
      pointHoverBackgroundColor: 'cc00cc',
      pointHoverBorderColor: 'cc00cc'
    },
    { // grey
      backgroundColor: '#8c7373',
      borderColor: '#8c7373',
      pointBackgroundColor: '8c7373',
      pointBorderColor: '8c7373',
      pointHoverBackgroundColor: '8c7373',
      pointHoverBorderColor: '8c7373'
    },
    { // orange
      backgroundColor: '#ff6600',
      borderColor: '#ff6600',
      pointBackgroundColor: 'ff6600',
      pointBorderColor: 'ff6600',
      pointHoverBackgroundColor: 'ff6600',
      pointHoverBorderColor: 'ff6600'
    },
    { // l.green
      backgroundColor: '#99ff66',
      borderColor: '#99ff66',
      pointBackgroundColor: '99ff66',
      pointBorderColor: '99ff66',
      pointHoverBackgroundColor: '99ff66',
      pointHoverBorderColor: '99ff66'
    },
    { // l.blue
      backgroundColor: '#66ffcc',
      borderColor: '#66ffcc',
      pointBackgroundColor: '66ffcc',
      pointBorderColor: '66ffcc',
      pointHoverBackgroundColor: '66ffcc',
      pointHoverBorderColor: '66ffcc'
    },
    { // c1
      backgroundColor: '#9966ff',
      borderColor: '#9966ff',
      pointBackgroundColor: '9966ff',
      pointBorderColor: '9966ff',
      pointHoverBackgroundColor: '9966ff',
      pointHoverBorderColor: '9966ff'
    },
    { // c2
      backgroundColor: '#336699',
      borderColor: '#336699',
      pointBackgroundColor: '336699',
      pointBorderColor: '336699',
      pointHoverBackgroundColor: '336699',
      pointHoverBorderColor: '336699'
    },
    { // c3
      backgroundColor: '#6600ff',
      borderColor: '#6600ff',
      pointBackgroundColor: '6600ff',
      pointBorderColor: '6600ff',
      pointHoverBackgroundColor: '6600ff',
      pointHoverBorderColor: '6600ff'
    },
  ];

  private lineChartDataset = [
  ];



  error: any = '';
  polDetails : any;
  polFiltersList : any = '';
  date = new Date();
  latestDate : any;
  countriesList : any;
  buList : any;
  siteList : any;

  params: URLSearchParams = new URLSearchParams();

  constructor(private router: Router,
    private dashboardService: DashboardService){
    let currentMonth = this.date.getMonth()+1;
    if(new Date().getMonth() < 10 && new Date().getDate() < 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() < 10 && new Date().getMonth() >= 10) {
      this.latestDate = '0' + this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    } else if(new Date().getDate() >= 10 && new Date().getMonth() < 10) {
      this.latestDate = this.date.getDate() + '/' + '0' + currentMonth + '/' + this.date.getFullYear();
    } else {
      this.latestDate = this.date.getDate() + '/' + currentMonth + '/' + this.date.getFullYear();
    }
    this.params.set('date', this.latestDate);
    this.params.set('UserId', localStorage.getItem('user_nameId'));
    this.getpolDetailsBySite(this.params);
  }

  shifttotank(){
    this.router.navigate(['./home/dashboard/tank']);
    // this.dashboardService.clearPresentationInterval();
  }

  isChartDetails = false;
  getpolDetailsBySite(params) {
    this.barChartLabels = [];
    this.dashboardService.getpolDetailsBySite(params).then(response =>  {//debugger
      this.polDetails = response;

      if(this.polDetails.length != 0) {
        this.polDetails[0].data.forEach(element => {
          this.barChartLabels.push(element.site.name);

        });
        if(this.polDetails[0].data.length == this.barChartLabels.length) {
          this.isChartDetails = true;
          this.plotStockBarChart();
        }
      }
    }).catch(response =>  {
        this.handleError(response);
    });
  }

  plotStockBarChart() {
    let quantityBySites = [];
    let stockOnLineChart = [];
    let issuanceOnLineChart = [];
    let quantityByAvgStock = [];
    let index = 0;
    this.polDetails.forEach(chartData => {
      quantityBySites.push({ label : 'Stock Status for '+chartData.pol.name, data : [], type: "bar" });
      stockOnLineChart.push({ label : 'Stock Status for '+chartData.pol.name, data : [], type: "line", fill: "false" });
      issuanceOnLineChart.push({ label : 'Issuance for '+chartData.pol.name, data : [], type: "line", fill: "false" });
      quantityByAvgStock.push({ label : chartData.pol.name, data : [], type: "bar" });
      //quantityBySites.data = [];
      chartData.data.forEach(siteData => {
        quantityBySites[index]['data'].push(siteData.totalQty);
        stockOnLineChart[index]['data'].push(siteData.totalQty);
        issuanceOnLineChart[index]['data'].push(siteData.fuelIssuedQty);
        quantityByAvgStock[index]['data'].push(siteData.avgQty);
      });
      index++;
    });
    this.lineChartDataset = [];
    this.lineChartDataset = stockOnLineChart;
    this.lineChartDataset.push(...issuanceOnLineChart);
    this.lineChartDataset.push(...quantityByAvgStock);
  }

  private handleError(e: any) {
    this.error = e;
    let detail = e.detail
    if(e.status == '401') {
      this.router.navigate(['./']);
    } else if(detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }

  polCheckBoxFilters : any = [];
  countryCheckBoxFilters : any = [];
  businessUnitCheckBoxFilters : any = [];
  siteCheckBoxFilters : any = [];

}
