import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'stock-summary',
    template: `<router-outlet></router-outlet>`
})
export class StockSummaryComponent {
	constructor(private router: Router) {
	}
}
