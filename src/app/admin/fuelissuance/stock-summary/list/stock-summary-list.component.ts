import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListViewFormService } from '../../ListView/services/listview.service';
import { Configuration } from 'app/app.constants';
import { URLSearchParams } from '@angular/http';

@Component({
  selector: 'list',
  template: require('./stock-summary-list.component.html')
})
export class StockSummaryListComponent implements OnInit{

  public error = {};
  public success = '';
  public customer: any;

  public start: number = 1;
  public loading: boolean;
  public rows: Array<any> = [];
  // For headers
  public columns: Array<any> = [
    { title: 'ID', name: 'Id', sort: false, },
    { title: 'Site', name: 'site', sort: false},
    { title: 'POL', name: 'pol', sort: false },
    { title: 'Quantity Available', name: 'qtyAvailable', sort: false },
    { title: 'Total issue for current month', name: 'issueforcurrentMonth', sort: false },
    { title: 'Tank Capacity', name: 'capacity', sort: false },
    { title: 'Ullage', name: 'ullage', sort: false }
  ];

  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number =10;
  public maxSize:number = 5;
  public numPages:number = 0;
  public length:number = 5;
  public next = '';
  public businessunit_deleted_id = '';
  public deletemessage='';
  public deleteattachmentmessage='';
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  stockSummaryList : any = [];

  params: URLSearchParams = new URLSearchParams();
  constructor(private router: Router, private _service : ListViewFormService, private _config : Configuration) {
    this.params.set('limit', this._config.itemsPerPage.toString());
    this.params.set('UserId', localStorage.getItem('user_nameId'));
  }

  ngOnInit() {
    this.getStockSummaryList(this.params);
  }

  getStockSummaryList(params) {
    this._service.getStockSummaryList(params).then(response => {
      this.stockSummaryList = response['result'];
      this.length = response['count'];
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach( function( col, key ) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getStockSummaryList(this.params);
  }

  private handleError(e: any) {
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}