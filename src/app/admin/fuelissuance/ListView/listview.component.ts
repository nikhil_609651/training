
import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';

@Component({
  
  selector: 'listview',
    template: `<router-outlet></router-outlet>`
})
export class ListViewComponent {
	constructor(private router: Router) {
	}
}
