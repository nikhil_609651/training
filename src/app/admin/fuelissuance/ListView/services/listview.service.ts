import { Injectable } from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()
export class ListViewFormService {
    public getSTfromPolUrl = '';
    public getPolfromSiteUrl = '';
    public listUrl = '';
    public issuerlistUrl = '';
    public sitelistUrl = '';
    public uomlistUrl = '';
    public pollistUrl = '';
    public storagetanklistUrl = '';
    public registrationnumberlistUrl = '';
    public getRegistrationNoUrl = '';
    public customerlistUrl = '';
    public sectionlistUrl = '';
    public sectionlistfilterUrl = '';
    public receiverlistUrl = '';
    public issuedbylistUrl = '';
    public generatorlistUrl = '';
    public getGeneratorMakeUrl = '';
    public getreceiverdetailsUrl = '';
    public getissuerdetailsUrl = '';
    public saveUrl = '';
    public getPolUrl = '';
    public getIssueTypeNameUrl = '';
    public listviewurl = '';
    public sitefilterlistUrl = '';
    public statusfilterlistUrl = '';
    public approverfilterlistUrl = '';
    public receiverfilterlistUrl = '';
    public attachmenturl = '';
    public deleteattachmentUrl = '';
    public deleteUrl = '';
    public stockUrl = '';
    public quantityUrl = '';
    public batchUrl = '';
    public getSTUrl = '';
    public BatchNumbersbyTankIdUrl = '';
    public saveVehicleUrl = '';
    public saveAircraftUrl = '';
    public saveSectionUrl = '';
    public saveReceiverUrl = '';
    public saveIssuerUrl = '';
    public aircraftlistUrl = '';
    public ownershipinfoUrl = '';
    public vehicletypeUrl = '';
    public makemodelurl = '';
    public departmentUrl = '';
    public aircraftUrl = '';
    public vehicleBySiteIdUrl = '';
    public getBatchUrl = '';
    public getLubricantAvailabilityUrl = '';
    public getAllSiteInCountryFromSiteIdUrl = '';
    public checkIssueSlipNumberUrl = '';
    public getEquipmentsFromSiteIdUrl = '';
    public getStockSummaryListUrl = '';
    public sitedropdownUrl = '';
    public vehicleBySiteAndUserUrl = '';

    constructor(private authHttp: AuthHttp, private _configuration: Configuration) {

        this.listUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/FuelIssuance';
        this.issuerlistUrl = _configuration.ServerWithApiUrl + 'Masters/EquipmentType';
        this.sitelistUrl = _configuration.ServerWithApiUrl + 'Masters/Site';
        this.pollistUrl = _configuration.ServerWithApiUrl + 'Masters/GetAllPOLByLubiricant';
        this.storagetanklistUrl = _configuration.ServerWithApiUrl + 'Masters/StorageTank';
        this.registrationnumberlistUrl = _configuration.ServerWithApiUrl + 'Masters/Vehicle';
        this.getRegistrationNoUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetPreviousFuelIssuanceByVehicleID?VehicleId=';
        this.customerlistUrl = _configuration.ServerWithApiUrl + 'Masters/Customer';
        this.aircraftlistUrl = _configuration.ServerWithApiUrl + 'Masters/GetAircraftByBUAndSite';
        this.sectionlistUrl = _configuration.ServerWithApiUrl + 'Masters/Section';
        this.sectionlistfilterUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetSectionFilter';
        this.receiverlistUrl = _configuration.ServerWithApiUrl + 'Masters/GetReceiverByBUAndSite';
        this.issuedbylistUrl = _configuration.ServerWithApiUrl + 'Masters/Issuer';
        this.generatorlistUrl = _configuration.ServerWithApiUrl + 'Masters/Generator';
        this.uomlistUrl = _configuration.ServerWithApiUrl + 'Masters/UOM';
        this.getGeneratorMakeUrl = _configuration.ServerWithApiUrl + 'Masters/GetGeneratorByID?id=';
        this.saveUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/SaveFuelIssuance';
        this.deleteUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/DeleteFuelIssuanceByID?id=';
        this.getPolUrl = _configuration.ServerWithApiUrl + 'Masters/GetPOLByID?id=';
        this.getIssueTypeNameUrl = _configuration.ServerWithApiUrl + 'Masters/GetEquipmentTypeByID?id=';
        this.listviewurl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetFuelIssuanceByID?id=';
        this.sitefilterlistUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetNewFuelIssuanceSiteFilter';
        this.statusfilterlistUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetFuelIssuanceApprovedStatusFilter';
        this.approverfilterlistUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetNewFuelIssuanceApprovedUserFilter';
        this.receiverfilterlistUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetReceiverFilter';
        this.attachmenturl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetAllFuelIssuanceAttachments?id=';
        this.deleteattachmentUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/DeleteFuelIssuanceAttachments?Id=';
        this.stockUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/GetPolAvailableQuantityBySite?SiteID='
        this.quantityUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/CalculateQuantityAt15DegreeCelsius?Temprature='
        this.batchUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetAllBatchDetailsByQuantityAndTank?TankId='
        // tslint:disable-next-line:max-line-length
        this.getPolfromSiteUrl = _configuration.ServerWithApiUrl + 'Masters/getAllPOLBySite?SiteId=';
        this.getSTfromPolUrl = _configuration.ServerWithApiUrl + 'Masters/GetAllStorageTankByPOLIdAndSiteID?SiteId=';
        this.BatchNumbersbyTankIdUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetAllBatchDetailsByTankID?TankId='
        this.saveVehicleUrl = _configuration.ServerWithApiUrl + 'Masters/SaveVehicle';
        this.saveAircraftUrl = _configuration.ServerWithApiUrl + 'Masters/SaveAircraft';
        this.saveSectionUrl = _configuration.ServerWithApiUrl + 'Masters/SaveSection';
        this.saveReceiverUrl = _configuration.ServerWithApiUrl + 'Masters/SaveReceiver';
        this.saveIssuerUrl = _configuration.ServerWithApiUrl + 'Masters/SaveIssuer';
        this.ownershipinfoUrl = _configuration.ServerWithApiUrl + 'Masters/Ownership/';
        this.vehicletypeUrl = _configuration.ServerWithApiUrl + 'Masters/VehicleType/';
        this.makemodelurl = _configuration.ServerWithApiUrl + 'Masters/VehicleMakeAndModel/';
        this.departmentUrl = _configuration.ServerWithApiUrl + 'Masters/Department/';
        this.getSTUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/GetAvailableQuantityByTankId?TankId=';
        this.aircraftUrl = _configuration.ServerWithApiUrl + 'Masters/AircraftType/';
        this.vehicleBySiteIdUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetVehicleRegNo?SiteID=';
        this.getreceiverdetailsUrl = _configuration.ServerWithApiUrl + 'Masters/GetReceiverByID?id=';
        this.getissuerdetailsUrl = _configuration.ServerWithApiUrl + 'Masters/GetIssuerByID?id=';
        this.getBatchUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetAllBatchDetailsByTankID?';
        this.getLubricantAvailabilityUrl = _configuration.ServerWithApiUrl + 'ReceiptAndTransfer/LubricantAvailibility?SiteFilter=';
        this.getAllSiteInCountryFromSiteIdUrl = _configuration.ServerWithApiUrl + 'Masters/GetAllSitesInCountry?siteId=';
        this.checkIssueSlipNumberUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/CheckIssueSlipNumber?issueSlipNo=';
        this.getEquipmentsFromSiteIdUrl = _configuration.ServerWithApiUrl + 'Masters/GetEquipmentBySiteAndType?siteId=';
        this.getStockSummaryListUrl = _configuration.ServerWithApiUrl + 'StockAdjustment/StockSummary';
        this.sitedropdownUrl = _configuration.ServerWithApiUrl+'UserManagement/GetSitebyUserId?userid=';

        this.vehicleBySiteAndUserUrl = _configuration.ServerWithApiUrl + 'FuelIssuance/GetVehicleRegNoForFuelIssuance?SiteID=';
    }


    // List API for vehicle by site
    getVehicleBySiteId(id: any, vehicleType: any): Promise<any> {
        return this.authHttp.get(this.vehicleBySiteIdUrl + id + '&vehicleType=' + vehicleType)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

       // List API for vehicle by site and User
       getVehicleBySiteAndUser( id: any, vehicleType: any, userId : any): Promise<any> {

        return this.authHttp.get(this.vehicleBySiteAndUserUrl + id + '&vehicleType=' + vehicleType + '&userId=' + userId )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }




    // List API for FuelIssuance
    getfuelissuancelist(params: any): Promise<any> {
        return this.authHttp.get(this.listUrl, { search: params })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getissuerlist(): Promise<any> {
        return this.authHttp.get(this.issuerlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    
    getsitelist(): Promise<any> {
        return this.authHttp.get(this.sitelistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getsitefilterlist(userId): Promise<any> {
        return this.authHttp.get(this.sitefilterlistUrl + '?UserId=' + userId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getreceiverfilterlist(): Promise<any> {
        return this.authHttp.get(this.receiverfilterlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getuomlist(): Promise<any> {
        return this.authHttp.get(this.uomlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getpollist(): Promise<any> {
        return this.authHttp.get(this.pollistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getPolDetailsById(id: string) {
        return this.authHttp.get(this.getPolUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getstoragetanklist(): Promise<any> {
        return this.authHttp.get(this.storagetanklistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getregistrationnumberlist(): Promise<any> {
        return this.authHttp.get(this.registrationnumberlistUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getcustomerlist(params): Promise<any> {
        return this.authHttp.get(this.customerlistUrl, {search: params} )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getaircraftlist(site): Promise<any> {
        return this.authHttp.get(this.aircraftlistUrl+'?SiteId='+site)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getsectionlist(): Promise<any> {
        return this.authHttp.get(this.sectionlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getsectionfilterlist(): Promise<any> {
        return this.authHttp.get(this.sectionlistfilterUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getstatuslist(): Promise<any> {
        return this.authHttp.get(this.statusfilterlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getapproverlist(): Promise<any> {
        return this.authHttp.get(this.approverfilterlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getreceiverlist(site): Promise<any> {
        return this.authHttp.get(this.receiverlistUrl+'?SiteId='+site)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    getissuedbylist(): Promise<any> {
        return this.authHttp.get(this.issuedbylistUrl+'?UserId='+localStorage.getItem('user_nameId'))
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getgeneratorlist(): Promise<any> {
        return this.authHttp.get(this.generatorlistUrl, )
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    getIssueTypeName(id: string) {
        return this.authHttp.get(this.getIssueTypeNameUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getRegistrationNoDetails(id: string) {
        return this.authHttp.get(this.getRegistrationNoUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getBatchNumbersbyTankId(id: string) {
        return this.authHttp.get(this.BatchNumbersbyTankIdUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getGeneratorMake(id: string) {
        return this.authHttp.get(this.getGeneratorMakeUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getreceiverdetails(id: string) {
        return this.authHttp.get(this.getreceiverdetailsUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getissuerdetails(id: string) {
        return this.authHttp.get(this.getissuerdetailsUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getlistview(id: string) {
        return this.authHttp.get(this.listviewurl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getallfuelissuanceattachments(id): Promise<any> {
        return this.authHttp.get(this.attachmenturl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    deleteattachment(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.deleteattachmentUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getCalculateQuantityAt15DegreeCelsius(Density: string, Temperature: string, quantityissued) {
        return this.authHttp.get(this.quantityUrl + Temperature + '&Density=' + Density + '&Quantity=' + quantityissued)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTankCapacity(id: string) {
        return this.authHttp.get(this.getSTUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getBatchNumber(TankId: any, Quantity: any) {
        return this.authHttp.get(this.batchUrl + TankId + '&Quantity=' + Quantity)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getpolstock(siteId: string, polId: string) {
        return this.authHttp.get(this.stockUrl + siteId + '&POLID=' + polId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    //   Save API for Fuel Issuance
    Save(bu: any): Promise<any> {

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    // Delete API for Fuel Issuance
    delete(id: any) {
        const formData = new FormData();
        formData.append('id', id);

        let headers = new Headers({});
        let options = new RequestOptions({ headers });

        let url = this.deleteUrl;
        return this.authHttp
            .post(this.deleteUrl + id, options)
            .toPromise()
            .then(() => id)
            .catch(this.handleError);
    }

    getStorageTankListFromSiteById(siteid: string, polid: string) {
        return this.authHttp.get(this.getSTfromPolUrl + siteid + '&POLId=' + polid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getPolListFromSiteById(id: string) {
        return this.authHttp.get(this.getPolfromSiteUrl + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getVehicleTypeList(): Promise<any> {
        return this.authHttp.get(this.vehicletypeUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getOwnershipInfoList(): Promise<any> {
        return this.authHttp.get(this.ownershipinfoUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    getMakeModelList(): Promise<any> {
        return this.authHttp.get(this.makemodelurl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getDepartmentList(): Promise<any> {
        return this.authHttp.get(this.departmentUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }


    // Popup Saving
    //   Save API for Vehicle
    SaveVehicle(bu: any): Promise<any> {

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveVehicleUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    //   Save API for Aircraft
    SaveAircraft(bu: any): Promise<any> {

        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveAircraftUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    //   Save API for Section
    SaveSection(bu: any): Promise<any> {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveSectionUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    //   Save API for Receiver
    SaveReceiver(bu: any): Promise<any> {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveReceiverUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }


    //   Save API for Issuer
    SaveIssuer(bu: any): Promise<any> {
        let headers = new Headers({});
        let options = new RequestOptions({ headers });
        return this.authHttp
            .post(this.saveIssuerUrl, bu)
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    getAircraftTypeList(): Promise<any> {
        return this.authHttp.get(this.aircraftUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getBatchNo(TankId:string): Promise<any> {
        return this.authHttp.get(this.getBatchUrl + 'TankId=' + TankId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getLubcricantAvailabilityFromSite(id): Promise<any> {
        return this.authHttp.get(this.getLubricantAvailabilityUrl+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getAllSiteInCountryFromSiteId(id): Promise<any> {
        return this.authHttp.get(this.getAllSiteInCountryFromSiteIdUrl+id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    checkIssueSlipNumber(issueSlipId, siteId): Promise<any> {
        return this.authHttp.get(this.checkIssueSlipNumberUrl+issueSlipId+'&siteId='+siteId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getEquipmentsFromSiteId(issueTypeId, siteId): Promise<any> {
        return this.authHttp.get(this.getEquipmentsFromSiteIdUrl + siteId + '&equipmentTypeId=' + issueTypeId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getStockSummaryList(params) {
        return this.authHttp.get(this.getStockSummaryListUrl, {search: params})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getSiteList(userid: any): Promise<any> {
        return this.authHttp.get(this.sitedropdownUrl + userid)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}