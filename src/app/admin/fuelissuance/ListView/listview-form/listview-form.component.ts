import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { URLSearchParams } from '@angular/http';
import { ListViewFormService } from 'app/admin/fuelissuance/ListView/services/listview.service';
import { IMyOptions } from 'mydatepicker';

@Component({

  selector: 'listview-form',
  template: require('./listview-form.html')
})

export class ListViewFormComponent implements OnInit, OnChanges {
  userId: string;
  fuelTempEnd: any;
  fuelTempStart: any;
  customerReq: string;
  uomId: any;
  conversionFactor: any = 1;
  sitedropdownlistOr = [];
  typeflag: boolean = false;
  vehiclebySite: any;
  aircrafttypeList: any;
  terrorownershipinfo: string = "Ownership Info  is required";
  terrorsection: string = "Section is required";
  terrorsite: string = "Basestation  is required";
  terrordepartment: string = "Department  is required";
  terrorcustomer: string = "Customer is required";
  terrorcallsign: string = "Call Sign/Flight No is required";
  terrormaxfuelfillcapacity: string = "Fuel capacity is required";
  terroraircrafttype: string = "Aircraft type is required";
  terrorflightno: string = "Tail Registration No is required";
  ierrorfirstName: string = "FirstName is required";
  ierrorsite: string = "Site is required";
  ierrorunid: string = "UNID  is required";
  rerrorfirstName: string = "FirstName is required";;
  rerrorlastname: string = "LastName is required";
  rerrorsite: string = "Site is required";
  rerrorcustomer: string = "Customer is required";
  rerrordepartment: string = "Department is required"
  rerrorunid: string = "UNID is required";
  rerrorsection: string = "Section is required";
  quantityalert: string = '';
  availableQuantity: any;
  capacity: any;
  batchnumberlist: any = [];
  tankid: any=0;
  tastm: string = '';
  dastm='';
  issuingDateViewPage : any = '';

  aircraftlist: any;
  numberofpacks: any = 0;;
  lubeList: any = [];
  lubealertmessage: string = '';
  lubeId: any = 0;
  initialvalue: any = 0;
  errorownershipinfo: string = "Ownership Info is required";
  errorsection: string = "Section is required";
  Serrorsection: string = "Section  is required";
  errorbasestation: string = "Basestation is required";
  errordepartment: string = "Department is required";
  errorcustomer: string = "Customer is required";
  POLId: any;
  grList = [];
  idalertmessage: string='';
  gId = 0;
  nullalertmessage: string = '';
  errorvehicletype: string = "Vehicle Type is required";
  errormakemodel: string = "Make and Model is required";
  errorvehicle: string = "VehicleId is required";
  ownershipinfoList: any;
  vehicletypeList: any;
  makemodelList: any;
  departmentList: any;
  pollist: any;
  storagetanklist: any = [];
  SiteId: any;
  receiverReq: string;
  issuedByReq: string;
  sectionReq: string;
  slipnoReq: string;
  issuetypeReq: string;
  tankReq: string;
  uomReq: string;
  siteReq: string;
  polreq:string;
  tempReq: string;
  densityReq: string;
  meterStartedReq: string;
  meterEndedReq: string;
  quantityIssuedReq: string;
  quantityIssuedAt15Req: string;
  drumsReq: string;
  tailRegReq: string;
  odometerReq: string;
  savebutton: boolean = true;
  quantityissued15: number = 0.0;;
  Temperature: number = 0.0;
  Density: number = 0.0;
  batchnumber: any=[];
  receivername: any;
  volumeofpreviousissue: any;
  dateofpreviousissue: any;
  odometerreading: any;
  stockmessage: string = '';
  siteId: any;
  polId: any;
  stock: any;
  public dateTime2: Date;
  quantityissued: number = 0;
  meterended: number = 0;
  meterstarted: number = 0;
  IssueTypeName: any;
  packsize: any;
  package: any;
  NonSiteVehicle: boolean = false;
  IsLubricantIssue: boolean = false;
  IsWaterTest: boolean = true;
  IsVisualInspection: boolean = true;
  IsSedimentFree: boolean = true;
  registration: boolean = false;
  uomdropdownlist: any;
  Make: any;
  public loading: boolean;
  generatorlist: any = [];
  IssuerId: any = 0;
  ReceiverId: any = 0;
  issuedbylist: any = [];
  issuernamelist: any;
  receiverlist: any = [];
  sectionlist: any;
  customerlist: any;
  IsIssuetype: string = "";
  registrationnumberdropdownlist: any;
  tankdropdownlist: any;
  poldropdownlist: any;
  sitedropdownlist = [];
  startedmessage = '';
  odometermessage = '';
  issuerlist: any;
  IsDrums: boolean  = false;
  filelength: number;
  vfilelength: number;
  sizeError: string;
  ErrorList = [];
  filetype = [];
  filelist = [];
  vErrorList = [];
  vfiletype = [];
  vfilelist = [];
  notificationMessage : any = "";
  vehicleForm: FormGroup;
  sectionForm: FormGroup;
  receiverForm: FormGroup;
  issuerForm:FormGroup;
  aircraftForm:FormGroup;
  private currentdate = new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
  };
  public IssuingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  fuelissuanceForm: FormGroup;
  params: URLSearchParams = new URLSearchParams();
  paramsLube: URLSearchParams = new URLSearchParams();
  public formSubmited = false;
  public vehicleFormSubmited = false;
  public receiverFormSubmited = false;
  public issuerFormSubmited = false;
  public sectionFormSubmited = false;
  public tailRegistrationFormSubmited = false;
  vehicleRegReq: string;
  lubricantError = '';
  lubeAvailabilityNotificationMessage = [];
  compBatchList = [];
  batchDetailsList = [];
  batchList = [];
  batchListClone = [];
  compartmentVolume = [];
  arrangedBatchList = [];
  meterStarted : any = '';
  meterEnded : any = '';
  prList: any;
  isViewPage: boolean = false;

  constructor(private fb: FormBuilder, private service: ListViewFormService,
    private router: Router, private route: ActivatedRoute) {
    if(this.route.snapshot.url.length != 0) {
      if(this.route.snapshot.url[0].path == "view") {
        this.isViewPage = true;
        this.page = 'view';
      } else {
        this.isViewPage = false;
      }
    } else {
      this.isViewPage = false;
    }

    this.userId=localStorage.getItem('user_nameId');
    this.params.set('UserId', localStorage.getItem('user_nameId'));

    // if(new Date().getDate() < 10) {
    //   this.IssuingDate = { date: { day: '0' + this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    // }

    this.vehicleForm = fb.group({
      'Id': [''],
      'vehicleId': ['', Validators.compose([Validators.required])],
      'vehicleTypeId': ['', Validators.compose([Validators.required])],
      'startingOdometer': [''],
      'makemodelId': ['', Validators.compose([Validators.required])],
      'maxfuelfillcapacity': [''],
      'customer': ['', Validators.compose([Validators.required])],
      'department': ['', Validators.compose([Validators.required])],
      'basestation': ['', Validators.compose([Validators.required])],
      'section': ['', Validators.compose([Validators.required])],
      'ownershipinfo': ['', Validators.compose([Validators.required])],
      'BarCode': [''],
      'FileType': [],
    });
    var cForm = this.vehicleForm;

    this.sectionForm = fb.group({
      'Id': [''],
      'section': ['', Validators.compose([Validators.required])],
      'description': [''],
      'FileType': [],
    });
    var sForm = this.sectionForm;

    this.receiverForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'receiverId': [''],
      'lastName': ['', Validators.compose([Validators.required])],
      'siteId': ['', Validators.compose([Validators.required])],
      'customerId': ['', Validators.compose([Validators.required])],
      'sectionId': ['', Validators.compose([Validators.required])],
      'departmentId': ['', Validators.compose([Validators.required])],
      'unidNo': ['', Validators.compose([Validators.required])],
      'siteName': [''],
      'customerName': [''],
      'sectionName': [''],
      'FileType':[],
    });
    var rForm = this.receiverForm;

    this.issuerForm = fb.group({
      'firstName': ['', Validators.compose([Validators.required])],
      'issuerId': [''],
      'lastName': ['', ],
      'siteId': ['', Validators.compose([Validators.required])],
      'unidNo': ['',Validators.compose([Validators.required])],
      'FileType':[''],
    });
    var iForm = this.issuerForm;

    this.aircraftForm = fb.group({
      'flightno': ['',Validators.compose([Validators.required])],
      'aircraftTypeId': ['',Validators.compose([Validators.required])],
      'callsign': ['',Validators.compose([Validators.required])],
      'maxfuelfillcapacity': ['',Validators.compose([Validators.required])],
      'customer': ['',Validators.compose([Validators.required])],
      'department': ['',Validators.compose([Validators.required])],
      'basestation': ['',Validators.compose([Validators.required])],
      'section': ['',Validators.compose([Validators.required])],
      'ownershipinfo': ['',Validators.compose([Validators.required])],
      'FileType':[],
    });
    var aForm = this.aircraftForm;
  }

  @Input() listview;
  @Input() error;
  @Input() sectionerror;
  @Input() page: string;
  @Output() saved = new EventEmitter();
  listviewForm: FormGroup;
  public file = '';
  PackSize = 0;
  Package = '';
  showAddBtn = [];
  uom: any;
  isVehicle : boolean = false;
  isAddedAllGeneratores = false;
  startTime : any = '';
  endTime : any = '';
  fuelTimeError : string = '';
  generatorArrayError = "";
  lubricantArrayError = "";
  lubricantList = [];
  isAddedAllLubricants = false;
  showAddLubeBtn = [];
  lubricantErrorMsg = "";
  quantityInLitres = 0;

  ngOnInit() {
    this.getCustomerList();
    this.getDepartmentList();
    this.getSectionList();
    this.getOwnershipInfoList();
    this.getVehicleTypeList();
    this.getMakeModelList();
    this.typeflag = false;
    this.fuelissuanceForm = new FormGroup({
      IsLubricantIssue: new FormControl(false),
      SiteId: new FormControl('', Validators.required),
      IssuingDate: new FormControl('', Validators.required),
      PolId: new FormControl('', [Validators.required]),
      IsDrumIssue: new FormControl(false),
      NoOfDrums: new FormControl('', [Validators.required]),
      TankId: new FormControl('', [Validators.required]),
      NonSiteVehicle: new FormControl(false),
      IssueTypeId: new FormControl('', Validators.required),
      IssueTypeName: new FormControl(''),
      RegistrationNumberId: new FormControl('', Validators.required),
      OdometerReading: new FormControl('', Validators.required),
      IssueSlipNumber: new FormControl('', Validators.required),
      TailRegistrationId: new FormControl('', Validators.required),
      ArrivedFrom: new FormControl(''),
      DepartingTo: new FormControl(''),
      FuelingStartingTime: new FormControl(''),
      FuelingEndingTime: new FormControl(''),
      IsWaterTest: new FormControl(true),
      IsVisualInspection: new FormControl(true),
      IsSedimentFree: new FormControl(true),
      GeneratorIssueTypeform: new FormArray([
        this.initSection([]),
      ]),
      FuelIssuanceLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),
      BatchNumber: new FormControl(''),
      UniqueNumber: new FormControl(''),
      IssuedToCustomer: new FormControl('',[Validators.required]),
      SectionId: new FormControl('', Validators.required),
      ReceiverId: new FormControl('', Validators.required),
      UomId: new FormControl('',[Validators.required]),
      IssuedBy: new FormControl('', [Validators.required]),
      Temperature: new FormControl('',[Validators.required]),
      Density: new FormControl('',[Validators.required]),
      MeterStarted: new FormControl('' ,[Validators.required]),
      QuantityIssued: new FormControl(''),
      MeterEnded: new FormControl('',[Validators.required]),
      QuantityIssued15DegreeC: new FormControl(''),
      Remarks: new FormControl(''),
      FileType: new FormControl(''),
      FuelIssuancePurchaseReceiptDetails: new FormControl([]),
    });
    this.issuetypelist();
    this.sitelist();
    this.productlist();
    //this.storagetanklist();
    // this.registrationnumberlist();

    this.getCustomerList();
    //this.getAircraftList();
    this.getSectionList();
    //this.getReceiverList();
    this.getIssuedbyList();
    this.uomlist();
    this.getAircraftTypeList();
    if(this.isViewPage){
      this.fuelissuanceForm.disable();
    }
   // console.log('oninit');
 //   this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
  }

  initSection(data = []) {
    let GeneratorId = data['generatorId'] ? data['generatorId'] : '';
    let Make = data['make'] ? data['make'] : '';
    let HoursRun = data['hoursPerRun'] ? data['hoursPerRun'] : '';

    if (this.page == 'edit') {
      this.grList.push(GeneratorId);
      this.initialvalue = HoursRun;
      this.gId = GeneratorId;
    }
    return new FormGroup({
      GeneratorId: new FormControl(GeneratorId),
      Make: new FormControl(Make),
      HoursPerRun: new FormControl(HoursRun),
    });
  }

  initLubeSection(data = []) {

    let Product = data['polId'] ? data['polId'] : '';
    let Package = data['Package'] ? data['Package'] : '';
    let PackSize = data['packSize'] ? data['packSize'] : '';
    let NoOfPacks = data['numberOfPackets'] ? data['numberOfPackets'] : '';
    let Volume = data['volume'] ? data['volume'] : '';

    if (this.page == 'edit') {
      this.lubeList.push(Product);
      this.initialvalue = NoOfPacks;
      this.lubeId = Product;
    }
    return new FormGroup({
      POLId: new FormControl(Product),
      Package: new FormControl(Package),
      PackSize: new FormControl(PackSize),
      NumberOfPackets: new FormControl(NoOfPacks),
      Volume: new FormControl(Volume),
    });
  }


  getGeneratorMake(event, section, index) {
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.generatorArrayError = "";
    this.gId = event;
    this.isAddedAllGeneratores = false;
    let isPresent = false;
    this.grList.forEach(generator=>{
      if(generator == event) {
        isPresent = true;
      }
    });
    if(isPresent) {
      this.idalertmessage = "Generator Id is already selected.Choose another one!";
      if(this.grList.length > index-1) {
        if(this.grList[index] == event) {
          this.idalertmessage = '';
        }
      }
    } else {
      if(this.grList.length+1 == this.generatorlist.length) {
        this.grList[index] = event;
      } else
      this.idalertmessage = '';
      this.service.getGeneratorMake(event).then(response => {
        section.controls['Make'].setValue(response.generatorMakeAndModel.make);
        section.controls['HoursPerRun'].enable();
        /// this.Make=response.generatorMakeAndModel.make;
      })
    }
  }

  addSection(item, i) {
    let itemValue = item.value;
    let control = (<FormGroup>(<FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform')).controls[i]).value;
    if(itemValue.GeneratorId == "" || itemValue.HoursPerRun == "" || itemValue.Make == "") {
      this.nullalertmessage = "Generator Details Can't be Empty!"
    } else {
      this.nullalertmessage = "";
      if(this.grList.length == 0) {
        this.grList[i] = control['GeneratorId'];
        if(this.grList.length == this.generatorlist.length) {
          this.isAddedAllGeneratores = true;
        } else {
          this.isAddedAllGeneratores = false;
        }
        if(!this.isAddedAllGeneratores) {
          const generatorControl = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
          generatorControl.push(this.initSection());
          this.showAddBtn[i] = true;
        }
      } else {
          let isGeneratorPresent = false;
          this.grList.forEach(generator => {
            if(generator == this.gId) {
              isGeneratorPresent = true;
            }
          });
          if(!isGeneratorPresent) {
            this.grList[i] = control['GeneratorId'];
            this.showAddBtn[i] = true;
            if(this.grList.length == this.generatorlist.length) {
              this.isAddedAllGeneratores = true;
            } else {
              this.isAddedAllGeneratores = false;
            }
            if(!this.isAddedAllGeneratores) {
              const generatorControl = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
              generatorControl.push(this.initSection());
              this.showAddBtn[i] = true;
            }
            this.idalertmessage = '';
          } else {
            this.idalertmessage = 'Generator is already selected.Choose another one!';
            if(this.grList.length == this.generatorlist.length) {
              this.idalertmessage = '';
            } else if(this.grList.length == i+1) {
              this.showAddBtn[i] = true;
              this.idalertmessage = '';
              const generatorControl = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
              generatorControl.push(this.initSection());
              this.showAddBtn[i] = true;
            }
          }
      }
    }
  }

  removeSection(i, item) {
    const control = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
    control.removeAt(i);
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.generatorArrayError="";
    this.showAddBtn[control.length-1] = false;
    this.grList.splice(i, 1);
    if(this.grList.length == this.generatorlist.length) {
      this.isAddedAllGeneratores = true;
    } else {
      this.isAddedAllGeneratores = false;
    }
  }

  getGeneratorDetails(form) {
    return form.controls.GeneratorIssueTypeform.controls;
  }

  addLubeSection(item, i) {
    let itemValue = item.value;
    let control = (<FormGroup>(<FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails')).controls[i]).value;
    if(itemValue.POLId == "" || itemValue.NumberOfPackets == "" ) {
      this.lubricantErrorMsg = "Lubricant Details Can't be Empty!"
    } else {
      this.lubricantErrorMsg = "";
      if(this.lubricantList.length == 0) {
        this.lubricantList[i] = control['POLId'];
        if(this.lubricantList.length == this.poldropdownlist.length) {
          this.isAddedAllLubricants = true;
        } else {
          this.isAddedAllLubricants = false;
        }
        if(!this.isAddedAllLubricants) {
          const lubeControl = <FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails');
          lubeControl.push(this.initLubeSection());
          this.showAddLubeBtn[i] = true;
        }
      } else {
          let isLubePresent = false;
          this.lubricantList.forEach(lube => {
            if(lube == this.gId) {
              isLubePresent = true;
            }
          });
          if(!isLubePresent) {
            this.lubricantList[i] = control['POLId'];
            this.showAddLubeBtn[i] = true;
            if(this.lubricantList.length == this.poldropdownlist.length) {
              this.isAddedAllLubricants = true;
            } else {
              this.isAddedAllLubricants = false;
            }
            if(!this.isAddedAllLubricants) {
              const lubeControl = <FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails');
              lubeControl.push(this.initLubeSection());
              this.showAddLubeBtn[i] = true;
            }
            this.lubricantErrorMsg = '';
          } else {
            this.lubricantErrorMsg = 'Lubricant is already selected.Choose another one!';
            if(this.lubricantList.length == this.poldropdownlist.length) {
              this.lubricantErrorMsg = '';
            } else if(this.lubricantList.length == i+1) {
              this.showAddLubeBtn[i] = true;
              this.lubricantErrorMsg = '';
              const lubeControl = <FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails');
              lubeControl.push(this.initLubeSection());
              this.showAddLubeBtn[i] = true;
            }
          }
      }
    }
  }

  removeLubeSection(i, item) {
    const control = <FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails');
    control.removeAt(i);
    this.lubricantErrorMsg = "";
    this.lubricantArrayError = "";
    this.lubricantError = "";
    this.showAddLubeBtn[control.length-1] = false;
    this.lubricantList.splice(i, 1);
    if(this.lubricantList.length == this.poldropdownlist.length) {
      this.isAddedAllLubricants = true;
    } else {
      this.isAddedAllLubricants = false;
    }
  }

  onTextChange(event: any, section) {
    this.initialvalue = event
  }

  onPackChange(event: any, item) {
    this.lubricantError = "";
    this.numberofpacks = event;

    item.controls['Volume'].setValue(
      (item.controls['PackSize'].value === '' ? 0 : Number(item.controls['PackSize'].value))
      * (item.controls['NumberOfPackets'].value === '' ? 0 : Number(item.controls['NumberOfPackets'].value))
    );
  }

  // For getting Aircraft Type List
  getAircraftTypeList() {
    this.service.getAircraftTypeList().then(r =>  {
      this.aircrafttypeList = r['result'];
     // this.loading = false;
    });
  }

  // For getting IssueType List
  issuetypelist() {
    this.service.getissuerlist().then(response => {

      this.issuerlist = response['result'];

    })
  }

  // For getting Site List
  sitelist() {
    this.service.getSiteList(this.userId).then(r => {
        this.sitedropdownlist = r;
        for (var i = 0 ; i < this.sitedropdownlistOr.length ; i++) {
          if (this.sitedropdownlistOr[i].isSfr === false) {
            this.sitedropdownlist.push(this.sitedropdownlistOr[i]);
          }
        }
    })
    .catch(r => {
        this.handleError(r);
    });
  }

  // For getting UOM List
  uomlist() {
    this.service.getuomlist().then(response => {
      this.uomdropdownlist = response['result'];
      this.fuelissuanceForm.controls['UomId'].setValue(1);
    })
  }

  //For getting POL List inside product for lubricantcase
  productlist() {
    this.service.getpollist().then(response => {
      this.poldropdownlist = response;
    })
  }

  // For getting Registration Number List
  registrationnumberlist() {
    this.service.getregistrationnumberlist().then(response => {
      this.registrationnumberdropdownlist = response['result'];
    })
  }

  // For getting Customer List
  getCustomerList() {
    this.service.getcustomerlist(this.params).then(response => {
      this.customerlist = response['result'];

    })
  }

  // For getting Aircraft List
  getAircraftList(site) {
    this.service.getaircraftlist(site).then(response => {
      this.aircraftlist = response;
    })
  }

  // For getting Section List
  getSectionList() {
    this.service.getsectionlist().then(response => {
      this.sectionlist = response['result'];

    })
  }

  // For getting Receiver List
  getReceiverList(receiverId = 0, site) {
    this.receiverlist = [];
    this.service.getreceiverlist(site).then(response => {
      this.receiverlist = response;
      /*response['result'].forEach(receiver => {
        if(receiver.siteId == this.SiteId) {
          this.receiverlist.push(receiver);
        }
      });*/
      if(this.page == 'edit') {
        this.fuelissuanceForm.controls['ReceiverId'].setValue(receiverId);
      }
    })
  }

  // For getting Issued by List
  getIssuedbyList(issuerId = 0) {
    this.service.getissuedbylist().then(response => {
      this.issuedbylist = [];
      response['result'].forEach(issuer => {
        if(issuer.siteId == this.SiteId) {
          this.issuedbylist.push(issuer);
        }
      });
      if(this.page == 'edit') {
        this.fuelissuanceForm.controls['IssuedBy'].setValue(issuerId);
      }
    })
  }

  // For getting generatorlist
  generatorList() {
    this.generatorlist = [];
    this.service.getgeneratorlist().then(response => {
      response['result'].forEach(generator => {
        if (generator.siteId == this.SiteId) {
          this.generatorlist.push(generator);
        }
      });
    })
  }

  // getRegistrationNoDetails(id) {
    getRegistrationNoDetails(event) {
      var vid = this.fuelissuanceForm.get('RegistrationNumberId').value;

   
    this.service.getRegistrationNoDetails(vid).then(response => {
      this.registration = false;
      if (response!= null) {
        this.registration = true;
        this.dateofpreviousissue = response.issueDate;
        this.volumeofpreviousissue = response.quantityIssued;
        this.odometerreading = response.odoMeterReading;
        this.receivername = response.receiver.firstName + response.receiver.lastName;
      }
      //this.companyName=response.transportCompany.name;
    })
  }

  onOdometerChange(value) {
    if(this.odometerreading > value) {
      this.odometermessage = "Entered Odometer Reading must be greater than previous odometer reading";
    } else {
      this.odometermessage="";
    }
  }

  getUom(id) {
    this.uomId = id;
    let other = [];
    this.quantityInLitres = 0;
    this.uomdropdownlist.forEach(element => {
      if(element.id == id) {
        other.push(element);
        this.conversionFactor = element.convertionFactor;
      }
    });
    this.quantityInLitres = this.quantityissued * this.conversionFactor;
    this.calculation15();
  }

  issueTypeId : any;
  equipmentList : any = [];
  getIssueType(event) {
    this.fuelissuanceForm.controls['IssueSlipNumber'].setValue('');
    this.fuelissuanceForm.controls['UniqueNumber'].setValue('');
    this.fuelissuanceForm.controls['OdometerReading'].setValue('');
    this.fuelissuanceForm.controls['RegistrationNumberId'].setValue('');
    this.fuelissuanceForm.controls['NonSiteVehicle'].setValue(false);
    this.fuelissuanceForm.controls['TailRegistrationId'].setValue('');
    this.fuelissuanceForm.controls['ArrivedFrom'].setValue('');
    this.fuelissuanceForm.controls['DepartingTo'].setValue('');
    this.fuelissuanceForm.controls['FuelingStartingTime'].setValue('');
    this.fuelissuanceForm.controls['FuelingEndingTime'].setValue('');
    this.fuelissuanceForm.controls['IsWaterTest'].setValue(true);
    this.fuelissuanceForm.controls['IsVisualInspection'].setValue(true);
    this.fuelissuanceForm.controls['IsSedimentFree'].setValue(true);
    const generatorControl = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
    let generatorControlLength = generatorControl.length;
    for( let i = 0; i < generatorControlLength; i++) {
      generatorControl.removeAt(0);
    }
    generatorControl.push(this.initSection());
    this.registration = false;
    this.odometermessage="";
    this.fuelTimeError = '';
    this.nullalertmessage = '';
    this.tailRegReq = "";
    this.generatorArrayError = "";
    this.slipnoReq = "";
    this.idalertmessage = '';
    this.generatorArrayError = "";
    this.vehicleRegReq = "";
    this.odometerReq = "";
    this.registrationnumberdropdownlist = [];
    this.typeflag = false;
    this.service.getIssueTypeName(event).then(response => {
      if (response.name.toUpperCase() == 'VEHICLE' || response.name.toUpperCase() == 'AIRCRAFT' || response.name.toUpperCase() == 'GENERATOR') {
        this.IsIssuetype = response.name.toUpperCase();
        if(this.IsIssuetype == 'VEHICLE') {
          this.isVehicle = true;
          if(this.SiteId != "" && typeof(this.SiteId) != "undefined" && this.page == 'add')  {
            //this.getVehiclelistBySiteId(this.SiteId,  this.typeflag);
            this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
          }
        } else {
          this.isVehicle = false ;
        }
        this.generatorList();
      } else {
        this.IsIssuetype = 'Others';
        this.issueTypeId = event;
        if(this.siteId) {
          this.service.getEquipmentsFromSiteId(event, this.siteId).then(response => {
            this.equipmentList = response;
          })
        }
      }
    })
  }

  getReceiverId(event) {
    //this.ReceiverId = event;
     this.service.getreceiverdetails(event).then(response=> {
       this.ReceiverId = response.unidNo;
    });
  }

  getIssuerId(event) {
    //this.IssuerId = event;
    this.service.getissuerdetails(event).then(response=> {
      this.IssuerId = response.unidNo;
    });
  }

  onDateChanged(event: any) {
    this.IssuingDate = event.formatted;
  }

  selectAll(event, type) {

    if(type === 'lubricant') {
      this.clearmsgs()
      this.clearForm(event.target.checked);
    }
    if(this.SiteId == '' || this.SiteId == null || this.SiteId == undefined) {
      this.SiteId = 0;
    }
    this.typeflag = event.target.checked;
    if(event.target.checked == true) {
      if(type === 'drums') {
        this.IsDrums = true;
        this.fuelissuanceForm.controls['IsDrumIssue'].setValue(true);
      }
      if(type === 'watertest') {
        this.fuelissuanceForm.controls['IsWaterTest'].setValue(true);
      }
      if(type === 'sedimentfree') {
        this.fuelissuanceForm.controls['IsSedimentFree'].setValue(true);
      }
      if(type === 'visualinspection') {
        this.fuelissuanceForm.controls['IsVisualInspection'].setValue(true);
      }
      if(type === 'lubricant') {
        this.IsLubricantIssue = true;
        this.IsDrums = false;
        let controls = (<FormArray>this.fuelissuanceForm.controls['FuelIssuanceLubeDetails']).controls;
       for(let i = 0; i< controls.length ; i++) {
          (<FormArray>controls[i]).controls['POLId'].setValidators([Validators.required]);
          (<FormArray>controls[i]).controls['NumberOfPackets'].setValidators([Validators.required]);
       }
        this.fuelissuanceForm.controls['IsLubricantIssue'].setValue(true);
        this.fuelissuanceForm.get('PolId').setErrors(null);
        this.fuelissuanceForm.get('TankId').setErrors(null);
        this.fuelissuanceForm.get('UomId').setErrors(null);
        this.fuelissuanceForm.get('Temperature').setErrors(null);
        this.fuelissuanceForm.get('Density').setErrors(null);
        this.fuelissuanceForm.get('MeterStarted').setErrors(null);
        this.fuelissuanceForm.get('QuantityIssued').setErrors(null);
        this.fuelissuanceForm.get('MeterEnded').setErrors(null);
        this.fuelissuanceForm.get('QuantityIssued15DegreeC').setErrors(null);
        this.tempReq = "";
        this.densityReq = "";
        this.meterStartedReq = "";
        this.meterEndedReq = "";
        this.quantityIssuedReq = "";
        this.quantityIssuedAt15Req = "";
      }
      // else {
      //   this.fuelissuanceForm.get('PolId').setValidators([Validators.required]);
      //   this.fuelissuanceForm.get('TankId').setValidators([Validators.required]);
      //   this.fuelissuanceForm.get('UomId').setValidators([Validators.required]);
      // }
      if(type === 'nonsite') {

        this.NonSiteVehicle=true;
        this.fuelissuanceForm.controls['NonSiteVehicle'].setValue(true);
         //this.registrationnumberlist();
        this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag , this.userId );
      }
    } else {
      if(type === 'drums') {
        this.IsDrums=false;
        this.fuelissuanceForm.controls['IsDrumIssue'].setValue(false);
      }
      if(type === 'lubricant'){
        this.IsLubricantIssue=false;
        this.tempReq = "";
        this.densityReq = "";
        this.meterStartedReq = "";
        this.meterEndedReq = "";
        this.quantityIssuedReq = "";
        this.quantityIssuedAt15Req = "";
        let controls = (<FormArray>this.fuelissuanceForm.controls['FuelIssuanceLubeDetails']).controls;
        for(let i = 0; i< controls.length ; i++) {
            (<FormArray>controls[i]).controls['POLId'].setErrors(null);
            (<FormArray>controls[i]).controls['NumberOfPackets'].setErrors(null);
        }
        this.fuelissuanceForm.controls['IsLubricantIssue'].setValue(false);
        this.fuelissuanceForm.get('PolId').setValidators([Validators.required]);
        this.fuelissuanceForm.get('TankId').setValidators([Validators.required]);
        this.fuelissuanceForm.get('UomId').setValidators([Validators.required]);
        this.fuelissuanceForm.get('Temperature').setValidators([Validators.required]);
        this.fuelissuanceForm.get('Density').setValidators([Validators.required]);
        this.fuelissuanceForm.get('MeterStarted').setValidators([Validators.required]);
        this.fuelissuanceForm.get('QuantityIssued').setValidators([Validators.required]);
        this.fuelissuanceForm.get('MeterEnded').setValidators([Validators.required]);
        this.fuelissuanceForm.get('QuantityIssued15DegreeC').setValidators([Validators.required]);
      }
      if(type === 'nonsite') {
 
        this.NonSiteVehicle=event.target.checked;
        this.fuelissuanceForm.controls['NonSiteVehicle'].setValue(event.target.checked);
       // this.getVehiclelistBySiteId(this.SiteId,  this.typeflag);
        this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag , this.userId );
      }
      if(type === 'watertest') {
        this.fuelissuanceForm.controls['IsWaterTest'].setValue(false);
      }
      if(type === 'sedimentfree') {
        this.fuelissuanceForm.controls['IsSedimentFree'].setValue(false);
      }
      if(type === 'visualinspection') {
        this.fuelissuanceForm.controls['IsVisualInspection'].setValue(false);
      }
    }
  }

  updated($event) {
    const files = $event.target.files || $event.srcElement.files;
    this.file = files[0];
  }

  onChange(event) {
    this.ErrorList = [];
    this.filelist = [];
    this.filetype = [];
    this.filelist = <Array<File>>event.target.files;
    for(let i = 0; i < this.filelist.length; i++) {
      let fSize = this.filelist[i].size;
      let fName = this.filelist[i].name;
      let extnsion = fName.substr(fName.lastIndexOf('.') + 1);
      if(fSize > 2097152) {
        this.ErrorList.push("The file " + this.filelist[i].name + " is too large. Maximum file size permitted is 2 Mb");
      } else if (extnsion != "pdf" && extnsion != "doc" && extnsion != "docx" && extnsion != "xls" && extnsion != "xlsx" && extnsion != "png" && extnsion != "jpg" && extnsion != "jpeg" && extnsion != "gif" && extnsion != "txt") {
        this.ErrorList.push("The selected file " + this.filelist[i].name + " is a an unsupported file");
      } else {
        this.ErrorList = [];
        this.filetype.push(this.filelist[i]);
        this.filelength = this.filetype.length;
      }
    }
  }

  getFuelIssuanceLubeDetails(form) {
    return form.controls.FuelIssuanceLubeDetails.controls;
  }

  getPolDetails(event, section, index) {
    this.lubricantError = "";
    this.nullalertmessage = "";
    this.idalertmessage = "";
    this.lubricantErrorMsg = "";
    this.lubricantArrayError = "";
    this.lubeId = event;
    this.isAddedAllLubricants = false;
    let isPresent = false;
    this.lubricantList.forEach(lube=>{
      if(lube == event) {
        isPresent = true;
      }
    });
    if(isPresent) {
      this.lubricantErrorMsg = "Lubricant is already selected.Choose another one!";
      if(this.lubricantList.length > index-1) {
        if(this.lubricantList[index] == event) {
          this.lubricantErrorMsg = '';
        }
      }
    } else {
      if(this.lubricantList.length+1 == this.poldropdownlist.length) {
        this.lubricantList[index] = event;
      }
      this.lubricantErrorMsg = '';
      this.service.getPolDetailsById(event).then(response => {
        section.controls['Package'].setValue(response.uom.name);
        section.controls['PackSize'].setValue(response.packSize);
        section.controls['NumberOfPackets'].setValue('');
        section.controls['Volume'].setValue(0);
        section.controls['NumberOfPackets'].enable();
      });
      if(this.lubeAvailabilityList.length > 0) {
        for(let lubricant of this.lubeAvailabilityList) {
          if(event == lubricant.polId) {
            this.lubeAvailabilityNotificationMessage[index]=" Available No of Packets: " + lubricant.availableNumberOfPackets;
            break;
          } else {
            this.lubeAvailabilityNotificationMessage[index]=" Available No of Packets: " + 0;
          }
        }
      } else {
        this.lubeAvailabilityNotificationMessage[index]=" Available No of Packets: " + 0;
      }
    }
	}

  vehicleRegistrationNumberId : any = '';
  ngOnChanges(change) {
    this.loading = this.page == 'add' ? false : true;
    if (change.listview && change.listview.currentValue) {
      this.fuelIssuancePrint = change.listview.currentValue;
      this.getIssueType(change.listview.currentValue.equipmentTypeId);
      this.getPolListFromSiteId(change.listview.currentValue.siteId);
      //this.getPolListFromSite(change.listview.currentValue.siteId);
      if(change.listview.currentValue.polId) {
        this.getStorageTankListFromPol(change.listview.currentValue.polId);
      }
      // this.registrationnumberlist();
      //this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
      this.uomlist();
      this.productlist();

      this.loading = false;
      this.fuelissuanceForm.get('PolId').clearValidators();
      this.fuelissuanceForm.get('TankId').clearValidators();
      this.fuelissuanceForm.get('UomId').clearValidators();
      this.service.getuomlist().then(response => {
        this.uomdropdownlist = response['result'];
        this.fuelissuanceForm.controls['UomId'].setValue(change.listview.currentValue.uomId);
      })
      this.fuelissuanceForm.controls['IsLubricantIssue'].setValue(change.listview.currentValue.isLubricant);
      this.fuelissuanceForm.controls['SiteId'].setValue(change.listview.currentValue.siteId);
      let issuingDate = new Date(change.listview.currentValue.issuingDate);
      if(issuingDate != null) {
        this.issuingDateViewPage = issuingDate.getDate() + '-' + Number(issuingDate.getMonth()+1) + '-' + issuingDate.getFullYear();
        let date: Object = { date: { day: issuingDate.getDate(), month: issuingDate.getMonth() + 1, year: issuingDate.getFullYear() } };
        this.fuelissuanceForm.controls['IssuingDate'].setValue(date);
      }
      this.fuelissuanceForm.controls['PolId'].setValue(change.listview.currentValue.polId);

      this.fuelissuanceForm.controls['IsDrumIssue'].setValue(change.listview.currentValue.drumIssue);
      this.fuelissuanceForm.controls['IsSedimentFree'].setValue(change.listview.currentValue.sedimentFree);
      this.fuelissuanceForm.controls['IsVisualInspection'].setValue(change.listview.currentValue.visualInspection);
      this.fuelissuanceForm.controls['IsWaterTest'].setValue(change.listview.currentValue.waterTest);
      this.fuelissuanceForm.controls['NoOfDrums'].setValue(change.listview.currentValue.numberOfDums);
      this.fuelissuanceForm.controls['TankId'].setValue(change.listview.currentValue.storageTankId);
      this.fuelissuanceForm.controls['IssueTypeId'].setValue(change.listview.currentValue.equipmentTypeId);
      this.fuelissuanceForm.controls['IssueTypeName'].setValue(change.listview.currentValue.equipmentTypeId);
      this.fuelissuanceForm.controls['OdometerReading'].setValue(change.listview.currentValue.odoMeterReading);
      this.fuelissuanceForm.controls['IssueSlipNumber'].setValue(change.listview.currentValue.issueSlipNumber); 
      this.fuelissuanceForm.controls['NonSiteVehicle'].setValue(change.listview.currentValue.nonSiteVehicle);
      if(change.listview.currentValue.nonSiteVehicle) {
        var site = change.listview.currentValue.siteId; 
        this.service.getVehicleBySiteAndUser(site, false, this.userId).then(response => {
      //  this.service.getregistrationnumberlist().then(response => {
          // this.registrationnumberdropdownlist = response['result'];
          // this.fuelissuanceForm.controls['RegistrationNumberId'].setValue(change.listview.currentValue.vehicleId);
          this.registrationnumberdropdownlist = response;
          this.fuelissuanceForm.controls['RegistrationNumberId'].setValue(change.listview.currentValue.vehicleId);
        })
        // this.fuelissuanceForm.controls['RegistrationNumberId'].setValue(change.listview.currentValue.vehicleId);
        this.vehicleRegistrationNumberId = change.listview.currentValue.vehicleId;
      }else{
        var site = change.listview.currentValue.siteId; 
        this.service.getVehicleBySiteAndUser(site,true, this.userId).then(response => {
          this.registrationnumberdropdownlist = response;
          this.fuelissuanceForm.controls['RegistrationNumberId'].setValue(change.listview.currentValue.vehicleId);
        })
      }
      this.service.getaircraftlist(change.listview.currentValue.siteId).then(response => {
        this.aircraftlist = response;
        this.fuelissuanceForm.controls['TailRegistrationId'].setValue(change.listview.currentValue.aircraftId);
      })
      this.fuelissuanceForm.controls['ArrivedFrom'].setValue(change.listview.currentValue.arrivedFromSiteId);
      this.fuelissuanceForm.controls['DepartingTo'].setValue(change.listview.currentValue.departingtoSiteId);
      this.fuelissuanceForm.controls['IssuedToCustomer'].setValue(change.listview.currentValue.customerId);
      this.fuelissuanceForm.controls['SectionId'].setValue(change.listview.currentValue.sectionId);
      this.getReceiverId(change.listview.currentValue.receiverId);
      this.getIssuerId(change.listview.currentValue.issuerId);
      if(this.page == 'edit') {
        this.getReceiverList(change.listview.currentValue.receiverId, change.listview.currentValue.siteId);
        this.getIssuedbyList(change.listview.currentValue.issuerId);
      }
      this.fuelissuanceForm.controls['Temperature'].setValue(change.listview.currentValue.temperature);
      this.fuelissuanceForm.controls['Density'].setValue(change.listview.currentValue.density);
      this.fuelissuanceForm.controls['MeterStarted'].setValue(change.listview.currentValue.meterStarted);
      this.fuelissuanceForm.controls['QuantityIssued'].setValue(change.listview.currentValue.quantityIssued);
      this.fuelissuanceForm.controls['MeterEnded'].setValue(change.listview.currentValue.meterEnded);
      this.fuelissuanceForm.controls['QuantityIssued15DegreeC'].setValue(change.listview.currentValue.quantityIssued15DegreeC);
      this.fuelissuanceForm.controls['Remarks'].setValue(change.listview.currentValue.remarks);
      this.getEquipmentList(change.listview.currentValue.siteId);
      this.service.getEquipmentsFromSiteId(change.listview.currentValue.equipmentTypeId, change.listview.currentValue.siteId).then(response => {
        this.equipmentList = response;
        this.fuelissuanceForm.controls['UniqueNumber'].setValue(change.listview.currentValue.uniqueNumber);
      })
      this.fuelissuanceForm.controls['BatchNumber'].setValue(change.listview.currentValue.batchNumber);
      this.compBatchList.push(change.listview.currentValue.batchNumber);
      //  this.fuelissuanceForm.controls['FuelingStartingTime'].setValue(change.listview.currentValue.fuelStartTime);
      // this.fuelissuanceForm.controls['FuelingEndingTime'].setValue(change.listview.currentValue.fuelEndTime);

      if (change.listview.currentValue.fuelStartTime != '' ||
      change.listview.currentValue.fuelStartTime != null ||
      change.listview.currentValue.fuelStartTime != undefined) {
            this.fuelTempStart = change.listview.currentValue.fuelStartTime;
            var d1 = new Date( this.fuelTempStart);
            var minute = d1.getMinutes();
            var hour = d1.getHours();
            let StartTimeFinal = (hour < 10 ? '0' + hour : hour) + ':' + (minute < 10 ? '0' + minute : minute);
            this.fuelissuanceForm.controls['FuelingStartingTime'].setValue(StartTimeFinal);
      }
      if (change.listview.currentValue.fuelEndTime != '' || change.listview.currentValue.fuelEndTime != null || change.listview.currentValue.fuelEndTime != undefined) {
          this.fuelTempEnd = change.listview.currentValue.fuelEndTime;
          var d1 = new Date( this.fuelTempEnd);
          var minute = d1.getMinutes();
          var hour = d1.getHours();
                    let EndTimeFinal = (hour < 10 ? '0' + hour : hour) + ':' + (minute < 10 ? '0' + minute : minute);
                    this.fuelissuanceForm.controls['FuelingEndingTime'].setValue(EndTimeFinal);
      }
      this.fuelissuanceForm.controls['FileType'].setValue(this.filetype);
      this.meterstarted = change.listview.currentValue.meterStarted;
      this.meterended = change.listview.currentValue.meterEnded;
      if (change.listview.currentValue.isLubricant == false) {
        this.IsLubricantIssue = false;
        this.fuelissuanceForm.get('PolId').clearValidators();
        this.fuelissuanceForm.get('TankId').clearValidators();
        this.fuelissuanceForm.get('UomId').clearValidators();
      }
      if (change.listview.currentValue.isLubricant == true) {
        this.IsLubricantIssue = true;
        this.fuelissuanceForm.get('PolId').clearValidators();
        this.fuelissuanceForm.get('TankId').clearValidators();
        this.fuelissuanceForm.get('UomId').clearValidators();
      }
      if(change.listview.currentValue.drumIssue == true) {
        this.IsDrums=true;
      }
      if (change.listview.currentValue.fuelIssuanceGeneratorDetails.length  > 0) {
        let generatorvalue = change.listview.currentValue.fuelIssuanceGeneratorDetails;
        // let valuchanged = [];
        let items = this.fuelissuanceForm.get('GeneratorIssueTypeform') as FormArray;
        generatorvalue.forEach(element => {
          // valuchanged.push(this.initSection(element));
          items.push(this.initSection(element));
        });
        items.removeAt(0);
      }
      if (change.listview.currentValue.fuelIssuanceLubricantDetails.length  > 0) {
        let lubvalue = change.listview.currentValue.fuelIssuanceLubricantDetails;
        let itemslub = this.fuelissuanceForm.get('FuelIssuanceLubeDetails') as FormArray;
        let i = 0;
        lubvalue.forEach(element => {
          itemslub.push(this.initLubeSection(element));
          this.service.getPolDetailsById(element.polId).then(response => {
            (<FormGroup>itemslub.controls[i]).controls['Package'].setValue(response.uom.name);
            i++;
          });
        });
        itemslub.removeAt(0);
      }
      if (change.listview.currentValue.polId == null) {
        change.listview.currentValue.polId = 0;
      }
      let purchaserecpt= change.listview.currentValue.fuelIssuancePurchaseReceiptDetails;
      if(purchaserecpt == null){
        purchaserecpt = [];
      }
      let purchaseidlist =[];
      for(let i = 0 ; i<purchaserecpt.length ; i++){
        purchaseidlist.push(purchaserecpt[i].purchaseReceiptId);
      }
      this.fuelissuanceForm.controls['FuelIssuancePurchaseReceiptDetails'].setValue(purchaseidlist);
      this.disableControls();
    } else {
      this.loading = false;
    }
  }

  disableControls() {
    if(this.page) {
      this.fuelissuanceForm.controls['IsLubricantIssue'].disable();
      this.fuelissuanceForm.controls['IsDrumIssue'].disable();
      this.fuelissuanceForm.controls['IssueTypeName'].disable();
    }
  }

  /**allow only numbers */
  _keyPress(event: any) {
    const pattern = /[0-9\.]/;
    let inputChar = String.fromCharCode(event.charCode);
    let charCode = (typeof event.which == "number") ? event.which : event.keyCode;
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      return false;
    }
  }

  issueSlipErrorMsg = '';
  issueSlipId= '';
  onIssueSlipChange(event: any, control) {
    this.issueSlipErrorMsg = "";
    if(control === 'site') {
      this.siteId = event.target.value;
      if(this.issueSlipId) {
        this.service.checkIssueSlipNumber(this.issueSlipId, this.siteId).then(response => {
          this.issueSlipErrorMsg = response;
        });
      }
    } else if(control === 'issueSlip') {
      this.issueSlipId = event.target.value;
      if(this.siteId) {
        this.service.checkIssueSlipNumber(this.issueSlipId, this.siteId).then(response => {
          this.issueSlipErrorMsg = response;
        });
      }
    }
  }

  /**allow numbers with two decimal points */
  _keyMPress(event){
    var number = event.target.value
    var wholePart = Math.floor(Number(number));
    var wholePartLength = wholePart.toString().length;
    var fractionPart = Number(number) % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 ) && (charCode < 48 || charCode > 57) && ( charCode != 46 )) {
        return false;
    }
    if(noOfDigits >= 2) {
        return false;
    } else {
        return true;
    }
  }

  /**allow numbers with four decimal points */
  _keyTDPress(event){
    var number = event.target.value
    var wholePart = Math.floor(Number(number));
    var wholePartLength = wholePart.toString().length;
    var fractionPart = Number(number) % 1;
    var fractionPartLength = (number.toString().length - wholePartLength) - 1;
    fractionPart = Math.round(fractionPart*(Math.pow(10,fractionPartLength)));
    var noOfDigits = fractionPart.toString().length;
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 ) && (charCode < 48 || charCode > 57) && ( charCode != 46 )) {
        return false;
    }
    if(noOfDigits >= 4) {
        return false;
    } else {
        return true;
    }
  }

  onDensityChange(value) {
    let no = Number(value);
    if((no < 0.670)  || (no > 1.06) || (value == '.') || value == ''  ) {
      this.dastm = "Density should be between 0.670 and 1.06";
    } else {
     this.dastm ="";
     this.Density = value;
     this.calculation15();
    }
  }

  onTemperatureChange(value) {
    let no = Number(value);
    if(no > 50 || (value == '.') || value == '') {
      this.tastm = "Temperature should be between 0 and 50";
    } else {
     this.tastm ="";
     this.Temperature = value;
     this.calculation15();
    }
  }

  calculation15() {
    this.quantityInLitres = this.quantityissued * this.conversionFactor;
    this.service.getCalculateQuantityAt15DegreeCelsius(this.Density.toString(), this.Temperature.toString(),
      this.quantityInLitres).then(response => {
        this.quantityissued15 = response.quantityAt15Degree;
        this.fuelissuanceForm.controls['QuantityIssued15DegreeC'].setValue(this.quantityissued15);
      })
  }

  meterstartedcalculation(value) {
    this.meterstarted = Number(value);
    this.metercalculation();
  }

  meterendedcalculation(value) {
    this.meterended = Number(value);
    this.metercalculation();
  }

  getTankId(id) {
    this.notificationMessage = "";
    this.tankid=id;
    this.compBatchList = [];
    this.batchDetailsList = [];
    this.service.getTankCapacity(id).then(response => {
      this.availableQuantity = response.availableQuantity;
      this.notificationMessage="Available Quantity : " + this.availableQuantity;
      this.getBatchFromTank(this.tankid);
      this.batchDetailsList = [];
    });
    this.quantitycheck();
    this.quantityalert = "";
  }

  metercalculation() {
    if (Number(this.meterended) != 0 && (Number(this.meterstarted) >= Number(this.meterended))) {
      this.startedmessage = "Meter Ended should be greater than Meter Started";
    } else {
      this.startedmessage = '';
      this.quantityissued = this.meterended - this.meterstarted;
      this.fuelissuanceForm.controls['QuantityIssued'].setValue(this.quantityissued);
      this.service.getBatchNumber(this.tankid,this.quantityissued).then(response => {
        let batchnumberlist =[];
        let purchaseidlist =[];
        for(let i =0 ; i<  response.purchaseReceipt.length ; i++) {
          batchnumberlist.push(response.purchaseReceipt[i].batchNumber);
          purchaseidlist.push(response.purchaseReceipt[i].purchaseReceiptId);
        }
        this.fuelissuanceForm.controls['BatchNumber'].setValue(batchnumberlist);
        this.fuelissuanceForm.controls['FuelIssuancePurchaseReceiptDetails'].setValue(purchaseidlist)
      });
    }
    if(Number(this.meterended) != 0 && Number(this.quantityissued) < 0 ) {
      this.startedmessage = "Meter Started must be less than  Meter Ended"
    }
    if(this.siteId != undefined && this.polId != undefined) {
        this.service.getpolstock(this.siteId, this.polId).then(response => {
        this.stock = response.availableQuantity;
        if (Number(this.quantityissued) <= Number(this.stock)) {
          this.stockmessage = ''
        } else {
          this.stockmessage = "Quantity Issued must be less than  Actual"
        }
      })
    }
    this.calculation15();
    this.quantitycheck();
  }

  quantitycheck(){
    if(this.quantityissued > this.availableQuantity) {
      this.quantityalert = "Insufficient POL in tank";
    } else {
      this.quantityalert = "";
    }
  }

  getSite(value) {
    this.siteId = value;
  }

  getPol(value) {
    this.polId = value;
  }

  onSubmit(validPost) {
    this.clearerror();
    this.notificationMessage = "";
    this.odometermessage = "";
    if(this.page == 'edit') {
      validPost.IsLubricantIssue = this.IsLubricantIssue;
      validPost.IsDrumIssue = this.IsDrums;
      if(!this.IsLubricantIssue) {
        validPost.FuelIssuanceLubeDetails = [];
      }
    }
    if(validPost.FuelIssuanceLubeDetails.length != 0 && this.page == 'add' && validPost.FuelIssuanceLubeDetails[validPost.FuelIssuanceLubeDetails.length - 1].POLId != "") {
      let total = 0;
      let hasLubeAvailabilityError = false;
      for(let lube of validPost.FuelIssuanceLubeDetails) {
        for(let lubeAvailability of this.lubeAvailabilityList) {
          if(lube.POLId == lubeAvailability.polId) {
            if(lube.NumberOfPackets > lubeAvailability.availableNumberOfPackets) {
              hasLubeAvailabilityError = true;
              break;
            }
          }
        }
      }
      if(this.lubeAvailabilityList.length == 0 || this.lubeAvailabilityList.length != validPost.FuelIssuanceLubeDetails.length) {
        hasLubeAvailabilityError = true;
      }
      if(hasLubeAvailabilityError) {
        this.lubricantError = 'Available Number of Packets and number of packets issuing are not same!'
      }
    } else {
      this.lubricantError = '';
    }
    //this.lubeAvailabilityNotificationMessage = "";
    if(validPost.GeneratorIssueTypeform.length == 0 && this.IsIssuetype == 'GENERATOR') {
      this.generatorArrayError = "Generator Details are Required";
    } else if(validPost.GeneratorIssueTypeform.length > 0 && this.IsIssuetype == 'GENERATOR') {
      validPost.GeneratorIssueTypeform.forEach(generator => {
        if(generator.GeneratorId == "" || generator.Make == "" || generator.HoursPerRun == "" ) {
          this.generatorArrayError = "Generator Details are Required";
        } else {
          this.generatorArrayError = "";
        }
      });
    }

    if(validPost.FuelIssuanceLubeDetails.length == 0 && this.IsLubricantIssue) {
      this.lubricantArrayError = "Lubricant Details are Required";
    } else if(validPost.FuelIssuanceLubeDetails.length > 0 && this.IsLubricantIssue) {
      validPost.FuelIssuanceLubeDetails.forEach(lube => {
        if(lube.POLId == "" || lube.NumberOfPackets == "" ) {
          this.lubricantArrayError = "Lubricant Details are Required";
        } else {
          this.lubricantArrayError = "";
        }
      });
    }

    if(this.quantityissued > this.availableQuantity) {
      this.quantityalert = "Insufficient POL in tank";
    }
    if( validPost.UomId == null || validPost.UomId == '' || validPost.UomId == undefined){
     // validPost.FuelingEndingTime = "00:00";
    }
    if(validPost.FuelingEndingTime == null){
      validPost.FuelingEndingTime = "00:00";
    }
    if(validPost.SiteId == null || validPost.SiteId == ""){
      this.siteReq = "Site Name is required";
    } else {
      this.siteReq = "";
    }
    if((validPost.PolId == null || validPost.PolId == "") && !this.IsLubricantIssue) {
      this.polreq="POL is required";
    } else {
      this.polreq="";
    }
    if((validPost.TankId == null || validPost.TankId == "") && !this.IsLubricantIssue) {
      this.tankReq = "Tank Name is required";
    } else {
      this.tankReq = "";
    }
    if((validPost.UomId == null || validPost.UomId == "") && !this.IsLubricantIssue) {
      this.uomReq = "UOM is required";
    } else {
      this.uomReq = "";
    }
    if(validPost.IssueTypeId == null || validPost.IssueTypeId == ""){
      this.issuetypeReq = "Issue Type is required";
    } else {
      this.issuetypeReq = "";
    }
    if(validPost.IssueSlipNumber == null || validPost.IssueSlipNumber == ""){
      this.slipnoReq = "Issue Slip Number is required";
    } else {
      this.slipnoReq = "";
    }
    if(validPost.SectionId == null || validPost.SectionId == ""){
      this.sectionReq = "Section is required";
    } else {
      this.sectionReq = "";
    }
    if(validPost.ReceiverId == null || validPost.ReceiverId == ""){
      this.receiverReq = "Receiver is required";
    } else {
      this.receiverReq = "";
    }
    if(validPost.IssuedBy == null || validPost.IssuedBy == ""){
      this.issuedByReq = "Issued By is required";
    } else {
      this.issuedByReq = "";
    }
    if((validPost.Temperature == null || validPost.Temperature == "") && !this.IsLubricantIssue) {
      this.tempReq = "Temperature is required";
    } else {
      this.tempReq = "";
    }
    if((validPost.Density == null || validPost.Density == "") && !this.IsLubricantIssue) {
      this.densityReq = "Density is required";
    } else {
      this.densityReq = "";
    }
    if((validPost.MeterStarted == null || validPost.MeterStarted == "") && !this.IsLubricantIssue) {
      this.meterStartedReq = "Meter Started is required";
    } else {
      this.meterStartedReq = "";
    }
    if((validPost.MeterEnded == null || validPost.MeterEnded == "") && !this.IsLubricantIssue) {
      this.meterEndedReq = "Meter Ended is required";
    } else {
      this.meterEndedReq = "";
    }
    if((validPost.QuantityIssued == null || validPost.QuantityIssued == "") && !this.IsLubricantIssue) {
      this.quantityIssuedReq = "Quantity Issued is required";
    } else {
      this.quantityIssuedReq = "";
    }
    if((validPost.NoOfDrums == null || validPost.NoOfDrums == "") && this.IsDrums){
      this.drumsReq = "Number of Drums is required";
    } else {
      this.drumsReq = "";
    }
    if((validPost.TailRegistrationId == null || validPost.TailRegistrationId == "") && this.IsIssuetype == 'AIRCRAFT') {
      this.tailRegReq = "Tail Registration is required";
    } else {
      this.tailRegReq = "";
    }
    if((validPost.RegistrationNumberId == null || validPost.RegistrationNumberId == "") && this.IsIssuetype == 'VEHICLE') {
      this.vehicleRegReq = "Registration Number is required";
    } else {
      this.vehicleRegReq = "";
    }
    if((validPost.OdometerReading == null || validPost.OdometerReading == "") && this.IsIssuetype == 'VEHICLE') {
      this.odometerReq = "Odometer Reading is required";
    } else {
      this.odometerReq = "";
    }
    this.quantityIssuedAt15Req = "";
    if(validPost.IssuedToCustomer == null || validPost.IssuedToCustomer == ""){
      this.customerReq = "Customer is required";
    } else {
      this.customerReq = "";
    }
    this.formSubmited = true;
    if(this.startedmessage === '' && this.stockmessage == '' && this.tastm == '' && this.dastm == '' &&
      this.quantityalert =='' && this.siteReq =='' && this.polreq ==''&&
      this.tankReq =='' && this.uomReq =='' && this.issuetypeReq =='' && this.slipnoReq =='' && this.sectionReq =='' &&
      this.receiverReq =='' && this.tempReq ==''  && this.densityReq ==''  && this.meterStartedReq ==''  && this.meterEndedReq =='' &&
      this.quantityIssuedReq =='' && this.quantityIssuedAt15Req =='' && this.customerReq =='' && this.issuedByReq =='' &&
      this.generatorArrayError =='' && this.lubricantArrayError == '' && this.drumsReq == '' && this.tailRegReq == '' &&
      this.fuelTimeError == '' && this.vehicleRegReq == '' && this.lubricantError == '' && this.issueSlipErrorMsg == '' && this.odometerReq == '') {
          //validPost.IsDrumIssue = this.IsDrums;
          //validPost.IsLubricantIssue = this.IsLubricantIssue;
          //validPost.NonSiteVehicle = this.NonSiteVehicle;
          validPost.uploadedFile = this.filetype;
          validPost.IssueTypeName = this.IsIssuetype;
          validPost['BatchDetails'] = this.batchDetailsList;
          this.saved.emit(validPost);
    } else {
          jQuery('form').find(':input.ng-invalid:first').focus();
    }
  }

  getPolListFromSite(id) {
    this.SiteId = id;
    this.storagetanklist = [];
    this.compBatchList = [];
    this.batchDetailsList = [];
    this.lubricantError = "";
    this.getAircraftList(this.SiteId);
    this.service.getPolListFromSiteById(id).then(response => {
      this.fuelissuanceForm.controls['PolId'].setValue('');
      this.fuelissuanceForm.controls['TankId'].setValue('');
      this.pollist = response;
      this.batchDetailsList = [];
    });
    this.notificationMessage="";
    this.quantityalert = "";
    if(this.page == 'add') {
     // this.getVehiclelistBySiteId(id, this.typeflag);
     this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
      this.getReceiverList(0, this.SiteId);
      this.getIssuedbyList();
    }
    this.generatorList();
    if(this.IsLubricantIssue) {
      this.getLubcricantAvailabilityFromSite(id);
    }
  }

  siteListByCountry : any = [];
  getAllSiteInCountryFromSiteId(id) {
    this.service.getAllSiteInCountryFromSiteId(id).then(response => {
      this.siteListByCountry = response;
    }).catch(r => {
      this.handleError(r);
    })
  }

  lubeAvailabilityList = [];
  getLubcricantAvailabilityFromSite(id) {
    this.lubeAvailabilityList = [];
    this.service.getLubcricantAvailabilityFromSite(id).then(response => {
      if(response.result.length > 0) {
        this.lubeAvailabilityList = response.result;
      }
    })
  }

  getPolListFromSiteId(id) {
    this.SiteId = id;
    this.service.getPolListFromSiteById(id).then(response => {
      this.pollist = response;
    })
    if(this.page == 'add') {
     // this.getVehiclelistBySiteId(id, this.typeflag);
     this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
    }
  }

  getStorageTankListFromPol(id) {
    this.notificationMessage="";
    this.POLId = id;
    this.storagetanklist = [];
    this.compBatchList = [];
    this.batchDetailsList = [];
    this.fuelissuanceForm.controls['TankId'].setValue('');
    this.service.getStorageTankListFromSiteById(this.SiteId, this.POLId).then(response => {
      this.storagetanklist = response;
      this.batchDetailsList = [];
    });
    this.quantityalert = "";
  }

  //For getting Department List
  getDepartmentList() {
    this.service.getDepartmentList().then(r => {
      this.departmentList = r['result'];
    });
  }

  //For getting OwnershipInfo List
  getOwnershipInfoList() {
    this.service.getOwnershipInfoList().then(r => {
      this.ownershipinfoList = r['result'];

    });
  }

  getVehicleTypeList() {
    this.service.getVehicleTypeList().then(r => {
      this.vehicletypeList = r['result'];
    });
  }
  getMakeModelList() {
    this.service.getMakeModelList().then(r => {
      this.makemodelList = r['result'];

    });
  }

 // Save Vehicle
  onSaveVehicle(data) {
    let formData = new FormData();
    this.vehicleFormSubmited = true;
    if (this.vehicleForm.valid) {
      let startingOdometer = this.vehicleForm.value.startingOdometer ? this.vehicleForm.value.startingOdometer : 0;
      let maxfuelfillcapacity = this.vehicleForm.value.maxfuelfillcapacity ? this.vehicleForm.value.maxfuelfillcapacity : 0;
      formData.append('Id', '0');
      formData.append('VehicleId', this.vehicleForm.value.vehicleId);
      formData.append('StartingOdoMeter', startingOdometer);
      formData.append('MakeAndModelId', this.vehicleForm.value.makemodelId);
      formData.append('vehicleTypeId', this.vehicleForm.value.vehicleTypeId);
      formData.append('MaxFuelCapacity', maxfuelfillcapacity);
      formData.append('CustomerId', this.vehicleForm.value.customer);
      formData.append('DepartmentId', this.vehicleForm.value.department);
      formData.append('SiteId', this.vehicleForm.value.basestation);
      formData.append('SectionId', this.vehicleForm.value.section);
      formData.append('OwnershipId', this.vehicleForm.value.ownershipinfo);
      formData.append('BarCode', this.vehicleForm.value.BarCode);
      formData.append('isDeleted', '');
      formData.append('fileURL', '');
      this.service.SaveVehicle(formData).then(r => {
        jQuery('.modal-backdrop').remove();
       // this.registrationnumberlist();
       this.getVehiclelistBySiteIdAndUser(this.SiteId,  this.typeflag, this.userId);
        this.vehicleForm.reset();
        this.modalclose('vehicle');
        setTimeout(function () {
          jQuery('#AddVehicle').modal('toggle');
        }.bind(this), 500);

      }).catch(r => {
        this.handleError(r);
      })
    }
    // jQuery('AddVehicle').modal('hide');
    // jQuery('body').removeClass('modal-open');

  }

  // Save Aircraft
  onSaveAircraft(data) {
    let formData = new FormData();
    this.tailRegistrationFormSubmited = true;
    if (this.aircraftForm.valid) {
        formData.append('Id', '0');
        formData.append('FlightNo', this.aircraftForm.value.flightno);
        formData.append('AircraftTypeId', this.aircraftForm.value.aircraftTypeId);
        formData.append('Callsign', this.aircraftForm.value.callsign);
        formData.append('MaxFuelCapacity',this.aircraftForm.value.maxfuelfillcapacity);
        formData.append('CustomerId',this.aircraftForm.value.customer);
        formData.append('DepartmentId',this.aircraftForm.value.department);
        formData.append('BaseStationId', this.aircraftForm.value.basestation);
        formData.append('SectionId',this.aircraftForm.value.section);
        formData.append('OwnerInfo',this.aircraftForm.value.ownershipinfo);
        formData.append('isDeleted', '');
        formData.append('fileURL', '');
        this.service.SaveAircraft(formData).then(r => {
          jQuery('.modal-backdrop').remove();
          this.modalclose('tailregistration');
          // this.registrationnumberlist();
          this.getAircraftList(this.SiteId);
          setTimeout(function () {
            jQuery('#AddTailRegistration').modal('toggle');
          }.bind(this), 500);
        }).catch(r => {
          this.handleError(r);
        })
      }
      // jQuery('AddTailRegistration').modal('hide');
      // jQuery('body').removeClass('modal-open');
  }

  // Save Section
  onSaveSection(data) {
    let formData = new FormData();
    this.sectionFormSubmited = true;
    if( this.sectionForm.valid) {
      formData.append('SectionId', '0');
      formData.append('SectionName', this.sectionForm.value.section);
      formData.append('Description', this.sectionForm.value.description);
      formData.append('isDeleted', '');
      formData.append('fileURL', '');
      this.service.SaveSection(formData).then(r => {
        jQuery('.modal-backdrop').remove();
        this.getSectionList();
        //this.sectionForm.reset();
        this.modalclose('section');
        setTimeout(function () {
          jQuery('#AddSection').modal('toggle');
        }.bind(this), 500);
      }).catch(e => {
        if (e.firstKey) {
          this.Serrorsection = this.error.firstKey;
        } else {
          this.Serrorsection = "";
        }
      });
    }
    jQuery('AddSection').modal('hide');
    jQuery('body').removeClass('modal-open');

  }

  unidError = '';
  // Save Receiver
  onSaveReceiver(data) {
    let formData = new FormData();
    this.receiverFormSubmited = true;
    if (this.receiverForm.valid) {
      formData.append('ReceiverId', '0');
      formData.append('FirstName', this.receiverForm.value.firstName);
      formData.append('LastName', this.receiverForm.value.lastName);
      formData.append('UNIDNo', this.receiverForm.value.unidNo);
      formData.append('SiteId', this.receiverForm.value.siteId);
      formData.append('CustomerId', this.receiverForm.value.customerId);
      formData.append('departmentId', this.receiverForm.value.departmentId);
      formData.append('SectionId', this.receiverForm.value.sectionId);
      formData.append('isDeleted', '');
      formData.append('fileURL', '');
      this.service.SaveReceiver(formData).then(r => {
        //this.receiverForm.reset();
        this.modalclose('receiver');
        jQuery('.modal-backdrop').remove();
        this.getReceiverList(0, this.SiteId);
        setTimeout(function () {
          jQuery('#AddReceiver').modal('toggle');
        }.bind(this), 500);

      }).catch(r => {
        this.unidError = r.firstKey[0];
      })
    }
    jQuery('AddReceiver').modal('hide');
    jQuery('body').removeClass('modal-open');
  }

  onUnidKeyUp() {
    this.unidError = '';
  }

  unidErrorIssuer = '';
  // Save Issuer
  onSaveIssuer(data) {
    let formData = new FormData();
    this.issuerFormSubmited = true;
    if (this.issuerForm.valid) {
      formData.append('IssuerId', '0');
      formData.append('FirstName', this.issuerForm.value.firstName);
      formData.append('LastName', this.issuerForm.value.lastName);
      formData.append('UNIDNo', this.issuerForm.value.unidNo);
      formData.append('SiteId', this.issuerForm.value.siteId);
      formData.append('isDeleted', '');
      formData.append('fileURL', '');
      this.service.SaveIssuer(formData).then(r => {
        jQuery('.modal-backdrop').remove();
        this.modalclose('issuer');
        this.getIssuedbyList();
        setTimeout(function () {
          jQuery('#AddIssuer').modal('toggle');
        }.bind(this), 0);

      }).catch(r => {
        this.unidErrorIssuer = r.firstKey[0];
      })
    }
    jQuery('AddIssuer').modal('hide');
    jQuery('body').removeClass('modal-open');
  }

  onUnidKeyUpIssuer() {
    this.unidErrorIssuer = '';
  }

  modalclose(item) {
    if (item == 'vehicle') {
      this.vehicleFormSubmited = false;
      this.vehicleForm = this.fb.group({
        'Id': [''],
        'vehicleId': ['', Validators.compose([Validators.required])],
        'vehicleTypeId': ['', Validators.compose([Validators.required])],
        'startingOdometer': [''],
        'makemodelId': ['', Validators.compose([Validators.required])],
        'maxfuelfillcapacity': [''],
        'customer': ['', Validators.compose([Validators.required])],
        'department': ['', Validators.compose([Validators.required])],
        'basestation': ['', Validators.compose([Validators.required])],
        'section': ['', Validators.compose([Validators.required])],
        'ownershipinfo': ['', Validators.compose([Validators.required])],
        'BarCode': [''],
        'FileType': [],
      });
    }
    if (item == 'receiver') {
      this.receiverFormSubmited = false;
      this.receiverForm = this.fb.group({
        'firstName': ['', Validators.compose([Validators.required])],
        'receiverId': [''],
        'lastName': ['', Validators.compose([Validators.required])],
        'siteId': ['', Validators.compose([Validators.required])],
        'customerId': ['', Validators.compose([Validators.required])],
        'sectionId': ['', Validators.compose([Validators.required])],
        'departmentId': ['', Validators.compose([Validators.required])],
        'unidNo': ['', Validators.compose([Validators.required])],
        'siteName': [''],
        'customerName': [''],
        'sectionName': [''],
        'FileType':[],
      });
    }
    if (item == 'section') {
      this.sectionFormSubmited = false;
      this.sectionForm = this.fb.group({
        'Id': [''],
        'section': ['', Validators.compose([Validators.required])],
        'description': [''],
        'FileType': [],
      });
    }
    if (item == 'issuer') {
      this.issuerFormSubmited = false;
      this.issuerForm = this.fb.group({
        'firstName': ['', Validators.compose([Validators.required])],
        'issuerId': [''],
        'lastName': ['', ],
        'siteId': ['', Validators.compose([Validators.required])],
        'unidNo': ['',Validators.compose([Validators.required])],
        'FileType':[''],
      });
    }
    if (item == 'tailregistration') {
      this.tailRegistrationFormSubmited = false;
      this.aircraftForm = this.fb.group({
        'flightno': ['',Validators.compose([Validators.required])],
        'aircraftTypeId': ['',Validators.compose([Validators.required])],
        'callsign': ['',Validators.compose([Validators.required])],
        'maxfuelfillcapacity': ['',Validators.compose([Validators.required])],
        'customer': ['',Validators.compose([Validators.required])],
        'department': ['',Validators.compose([Validators.required])],
        'basestation': ['',Validators.compose([Validators.required])],
        'section': ['',Validators.compose([Validators.required])],
        'ownershipinfo': ['',Validators.compose([Validators.required])],
        'FileType':[],
      });
    }
  }

  editLubeSection(i, section) {
    section.controls['NumberOfPackets'].enable();
    this.gId=section.value.GeneratorId;
  }

  editGenSection(i, section) {
    section.controls['HoursPerRun'].enable();
  }

  clearerror() {
    this.error = "";
  }

  clearmsgs() {
    if (this.fuelissuanceForm.controls['SiteId'].hasError('required') || (this.fuelissuanceForm.controls['SiteId'].touched)) {
      this.siteReq = "";
    }
    if (this.fuelissuanceForm.controls['PolId'].hasError('required') || (this.fuelissuanceForm.controls['PolId'].touched)) {
      this.polreq = "";
    }
    if (this.fuelissuanceForm.controls['TankId'].hasError('required') || (this.fuelissuanceForm.controls['TankId'].touched)) {
      this.tankReq = "";
    }
    if (this.fuelissuanceForm.controls['UomId'].hasError('required') || (this.fuelissuanceForm.controls['UomId'].touched)) {
      this.uomReq = "";
    }
    if (this.fuelissuanceForm.controls['IssueTypeId'].hasError('required') || (this.fuelissuanceForm.controls['IssueTypeId'].touched)) {
      this.issuetypeReq = "";
    }
    if (this.fuelissuanceForm.controls['IssueSlipNumber'].hasError('required') || (this.fuelissuanceForm.controls['IssueSlipNumber'].touched)) {
      this.slipnoReq = "";
    }
    if (this.fuelissuanceForm.controls['SectionId'].hasError('required') || (this.fuelissuanceForm.controls['SectionId'].touched)) {
      this.sectionReq = "";
    }
    if (this.fuelissuanceForm.controls['ReceiverId'].hasError('required') || (this.fuelissuanceForm.controls['ReceiverId'].touched)) {
      this.receiverReq = '';
    }
    if (this.fuelissuanceForm.controls['IssuedBy'].hasError('required') || (this.fuelissuanceForm.controls['IssuedBy'].touched)) {
      this.issuedByReq = '';
    }
    if (this.fuelissuanceForm.controls['NoOfDrums'].hasError('required') || (this.fuelissuanceForm.controls['NoOfDrums'].touched)) {
      this.drumsReq = '';
    }
    if (this.fuelissuanceForm.controls['TailRegistrationId'].hasError('required') || (this.fuelissuanceForm.controls['TailRegistrationId'].touched)) {
      this.tailRegReq = '';
    }
    if (this.fuelissuanceForm.controls['RegistrationNumberId'].hasError('required') || (this.fuelissuanceForm.controls['RegistrationNumberId'].touched)) {
      this.vehicleRegReq = '';
    }
    if (this.fuelissuanceForm.controls['OdometerReading'].hasError('required') || (this.fuelissuanceForm.controls['OdometerReading'].touched)) {
      this.odometerReq = '';
    }
    //this.IssuingDate = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

    this.notificationMessage = "";
    this.lubeAvailabilityNotificationMessage = [];
    this.error = "";
    this.ErrorList = [];
    this.registration = false;
    this.dateofpreviousissue = '';
    this.volumeofpreviousissue = '';
    this.odometerreading = '';
    this.receivername = '';
    this.IssuerId = '';
    this.ReceiverId = '';
    this.nullalertmessage = ""
    this.lubealertmessage = '';
    this.tempReq = "";
    this.densityReq = "";
    this.meterStartedReq = "";
    this.meterEndedReq = "";
    this.quantityIssuedReq = "";
    this.quantityIssuedAt15Req = "";
    this.customerReq = "";
    this.generatorArrayError="";
    this.lubricantArrayError="";
    this.quantityalert="";
    this.fuelTimeError = '';
    this.NonSiteVehicle = false;
    this.IsWaterTest = true;
    this.IsVisualInspection = true;
    this.IsSedimentFree = true;
    this.IsDrums = false;
    this.idalertmessage='';
    this.tastm = '';
    this.dastm ='';
    this.grList=[];
    this.lubeList=[];
    this.gId = 0;
    this.initialvalue = 0;
    this.lubeId = 0;
    this.numberofpacks = 0;
    this.grList = [];
    this.isAddedAllGeneratores = false;
    this.showAddBtn = [];
    this.lubricantList = [];
    this.isAddedAllLubricants = false;
    this.showAddLubeBtn = [];
    this.lubricantErrorMsg = "";
    this.batchDetailsList = [];
    this.lubricantError = "";
    this.issueSlipErrorMsg = "";
    /*const control = <FormArray>this.fuelissuanceForm.get('GeneratorIssueTypeform');
    for (let i = 1; i <= control.length; i++) {
      control.removeAt(i);
    }
    this.IsIssuetype='';
    const lubcontrol = <FormArray>this.fuelissuanceForm.get('FuelIssuanceLubeDetails');
    for (let i = 1; i <= lubcontrol.length; i++) {
      lubcontrol.removeAt(i);
    }*/
    this.IsLubricantIssue=false;
    this.pollist = [];
    this.storagetanklist = [];
    this.quantityInLitres = 0;
    this.clearForm(false);
  }

  clearForm(isLubricantIssue) {
    let issueType = this.fuelissuanceForm.get('IssueTypeId').value;
    this.fuelissuanceForm = new FormGroup({
      IsLubricantIssue: new FormControl(isLubricantIssue),
      SiteId: new FormControl('', Validators.required),
      IssuingDate: new FormControl('', Validators.required),
      PolId: new FormControl('', [Validators.required]),
      IsDrumIssue: new FormControl(false),
      NoOfDrums: new FormControl('', Validators.required),
      TankId: new FormControl('', [Validators.required]),
      NonSiteVehicle: new FormControl(false),
      IssueTypeId: new FormControl(issueType, Validators.required),
      IssueTypeName: new FormControl(''),
      RegistrationNumberId: new FormControl('', Validators.required),
      OdometerReading: new FormControl('', Validators.required),
      IssueSlipNumber: new FormControl('', Validators.required),
      TailRegistrationId: new FormControl('', Validators.required),
      ArrivedFrom: new FormControl(''),
      DepartingTo: new FormControl(''),
      FuelingStartingTime: new FormControl(''),
      FuelingEndingTime: new FormControl(''),
      IsWaterTest: new FormControl(true),
      IsVisualInspection: new FormControl(true),
      IsSedimentFree: new FormControl(true),
      GeneratorIssueTypeform: new FormArray([
        this.initSection([]),
      ]),
      FuelIssuanceLubeDetails: new FormArray([
        this.initLubeSection([]),
      ]),
      BatchNumber: new FormControl(''),
      UniqueNumber: new FormControl(''),
      IssuedToCustomer: new FormControl('',[Validators.required]),
      SectionId: new FormControl('', Validators.required),
      ReceiverId: new FormControl('', Validators.required),
      UomId: new FormControl('',[Validators.required]),
      IssuedBy: new FormControl('', [Validators.required]),
      Temperature: new FormControl('',[Validators.required]),
      Density: new FormControl('',[Validators.required]),
      MeterStarted: new FormControl('' ,[Validators.required]),
      QuantityIssued: new FormControl(''),
      MeterEnded: new FormControl('',[Validators.required]),
      QuantityIssued15DegreeC: new FormControl(''),
      Remarks: new FormControl(''),
      FileType: new FormControl(''),
      FuelIssuancePurchaseReceiptDetails: new FormControl([]),
    });

    let IssuingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
    this.fuelissuanceForm.controls['IssuingDate'].setValue(IssuingDate);
    jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
  }

  onStartTimeChange(event){
    this.startTime = event.target.valueAsNumber;
    if(this.endTime != '' && this.startTime >= this.endTime) {
      this.fuelTimeError = 'Fuelling End Time must be greater than Fuelling Start Time';
    } else {
      this.fuelTimeError = '';
    }
  }

  onEndTimeChange(event){
    this.endTime = event.target.valueAsNumber;
    if(this.startTime != '' && this.startTime >= this.endTime) {
      this.fuelTimeError = 'Fuelling End Time must be greater than Fuelling Start Time';
    } else {
      this.fuelTimeError = '';
    }
  }

  /*****************************************************
   *  set corresponding batch from the batch number list
   *  based on the quantity of fuel issued
   * ***************************************************/
  setBatchDetails() {
    let control = (<FormArray>this.fuelissuanceForm.get('CompartmentDetails'));
    //this.startedmessage = "";
    this.batchDetailsList = [];
    if(typeof(this.prList) != 'undefined' && this.startedmessage == "") {
      let volume;
      if(this.meterEnded != "" && this.meterStarted != "") {
        volume = this.meterEnded - this.meterStarted;
        this.compBatchList = [];
      } else {
        volume = 0;
      }
      this.batchList = JSON.parse(JSON.stringify(this.prList));
      for(let batch of this.batchList) {
        if (volume == batch.quantity && volume != 0) {
          this.compBatchList.push(batch.batchNumber);
          let batchDetails = {
            'batch' : batch.purchaseReceiptId,
            'volume' : Number(volume)
          }
          this.batchDetailsList.push(batchDetails);
          batch.quantity = 0;
          break;
        } else if (volume > batch.quantity && batch.quantity != 0) {
            let requiredVolOnNextBatch = 0;
            this.compBatchList.push(batch.batchNumber);
              requiredVolOnNextBatch = volume - batch.quantity;
              volume = requiredVolOnNextBatch;
              let batchDetails = {
                'batch' : batch.purchaseReceiptId,
                'volume' : Number(batch.quantity)
              }
              this.batchDetailsList.push(batchDetails);
              batch.quantity = 0;
        } else if (volume < batch.quantity) {
            let quantityLeftOnBatch = 0;
            this.compBatchList.push(batch.batchNumber);
            quantityLeftOnBatch = batch.quantity - volume;
            batch.quantity = quantityLeftOnBatch;
            let batchDetails = {
              'batch' : batch.purchaseReceiptId,
              'volume' : Number(volume)
            }
            this.batchDetailsList.push(batchDetails);
            break;
        }
      }
    }
  }

  getBatchFromTank(tankId) {
    this.service.getBatchNo(tankId).then(response=>{
      this.prList = response['purchaseReceipt'];
      if(this.meterEnded != '' && this.meterStarted != '') {
        this.setBatchDetails();
      }
    }).catch(r => {
      this.handleError(r);
    })
  }

  handleError(e: any) {
    this.error = e;
    let detail = e.detail;
    console.log('error', this.error);
    if (detail && detail == 'Signature has expired.') {
      this.router.navigate(['./']);
    }
  }

  getVehiclelistBySiteId(siteId, typeflag) {
    this.service.getVehicleBySiteId(siteId, !typeflag).then(r => {
      this.registrationnumberdropdownlist = r;
    }).catch(r => {
      this.handleError(r);
    })
  }


  getVehiclelistBySiteIdAndUser(siteId, typeflag, userId ) {

    this.service.getVehicleBySiteAndUser(siteId, !typeflag, userId).then(r => {
      this.registrationnumberdropdownlist = r;
 
    }).catch(r => {
      this.handleError(r);
    })
  }
  //

  fuelIssuancePrint : any = {

  };

  isIssueSlipDisabled : boolean = true;
  getEquipmentList(siteId) {
    this.isIssueSlipDisabled = false;
    this.siteId = siteId;
    if(this.issueTypeId) {
      this.service.getEquipmentsFromSiteId(this.issueTypeId, siteId).then(response => {
        this.equipmentList = response;
      })
    }
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html lang="en">
      <head>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
      <script src='https://printjs-4de6.kxcdn.com/print.min.js'></script>
      <style></style>
      </head>
      <body onload="window.print();window.close()">${printContents}</body>
    </html>`);
    popupWin.document.close();
  }
}
