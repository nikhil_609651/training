import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from "../services/customer.service";
import { OrdersService } from "../../orders/services/orders.service";
import { URLSearchParams } from '@angular/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Configuration } from 'app/app.constants';
import { ListViewFormService } from 'app/admin/fuelissuance/ListView/services/listview.service';
import { IMyOptions } from 'mydatepicker';
import { Subscription } from 'rxjs';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'listview-list',
  encapsulation: ViewEncapsulation.None,
  template: require('./listview-list.html')
})
export class ListViewListComponent implements OnInit{
  selectedapproverAll: any;
  selectedsectionAll: any;
  selectedstatusAll: any;
  selectedreceivernameAll: any;
  selectedreceiveridAll: any;
  selectedsiteAll: any;
  deletemessage: string;
  fuelissuance_deleted_id: any;
  sId: any;
  approverlist: any;
  selectedAll: any;
  statuslist: any;
  sectionlist: any;
  receiverlist: any;
  //checked: string[] = [];
  checkedapprover: string[] = [];
  checkedstatus: string[] = [];
  checkedsection: string[] = [];
  checkedreceiverid: string[] = [];
  checkedreceivername: string[] = [];
  checkedsite: string[] = [];
  sitedropdownlist: any;
  viewForm: FormGroup;
  viewFuelIssuanceForm: FormGroup;
  public totalfeilds = 0;
  public page:number = 1;
  public itemsPerPage:number = 3;
  public maxSize:number = 5;
  public numPages:number = 2;
  public length:number = 5;
  public next = '';
  attachment_deleted_id: any;
  attachmentmessage: string;
  attachmentGroup: any[];
  attachments: any;
  API_URL: string;
  customerId: any;
  public deleteattachmentmessage='';
  public fuelissuancelist: any=[];
  error: any;
  public start:number = 1;
  public loading: boolean;
  public rows:Array<any> = [];
  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true },
    { title: 'Issue Slip Number', name: 'IssueSlipNumber', sort: true },
    // { title: 'Unique Number', name: 'UniqueNumber', sort: false },
    { title: 'Date', name: 'Date', sort: true },
    { title: 'Site', name: 'Site', sort: true,filter: true },
    { title: 'Receiver ID', name: 'ReceiverId', sort: true,filter: true },
    { title: 'Receiver', name: 'ReceiverName', sort: true,filter: true },
    { title: 'Section', name: 'Section', sort: true,filter: true },
    { title: 'Status', name: 'Status', sort: false,filter: true },
    { title: 'Approver', name: 'ApprovedBy', sort: true ,filter: true},
    { title: 'Customer', name: 'Customer', sort: false },
    { title: 'Issuer', name: 'Issuer', sort: false },
    { title: 'Tank', name: 'tank', sort: false },
    { title: 'Issue Type', name: 'issueType', sort: false },
    { title: 'Aircraft Call Sign', name: 'aircraft', sort: false },
    { title: 'Vehicle', name: 'vehicle', sort: false },
    { title: 'Batch No.', name: 'batch', sort: false },
    { title: 'Temperature', name: 'temperature', sort: false },
    { title: 'Density', name: 'density', sort: false },
    { title: 'Meter Started', name: 'meterStarted', sort: false },
    { title: 'Meter Ended', name: 'meterEnded', sort: false },
    { title: 'Quantity', name: 'quantity', sort: false },
    { title: 'Quantity Issued(at 15°C)', name: 'quantity@15', sort: false },
    { title: 'Actions', name: 'actions', sort: false },
  ];
  API_URL_Export: string;
  private currentdate= new Date();
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    //disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() +1, day: this.currentdate.getDate()+1}
  };
  //public IssuingDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialFromDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  fromDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
  toDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();

  private initialSearchFromDate: Object = { date: { day: 1, month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };
  private initialSearchToDate: Object = { date: { day: this.currentdate.getDate(), month: this.currentdate.getMonth() + 1, year: this.currentdate.getFullYear() } };

  private searchFromDatePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };

  private searchToDatePickerOptions: IMyOptions = {
      dateFormat: 'dd/mm/yyyy',
      disableSince: { year: this.currentdate.getFullYear(), month: this.currentdate.getMonth() + 1, day: this.currentdate.getDate() + 1 }
  };

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };
  params: URLSearchParams = new URLSearchParams();
  
  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;

  constructor(private router: Router,
    private Service: ListViewFormService,
    private fb: FormBuilder, private _sharedService : SharedService,
      private configuration: Configuration) {
        /**user privileges**/	
        this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
	          this.privileges = privileges;
        });

        if ((new Date().getDate() < 10) && (new Date().getMonth() > 10) ) {
          this.toDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }

        if ((new Date().getDate() < 10) && (new Date().getMonth() < 10) ) {
          this.toDate = '0' + new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }

        if ((new Date().getDate() > 10) && (new Date().getMonth() < 10) ) {
          this.toDate =  new Date().getDate() + '/'+'0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
          this.fromDate =  new Date().getDate() + '/' + '0' + Number(new Date().getMonth() + 1) + '/' + new Date().getFullYear();
        }

        let searchToDate = '';
      if(Number(new Date().getMonth()+1) < 10 && new Date().getDate() < 10) {
        searchToDate = '0' + new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      } else if(new Date().getDate() < 10 && Number(new Date().getMonth()+1) >= 10) {
        searchToDate = '0' + new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      } else if(new Date().getDate() >= 10 && Number(new Date().getMonth()+1) < 10) {
        searchToDate = new Date().getDate() + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      } else {
        searchToDate = new Date().getDate() + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      }
      let searchFromDate = '';
      if(Number(new Date().getMonth()+1) < 10) {
        searchFromDate = '01' + '/' + '0' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      } else if( Number(new Date().getMonth()+1) >= 10) {
        searchFromDate = '01' + '/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      } else {
        searchFromDate = '01/' + Number(new Date().getMonth()+1) + '/' + new Date().getFullYear();
      }
      this.params.set('FromDate', searchFromDate);
      this.params.set('ToDate', searchToDate);

      this.itemsPerPage = configuration.itemsPerPage;
      this.params.set('limit', configuration.itemsPerPage.toString());
      this.params.set('SiteFilter', '');
      this.params.set('ReceiverIdFilter', '');
      this.params.set('ReceiverNameFilter', '');
      this.params.set('SectionFilter', '');
      this.params.set('StatusFilter', '');
      this.params.set('ApproverFilter', '');
      this.params.set('UserId', localStorage.getItem('user_nameId'));
      this.rows = configuration.rows;
      this.viewForm = fb.group({
        'fname': [''],
        'customerId': [''],
        'lname': [''],
        'unidNo': [''],
    });
    var vForm = this.viewForm;

    this.viewFuelIssuanceForm = fb.group({
                  'vId': [''],
                  'vIssueSlipNumber': [''],
                  'vUniqueNumber': [''],
                  'vDate': [''],
                  'vSite': [''],
                  'vReceiverId': [''],
                  'vReceiver': [''],
                  'vSection': [''],
                  'vStatus': [''],
                  'vApprover': [''],
    });
    var vForm = this.viewFuelIssuanceForm;
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  ngOnInit(){
      let userId = localStorage.getItem('user_nameId');
      this.API_URL_Export = this.configuration.ServerWithApiUrl + 'FuelIssuance/ExportFuelIssuanceFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
      this.getfuelissuancelist(this.params);
      this.sitelist(userId);
      this.getReceiverList();
      this.getSectionList();
      this.getStatusList();
      this.getApproverList();
  }

  // For getting Site List
  sitelist(userId){
    this.Service.getsitefilterlist(userId).then(response => {
        this.sitedropdownlist = response;
    })
  }

  // For getting Receiver List
  getReceiverList(){
    this.Service.getreceiverfilterlist().then(response => {
      this.receiverlist = response;
    })
  }

  // For getting Section List
  getSectionList(){
    this.Service.getsectionfilterlist().then(response => {
      this.sectionlist = response;
    })
  }

  // For getting Status List
  getStatusList(){
    this.Service.getstatuslist().then(response => {
      this.statuslist = response;
    })
  }

  // For getting Approver List
  getApproverList(){
    this.Service.getapproverlist().then(response => {
      this.approverlist = response;
    })
  }

  getfuelissuancelist(params: any){
    this.loading = true;
    this.Service.getfuelissuancelist(params).then(response => {
      this.fuelissuancelist = response['result'];
      if(this.fuelissuancelist.length>0) {
        this.length = response['count'];
        this.next = response['next'];
        this.loading = false; 
      } else{
        this.page = 1;
        this.getAllLists(this.params);
      }
    }).catch(r =>  {
      this.handleError(r);
    });
  }

  getAllLists(params: any) {
    this.loading = true;
    this.Service.getfuelissuancelist(params).then(response => {
      this.fuelissuancelist = response['results']
      this.length = response['count'];
      this.next = response['next'];
      this.loading = false;

    }).catch(r =>  {
      this.handleError(r);
    });
  }

  add() {
    this.router.navigate(['./home/fuel-issuance-summary/list-view/add']);
  }

  edit(id) {
    this.router.navigate(['./home/fuel-issuance-summary/list-view/edit/', id]);
  }

  apply(item:any){
    this.getfuelissuancelist(this.params);
    if(item === 'Site'){
      setTimeout(function () {
        jQuery('#ViewSiteModal').modal('toggle');
      }.bind(this), 0);
    }
    if(item === 'ReceiverId'){
      setTimeout(function () {
        jQuery('#ViewReceiverIdModal').modal('toggle');
      }.bind(this), 0);
    }
    if(item === 'ReceiverName'){
      setTimeout(function () {
        jQuery('#ViewReceiverNameModal').modal('toggle');
      }.bind(this), 0);
    }
    if(item === 'Section'){
      setTimeout(function () {
        jQuery('#ViewSectionModal').modal('toggle');
      }.bind(this), 0);
    }
    if(item === 'Status'){
      setTimeout(function () {
        jQuery('#ViewStatusModal').modal('toggle');
      }.bind(this), 0);

    }

    if(item === 'Approver'){
      setTimeout(function () {
        jQuery('#ViewApprovedByModal').modal('toggle');
      }.bind(this), 0);

    }
    jQuery('ViewSiteModal').modal('hide');
    jQuery('ViewReceiverIdModal').modal('hide');
    jQuery('ViewSectionModal').modal('hide');
    jQuery('ViewReceiverNameModal').modal('hide');
    jQuery('ViewStatusModal').modal('hide');
    jQuery('ViewApprovedByModal').modal('hide');
    jQuery('body').removeClass('modal-open');
    jQuery('.modal-backdrop').remove();
  }
  // Filtering
  selectAll(item) {
    if(item === 'Site'){


    for (var i = 0; i < this.sitedropdownlist.length; i++) {

      this.sitedropdownlist[i].selected = this.selectedsiteAll;
    }
    this.params.set('SiteFilter', null);
    }

    if(item === 'ReceiverId'){


        for (var i = 0; i < this.receiverlist.length; i++) {

          this.receiverlist[i].selected = this.selectedreceiveridAll;
        }
        this.params.set('ReceiverIdFilter', null);
        }

        if(item === 'ReceiverName'){


            for (var i = 0; i < this.receiverlist.length; i++) {

              this.receiverlist[i].selected = this.selectedreceivernameAll;
            }
            this.params.set('ReceiverNameFilter', null);
            }

            if(item === 'Section'){


                        for (var i = 0; i < this.sectionlist.length; i++) {

                          this.sectionlist[i].selected = this.selectedsectionAll;
                        }
                        this.params.set('SectionFilter', null);
                        }


                        if(item === 'Status'){


                                                for (var i = 0; i < this.statuslist.length; i++) {

                                                  this.statuslist[i].selected = this.selectedstatusAll;
                                                }
                                                this.params.set('StatusFilter', null);
                                                }

                                                if(item === 'Approver'){


                                                                                                for (var i = 0; i < this.approverlist.length; i++) {

                                                                                                  this.approverlist[i].selected = this.selectedapproverAll;
                                                                                                }
                                                                                                this.params.set('ApproverFilter', null);
                                                                                                }
  }

  checkIfAllSelected(option, event,filteritem) {

    if(filteritem === 'Site'){
    this.selectedsiteAll = this.sitedropdownlist.every(function(item:any) {
        return item.selected == true;
      })

      var index = this.checkedsite.indexOf(option.siteId);
    if(event.target.checked) {
      if(index === -1) {
        this.checkedsite.push(option.siteId);

      }
    }
    else {
      if(index !== -1) {
        this.checkedsite.splice(index, 1);
      }
    }
  this.params.set('SiteFilter', this.checkedsite.toString());
    }

    if(filteritem === 'ReceiverId'){
      this.selectedreceiveridAll = this.receiverlist.every(function(item:any) {
          return item.selected == true;
        })

        var index = this.checkedreceiverid.indexOf(option.receiverId);
      if(event.target.checked) {
        if(index === -1) {
          this.checkedreceiverid.push(option.receiverId);

        }
      }
      else {
        if(index !== -1) {
          this.checkedreceiverid.splice(index, 1);
        }
      }
    this.params.set('ReceiverIdFilter', this.checkedreceiverid.toString());
      }

      if(filteritem === 'ReceiverName'){
        this.selectedreceivernameAll = this.receiverlist.every(function(item:any) {
            return item.selected == true;
          })

          var index = this.checkedreceivername.indexOf(option.receiverId);
        if(event.target.checked) {
          if(index === -1) {
            this.checkedreceivername.push(option.receiverId);

          }
        }
        else {
          if(index !== -1) {
            this.checkedreceivername.splice(index, 1);
          }
        }
      this.params.set('ReceiverNameFilter', this.checkedreceivername.toString());
        }

        if(filteritem === 'Section'){
          this.selectedsectionAll = this.sectionlist.every(function(item:any) {
              return item.selected == true;
            })

            var index = this.checkedsection.indexOf(option.sectionId);
          if(event.target.checked) {
            if(index === -1) {
              this.checkedsection.push(option.sectionId);

            }
          }
          else {
            if(index !== -1) {
              this.checkedsection.splice(index, 1);
            }
          }
        this.params.set('SectionFilter', this.checkedsection.toString());
          }


          if(filteritem === 'Status'){
            this.selectedstatusAll = this.statuslist.every(function(item:any) {
                return item.selected == true;
              })

              var index = this.checkedstatus.indexOf(option.id);
            if(event.target.checked) {
              if(index === -1) {
                this.checkedstatus.push(option.id);

              }
            }
            else {
              if(index !== -1) {
                this.checkedstatus.splice(index, 1);
              }
            }
          this.params.set('StatusFilter', this.checkedstatus.toString());
            }

            if(filteritem === 'Approver'){
              this.selectedapproverAll = this.approverlist.every(function(item:any) {
                  return item.selected == true;
                })

                var index = this.checkedapprover.indexOf(option.id);
              if(event.target.checked) {
                if(index === -1) {
                  this.checkedapprover.push(option.id);

                }
              }
              else {
                if(index !== -1) {
                  this.checkedapprover.splice(index, 1);
                }
              }
            this.params.set('ApproverFilter', this.checkedapprover.toString());
              }
  }

  // Export to Excel List

  onDateChanged(event:any, type: any) {
    if(type == 'FromDate'){
      this.fromDate = event.formatted;
    }
    if(type == 'ToDate'){
      this.toDate = event.formatted;
    }
  }

    export(){
      let userId = localStorage.getItem('user_nameId');
      this.API_URL_Export = this.configuration.ServerWithApiUrl + 'FuelIssuance/ExportFuelIssuanceFiles?FromDate=' + this.fromDate + '&ToDate='+this.toDate+ '&UserId=' + userId;
    }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {

      let params = this.params;
      let start = (page.page - 1) * page.itemsPerPage;
      this.start = start + 1;
      params.set('limit', page.itemsPerPage);
      params.set('offset',  start.toString());

      var sortParam = '';

      // if (config.sorting) {
      //   Object.assign(this.config.sorting, config.sorting);
      // }

      if (config.filtering) {
        Object.assign(this.config.filtering, config.filtering);
        params.set(this.config.filtering.name, this.config.filtering.filterString);
      }


      this.getfuelissuancelist(this.params);
    }
  // For File Attachments
  attachment(id){
    this.sId=id;
    this.attachmentmessage='';
    this.API_URL = this.configuration.ServerWithApiUrl +'FuelIssuance/DownloadFuelIssuanceFiles?FileId=';
  this.Service.getallfuelissuanceattachments(id).then(response => {
  this.attachments=response;
  if(this.attachments.length > 0){
    var LongArray = [];
    for(var i = 0; i < this.attachments.length; i++) {
      let ext=this.attachments[i].location;
      var Obj = {
      id :this.attachments[i].id,
      referenceId:this.attachments[i].referenceId,
      shortFileName : this.attachments[i].shortFileName,
      fileName:this.attachments[i].fileName,
      createdDate: this.attachments[i].createdDate,
        ext :  ext.substr(ext.lastIndexOf('.') + 1),

      };
      LongArray.push(Obj);
      this.attachmentGroup=LongArray;
    }
  }
  else{
  this.getfuelissuancelist(this.params);
  this.attachmentmessage='No Attachments Found'
  //  setTimeout(function () {
  //    jQuery('#ViewAttachmentModal').modal('toggle');
  //  }.bind(this), 1000);
  }
  return this.attachmentGroup;
  })

  }

  // For Deleting Attachments

  deleteattachment(id){
    this.attachment_deleted_id = id;
  }
  deleteattachmentmessageclear(){
    this.deleteattachmentmessage = '';
  }

  deleteattachmentconfirm(id){
  //alert(deleted);
    // if (confirm) {

      this.Service.deleteattachment(this.attachment_deleted_id).then(r =>  {
        this.attachment(this.sId);
        this.deleteattachmentmessage = "Attachment Deleted Successfully";

      }).catch(r =>  {
        this.handleError(r);
        this.deleteattachmentmessage = r.name[0];
      })
  }

  viewcloseattachment(){
    setTimeout(function () {
        jQuery('#ViewAttachmentModal').modal('toggle');
      }.bind(this), 0);
  }



  view(id){
    this.router.navigate(['./home/fuel-issuance-summary/list-view/view/', id]);
  }
  viewclose(){
    setTimeout(function () {
        jQuery('#ViewFuelIssuance').modal('toggle');
      }.bind(this), 0);
  }

  delete(data){
    this.fuelissuance_deleted_id = data.id;
  }
  deletemessageclear(){
    this.deletemessage = '';
  }
  // For Deleting Fuelissuance
  deleteitemconfirm(data){
    // this.fuelissuance_deleted_id = data;
      this.Service.delete(  this.fuelissuance_deleted_id).then(r =>  {
        this.getfuelissuancelist(this.params);
        this.deletemessage = 'Fuel Issuance Deleted Successfully';
      }).catch(r =>  {
        this.handleError(r);
        this.deletemessage = r.name[0];
      });
  }

  // For Sorting
  Sort(param,order){
    //this.params.set('ordering', sortParam);
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
        }
        this.getfuelissuancelist(this.params);
  }

  onSearchDateChanged(date, type) {
    let dateFromPicker = { year: date.date.year, month: date.date.month, day: date.date.day };
    if(date.date.month < 10 && date.date.day < 10) {
      date = '0' + date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else if(date.date.day < 10 && date.date.month >= 10) {
      date = '0' + date.date.day + '/' + date.date.month + '/' + date.date.year;
    } else if(date.date.day >= 10 && date.date.month < 10) {
      date = date.date.day + '/' + '0' + date.date.month + '/' + date.date.year;
    } else {
      date = date.date.day + '/' + date.date.month + '/' + date.date.year;
    }
    if(type == 'fromDate') {
      let toDateOptionCopy = this.getCopyOfSearchToDatePickerOptions()
      dateFromPicker.day = dateFromPicker.day-1;
      toDateOptionCopy.disableUntil = dateFromPicker;
      this.searchToDatePickerOptions = toDateOptionCopy;
      this.params.set('FromDate', date);
      this.getfuelissuancelist(this.params);
    } else if(type == 'toDate') {
      let fromDateOptionCopy = this.getCopyOfSearchFromDatePickerOptions();
      dateFromPicker.day = dateFromPicker.day+1;
      fromDateOptionCopy.disableSince = dateFromPicker;
      this.searchFromDatePickerOptions = fromDateOptionCopy;
      this.params.set('ToDate', date);
      this.getfuelissuancelist(this.params);
    }
  }

  getCopyOfSearchToDatePickerOptions(): IMyOptions {
      return JSON.parse(JSON.stringify(this.searchToDatePickerOptions));
  }

  getCopyOfSearchFromDatePickerOptions(): IMyOptions {
      return JSON.parse(JSON.stringify(this.searchFromDatePickerOptions));
  }
  private handleError(e: any) {
    let detail = e.detail;
    if(detail && detail == 'Signature has expired.'){
      this.router.navigate(['./']);
    }
  }
}