import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ListViewFormService } from 'app/admin/fuelissuance/ListView/services/listview.service';
@Component({
  selector: 'add',
  template: require('./listview-add.html')
})
export class ListViewAddComponent implements OnInit{

  public error = {};
  public success = '';
  public customer: any;
  public page = '';
  constructor(private router: Router,
    private service: ListViewFormService) {
  }
    ngOnInit() {
      this.page = 'add';
    }

    onSave(pr: any) {
      let files = pr.uploadedFile;
      let GeneratorIssueDetails=pr.GeneratorIssueTypeform;
      let LubricantDetails =pr.FuelIssuanceLubeDetails;
      let PurchaseDetails=pr.FuelIssuancePurchaseReceiptDetails;
      let prDetails;
      let formData = new FormData();
      prDetails =  JSON.stringify(GeneratorIssueDetails);
      let lubeDetails =  JSON.stringify(LubricantDetails);
      let purDetails=JSON.stringify(PurchaseDetails);
      let issuingdate = pr.IssuingDate;
      if(pr.NoOfDrums == "") {
          pr.NoOfDrums = "0"
      }
      if(typeof issuingdate == 'object') {
        if(issuingdate.date.month < 10 && issuingdate.date.day < 10) {
          issuingdate = '0' + issuingdate.date.day + '/' + '0' + issuingdate.date.month + '/' + issuingdate.date.year;
        }else if(issuingdate.date.day < 10 && issuingdate.date.month >= 10) {
          issuingdate = '0' + issuingdate.date.day + '/' + issuingdate.date.month + '/' + issuingdate.date.year;
        }else if(issuingdate.date.day >= 10 && issuingdate.date.month < 10) {
          issuingdate = issuingdate.date.day + '/' + '0' + issuingdate.date.month + '/' + issuingdate.date.year;
        } else {
          issuingdate = issuingdate.date.day + '/' + issuingdate.date.month + '/' + issuingdate.date.year;
        }
      } else {
          issuingdate= pr.IssuingDate;
      }
      for(let i =0; i < files.length; i++) {
          formData.append("FileType", files[i], files[i]['name']);
      }

      formData.append('Id', '0');
      formData.append('IsLubricant', pr.IsLubricantIssue);
      formData.append('SiteId', pr.SiteId);
      formData.append('IssuingDate', issuingdate);
      formData.append('POLId', pr.PolId);
      formData.append('DrumIssue', pr.IsDrumIssue);
      formData.append('StorageTankId', pr.TankId);
      formData.append('NonSiteVehicle', pr.NonSiteVehicle);
      formData.append('IssueSlipNumber', pr.IssueSlipNumber);
      formData.append('UniqueNumber', pr.UniqueNumber);
      formData.append('BatchNumber', pr.BatchNumber);
      formData.append('EquipmentTypeId',pr.IssueTypeId);
      formData.append('EquipmentTypeName',pr.IssueTypeName)
      formData.append('VehicleId', pr.RegistrationNumberId);
      formData.append('OdometerReading',pr.OdometerReading);
      formData.append('AircraftId', pr.TailRegistrationId);
      formData.append('ArrivedFromSiteId', pr.ArrivedFrom);
      formData.append('DepartingtoSiteId', pr.DepartingTo);
      formData.append('FuelStartTime',pr.FuelingStartingTime);
      formData.append('FuelEndTime', pr.FuelingEndingTime);
      formData.append('CustomerId',pr.IssuedToCustomer);
      formData.append('SectionId', pr.SectionId);
      formData.append('ReceiverId', pr.ReceiverId);
      formData.append('IssuerId', pr.IssuedBy);
      formData.append('IssueTypeId', pr.IssueTypeId);
      formData.append('Temperature', pr.Temperature);
      formData.append('Density', pr.Density);
      formData.append('MeterStarted', pr.MeterStarted);
      formData.append('MeterEnded', pr.MeterEnded);
      formData.append('NumberOfDrums', pr.NoOfDrums);
      formData.append('UOMId', pr.UomId);
      formData.append('Remarks', pr.Remarks);
      formData.append('LubricantDetails', lubeDetails);
      formData.append('GeneratorDetails', prDetails);
      formData.append('QuantityIssued', pr.QuantityIssued);
      formData.append('QuantityIssued15DegreeC', pr.QuantityIssued15DegreeC);
      formData.append('waterTest', pr.IsWaterTest);
      formData.append('visualInspection', pr.IsVisualInspection);
      formData.append('sedimentFree', pr.IsSedimentFree);
      formData.append('FuelIssuancePurchaseReceiptDetails', purDetails);
      formData.append('isDeleted',"");
      formData.append('fileURL',"");
      formData.append('BatchDetails', JSON.stringify(pr.BatchDetails));
      this.service.Save(formData).then(r =>  {
        this.success = 'Fuel Issuance Created Successfully!';
        jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
        setTimeout(function() {
          this.success = '';
          this.router.navigate(['./home/fuel-issuance-summary/list-view']);
        }.bind(this), 3000);
      }).catch(r =>  {
        this.handleError(r);
    })
  }

  private handleError(e: any) {
    console.log(e)
    this.error = e;
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }
}