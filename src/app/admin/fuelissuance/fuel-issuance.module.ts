import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { PaginationModule, TabsModule, TooltipModule } from 'ng2-bootstrap';
import { DatePickerModule } from 'ng2-datepicker';
import { MyDatePickerModule } from 'mydatepicker';

import { routing }       from './fuel-issuance.routing';

import { Ng2SimplePageScrollModule } from 'ng2-simple-page-scroll';
import { ListViewListComponent } from './ListView/list/listview-list.component';
import { ListViewComponent } from './ListView/listview.component';
import { ListViewAddComponent } from './ListView/add/listview-add.component';
import { ListViewEditComponent } from './ListView/edit/listview-edit.component';
import { ListViewFormComponent } from './ListView/listview-form/listview-form.component';
import { ListViewFormService } from './ListView/services/listview.service';
import { StockSummaryListComponent } from './stock-summary/list/stock-summary-list.component';
import { StockSummaryComponent } from './stock-summary/stock-summary.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule,
    PaginationModule,
    TabsModule,
    TooltipModule,
    DatePickerModule,
    MyDatePickerModule,
    routing,
    Ng2SimplePageScrollModule.forRoot(),
  ],
  declarations: [
    ListViewAddComponent, ListViewComponent, ListViewFormComponent, ListViewListComponent,
    ListViewEditComponent, StockSummaryListComponent, StockSummaryComponent
  ],
  entryComponents:    [ ListViewFormComponent ],
  providers: [
    ListViewFormService, 
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export default class FuelissuanceModule {}
