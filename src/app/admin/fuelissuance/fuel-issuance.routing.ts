import { Routes, RouterModule }  from '@angular/router';
import { ListViewComponent } from './ListView/listview.component';
import { ListViewListComponent } from './ListView/list/listview-list.component';
import { ListViewAddComponent } from './ListView/add/listview-add.component';
import { ListViewEditComponent } from './ListView/edit/listview-edit.component';
import { StockSummaryListComponent } from './stock-summary/list/stock-summary-list.component';
import { StockSummaryComponent } from './stock-summary/stock-summary.component';

const routes: Routes = [
      {
       path: 'list-view',
       component: ListViewComponent,
       children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          //{ path: 'list-view', component: ListViewListComponent },
          { path: 'list', component: ListViewListComponent },
          { path: 'add', component: ListViewAddComponent },
          { path: 'edit/:id', component: ListViewEditComponent },
          { path: 'view/:id', component: ListViewEditComponent },
        ]
      },
      {
        path: '',
        component: ListViewComponent,
        children: [
          { path: '', component: ListViewAddComponent },
          { path: 'stock-summary', component: StockSummaryListComponent },
        ]
  }
];

export const routing = RouterModule.forChild(routes);
