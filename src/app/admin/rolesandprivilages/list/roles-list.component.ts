import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';
import { Configuration } from 'app/app.constants';
import { SharedService }  from '../../../services/shared.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'list',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './roles-list.component.html',
})
export class RolesListComponent implements OnInit {
  statusSelected: number;
  checkArr: any[];

  roleList : any = '';
  error : any = '';
  deleteRoleId : any = '';
  deleteMsg : string = '';
  loading : boolean = false;
  public page : number = 1;
  public length : number = 0;
  public next : string = '';
  public itemsPerPage : number = 10;
  public start : number = 1;
  public maxSize : number = 5;
  public numPages : number = 2;

  public columns:Array<any> = [
    { title: 'ID', name: 'Id', sort: true },
    { title: 'Role', name: 'Role', sort: true },
    { title: 'Status', name: 'Status', sort: false, filter:true, status:0 },
    { title: 'Updated On', name: 'Updated On', sort: true },
    // { title: 'Detail', name: 'Detail', sort: false },
    { title: 'Actions', name: 'Actions', sort: false, className: ['text-center'] },
  ];
  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-bordered']
  };

  public statusList :Array<any> = [
    { id: 0, name: 'InActive', selected: false, status: false },
    { id: 1, name: 'Active', selected: false, status: true },
  ];

  params: URLSearchParams = new URLSearchParams()

  privileges : any = {
    isAdd: false,
    isDelete : false,
    isEdit : false,
    isView : false
  }
  privilegeSubscription : Subscription;
  showListPage : boolean = false;
  
  constructor(private _router : Router, private rolesService : RolesComponentService, private _config : Configuration, private _sharedService : SharedService ) {
    this.params.set('limit', this._config.itemsPerPage.toString());
  }

  ngOnInit() {
    this.statusSelected = 2;
    this.getUserRoleList( '' );
    /**user privileges**/	
    this.privilegeSubscription = this._sharedService.setUserPrivileges().subscribe((privileges)=>{
      this.privileges = privileges;
      this.showListPage = true;
    });
  }

  ngOnDestroy() {
    this.privilegeSubscription.unsubscribe();
  }

  addNewRole(){
    this._router.navigate(['./home/roles-and-privileges/add']);
  }

  getUserRoleList( params : any ){
    this.loading = true;
    this.rolesService.getUserRoleList( params ).then(response =>  {
      this.roleList = response.result;
      if( response.result.length > 0 ) {
        this.length = response['count'];
        this.next = response['next'];
      }
      this.loading = false;
      }).catch(response =>  {
        this.handleError(response);
    });
  }

  deleteUserRole(roleId){
    this.deleteRoleId = roleId;
    this.deleteMsg = '';
  }

  deleteMessageClear(){
    this.deleteMsg = '';
  }

  deleteConfirm() {
    this.rolesService.deleteUserRole(  this.deleteRoleId ).then(response =>  {
      this.deleteMsg = 'Role successfully deleted';
      }).catch(response =>  {
        this.deleteMsg = 'Failed to delete role';
        this.handleError(response);
    });
  }

  cancelModal() {
    this._router.navigate(['./home/roles-and-privileges']);
  }

  editRoles(roles){
    this._router.navigate(['./home/roles-and-privileges/edit', roles.id ]);
  }

  viewRoles(roles){
    this._router.navigate(['./home/roles-and-privileges/view', roles.id ]);
  }

  private handleError(e: any) {
    this.error = e;
    console.log('error', this.error);
    let detail = e.detail
    if(e.status == '401') {
      this._router.navigate(['./login']);
    } else if(detail && detail == 'Signature has expired.') {
      this._router.navigate(['./login']);
    }
  }

  sort(param, order){
    this.params.set('ordering', param);
    if(order === 'desc'){
      this.params.set('ordering', '-'+param);
    }
    if(order === 'asc'){
      this.params.set('ordering', param);
    }
    this.getUserRoleList(this.params);
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    let params = this.params;
    let start = (page.page - 1) * page.itemsPerPage;
    this.start = start + 1;
    params.set('limit', page.itemsPerPage);
    params.set('offset',start.toString());
    var sortParam = '';
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
      params.set(this.config.filtering.name, this.config.filtering.filterString);
    }
    var sortParam = '';
    this.config.sorting.columns.forEach( function( col, key ) {
      if(col.sort){
        if(col.sort == 'desc') {
          sortParam = sortParam != '' ? sortParam+',-'+col.name  : '-'+col.name;
        } else if(col.sort == 'asc') {
          sortParam = sortParam != '' ? sortParam+','+col.name  : col.name;
        }
      }
    });
    if(sortParam != '') {
      params.set('ordering', sortParam);
    }
    this.getUserRoleList(this.params);
  }

  filterArray = [];
  selectAll(type, event) {
    if(type === 'status') {
      this.filterArray = [];
      for (var i = 0; i < this.statusList.length; i++) {
        this.statusList[i].selected = this.selectedStatusAll;
        if(event.target.checked) {
          this.filterArray.push(this.statusList[i].status.toString());
        } else {
          this.filterArray = [];
        }
      }
       this.params.set('StatusFilter', this.filterArray.toString());
    }
  }

  selectedStatusAll;
  checkedStatus = [];
  checkIfAllSelected(option, event, item) {
    if( item === 'status') {
      this.selectedStatusAll = this.statusList.every(function(item:any) {
        return item.selected == true;
      })
      var key = event.target.value.toString();
      var index = this.checkedStatus.indexOf(key);
      if(event.target.checked) {
        this.checkedStatus.push(event.target.value);
      } else {
        this.checkedStatus.splice(index, 1);
      }
      this.params.set('StatusFilter', this.checkedStatus.toString());
    }
  }

  apply(statusSelected){
    if (this.statusSelected == 2) {
      this.getUserRoleList( '' );
    } else {
      this.rolesService.getUserRolesByStatus(statusSelected).then(response =>  {
        this.roleList = response;
        if( response.result.length > 0 ) {
          this.length = response['count'];
          this.next = response['next'];
        }
        jQuery('#ViewStatusModal').modal('toggle');
        this.loading = false;
        }).catch(response =>  {
          this.handleError(response);
      });
    }
  }
}
