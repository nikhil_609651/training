import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';

@Component({
  selector: 'roles-view',
  templateUrl: './roles-view.component.html',
})
export class RolesViewComponent implements OnInit {

    error : any = '';
    page : string = 'view';
	constructor(private _router: Router, private rolesService : RolesComponentService ) {
        
    }
    
    ngOnInit() {

    }
}
