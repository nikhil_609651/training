import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';

@Component({
  selector: 'add',
  templateUrl: './roles-add.component.html',
})
export class RolesAddComponent implements OnInit {

    error : any = '';
    page : string = 'add';
    private successMsg : string = '';
    hasErrorMsg = '';
    hasError = false;
	constructor(private _router: Router, private rolesService : RolesComponentService ) {}
    
    ngOnInit() {}
    
    onSaveRoles(requestArray) {
        this.rolesService.saveRoles(requestArray).then(response =>  {
            this.successMsg = 'Roles and Privileges saved Successfully!';
            jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
            setTimeout(function() {
                this.successMsg = '';
                this._router.navigate(['./home/roles-and-privileges']);
            }.bind(this), 3000);
            }).catch(response =>  {
                if(response.status == '400' && response.statusText == 'Bad Request') {
                    let body = JSON.parse(response._body);
                    if(body.hasOwnProperty('firstKey')){
                        if(body.firstKey instanceof Array){
                            if(body.firstKey[0] == 'Name Already Exists'){
                                this.hasError = true;
                                this.hasErrorMsg = 'Name Already Exists';
                                jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
                            }
                        }
                    }
                }
                this.handleError(response);
        });
    }

    private handleError(e: any) {
        this.error = e;
        console.log('this.error', this.error);
        let detail = e.detail
        if(e.status == '401') {
          this._router.navigate(['./login']);
        } else if(detail && detail == 'Signature has expired.') {
          this._router.navigate(['./login']);
        }
      }
}
