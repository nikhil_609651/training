import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormArray, FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';

@Component({
  selector: 'roles-form',
  templateUrl: './roles-form.component.html',
  styleUrls: ['./roles-form.component.css'],
})
export class RolesFormComponent implements OnInit {
  
  @Input() page;
  @Output() saveRolesEvent = new EventEmitter();

  rolesForm : any;
  showChild : any = [];
  data : any = [];
  rolesArray : any = [];
  private error : any = '';
  isDataAvailable : Promise<boolean>;
  isEditPage : boolean = false;
  isAddPage : boolean = false;
  roleId : number = 0;
  readOnly : boolean = false;
  isFormSave : boolean = false;
  nameRequiredMsg : string = 'Role name is required';
  showCheckBoxes : boolean = false;
  imgShow = [];
  viewCheckBox = [];
  addCheckBox = [];
  editCheckBox = [];
  delCheckBox = [];

  constructor(private _router : Router, private formBuilder: FormBuilder, private rolesService : RolesComponentService,
              private _activatedRoute : ActivatedRoute) {
              }

   ngOnInit() {
        if( this.page === 'add') {
            this.initForm();
            this.getRolesMenu();
            this.isEditPage = true;
        } else if( this.page === 'edit' ) {
            this._activatedRoute.params.subscribe( params => this.roleId = params['id']);
            this.isAddPage = true;
            this.initForm();
            this.getRolesMenu();
            this.getUserRolesById(this.roleId);
            this.readOnly = false;
        } else if( this.page === 'view' ) {
            this._activatedRoute.params.subscribe( params => this.roleId = params['id']);
            this.isAddPage = true;
            this.initForm();
            this.getRolesMenu();
            this.getUserRolesById(this.roleId);
            this.readOnly = true;
            this.showCheckBoxes = true;
        }
   }

   initForm() {
        let parentRoles = new FormArray([]);
        this.rolesForm = this.formBuilder.group({
            role : new FormControl('', Validators.required ),
            status : new FormControl(false),
            privilages :  parentRoles
        });
   }

   addParentRoles(role?) {
        let childRoles = new FormArray([]);
        let roleTitle = role.title  ? role.title : '';
        (<FormArray>this.rolesForm.controls['privilages']).push(
            new FormGroup({
                parentRoleName : new FormControl(false),
                parentRoleId : new FormControl(role.id),
                childRoles : childRoles
            })
        )
   }

   rolesFormInit(data) {
        let index = 0;
        data.forEach(role => {
            if(role.parent == null) {
                this.rolesArray.push(role);
                this.addParentRoles(role);
                this.imgShow.push('/assets/icon/delete-icon.png');
                if(this.page === 'view' || this.page === 'edit'){
                    this.showChild[index] = true;
                }
                index++;
            }
        });
        (<FormArray>this.rolesForm.controls['privilages']).controls.forEach(parent => {
            data.forEach(child => {
                if((<FormGroup>parent).value.parentRoleId == child.parent){
                    (<FormArray>(<FormGroup>parent).controls['childRoles']).push(
                        new FormGroup({
                            childRoleName : new FormControl(false),
                            childRoleId : new FormControl(child.id),
                            roles : new FormGroup({
                                view : new FormControl(false),
                                add : new FormControl(false),
                                edit : new FormControl(false),
                                delete : new FormControl(false)
                            })
                        })
                    )
                }
            });
        });
        (<FormArray>this.rolesForm.controls['privilages']).controls.forEach(parent => {
            if(parent.value.childRoles.length === 0){
                (<FormArray>(<FormGroup>parent).controls['childRoles']).push(
                    new FormGroup({
                        childRoleName : new FormControl(false),
                        childRoleId : new FormControl(parent.value.parentRoleId),
                        roles : new FormGroup({
                            view : new FormControl(false),
                            add : new FormControl(false),
                            edit : new FormControl(false),
                            delete : new FormControl(false)
                        })
                    })
                )
            }
        });
        this.rolesArray.forEach(roles => {
            roles['children'] = [];
            data.forEach(child => {
                if(roles.id == child.parent){
                    roles.children.push(child);
                }
            });
            if(roles.children.length == 0){
                roles.children.push({ 'title': roles.title, 'id': roles.id });
            }
        });
   }

   getRolesMenu() {
        this.rolesService.getRolesMenu().then(response =>  {
                this.data = response;
                this.rolesFormInit(this.data);
                this.isDataAvailable = Promise.resolve(true);
            }).catch(response =>  {
                this.handleError(response);
        });
   }

   saveUserRoles(rolesForm) {
        let requestArray = {};
        requestArray['name'] = rolesForm.controls['role'].value;
        requestArray['userRolesPrivileges'] = [];
        requestArray['activeStatus'] = rolesForm.controls['status'].value;
        if(this.roleId) {
            requestArray['id'] = this.roleId;
        }
        (<FormArray>rolesForm.controls['privilages']).controls.forEach( parent => {
            //if((<FormArray>(<FormGroup>parent).controls['parentRoleName']).value) {
                (<FormArray>(<FormGroup>parent).controls['childRoles']).controls.forEach(child => {


                    let pageId = (<FormGroup>(<FormGroup>child).controls['childRoleId']).value;
                    let userPrivileges = {};
                    userPrivileges['menuPageId'] = pageId;
                    userPrivileges['isAdd'] = false;
                    userPrivileges['isView'] = false;
                    userPrivileges['isDelete'] = false;
                    userPrivileges['isEdit'] = false;
                    if((<FormGroup>(<FormGroup>child).controls['roles']).controls['add'].value) {
                        userPrivileges['isAdd'] = true
                    }
                    if((<FormGroup>(<FormGroup>child).controls['roles']).controls['view'].value) {
                        userPrivileges['isView'] = true
                    }
                    if((<FormGroup>(<FormGroup>child).controls['roles']).controls['edit'].value) {
                        userPrivileges['isEdit'] = true
                    }
                    if((<FormGroup>(<FormGroup>child).controls['roles']).controls['delete'].value) {
                        userPrivileges['isDelete'] = true
                    }
                    requestArray['userRolesPrivileges'].push(userPrivileges);
                });
           // }
                if(parent.value['childRoles'].length > 1) {
                    for( let childRoles of (<FormArray>(<FormGroup>parent).controls['childRoles']).value) {
                        if(childRoles.roles['add'] || childRoles.roles['delete'] || childRoles.roles['edit'] || childRoles.roles['view']) {
                            let pageId = parent.value['parentRoleId'];
                            let userPrivileges = {};
                            userPrivileges['menuPageId'] = pageId;
                            userPrivileges['isAdd'] = false;
                            userPrivileges['isView'] = false;
                            userPrivileges['isDelete'] = false;
                            userPrivileges['isEdit'] = false;
                            if(childRoles.roles['add']) {
                                userPrivileges['isAdd'] = true
                            }
                            if(childRoles.roles['view']) {
                                userPrivileges['isView'] = true
                            }
                            if(childRoles.roles['edit']) {
                                userPrivileges['isEdit'] = true
                            }
                            if(childRoles.roles['delete']) {
                                userPrivileges['isDelete'] = true
                            }
                            requestArray['userRolesPrivileges'].push(userPrivileges);
                            break;
                        }
                    }
                }                
        });
        this.saveRolesEvent.emit(requestArray);
   }

   rolesData : any;
   getUserRolesById(roleId) {
        this.rolesService.getUserRolesById(roleId).then(response =>  {
            this.rolesData = response;
            this.initRolesFormById(response);
            }).catch(response =>  {
            this.handleError(response);
        });
   }

   initRolesFormById(roles) {
        let data = roles.userRolesPrivileges;
        this.rolesForm.controls['role'].setValue(roles.name);
        this.rolesForm.controls['status'].setValue(roles.activeStatus);
        if(this.readOnly) {
            this.rolesForm.controls['role'].disable();
            this.rolesForm.controls['status'].disable();
        }
        if(data) {
            data.forEach(role => {
                let parentRoleId = '';
                if(role.menuPage.parent === null) {
                    parentRoleId = role.menuPageId;
                } else {
                    parentRoleId = role.menuPage.parent;
                }
                let i = 0;
                (<FormArray>this.rolesForm.controls['privilages']).controls.forEach(parent => {
                    if((<FormGroup>parent).controls['parentRoleId'].value === parentRoleId ) {
                        //(<FormGroup>parent).controls['parentRoleName'].setValue(true);
                        (<FormArray>(<FormGroup>parent).controls['childRoles']).controls.forEach(child => {
                            if( role.menuPageId == (<FormGroup>child).controls['childRoleId'].value ) {
                                if(role.isAdd || role.isEdit || role.isDelete || role.isView) {
                                    (<FormGroup>child).controls['childRoleName'].setValue(true);
                                    (<FormGroup>parent).controls['parentRoleName'].setValue(true);
                                }
                                (<FormGroup>(<FormGroup>child).controls['roles']).controls['add'].setValue(role.isAdd);
                                (<FormGroup>(<FormGroup>child).controls['roles']).controls['edit'].setValue(role.isEdit);
                                (<FormGroup>(<FormGroup>child).controls['roles']).controls['delete'].setValue(role.isDelete);
                                (<FormGroup>(<FormGroup>child).controls['roles']).controls['view'].setValue(role.isView);
                                this.viewCheckBox[i] = [];
                                this.addCheckBox[i] = [];
                                this.editCheckBox[i] = [];
                                this.delCheckBox[i] = [];
                            }
                        });
                    }
                    if(this.readOnly) {
                        (<FormArray>(<FormGroup>parent).controls['childRoles']).controls.forEach(child => {
                            (<FormGroup>child).controls['childRoleName'].disable();
                            (<FormGroup>(<FormGroup>child).controls['roles']).controls['add'].disable();
                            (<FormGroup>(<FormGroup>child).controls['roles']).controls['edit'].disable();
                            (<FormGroup>(<FormGroup>child).controls['roles']).controls['delete'].disable();
                            (<FormGroup>(<FormGroup>child).controls['roles']).controls['view'].disable();
                        });
                    }
                    i++;
                });
            });
        }
   }

   private handleError(e: any) {
        this.error = e;
        console.log('this.error', this.error);
        let detail = e.detail
        if(e.status == '401'){
        this._router.navigate(['./login']);
        }else
        if(detail && detail == 'Signature has expired.'){
        this._router.navigate(['./login']);
        }
   }
    
   showChildRoles(index) {
        this.showChild[index] = !this.showChild[index];
        if (this.showChild[index]) {
            this.imgShow[index]='/assets/icon/delete-icon.png';
        } else {
            this.imgShow[index]='/assets/icon/add-icon.png';
        }
   }

   resetForm() {
        this.rolesForm.reset();
        this.showChild.forEach(element => {
            element = false;
        });
   }

   saveRoles(rolesForm) {
        this.isFormSave = true;
        if( this.rolesForm.valid ) {
           this.saveUserRoles(rolesForm);
        } else {
            jQuery('html, body').animate({scrollTop:0}, {duration: 1000});
        }
   }

   cancelForm() {
        this._router.navigate(['./home/roles-and-privileges']);
   }

   selectParent(i, j, event, type) {
        if(type == 'view') {
            this.viewCheckBox[i][j] = event.target.checked;
        }
        if(type == 'add') {
            this.addCheckBox[i][j] = event.target.checked;
        }
        if(type == 'edit') {
            this.editCheckBox[i][j] = event.target.checked;
        }
        if(type == 'delete') {
            this.delCheckBox[i][j] = event.target.checked;
        }
        let roleControls = (<FormGroup>(<FormGroup>(<FormArray>(<FormGroup>(<FormArray>this.rolesForm.controls['privilages']).controls[i]).controls['childRoles']).controls[j]).controls['roles']).value;
        let parentControl =(<FormGroup>(<FormArray>(<FormGroup>(<FormArray>this.rolesForm.controls['privilages']).controls[i]).controls['childRoles']).controls[j]).controls['childRoleName'];
        parentControl.setValue(this.viewCheckBox[i][j] || this.editCheckBox[i][j] || this.addCheckBox[i][j] || this.delCheckBox[i][j]);
   }
}
