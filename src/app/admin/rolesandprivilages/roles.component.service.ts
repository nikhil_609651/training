import {Injectable} from '@angular/core';

import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from 'app/app.constants';

@Injectable()

export class RolesComponentService{
    listRolesMenu = '';
    saveRolesUrl = '';
    userRoleList = '';
    deleteUserRoleById = '';
    getRoleById = '';
    getRoleListByStatusUrl = '';
    
    constructor( private _authHttp : AuthHttp, private _config : Configuration ){
        this.listRolesMenu = _config.ServerWithApiUrl + 'UserManagement/Menu';
        this.saveRolesUrl = _config.ServerWithApiUrl + 'UserManagement/SaveUserRoles';
        this.userRoleList = _config.ServerWithApiUrl + 'UserManagement/UserRoles/';
        this.deleteUserRoleById = _config.ServerWithApiUrl + 'UserManagement/DeleteUserRolesByID';
        this.getRoleById = _config.ServerWithApiUrl + 'UserManagement/GetUserRolesByID';
        this.getRoleListByStatusUrl = _config.ServerWithApiUrl + 'UserManagement/SearchUserRolesByStatus';
    }

    private handleError(error: any) {
        return Promise.reject(error);
    }
    getUserRolesByStatus(Search): Promise<any>{
        return this._authHttp.get(this.getRoleListByStatusUrl + '?Search=' + Search)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getRolesMenu(): Promise<any>{
        return this._authHttp.get(this.listRolesMenu)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    saveRoles(params): Promise<any>{
        return this._authHttp.post(this.saveRolesUrl, params)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getUserRoleList( params ): Promise<any> {
        return this._authHttp.get(this.userRoleList, {search: params})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    deleteUserRole(id): Promise<any>{
        return this._authHttp.post(this.deleteUserRoleById + '?id=' + id, '' )
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getUserRolesById(roleId): Promise<any>{
        return this._authHttp.get(this.getRoleById + '?id=' + roleId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }
}