import {Component, ViewEncapsulation} from '@angular/core';

import { Router } from '@angular/router';
import { RolesComponentService } from 'app/admin/rolesandprivilages/roles.component.service';

@Component({
  selector: 'roles-and-privileges',
    template: `<router-outlet></router-outlet>`
})
export class RolesComponent {

	constructor(private _router: Router, private rolesService : RolesComponentService ) {
    }
}
