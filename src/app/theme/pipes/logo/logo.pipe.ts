import {Pipe, PipeTransform} from '@angular/core';
import {layoutPaths} from '../../../theme';

@Pipe({name: 'logo'})
export class LogoPipe implements PipeTransform {

  transform(input:string, ext = 'svg'):string {
    return layoutPaths.images.root + input + '.' + ext;
  }
}
