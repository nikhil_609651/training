import {Component, ViewEncapsulation} from '@angular/core';
import {GlobalState} from '../../../global.state';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie';



@Component({
  selector: 'ba-page-top',
  styles: [require('./baPageTop.scss')],
  template: require('./baPageTop.html'),
  encapsulation: ViewEncapsulation.None
})
export class BaPageTop {

  public isScrolled:boolean = false;
  public isMenuCollapsed:boolean = false;
  userName : string = '';

  interval;
  constructor(private _state:GlobalState, private router: Router ) {
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
    this.refreshData();
    /*this.interval = setInterval(() => { 
        this.refreshData(); 
    }, 10000);*/
    this.userName = localStorage.getItem('given_name');
  }


  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }

  public signOut() {
    sessionStorage.removeItem('BUFilter');
    sessionStorage.removeItem('CountryFilter');
    sessionStorage.removeItem('POLFilter');
    sessionStorage.removeItem('SiteFilter');
    location.reload();
    this.router.navigateByUrl('/');
  }

  notificationCount = 0;
  private refreshData(): void {
    this.notificationCount = Math.ceil(Math.random()*10);
  }
}
