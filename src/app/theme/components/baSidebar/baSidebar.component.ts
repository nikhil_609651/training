import {Component, ElementRef, HostListener, ViewEncapsulation} from '@angular/core';
import {GlobalState} from '../../../global.state';
import {layoutSizes} from '../../../theme';
import {MENU} from '../../../../app/app.menu';
import { Router } from '@angular/router';

import {NavigationService} from '../../../services/navigation.service';
import * as _ from 'lodash';

@Component({
  selector: 'ba-sidebar',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./baSidebar.scss')],
  template: require('./baSidebar.html')
})
export class BaSidebar {

  // here we declare which routes we want to use as a menu in our sidebar
  //public routes = _.cloneDeep(MENU); // we're creating a deep copy since we are going to change that object

  public routes: any;
  public menuHeight:number;
  public isMenuCollapsed:boolean = false;
  public isMenuShouldCollapsed:boolean = false;
  public items = [];
  public user: any;

  constructor(private _elementRef:ElementRef, private _state:GlobalState, private nav: NavigationService, private router: Router,) {

    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

  }


  public ngOnInit() {

    const ADMIN_MENU = [
      {
        path: 'home',
        children: []
      }
    ]
    this.nav.getNav().then(response => {
      const Nav = response;
      if(Nav.length > 0){
          Nav.forEach((item) => {
              var temp = {
                          path: this.slugify(item.title),
                          data: {
                                menu: {
                                  title: item.title,
                                  order: item.sort_order,
                                }
                          },
                          children: [],
                          menuId: item.id
                      };

              if(item.parent){
                  if(!this.items[item.parent]){
                      this.items[item.parent] = {
                          path: '',
                          data: {
                                menu: {
                                  title: '',
                                  order: '',
                                  icon: '',
                                  selected: false,
                                  expanded: false
                                }
                          },
                          children: [],
                      }
                  }

                  this.items[item.parent].children.push(temp)
                  this.items[item.parent].children.sort(this.GetSortOrder("order"));
              }
              else{
                  if(!this.items[item.id]){
                      this.items[item.id] = {
                          path: '',
                          data: {
                                menu: {
                                  title: '',
                                  order: '',
                                  icon: '',
                                  selected: false,
                                  expanded: false
                                }
                          },
                          children: []
                      };
                  }
                  this.items[item.id].path = this.slugify(item.title);
                  this.items[item.id].data.menu.title = item.title;
                  this.items[item.id].data.menu.order = item.sort_order;
                  this.items[item.id].data.menu.icon = item.hint;
                  this.items[item.id].menuId = item.id;
              }
          });
          this.items.sort(this.GetSortOrder("order"));
      }
      ADMIN_MENU[0].children =this.items
      this.routes = ADMIN_MENU;
    })
    .catch(r =>  {
      this.handleError(r);
    });
    if (this._shouldMenuCollapse()) {
      this.menuCollapse();
    }
  }

  public ngAfterViewInit():void {
    setTimeout(() => this.updateSidebarHeight());
  }

  @HostListener('window:resize')
  public onWindowResize():void {

    var isMenuShouldCollapsed = this._shouldMenuCollapse();

    if (this.isMenuShouldCollapsed !== isMenuShouldCollapsed) {
      this.menuCollapseStateChange(isMenuShouldCollapsed);
    }
    this.isMenuShouldCollapsed = isMenuShouldCollapsed;
    this.updateSidebarHeight();
  }

  public menuExpand():void {
    this.menuCollapseStateChange(false);
  }

  public menuCollapse():void {
    this.menuCollapseStateChange(true);
  }

  public menuCollapseStateChange(isCollapsed:boolean):void {
    this.isMenuCollapsed = isCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
  }

  public updateSidebarHeight():void {
    // TODO: get rid of magic 84 constant
    // Decreased the magic constant from 84 to 74 from menu height as the menu was not visibile
    this.menuHeight = this._elementRef.nativeElement.childNodes[0].clientHeight - 104;
  }

  private _shouldMenuCollapse():boolean {
    return window.innerWidth <= layoutSizes.resWidthCollapseSidebar;
  }

  public slugify(s: string): any {
      if (!s) { return ''; }
      let ascii = [];
      let ch, cp;
      for (var i = 0; i < s.length; i++) {
        if ((cp = s.charCodeAt(i)) < 0x180) {
          ch = String.fromCharCode(cp);
          ascii.push(ch);
        }
      }
      s = ascii.join('');
      s = s.replace(/[^\w\s-]/g, '').trim().toLowerCase();

      return s.replace(/[-\s]+/g, '-');
  }

  public GetSortOrder(prop) {

      return function(a, b) {
          if (a.data.menu.order > b.data.menu.order) {
              return 1;
          } else if (a.data.menu.order < b.data.menu.order) {
              return -1;
          }
          return 0;
      }
  }
  private handleError(e: any) {
    console.log(e)
      let detail = e.detail
      if(detail && detail == 'Signature has expired.'){
        this.router.navigate(['./']);
      }
  }

}
