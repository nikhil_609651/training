import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import { SharedService } from 'app/services/shared.service';

@Component({
  selector: 'ba-menu-item',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./baMenuItem.scss')],
  template: require('./baMenuItem.html')
})
export class BaMenuItem {

  @Input() menuItem:any;
  @Input() child:boolean = false;

  @Output() itemHover = new EventEmitter<any>();
  @Output() toggleSubMenu = new EventEmitter<any>();

  constructor(private _service : SharedService) {

  }
  public onHoverItem($event):void {
    this.itemHover.emit($event);
  }

  public onToggleSubMenu($event, item):boolean {
    $event.item = item;
    this.toggleSubMenu.emit($event);
    return false;
  }

  onMenuClick(subItem) {
    localStorage.setItem('menuId', subItem.route.menuId);
    this._service.changeMenu(subItem.route.menuId);
  }
}
