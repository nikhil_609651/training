import {Component, ViewEncapsulation, Input, Output, EventEmitter} from '@angular/core';
import {Router, Routes, NavigationEnd} from '@angular/router';
import {Subscription} from 'rxjs/Rx';

import {BaMenuService} from './baMenu.service';
import {GlobalState} from '../../../global.state';
import {AuthService} from "../../../services/auth.service";
import {LocalStorageService, SessionStorageService} from 'ng2-webstorage';
import { SharedService } from 'app/services/shared.service';


@Component({
  selector: 'ba-menu',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./baMenu.scss')],
  template: require('./baMenu.html'),
  providers: [BaMenuService]
})
export class BaMenu {

  @Input() menuRoutes:Routes = [];
  @Input() sidebarCollapsed:boolean = false;
  @Input() menuHeight:number;

  @Output() expandMenu = new EventEmitter<any>();

  public menuItems:any[];
  public showHoverElem:boolean;
  public hoverElemHeight:number;
  public hoverElemTop:number;
  protected _onRouteChange:Subscription;
  public outOfArea:number = -200;
  public user: any;

  constructor(private storage:LocalStorageService, private _router:Router, private authService: AuthService, private _service:BaMenuService, private _state:GlobalState, private _sharedService:SharedService) {
    this._onRouteChange = this._router.events.subscribe((event) => {

      if (event instanceof NavigationEnd) {
        if (this.menuItems) {
          this.selectMenuAndNotify();
        } else {
          // on page load we have to wait as event is fired before menu elements are prepared
          setTimeout(() => this.selectMenuAndNotify());
        }
      }
    });

    //const token = this.authService.getDecodedToken();

    var data = sessionStorage.getItem('user');
    if(data){
      this.user = JSON.parse(data);
    }
  }

  public selectMenuAndNotify():void {
    if (this.menuItems) {
      this.menuItems = this._service.selectMenuItem(this.menuItems);
      this._state.notifyDataChanged('menu.activeLink', this._service.getCurrentItem());
    }
  }

  public ngOnInit():void {
    this.menuRoutes[0].children.forEach(menu => {
      if(!menu['path']) {
        let index = this.menuRoutes[0].children.map(function(e) { return e['menuId']; }).indexOf(menu['menuId']);
        this.menuRoutes[0].children.splice(index, 1);
      }
    });
    this.menuItems = this._service.convertRoutesToMenus(this.menuRoutes);
    this.storage.observe('user')
    .subscribe((value) => {
      this.user = value;
    });
  }

  public ngOnDestroy():void {
    this._onRouteChange.unsubscribe();
  }

  public hoverItem($event):void {
    this.showHoverElem = true;
    this.hoverElemHeight = $event.currentTarget.clientHeight;
    // TODO: get rid of magic 66 constant
    this.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - 66;
  }

  public toggleSubMenu($event):boolean {
    var submenu = jQuery($event.currentTarget).next();

    if (this.sidebarCollapsed) {
      this.expandMenu.emit(null);
      if (!$event.item.expanded) {
        $event.item.expanded = true;
      }
    } else {
      $event.item.expanded = !$event.item.expanded;
      submenu.slideToggle();
    }

    return false;
  }

  onMenuClick(item) {
    if(!item.hasOwnProperty('children')) {
      localStorage.setItem('menuId', item.route.menuId);
      this._sharedService.changeMenu(item.route.menuId);
    }
  }

  doLogout() {
    this.authService.logout();
  }
}
