import { NgModule, ModuleWithProviders }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgTableFilteringDirective } from '../shared/table/ng-table-filtering.directive';
import { NgTablePagingDirective } from '../shared/table/ng-table-paging.directive';
import { NgTableSortingDirective } from '../shared/table/ng-table-sorting.directive';
import { OverlayLoaderComponent } from '../shared/overlay-loader.component';

import {
  BaThemeConfig
} from './theme.config';

import {
  BaThemeConfigProvider
} from './theme.configProvider';

import {
  BaAmChart,
  BaBackTop,
  BaMenuItem,
  BaMenu,
  BaPageTop,
  BaSidebar
} from './components';

import { BaCardBlur } from './components/baCard/baCardBlur.directive';

import {
  BaScrollPosition,
  BaSlimScroll,
  BaThemeRun
} from './directives';


import {
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
} from './services';
import { SharedService } from 'app/services/shared.service';


const NGA_COMPONENTS = [
  BaAmChart,
  BaBackTop,
  BaMenuItem,
  BaMenu,
  BaPageTop,
  BaSidebar,
  OverlayLoaderComponent,
  NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective  
];

const NGA_DIRECTIVES = [
  BaScrollPosition,
  BaSlimScroll,
  BaThemeRun,
  BaCardBlur
];


const NGA_SERVICES = [
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner
];


@NgModule({
  declarations: [
    ...NGA_DIRECTIVES,
    ...NGA_COMPONENTS
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    ...NGA_DIRECTIVES,
    ...NGA_COMPONENTS
  ],
  providers : [
    SharedService
  ]
})
export class NgaModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: NgaModule,
      providers: [
        BaThemeConfigProvider,
        BaThemeConfig,
        ...NGA_SERVICES
      ],
    };
  }
}
