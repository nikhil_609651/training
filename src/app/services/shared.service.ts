import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../app.constants';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';

@Injectable()
export class SharedService {

    public getPrivilegesUrl = '';
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) { 
	    this.getPrivilegesUrl = _configuration.ServerWithApiUrl + 'UserManagement/GetRolesAndPrivilagesByUserId';
    }

    getStylePriceOptions() {
        return this.authHttp.get(this._configuration.ServerWithApiUrl+'style_price_options/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getCountryStateList() {
        return this.authHttp.get(this._configuration.ServerWithApiUrl+'country_state_list/')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUserGroups(): Promise<any> {
        return this.authHttp.get(this._configuration.ServerWithApiUrl+'groups')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getUserSettings(params: any): Promise<any> {
        return this.authHttp.get(this._configuration.ServerWithApiUrl+'user_settings', { search: params })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    /**get the menu id when a menu is clicked */
    private menuId = new BehaviorSubject(localStorage.getItem('menuId'));
    selectedMenuId = this.menuId.asObservable();
  
    changeMenu(menuId) {
        this.menuId.next(menuId);
    }

    /*private privileges = new BehaviorSubject({});
    getPrivileges = this.privileges.asObservable();
  
    setPrivileges(menuId) {
        this.privileges.next(menuId);
    }*/

	getRolesAndPrivilagesByUserId(roleId, menuId): Promise<any> {
        let privileges : any = {
            isAdd: false,
            isDelete : false,
            isEdit : false,
            isView : false
        }
        return this.authHttp.get(this.getPrivilegesUrl + '?UserId=' + roleId)
        .toPromise()
        .then(response => 
            {
                response = response.json();
                let userPrivileges = response[0];
                if(userPrivileges['roleName'].toUpperCase() === 'ADMIN') {
                    privileges['isAdd'] = true;
                    privileges['isDelete'] = true;
                    privileges['isEdit'] = true;
                    privileges['isView'] = true;
                    return privileges;
                } else {
                    for(let privileges of userPrivileges.privilageDetails) {
                        if(privileges.menuPageId == menuId) {
                            privileges['isAdd'] = privileges.isAdd;
                            privileges['isDelete'] = privileges.isDelete;
                            privileges['isEdit'] = privileges.isEdit;
                            privileges['isView'] = privileges.isView;
                            return privileges;
                        }
                    }
                }
                }
            )
        .catch(this.handleError);
    }

    menuSubscription: Subscription;
    setUserPrivileges() : any {
        let userId = localStorage.getItem('user_nameId');
        let privileges = new Subject<any>();
        this.menuSubscription = this.selectedMenuId.subscribe(menuId => {
            this.getRolesAndPrivilagesByUserId(userId, menuId).then(response =>  {
                privileges.next(response);
            }).catch(response =>  {
                this.handleError(response);
            });
        });
        //this.menuSubscription.unsubscribe();
        return privileges.asObservable();
    }

    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}