import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';
import { Configuration } from '../app.constants';

@Injectable()
export class NavigationService {
    UId: string;
    public items = [];
    constructor(private authHttp: AuthHttp, private _configuration: Configuration) { }
    getNav() {
        this.UId = localStorage.getItem('user_nameId');
        return this.authHttp.get(this._configuration.ServerWithApiUrl+'UserManagement/GetMenuPageByRoleId' +'?UserID=' + this.UId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }
    private handleError(error: any) {
        return Promise.reject(error.json());
    }
}