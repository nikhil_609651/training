// auth.service.ts
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Configuration } from '../app.constants';
import { tokenNotExpired, JwtHelper, AuthHttp } from 'angular2-jwt';
import {LocalStorageService, SessionStorageService} from 'ng2-webstorage';
import { CookieService } from 'angular2-cookie';

@Injectable()
export class AuthService {
  private loggedIn = false;
  private actionUrl: string;
  private forgotPasswordUrl:string;
  jwtHelper: JwtHelper = new JwtHelper();
  idToken: string;
  refreshSubscription: any;
  public messageUrl ='';
  constructor(private storage:LocalStorageService, private authHttp: AuthHttp, private http: Http, private _configuration: Configuration, private router: Router) {
    this.loggedIn = !!localStorage.getItem('id_token');
    this.actionUrl = _configuration.ServerWithApiUrl + 'UserManagement/Login';
    this.messageUrl = _configuration.ServerWithApiUrl + 'SystemPreference/GetSystemPreferenceBytodayDate';
    this.forgotPasswordUrl=_configuration.ServerWithApiUrl + 'UserManagement/ForgetPassword?Email=';
    let token = localStorage.getItem('id_token');
    if(token){
      this.idToken = token;
    }
  }

  login(userName: string, password: string): Promise<boolean> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.actionUrl,{ userName: userName, password: password }).toPromise()
            .then(response => {
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    localStorage.setItem('id_token', token);
                    this.idToken = token;
                    const user = this.jwtHelper.decodeToken(token);
                    localStorage.setItem('given_name', user.given_name);
                    localStorage.setItem('user_nameId', user.nameid);
                    sessionStorage.removeItem('user');

                    //this.getCurrentUser();
                    this.scheduleRefresh();
                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            })
            .catch(this.handleError);
  }

  public scheduleRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token
    let source = Observable.of(this.idToken).flatMap(
      token => {
        // The delay to generate in this case is the difference
        // between the expiry time and the issued at time
        let jwtIat = this.jwtHelper.decodeToken(token).orig_iat;
        let jwtExp = this.jwtHelper.decodeToken(token).exp;
        let iat = new Date(0);
        let exp = new Date(0);
        let delay = (exp.setUTCSeconds(jwtExp) - iat.setUTCSeconds(jwtIat));
        return Observable.interval(delay);
      });
    this.refreshSubscription = source.subscribe(() => {
      this.getNewJwt();
    });
  }

  public startupTokenRefresh() {
    // If the user is authenticated, use the token stream
    // provided by angular2-jwt and flatMap the token
    if (this.authenticated()) {
      let source = Observable.of(this.idToken).flatMap(
        token => {
          // Get the expiry time to generate
          // a delay in milliseconds
          let now: number = new Date().valueOf();
          let jwtExp: number = this.jwtHelper.decodeToken(token).exp;
          let exp: Date = new Date(0);
          exp.setUTCSeconds(jwtExp);
          let delay: number = exp.valueOf() - now;
          // Use the delay in a timer to
          // run the refresh at the proper time
          return Observable.timer(delay);
        });

       // Once the delay time from above is
       // reached, get a new JWT and schedule
       // additional refreshes
       source.subscribe(() => {
         this.getNewJwt();
         this.scheduleRefresh();
       });
    }
  }

  public unscheduleRefresh() {
    // Unsubscribe from the refresh
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  public getNewJwt() {
    // Get a new JWT from Auth0 using the refresh token saved
    // in local storage

    // this.http.post(this._configuration.ServerWithApiUrl +'api-token-refresh/',{ token:  this.idToken}).toPromise()
    //         .then(response => {
    //             let token = response.json() && response.json().token;

    //             if (token) {
    //                 // set token property
    //                 localStorage.setItem('id_token', token);
    //                 this.idToken = token;
    //             }
    //         })
    //         .catch(this.handleError);

  }

  getUserInfo(user_id) {
    return this.authHttp.get(this._configuration.ServerWithApiUrl+'users/'+user_id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }

  getCurrentUser() {
    var data = sessionStorage.getItem('user');
    if(data){
      return JSON.parse(data);
    }
    else{
      const user = this.jwtHelper.decodeToken(localStorage.getItem('id_token'));
      this.getUserInfo(user.user_id).then(response => {
          sessionStorage.setItem('user', JSON.stringify(response));
          this.storage.store('user', response);
          return response;
      });
    }
  }

  getDecodedToken() {
    const user = this.jwtHelper.decodeToken(localStorage.getItem('id_token'));
    return user;
  }

  logout(): void {
    localStorage.removeItem('id_token');
    localStorage.removeItem('menuId');
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('dashboard');
    sessionStorage.removeItem('userSettings');
    sessionStorage.removeItem('orderTabId');
    sessionStorage.removeItem('userOrderTabs');
    sessionStorage.removeItem('user_nameId');
    this.idToken = null;

    // Unschedule the token refresh
    this.unscheduleRefresh();

    //this.router.navigate(['./']);
  }

  sendResetPwdMail( email ) {
    let headers = new Headers({});
    let options = new RequestOptions({ headers });
    return this.authHttp
    .post(this.forgotPasswordUrl + email.toString() , options)
    .toPromise()
    .then(response => {
        console.log('response', response);
        return response.json();
    })
    .catch(this.handleError);
  }

  isLoggedIn() {
    return tokenNotExpired();
  }

  getMessageList(): Promise<any> {
    return this.authHttp.get(this.messageUrl)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  
  public authenticated() {
    return tokenNotExpired('id_token', this.idToken);
  }

  private handleError(error: any) {
      return Promise.reject(error);
  }
}