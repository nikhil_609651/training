import { Component } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

@Component({
  selector: "breadcrumbs",
  template: `
    <ul *ngIf="segments.length > 0" class="breadcrumb">
      <li *ngFor="let segment of segments; let last = last; let index = index;" 
          [ngClass]="{ 'active' : last }"> <!-- disable link of last item -->
        <span *ngIf="(segment.title == 'receipt and transfer' || segment.title == 'fuel issuance summary' || segment.title == 'master')">{{segment.title}}</span><!-- disable link of receipt and transfer, master and fuel issuance summary -->
        <a *ngIf="!last && !(segment.title == 'receipt and transfer' || segment.title == 'fuel issuance summary' || segment.title == 'master')" (click)="navigateTo(segment.route)">{{segment.title}}</a>
        <span *ngIf="last">{{segment.title}}</span>
      </li>
    </ul>`
})
export class BreadcrumbsComponent {
  private segments= new Array();

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.router.events
      .subscribe(event => {
        if(event instanceof NavigationEnd) {
          this.routeChanged(event);
        }
      });
  }

  public routeChanged(event: any) {
    this.segments.length = 0;
    this.generateBreadcrumbTrail(this.router.routerState.root);
  }

  public generateBreadcrumbTrail(route: ActivatedRoute): void {
    route.children.forEach(childRoute => {

      if (childRoute.outlet === "primary") {
        if (childRoute.snapshot.url.length > 0) {
          childRoute.snapshot.url.map(segment => {
            if(isNaN(parseInt(segment.path))){
              let title = segment.path;
              title = title.replace(/-/g , " ");
              this.segments.push({'title': title, 'route': childRoute});
            }
          })
        }
        this.generateBreadcrumbTrail(childRoute);
      }
    });
  }



  public navigateTo(route: ActivatedRoute): void {
    this.router.navigateByUrl(this.buildUrl(route));
  }

  public buildUrl(route: ActivatedRoute): string {
    let url = "";
    route.pathFromRoot.forEach((parentRoute: ActivatedRoute) => {
      if (parentRoute.snapshot.url.length > 0) {
        url += "/" + parentRoute.snapshot.url.map(segment => segment.path).join("/");
      }
    });
    return url;
  }

}