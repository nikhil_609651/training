import { NgTableFilteringDirective } from './table/ng-table-filtering.directive';
import { NgTablePagingDirective } from './table/ng-table-paging.directive';
import { NgTableSortingDirective } from './table/ng-table-sorting.directive';
export const NG_TABLE_DIRECTIVES = [NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective];