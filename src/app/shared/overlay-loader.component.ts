import {Component} from '@angular/core';

@Component({
  selector: 'overlay-loader',
  template: `
			<div class="ng-busy-default-wrapper">
			    <div class="ng-busy-default-sign">
			        <div class="loader-inner ball-clip-rotate"><div></div></div>
			    </div>
			</div>
			<div class="ng-busy-backdrop"></div>`

})
export class OverlayLoaderComponent {

}
