// access.guard.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild , ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AccessGuard implements CanActivate, CanActivateChild {

    permissions;
    currentRoute;
    constructor(private authService:AuthService,private router:Router){
        //this.permissions = this.authService.getPermissions();
        this.permissions = [];
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.checkHavePermission(state.url);
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        return this.checkHavePermission(state.url);
    }

    private checkHavePermission(url){
        //console.log(url)
        return true;
        /*
        switch (true) {
            case url.match(/^\/panel\/users[\/.*]?/):
                return this.getPermission('user.view');
            case url.match(/^\/panel\/dashboard/):
                return true;
            case url.match(/^\/panel\/permissions/):
                return this.getPermission('permissions.manager');
            case url.match(/^\/panel\/candidates[\/.*]?/):
                return this.getPermission('candidate.view');
        }*/
    }


    getPermission(perm){
        for(var i=0;i<this.permissions.length;i++){
            if(this.permissions[i].name == perm ){
                return true;
            }
        }
        return false;
    }

}