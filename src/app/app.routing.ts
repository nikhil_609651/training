import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
	{ path: 'login', redirectTo: '', pathMatch: 'full' },
	{
		path: '',
		loadChildren: () => System.import('./admin/login/login.module')
	},
	{
		path: 'home',
		loadChildren: () => System.import('./admin/admin.module')
	},
];

export const routing = RouterModule.forRoot(routes, { useHash: true });
